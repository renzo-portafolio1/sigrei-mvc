﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{

    public class JQGridModels<T>
    {
        public JQGridModels()
        { }

        public JQGridModels(int _nropaginas, int _pagina, int _total, IEnumerable<T> _lista)
        {
            Total = _nropaginas;
            Page = _pagina;
            Records = _total;
            Rows = _lista;
        }
        public int Records { get; set; }
        public int Page { get; set; }
        public int Total { get; set; }
        public IEnumerable<T> Rows;
    }
    public class JQGridSearchFilterItem
    {
        public string Field { get; set; }
        public string Op { get; set; }
        public string Data { get; set; }
    }
    public class JQGridSearchFilter
    {
        public string GroupOp { get; set; }
        public List<JQGridSearchFilterItem> Rules { get; set; }
    }

    public class PaginacionBE<T>
    {
        public PaginacionBE(int pagActual, int RegxPagina, int totalReg, IEnumerable<T> registros)
        {
            PaginaActual = pagActual;
            TotalRegistros = totalReg;
            Registros = registros;
            RegistrosPagina = RegxPagina;
        }

        public int PaginaActual
        {
            get;
            private set;
        }

        public int TotalRegistros
        {
            get;
            private set;
        }

        public IEnumerable<T> Registros
        {
            get;
            private set;
        }

        private int totalPaginas;

        public int TotalPaginas
        {
            get
            {
                if (totalPaginas == 0)
                {
                    totalPaginas = (int)Math.Ceiling((double)TotalRegistros / RegistrosPagina);
                }

                return totalPaginas;
            }
        }

        public int RegistrosPagina
        {
            get;
            private set;
        }

        public bool TieneAnterior()
        {
            return PaginaActual > 1;
        }

        public bool TieneSiguiente()
        {
            return PaginaActual < TotalPaginas;
        }

        public bool TienePrimero()
        {
            return PaginaActual != 1;
        }

        public bool TieneUltimo()
        {
            return PaginaActual != TotalPaginas && TotalPaginas > 0;
        }
    }


    public class FiltroBusquedaBE
    {
        public int RegistrosxPagina { get; set; }
        public int Pagina { get; set; }
        public int Total { get; set; }
        public string Orden { get; set; }
        public bool EsBusqueda { get; set; }
        //public long IdPerfil { get; set; }
        //public string Usuario { get; set; }
        public string IpCliente { get; set; }
        public int PaginaInvocadora { get; set; }
        //public long Id_Expediente { get; set; }
    }
}
