﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_EIR : BE_AUDITORIA_DTO
    {
        public BE_EIR() { }

        public int nroItem { get; set; }
        public String nroImei { get; set; }
      
        public String periodo { get; set; }
        public String mes { get; set; }
        public String anio { get; set; }
        public String nombreConcesionaria { get; set; }
        public Int32 opcion { get; set; }
        public String valores { get; set; }
        public String valoresUno { get; set; }
        public String valoresDos { get; set; }
        public String valoresTres { get; set; }
        public String valoresCuatro { get; set; }
        public Int32 tipoConsulta { get; set; }
        public String fechaRegistro { get; set; }
    }
}