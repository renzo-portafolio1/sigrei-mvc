﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_GSMA : BE_AUDITORIA_DTO
    { 
        public String modeloComercial { get; set; }
        public String modeloTecnico { get; set; }
        public String marca { get; set; }

        public String nroTAc { get; set; }

        public String nroImei { get; set; }

        public int nroItem { get; set; }
        public Int32 opcion { get; set; }

        public BE_GSMA(){}

    }
}