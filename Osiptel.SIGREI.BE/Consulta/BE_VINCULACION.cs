﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_VINCULACION : BE_AUDITORIA_DTO
    {
        public BE_VINCULACION() { }

        public int nroItem { get; set; }
        public String nroImei { get; set; }
        public String nroImsi { get; set; }
        public String periodo { get; set; }
        public String mes { get; set; }
        public String anio { get; set; }
        public String nroTelefonoMovil { get; set; }
        public String nombreConcesionaria { get; set; }

        public String fechaVinculacionVoz { get; set; }
        public String fechaVinculacionDatos { get; set; }
        public Int32 opcion { get; set; }
        public Int32 tipoConsulta { get; set; }
        public String valores { get; set; }
        public String valoresUno { get; set; }
        public String valoresDos { get; set; }
        public String valoresTres { get; set; }
        public String valoresCuatro { get; set; }
    }
}