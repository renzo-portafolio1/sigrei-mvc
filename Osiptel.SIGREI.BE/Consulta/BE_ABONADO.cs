﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_ABONADO : BE_AUDITORIA_DTO
    {
        public BE_ABONADO() { }

        public int nroItem { get; set; }
        public String nroImei { get; set; }
        public String marcaEquipo { get; set; }

        public String modeloEquipo { get; set; }
        public String nroImsi { get; set; }
        public String nombreConcesionaria { get; set; }
        public String nroServicioMovil { get; set; }
        public String nroTelefonoReporta { get; set; }
        public String descModalidadContrato { get; set; }
        public String nombresAbonado { get; set; }
        public String ApPaternoAbonado { get; set; }
        public String ApMaternoAbonado { get; set; }

        public String razonSocial { get; set; }
        public String tipoDocLegal { get; set; }
        public String nroDocLegal { get; set; }
        public String nombreRepLegal { get; set; }

        public String ApPatenoRepLegal { get; set; }
        public String ApMatenoRepLegal { get; set; }

        public String tipoDocRepLegal { get; set; }
        public String nroDocRepLegal { get; set; }

        public String fechaVinculacion { get; set; }
        public String fechaActivacion { get; set; }

        public String estadoServicio { get; set; }

        public String fechaReporte { get; set; }
        public String fechaBloqueoDesbloqueo { get; set; }


        public String paisReporte { get; set; }
        public String descMotivoReporte { get; set; }

        public String fechaVinculacionInicio { get; set; }
        public String fechaVinculacionFin { get; set; }
        public String fechaActivacionInicio { get; set; }
        public String fechaActivacionFin { get; set; }
        public Int32 opcion { get; set; }

        public String vinculacion { get; set; }
        public String tipoAbonado { get; set; }

        public String fechaReportaInicio { get; set; }
        public String fechaReportaFin { get; set; }
        public String fechaBloqueoInicio { get; set; }
        public String fechaBloqueoFin { get; set; }

        public String fuenteReporte { get; set; }
        public String motivoReporte { get; set; }
        public String codigoReporte { get; set; }
        public Int32 idPerfil { get; set; }

        public String valores { get; set; }
        public String valoresUno { get; set; }
        public String valoresDos { get; set; }
        public String valoresTres { get; set; }
        public String valoresCuatro { get; set; }


        public Int32 tipoConsulta { get; set; }

        public String marcaGsma { get; set; }
        public String modeloGsma { get; set; }
        public String fechaRegistro { get; set; }
    }
}