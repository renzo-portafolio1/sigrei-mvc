﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_VARIABLEPARAMETRO
    {
        public BE_VARIABLEPARAMETRO()
        {          
        }
        public String variable { get; set; }
        public string valorVariable { get; set; }

        public string hostname { get; set; }
        public string puerto { get; set; }
        public string mensaje { get; set; }
        public string para { get; set; }
        public string deParte { get; set; }
        public string claveDeparte { get; set; }
        public string conCopia { get; set; }
        public string conCopiaOculta { get; set; }

        public string asunto { get; set; }

        public Int64 idRequerimientoSol { get; set; }



    }
}