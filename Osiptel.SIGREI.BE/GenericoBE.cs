﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class GenericoBE
    {
        public List<EstadoBE> ESTADOS = new List<EstadoBE>(){
                                                                     new EstadoBE() { IDESTADO = "1",ESTADO = "Activo"},
                                                                     new EstadoBE() { IDESTADO = "0",ESTADO = "Inactivo"}
                                                                      };
        public List<EstadoBE> ESTADOSREQEP = new List<EstadoBE>(){
                                                                     new EstadoBE() { IDESTADO = "1",ESTADO = "Registrado"},
                                                                     new EstadoBE() { IDESTADO = "0",ESTADO = "Asignado"}
                                                                      };

        public List<TipoMenuBE> LISTAR_TIPO_MENU = new List<TipoMenuBE>(){
                                                                     new TipoMenuBE() { IDTIPMENU = "1",DESCRIPCION = "Principal"},
                                                                     new TipoMenuBE() { IDTIPMENU = "0",DESCRIPCION = "Secundario"}
                                                                      };

        public List<SexoBE> SEXOS = new List<SexoBE>(){
                                                                     new SexoBE() { IDSEXO = "M",SEXO = "Masculino"},
                                                                     new SexoBE() { IDSEXO = "F",SEXO = "Fememnino"}
                                                                      };

        public List<TipoDocumentoBE> TIPODOCUMENTOS = new List<TipoDocumentoBE>(){
                                                                     new TipoDocumentoBE() { IDTIPODOCUMENTO = "D",TIPODOCUMENTO = "DNI"},
                                                                     new TipoDocumentoBE() { IDTIPODOCUMENTO = "F",TIPODOCUMENTO = "FACTURA"}
                                                                      };
        public string ESTADO_CONTRARIO(string dato)
        {
            if (string.IsNullOrEmpty(dato))
                return "0";
            if (dato.CompareTo("0") != 0 && dato.CompareTo("1") != 0)
                return "0";
            GenericoBE GenericoBE = new GenericoBE();
            string xxx = GenericoBE.ESTADOS.Where(p => p.IDESTADO != dato).First().IDESTADO;
            return xxx;
        }

    }

}
