﻿

namespace Osiptel.SIGREI.BE
{
    public class BeContrasenia
    {
        public string USUARIO { get; set; }
        public string CONTRASENIA { get; set; }
        public string NUEVA_CONTRASENIA { get; set; }
        public string NUEVA_CONTRASENIA_CONF { get; set; }
    }
}
