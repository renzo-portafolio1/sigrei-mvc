﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_REQUERIMIENTO_EO : BE_AUDITORIA_DTO
    {
        public long idRequerimientoEO { get; set; }
        public string nroRequerimientoEO { get; set; }
        public int idTipoSolicitante { get; set; }  
        public long idEmpresaEO { get; set; }
        public string empresaOperadora { get; set; }

        public string fecRegistro { get; set; }
        public string fechaRecibido { get; set; }
        public string fechaPlazo { get; set; }

        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }

        public string usuarioEnvio { get; set; }
        public string fechaEnvio { get; set; }
        public string fechaAtendido { get; set; }
        public string usuarioAtendido { get; set; }

        public int idTipoMotivoRechazo { get; set; }
        public string usuarioRechazo { get; set; }
        public string fechaRechazo { get; set; }
        public string comentarioRechazo { get; set; }
        public string comentario { get; set; }

        public int idTipoMotivoVencimiento { get; set; }
        public string usuarioVencimiento { get; set; }

        public int idEstado { get; set; }
        public string descEstado { get; set; }
        public BE_AUDITORIA_DTO auditoria { get; set; }
        public int nroItem { get; set; }

        public int cantidadRegistrado { get; set; }
        public int cantidadAtendido { get; set; }
        public int cantidadPorAtender { get; set; }
        public int cantidadEnviado { get; set; }
        public int cantidadPorDevolver { get; set; }
        

        public string nroImei { get; set; }

        public List<BE_REQUERIMIENTO_IMEI_EO> listaRequerimientoImeiEO { get; set; }

        public BE_REQUERIMIENTO_EO()
       {
           this.auditoria = new BE_AUDITORIA_DTO();
       }

    }
}