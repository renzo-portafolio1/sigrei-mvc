﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_REQUERIMIENTO_SOL : BE_AUDITORIA_DTO
    {
        public long idRequerimientoSol { get; set; }
        public string nroRequerimientoSol { get; set; }
        public long idRequerimiento { get; set; }

        public string fecRegistro { get; set; }
        public int idTipoAtencion { get; set; }
        public Int64 idDocumentoAdjunto { get; set; }
        public String descDocumentoAdjunto { get; set; }  

        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }

        public string fechaAtendido { get; set; }
        public string usuarioAtendido { get; set; }
        public string fechaRecibido { get; set; }
        public string formatoCorreo { get; set; }
        public string comentario { get; set; }
        
        public BE_AUDITORIA_DTO auditoria { get; set; }
        public int nroItem { get; set; }

        public int cantidadAtendido { get; set; }

        public string nroImei { get; set; }

        public BE_REQUERIMIENTO_SOL()
       {
           this.auditoria = new BE_AUDITORIA_DTO();
       }

    }
}