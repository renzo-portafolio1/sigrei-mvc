﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_REQUERIMIENTO_IMEI : BE_AUDITORIA_DTO
    {
        public long idRequerimiento { get; set; }
        public string nroRequerimiento { get; set; }
        public long idImei { get; set; }
        public int idTipoSolicitante { get; set; }
        public long idRequerimientoImei { get; set; }
        public string nroImei { get; set; }
        public string nroSisDoc { get; set; }
        public int idTipoReporte { get; set; }
        public string descTipoReporte { get; set; }
        public long idEmpresaEO { get; set; }
        public string empresaOperadora { get; set; }
        public long idRequerimientoSol { get; set; }
        public string nroRequerimientoSol { get; set; }

        public string estadoImei { get; set; }
        public string fecReporte { get; set; }
        public string nroServicio { get; set; }
        public string nombreTitular { get; set; }
        public string apPaternoTitular { get; set; }
        public string apMaternoTitular { get; set; }
        public string nombreCompletoTitular { get; set; }
        public String idTipoDocumento { get; set; }
        public string descTipoDocumento { get; set; }

        public string nroDocumento { get; set; }
        public string fecRegistro { get; set; }
        public string fechaAtencion { get; set; }
        public string usuarioAtencion { get; set; }
        public string fechaEnvio { get; set; }
        public string usuarioEnvio { get; set; }
        public string usuarioDevolucion { get; set; }
        public string fechaDevolucion { get; set; }
        public string usuarioAsignacion { get; set; }
        public string fechaAsignacion { get; set; }     

        public int nroItem { get; set; }
        public int filaImei { get; set; }
        public Boolean isRevisado { get; set; }
        public Boolean indImeiEncontrado { get; set; }

        public int longitudImei { get; set; }
        public string usuSisDocAnterior { get; set; }
        public string fechaArchivo { get; set; }
        public string pais { get; set; }
        public int cantEventos { get; set; }
        public string estadoLuhn { get; set; }
        public int ultimoDigito { get; set; }
        public string nroSisDocAnteriores { get; set; }
        public string fecSisDocAnterior { get; set; }
        public string validacionFormato { get; set; }
        public string evento { get; set; }
        public string imeiLogico { get; set; }

        public int itemEnviar { get; set; }
        public int itemDevolver { get; set; }
        public int itemAtender { get; set; }

        public int idTipoMotivo { get; set; }
        public string descTipoMotivo { get; set; }
        public string descTipoMotivoAtencion { get; set; }
        public string observacion { get; set; }
        public Int64 idEventoSeleccionado { get; set; }
        public long idAdjunto { get; set; }

        public int indDevolver { get; set; }
        public int indProcesado { get; set; }
        public int indLogico { get; set; }
        public int indValido { get; set; }

        public String descDocumentoAdjunto { get; set; }

        public string comentario{ get; set; }
        public string descMarcaModelo { get; set; }
        public string estadoIMEI { get; set; }
        public string IMEI { get; set; }
        public string FUENTE_REPORTE { get; set; }
        public string MOTIVO_REPORTE { get; set; }
        public string FECHA_BLOQUEO { get; set; }
        public string CONCESIONARIO { get; set; }
        public string NOM_ABONADO { get; set; }
        public string TIPO_DOC_LEGAL { get; set; }
        public string NUMERO_DOC_LEGAL { get; set; }
        public string NUMERO_SERVICIO_MOVIL { get; set; }
       

    }
}