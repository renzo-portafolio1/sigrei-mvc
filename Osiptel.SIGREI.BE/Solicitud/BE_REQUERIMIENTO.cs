﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_REQUERIMIENTO : BE_AUDITORIA_DTO
    {
        public long idRequerimiento { get; set; }
        public long idMovSgd { get; set; }
        public int idTipoSolicitante { get; set; }
        public String tipoSolicitante { get; set; }
        public string descTipoSolicitante { get; set; }
        public string nroRequerimiento { get; set; }
        public string fecRegistro { get; set; }
        public string usuarioRegistro { get; set; }
        public long idInstitucion { get; set; }
        public string nombreInstitucion { get; set; }
        public string nombreSolicitante { get; set; }
        public string cargo { get; set; }
        public string idUbigeo { get; set; }
        public string departamento { get; set; }
        public string direccion { get; set; }
        public string correo { get; set; }
        public string nroOficio { get; set; }
        public string asunto { get; set; }
        public long idAdjuntoOficio { get; set; }
        public string contacto { get; set; }
        public long idAdjuntoIMEI { get; set; }

        public string nroDocReferenciado { get; set; }

        public string nroSisDoc { get; set; }
        public long idEmpresaEO { get; set; }
        public string nroImei{ get; set; }
        public string nroImeiDos { get; set; }

        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }

        public string fechaDerivacion { get; set; }
        public string usuarioDerivacion { get; set; }
        public string fechaAsignacion { get; set; }
        public string usuarioAsignacion { get; set; }
        public string fechaFinalizacion { get; set; }
        public string usuarioFinalizacion { get; set; }

        public string fechaAprobacion { get; set; }
        public string usuarioAprobacion { get; set; }
        public int idTipoMotivoAprobacion { get; set; }
        public string comentarioAprobacion { get; set; }

        public string fechaRechazo { get; set; }
        public string usuarioRechazo { get; set; }
        public int idTipoMotivoRechazo { get; set; }
        public string comentarioRechazo { get; set; }
        public string comentario { get; set; }

        public string comentarioFinalizacion { get; set; }

        public string nombreArchivoAdjuntoOficio { get; set; }
        public string nombreArchivoAdjuntoIMEI { get; set; }
        public string claveSolicitud { get; set; }

        public int nroItem { get; set; }

        public int cantidadRegistrado { get; set; }
        public int cantidadValido { get; set; }
        public int cantidadInvalido { get; set; }
        public int cantidadAtendido { get; set; }
        public int cantidadPorAtender { get; set; }
        public int cantidadEnviado { get; set; }
        public int cantidadPorDevolver { get; set; }

        public int idTipoOrigen { get; set; }
        public string descTipoOrigen { get; set; }
        public string nroRequerimientoI { get; set; }
        public string nroRequerimientoF { get; set; }

        public int longitudImei { get; set; }
        public string empresaOperadora { get; set; }
        public string fecReporte { get; set; }
        public string fechaRecibido { get; set; }
        public int cantidadRecibido { get; set; }

        public string estadoIMEI { get; set; }
        public string indLogico { get; set; }

        public string fechaArchivo { get; set; }
        public string pais { get; set; }
        public int cantEventos { get; set; }
        public string estadoLuhn { get; set; }
        public int ultimoDigito { get; set; }
        public string usuSisDocAnterior { get; set; }
        public string nroSisDocAnteriores { get; set; }
        public string fecSisDocAnterior { get; set; }
        public string validacionFormato { get; set; }
        public string evento { get; set; }

        public int opcion { get; set; }
        public int imeiAsignado { get; set; }

        public Boolean isRevisado { get; set; }

        public int ent_publica { get; set; }
        public int uni_ent_publica { get; set; }
        public string delito { get; set; }
        public string motivo { get; set; }
        public string num_denuncia { get; set; }
        public string tipo_dato { get; set; }
        public string tipo_carga { get; set; }
        public string NROSERVICIOMOVIL { get; set; }
        public string MOTIVOREPORTE { get; set; }
        public string FUENTEREPORTE { get; set; }
        public string TIPO_DOC_LEGAL { get; set; }
        public string NUMERO_DOC_LEGAL { get; set; }
        public string HOSTNAME { get; set; }
        public string PUERTO { get; set; }
        public string DE_PARTE { get; set; }
        public string CLAVE_PARTE { get; set; }
        public string ASUNTO { get; set; }
        public string MENSAJE { get; set; }
        public string IND_EXCEPCION { get; set; }
        public string DES_EXCEPCION { get; set; }
        public string CANTIDAD_MOVIL { get; set; }
        public string COPIA { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDOS { get; set; }
        public string EXPED { get; set; }
        public string DELITO { get; set; }
        public string MOTIVO { get; set; }



        public BE_AUDITORIA_DTO auditoria { get; set; }
        public List<BE_REQUERIMIENTO_IMEI> listaRequerimientoIMEI { get; set; }

        public BE_REQUERIMIENTO()
       {
           this.auditoria = new BE_AUDITORIA_DTO();
       }

        public List<BE_REQUERIMIENTO> listaIMEI { get; set; }

    }
}