﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_MOVREQUERIMIENTO : BE_AUDITORIA_DTO
    {
        public long idRequerimiento { get; set; }
        public long idMovSolicitud { get; set; }
        public int idMotivo { get; set; }
        public string comentario { get; set; }
        public string descMotivo { get; set; }
        public string fecRegistro { get; set; }        
        public int nroItem { get; set; }       
           
    }
}