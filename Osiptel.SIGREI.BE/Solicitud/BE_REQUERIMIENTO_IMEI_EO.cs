﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_REQUERIMIENTO_IMEI_EO : BE_AUDITORIA_DTO 
    {
        public long idRequerimientoEO { get; set; }
        public long idRequerimiento { get; set; }
        public string nroRequerimiento { get; set; }
        public long idImeiEO { get; set; }
        public long idImei { get; set; }
        public string nroImei { get; set; }
        public string nroSisDoc { get; set; }
        public int idTipoReporte { get; set; }
        public string descTipoReporte { get; set; }
        public long idEmpresaEO { get; set; }
        public string empresaOperadora { get; set; }

        public string estadoImei { get; set; }
        public string fecReporte { get; set; }
        public string nroServicio { get; set; }
        public string nombreTitular { get; set; }
        public string apPaternoTitular { get; set; }
        public string apMaternoTitular { get; set; }
        public string nombreCompletoTitular { get; set; }
        public String idTipoDocumento { get; set; }
        public string descTipoDocumento { get; set; }

        public string nroDocumento { get; set; }
        public string fecRegistro { get; set; }
        public string fechaAtencion { get; set; }
        public string usuarioAtencion { get; set; }
        public string fechaEnvio { get; set; }
        public string usuarioEnvio { get; set; }
        public string usuarioDevolucion { get; set; }
        public string fechaDevolucion { get; set; }
        public string comentario { get; set; }  

        public int nroItem { get; set; }

        public int idTipoObservacion { get; set; }
        public string descTipoObservacion { get; set; }
        public string observacion { get; set; }

        public int itemEnviar { get; set; }
        public int itemDevolver { get; set; }
        public int itemAtender { get; set; }

        public Boolean indImeiEncontrado { get; set; }
        public long idAdjunto { get; set; }
        public Boolean conError { get; set; }
        public string documentoAdjunto { get; set; }
    }
}