﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_REPORTE_IMEI : BE_AUDITORIA_DTO
    {
        public long idRequerimiento { get; set; }
        public string nroRequerimiento { get; set; }
        public int idTipoSolicitante { get; set; }
        public string nombreInstitucion { get; set; }
        public string nombreSolicitante { get; set; }
        public long idEmpresaEO { get; set; }
        public string empresaOperadora { get; set; }
        public string nroImei { get; set; }
        public string nroSisDoc { get; set; }
        public string idTipoReporte { get; set; }
        public string descTipoReporte { get; set; }

        public string fechaRegistro { get; set; }
        public string usuarioEnvio { get; set; }
        public string fechaEnvio { get; set; }

        public string usuarioRecibido { get; set; }
        public string fechaRecibido { get; set; }

        public string usuarioAtendido { get; set; }
        public string fechaAtendido { get; set; }

        public int idTipoMotivoRechazo { get; set; }
        public string usuarioRechazo { get; set; }
        public string fechaRechazo { get; set; }
        public string comentarioRechazo { get; set; }
        public string comentario { get; set; }

        public BE_AUDITORIA_DTO auditoria { get; set; }

        public int nroItem { get; set; }

        public int cantidadRegistrado { get; set; }
        public int cantidadAtendido { get; set; }
        public int cantidadPorAtender { get; set; }
        public int cantidadEnviado { get; set; }
        public int cantidadPorDevolver { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public string departamento { get; set; }
        public string estadoImei { get; set; }
        public string fecPlazo { get; set; }

        public string usuRespuesta { get; set; }
        public string fecRespuesta { get; set; }
        public Int32 idAnio { get; set; }
        public Int32 idUbigeo { get; set; }

        public string solicitante { get; set; }

        public decimal enero { get; set; }
        public decimal febrero { get; set; }
        public decimal marzo { get; set; }
        public decimal abril { get; set; }
        public decimal mayo { get; set; }
        public decimal junio { get; set; }
        public decimal julio { get; set; }
        public decimal agosto { get; set; }
        public decimal setiembre { get; set; }
        public decimal octubre { get; set; }
        public decimal noviembre { get; set; }
        public decimal diciembre { get; set; }
        public decimal totalFila { get; set; }
        public decimal participacion { get; set; }

        public decimal totalEnero { get; set; }
        public decimal totalFebrero { get; set; }
        public decimal totalMarzo { get; set; }
        public decimal totalAbril { get; set; }
        public decimal totalMayo { get; set; }
        public decimal totalJunio { get; set; }
        public decimal totalJulio { get; set; }
        public decimal totalAgosto { get; set; }
        public decimal totalSetiembre { get; set; }
        public decimal totalOctubre { get; set; }
        public decimal totalNoviembre { get; set; }
        public decimal totalDiciembre { get; set; }
        public decimal totalParticipacion { get; set; }

        public String cumplioPlazo { get; set; }
        public String descripcion { get; set; }



        public BE_REPORTE_IMEI()
       {
           this.auditoria = new BE_AUDITORIA_DTO();
       }

    }
}