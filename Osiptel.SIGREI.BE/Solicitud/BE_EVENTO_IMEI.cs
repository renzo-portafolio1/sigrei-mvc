﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class BE_EVENTO_IMEI : BE_AUDITORIA_DTO
    {
        public long idTermMovi { get; set; }
        public long idEvento { get; set; }
        public int nroItem { get; set; }
        public string nroImei { get; set; }
        public string pais { get; set; }
        public string empresaOperadora { get; set; }
        public string nomArchivo { get; set; }
        public string fechaHora { get; set; }
        public string estadoReporte { get; set; }
        public long idTermMoviSeleccionado { get; set; }

    }
}