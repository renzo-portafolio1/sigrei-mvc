﻿

namespace Osiptel.SIGREI.BE
{
    public class BeToken
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public int state { get; set; }
        public string expire_datetime { get; set; }
        public string user_iderror { get; set; }
        public string user_error { get; set; }
    }
}
