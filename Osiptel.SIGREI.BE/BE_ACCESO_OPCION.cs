﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_ACCESO_OPCION
    {
        #region "Atributos"
        private string _IDOPCION;
        private string _APLICACION;
        private string _RUTA;
        private string _DESCRIPCION;
        private string _OBJECTO;

        private string _IDPADRE;
        private string _ORDEN;
        private string _FLGRUTA;
        private string _ESTADO;
        private string _IDPERFIL;
        #endregion

        #region "Propiedades"

        public string IDOPCION
        {
            get { return _IDOPCION; }
            set { _IDOPCION = value; }
        }

        public string APLICACION
        {
            get { return _APLICACION; }
            set { _APLICACION = value; }
        }
        public string RUTA
        {
            get { return _RUTA; }
            set { _RUTA = value; }
        }

        public string DESCRIPCION
        {
            get { return _DESCRIPCION; }
            set { _DESCRIPCION = value; }
        }


        public string OBJETO
        {
            get { return _OBJECTO; }
            set { _OBJECTO= value; }
        }
        public string IDPADRE
        {
            get { return _IDPADRE; }
            set { _IDPADRE = value; }
        }

        public string ORDEN
        {
            get { return _ORDEN; }
            set { _ORDEN = value; }
        }

        public string FLGRUTA
        {
            get { return _FLGRUTA; }
            set { _FLGRUTA = value; }
        }

        public string ESTADO
        {
            get { return _ESTADO; }
            set { _ESTADO = value; }
        }

        public string IDPERFIL
        {
            get { return _IDPERFIL; }
            set { _IDPERFIL = value; }
        }

        #endregion
    }
}
