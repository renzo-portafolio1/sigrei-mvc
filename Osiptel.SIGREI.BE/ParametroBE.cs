﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Osiptel.SIGREI.BE
{
    public class EstadoBE
    {
        public string IDESTADO { get; set; }
        public string ESTADO { get; set; }
    }
    public class SexoBE
    {
        public string IDSEXO { get; set; }
        public string SEXO { get; set; }
    }
    public class TipoDocumentoBE
    {
        public string IDTIPODOCUMENTO { get; set; }
        public string TIPODOCUMENTO { get; set; }
    }
    public class AnioBE
    {
        public string ANIO { get; set; }
        public string _ANIO_ { get; set; }
    }
    public class TipoMenuBE
    {
        public string IDTIPMENU { get; set; }
        public string DESCRIPCION { get; set; }
    }



}
