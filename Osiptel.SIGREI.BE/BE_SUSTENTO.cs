﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_SUSTENTO
    {
        public string nroitem { get; set; }
        public string dato { get; set; }
        public string revi { get; set; }
        public string proce { get; set; }
        public string indi { get; set; }
    }
}
