﻿

namespace Osiptel.SIGREI.BE
{
    public class BeInsExterna
    {
        public string NUM_RUC { get; set; }
        public string DES_RAZ_SOC { get; set; }
        public string TIP_INSTEXT { get; set; }
        public string USUCRE { get; set; }
        public string USUMOD { get; set; }
        public string ACTIVO { get; set; }
        public string DesActivo { get; set; }
        public string TipInstitucion { get; set; }

        public string NUM_RUC_ACT { get; set; }

        public int orden { get; set; }

        public int IDENTPUBLICA { get; set; }

        public string DesEntPublica { get; set; }

    }
}
