﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Osiptel.SIGREI.BE.Reporte
{

    public class BE_GRAFICA_DATABASE
    {
        public int idubigeo { get; set; }
        public string departament { get; set; }
        public string servicio { get; set; }
        public int cantidad { get; set; }
    }

    public class BE_GRAFICA
    {
        public List<string> labels { get; set; }
        public List<BE_GRAFICA_FORMATO> datasets { get; set; }
    }

    public class BE_GRAFICA_PIE
    {
        public List<string> labels { get; set; }
        public List<BE_PIE_GRAFICA_FORMATO> datasets { get; set; }
    }

    public class BE_GRAFICA_FORMATO
    {
        public string label { get; set; }
        public List<int> data { get; set; }
        public string backgroundColor { get; set; }
        public int barThickness { get; set; }
    }

    public class BE_PIE_GRAFICA_FORMATO
    {
        public string label { get; set; }
        public List<int> data { get; set; }
        public List<string> backgroundColor { get; set; }
    }

}
