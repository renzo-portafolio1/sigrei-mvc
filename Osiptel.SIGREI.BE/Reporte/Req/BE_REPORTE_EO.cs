﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Osiptel.SIGREI.BE.Reporte.Req
{
    public class BE_REPORTE_EO
    {
        public string anio { get; set; }
        public string mes { get; set; }
        public string eo { get; set; }
        public string requerimiento { get; set; }
        public string desde { get; set; }
        public string hasta { get; set; }
    }
}
