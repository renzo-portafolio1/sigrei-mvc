﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Osiptel.SIGREI.BE.Reporte.Req
{
    public class BE_GERENCIAL_EO
    {
        public string anio { get; set; }
        public string mes { get; set; }
        //public List<String> mes { get; set; }
        public string empresa { get; set; }
        public string institucion { get; set; }
        public string requerimiento { get; set; }
    }
}
