﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Osiptel.SIGREI.BE.Reporte
{
    public class BE_TABLA
    {
        public List<BE_TABLA_REGION> rowsRegion { get; set; } 
    }

    public class BE_TABLA_FORMATO_FECHA
    {
        public string anio { get; set; }
        public List<BE_TABLA_MES_ANIO> meses { get; set; }
    }

    public class BE_TABLA_MES_ANIO
    {
        public int idMes { get; set; }
        public string mes { get; set; }
        public List<string> dias { get; set; }
    }

    public class BE_TABLA_REGION
    {
        public int idUbigeo { get; set; }
        public string departamento { get; set; }
        public string entidad { get; set; }
        public string externa { get; set; }
        public string unidad { get; set; }
        public string ruc { get; set; }
        public string servicio { get; set; }
        public string anio { get; set; }
        public int idMes { get; set; }
        public List<int> meses { get; set; }
        public string mes { get; set; }
        public List<int> dias { get; set; }
        public string dia { get; set; }
        public int cantidad { get; set; }
        public int total { get; set; }
    }

    public class BE_TABLA_INSTITUCION
    {
        public string estado { get; set; }
        public string entidad { get; set; }
        public string externa { get; set; }
        public string unidad { get; set; }
        public string ruc { get; set; }
        public string anio { get; set; }
        public int idMes{ get; set; }
        public string mes { get; set; }
        public string dia { get; set; }
        public string servicio { get; set; }
        public string cantidad { get; set; }
    }

    public class BE_TABLA_EO
    {
        public string empresa { get; set; }
        public string tiposervicio { get; set; }
        public string estado { get; set; }
        public string atencion { get; set; }
        public string anio { get; set; }
        public int idMes { get; set; }
        public string mes { get; set; }
        public string dia { get; set; }
        public string cantidad { get; set; }
    }
}
