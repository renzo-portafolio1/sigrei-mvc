﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_PAGINACION
    {
        [DataMember()]

        public int Tamanio_Pagina { get; set; }

        public int Numero_Pagina { get; set; }

        public string Sort_Column { get; set; }

        public string Sort_Order { get; set; }

        public decimal Total_Archivos { get; set; }

        public decimal Total_Paginas { get; set; }

        public decimal Total_Archivos_Mostrados { get; set; }

    }


}
