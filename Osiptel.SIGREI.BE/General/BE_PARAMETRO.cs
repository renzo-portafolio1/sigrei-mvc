﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_PARAMETRO : BE_AUDITORIA_DTO
    {
        public int idParametro { get; set; }
        public int idTipoParametro { get; set; }
        public int idTipoValorParametro { get; set; }
        public string tipoParametro { get; set; }
        public string tipoValorParametro { get; set; }
        public string valorParametro { get; set; }
        public string descripcionEstado { get; set; }
        public string aplicacion { get; set; }
        public string tabla { get; set; }
        public string columna { get; set; }
        public string valor { get; set; }
        public string abreviado { get; set; }
        public string descripcion { get; set; }
        public string IDPP { get; set; }
        public string PROVINCIA { get; set; }
        public string IDDIS { get; set; }
        public string DISTRITO { get; set; }
        public int orden { get; set; }

        public int DEPENDENCIA { get; set; }

        public string ESTADO { get; set; }

    }
}
