﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_AUDITORIA_DTO
    {
        public string activo { get; set; }
        public int idEstado { get; set; }
        public string descEstado { get; set; }
        public string usuCre { get; set; }
        public DateTime fecCre { get; set; }
        public string usuMod { get; set; }
        public DateTime fecMod { get; set; }

    }
}
