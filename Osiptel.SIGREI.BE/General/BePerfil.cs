﻿

namespace Osiptel.SIGREI.BE
{
    public class BePerfil : BE_AUDITORIA_DTO
    {
        public int idPerfil { get; set; }
        public int idModulo { get; set; }
        public int cantidad { get; set; }
        public string descripcion { get; set; }

        public string aplicacion { get; set; }
        public int nroItem { get; set; }
        
    }
}
