﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_UBIGEO : BE_AUDITORIA_DTO
    {
      
        public string idUbigeo { get; set; }
        public string descUbigeo { get; set; }
        public string idDepartamento { get; set; }
        public string idProvincia { get; set; }
        public string idDistrito { get; set; }

    }
}
