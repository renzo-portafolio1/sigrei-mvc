﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_PARAMETROGENERAL : BE_AUDITORIA_DTO
    {

        public string aplicacion { get; set; }
        public string tabla { get; set; }
        public string columna { get; set; }
        public string valor { get; set; }
        public string idTipo { get; set; }
        public string abreviado { get; set; }
        public string descripcion { get; set; }
        public int orden { get; set; }

     }
}
