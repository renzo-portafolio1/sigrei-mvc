﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_USUARIOGPSU : BE_AUDITORIA_DTO
    {
      
        public string codigo { get; set; }
        public string nombreCompleto { get; set; }
        public string unidad { get; set; }
        public int opcion { get; set; }
    }
}
