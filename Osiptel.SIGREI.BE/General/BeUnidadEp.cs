﻿
namespace Osiptel.SIGREI.BE
{
    public class BeUnidadEp
    {
        public int nroItem { get; set; }
        public int idUnidadEP { get; set; }
        public int idEntidadP { get; set; }
        public string nomUniEntPublica { get; set; }
        public string nomEntPublica { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }
        public string extCorreo { get; set; }
        public int activo { get; set; }
        public int estado { get; set; }
        public string desActivo { get; set; }
        public string usuCre { get; set; }

        public string idInsExterna { get; set; }
        public string nomInsExterna { get; set; }
        public string iddepartamento { get; set; }
        public string idprovincia { get; set; }
        public string iddistrito { get; set; }
        public string nomdepartamento { get; set; }
    }
}
