﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_EMPRESA : BE_AUDITORIA_DTO
    {
        public long idEmpresa { get; set; }
        public string codigo { get; set; }
        public string empresaOperadora { get; set; }   
        public string ruc { get; set; }
        public string razonSocial { get; set; }
        public string nombreComercial { get; set; }      
        public int orden { get; set; }

    }
}
