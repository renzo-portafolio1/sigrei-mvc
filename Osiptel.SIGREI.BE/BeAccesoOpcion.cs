﻿

namespace Osiptel.SIGREI.BE
{
    public class BeAccesoOpcion
    {
        #region "Atributos"
        public string IDOPCION { get; set; }
        public string APLICACION { get; set; }
        public string RUTA { get; set; }
        public string DESCRIPCION { get; set; }
        public string OBJETO { get; set; }

        public string IDPADRE { get; set; }
        public string ORDEN { get; set; }
        public string FLGRUTA { get; set; }
        public string ESTADO { get; set; }
        public string IDPERFIL { get; set; }
        public int NROITEM { get; set; }

        public string USUCRE { get; set; }
        #endregion


    }
}
