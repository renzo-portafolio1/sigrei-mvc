﻿
using System;
using System.Linq;

namespace Osiptel.SIGREI.BE
{
    public class UsuarioBE
    {

        private string a_strIdUsuario;
        private System.String _idempresa = "";
        private System.String _PERFIL = "";
        private string a_strClave;
        private string a_strEstado;
        private string a_str_Nombres;
        private string a_str_ApePat;
        private string a_str_ApeMat;
        private string a_str_Correo;
        private string a_str_TelFijo;
        private string a_str_TelMov;
        private string a_str_Sexo;
        private string a_str_TipoDoc;
        private string a_str_numdocumento;
        private string a_str_Area;
        private string a_str_Cargo;
        private string a_str_InfAdic;
        private string a_str_EmailPers;
        private int a_int_Cod_Empresa;
        private string a_str_Nom_Empresa;
        private System.String _Departamento = "";
        private System.String _Descripcion = "";
        private int a_int_IdPerfil;
        private string a_str_fecvigencia;
        private string _USUCRE;
        private string _FECCRE;
        private string _USUMOD;
        private string _FECMOD;

        private string a_str_usuacrea;
        private string a_str_usuamodi;

        public int idTipoSolicitante { get; set; }
        public string descTipoSolicitante { get; set; }

        private string _des_perfil;

        public string des_perfil
        {
            get => _des_perfil;
            set => _des_perfil = value;
        }

        public string IdUsuario
        {
            get => a_strIdUsuario;
            set => a_strIdUsuario = value;
        }
        public string Clave
        {
            get => a_strClave;
            set => a_strClave = value;
        }
        public string Estado
        {
            get => a_strEstado;
            set => a_strEstado = value;
        }
        public string Nombres
        {
            get => a_str_Nombres;
            set => a_str_Nombres = value;
        }
        public string ApePat
        {
            get => a_str_ApePat;
            set => a_str_ApePat = value;
        }
        public string ApeMat
        {
            get => a_str_ApeMat;
            set => a_str_ApeMat = value;
        }
        public string Correo
        {
            get => a_str_Correo;
            set => a_str_Correo = value;
        }
        public System.String idempresa
        {
            get => _idempresa;
            set => _idempresa = value;
        }
        public System.String PERFIL
        {
            get => _PERFIL;
            set => _PERFIL = value;
        }
        public string TelFijo
        {
            get => a_str_TelFijo;
            set => a_str_TelFijo = value;
        }
        public string TelMovil
        {
            get => a_str_TelMov;
            set => a_str_TelMov = value;
        }
        public string Sexo
        {
            get => a_str_Sexo;
            set => a_str_Sexo = value;
        }
        public string TipoDoc
        {
            get => a_str_TipoDoc;
            set => a_str_TipoDoc = value;
        }
        public string Documento
        {
            get => a_str_numdocumento;
            set => a_str_numdocumento = value;
        }
        public string Area
        {
            get => a_str_Area;
            set => a_str_Area = value;
        }
        public string Cargo
        {
            get => a_str_Cargo;
            set => a_str_Cargo = value;
        }
        public string InfAdic
        {
            get => a_str_InfAdic;
            set => a_str_InfAdic = value;
        }
        public string EmailPers
        {
            get => a_str_EmailPers;
            set => a_str_EmailPers = value;
        }
        public int Cod_Empresa
        {
            get => a_int_Cod_Empresa;
            set => a_int_Cod_Empresa = value;
        }
        public string Nom_Empresa
        {
            get => a_str_Nom_Empresa;
            set => a_str_Nom_Empresa = value;
        }
        public int IdPerfil
        {
            get => a_int_IdPerfil;
            set => a_int_IdPerfil = value;
        }

        public System.String Departamento
        {
            get => _Departamento;
            set => _Departamento = value;
        }
        public System.String Descripcion
        {
            get => _Descripcion;
            set => _Descripcion = value;
        }

        public string FecVigencia
        {
            get => a_str_fecvigencia;
            set => a_str_fecvigencia = value;
        }

        public string UsuarioCreacion
        {
            get => a_str_usuacrea;
            set => a_str_usuacrea = value;
        }
        public string UsuarioModificacion
        {
            get => a_str_usuamodi;
            set => a_str_usuamodi = value;
        }

        public string USUCRE
        {
            get => _USUCRE;
            set => _USUCRE = value;
        }
        public string FECCRE
        {
            get => _FECCRE;
            set => _FECCRE = value;
        }
        public string USUMOD
        {
            get => _USUMOD;
            set => _USUMOD = value;
        }
        public string FECMOD
        {
            get => _FECMOD;
            set => _FECMOD = value;
        }

    }
    public class UsuarioEOBE : FiltroBusquedaBE
    {

        public Int32 NROITEM { get; set; }
        public string IDEMPRESA { get; set; }
        public string USUARIO { get; set; }
        public string IDUSUARIO { get; set; }
        public string CLAVE { get; set; }
        public string CONFCLAVE { get; set; }
        public string NOMBRE { get; set; }
        public string APEMAT { get; set; }
        public string APEPAT { get; set; }
        public string IDESTADO { get; set; }
        public string AREA { get; set; }
        public string CARGO { get; set; }
        public string EMAILEO { get; set; }
        public string EMAILPER { get; set; }
        public string FECHAVIG { get; set; }
        public string SEXO { get; set; }
        public string TIPODOC { get; set; }
        public string NRODOC { get; set; }
        public string TELEFIJO { get; set; }
        public string TELEMOVI { get; set; }
        public string INFOADIC { get; set; }
        public string IDPERFIL { get; set; }

        public string TELFIJ { get; set; }
        public string TELMOV { get; set; }
        public string EMAILPERS { get; set; }
        //public string ACTIVO { get; set; }
        public string FECVIG { get; set; }
        public string EMPRESA { get; set; }
        public string PERFIL { get; set; }
        public string USUCRE { get; set; }
        public string FECCRE { get; set; }
        public string USUMOD { get; set; }
        public string FECMOD { get; set; }

        public string ESTADO
        {
            get
            {
                if (string.IsNullOrEmpty(this.IDESTADO))
                {
                    return string.Empty;
                }

                if (this.IDESTADO.CompareTo("0") != 0 && this.IDESTADO.CompareTo("1") != 0)
                {
                    return this.IDESTADO;
                }

                GenericoBE GenericoBE = new GenericoBE();
                string xxx = GenericoBE.ESTADOS.Where(p => p.IDESTADO == this.IDESTADO).First().ESTADO;
                return xxx;
            }
            set { }
        }

    }

    public class UsuarioOEIBE : FiltroBusquedaBE
    {
        public string APLICACION { get; set; }
        public string IDUSUARIO { get; set; }
        public string IDOEI { get; set; }
        public string NOMBRE { get; set; }
        public string APEPAT { get; set; }
        public string APEMAT { get; set; }
        public string CLAVE { get; set; }
        public string USUARIO { get; set; }
        public string EMAILOEI { get; set; }
        public string TELFIJ { get; set; }
        public string TELMOV { get; set; }
        public string SEXO { get; set; }
        public string TIPODOC { get; set; }
        public string NRODOC { get; set; }
        public string EMAILPERS { get; set; }
        public string AREA { get; set; }
        public string CARGO { get; set; }
        public string INFOADIC { get; set; }
        public string IDESTADO { get; set; }
        public string FECVIG { get; set; }
        public string IDPERFIL { get; set; }
        public string PERFIL { get; set; }
        public string USUCRE { get; set; }
        public string FECCRE { get; set; }
        public string USUMOD { get; set; }
        public string FECMOD { get; set; }
        public string EMPRESA { get; set; }
        public string ESTADO
        {
            get
            {
                if (string.IsNullOrEmpty(this.IDESTADO))
                {
                    return string.Empty;
                }

                if (this.IDESTADO.CompareTo("0") != 0 && this.IDESTADO.CompareTo("1") != 0)
                {
                    return this.IDESTADO;
                }

                GenericoBE GenericoBE = new GenericoBE();
                string xxx = GenericoBE.ESTADOS.Where(p => p.IDESTADO == this.IDESTADO).First().ESTADO;
                return xxx;
            }
            set { }
        }
    }

    public class OeiBE : FiltroBusquedaBE
    {
        public string IDEMPRESA { get; set; }
        public string RAZSOC { get; set; }
        public string NOMCOM { get; set; }
        public string IDESTADO { get; set; }

    }
    public class PerfilBE : FiltroBusquedaBE
    {
        public string IDPERFIL { get; set; }
        public string DESCRIPCION { get; set; }
    }
}
