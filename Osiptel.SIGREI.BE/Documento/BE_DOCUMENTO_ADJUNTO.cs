﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SIGREI.BE
{
    public class BE_DOCUMENTO_ADJUNTO : BE_AUDITORIA_DTO
    {

        public long idDocumentoAdjunto { get; set; }       
        public int nroItem { get; set; }
        public string nombreDocumento { get; set; }
        public string descDocumento { get; set; }
        public int anioDocumento { get; set; }
        public string fecDocumento { get; set; }
    
    }
}
