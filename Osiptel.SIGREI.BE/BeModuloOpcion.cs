﻿

namespace Osiptel.SIGREI.BE
{
    public class BeModuloOpcion
    {
        public int IDMODULO { get; set; }
        public int IDOPCION { get; set; }
        public int IDPADRE { get; set; }
        public string DESCRIPCION { get; set; }
        public int ACTIVO { get; set; }
        public string USUCRE { get; set; }

    }
}
