﻿

namespace Osiptel.SIGREI.BE
{
    public class BeUsuarioEP
    {
        public string IDUSUARIO { get; set; }
        public string CONTRASENIA { get; set; }
        public string CONTRASENIA_ENCR { get; set; }
        public string NUM_RUC { get; set; }
        public string COD_DOCIDE { get; set; }
        public string NUM_DOCIDE { get; set; }
        public string nomEntPublica { get; set; }
        public string DES_NOMBRE { get; set; }
        public string DES_NOmBRE_LAST { get; set; }
        public string DES_APEPAT { get; set; }
        public string DES_APEMAT { get; set; }
        public string IND_SEXO { get; set; }
        public string DES_TLFFIJ { get; set; }
        public string DES_TLFMVL { get; set; }
        public string DES_PUESTO { get; set; }
        public string DES_CORELE { get; set; }
        public string DES_UUOO { get; set; }
        public string IND_ESTADO { get; set; }
        public string USUBAJA { get; set; }
        public string USUCRE { get; set; }
        public string USUMOD { get; set; }
        public int CANTINTENTOS { get; set; }
        public int INDCAMBIOCNIA { get; set; }
        public int IDUNIDADEP { get; set; }
        public string DIRECCION { get; set; }
        public int IDPERFIL { get; set; }
        public int IDMODULO { get; set; }
        public int nroItem { get; set; }
        public string INS_EXT { get; set; }
        public string EXTCORREO { get; set; }
        public string CORREO_COMPL { get; set; }
        public string DES_TDOC { get; set; }
        public string DES_MODU { get; set; }
        public string DES_PERFIL { get; set; }
        public string DES_ENTPUB { get; set; }
        public string NOMBREUNIDADP { get; set; }
        public string desActivo { get; set; }
        public int IDENTPUBLICA { get; set; }

        public string FECINI { get; set; }
        public string FECEXPCONTRASENIA { get; set; }
        public string FECCRE { get; set; }
        public string FECMOD { get; set; }


        public string HOSTNAME { get; set; }
        public string PUERTO { get; set; }
        public string DE_PARTE { get; set; }
        public string CLAVE_PARTE { get; set; }
        public string PARA { get; set; }
        public string ASUNTO { get; set; }
        public string MENSAJE { get; set; }
        public string COPIA { get; set; }
        public string Lista_Tip_Dato { get; set; }
        public string Lista_Tip_Carga { get; set; }
        public string MOTIVO { get; set; }
        public string NRODOCREF { get; set; }
        public string DELITO { get; set; }

        public string IDUBIGEO { get; set; }
        public string UBIGEO { get; set; } 
        public string CARGO { get; set; } 
        public string APLICACION { get; set; } 

        public string DES_APE_ALL {
            get {
                return DES_APEPAT + " " + DES_APEMAT;
            }
        }

    }
}
