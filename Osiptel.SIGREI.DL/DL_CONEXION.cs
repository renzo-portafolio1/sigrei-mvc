﻿using Microsoft.Extensions.Configuration;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Osiptel.SIGREI.DL
{

    public interface IConexion
    {
        OracleConnection retConexion();
        string retStrConexion();
        void retOpen();
        void retClose();
    }

    public class DL_CONEXION : IConexion, IDisposable
    {
        #region "variables"
        public OracleConnection conexion;
        private Component component = new Component();
        private bool disposed = false;
        #endregion

        #region "Metodos"
        public string retRegEdit()
        {
            string strUser = string.Empty;
            string strPassword = string.Empty;
            string strSource = string.Empty;
            string strConexion = string.Empty;
            try
            {
                strUser = "";
                strPassword = "";
                strSource = "";
                strConexion = "";
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {

            }
            return strConexion;
        }
        public static IConfiguration Configuration { get; set; }
        private string GetConnectionString()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            var opcion = Configuration["opcionDesarrollo"];
            var RutaLog = Configuration["RutaLog"];
            if (opcion.Equals("1"))
            {
                return Configuration.GetConnectionString("ConexionSIGREIDesa");
            }
            else
            {
                var urlDesincriptado = Configuration["urlWSDesincriptado"];
                String clave = Configuration.GetConnectionString("ConexionSIGREI");
                return descriptarConexion(urlDesincriptado, clave, RutaLog);
            }
        }

        private string descriptarConexion(String url, String clave, String RutaLog)
        {

            var proxyTres = new WebProxy("srvproxy01.osiptel.gob.pe", 3128)
            {
                UseDefaultCredentials = false,
                Credentials = CredentialCache.DefaultCredentials
            };
            var handler = new HttpClientHandler
            {
                Proxy = proxyTres,
                PreAuthenticate = true,
                UseDefaultCredentials = true
            };
            String strConexionDesc = "";
            try
            {
                using (var cliente = new HttpClient())
                {
                    var urlServicioToken = url + "" + clave;
                    cliente.DefaultRequestHeaders.Accept.Clear();
                    HttpResponseMessage response = cliente.PostAsync(urlServicioToken, new StringContent(string.Format(""), Encoding.UTF8, "application/json")).Result;
                    String respuestaDos = response.StatusCode.ToString();
                    if (response.IsSuccessStatusCode)
                    {
                        strConexionDesc = response.Content.ReadAsStringAsync().Result;
                    }
                    else
                    {
                        strConexionDesc = "";
                    }
                }
            }
            catch (Exception ex)
            {

                using (System.IO.StreamWriter file = new System.IO.StreamWriter(RutaLog, true))
                {

                    file.WriteLine("-----           -INICIO DE ERROR          ----");
                    file.WriteLine("Elaborado                : Programador");
                    file.WriteLine("Fecha                    : " + DateTime.Now.ToLongDateString()); // Escribe la Traza
                    file.WriteLine("Hora de incidencia       : " + DateTime.Now.ToString("hh:mm:ss").ToString()); // Escribe la Traza
                    file.WriteLine("Mensaje de Error         :   " + ex.Message);// escribe el mensaje
                    file.WriteLine(ex.HelpLink); // Escribe la Traza
                    file.WriteLine(ex.Source); // Escribe la Traza
                    file.WriteLine(ex.StackTrace); // Escribe la Traza
                    file.WriteLine(ex.TargetSite.ToString()); // Escribe la Traza
                    file.WriteLine("Parametros=> url=>{0} clave{1},rutalog{2}", url, clave, RutaLog);
                    file.WriteLine("-----           - FIN DE ERROR           ----");
                }

            }

            return strConexionDesc;
        }

        public string retStrConexion()
        {
            string strConexion = string.Empty;
            try
            {
                strConexion = GetConnectionString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strConexion;
        }

        public OracleConnection retConexion()
        {
            return conexion;
        }

        public void retOpen()
        {
            if (conexion == null)
            {
                conexion = new OracleConnection(retStrConexion());

                if (conexion.State != ConnectionState.Open)
                {
                    try
                    {
                        conexion.Open();
                    }
                    catch (Exception ex)
                    {
                        var RutaLog = Configuration["RutaLog"];
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(RutaLog, true))
                        {

                            file.WriteLine("-----           -INICIO DE ERROR          ----");
                            file.WriteLine("Elaborado                : Programador");
                            file.WriteLine("Fecha                    : " + DateTime.Now.ToLongDateString()); // Escribe la Traza
                            file.WriteLine("Hora de incidencia       : " + DateTime.Now.ToString("hh:mm:ss").ToString()); // Escribe la Traza
                            file.WriteLine("Mensaje de Error         :   " + ex.Message);// escribe el mensaje
                            file.WriteLine(ex.HelpLink); // Escribe la Traza
                            file.WriteLine(ex.Source); // Escribe la Traza
                            file.WriteLine(ex.StackTrace); // Escribe la Traza
                            file.WriteLine(ex.TargetSite.ToString()); // Escribe la Traza
                            file.WriteLine("-----           - FIN DE ERROR           ----");
                        }
                    }
                }
            }
        }

        public void retClose()
        {
            conexion.Close();
            conexion.Dispose();
        }

        #endregion

        #region Devolver conjunto de registros en un DataTable

        public DataTable getTable(OracleCommand cmd)
        {
            DataTable dtt = new DataTable();
            try
            {
                retOpen();
                cmd.Connection = conexion;

                using (OracleDataReader reader = cmd.ExecuteReader())
                {
                    dtt.Load(reader, LoadOption.OverwriteChanges);
                    reader.Close();
                }
                retClose();
            }
            catch (OracleException ex)
            {
                throw new Exception(ex.Message);
            }
            return dtt;
        }

        #endregion

        #region Ejecutar Command
        public int ejecutaSQL(OracleCommand cmd)
        {
            int res = 0;
            try
            {
                retOpen();
                cmd.Connection = conexion;
                res = cmd.ExecuteNonQuery();
                retClose();
            }
            catch (OracleException ex)
            {
                throw new Exception(ex.Message);
            }
            return res;
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion
    }

}
