﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_DOCUMENTO_ADJUNTO
    {

        #region "Transaccionales"

        public long Insertar(BE_DOCUMENTO_ADJUNTO objeto)
        {
            long idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_INSERTAR_DOCUMENTO_ADJUNTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NombreDocumento", OracleType.VarChar, ParameterDirection.Input, objeto.nombreDocumento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_DescDocumento", OracleType.VarChar, ParameterDirection.Input, objeto.descDocumento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objeto.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDDocumentoAdjunto", OracleType.Number));
                  
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDDocumentoAdjunto"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Registro de Documento Adjuntos", ex.ToString(), objeto.usuMod);
                throw ex;
            }
        }
       
        #endregion

    }
}

