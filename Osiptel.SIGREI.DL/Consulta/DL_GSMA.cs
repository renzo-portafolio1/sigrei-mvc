﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_GSMA
    {

        public List<BE_GSMA> Listar(ref BE_PAGINACION objPaginacion, BE_GSMA pFiltros)
        {
            BE_GSMA objConsultaBE;
            List<BE_GSMA> llstConsultaBE = new List<BE_GSMA>();
            try
            {
                String cadena = new DL_CONEXION().retStrConexion();
                using (OracleConnection conexOsipweb = new OracleConnection(cadena))
                {

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conexOsipweb;

                    cmd.CommandText = "PKG_CONSULTA.SP_CONSULTA_GSMA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Imei", OracleType.VarChar, ParameterDirection.Input, pFiltros.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tac", OracleType.VarChar, ParameterDirection.Input, pFiltros.nroTAc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Marca", OracleType.VarChar, ParameterDirection.Input, pFiltros.marca));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ModeloComercial", OracleType.VarChar, ParameterDirection.Input, pFiltros.modeloComercial));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ModeloTecnico", OracleType.VarChar, ParameterDirection.Input, pFiltros.modeloTecnico));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pagina", OracleType.Number, ParameterDirection.Input, objPaginacion.Numero_Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_RegxPag", OracleType.Number, ParameterDirection.Input, objPaginacion.Tamanio_Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    try
                    {
                        conexOsipweb.Open();

                        using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                        {
                            while (objDRConsulta.Read())
                            {
                                objConsultaBE = new BE_GSMA();
                                objPaginacion.Total_Archivos = DL_HELPER.getInt64(objDRConsulta, "TOTAL");
                                objConsultaBE.nroItem = DL_HELPER.getInt32(objDRConsulta, "NRO_ITEM");
                                objConsultaBE.nroTAc = DL_HELPER.getString(objDRConsulta, "TAC");
                                objConsultaBE.marca = DL_HELPER.getString(objDRConsulta, "MARCA");
                                objConsultaBE.modeloComercial = DL_HELPER.getString(objDRConsulta, "MODELO_COMERCIAL");
                                objConsultaBE.modeloTecnico = DL_HELPER.getString(objDRConsulta, "MODELO_TECNICO");

                                llstConsultaBE.Add(objConsultaBE);
                            }
                            objDRConsulta.Close();
                        }
                        conexOsipweb.Close();

                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        return new List<BE_GSMA>();
                    }
                    finally
                    {
                        if (conexOsipweb.State == ConnectionState.Open) conexOsipweb.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return llstConsultaBE;
        }

        public DataTable ListarConsultaGSMA_DT(BE_GSMA pFiltros)
        {
            DataTable dtResultado = new DataTable();
            try
            {
                String cadena = new DL_CONEXION().retStrConexion();
                using (OracleConnection conexOsipweb = new OracleConnection(cadena))
                {

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conexOsipweb;
                    cmd.CommandText = "PKG_CONSULTA.SP_CONSULTA_GSMA_DT";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Imei", OracleType.VarChar, ParameterDirection.Input, pFiltros.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tac", OracleType.VarChar, ParameterDirection.Input, pFiltros.nroTAc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Marca", OracleType.VarChar, ParameterDirection.Input, pFiltros.marca));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ModeloComercial", OracleType.VarChar, ParameterDirection.Input, pFiltros.modeloComercial));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ModeloTecnico", OracleType.VarChar, ParameterDirection.Input, pFiltros.modeloTecnico));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    try
                    {
                        conexOsipweb.Open();
                        using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                        {
                            dtResultado.Load(objDRConsulta);
                        }
                        conexOsipweb.Close();
                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        return dtResultado;
                    }
                    finally
                    {
                        if (conexOsipweb.State == ConnectionState.Open) conexOsipweb.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtResultado;
        }
    }
}
