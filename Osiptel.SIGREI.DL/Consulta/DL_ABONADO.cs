﻿using Osiptel.SIGREI.BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;

namespace Osiptel.SIGREI.DL
{
    public class DL_ABONADO
    {

        public List<BE_ABONADO> BuscarAbonados(ref BE_PAGINACION objPaginacion, BE_ABONADO pFiltros)
        {
            BE_ABONADO objConsultaBE;
            List<BE_ABONADO> llstConsultaBE = new List<BE_ABONADO>();
            
            try
            {
                String cadena = new DL_CONEXION().retStrConexion();
                using (OracleConnection conexOsipweb = new OracleConnection(cadena))
                {

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conexOsipweb;


                    cmd.CommandText = "PKG_CONSULTA.SP_BUSCAR_ABONADOS";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 18000;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoConsulta", OracleType.Number, ParameterDirection.Input, pFiltros.tipoConsulta));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresUno", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresUno));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresDos", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresDos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresTres", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresTres));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresCuatro", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresCuatro));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoDocumento", OracleType.VarChar, ParameterDirection.Input, pFiltros.tipoDocLegal));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaActivacionInicio", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaActivacionInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaActivacionFin", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaActivacionFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pagina", OracleType.Number, ParameterDirection.Input, objPaginacion.Numero_Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_RegxPag", OracleType.Number, ParameterDirection.Input, objPaginacion.Tamanio_Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    try
                    {
                        conexOsipweb.Open();

                        using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                        {
                            while (objDRConsulta.Read())
                            {
                                objConsultaBE = new BE_ABONADO();
                                objPaginacion.Total_Archivos = DL_HELPER.getInt64(objDRConsulta, "TOTAL");
                                objConsultaBE.nroItem = DL_HELPER.getInt32(objDRConsulta, "NRO_ITEM");
                                objConsultaBE.nroImei = DL_HELPER.getString(objDRConsulta, "IMEI");

                                objConsultaBE.nroImsi = DL_HELPER.getString(objDRConsulta, "IMSI");
                                objConsultaBE.nombreConcesionaria = Convert.ToString(objDRConsulta["CONCESIONARIO"].ToString());
                                objConsultaBE.nroServicioMovil = Convert.ToString(objDRConsulta["NUMERO_SERVICIO_MOVIL"].ToString());
                                objConsultaBE.tipoAbonado = Convert.ToString(objDRConsulta["DESC_TIPO_ABONADO"].ToString());

                                objConsultaBE.nombresAbonado = Convert.ToString(objDRConsulta["NOMBRES_ABONADO"].ToString());
                                objConsultaBE.ApPaternoAbonado = Convert.ToString(objDRConsulta["A_PATERNO_ABONADO"].ToString());
                                objConsultaBE.ApMaternoAbonado = Convert.ToString(objDRConsulta["A_MATERNO_ABONADO"].ToString());
                                objConsultaBE.razonSocial = Convert.ToString(objDRConsulta["RAZON_SOCIAL"]);
                                objConsultaBE.tipoDocLegal = Convert.ToString(objDRConsulta["TIPO_DOC_LEGAL"].ToString());
                                objConsultaBE.nroDocLegal = Convert.ToString(objDRConsulta["NUMERO_DOC_LEGAL"].ToString());

                                objConsultaBE.nombreRepLegal = Convert.ToString(objDRConsulta["NOMBRE_REP_LEGAL"].ToString());
                                objConsultaBE.ApPatenoRepLegal = Convert.ToString(objDRConsulta["A_PATERNO_REP_LEGAL"].ToString());
                                objConsultaBE.ApMatenoRepLegal = Convert.ToString(objDRConsulta["A_MATERNO_REP_LEGAL"].ToString());
                                objConsultaBE.tipoDocRepLegal = Convert.ToString(objDRConsulta["TIPO_DOC_LEGAL_REP_LEGAL"].ToString());
                                objConsultaBE.nroDocRepLegal = Convert.ToString(objDRConsulta["NUMERO_DOC_LEGAL_REP_LEGAL"].ToString());
                                objConsultaBE.fechaActivacion = Convert.ToString(objDRConsulta["FECHA_HORA_ACTIVACION"].ToString());
                                objConsultaBE.estadoServicio = Convert.ToString(objDRConsulta["ESTADO_SERVICIO"].ToString());
                                objConsultaBE.marcaEquipo = Convert.ToString(objDRConsulta["MARCA_EQUIPO"]);
                                objConsultaBE.modeloEquipo = Convert.ToString(objDRConsulta["MODELO_EQUIPO"]);

                                objConsultaBE.marcaGsma = Convert.ToString(objDRConsulta["MARCA_GSMA"]);
                                objConsultaBE.modeloGsma = Convert.ToString(objDRConsulta["MODELO_GSMA"]);

                                objConsultaBE.vinculacion = Convert.ToString(objDRConsulta["DESC_VINCULACION"].ToString());
                                objConsultaBE.fechaVinculacion = Convert.ToString(objDRConsulta["FECHA_HORA_VINCULACION"].ToString());

                                objConsultaBE.fechaRegistro = Convert.ToString(objDRConsulta["FECHA_REGISTRO"]);

                                llstConsultaBE.Add(objConsultaBE);
                            }
                            objDRConsulta.Close();
                        }
                        conexOsipweb.Close();

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        if (conexOsipweb.State == ConnectionState.Open) conexOsipweb.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return llstConsultaBE;
        }


        public DataTable ExportarAbonados_DT(BE_ABONADO pFiltros)
        {
            DataTable dtResultado = new DataTable();
            try
            {
                String cadena = new DL_CONEXION().retStrConexion();
                using (OracleConnection conexOsipweb = new OracleConnection(cadena))
                {

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conexOsipweb;
                    cmd.CommandText = "PKG_CONSULTA.SP_EXPORTAR_ABONADOS";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 18000;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoConsulta", OracleType.Number, ParameterDirection.Input, pFiltros.tipoConsulta));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresUno", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresUno));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresDos", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresDos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresTres", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresTres));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresCuatro", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresCuatro));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoDocumento", OracleType.VarChar, ParameterDirection.Input, pFiltros.tipoDocLegal));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaActivacionInicio", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaActivacionInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaActivacionFin", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaActivacionFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.VarChar, ParameterDirection.Input, pFiltros.idPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));
                    try
                    {
                        conexOsipweb.Open();

                        using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                        {
                            dtResultado.Load(objDRConsulta);
                        }
                        conexOsipweb.Close();

                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        return dtResultado;
                    }
                    finally
                    {
                        if (conexOsipweb.State == ConnectionState.Open) conexOsipweb.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtResultado;
        }

        public List<BE_ABONADO> BuscarListaNegra(ref BE_PAGINACION objPaginacion, BE_ABONADO pFiltros)
        {
            BE_ABONADO objConsultaBE;
            List<BE_ABONADO> llstConsultaBE = new List<BE_ABONADO>();

            try
            {
                String cadena = new DL_CONEXION().retStrConexion();
                using (OracleConnection conexOsipweb = new OracleConnection(cadena))
                {

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conexOsipweb;

                    cmd.CommandText = "PKG_CONSULTA.SP_BUSCAR_LISTANEGRA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 18000;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoConsulta", OracleType.Number, ParameterDirection.Input, pFiltros.tipoConsulta));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresUno", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresUno));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresDos", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresDos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresTres", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresTres));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresCuatro", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresCuatro));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaReporteInicio", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaReportaInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaReporteFin", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaReportaFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaBloqueoInicio", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaBloqueoInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaBloqueoFin", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaBloqueoFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pagina", OracleType.Number, ParameterDirection.Input, objPaginacion.Numero_Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_RegxPag", OracleType.Number, ParameterDirection.Input, objPaginacion.Tamanio_Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    try
                    {
                        conexOsipweb.Open();

                        using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                        {
                            while (objDRConsulta.Read())
                            {
                                objConsultaBE = new BE_ABONADO();
                                objPaginacion.Total_Archivos = DL_HELPER.getInt64(objDRConsulta, "TOTAL");
                                objConsultaBE.nroItem = DL_HELPER.getInt32(objDRConsulta, "NRO_ITEM");
                                objConsultaBE.nombreConcesionaria = Convert.ToString(objDRConsulta["CONCESIONARIO"].ToString());
                                objConsultaBE.nroServicioMovil = Convert.ToString(objDRConsulta["NUMERO_SERVICIO_MOVIL"].ToString());
                                objConsultaBE.nroImsi = DL_HELPER.getString(objDRConsulta, "IMSI");
                                objConsultaBE.nroImei = DL_HELPER.getString(objDRConsulta, "IMEI");
                                objConsultaBE.marcaEquipo = Convert.ToString(objDRConsulta["MARCA_EQUIPO"]);
                                objConsultaBE.modeloEquipo = Convert.ToString(objDRConsulta["MODELO_EQUIPO"]);

                                objConsultaBE.marcaGsma = Convert.ToString(objDRConsulta["MARCA_GSMA"]);
                                objConsultaBE.modeloGsma = Convert.ToString(objDRConsulta["MODELO_GSMA"]);

                                objConsultaBE.nroTelefonoReporta = Convert.ToString(objDRConsulta["NUMERO_SERVICIO_REPORTA"].ToString());
                                objConsultaBE.fuenteReporte = Convert.ToString(objDRConsulta["FUENTE_REPORTE"].ToString());
                                objConsultaBE.motivoReporte = Convert.ToString(objDRConsulta["MOTIVO_REPORTE"].ToString());
                                objConsultaBE.codigoReporte = Convert.ToString(objDRConsulta["CODIGO_REPORTE"].ToString());

                                objConsultaBE.fechaReporte = Convert.ToString(objDRConsulta["FECHA_REPORTE"].ToString());
                                objConsultaBE.fechaBloqueoDesbloqueo = Convert.ToString(objDRConsulta["FECHA_BLOQUEO"].ToString());

                                objConsultaBE.nombresAbonado = Convert.ToString(objDRConsulta["NOMBRES_ABONADO_USUARIO"].ToString());
                                objConsultaBE.ApPaternoAbonado = Convert.ToString(objDRConsulta["A_PATERNO_ABONADO_USUARIO"].ToString());
                                objConsultaBE.ApMaternoAbonado = Convert.ToString(objDRConsulta["A_MATERNO_ABONADO_USUARIO"].ToString());
                                objConsultaBE.razonSocial = Convert.ToString(objDRConsulta["RAZON_SOCIAL"]);
                                objConsultaBE.tipoDocLegal = Convert.ToString(objDRConsulta["TIPO_DOC_LEGAL"].ToString());
                                objConsultaBE.nroDocLegal = Convert.ToString(objDRConsulta["NUMERO_DOC_LEGAL"].ToString());

                                objConsultaBE.nombreRepLegal = Convert.ToString(objDRConsulta["NOMBRE_REP_LEGAL"].ToString());
                                objConsultaBE.ApPatenoRepLegal = Convert.ToString(objDRConsulta["A_PATERNO_REP_LEGAL"].ToString());
                                objConsultaBE.ApMatenoRepLegal = Convert.ToString(objDRConsulta["A_MATERNO_REP_LEGAL"].ToString());
                                objConsultaBE.tipoDocRepLegal = Convert.ToString(objDRConsulta["TIPO_DOC_LEGAL_REP_LEGAL"].ToString());
                                objConsultaBE.nroDocRepLegal = Convert.ToString(objDRConsulta["NUMERO_DOC_LEGAL_REP_LEGAL"].ToString());
                                objConsultaBE.descEstado = Convert.ToString(objDRConsulta["ESTADO"].ToString());
                                objConsultaBE.fechaRegistro = Convert.ToString(objDRConsulta["FECHA_REGISTRO"].ToString());

                                llstConsultaBE.Add(objConsultaBE);
                            }
                            objDRConsulta.Close();
                        }


                        conexOsipweb.Open();
                    }

                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        return new List<BE_ABONADO>();
                    }
                    finally
                    {
                        if (conexOsipweb.State == ConnectionState.Open) conexOsipweb.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return llstConsultaBE;
        }

        public DataTable ExportarListaNegra_DT(BE_ABONADO pFiltros)
        {
            DataTable dtResultado = new DataTable();
            try
            {
                String cadena = new DL_CONEXION().retStrConexion();
                using (OracleConnection conexOsipweb = new OracleConnection(cadena))
                {

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conexOsipweb;

                    cmd.CommandText = "PKG_CONSULTA.SP_EXPORTAR_LISTANEGRA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 18000;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoConsulta", OracleType.Number, ParameterDirection.Input, pFiltros.tipoConsulta));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresUno", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresUno));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresDos", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresDos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresTres", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresTres));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresCuatro", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresCuatro));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaReporteInicio", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaReportaInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaReporteFin", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaReportaFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaBloqueoInicio", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaBloqueoInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaBloqueoFin", OracleType.VarChar, ParameterDirection.Input, pFiltros.fechaBloqueoFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.VarChar, ParameterDirection.Input, pFiltros.idPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    try
                    {
                        conexOsipweb.Open();

                        using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                        {
                            dtResultado.Load(objDRConsulta);
                        }
                        conexOsipweb.Close();
                    
                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        return dtResultado;
                    }
                    finally
                    {
                        if (conexOsipweb.State == ConnectionState.Open) conexOsipweb.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtResultado;
        }
    }
}
