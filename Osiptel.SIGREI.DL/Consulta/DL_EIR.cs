﻿using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;

namespace Osiptel.SIGREI.DL
{


    public class DL_EIR
    {
        private readonly IDbConnection _dbConnection;
        public DL_EIR(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }
        public DL_EIR()
        {

        }

        public List<BE_EIR> BuscarEIR(ref BE_PAGINACION objPaginacion, BE_EIR pFiltros)
        {
            BE_EIR objConsultaBE;
            List<BE_EIR> llstConsultaBE = new List<BE_EIR>();

            try
            {

                String cadena = new DL_CONEXION().retStrConexion();
                using (OracleConnection conexOsipweb = new OracleConnection(cadena))
                {

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conexOsipweb;

                    cmd.CommandText = "PKG_CONSULTA.SP_BUSCAR_LISTAEIR";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 18000;


                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoConsulta", OracleType.Number, ParameterDirection.Input, pFiltros.tipoConsulta));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresUno", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresUno));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresDos", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresDos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresTres", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresTres));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresCuatro", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresCuatro));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pagina", OracleType.Number, ParameterDirection.Input, objPaginacion.Numero_Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_RegxPag", OracleType.Number, ParameterDirection.Input, objPaginacion.Tamanio_Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    try
                    {
                        conexOsipweb.Open();

                        using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                        {
                            while (objDRConsulta.Read())
                            {
                                objConsultaBE = new BE_EIR();
                                objPaginacion.Total_Archivos = DL_HELPER.getInt64(objDRConsulta, "TOTAL");

                                objConsultaBE.nroItem = DL_HELPER.getInt32(objDRConsulta, "NRO_ITEM");
                                objConsultaBE.nombreConcesionaria = Convert.ToString(objDRConsulta["CONCESIONARIO"].ToString());
                                objConsultaBE.nroImei = DL_HELPER.getString(objDRConsulta, "IMEI");
                                objConsultaBE.periodo = Convert.ToString(objDRConsulta["PERIODO"].ToString());
                                objConsultaBE.anio = Convert.ToString(objDRConsulta["ANIO"].ToString());
                                objConsultaBE.mes = Convert.ToString(objDRConsulta["MES"].ToString());
                                objConsultaBE.fechaRegistro = Convert.ToString(objDRConsulta["FECHA_REGISTRO"].ToString());


                                llstConsultaBE.Add(objConsultaBE);
                            }
                            objDRConsulta.Close();
                        }
                        conexOsipweb.Close();


                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        return new List<BE_EIR>();
                    }
                    finally
                    {
                        if (conexOsipweb.State == ConnectionState.Open) conexOsipweb.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return llstConsultaBE;
        }

        public DataTable ExportarEIR_DT(BE_EIR pFiltros)
        {
            DataTable dtResultado = new DataTable();
            try
            {
                String cadena = new DL_CONEXION().retStrConexion();
                using (OracleConnection conexOsipweb = new OracleConnection(cadena))
                {

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = conexOsipweb;

                    cmd.CommandText = "PKG_CONSULTA.SP_EXPORTAR_LISTAEIR";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 18000;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoConsulta", OracleType.Number, ParameterDirection.Input, pFiltros.tipoConsulta));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresUno", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresUno));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresDos", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresDos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresTres", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresTres));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ValoresCuatro", OracleType.VarChar, ParameterDirection.Input, pFiltros.valoresCuatro));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    try
                    {
                        conexOsipweb.Open();

                        using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                        {

                            dtResultado.Load(objDRConsulta);
                        }
                        conexOsipweb.Close();

                    }
                    catch (Exception ex)
                    {
                        string error = ex.ToString();
                        return dtResultado;
                    }
                    finally
                    {
                        if (conexOsipweb.State == ConnectionState.Open) conexOsipweb.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtResultado;
        }

    }
}
