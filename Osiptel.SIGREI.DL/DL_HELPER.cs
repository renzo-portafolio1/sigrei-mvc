﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.ComponentModel;

namespace Osiptel.SIGREI.DL
{
    internal static class DL_HELPER
    {
        public static OracleParameter getParam(string pClave, OracleType pTipo)
        {
            return getParam(pClave, pTipo, 0);
        }

        public static OracleParameter getParam(string pClave, OracleType pTipo, int pLongitud)
        {
            OracleParameter prm = new OracleParameter();
            prm.ParameterName = pClave;
            prm.OracleType = pTipo;
            prm.Direction = ParameterDirection.Output;
            if (pLongitud > 0)
                prm.Size = pLongitud;

            return prm;
        }

        public static OracleParameter getParam(string pClave, OracleType pTipo, ParameterDirection pdireccion, object pValor)
        {
            OracleParameter prm = new OracleParameter();
            prm.ParameterName = pClave;
            prm.OracleType = pTipo;

            if (pValor != null) { 
            prm.Value = pValor;
            } 
            else {
                if (pTipo == OracleType.Number) prm.Value = 0;
                if (pTipo == OracleType.VarChar) prm.Value = "";
                if (pTipo == OracleType.DateTime) prm.Value = null;
            
            }

          prm.Direction = pdireccion;

            return prm;
        }

        #region Lectura de DataReader

        public static Int32 getInt32(OracleDataReader _dr, string pClave)
        {
            return (_dr[pClave] != DBNull.Value) ? Convert.ToInt32(_dr[pClave]) : default(Int32);
        }
        public static Int64 getInt64(OracleDataReader _dr, string pClave)
        {
            return (_dr[pClave] != DBNull.Value) ? Convert.ToInt64(_dr[pClave]) : default(Int64);
        }
        public static DateTime getDateTime(OracleDataReader _dr, string pClave)
        {
            return (_dr[pClave] != DBNull.Value) ? Convert.ToDateTime(_dr[pClave]) : default(DateTime);
        }
        public static Decimal getDecimal(OracleDataReader _dr, string pClave)
        {
            return (_dr[pClave] != DBNull.Value) ? Convert.ToDecimal(_dr[pClave]) : default(Decimal);
        }    
        public static Double getDouble(OracleDataReader _dr, string pClave)
        {
            return (_dr[pClave] != DBNull.Value) ? Convert.ToDouble(_dr[pClave]) : default(Double);
        }
        public static String getString(OracleDataReader _dr, string pClave)
        {
            return (_dr[pClave] != DBNull.Value) ? Convert.ToString(_dr[pClave]) : default(String);
        }
        public static Decimal? getDecimalNull(OracleDataReader _dr, string pClave)
        {
            if (_dr[pClave] != DBNull.Value)
            {
                return  Convert.ToDecimal(_dr[pClave]) ;
            }

            return null;
        }
        #endregion
    }
}
