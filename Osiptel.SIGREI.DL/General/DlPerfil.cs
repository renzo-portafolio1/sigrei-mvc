﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Data.OracleClient;

using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DlPerfil
    {
        public List<BePerfil> ListarPerfilByUsuario(BePerfil pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_PERFIL_BY_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, pFiltros.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BePerfil objConsultaBE;
                            List<BePerfil> llstConsultaBE = new List<BePerfil>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BePerfil();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idPerfil = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BePerfil>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BePerfil> ValidarPerfilDuplicado(BePerfil pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_VALIDAR_PERFIL_DUPLICADO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, pFiltros.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.Int16, ParameterDirection.Input, pFiltros.idPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BePerfil objConsultaBE;
                            List<BePerfil> llstConsultaBE = new List<BePerfil>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BePerfil();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idPerfil = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BePerfil>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BePerfil> ListarPerfilByEstado_filtro(BePerfil pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_PERFIL_ENT_PUBLICA";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BePerfil objConsultaBE;
                            List<BePerfil> llstConsultaBE = new List<BePerfil>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BePerfil();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idPerfil = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BePerfil>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BePerfil> ListarPerfilByEstado(BePerfil pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_PERFIL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pAplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("oListado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BePerfil objConsultaBE;
                            List<BePerfil> llstConsultaBE = new List<BePerfil>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BePerfil();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idPerfil = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new List<BePerfil>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BePerfil> ValidarAsignacionPerfilMod(BePerfil pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_VALIDAR_ASIGNACION_PERFIL_MODULO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Codigo", OracleType.Number, ParameterDirection.Input, pFiltros.idPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Modulo", OracleType.Number, ParameterDirection.Input, pFiltros.idModulo));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BePerfil objConsultaBE;
                            List<BePerfil> llstConsultaBE = new List<BePerfil>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BePerfil();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idPerfil = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.idModulo = DL_HELPER.getInt32(objDRConsulta, "IDMODULO");
                                    objConsultaBE.cantidad = DL_HELPER.getInt32(objDRConsulta, "CANTIDAD");
                                   

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BePerfil>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BePerfil> ListarPerfilByTodo(BePerfil pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_PERFIL_TODO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pAplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("oListado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BePerfil objConsultaBE;
                            List<BePerfil> llstConsultaBE = new List<BePerfil>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BePerfil();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idPerfil = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.activo = DL_HELPER.getString(objDRConsulta, "ESTADO");
                                    if (Convert.ToString(objDRConsulta["ESTADO"]) == "1")
                                    {
                                        objConsultaBE.descEstado = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.descEstado = "Inactivo";
                                    }


                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {
                            
                            return new List<BePerfil>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public String SP_INSERTAR_PERFIL(BePerfil ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_INSERTAR_PERFIL_GENERAL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, ParametroEP.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.Int16, ParameterDirection.Input, 0));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Descripcion", OracleType.VarChar, ParameterDirection.Input, ParametroEP.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.VarChar, ParameterDirection.Input, ParametroEP.activo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, ParametroEP.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Int16, ParameterDirection.Output,0));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.VarChar, ParameterDirection.Output, ""));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            //resp = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public BePerfil SP_OBTENER_PERFIL_BY_CODIGO(BePerfil pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_OBTENER_PERFIL_X_ID";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pIdPerfil", OracleType.Int16, ParameterDirection.Input, pFiltros.idPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("pAplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.descripcion));

                    cmd.Parameters.Add(DL_HELPER.getParam("oListado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        BePerfil objConsultaBE = new BePerfil();
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            
                            List<BePerfil> llstConsultaBE = new List<BePerfil>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BePerfil();

                                    objConsultaBE.idPerfil = DL_HELPER.getInt32(objDRConsulta, "idperfil");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "descripcion");
                                    objConsultaBE.activo = DL_HELPER.getString(objDRConsulta, "estado");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new BePerfil();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public String SP_ACTUALIZAR_PERFIL(BePerfil ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_ACTUALIZAR_PERFIL_GENERAL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, ParametroEP.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.Int16, ParameterDirection.Input, ParametroEP.idPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Descripcion", OracleType.VarChar, ParameterDirection.Input, ParametroEP.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Int16, ParameterDirection.Input, ParametroEP.activo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuAct", OracleType.VarChar, ParameterDirection.Input, ParametroEP.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Int16, ParameterDirection.Output, 0));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.VarChar, ParameterDirection.Output, ""));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            //resp = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }
    }
}
