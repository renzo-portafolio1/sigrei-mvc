﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_USUARIOGPSU
    {

        public List<BE_USUARIOGPSU> Listar(BE_USUARIOGPSU pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Unidad", OracleType.VarChar, ParameterDirection.Input, pFiltros.unidad));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Opcion", OracleType.Number, ParameterDirection.Input, pFiltros.opcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_USUARIOGPSU objConsultaBE;
                            List<BE_USUARIOGPSU> llstConsultaBE = new List<BE_USUARIOGPSU>();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_USUARIOGPSU();

                                    objConsultaBE.codigo = DL_HELPER.getString(objDRConsulta, "CODUSUARIO");
                                    objConsultaBE.nombreCompleto = DL_HELPER.getString(objDRConsulta, "NOMCOMPLETO");
                                    
                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            return new List<BE_USUARIOGPSU>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
   
    }
}
