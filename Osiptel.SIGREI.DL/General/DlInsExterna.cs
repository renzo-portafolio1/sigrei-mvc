﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DlInsExterna
    {
        public String SP_INSERTAR_REGISTRO_CONSULTA_WS(String Aplicacion, String idOpcion, String usuario, String num_ruc, String origen, String proceso, String respuesta, String localIP, String mac)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_INSERTAR_AUDITORIA_CONSULTA";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input,Aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Opcion", OracleType.VarChar, ParameterDirection.Input, idOpcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, usuario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroDoc", OracleType.VarChar, ParameterDirection.Input, num_ruc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Origen", OracleType.VarChar, ParameterDirection.Input, origen));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Proceso", OracleType.VarChar, ParameterDirection.Input, proceso));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Respuesta", OracleType.VarChar, ParameterDirection.Input, respuesta));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Ip", OracleType.VarChar ,ParameterDirection.Input, localIP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Mac", OracleType.VarChar, ParameterDirection.Input, mac));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_NumError", OracleType.Int16, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_MsgError", OracleType.VarChar, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }
        public String SP_INERTAR_INSTITUCION_EXTERNA(BeInsExterna ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_REGISTRAR_INSTITUCION_EXTERNA";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumRuc", OracleType.VarChar, ParameterDirection.Input, ParametroEP.NUM_RUC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_DesRazSocial", OracleType.VarChar, ParameterDirection.Input, ParametroEP.DES_RAZ_SOC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipInsExt", OracleType.VarChar, ParameterDirection.Input, ParametroEP.TIP_INSTEXT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, ParametroEP.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_EntPublica", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDENTPUBLICA));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_retorno", OracleType.Int16, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public List<BeInsExterna> ListarInsExternasByEP(int nCodEP, int estado)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_LISTAR_INST_EXTERNA_BY_ENT_PUBLICA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_EntPublica", OracleType.Int16, ParameterDirection.Input, nCodEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.VarChar, ParameterDirection.Input,estado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeInsExterna objConsultaBE;
                            List<BeInsExterna> llstConsultaBE = new List<BeInsExterna>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int orden = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BeInsExterna();

                                    objConsultaBE.orden = orden;
                                    objConsultaBE.NUM_RUC = DL_HELPER.getString(objDRConsulta, "NUM_RUC");
                                    objConsultaBE.DES_RAZ_SOC = DL_HELPER.getString(objDRConsulta, "DES_RAZ_SOC");
                                    objConsultaBE.TIP_INSTEXT = DL_HELPER.getString(objDRConsulta, "TIP_INSTEXT");
                                    objConsultaBE.IDENTPUBLICA = DL_HELPER.getInt32(objDRConsulta, "IDENTPUBLICA");

                                    objConsultaBE.ACTIVO = DL_HELPER.getString(objDRConsulta, "ACTIVO");

                                    if (Convert.ToString(objDRConsulta["ACTIVO"]) == "1")
                                    {
                                        objConsultaBE.DesActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.DesActivo = "Inactivo";
                                    }
                                    llstConsultaBE.Add(objConsultaBE);

                                    orden++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BeInsExterna>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BeInsExterna> ListarInstituciones_Externas_By_Todo()
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_LISTAR_INST_EXTERNA_BY_TODO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeInsExterna objConsultaBE;
                            List<BeInsExterna> llstConsultaBE = new List<BeInsExterna>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int orden = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BeInsExterna();

                                    objConsultaBE.orden =orden;
                                    objConsultaBE.NUM_RUC = DL_HELPER.getString(objDRConsulta, "NUM_RUC");
                                    objConsultaBE.DES_RAZ_SOC = DL_HELPER.getString(objDRConsulta, "DES_RAZ_SOC");
                                    objConsultaBE.TIP_INSTEXT = DL_HELPER.getString(objDRConsulta, "TIP_INSTEXT");
                                    objConsultaBE.TipInstitucion = DL_HELPER.getString(objDRConsulta, "TipInstitucion");

                                    objConsultaBE.IDENTPUBLICA = DL_HELPER.getInt32(objDRConsulta, "IDENTPUBLICA");
                                    objConsultaBE.DesEntPublica = DL_HELPER.getString(objDRConsulta, "DesEntPublica");

                                    objConsultaBE.ACTIVO = DL_HELPER.getString(objDRConsulta, "ACTIVO");

                                    if (Convert.ToString(objDRConsulta["ACTIVO"]) == "1")
                                    {
                                        objConsultaBE.DesActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.DesActivo = "Inactivo";
                                    }
                                    llstConsultaBE.Add(objConsultaBE);

                                    orden++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new List<BeInsExterna>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public BeInsExterna SP_OBTENER_INST_EXTERNA_BY_RUC(BeInsExterna pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_OBTENER_INST_EXTERNA";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_num_ruc", OracleType.VarChar, ParameterDirection.Input, pFiltros.NUM_RUC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_usuAuditoria", OracleType.VarChar, ParameterDirection.Input, pFiltros.USUCRE));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_listado", OracleType.Cursor));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_retorno", OracleType.Int16, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        BeInsExterna objConsultaBE = new BeInsExterna();
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();

                            List<BeInsExterna> llstConsultaBE = new List<BeInsExterna>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BeInsExterna();
                                    objConsultaBE.NUM_RUC = DL_HELPER.getString(objDRConsulta, "NUM_RUC");
                                    objConsultaBE.NUM_RUC_ACT = DL_HELPER.getString(objDRConsulta, "NUM_RUC");
                                    objConsultaBE.DES_RAZ_SOC = DL_HELPER.getString(objDRConsulta, "DES_RAZ_SOC");
                                    objConsultaBE.TIP_INSTEXT = DL_HELPER.getString(objDRConsulta, "TIP_INSTEXT");
                                    objConsultaBE.ACTIVO = DL_HELPER.getString(objDRConsulta, "ACTIVO");
                                    objConsultaBE.IDENTPUBLICA = DL_HELPER.getInt32(objDRConsulta, "IDENTPUBLICA");

                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception e)
                        {

                            return new BeInsExterna();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public String SP_ACTUALIZAR_INSTITUCION_EXTERNA(BeInsExterna ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_ACTUALIZAR_INST_EXTERNA_BY_RUC";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumRuc", OracleType.Int16, ParameterDirection.Input, ParametroEP.NUM_RUC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_RazSocial", OracleType.VarChar, ParameterDirection.Input, ParametroEP.DES_RAZ_SOC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipInsEx", OracleType.VarChar, ParameterDirection.Input, ParametroEP.TIP_INSTEXT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Activo", OracleType.VarChar, ParameterDirection.Input, ParametroEP.ACTIVO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuAct", OracleType.VarChar, ParameterDirection.Input, ParametroEP.USUMOD));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_EntPublica", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDENTPUBLICA));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_retorno", OracleType.Int16, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;
        }
    }
}
