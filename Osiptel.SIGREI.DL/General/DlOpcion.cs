﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Data.OracleClient;

using Osiptel.SIGREI.BE;
namespace Osiptel.SIGREI.DL
{
    public class DlOpcion
    {
        public List<BePerfilModuloOpcion> ListarModulo_Opcion_By_Modulo_By_Perfil(string aplicacion, int idPerfil, int idModulo)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_LISTAR_OPCION_BY_MODULO_BY_PERFIL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.Int16, ParameterDirection.Input, idPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdModulo", OracleType.Int16, ParameterDirection.Input, idModulo));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BePerfilModuloOpcion objConsultaBE;
                            List<BePerfilModuloOpcion> llstConsultaBE = new List<BePerfilModuloOpcion>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BePerfilModuloOpcion();


                                    objConsultaBE.IDMODULO = DL_HELPER.getInt32(objDRConsulta, "IDMODULO");
                                    objConsultaBE.IDPERFIL = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.IDOPCION = DL_HELPER.getInt32(objDRConsulta, "IDOPCION");
                                    objConsultaBE.IDPADRE = DL_HELPER.getInt32(objDRConsulta, "IDPADRE");
                                    objConsultaBE.DESCRIPCION = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.ACTIVO = DL_HELPER.getInt32(objDRConsulta, "ACTIVO");




                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            return new List<BePerfilModuloOpcion>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BeAccesoOpcion> ValidarMenuDuplicado(string aplicacion, string descripcion, int idModulo)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_VALIDAR_OPCION_DUPLICADO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdOpcion", OracleType.Int16, ParameterDirection.Input, idModulo));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeAccesoOpcion objConsultaBE;
                            List<BeAccesoOpcion> llstConsultaBE = new List<BeAccesoOpcion>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeAccesoOpcion();

                                    objConsultaBE.IDOPCION = DL_HELPER.getString(objDRConsulta, "IDOPCION");
                                    objConsultaBE.DESCRIPCION = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            return new List<BeAccesoOpcion>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BeModuloOpcion> ListarModulo_Opcion_By_Modulo(string aplicacion, int idModulo)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_LISTAR_OPCION_BY_MODULO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdModulo", OracleType.Int16, ParameterDirection.Input, idModulo));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeModuloOpcion objConsultaBE;
                            List<BeModuloOpcion> llstConsultaBE = new List<BeModuloOpcion>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeModuloOpcion();

                                    
                                    objConsultaBE.IDMODULO = DL_HELPER.getInt32(objDRConsulta, "IDMODULO");
                                    objConsultaBE.IDOPCION = DL_HELPER.getInt32(objDRConsulta, "IDOPCION");
                                    objConsultaBE.IDPADRE = DL_HELPER.getInt32(objDRConsulta, "IDPADRE");
                                    objConsultaBE.DESCRIPCION = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.ACTIVO = DL_HELPER.getInt32(objDRConsulta, "ACTIVO");




                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            return new List<BeModuloOpcion>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BeAccesoOpcion> ListarOpcionByTodo(string aplicacion)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_OPCION_BY_TODO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeAccesoOpcion objConsultaBE;
                            List<BeAccesoOpcion> llstConsultaBE = new List<BeAccesoOpcion>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeAccesoOpcion();

                                    objConsultaBE.NROITEM = num;
                                    objConsultaBE.IDOPCION = DL_HELPER.getString(objDRConsulta, "IDOPCION");
                                    objConsultaBE.APLICACION = DL_HELPER.getString(objDRConsulta, "APLICACION");
                                    objConsultaBE.OBJETO = DL_HELPER.getString(objDRConsulta, "OBJETO");
                                    objConsultaBE.RUTA = DL_HELPER.getString(objDRConsulta, "RUTA");

                                    objConsultaBE.DESCRIPCION = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.IDPADRE = DL_HELPER.getString(objDRConsulta, "IDPADRE");
                                    objConsultaBE.ORDEN = DL_HELPER.getString(objDRConsulta, "ORDEN");
                                   

                                    objConsultaBE.ESTADO = DL_HELPER.getString(objDRConsulta, "ESTADO");

                                  

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new List<BeAccesoOpcion>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BeAccesoOpcion> ListarMenuPrincipales(string aplicacion)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_OPCION_RAIZ";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("sAplicacion", OracleType.VarChar, ParameterDirection.Input, aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("oListado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeAccesoOpcion objConsultaBE;
                            List<BeAccesoOpcion> llstConsultaBE = new List<BeAccesoOpcion>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeAccesoOpcion();

                                    objConsultaBE.NROITEM = num;
                                    objConsultaBE.IDOPCION = DL_HELPER.getString(objDRConsulta, "IDOPCION");
                                    objConsultaBE.APLICACION = DL_HELPER.getString(objDRConsulta, "APLICACION");
                                    objConsultaBE.OBJETO = DL_HELPER.getString(objDRConsulta, "OBJETO");
                                    objConsultaBE.RUTA = DL_HELPER.getString(objDRConsulta, "RUTA");

                                    objConsultaBE.DESCRIPCION = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.IDPADRE = DL_HELPER.getString(objDRConsulta, "IDPADRE");
                                    objConsultaBE.ORDEN = DL_HELPER.getString(objDRConsulta, "ORDEN");


                                    objConsultaBE.ESTADO = DL_HELPER.getString(objDRConsulta, "ESTADO");



                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new List<BeAccesoOpcion>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public String SP_INSERTAR_MENU(BeAccesoOpcion UnidadEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_INSERTAR_OPCION_GENERAL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.APLICACION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdOpcion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.IDOPCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Ruta", OracleType.VarChar, ParameterDirection.Input, UnidadEP.RUTA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Descripcion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DESCRIPCION));

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPadre", OracleType.VarChar, ParameterDirection.Input, UnidadEP.IDPADRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Orden", OracleType.VarChar, ParameterDirection.Input, UnidadEP.ORDEN));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FlgRuta", OracleType.VarChar, ParameterDirection.Input, UnidadEP.FLGRUTA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.VarChar, ParameterDirection.Input, UnidadEP.ESTADO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, UnidadEP.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output,""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output,""));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {
                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public BeAccesoOpcion SP_OBTENER_MENU_BY_CODIGO(BeAccesoOpcion pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_OPCION_BY_CODIGO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.APLICACION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdOpcion", OracleType.VarChar, ParameterDirection.Input, pFiltros.IDOPCION));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        BeAccesoOpcion objConsultaBE = new BeAccesoOpcion();
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            
                            List<BeAccesoOpcion> llstConsultaBE = new List<BeAccesoOpcion>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BeAccesoOpcion();

                                   
                                    objConsultaBE.IDOPCION = DL_HELPER.getString(objDRConsulta, "IDOPCION");
                                    objConsultaBE.APLICACION = DL_HELPER.getString(objDRConsulta, "APLICACION");
                                    objConsultaBE.OBJETO = DL_HELPER.getString(objDRConsulta, "OBJETO");
                                    objConsultaBE.RUTA = DL_HELPER.getString(objDRConsulta, "RUTA");

                                    objConsultaBE.DESCRIPCION = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.IDPADRE = DL_HELPER.getString(objDRConsulta, "IDPADRE");
                                    objConsultaBE.ORDEN = DL_HELPER.getString(objDRConsulta, "ORDEN");


                                    objConsultaBE.ESTADO = DL_HELPER.getString(objDRConsulta, "ESTADO");

                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new BeAccesoOpcion();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public String SP_ACTUALIZAR_MENU(BeAccesoOpcion UnidadEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_ACTUALIZAR_OPCION_GENERAL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.APLICACION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdOpcion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.IDOPCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Ruta", OracleType.VarChar, ParameterDirection.Input, UnidadEP.RUTA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Descripcion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DESCRIPCION));

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPadre", OracleType.VarChar, ParameterDirection.Input, UnidadEP.IDPADRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Orden", OracleType.VarChar, ParameterDirection.Input, UnidadEP.ORDEN));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FlgRuta", OracleType.VarChar, ParameterDirection.Input, UnidadEP.FLGRUTA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.VarChar, ParameterDirection.Input, UnidadEP.ESTADO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, UnidadEP.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {
                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public String Eliminar_Registro_Modulos(string aplicacion, int idModulo, string usuAct)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_ELIMINAR_MODULO_OPCION";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdModulo", OracleType.Int16, ParameterDirection.Input, idModulo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuActuali", OracleType.VarChar, ParameterDirection.Input, usuAct));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));



                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public String SP_INSERTAR_MODULO_OPCION(BeModuloOpcion UnidadEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_REGISTRAR_MODULO_OPCION";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DESCRIPCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdOpcion", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDOPCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdModulo", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDMODULO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Activo", OracleType.Int16, ParameterDirection.Input, UnidadEP.ACTIVO));

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCreacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.USUCRE));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {
                          
                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public String Eliminar_Registro_Modulos_Perfil_Opcion(string aplicacion, int idModulo, int idPerfil, string usuMod)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_ELIMINAR_OPCION_BY_MODULO_BY_PERFIL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.Int16, ParameterDirection.Input, idPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdModulo", OracleType.Int16, ParameterDirection.Input, idModulo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuActuali", OracleType.VarChar, ParameterDirection.Input, usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));



                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {
                           
                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public String SP_INSERTAR_MODULO_OPCION_PERFIL(BePerfilModuloOpcion UnidadEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_REGISTRAR_MODULO_OPCION_PERFIL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DESCRIPCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdOpcion", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDOPCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDPERFIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdModulo", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDMODULO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Activo", OracleType.Int16, ParameterDirection.Input, UnidadEP.ACTIVO));

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCreacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.USUCRE));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {
                           
                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }
    }
}
