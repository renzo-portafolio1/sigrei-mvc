﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Data.OracleClient;

using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DlUnidadEp
    {
        public string SP_INSERTAR_UNIDAD_ENTIDAD_PUBLICA(BeUnidadEp UnidadEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UNIDAD_EP.SP_INSERTAR_UNIDAD_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_InsExterna", OracleType.VarChar, ParameterDirection.Input, UnidadEP.idInsExterna));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NombreEP", OracleType.VarChar, ParameterDirection.Input, UnidadEP.nomUniEntPublica));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Telefono", OracleType.VarChar, ParameterDirection.Input, UnidadEP.telefono));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Departamento", OracleType.VarChar, ParameterDirection.Input, UnidadEP.iddepartamento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Provincia", OracleType.VarChar, ParameterDirection.Input, UnidadEP.idprovincia));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Distrito", OracleType.VarChar, ParameterDirection.Input, UnidadEP.iddistrito));

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Direccion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.direccion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ExtCorreo", OracleType.VarChar, ParameterDirection.Input, UnidadEP.extCorreo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Activo", OracleType.Int16, ParameterDirection.Input, UnidadEP.activo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCreacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.usuCre));
                    /*cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));*/
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Int16, ParameterDirection.Output, 0));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.VarChar, ParameterDirection.Output, ""));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {
                            DL_LOG.Insertar("DlUnidadEp.SP_INSERTAR_UNIDAD_ENTIDAD_PUBLICA_1", exp.Message + " - " + exp.StackTrace, UnidadEP.usuCre);
                            //throw new Exception("Mensaje de Error: ", exp);
                            throw exp;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("DlUnidadEp.SP_INSERTAR_UNIDAD_ENTIDAD_PUBLICA_2", ex.Message + " - " + ex.StackTrace, UnidadEP.usuCre);
                //throw new Exception("Mensaje de Error: ", ex);
                throw ex;
            }
            return resp;

        }

        public List<BeUnidadEp> ValidarRegistroUnidadEP(int idUnidadEP, string nombreUEP)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UNIDAD_EP.SP_VALIDAR_DUPLICADO_UNIDAD_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdUnidadEP", OracleType.Int16, ParameterDirection.Input, idUnidadEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NombreEP", OracleType.VarChar, ParameterDirection.Input, nombreUEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUnidadEp objConsultaBE;
                            List<BeUnidadEp> llstConsultaBE = new List<BeUnidadEp>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUnidadEp();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idUnidadEP = DL_HELPER.getInt32(objDRConsulta, "IDUNIDADEP");
                                    objConsultaBE.idInsExterna = DL_HELPER.getString(objDRConsulta, "IDINSEXTERNA");
                                    objConsultaBE.nomUniEntPublica = DL_HELPER.getString(objDRConsulta, "NOMBREUNIDADP");

                                    objConsultaBE.telefono = DL_HELPER.getString(objDRConsulta, "TELEFONO");
                                    objConsultaBE.direccion = DL_HELPER.getString(objDRConsulta, "DIRECCION");
                                    if (Convert.ToString(objDRConsulta["EXTCORREO"]) == "" || objDRConsulta["EXTCORREO"] is null)
                                    {
                                        objConsultaBE.extCorreo = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.extCorreo = DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    }


                                    objConsultaBE.activo = DL_HELPER.getInt32(objDRConsulta, "ACTIVO");
                                    objConsultaBE.estado = DL_HELPER.getInt32(objDRConsulta, "ESTADO");

                                    if (Convert.ToString(objDRConsulta["ACTIVO"]) == "1")
                                    {
                                        objConsultaBE.desActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.desActivo = "Inactivo";
                                    }

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BeUnidadEp>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BeUnidadEp> ListarUnidadEP_By_Todo(string aplicacion, string tabla, string columna)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UNIDAD_EP.SP_LISTAR_UNIDAD_ENTIDAD_PUBLICA";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input,columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUnidadEp objConsultaBE;
                            List<BeUnidadEp> llstConsultaBE = new List<BeUnidadEp>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUnidadEp();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idUnidadEP = DL_HELPER.getInt32(objDRConsulta, "IDUNIDADEP");
                                    objConsultaBE.idInsExterna = DL_HELPER.getString(objDRConsulta, "IDINSEXTERNA");
                                    objConsultaBE.nomInsExterna = DL_HELPER.getString(objDRConsulta, "DES_RAZ_SOC");
                                    objConsultaBE.idEntidadP = DL_HELPER.getInt32(objDRConsulta, "IDENTPUBLICA");
                                    objConsultaBE.nomEntPublica = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.nomUniEntPublica = DL_HELPER.getString(objDRConsulta, "NOMBREUNIDADP");

                                    objConsultaBE.telefono = DL_HELPER.getString(objDRConsulta, "TELEFONO");
                                    objConsultaBE.direccion = DL_HELPER.getString(objDRConsulta, "DIRECCION");
                                    objConsultaBE.iddepartamento = DL_HELPER.getString(objDRConsulta, "IDDEPARTAMENTO");
                                    if (Convert.ToString(objDRConsulta["EXTCORREO"]) == "" || objDRConsulta["EXTCORREO"] is null)
                                    {
                                        objConsultaBE.extCorreo = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.extCorreo = DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    }
                                    

                                    objConsultaBE.activo = DL_HELPER.getInt32(objDRConsulta, "ACTIVO");
                                    objConsultaBE.estado = DL_HELPER.getInt32(objDRConsulta, "ESTADO");

                                    if (Convert.ToString(objDRConsulta["ACTIVO"]) == "1")
                                    {
                                        objConsultaBE.desActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.desActivo = "Inactivo";
                                    }

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new List<BeUnidadEp>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public BeUnidadEp SP_OBTENER_UNIDAD_EP_BY_CODIGO(BeUnidadEp pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UNIDAD_EP.SP_LISTAR_UNIDAD_ENTIDAD_PUBLICA_BY_CODIGO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdUnidadEP", OracleType.Int16, ParameterDirection.Input, pFiltros.idUnidadEP));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        BeUnidadEp objConsultaBE = new BeUnidadEp();
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            
                            List<BeUnidadEp> llstConsultaBE = new List<BeUnidadEp>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BeUnidadEp();

                                    objConsultaBE.idUnidadEP = DL_HELPER.getInt32(objDRConsulta, "IDUNIDADEP");
                                    objConsultaBE.idInsExterna = DL_HELPER.getString(objDRConsulta, "IDINSEXTERNA");
                                    objConsultaBE.idEntidadP = DL_HELPER.getInt32(objDRConsulta, "IDENTPUBLICA");
                                    objConsultaBE.nomUniEntPublica = DL_HELPER.getString(objDRConsulta, "NOMBREUNIDADP");
                                    objConsultaBE.iddepartamento = DL_HELPER.getString(objDRConsulta, "IDDEPARTAMENTO");
                                    objConsultaBE.idprovincia = DL_HELPER.getString(objDRConsulta, "IDPROVINCIA");
                                    objConsultaBE.iddistrito = DL_HELPER.getString(objDRConsulta, "IDDISTRITO");

                                    objConsultaBE.telefono = DL_HELPER.getString(objDRConsulta, "TELEFONO");
                                    objConsultaBE.direccion = DL_HELPER.getString(objDRConsulta, "DIRECCION");
                                    if (Convert.ToString(objDRConsulta["EXTCORREO"]) == "" || objDRConsulta["EXTCORREO"] is null)
                                    {
                                        objConsultaBE.extCorreo = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.extCorreo = DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    }


                                    objConsultaBE.activo = DL_HELPER.getInt32(objDRConsulta, "ACTIVO");
                                    objConsultaBE.estado = DL_HELPER.getInt32(objDRConsulta, "ACTIVO");

                                    if (Convert.ToString(objDRConsulta["ACTIVO"]) == "1")
                                    {
                                        objConsultaBE.desActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.desActivo = "Inactivo";
                                    }

                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new BeUnidadEp();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public String SP_ACTUALIZAR_UNIDAD_ENTIDAD_PUBLICA(BeUnidadEp UnidadEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UNIDAD_EP.SP_ACTUALIZAR_UNIDAD_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdUnidadEP", OracleType.Int16, ParameterDirection.Input, UnidadEP.idUnidadEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_InsExterna", OracleType.VarChar, ParameterDirection.Input, UnidadEP.idInsExterna));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NombreEP", OracleType.VarChar, ParameterDirection.Input, UnidadEP.nomUniEntPublica));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Telefono", OracleType.VarChar, ParameterDirection.Input, UnidadEP.telefono));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Departamento", OracleType.VarChar, ParameterDirection.Input, UnidadEP.iddepartamento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Provincia", OracleType.VarChar, ParameterDirection.Input, UnidadEP.idprovincia));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Distrito", OracleType.VarChar, ParameterDirection.Input, UnidadEP.iddistrito));

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Direccion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.direccion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ExtCorreo", OracleType.VarChar, ParameterDirection.Input, UnidadEP.extCorreo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Activo", OracleType.Int16, ParameterDirection.Input, UnidadEP.activo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuActuali", OracleType.VarChar, ParameterDirection.Input, UnidadEP.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {
                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public List<BeUnidadEp> ListaUnidadEPByEstado( string nEntidadP,int estado)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UNIDAD_EP.SP_LISTAR_UNIDAD_ENTIDAD_BY_ESTADO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_InsExterna", OracleType.VarChar, ParameterDirection.Input, nEntidadP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Int16, ParameterDirection.Input, estado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUnidadEp objConsultaBE;
                            List<BeUnidadEp> llstConsultaBE = new List<BeUnidadEp>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUnidadEp();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idUnidadEP = DL_HELPER.getInt32(objDRConsulta, "IDUNIDADEP");
                                    objConsultaBE.idInsExterna = DL_HELPER.getString(objDRConsulta, "IDINSEXTERNA");
                                    objConsultaBE.nomUniEntPublica = DL_HELPER.getString(objDRConsulta, "NOMBREUNIDADP");

                                    objConsultaBE.telefono = DL_HELPER.getString(objDRConsulta, "TELEFONO");
                                    objConsultaBE.direccion = DL_HELPER.getString(objDRConsulta, "DIRECCION");
                                    if (Convert.ToString(objDRConsulta["EXTCORREO"]) == "" || objDRConsulta["EXTCORREO"] is null)
                                    {
                                        objConsultaBE.extCorreo = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.extCorreo = DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    }


                                    objConsultaBE.activo = DL_HELPER.getInt32(objDRConsulta, "ACTIVO");
                                    objConsultaBE.estado = DL_HELPER.getInt32(objDRConsulta, "ESTADO");

                                    if (Convert.ToString(objDRConsulta["ACTIVO"]) == "1")
                                    {
                                        objConsultaBE.desActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.desActivo = "Inactivo";
                                    }

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BeUnidadEp>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
    }
}
