﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_EMPRESA
    {

        public List<BE_EMPRESA> Listar(BE_EMPRESA pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_EMPRESA";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idEmpresa", OracleType.VarChar, ParameterDirection.Input, pFiltros.idEmpresa));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_EMPRESA objConsultaBE;
                            List<BE_EMPRESA> llstConsultaBE = new List<BE_EMPRESA>();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_EMPRESA();

                                    objConsultaBE.idEmpresa = DL_HELPER.getInt64(objDRConsulta, "IDEMPRESA");
                                    objConsultaBE.empresaOperadora = DL_HELPER.getString(objDRConsulta, "EMPRESAOPERADORA");
                                    
                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            return new List<BE_EMPRESA>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
   
    }
}
