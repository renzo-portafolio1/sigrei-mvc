﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Data.OracleClient;

using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DlUsuReniec
    {
        public List<BeParReniec> ExtrerUsuarioReniec(string usuario, int estado)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_OBTENER_USUARIO_RENIEC";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, usuario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Int16, ParameterDirection.Input, estado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeParReniec objConsultaBE;
                            List<BeParReniec> llstConsultaBE = new List<BeParReniec>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeParReniec();
                                    objConsultaBE.nuDniUsuario = DL_HELPER.getString(objDRConsulta, "DNI");
                                    objConsultaBE.password = DL_HELPER.getString(objDRConsulta, "CONTRASENA");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeParReniec>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
    }
}
