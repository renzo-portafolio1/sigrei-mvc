﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DlParametro
    {

        public List<BE_PARAMETRO> SP_PARAMETRO_GET_DATOS(ref BE_PARAMETRO P_UsuarioEOBE)
        {
            BE_PARAMETRO objResultado;
            List<BE_PARAMETRO> LISTA;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {

                    cmd.CommandText = "PKG_SI_PARAMETRO.SP_LISTAR_PARAMETRO_BY_TIPO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_codigo", OracleType.Int16, ParameterDirection.Input, P_UsuarioEOBE.idParametro));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_tipo", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.tipoParametro));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            LISTA = new List<BE_PARAMETRO>();

                            Int32 numerador = 1;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {

                                    objResultado = new BE_PARAMETRO();
                                    
                                    if (Convert.ToString(objDRConsulta["ACTIVO"]) == "1")
                                    {
                                        objResultado.activo = "Activo";
                                    }
                                    else
                                    {
                                        objResultado.activo = "Inactivo";
                                    }
                                   
                                    LISTA.Add(objResultado);
                                    numerador++;
                                }
                                objDRConsulta.Close();

                            }
                            NewConexion.retClose();
                        }
                        catch (Exception)
                        {
                           
                            LISTA = new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("SP_LISTAR_USUARIO_EO_X_APLIC", "", ex.Message.ToString());
                throw new Exception("Mensaje de Error: ", ex);
            }
            return LISTA;
        }

        public List<BE_PARAMETRO> ListarDisByProvincia(string codProvincia)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UTIL.SP_DISTRITOS_BY_DEPARTAMENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CodProvincia", OracleType.VarChar, ParameterDirection.Input, codProvincia));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.IDDIS = DL_HELPER.getString(objDRConsulta, "IDDIS");
                                    objConsultaBE.DISTRITO = DL_HELPER.getString(objDRConsulta, "DISTRITO");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BE_PARAMETRO> ListarProByDepartamento(string codDepartamento)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UTIL.SP_PROVINCIAS_BY_DEPARTAMENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CodDepartamento", OracleType.VarChar, ParameterDirection.Input, codDepartamento));
                    
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.IDPP = DL_HELPER.getString(objDRConsulta, "IDPP");
                                    objConsultaBE.PROVINCIA = DL_HELPER.getString(objDRConsulta, "PROVINCIA");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BE_PARAMETRO> ValidarDuplicadoParametro(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_PARAMETRO.SP_VALIDAR_IGUALDAD_PARAMETRO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, pFiltros.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Valor", OracleType.VarChar, ParameterDirection.Input, pFiltros.valor));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public String Obtener_Codigo_Parametro_By_Registro(BE_PARAMETRO ParametroBE)
        {
            string resp = string.Empty;

            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_PARAMETRO.SP_OBTENER_CODIGO_PARAMETRO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, ParametroBE.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, ParametroBE.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, ParametroBE.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    resp = DL_HELPER.getString(objDRConsulta, "VALOR");

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                          
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            return error;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        public String SP_INSERTAR_PARAMETRO(BE_PARAMETRO ParametroEP)
         {
             string resp = string.Empty;
             int resp_operacion = 0;
             try
             {
                 using (OracleCommand cmd = new OracleCommand())
                 {
                     cmd.CommandText = "ACCESO.PKG_PARAMETRO.SP_INSERTAR_PARAMETRO_GENERAL";
                     cmd.CommandType = System.Data.CommandType.StoredProcedure;

                     cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.Int16, ParameterDirection.Input, ParametroEP.aplicacion));
                     cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, ParametroEP.tabla));
                     cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, ParametroEP.columna));
                     cmd.Parameters.Add(DL_HELPER.getParam("pi_Valor", OracleType.VarChar, ParameterDirection.Input, ParametroEP.valor));

                     cmd.Parameters.Add(DL_HELPER.getParam("pi_Abreviado", OracleType.VarChar, ParameterDirection.Input, ParametroEP.abreviado));
                     cmd.Parameters.Add(DL_HELPER.getParam("pi_Descripcion", OracleType.VarChar, ParameterDirection.Input, ParametroEP.descripcion));
                     cmd.Parameters.Add(DL_HELPER.getParam("pi_Activo", OracleType.VarChar, ParameterDirection.Input, ParametroEP.activo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, ParametroEP.usuCre));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                     {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }
                       
                     }
                 }
             }
             catch (Exception ex)
             {
                throw new Exception("Mensaje de Error: ", ex);
            }
             return resp;

         }
        public List<BE_PARAMETRO> ListarModuloByPerfil(string nPerfil, int nEstado)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_LISTAR_MODULO_BY_PERFIL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Perfil", OracleType.VarChar, ParameterDirection.Input, nPerfil));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BE_PARAMETRO> ListarEntidadPByUsuario(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_LISTAR_ENTIDADP_BY_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, pFiltros.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BE_PARAMETRO> ListarModuloByUsuario(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_MODULO.SP_LISTAR_MODULO_BY_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, pFiltros.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        
        public List<BE_PARAMETRO> ListarValoresTipDocumento(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_PARAMETRO.SP_LISTAR_PARAMETRO_X_COLUMNA";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("sAplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("sTabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("sColumna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("oListado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ëx)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BE_PARAMETRO> ListarValoresContrasenia(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_GLOBAL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Global", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            //using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            int ret = cmd.ExecuteNonQuery();
                            using (OracleDataReader objDRConsulta = (OracleDataReader)cmd.Parameters["po_Listado"].Value)
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");
                                    objConsultaBE.activo = DL_HELPER.getString(objDRConsulta, "ESTADO");
                                    objConsultaBE.orden = DL_HELPER.getInt32(objDRConsulta, "VALOR");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BE_PARAMETRO> ListarValores_sexo(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_PARAMETRO.SP_LISTAR_PARAMETRO_X_COLUMNA";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("sAplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("sTabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("sColumna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("oListado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BE_PARAMETRO> ListarValores(BE_PARAMETRO pFiltros)
        {
            try
            {
                BE_PARAMETRO objConsultaBE = new BE_PARAMETRO();
                List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_PARAMETRO.SP_LISTAR_PARAMETRO_X_COLUMNA";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("saplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("stabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("scolumna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("sEstado", OracleType.VarChar, ParameterDirection.Input,"A"));
                    cmd.Parameters.Add(DL_HELPER.getParam("olistado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta,"VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta,"DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta,"ABREVIADO");

                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BE_PARAMETRO> ListarValores_By_Todo_Global(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_GLOBAL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Global", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");

                                    objConsultaBE.ESTADO = DL_HELPER.getString(objDRConsulta, "ESTADO");


                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BE_PARAMETRO> ListarValores_By_Todo(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_LISTAR_PARAMETRO_ENTIDAD_PUBLICA_BY_INS_EXTERNA";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta, "VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");

                                    objConsultaBE.ESTADO = DL_HELPER.getString(objDRConsulta, "ESTADO");

                                    objConsultaBE.DEPENDENCIA = DL_HELPER.getInt32(objDRConsulta, "DEPENDENCIA");

                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }


        public BE_PARAMETRO SP_OBTENER_PARAMETRO_BY_CODIGO(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_PARAMETRO.SP_OBTENER_PARAMETRO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("sAplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("sTabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("sColumna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("sValor", OracleType.VarChar, ParameterDirection.Input, pFiltros.valor));

                    cmd.Parameters.Add(DL_HELPER.getParam("oListado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        BE_PARAMETRO objConsultaBE = new BE_PARAMETRO();
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                           
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = pFiltros.valor;
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta, "DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta, "ABREVIADO");

                                    objConsultaBE.ESTADO = DL_HELPER.getString(objDRConsulta, "ACTIVO");

                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception)
                        {
                           
                            return new BE_PARAMETRO();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public String SP_ACTUALIZAR_PARAMETRO(BE_PARAMETRO ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_PARAMETRO.SP_ACTUALIZAR_PARAMETRO_GENERAL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.Int16, ParameterDirection.Input, ParametroEP.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, ParametroEP.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, ParametroEP.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Valor", OracleType.VarChar, ParameterDirection.Input, ParametroEP.valor));

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Abreviado", OracleType.VarChar, ParameterDirection.Input, ParametroEP.abreviado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Descripcion", OracleType.VarChar, ParameterDirection.Input, ParametroEP.descripcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Activo", OracleType.VarChar, ParameterDirection.Input, ParametroEP.activo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, ParametroEP.usuMod));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }
    }
}
