﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_PARAMETRO
    {

        public List<BE_PARAMETRO> ListarValores(BE_PARAMETRO pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_PARAMETRO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, pFiltros.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, pFiltros.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, pFiltros.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETRO objConsultaBE;
                            List<BE_PARAMETRO> llstConsultaBE = new List<BE_PARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETRO();

                                    objConsultaBE.valor = DL_HELPER.getString(objDRConsulta,"VALOR");
                                    objConsultaBE.descripcion = DL_HELPER.getString(objDRConsulta,"DESCRIPCION");
                                    objConsultaBE.abreviado = DL_HELPER.getString(objDRConsulta,"ABREVIADO");
                                    
                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            return new List<BE_PARAMETRO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

   
    }
}
