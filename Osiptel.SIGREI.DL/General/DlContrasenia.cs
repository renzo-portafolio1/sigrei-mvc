﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DlContrasenia
    {
        public String SP_ACTUALIZAR_CONTRASENIA_UEP(BeContrasenia ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_ACTUALIZAR_CONTRASENIA_UEP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, ParametroEP.USUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, ParametroEP.NUEVA_CONTRASENIA));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }
    }
}
