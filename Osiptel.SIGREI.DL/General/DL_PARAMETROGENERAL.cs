﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using System.Data.OracleClient;
using Osiptel.SIGREI.BE;
using System.Collections;
using System.Web;

namespace Osiptel.SIGREI.DL
{
    public class DL_PARAMETROGENERAL
    {

        #region "No Transaccionales"

        public List<BE_PARAMETROGENERAL> listaParametro(BE_PARAMETROGENERAL objObjeto)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_PARAMETRO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, objObjeto.aplicacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tabla", OracleType.VarChar, ParameterDirection.Input, objObjeto.tabla));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Columna", OracleType.VarChar, ParameterDirection.Input, objObjeto.columna));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_PARAMETROGENERAL objConsultaBE;
                            List<BE_PARAMETROGENERAL> llstConsultaBE = new List<BE_PARAMETROGENERAL>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_PARAMETROGENERAL();

                                    objConsultaBE.idTipo = Convert.ToString(objDRConsulta["VALOR"]);
                                    objConsultaBE.descripcion = Convert.ToString(objDRConsulta["DESCRIPCION"]);
                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            List<BE_PARAMETROGENERAL> llstConsultaBE = new List<BE_PARAMETROGENERAL>();
                            return llstConsultaBE;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<BE_VARIABLEPARAMETRO> obtenerValoresParametroDocumento(BE_REQUERIMIENTO_IMEI objObjeto)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_OBTENER_VALORES_PARAMETRO";
                    cmd.CommandType = CommandType.StoredProcedure;


                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDRequerimiento", OracleType.Number, ParameterDirection.Input, objObjeto.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImei", OracleType.Number, ParameterDirection.Input, objObjeto.idRequerimientoImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_VARIABLEPARAMETRO objResultado;
                            List<BE_VARIABLEPARAMETRO> listaResultado = new List<BE_VARIABLEPARAMETRO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objResultado = new BE_VARIABLEPARAMETRO();

                                    objResultado.variable = Convert.ToString(objDRConsulta["PARAMETRO"]);
                                    objResultado.valorVariable = objDRConsulta["VALOR"] == null ? "" : Convert.ToString(objDRConsulta["VALOR"]);
                                    listaResultado.Add(objResultado);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            List<BE_VARIABLEPARAMETRO> listaResultado = new List<BE_VARIABLEPARAMETRO>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public BE_VARIABLEPARAMETRO valoresNotificarSigreiMovil(BE_VARIABLEPARAMETRO objObjeto)
        {
            try
            {
                BE_VARIABLEPARAMETRO objResultado = new BE_VARIABLEPARAMETRO();

                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_DATOS_NOTIF_SIGREI_MOVIL";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdRequerimientoMovil", OracleType.Number, ParameterDirection.Input, objObjeto.idRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdAdjunto", OracleType.Number, ParameterDirection.Input, objObjeto.variable));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();


                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                if (objDRConsulta.Read())
                                {
                                    objResultado = new BE_VARIABLEPARAMETRO();

                                    objResultado.hostname = objDRConsulta["HOSTNAME"] == null ? "" : Convert.ToString(objDRConsulta["HOSTNAME"]);
                                    objResultado.puerto = objDRConsulta["PUERTO"] == null ? "" : Convert.ToString(objDRConsulta["PUERTO"]);
                                    objResultado.deParte = objDRConsulta["DE_PARTE"] == null ? "" : Convert.ToString(objDRConsulta["DE_PARTE"]);
                                    objResultado.claveDeparte = objDRConsulta["CLAVE_PARTE"] == null ? "" : Convert.ToString(objDRConsulta["CLAVE_PARTE"]);
                                    objResultado.para = objDRConsulta["PARA"] == null ? "" : Convert.ToString(objDRConsulta["PARA"]);
                                    objResultado.conCopia = objDRConsulta["CON_COPIA"] == null ? "" : Convert.ToString(objDRConsulta["CON_COPIA"]);
                                    objResultado.conCopiaOculta = objDRConsulta["CON_COPIA_OCULTA"] == null ? "" : Convert.ToString(objDRConsulta["CON_COPIA_OCULTA"]);
                                    objResultado.asunto = objDRConsulta["ASUNTO"] == null ? "" : Convert.ToString(objDRConsulta["ASUNTO"]);
                                    objResultado.mensaje = objDRConsulta["MENSAJE"] == null ? "" : Convert.ToString(objDRConsulta["MENSAJE"]);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();

                            return objResultado;
                        }
                        catch (Exception ex)
                        {
                            objResultado = new BE_VARIABLEPARAMETRO();
                            return objResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public BE_VARIABLEPARAMETRO valoresNotificarSigrei(BE_VARIABLEPARAMETRO objObjeto)
        {
            try
            {
                BE_VARIABLEPARAMETRO objResultado = new BE_VARIABLEPARAMETRO();

                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_DATOS_NOTIF_SIGREI";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdRequerimientoSol", OracleType.Number, ParameterDirection.Input, objObjeto.idRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                          
                            
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                if (objDRConsulta.Read())
                                {
                                    objResultado = new BE_VARIABLEPARAMETRO();
                                    
                                    objResultado.hostname = objDRConsulta["HOSTNAME"] == null ? "" : Convert.ToString(objDRConsulta["HOSTNAME"]);
                                    objResultado.puerto = objDRConsulta["PUERTO"] == null ? "" : Convert.ToString(objDRConsulta["PUERTO"]);
                                    objResultado.deParte = objDRConsulta["DE_PARTE"] == null ? "" : Convert.ToString(objDRConsulta["DE_PARTE"]);
                                    objResultado.claveDeparte = objDRConsulta["CLAVE_PARTE"] == null ? "" : Convert.ToString(objDRConsulta["CLAVE_PARTE"]);
                                    objResultado.para = objDRConsulta["PARA"] == null ? "" : Convert.ToString(objDRConsulta["PARA"]);
                                    objResultado.conCopia = objDRConsulta["CON_COPIA"] == null ? "" : Convert.ToString(objDRConsulta["CON_COPIA"]);
                                    objResultado.conCopiaOculta = objDRConsulta["CON_COPIA_OCULTA"] == null ? "" : Convert.ToString(objDRConsulta["CON_COPIA_OCULTA"]);
                                    objResultado.asunto = objDRConsulta["ASUNTO"] == null ? "" : Convert.ToString(objDRConsulta["ASUNTO"]);
                                    objResultado.mensaje = objDRConsulta["MENSAJE"] == null ? "" : Convert.ToString(objDRConsulta["MENSAJE"]);                                                                       
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();

                            return objResultado;
                        }
                        catch (Exception ex)
                        {
                            objResultado = new BE_VARIABLEPARAMETRO();
                            return objResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region "Transaccionales"

        #endregion

    }
}
