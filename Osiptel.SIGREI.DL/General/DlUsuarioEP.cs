﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Data.OracleClient;

using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DlUsuarioEP
    {

        public BeUsuarioEP SP_VALIDAR_USUARIO_EP_BY_DNI(BeUsuarioEP pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_CONSULTAR_USUARIO_BY_NUMDOC";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDoc", OracleType.VarChar, ParameterDirection.Input, pFiltros.NUM_DOCIDE));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE = new BeUsuarioEP();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;

                                   
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.IND_ESTADO = DL_HELPER.getString(objDRConsulta, "IND_ESTADO");
                                   

                                    if (Convert.ToString(objDRConsulta["DES_CORELE"]) == "" || objDRConsulta["DES_CORELE"] is null)
                                    {
                                        objConsultaBE.DES_CORELE = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "DES_CORELE");
                                    }


                                    if (Convert.ToString(objDRConsulta["IND_ESTADO"]) == "1")
                                    {
                                        objConsultaBE.desActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.desActivo = "Inactivo";
                                    }

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new BeUsuarioEP();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BeUsuarioEP> SP_REGISTRAR_MASIVO_USUARIO_EP(BeUsuarioEP ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_REGISTRAR_MASIVO_USUARIO_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDPERFIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdModulo", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDMODULO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumRuc", OracleType.VarChar, ParameterDirection.Input, ParametroEP.NUM_RUC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UniEP", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDUNIDADEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, ParametroEP.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, ParametroEP.CONTRASENIA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ContraEncrip", OracleType.VarChar, ParameterDirection.Input, ParametroEP.CONTRASENIA_ENCR));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, ParametroEP.APLICACION));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_MsMail", OracleType.Clob));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            String po_MsMail = "";
                            //int ret = NewConexion.ejecutaSQL(cmd);
                            int ret = cmd.ExecuteNonQuery();
                            OracleLob oracleLob = (OracleLob)cmd.Parameters["po_MsMail"].Value;
                            po_MsMail = (String)oracleLob.Value;
                            string[] stringSeparators = new string[] { "(-*)" };
                            string[] arrString = po_MsMail.Split(stringSeparators,StringSplitOptions.None);

                            //using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            using (OracleDataReader objDRConsulta = (OracleDataReader)cmd.Parameters["po_Listado"].Value)
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.DES_TDOC = DL_HELPER.getString(objDRConsulta, "DES_TDOC");
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");

                                    objConsultaBE.HOSTNAME = DL_HELPER.getString(objDRConsulta, "HOSTNAME");
                                    objConsultaBE.PUERTO = DL_HELPER.getString(objDRConsulta, "PUERTO");
                                    objConsultaBE.DE_PARTE = DL_HELPER.getString(objDRConsulta, "DE_PARTE");
                                    objConsultaBE.CLAVE_PARTE = DL_HELPER.getString(objDRConsulta, "CLAVE_PARTE");
                                    objConsultaBE.PARA = DL_HELPER.getString(objDRConsulta, "PARA");
                                    objConsultaBE.ASUNTO = DL_HELPER.getString(objDRConsulta, "ASUNTO");

                                    for (var i =0;i<arrString.Length -1;i++)
                                    {
                                        string[] stringSeparators2 = new string[] { "(*-*)" };
                                        string[] arrString2 = arrString[i].Split(stringSeparators2, StringSplitOptions.None);

                                        if (arrString2[0].Trim() == DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE"))
                                        {
                                            objConsultaBE.MENSAJE = arrString2[1].Trim();
                                        }
                                    }

                                    //objConsultaBE.MENSAJE = DL_HELPER.getString(objDRConsulta, "MENSAJE");
                                    if (Convert.ToString(objDRConsulta["COPIA"]) == "" || objDRConsulta["COPIA"] is null)
                                    {
                                        objConsultaBE.COPIA = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.COPIA = DL_HELPER.getString(objDRConsulta, "COPIA");
                                    }
                                    

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }
        /*public String SP_REGISTRAR_MASIVO_USUARIO_EP(BeUsuarioEP ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_REGISTRAR_MASIVO_USUARIO_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdPerfil", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDPERFIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdModulo", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDMODULO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumRuc", OracleType.VarChar, ParameterDirection.Input, ParametroEP.NUM_RUC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UniEP", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDUNIDADEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, ParametroEP.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, ParametroEP.CONTRASENIA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ContraEncrip", OracleType.VarChar, ParameterDirection.Input, ParametroEP.CONTRASENIA_ENCR));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }*/
        public List<BeUsuarioEP> ActualizarDatosTemporalUser(string dni, string DES_APEMAT, string DES_NOMBRE, string DES_NOmBRE_LAST,
            string DES_TLFFIJ, string COD_DOCIDE, string DES_TLFMVL, string NUM_DOCIDE, string DIRECCION, string DES_APEPAT, string DES_CORELE, string IND_SEXO,string CARGO)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_ACTUALIZAR_TMP_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDoc", OracleType.VarChar, ParameterDirection.Input, dni));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_DesApeMat", OracleType.VarChar, ParameterDirection.Input, DES_APEMAT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Des_Nombre", OracleType.VarChar, ParameterDirection.Input, DES_NOMBRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Des_NombreSec", OracleType.VarChar, ParameterDirection.Input, DES_NOmBRE_LAST));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TlfFijo", OracleType.VarChar, ParameterDirection.Input, DES_TLFFIJ));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipDoc", OracleType.VarChar, ParameterDirection.Input, COD_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TlfMov", OracleType.VarChar, ParameterDirection.Input, DES_TLFMVL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDocG", OracleType.VarChar, ParameterDirection.Input, NUM_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Direccion", OracleType.VarChar, ParameterDirection.Input, DIRECCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_DesApePat", OracleType.VarChar, ParameterDirection.Input, DES_APEPAT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Correo", OracleType.VarChar, ParameterDirection.Input, DES_CORELE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipSexo", OracleType.VarChar, ParameterDirection.Input, IND_SEXO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Cargo", OracleType.VarChar, ParameterDirection.Input, CARGO));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.COD_DOCIDE = DL_HELPER.getString(objDRConsulta, "COD_DOCIDE");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_TLFFIJ = DL_HELPER.getString(objDRConsulta, "DES_TLFFIJ");
                                    objConsultaBE.DES_TLFMVL = DL_HELPER.getString(objDRConsulta, "DES_TLFMVL");
                                    objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "DES_CORELE");
                                    objConsultaBE.IND_SEXO = DL_HELPER.getString(objDRConsulta, "IND_SEXO");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    //objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST");
                                    if (Convert.ToString(objDRConsulta["DES_NOMBRE_LAST"]) == "" || objDRConsulta["DES_NOMBRE_LAST"] is null)
                                    {
                                        objConsultaBE.DES_NOmBRE_LAST = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST").ToUpper();
                                    }
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_APEMAT = DL_HELPER.getString(objDRConsulta, "DES_APEMAT");
                                    objConsultaBE.DIRECCION = DL_HELPER.getString(objDRConsulta, "DIRECCION");
                                    objConsultaBE.CARGO = DL_HELPER.getString(objDRConsulta, "CARGO");


                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BeUsuarioEP> EliminarDatosTemporalUser(string dni)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_ELIMINAR_TMP_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDoc", OracleType.VarChar, ParameterDirection.Input, dni));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.COD_DOCIDE = DL_HELPER.getString(objDRConsulta, "COD_DOCIDE");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_TLFFIJ = DL_HELPER.getString(objDRConsulta, "DES_TLFFIJ");
                                    objConsultaBE.DES_TLFMVL = DL_HELPER.getString(objDRConsulta, "DES_TLFMVL");
                                    objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "DES_CORELE");
                                    objConsultaBE.IND_SEXO = DL_HELPER.getString(objDRConsulta, "IND_SEXO");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    //objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST");
                                    if (Convert.ToString(objDRConsulta["DES_NOMBRE_LAST"]) == "" || objDRConsulta["DES_NOMBRE_LAST"] is null)
                                    {
                                        objConsultaBE.DES_NOmBRE_LAST = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST").ToUpper();
                                    }
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_APEMAT = DL_HELPER.getString(objDRConsulta, "DES_APEMAT");
                                    objConsultaBE.DIRECCION = DL_HELPER.getString(objDRConsulta, "DIRECCION");


                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public BeUsuarioEP SP_OBTENER_USUARIO_EP_TEMPORAL(BeUsuarioEP pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_OBTENER_TMP_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDoc", OracleType.VarChar, ParameterDirection.Input, pFiltros.NUM_DOCIDE));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE = new BeUsuarioEP();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;

                                    objConsultaBE.COD_DOCIDE = DL_HELPER.getString(objDRConsulta, "COD_DOCIDE");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_TLFFIJ = DL_HELPER.getString(objDRConsulta, "DES_TLFFIJ");
                                    objConsultaBE.DES_TLFMVL = DL_HELPER.getString(objDRConsulta, "DES_TLFMVL");
                                    objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "DES_CORELE");
                                    objConsultaBE.IND_SEXO = DL_HELPER.getString(objDRConsulta, "IND_SEXO");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE").ToUpper();
                                    if (Convert.ToString(objDRConsulta["DES_NOMBRE_LAST"]) == "" || objDRConsulta["DES_NOMBRE_LAST"] is null)
                                    {
                                        objConsultaBE.DES_NOmBRE_LAST = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST").ToUpper();
                                    }
                                    //objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST").ToUpper();
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT").ToUpper();
                                    objConsultaBE.DES_APEMAT = DL_HELPER.getString(objDRConsulta, "DES_APEMAT").ToUpper();
                                    objConsultaBE.DIRECCION = DL_HELPER.getString(objDRConsulta, "DIRECCION").ToUpper();
                                    objConsultaBE.CARGO = DL_HELPER.getString(objDRConsulta, "CARGO").ToUpper();

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new BeUsuarioEP();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public String InsertarDatosTemporalUser(string cadena_tip_doc, string cadena_num_doc, string cadena_num_tlf, string cadena_num_cel, string cadena_tip_sexo, string cadena_correo,
                                                string cadena_prim_nombre, string cadena_sec_nombre, string cadena_pri_apellido, string cadena_sec_apellido,string cadena_sec_direccion,
                                                string cadena_cargo,string cadena_contrasenia,string cadena_contrasenia_encry)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_REGISTRO_TEMP_USUARIOS";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenTipoDoc", OracleType.VarChar, ParameterDirection.Input, cadena_tip_doc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenNumDoc", OracleType.VarChar, ParameterDirection.Input, cadena_num_doc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenNumTlf", OracleType.VarChar, ParameterDirection.Input, cadena_num_tlf));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenNumCel", OracleType.VarChar, ParameterDirection.Input, cadena_num_cel));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenTipSexo", OracleType.VarChar, ParameterDirection.Input, cadena_tip_sexo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenCorreo", OracleType.VarChar, ParameterDirection.Input, cadena_correo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenPriNombre", OracleType.VarChar, ParameterDirection.Input, cadena_prim_nombre));
                   
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenSecNombre", OracleType.VarChar, ParameterDirection.Input, cadena_sec_nombre));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenApePat", OracleType.VarChar, ParameterDirection.Input, cadena_pri_apellido));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenApeMat", OracleType.VarChar, ParameterDirection.Input, cadena_sec_apellido));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenDireccion", OracleType.VarChar, ParameterDirection.Input, cadena_sec_direccion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenCargo", OracleType.VarChar, ParameterDirection.Input, cadena_cargo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenContrasenia", OracleType.VarChar, ParameterDirection.Input, cadena_contrasenia));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_GenContraseniaEncr", OracleType.VarChar, ParameterDirection.Input, cadena_contrasenia_encry));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Int16, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.VarChar, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }



        public BeUsuarioEP SP_OBTENER_USUARIO_EP(BeUsuarioEP pFiltros)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_CONSULTAR_USUARIO_BY_CODIGO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdUsuario", OracleType.VarChar, ParameterDirection.Input, pFiltros.IDUSUARIO));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE = new BeUsuarioEP();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;

                                    objConsultaBE.IDPERFIL = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.IDMODULO = DL_HELPER.getInt32(objDRConsulta, "IDMODULO");
                                    objConsultaBE.IDENTPUBLICA = DL_HELPER.getInt32(objDRConsulta, "IDENTPUBLICA");
                                    objConsultaBE.IDUNIDADEP = DL_HELPER.getInt32(objDRConsulta, "IDUNIDADEP");
                                    objConsultaBE.IND_SEXO = DL_HELPER.getString(objDRConsulta, "IND_SEXO");
                                    objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    objConsultaBE.DES_APEMAT = DL_HELPER.getString(objDRConsulta, "DES_APEMAT").ToUpper();
                                    objConsultaBE.COD_DOCIDE = DL_HELPER.getString(objDRConsulta, "COD_DOCIDE");

                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE").ToUpper();
                                    //objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST").ToUpper();
                                    if (Convert.ToString(objDRConsulta["DES_NOMBRE_LAST"]) == "" || objDRConsulta["DES_NOMBRE_LAST"] is null)
                                    {
                                        objConsultaBE.DES_NOmBRE_LAST = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST").ToUpper();
                                    }
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT").ToUpper();
                                    objConsultaBE.DES_TDOC = DL_HELPER.getString(objDRConsulta, "DES_TDOC");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.nomEntPublica = DL_HELPER.getString(objDRConsulta, "IDUNIDADEP") + "//" + "@" + DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    objConsultaBE.EXTCORREO = DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    objConsultaBE.CORREO_COMPL = DL_HELPER.getString(objDRConsulta, "DES_CORELE") +  "@" + DL_HELPER.getString(objDRConsulta, "EXTCORREO");

                                    objConsultaBE.DES_TLFFIJ = DL_HELPER.getString(objDRConsulta, "DES_TLFFIJ");
                                    objConsultaBE.DES_TLFMVL = DL_HELPER.getString(objDRConsulta, "DES_TLFMVL");
                                    objConsultaBE.DIRECCION = DL_HELPER.getString(objDRConsulta, "DIRECCION").ToUpper();
                                    objConsultaBE.DES_MODU = DL_HELPER.getString(objDRConsulta, "DES_MODU");
                                    objConsultaBE.DES_PERFIL = DL_HELPER.getString(objDRConsulta, "DES_PERFIL");
                                    objConsultaBE.DES_ENTPUB = DL_HELPER.getString(objDRConsulta, "DES_ENTPUB");
                                    objConsultaBE.NOMBREUNIDADP = DL_HELPER.getString(objDRConsulta, "NOMBREUNIDADP");
                                    objConsultaBE.NUM_RUC =  DL_HELPER.getString(objDRConsulta, "NUM_RUC");
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.IND_ESTADO = DL_HELPER.getString(objDRConsulta, "IND_ESTADO");
                                    objConsultaBE.FECINI = DL_HELPER.getString(objDRConsulta, "FECINI");
                                    objConsultaBE.FECEXPCONTRASENIA = DL_HELPER.getString(objDRConsulta, "FECEXPCONTRASENIA");
                                    objConsultaBE.FECCRE = DL_HELPER.getString(objDRConsulta, "FECCRE");
                                    objConsultaBE.USUMOD = DL_HELPER.getString(objDRConsulta, "USUMOD");
                                    objConsultaBE.CARGO = DL_HELPER.getString(objDRConsulta, "CARGO");
                                    //objConsultaBE.FECMOD = DL_HELPER.getString(objDRConsulta, "FECMOD");
                                    objConsultaBE.DES_PUESTO = "";
                                    objConsultaBE.DES_UUOO = "";
                                    objConsultaBE.Lista_Tip_Dato = "";
                                    objConsultaBE.Lista_Tip_Carga = "";
                                    objConsultaBE.MOTIVO = "";
                                    objConsultaBE.NRODOCREF = "";
                                    objConsultaBE.DELITO = "";
                                    objConsultaBE.IDUBIGEO = DL_HELPER.getString(objDRConsulta, "IDUBIGEO");
                                    objConsultaBE.UBIGEO = DL_HELPER.getString(objDRConsulta,"UBIGEO");
                                    objConsultaBE.IDUBIGEO = DL_HELPER.getString(objDRConsulta,"IDUBIGEO");
                                    if (Convert.ToString(objDRConsulta["DES_CORELE"]) == "" || objDRConsulta["DES_CORELE"] is null)
                                    {
                                        objConsultaBE.DES_CORELE = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "DES_CORELE");
                                    }


                                    if (Convert.ToString(objDRConsulta["IND_ESTADO"]) == "1")
                                    {
                                        objConsultaBE.desActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.desActivo = "Inactivo";
                                    }

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new BeUsuarioEP();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                   
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public String SP_ACTUALIZAR_VIGENCIA_USUARIO(BeUsuarioEP ParametroEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_ACTUALIZAR_VIGENCIA_USUARIO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdUsuario", OracleType.Int16, ParameterDirection.Input, ParametroEP.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecInicio", OracleType.DateTime, ParameterDirection.Input, ParametroEP.FECINI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecFin", OracleType.DateTime, ParameterDirection.Input, ParametroEP.FECEXPCONTRASENIA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, ParametroEP.USUMOD));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            resp = "1";
                        }
                        catch (Exception exp)
                        {

                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            return resp;

        }

        //public String SP_INSERTAR_USUARIO_ENTIDAD_PUBLICA(BeUsuarioEP UnidadEP)
        public List<BeUsuarioEP> SP_INSERTAR_USUARIO_ENTIDAD_PUBLICA(BeUsuarioEP UnidadEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_INSERTAR_USUARIO_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDPerfil", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDPERFIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApeMaterno", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_APEMAT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDModulo", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDMODULO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_NOMBRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NombreSec", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_NOmBRE_LAST));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUEntPublica", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDUNIDADEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Telefono", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_TLFFIJ));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDTipDocumento", OracleType.VarChar, ParameterDirection.Input, UnidadEP.COD_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Celular", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_TLFMVL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDocumento", OracleType.VarChar, ParameterDirection.Input, UnidadEP.NUM_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Direccion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DIRECCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApePaterno", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_APEPAT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CorElectronico", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_CORELE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Sexo", OracleType.VarChar, ParameterDirection.Input, UnidadEP.IND_SEXO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Ruc", OracleType.VarChar, ParameterDirection.Input, UnidadEP.NUM_RUC ));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCreacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, UnidadEP.CONTRASENIA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ContraEncrip", OracleType.VarChar, ParameterDirection.Input, UnidadEP.CONTRASENIA_ENCR));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Cargo", OracleType.VarChar, ParameterDirection.Input, UnidadEP.CARGO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.APLICACION));
                    /*cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idusuario", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, 0));*/
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_MsMail", OracleType.Clob));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            String po_MsMail = "";
                            //int ret = NewConexion.ejecutaSQL(cmd);
                            int ret = cmd.ExecuteNonQuery();
                            OracleLob oracleLob = (OracleLob)cmd.Parameters["po_MsMail"].Value;
                            po_MsMail = (String)oracleLob.Value;

                            using (OracleDataReader objDRConsulta = (OracleDataReader)cmd.Parameters["po_Listado"].Value)
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BeUsuarioEP();
                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.DES_TDOC = DL_HELPER.getString(objDRConsulta, "DES_TDOC");
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");

                                    objConsultaBE.HOSTNAME = DL_HELPER.getString(objDRConsulta, "HOSTNAME");
                                    objConsultaBE.PUERTO = DL_HELPER.getString(objDRConsulta, "PUERTO");
                                    objConsultaBE.DE_PARTE = DL_HELPER.getString(objDRConsulta, "DE_PARTE");
                                    objConsultaBE.CLAVE_PARTE = DL_HELPER.getString(objDRConsulta, "CLAVE_PARTE");
                                    objConsultaBE.PARA = DL_HELPER.getString(objDRConsulta, "PARA");
                                    objConsultaBE.ASUNTO = DL_HELPER.getString(objDRConsulta, "ASUNTO");
                                    //objConsultaBE.MENSAJE = DL_HELPER.getString(objDRConsulta, "MENSAJE");
                                    objConsultaBE.MENSAJE = po_MsMail;
                                    if (Convert.ToString(objDRConsulta["COPIA"]) == "" || objDRConsulta["COPIA"] is null)
                                    {
                                        objConsultaBE.COPIA = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.COPIA = DL_HELPER.getString(objDRConsulta, "COPIA");
                                    }

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }


        public List<BeUsuarioEP> SP_ACTUALIZAR_USUARIO_ENTIDAD_PUBLICA(BeUsuarioEP UnidadEP)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_ACTUALIZAR_USUARIO_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDPerfil", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDPERFIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApeMaterno", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_APEMAT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDModulo", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDMODULO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_NOMBRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NombreSec", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_NOmBRE_LAST));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUEntPublica", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDUNIDADEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Telefono", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_TLFFIJ));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDTipDocumento", OracleType.VarChar, ParameterDirection.Input, UnidadEP.COD_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Celular", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_TLFMVL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDocumento", OracleType.VarChar, ParameterDirection.Input, UnidadEP.NUM_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Direccion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DIRECCION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApePaterno", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_APEPAT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CorElectronico", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_CORELE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Sexo", OracleType.VarChar, ParameterDirection.Input, UnidadEP.IND_SEXO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Ruc", OracleType.VarChar, ParameterDirection.Input, UnidadEP.NUM_RUC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, UnidadEP.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.VarChar, ParameterDirection.Input, UnidadEP.IND_ESTADO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdUsuario", OracleType.VarChar, ParameterDirection.Input, UnidadEP.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Cargo", OracleType.VarChar, ParameterDirection.Input, UnidadEP.CARGO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, UnidadEP.APLICACION));
                    /*cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idusuario", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, 0));*/
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));


                    /*using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            resp_operacion += NewConexion.ejecutaSQL(cmd);
                            String pvalor = Convert.ToString(cmd.Parameters["po_idusuario"].Value);
                            resp = pvalor;
                            //resp = "1";
                        }
                        catch (Exception exp)
                        {
                            throw new Exception("Mensaje de Error: ", exp);

                        }

                    }*/
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.DES_TDOC = DL_HELPER.getString(objDRConsulta, "DES_TDOC");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");


                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }
        

        public List<BeUsuarioEP> ListarUsuariosEP(BeUsuarioEP UnidadEP)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_LISTAR_USUARIOS_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDPerfil", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDPERFIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApeMaterno", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_APEMAT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDModulo", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDMODULO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, UnidadEP.DES_NOMBRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUEntPublica", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDUNIDADEP));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDTipDocumento", OracleType.VarChar, ParameterDirection.Input, UnidadEP.COD_DOCIDE));
                   
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDocumento", OracleType.VarChar, ParameterDirection.Input, UnidadEP.NUM_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEntPublica", OracleType.Int16, ParameterDirection.Input, UnidadEP.IDENTPUBLICA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_InstExterna", OracleType.VarChar, ParameterDirection.Input, UnidadEP.INS_EXT));


                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_TDOC = DL_HELPER.getString(objDRConsulta, "DES_TDOC");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");

                                    objConsultaBE.DES_TLFFIJ = DL_HELPER.getString(objDRConsulta, "DES_TLFFIJ");
                                    objConsultaBE.DES_TLFMVL = DL_HELPER.getString(objDRConsulta, "DES_TLFMVL");
                                    objConsultaBE.DIRECCION = DL_HELPER.getString(objDRConsulta, "DIRECCION");
                                    objConsultaBE.DES_MODU = DL_HELPER.getString(objDRConsulta, "DES_MODU");
                                    objConsultaBE.DES_PERFIL = DL_HELPER.getString(objDRConsulta, "DES_PERFIL");
                                    objConsultaBE.DES_ENTPUB = DL_HELPER.getString(objDRConsulta, "DES_ENTPUB");
                                    objConsultaBE.NOMBREUNIDADP = DL_HELPER.getString(objDRConsulta, "NOMBREUNIDADP");
                                    objConsultaBE.NUM_RUC = DL_HELPER.getString(objDRConsulta, "NUM_RUC");
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.IND_ESTADO = DL_HELPER.getString(objDRConsulta, "IND_ESTADO");
                                    objConsultaBE.FECINI = DL_HELPER.getString(objDRConsulta, "FECINI");
                                    objConsultaBE.FECEXPCONTRASENIA = DL_HELPER.getString(objDRConsulta, "FECEXPCONTRASENIA");
                                    objConsultaBE.FECCRE = DL_HELPER.getString(objDRConsulta, "FECCRE");
                                    objConsultaBE.USUMOD = DL_HELPER.getString(objDRConsulta, "USUMOD");
                                    objConsultaBE.USUCRE = DL_HELPER.getString(objDRConsulta, "USUCREACION");
                                    objConsultaBE.IDMODULO = DL_HELPER.getInt32(objDRConsulta, "IDMODULO");
                                    objConsultaBE.IDPERFIL = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.INS_EXT = DL_HELPER.getString(objDRConsulta, "DES_RAZ_SOC");
                                    objConsultaBE.CARGO = DL_HELPER.getString(objDRConsulta, "CARGO");
                                    //objConsultaBE.FECMOD = DL_HELPER.getString(objDRConsulta, "FECMOD");

                                    if (Convert.ToString(objDRConsulta["DES_CORELE"]) == "" || objDRConsulta["DES_CORELE"] is null)
                                    {
                                        objConsultaBE.DES_CORELE = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "DES_CORELE");
                                        objConsultaBE.CORREO_COMPL = DL_HELPER.getString(objDRConsulta, "DES_CORELE")+"@"+ DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    }


                                    if (Convert.ToString(objDRConsulta["IND_ESTADO"]) == "1")
                                    {
                                        objConsultaBE.desActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.desActivo = "Inactivo";
                                    }

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BeUsuarioEP> SP_VALIDAR_CAMBIO_CONTRASENIA(string usuario)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_VALIDAR_CAMBIO_CONTRASENIA";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, usuario));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");
                                    objConsultaBE.INDCAMBIOCNIA = DL_HELPER.getInt32(objDRConsulta, "INDCAMBIOCNIA");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }

        public List<BeUsuarioEP> SP_VALIDAR_EXPIRACION_CONTRASENIA_ENLACE(string usuario)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_VALIDAR_CONTRASENIA_ENLACE";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, usuario));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }

        public List<BeUsuarioEP> SP_VALIDAR_DATOS_USER_AFTER(string usuario, string contrasenia)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_VALIDAR_DATOS_USER_CONTRASENIA";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, usuario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, contrasenia));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }

        public List<BeUsuarioEP> SP_VALIDAR_CORREO_USER_EP(string correo, string tip_documento, string usuario)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_VALIDAR_CORREO_USER_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_MailUser", OracleType.VarChar, ParameterDirection.Input, correo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipDocumento", OracleType.VarChar, ParameterDirection.Input, tip_documento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, usuario));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }

        public List<BeUsuarioEP> SP_GENERAR_NUEVO_ENLACE_UEP(string correo)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_GENERAR_ENLACE_CONTRASENIA_USER_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdUsuario", OracleType.VarChar, ParameterDirection.Input, correo));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_MsMail", OracleType.Clob));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            String po_MsMail = "";
                            
                            int ret = cmd.ExecuteNonQuery();
                            OracleLob oracleLob = (OracleLob)cmd.Parameters["po_MsMail"].Value;
                            po_MsMail = (String)oracleLob.Value;

                            //using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            using (OracleDataReader objDRConsulta = (OracleDataReader)cmd.Parameters["po_Listado"].Value)
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");

                                    objConsultaBE.HOSTNAME = DL_HELPER.getString(objDRConsulta, "HOSTNAME");
                                    objConsultaBE.PUERTO = DL_HELPER.getString(objDRConsulta, "PUERTO");
                                    objConsultaBE.DE_PARTE = DL_HELPER.getString(objDRConsulta, "DE_PARTE");
                                    objConsultaBE.CLAVE_PARTE = DL_HELPER.getString(objDRConsulta, "CLAVE_PARTE");
                                    objConsultaBE.PARA = DL_HELPER.getString(objDRConsulta, "PARA");
                                    objConsultaBE.ASUNTO = DL_HELPER.getString(objDRConsulta, "ASUNTO");
                                    //objConsultaBE.MENSAJE = DL_HELPER.getString(objDRConsulta, "MENSAJE");
                                    objConsultaBE.MENSAJE = po_MsMail;

                                    if (Convert.ToString(objDRConsulta["COPIA"]) == "" || objDRConsulta["COPIA"] is null)
                                    {
                                        objConsultaBE.COPIA = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.COPIA = DL_HELPER.getString(objDRConsulta, "COPIA");
                                    }
                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }

        public List<BeUsuarioEP> SP_VALIDAR_CORREO_CAMBIO_CLAVE_USER_EP(string correo, string tip_doc, string num_doc)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_VALIDAR_CORREO_USER_CAMBIO_CLAVE_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_MailUser", OracleType.VarChar, ParameterDirection.Input, correo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipDocumento", OracleType.VarChar, ParameterDirection.Input, tip_doc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDocumento", OracleType.VarChar, ParameterDirection.Input, num_doc));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }

        public List<BeUsuarioEP> SP_GENERAR_NUEVO_CLAVE_TEMPORAL_UEP(string idusuario, string contra_enc, string contra)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_GENERAR_NUEVA_CONTRASENIA_TEMPORAL_USER_EP";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdUsuario", OracleType.VarChar, ParameterDirection.Input, idusuario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, contra));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ContraEncrip", OracleType.VarChar, ParameterDirection.Input, contra_enc));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_MsMail", OracleType.Clob));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeUsuarioEP objConsultaBE;
                            List<BeUsuarioEP> llstConsultaBE = new List<BeUsuarioEP>();
                            String po_MsMail = "";
                            
                            int ret = cmd.ExecuteNonQuery();
                            OracleLob oracleLob = (OracleLob)cmd.Parameters["po_MsMail"].Value;
                            po_MsMail = (String)oracleLob.Value;

                            //using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            using (OracleDataReader objDRConsulta = (OracleDataReader)cmd.Parameters["po_Listado"].Value)
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BeUsuarioEP();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.CONTRASENIA = DL_HELPER.getString(objDRConsulta, "CONTRASENIA");

                                    objConsultaBE.HOSTNAME = DL_HELPER.getString(objDRConsulta, "HOSTNAME");
                                    objConsultaBE.PUERTO = DL_HELPER.getString(objDRConsulta, "PUERTO");
                                    objConsultaBE.DE_PARTE = DL_HELPER.getString(objDRConsulta, "DE_PARTE");
                                    objConsultaBE.CLAVE_PARTE = DL_HELPER.getString(objDRConsulta, "CLAVE_PARTE");
                                    objConsultaBE.PARA = DL_HELPER.getString(objDRConsulta, "PARA");
                                    objConsultaBE.ASUNTO = DL_HELPER.getString(objDRConsulta, "ASUNTO");
                                    //objConsultaBE.MENSAJE = DL_HELPER.getString(objDRConsulta, "MENSAJE");
                                    objConsultaBE.MENSAJE = po_MsMail;
                                    if (Convert.ToString(objDRConsulta["COPIA"]) == "" || objDRConsulta["COPIA"] is null)
                                    {
                                        objConsultaBE.COPIA = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.COPIA = DL_HELPER.getString(objDRConsulta, "COPIA");
                                    }

                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {

                            return new List<BeUsuarioEP>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
            //return resp;

        }

    }
}
