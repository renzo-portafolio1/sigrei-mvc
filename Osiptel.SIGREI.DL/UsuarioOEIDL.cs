﻿/*******************************************************************************
* Nombre         : Usuario			             
* Descripción    : Clase que invoca las rutinas de BD necesarias para un usuario.
* Creado         : Enrique Diaz			                             
* Fecha y hora   : 15/10/2010
*********************************************************************************/
using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Data.OracleClient;
using System.Security.Cryptography;
using System.IO;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.DL
{
    public class UsuarioOEIDL
    {


        public UsuarioOEIDL()
        {
           
        }

        
        public UsuarioOEIBE SP_OBTENER_USUARIO_OEI(ref UsuarioOEIBE P_UsuarioOEIBE)
        {
            UsuarioOEIBE UsuarioOEIBE = new UsuarioOEIBE();
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_OBTENER_USUARIO_OEI";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.APLICACION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            UsuarioOEIBE = new UsuarioOEIBE();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    //                                    ParseLibrery ParseLibrery = new 

                                    UsuarioOEIBE = new UsuarioOEIBE();
                                    UsuarioOEIBE.IDUSUARIO = Convert.ToString(objDRConsulta["IDUSUARIO"]);
                                    UsuarioOEIBE.IDOEI = Convert.ToString(objDRConsulta["IDOEI"]);
                                    UsuarioOEIBE.CLAVE = Convert.ToString(objDRConsulta["CONTRASENIA"]);
                                    UsuarioOEIBE.NOMBRE = Convert.ToString(objDRConsulta["NOMBRE"]);
                                    UsuarioOEIBE.APEPAT = Convert.ToString(objDRConsulta["APEPAT"]);
                                    UsuarioOEIBE.APEMAT = Convert.ToString(objDRConsulta["APEMAT"]);
                                    UsuarioOEIBE.USUARIO = Convert.ToString(objDRConsulta["USUARIO"]);
                                    UsuarioOEIBE.EMAILOEI = Convert.ToString(objDRConsulta["EMAILOEI"]);
                                    UsuarioOEIBE.TELFIJ = Convert.ToString(objDRConsulta["TELFIJ"]);
                                    UsuarioOEIBE.TELMOV = Convert.ToString(objDRConsulta["TELMOV"]);
                                    UsuarioOEIBE.SEXO = Convert.ToString(objDRConsulta["SEXO"]);
                                    UsuarioOEIBE.TIPODOC = Convert.ToString(objDRConsulta["TIPODOC"]);
                                    UsuarioOEIBE.NRODOC = Convert.ToString(objDRConsulta["NRODOC"]);
                                    UsuarioOEIBE.EMAILPERS = Convert.ToString(objDRConsulta["EMAILPERS"]);
                                    UsuarioOEIBE.AREA = Convert.ToString(objDRConsulta["AREA"]);
                                    UsuarioOEIBE.CARGO = Convert.ToString(objDRConsulta["CARGO"]);
                                    UsuarioOEIBE.INFOADIC = Convert.ToString(objDRConsulta["INFOADIC"]);
                                    UsuarioOEIBE.IDESTADO = Convert.ToString(objDRConsulta["IDESTADO"]);
                                    UsuarioOEIBE.FECVIG = Convert.ToString(objDRConsulta["FECVIG"]);
                                    UsuarioOEIBE.IDPERFIL = Convert.ToString(objDRConsulta["IDPERFIL"]);
                                    //UsuarioOEIBE.FLACBIOCNIA = Convert.ToString(objDRConsulta["FLACBIOCNIA"]);
                                    //UsuarioOEIBE.FECCBIOCNIA = Convert.ToString(objDRConsulta["FECCBIOCNIA"]);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            UsuarioOEIBE = new UsuarioOEIBE();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("SP_OBTENER_USUARIO_OEI", "", ex.Message.ToString());
                throw ex;
            }
            return UsuarioOEIBE;
        }

        public String SP_VALIDAR_USUARIO_OEI(UsuarioOEIBE P_UsuarioOEIBE,string TIPOBUSKDA)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_VALIDAR_USUARIO_OEI";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.APLICACION));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.CLAVE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tipo", OracleType.VarChar, ParameterDirection.Input, TIPOBUSKDA));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Number));

                  
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        resp_operacion = NewConexion.ejecutaSQL(cmd);
                        resp = Convert.ToString(cmd.Parameters["po_Retorno"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }

        public String SP_VALIDAR_USUARIO_IE(BeUsuarioEP P_UsuarioOEIBE)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_VALIDAR_USUARIO_IE";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoDoc", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.COD_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.CONTRASENIA));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Number));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        resp_operacion = NewConexion.ejecutaSQL(cmd);
                        resp = Convert.ToString(cmd.Parameters["po_Retorno"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }

        public BeUsuarioEP SP_OBTENER_USUARIO_IE(ref BeUsuarioEP P_UsuarioOEIBE)
        {
            BeUsuarioEP UsuarioOEIBE = new BeUsuarioEP();
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GESTION_USU_IE.SP_OBTENER_USUARIO_IE";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoDoc", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.COD_DOCIDE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, P_UsuarioOEIBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            UsuarioOEIBE = new BeUsuarioEP();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    //                                    ParseLibrery ParseLibrery = new 

                                    UsuarioOEIBE = new BeUsuarioEP();
                                    UsuarioOEIBE.IDUSUARIO = Convert.ToString(objDRConsulta["IDUSUARIO"]);
                                    UsuarioOEIBE.COD_DOCIDE = Convert.ToString(objDRConsulta["COD_DOCIDE"]);
                                    UsuarioOEIBE.NUM_DOCIDE = Convert.ToString(objDRConsulta["NUM_DOCIDE"]);

                                    UsuarioOEIBE.DES_NOMBRE = Convert.ToString(objDRConsulta["DES_NOMBRE"]);
                                    UsuarioOEIBE.DES_NOmBRE_LAST = Convert.ToString(objDRConsulta["DES_NOMBRE_LAST"]);
                                    UsuarioOEIBE.DES_APEPAT = Convert.ToString(objDRConsulta["DES_APEPAT"]);
                                    UsuarioOEIBE.DES_APEMAT = Convert.ToString(objDRConsulta["DES_APEMAT"]);

                                    UsuarioOEIBE.DES_CORELE = Convert.ToString(objDRConsulta["DES_CORELE"]);

                                    
                                    UsuarioOEIBE.IDUNIDADEP = Convert.ToInt16(objDRConsulta["IDUNIDADEP"]);
                                    UsuarioOEIBE.IDPERFIL = Convert.ToInt16(objDRConsulta["IDPERFIL"]);
                                    UsuarioOEIBE.IDUNIDADEP = Convert.ToInt16(objDRConsulta["IDUNIDADEP"]);
                                    UsuarioOEIBE.DES_PERFIL = Convert.ToString(objDRConsulta["DES_PERFIL"]);
                                    UsuarioOEIBE.INDCAMBIOCNIA = Convert.ToInt16(objDRConsulta["INDCAMBIOCNIA"]);

                                    UsuarioOEIBE.UBIGEO = Convert.ToString(objDRConsulta["UBIGEO"]);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            UsuarioOEIBE = new BeUsuarioEP();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("SP_OBTENER_USUARIO_OEI", "", ex.Message.ToString());
                throw ex;
            }
            return UsuarioOEIBE;
        }




    }
}
