﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Osiptel.SIGREI.DL
{
    public class UsuarioDL
    {

        public string Validar(string pUsuario)
        {
            string sResult = string.Empty;
            Int32 nResult = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_VALIDAR_USUARIO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    OracleParameter prm1 = new OracleParameter("sUsuario", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("nRetorno", OracleType.Number);
                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Output;
                    prm1.Value = pUsuario;
                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);
                    using (DL_CONEXION NewDL_CONEXION = new DL_CONEXION())
                    {
                        nResult = NewDL_CONEXION.ejecutaSQL(cmd);
                        sResult = Convert.ToString(cmd.Parameters["nRetorno"].Value);                   
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sResult;
        }

        public UsuarioBE Obtener(String pIDUsuario)
        {

            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_OBTENER_USUARIO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    OracleParameter prm1 = new OracleParameter("sUsuario", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("oListado", OracleType.Cursor);
                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Output;
                    prm1.Value = pIDUsuario;
                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);

                    using (DL_CONEXION NewDL_CONEXION = new DL_CONEXION())
                    {
                        try
                        {
                            NewDL_CONEXION.retOpen();
                            cmd.Connection = NewDL_CONEXION.retConexion();
                            UsuarioBE objUsuarioBE = new UsuarioBE();
                            using (OracleDataReader oRead = cmd.ExecuteReader())
                            {
                                if (oRead.Read())
                                {
                                    objUsuarioBE.IdUsuario = (System.String)oRead.GetValue(oRead.GetOrdinal("Usuario"));
                                    objUsuarioBE.Nombres = (System.String)oRead.GetValue(oRead.GetOrdinal("Nombre"));
                                    if (oRead.GetValue(oRead.GetOrdinal("Correo")) == DBNull.Value) objUsuarioBE.Correo = ""; else objUsuarioBE.Correo = (System.String)oRead.GetValue(oRead.GetOrdinal("Correo"));
                                    if (oRead.GetValue(oRead.GetOrdinal("Area")) == DBNull.Value) objUsuarioBE.Departamento = ""; else objUsuarioBE.Departamento = (System.String)oRead.GetValue(oRead.GetOrdinal("Area"));
                                    if (oRead.GetValue(oRead.GetOrdinal("Cargo")) == DBNull.Value) objUsuarioBE.Descripcion = ""; else objUsuarioBE.Descripcion = (System.String)oRead.GetValue(oRead.GetOrdinal("Cargo"));
                                    objUsuarioBE.Estado = (System.String)oRead.GetValue(oRead.GetOrdinal("Estado"));
                                }
                                oRead.Close();
                            }
                            NewDL_CONEXION.retClose();
                            return objUsuarioBE;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        finally
                        {
                            if (NewDL_CONEXION.conexion.State == ConnectionState.Open) NewDL_CONEXION.retClose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Listar()
        {
            try
            {
                DataTable oDTUsuario = new DataTable();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_USUARIO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    OracleParameter prm = new OracleParameter("oListado", OracleType.Cursor);
                    prm.Direction = ParameterDirection.Output;
                    cmd.Parameters.Add(prm);

                    using (DL_CONEXION NewDL_CONEXION = new DL_CONEXION())
                    {
                        oDTUsuario = NewDL_CONEXION.getTable(cmd);
                        return oDTUsuario;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ObtenerIdPerfil(String pAplicacion, String pUsuario)
        {

            try
            {
                DataTable oDTPerfilUsuario = new DataTable(); ;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_OBTENER_PERFIL_X_USUARIO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    OracleParameter prm1 = new OracleParameter("sAplicacion", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("sUsuario", OracleType.VarChar);
                    OracleParameter prm3 = new OracleParameter("oListado", OracleType.Cursor);
                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Input;
                    prm3.Direction = ParameterDirection.Output;
                    prm1.Value = pAplicacion;
                    prm2.Value = pUsuario;
                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);
                    cmd.Parameters.Add(prm3);

                    using (DL_CONEXION NewDL_CONEXION = new DL_CONEXION())
                    {
                        oDTPerfilUsuario = NewDL_CONEXION.getTable(cmd);
                        return oDTPerfilUsuario;

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Int32 ValidarPermisos(String pAplicacion, String pUsuario)
        {
            Int32 lnResult = 0;
            Int32 nResult = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_VALIDAR_ACCESO_X_APLICACION";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    OracleParameter prm1 = new OracleParameter("sAplicacion", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("sUsuario", OracleType.VarChar);
                    OracleParameter prm3 = new OracleParameter("sReturn", OracleType.Number);
                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Input;
                    prm3.Direction = ParameterDirection.Output;
                    prm1.Value = pAplicacion;
                    prm2.Value = pUsuario;
                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);
                    cmd.Parameters.Add(prm3);

                    using (DL_CONEXION NewDL_CONEXION = new DL_CONEXION())
                    {
                        nResult = NewDL_CONEXION.ejecutaSQL(cmd);
                        lnResult = Convert.ToInt32(cmd.Parameters["sReturn"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lnResult;
        }


        public string ValidarUsuEO(String pUsuario, String PContrasenia, String aplicacion)
        {
            string sResult = string.Empty;
            Int32 nResult = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_VALIDAR_USUARIO_EO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Clear();                    
                    cmd.Parameters.Add("pAplicacion", aplicacion);
                    cmd.Parameters.Add("pIDUsuario", pUsuario);
                    cmd.Parameters.Add("pContrasenia", PContrasenia);
                    cmd.Parameters.Add(new OracleParameter("pRetorno", OracleType.Int32)).Direction = ParameterDirection.Output;

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        nResult = NewConexion.ejecutaSQL(cmd);
                        sResult = Convert.ToString(nResult); 
                        //Convert.ToString(cmd.Parameters["pRetorno"].Value);                  
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sResult;
        }

        public DataTable ObtenerUsuEO(String pIDUsuario, String aplicacion)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_OBTENER_USUARIO_EO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    
                    cmd.Parameters.Add("pAplicacion", aplicacion);
                    cmd.Parameters.Add("pIDUsuario", pIDUsuario);
                    cmd.Parameters.Add(new OracleParameter("oListado", OracleType.Cursor)).Direction = ParameterDirection.Output;

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            DataTable dt = new DataTable();
                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            NewConexion.retClose();
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static IConfiguration Configuration { get; set; }
        public List<BeAccesoOpcion> obtenerListaOpcionPerfil(string idPerfil)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            var RutaLog = Configuration["RutaLog"];
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_OPCION_X_PERFIL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    OracleParameter prm1 = new OracleParameter("sAplicacion", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("nIDPerfil", OracleType.VarChar);
                    OracleParameter prm6 = new OracleParameter("oListado", OracleType.Cursor);

                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Input;
                    prm6.Direction = ParameterDirection.Output;
                    string s = "SIGREI";

                    prm1.Value = s;
                    prm2.Value = idPerfil;

                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);
                    cmd.Parameters.Add(prm6);

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BeAccesoOpcion objConsultaBE;
                            List<BeAccesoOpcion> llstConsultaBE = new List<BeAccesoOpcion>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BeAccesoOpcion();
                                    objConsultaBE.IDOPCION = Convert.ToString(objDRConsulta["IDOPCION"]);
                                    objConsultaBE.APLICACION = Convert.ToString(objDRConsulta["APLICACION"]);                                    
                                    objConsultaBE.RUTA = Convert.ToString(objDRConsulta["RUTA"]);
                                    objConsultaBE.DESCRIPCION = Convert.ToString(objDRConsulta["DESCRIPCION"]);
                                    objConsultaBE.IDPADRE = Convert.ToString(objDRConsulta["IDPADRE"]);
                                    objConsultaBE.ORDEN = Convert.ToString(objDRConsulta["ORDEN"]);
                                    objConsultaBE.FLGRUTA = Convert.ToString(objDRConsulta["FLG_RUTA"]);
                                    objConsultaBE.ESTADO = Convert.ToString(objDRConsulta["ESTADO"]);
                                    objConsultaBE.IDPERFIL = Convert.ToString(objDRConsulta["IDPERFIL"]);
                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            using (System.IO.StreamWriter file = new System.IO.StreamWriter(RutaLog, true))
                            {

                                file.WriteLine("-----           -INICIO DE ERROR          ----");
                                file.WriteLine("Elaborado                : Programador");
                                file.WriteLine("Fecha                    : " + DateTime.Now.ToLongDateString()); // Escribe la Traza
                                file.WriteLine("Hora de incidencia       : " + DateTime.Now.ToString("hh:mm:ss").ToString()); // Escribe la Traza
                                file.WriteLine("Mensaje de Error         :   " + ex.Message);// escribe el mensaje
                                file.WriteLine(ex.HelpLink); // Escribe la Traza
                                file.WriteLine(ex.Source); // Escribe la Traza
                                file.WriteLine(ex.StackTrace); // Escribe la Traza
                                file.WriteLine(ex.TargetSite.ToString()); // Escribe la Traza
                                file.WriteLine("-----           - FIN DE ERROR           ----");
                            }
                            string error = ex.ToString();
                            List<BeAccesoOpcion> llstConsultaBE = new List<BeAccesoOpcion>();
                            return llstConsultaBE;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(RutaLog, true))
                {

                    file.WriteLine("-----           -INICIO DE ERROR          ----");
                    file.WriteLine("Elaborado                : Programador");
                    file.WriteLine("Fecha                    : " + DateTime.Now.ToLongDateString()); // Escribe la Traza
                    file.WriteLine("Hora de incidencia       : " + DateTime.Now.ToString("hh:mm:ss").ToString()); // Escribe la Traza
                    file.WriteLine("Mensaje de Error         :   " + ex.Message);// escribe el mensaje
                    file.WriteLine(ex.HelpLink); // Escribe la Traza
                    file.WriteLine(ex.Source); // Escribe la Traza
                    file.WriteLine(ex.StackTrace); // Escribe la Traza
                    file.WriteLine(ex.TargetSite.ToString()); // Escribe la Traza
                    file.WriteLine("-----           - FIN DE ERROR           ----");
                }
                throw ex;
            }
        }

        public string FModificaClave(UsuarioBE UsuarioBE)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_CAMBIAR_CLAVE_USUARIO_EO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("pAplicacion", OracleType.VarChar).Value = "";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("pIdusuario", OracleType.VarChar).Value = UsuarioBE.IdUsuario;
                    cmd.Parameters.Add("pContrasenia", OracleType.VarChar).Value = UsuarioBE.Clave;
                    cmd.Parameters.Add("pUsuCre", OracleType.VarChar).Value = UsuarioBE.UsuarioCreacion;

                    cmd.Parameters.Add("pRetorno", OracleType.Number).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("pError", OracleType.VarChar, 400).Direction = ParameterDirection.Output;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        string pvalor = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                        string strError = Convert.ToString(cmd.Parameters["pError"].Value);
                        return pvalor;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string FEstadoUsuario(UsuarioBE UsuarioBE)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_CAMBIAR_ESTADO_USUARIO_EO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("pAplicacion", OracleType.VarChar).Value = "";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("pIDUsuario", OracleType.VarChar).Value = UsuarioBE.IdUsuario;
                    cmd.Parameters.Add("pUsuCre", OracleType.VarChar).Value = UsuarioBE.UsuarioCreacion;
                    cmd.Parameters.Add("pEstado", OracleType.VarChar).Value = UsuarioBE.Estado;

                    cmd.Parameters.Add("pRetorno", OracleType.Number).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("pError", OracleType.VarChar, 400).Direction = ParameterDirection.Output;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        string pvalor = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                        string strError = Convert.ToString(cmd.Parameters["pError"].Value);
                        return pvalor;
                    }
                }
            }catch (Exception ex){
                throw ex;
            }
        }

        public string FActualiza_Acceso_x_Usuario(UsuarioBE UsuarioBE)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_ACTUALIZAR_ACCESO_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("pAplicacion", OracleType.VarChar).Value = "";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("pIdUsuario", OracleType.VarChar).Value = UsuarioBE.IdUsuario;
                    cmd.Parameters.Add("pIdPerfil", OracleType.VarChar).Value = UsuarioBE.PERFIL;

                    cmd.Parameters.Add("pRetorno", OracleType.Number).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("pError", OracleType.VarChar, 400).Direction = ParameterDirection.Output;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        string pvalor = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                        string strError = Convert.ToString(cmd.Parameters["pError"].Value);
                        return pvalor;
                    }
                }
            }catch (Exception ex){
                throw ex;
            }
        }

        public string FDesactivar_Acceso_x_Usuario(UsuarioBE UsuarioBE)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_DESACTIVAR_ACCESO_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("pAplicacion", OracleType.VarChar).Value = "";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("pIdUsuario", OracleType.VarChar).Value = UsuarioBE.IdUsuario;

                    cmd.Parameters.Add("pRetorno", OracleType.Number).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("pError", OracleType.VarChar, 400).Direction = ParameterDirection.Output;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        string pvalor = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                        string strError = Convert.ToString(cmd.Parameters["pError"].Value);
                        return pvalor;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UsuarioBE> FListarUsuario(UsuarioBE objUsuario_BE)
        {

            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_USUARIO_EO_X_APLIC";
                    cmd.CommandType = CommandType.StoredProcedure;

                    OracleParameter prm1 = new OracleParameter("pAplicacion", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("pIdUsuario", OracleType.VarChar);
                    OracleParameter prm3 = new OracleParameter("pNombres", OracleType.VarChar);
                    OracleParameter prm4 = new OracleParameter("pIdEmpresa", OracleType.VarChar);
                    OracleParameter prm5 = new OracleParameter("pActivo", OracleType.VarChar);
                    OracleParameter prm6 = new OracleParameter("oListado", OracleType.Cursor);

                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Input;
                    prm3.Direction = ParameterDirection.Input;
                    prm4.Direction = ParameterDirection.Input;
                    prm5.Direction = ParameterDirection.Input;
                    prm6.Direction = ParameterDirection.Output;

                    prm1.Value = "";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    prm2.Value = objUsuario_BE.IdUsuario;
                    prm3.Value = objUsuario_BE.Nombres;
                    prm4.Value = objUsuario_BE.Nom_Empresa;
                    prm5.Value = objUsuario_BE.Estado;

                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);
                    cmd.Parameters.Add(prm3);
                    cmd.Parameters.Add(prm4);
                    cmd.Parameters.Add(prm5);
                    cmd.Parameters.Add(prm6);

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            UsuarioBE objConsultaBE;
                            List<UsuarioBE> llstConsultaBE = new List<UsuarioBE>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new UsuarioBE();
                                    objConsultaBE.IdUsuario = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.Nombres = Convert.ToString(objDRConsulta["NOMBRE"]);
                                    objConsultaBE.ApePat = Convert.ToString(objDRConsulta["APEPAT"]);
                                    objConsultaBE.ApeMat = Convert.ToString(objDRConsulta["APEMAT"]);
                                    objConsultaBE.TelFijo = Convert.ToString(objDRConsulta["TELFIJ"]);
                                    objConsultaBE.TelMovil = Convert.ToString(objDRConsulta["TELMOV"]);
                                    objConsultaBE.Sexo = Convert.ToString(objDRConsulta["SEXO"]);
                                    objConsultaBE.TipoDoc = Convert.ToString(objDRConsulta["TIPODOC"]);
                                    objConsultaBE.Documento = Convert.ToString(objDRConsulta["NRODOC"]);
                                    objConsultaBE.Area = Convert.ToString(objDRConsulta["AREA"]);
                                    objConsultaBE.Cargo = Convert.ToString(objDRConsulta["CARGO"]);
                                    objConsultaBE.InfAdic = Convert.ToString(objDRConsulta["INFOADIC"]);
                                    objConsultaBE.Estado = Convert.ToString(objDRConsulta["ACTIVO"]);
                                    objConsultaBE.FecVigencia = Convert.ToString(objDRConsulta["FECVIG"]).Length > 10 ? Convert.ToString(objDRConsulta["FECVIG"]).Substring(0,10) : String.Empty;
                                    objConsultaBE.Nom_Empresa = Convert.ToString(objDRConsulta["EMPRESA"]);
                                    objConsultaBE.PERFIL = Convert.ToString(objDRConsulta["DESPERFIL"]);
                                    /*
                                    objConsultaBE.USUCRE = Convert.ToString(objDRConsulta["USUCRE"]);
                                    objConsultaBE.FECCRE = Convert.ToString(objDRConsulta["FECCRE"]).Length > 10 ? Convert.ToString(objDRConsulta["FECCRE"]).Substring(0, 10) : String.Empty;
                                    objConsultaBE.USUMOD = Convert.ToString(objDRConsulta["USUMOD"]);
                                    objConsultaBE.FECMOD = Convert.ToString(objDRConsulta["FECMOD"]).Length > 10 ? Convert.ToString(objDRConsulta["FECMOD"]).Substring(0, 10) : String.Empty;
                                    */
                                    objConsultaBE.Correo = Convert.ToString(objDRConsulta["EMAILEO"]);
                                    objConsultaBE.EmailPers = Convert.ToString(objDRConsulta["EMAILPERS"]);

                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }catch (Exception ex){
                            string error = ex.ToString();
                            List<UsuarioBE> llstConsultaBE = new List<UsuarioBE>();
                            return llstConsultaBE;
                        }finally{
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }catch (Exception ex){
                throw ex;
            }
        }

        public string FInsertaUsuario(UsuarioBE objBE_Usuario)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_INSERTAR_USUARIO_EO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("pAplicacion", OracleType.VarChar).Value = "";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("pIdUsuario", OracleType.VarChar).Value = objBE_Usuario.IdUsuario;
                    cmd.Parameters.Add("pIdEmpresa", OracleType.VarChar).Value = objBE_Usuario.Nom_Empresa;
                    cmd.Parameters.Add("pContrasenia", OracleType.VarChar).Value = objBE_Usuario.Clave;
                    cmd.Parameters.Add("pNombre", OracleType.VarChar).Value = objBE_Usuario.Nombres;
                    cmd.Parameters.Add("pApePat", OracleType.VarChar).Value = objBE_Usuario.ApePat;
                    cmd.Parameters.Add("pApeMat", OracleType.VarChar).Value = objBE_Usuario.ApeMat;
                    cmd.Parameters.Add("pUsuario", OracleType.VarChar).Value = objBE_Usuario.IdUsuario;
                    cmd.Parameters.Add("pEmailEO", OracleType.VarChar).Value = objBE_Usuario.Correo;
                    cmd.Parameters.Add("pTelFij", OracleType.VarChar).Value = objBE_Usuario.TelFijo;
                    cmd.Parameters.Add("pTelMov", OracleType.VarChar).Value = objBE_Usuario.TelMovil;
                    cmd.Parameters.Add("pSexo", OracleType.VarChar).Value = objBE_Usuario.Sexo;
                    cmd.Parameters.Add("pTipoDoc", OracleType.VarChar).Value = objBE_Usuario.TipoDoc;
                    cmd.Parameters.Add("pNroDoc", OracleType.VarChar).Value = objBE_Usuario.Documento;
                    cmd.Parameters.Add("pEmailPers", OracleType.VarChar).Value = objBE_Usuario.EmailPers;
                    cmd.Parameters.Add("pArea", OracleType.VarChar).Value = objBE_Usuario.Area;
                    cmd.Parameters.Add("pCargo", OracleType.VarChar).Value = objBE_Usuario.Cargo;
                    cmd.Parameters.Add("pInfAdic", OracleType.VarChar).Value = objBE_Usuario.InfAdic;
                    cmd.Parameters.Add("pActivo", OracleType.VarChar).Value = objBE_Usuario.Estado;
                    cmd.Parameters.Add("pUsuCre", OracleType.VarChar).Value = objBE_Usuario.UsuarioCreacion;
                    cmd.Parameters.Add("pFecVig", OracleType.VarChar).Value = objBE_Usuario.FecVigencia;

                    cmd.Parameters.Add("pRetorno", OracleType.Number).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("pError", OracleType.VarChar, 400).Direction = ParameterDirection.Output;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        string pvalor = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                        string strError = Convert.ToString(cmd.Parameters["pError"].Value);
                        return pvalor;
                    }
                }
            }catch (Exception ex){
                throw ex;
            }
        }


        public string FActualizaUsuario(UsuarioBE objBE_Usuario)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_ACTUALIZAR_USUARIO_EO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("pAplicacion", OracleType.VarChar).Value = "";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("pIdUsuario", OracleType.VarChar).Value = objBE_Usuario.IdUsuario;
                    cmd.Parameters.Add("pIdEmpresa", OracleType.Number).Value = objBE_Usuario.Nom_Empresa;
                    cmd.Parameters.Add("pNombre", OracleType.VarChar).Value = objBE_Usuario.Nombres;
                    cmd.Parameters.Add("pApePat", OracleType.VarChar).Value = objBE_Usuario.ApePat;
                    cmd.Parameters.Add("pApeMat", OracleType.VarChar).Value = objBE_Usuario.ApeMat;
                    cmd.Parameters.Add("pUsuario", OracleType.VarChar).Value = objBE_Usuario.IdUsuario;
                    cmd.Parameters.Add("pEmailEO", OracleType.VarChar).Value = objBE_Usuario.Correo;
                    cmd.Parameters.Add("pTelFij", OracleType.VarChar).Value = objBE_Usuario.TelFijo;
                    cmd.Parameters.Add("pTelMov", OracleType.VarChar).Value = objBE_Usuario.TelMovil;
                    cmd.Parameters.Add("pSexo", OracleType.Char).Value = objBE_Usuario.Sexo;
                    cmd.Parameters.Add("pTipoDoc", OracleType.Char).Value = objBE_Usuario.TipoDoc;
                    cmd.Parameters.Add("pNroDoc", OracleType.VarChar).Value = objBE_Usuario.Documento;
                    cmd.Parameters.Add("pEmailPers", OracleType.VarChar).Value = objBE_Usuario.EmailPers;
                    cmd.Parameters.Add("pArea", OracleType.VarChar).Value = objBE_Usuario.Area;
                    cmd.Parameters.Add("pCargo", OracleType.VarChar).Value = objBE_Usuario.Cargo;
                    cmd.Parameters.Add("pInfAdic", OracleType.VarChar, 500).Value = objBE_Usuario.InfAdic;
                    cmd.Parameters.Add("pActivo", OracleType.Char).Value = objBE_Usuario.Estado;
                    cmd.Parameters.Add("pFecVig", OracleType.VarChar).Value = objBE_Usuario.FecVigencia;
                    cmd.Parameters.Add("pUsuCre", OracleType.VarChar).Value = objBE_Usuario.UsuarioCreacion;

                    cmd.Parameters.Add("pRetorno", OracleType.Number).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("pError", OracleType.VarChar, 400).Direction = ParameterDirection.Output;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        string pvalor = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                        string strError = Convert.ToString(cmd.Parameters["pError"].Value);
                        return pvalor;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public string FInsertaPerfil_x_Usuario(UsuarioBE objBE_Usuario)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_INSERTAR_PERFIL_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("pAplicacion", OracleType.VarChar).Value = "";//ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("pIdPerfil", OracleType.VarChar).Value = objBE_Usuario.PERFIL;
                    cmd.Parameters.Add("pUsuario", OracleType.VarChar).Value = objBE_Usuario.IdUsuario;
                  
                    cmd.Parameters.Add("pRetorno", OracleType.Number).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("pError", OracleType.VarChar, 400).Direction = ParameterDirection.Output;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        string pvalor = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                        string strError = Convert.ToString(cmd.Parameters["pError"].Value);
                        return pvalor;
                    }
                }
            }catch (Exception ex){
                throw ex;
            }
        }

        public string FActualizaPerfil_x_Usuario(UsuarioBE objBE_Usuario)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_ACTUALIZAR_PERFIL_USUARIO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("pAplicacion", OracleType.VarChar).Value = "";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("pUsuario", OracleType.VarChar).Value = objBE_Usuario.IdUsuario;
                    cmd.Parameters.Add("pEstado", OracleType.VarChar).Value = objBE_Usuario.Estado;

                    cmd.Parameters.Add("pRetorno", OracleType.Number).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("pError", OracleType.VarChar, 400).Direction = ParameterDirection.Output;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        string pvalor = Convert.ToString(cmd.Parameters["pRetorno"].Value);
                        string strError = Convert.ToString(cmd.Parameters["pError"].Value);
                        return pvalor;
                    }
                }
            }catch (Exception ex){
                throw ex;
            }
        }


        public List<UsuarioBE> FListarUsuario_Osiptel(UsuarioBE objUsuario_BE)
        {

            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_ACCESO.SP_LISTAR_USUARIO_OSIPTEL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    OracleParameter prm1 = new OracleParameter("pAplicacion", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("pIdUsuario", OracleType.VarChar);
                    OracleParameter prm3 = new OracleParameter("pNombre", OracleType.VarChar);
                    OracleParameter prm4 = new OracleParameter("pEstado", OracleType.VarChar);
                    OracleParameter prm6 = new OracleParameter("oListado", OracleType.Cursor);

                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Input;
                    prm3.Direction = ParameterDirection.Input;
                    prm4.Direction = ParameterDirection.Input;
                    prm6.Direction = ParameterDirection.Output;

                    prm1.Value = "";//ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    prm2.Value = objUsuario_BE.IdUsuario;
                    prm3.Value = objUsuario_BE.Nombres;
                    prm4.Value = objUsuario_BE.Estado;

                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);
                    cmd.Parameters.Add(prm3);
                    cmd.Parameters.Add(prm4);
                    cmd.Parameters.Add(prm6);

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            UsuarioBE objConsultaBE;
                            List<UsuarioBE> llstConsultaBE = new List<UsuarioBE>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new UsuarioBE();
                                    objConsultaBE.IdUsuario = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.Nombres = Convert.ToString(objDRConsulta["NOMBRE"]);
                                    objConsultaBE.Correo = Convert.ToString(objDRConsulta["CORREO"]);
                                    objConsultaBE.Area = Convert.ToString(objDRConsulta["AREA"]);
                                    objConsultaBE.Cargo = Convert.ToString(objDRConsulta["CARGO"]);
                                    objConsultaBE.Estado = Convert.ToString(objDRConsulta["DES_ESTADO"]);
                                    objConsultaBE.PERFIL = Convert.ToString(objDRConsulta["PERFIL"]);
                                    objConsultaBE.des_perfil = Convert.ToString(objDRConsulta["DES_PERFIL"]);
                                
                                    llstConsultaBE.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }catch (Exception ex){
                            string error = ex.ToString();
                            List<UsuarioBE> llstConsultaBE = new List<UsuarioBE>();
                            return llstConsultaBE;
                        }finally{
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }catch (Exception ex){
                throw ex;
            }
        }

        public UsuarioBE ObtenerUsuarioSolicitante(String pIDUsuario)
        {

            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_GENERAL.SP_OBTENER_USUARIO_SOLICITANTE";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    OracleParameter prm1 = new OracleParameter("pi_idUsuario", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("po_Listado", OracleType.Cursor);

                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Output;
                    prm1.Value = pIDUsuario;

                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);

                    using (DL_CONEXION NewDL_CONEXION = new DL_CONEXION())
                    {
                        try
                        {
                            NewDL_CONEXION.retOpen();
                            cmd.Connection = NewDL_CONEXION.retConexion();
                            UsuarioBE objUsuarioBE = new UsuarioBE();
                            using (OracleDataReader oRead = cmd.ExecuteReader())
                            {
                                if (oRead.Read())
                                {
                                    objUsuarioBE.IdUsuario = (System.String)oRead.GetValue(oRead.GetOrdinal("IDUSUARIO"));
                                    objUsuarioBE.idTipoSolicitante = Convert.ToInt32(oRead["IDTIPOSOLICITANTE"]);
                                    objUsuarioBE.descTipoSolicitante = Convert.ToString(oRead["DESC_TIPOSOLICITANTE"]);

                                }
                                oRead.Close();
                            }
                            NewDL_CONEXION.retClose();
                            return objUsuarioBE;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        finally
                        {
                            if (NewDL_CONEXION.conexion.State == ConnectionState.Open) NewDL_CONEXION.retClose();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
