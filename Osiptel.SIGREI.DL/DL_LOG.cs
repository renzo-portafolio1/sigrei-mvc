﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_LOG
    {
 
        public static void Insertar(String Opcion, String Error, String Usuario)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_LOG.SP_INSERTAR_LOG";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("sAplicacion", OracleType.VarChar).Value = "SIGREI";// ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    cmd.Parameters.Add("sOpcion", OracleType.VarChar).Value = Opcion;
                    cmd.Parameters.Add("nUsuario", OracleType.VarChar).Value = Usuario;
                    cmd.Parameters.Add("sError", OracleType.VarChar).Value = Error;
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
