﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;


namespace Osiptel.SIGREI.DL
{
    public class GlobalDL
    {

        public String Obtener(String pAplicacion, String pGlobal)
        {

            String sResult = String.Empty;
            Int32 nResult = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_GLOBAL.SP_OBTENER_GLOBAL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    OracleParameter prm1 = new OracleParameter("sAplicacion", OracleType.VarChar);
                    OracleParameter prm2 = new OracleParameter("sGlobal", OracleType.VarChar);
                    OracleParameter prm3 = new OracleParameter("sValor", OracleType.VarChar, 500);
                    prm1.Direction = ParameterDirection.Input;
                    prm2.Direction = ParameterDirection.Input;
                    prm3.Direction = ParameterDirection.Output;
                    prm1.Value = pAplicacion;
                    prm2.Value = pGlobal;
                    cmd.Parameters.Add(prm1);
                    cmd.Parameters.Add(prm2);
                    cmd.Parameters.Add(prm3);

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        nResult = NewConexion.ejecutaSQL(cmd);
                        sResult = Convert.ToString(cmd.Parameters["sValor"].Value);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //throw ex;
            }
            return sResult;
        }
    }
}
