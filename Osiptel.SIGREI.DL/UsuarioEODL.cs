﻿/*******************************************************************************
* Nombre         : Usuario			             
* Descripción    : Clase que invoca las rutinas de BD necesarias para un usuario.
* Creado         : Enrique Diaz			                             
* Fecha y hora   : 15/10/2010
*********************************************************************************/
using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Data.OracleClient;
using System.Security.Cryptography;
using System.IO;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;


namespace Osiptel.SIGREI.DL
{
    public class UsuarioEODL
    {


        public UsuarioEODL()
        {
            //log.in
            //Conexion cnx = new Conexion();
            //this.a_strCadenaConexion = cnx.retStrConexion();
        }
        //ESIGREI
        public List<UsuarioEOBE> SP_LISTAR_USUARIO_EO_X_APLIC(ref UsuarioEOBE P_UsuarioEOBE)
        {
            UsuarioEOBE objResultado = new UsuarioEOBE();
            List<UsuarioEOBE> LISTA = new List<UsuarioEOBE>();
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_LISTAR_USUARIO_EO_X_APLIC";                    
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, "SIGREI"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombres", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.NOMBRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdEmpresa", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.IDEMPRESA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEstado", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.IDESTADO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pagina", OracleType.Number, ParameterDirection.Input, P_UsuarioEOBE.Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_RegxPag", OracleType.Number, ParameterDirection.Input, P_UsuarioEOBE.RegistrosxPagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Orden", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.Orden));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));


                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            LISTA = new List<UsuarioEOBE>();

                            Int32 numerador = 1;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {                                   

                                    objResultado = new UsuarioEOBE();
                                    objResultado.NROITEM = numerador;
                                    objResultado.Total = Convert.ToInt32(objDRConsulta["TOTAL"]);
                                    P_UsuarioEOBE.Total = Convert.ToInt32(objDRConsulta["TOTAL"]);
                                    objResultado.IDUSUARIO = Convert.ToString(objDRConsulta["IDUSUARIO"]);
                                    objResultado.IDEMPRESA = Convert.ToString(objDRConsulta["IDEMPRESA"]);
                                    objResultado.NOMBRE = Convert.ToString(objDRConsulta["NOMBRE"]);
                                    objResultado.APEPAT = Convert.ToString(objDRConsulta["APEPAT"]);
                                    objResultado.APEMAT = Convert.ToString(objDRConsulta["APEMAT"]);
                                    objResultado.USUARIO = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objResultado.EMAILEO = Convert.ToString(objDRConsulta["EMAILEO"]);
                                    objResultado.TELFIJ = Convert.ToString(objDRConsulta["TELFIJ"]);
                                    objResultado.TELMOV = Convert.ToString(objDRConsulta["TELMOV"]);
                                    objResultado.SEXO = Convert.ToString(objDRConsulta["SEXO"]);
                                    objResultado.TIPODOC = Convert.ToString(objDRConsulta["TIPODOC"]);
                                    objResultado.NRODOC = Convert.ToString(objDRConsulta["NRODOC"]);
                                    objResultado.EMAILPERS = Convert.ToString(objDRConsulta["EMAILPERS"]);
                                    objResultado.AREA = Convert.ToString(objDRConsulta["AREA"]);
                                    objResultado.CARGO = Convert.ToString(objDRConsulta["CARGO"]);
                                    objResultado.INFOADIC = Convert.ToString(objDRConsulta["INFOADIC"]);
                                    objResultado.IDESTADO = Convert.ToString(objDRConsulta["IDESTADO"]);
                                    objResultado.FECVIG = Convert.ToString(objDRConsulta["FECVIG"]);
                                    objResultado.EMPRESA = Convert.ToString(objDRConsulta["EMPRESA"]);
                                    objResultado.IDPERFIL = Convert.ToString(objDRConsulta["IDPERFIL"]);
                                    objResultado.PERFIL = Convert.ToString(objDRConsulta["PERFIL"]);
                                    objResultado.USUCRE = Convert.ToString(objDRConsulta["USUCRE"]);
                                    objResultado.FECCRE = Convert.ToString(objDRConsulta["FECCRE"]);
                                    objResultado.USUMOD = Convert.ToString(objDRConsulta["USUMOD"]);
                                    objResultado.FECMOD = Convert.ToString(objDRConsulta["FECMOD"]);
                                    LISTA.Add(objResultado);
                                    numerador++;
                                }
                                objDRConsulta.Close();

                            }
                            NewConexion.retClose();
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            LISTA = new List<UsuarioEOBE>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("SP_LISTAR_USUARIO_EO_X_APLIC", "", ex.Message.ToString());
                throw ex;
            }
            return LISTA;
        }
        
        public string SP_LISTAR_USUARIO_EO_X_APLIC_EXCEL(ref UsuarioEOBE P_UsuarioEOBE)
        {
            UsuarioEOBE UsuarioEOBE = new UsuarioEOBE();
            string ROW = "";
            ROW += "<table>";
            ROW += "<thead>";
            ROW += "<tr>";
            ROW += "<th>Id Usuario</th>";
            //ROW += "<th>IDEMPRESA</th>";
            ROW += "<th>Nombre</th>";
            ROW += "<th>Ap Paterno</th>";
            ROW += "<th>Ap Materno</th>";
            //ROW += "<th>USUARIO</th>";
            ROW += "<th>Email</th>";
            ROW += "<th>Tel Fijo</th>";
            ROW += "<th>Tel Movil</th>";
            ROW += "<th>Sexo</th>";
            ROW += "<th>Tipo Documento</th>";
            ROW += "<th>Nro Documento</th>";
            ROW += "<th>Email Personal</th>";
            ROW += "<th>Area</th>";
            ROW += "<th>Cargo</th>";
            ROW += "<th>Inf Adicional</th>";
            ROW += "<th>Estado</th>";
            ROW += "<th>Fec Vig</th>";
            ROW += "<th>Empresa</th>";
            //ROW += "<th>IDPERFIL</th>";
            ROW += "<th>Perfil</th>";
            ROW += "<th>Usu Cre</th>";
            ROW += "<th>Fec Cre</th>";
            ROW += "<th>Usu Mod</th>";
            ROW += "<th>Fec Mod</th>";
            ROW += "</tr>";
            ROW += "</thead>";
            ROW += "<tbody>";
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_LISTAR_USUARIO_EO_X_APLIC";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                   // cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, ConfigurationManager.AppSettings["Aplicacion"].ToString()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombres", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.NOMBRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdEmpresa", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.IDEMPRESA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEstado", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.IDESTADO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pagina", OracleType.Number, ParameterDirection.Input, P_UsuarioEOBE.Pagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_RegxPag", OracleType.Number, ParameterDirection.Input, P_UsuarioEOBE.RegistrosxPagina));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Orden", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.Orden));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    ROW += "<tr>";

                                    //UsuarioEOBE.Total = Convert.ToInt32(objDRConsulta["TOTAL"]);
                                    //P_UsuarioEOBE.Total = Convert.ToInt32(objDRConsulta["TOTAL"]);
                                    ROW += "<td>&nbsp;" + (objDRConsulta["IDUSUARIO"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["IDUSUARIO"])) + "</td>";
                                    //ROW += "<td>&nbsp;" + (objDRConsulta["IDEMPRESA"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["IDEMPRESA"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["NOMBRE"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["NOMBRE"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["APEPAT"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["APEPAT"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["APEMAT"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["APEMAT"])) + "</td>";
                                    //ROW += "<td>&nbsp;" + (objDRConsulta["USUARIO"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["USUARIO"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["EMAILEO"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["EMAILEO"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["TELFIJ"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["TELFIJ"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["TELMOV"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["TELMOV"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["SEXO"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["SEXO"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["TIPODOC"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["TIPODOC"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["NRODOC"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["NRODOC"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["EMAILPERS"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["EMAILPERS"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["AREA"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["AREA"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["CARGO"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["CARGO"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["INFOADIC"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["INFOADIC"])) + "</td>";
                                    //ROW += "<td>&nbsp;" + (objDRConsulta["ACTIVO"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["ACTIVO"])) + "</td>";
                                    UsuarioEOBE.IDESTADO = Convert.ToString(objDRConsulta["IDESTADO"]);
                                    //ROW += "<td>&nbsp;" + UsuarioEOBE.ESTADO + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["FECVIG"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["FECVIG"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["EMPRESA"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["EMPRESA"])) + "</td>";
                                    //ROW += "<td>&nbsp;" + (objDRConsulta["IDPERFIL"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["IDPERFIL"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["PERFIL"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["PERFIL"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["USUCRE"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["USUCRE"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["FECCRE"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["FECCRE"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["USUMOD"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["USUMOD"])) + "</td>";
                                    ROW += "<td>&nbsp;" + (objDRConsulta["FECMOD"] == DBNull.Value ? "" : Convert.ToString(objDRConsulta["FECMOD"])) + "</td>";
                                    ROW += "</tr>";
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            //LISTA = new List<UsuarioEOBE>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar ("SP_LISTAR_USUARIO_EO_X_APLIC", "", ex.Message.ToString());
                throw ex;
            }
            ROW += "</tbody>";
            ROW += "</table>";
            return ROW;
        }
     
        //ESIGREI
        public List<PerfilBE> SP_LISTAR_PERFIL(string PERFIL)
        {
            PerfilBE PerfilBE = new PerfilBE();
            List<PerfilBE> LISTA = new List<PerfilBE>();
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_LISTAR_PERFIL";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, "SIGREI"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Perfil", OracleType.VarChar, ParameterDirection.Input, PERFIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            LISTA = new List<PerfilBE>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    //                                    ParseLibrery ParseLibrery = new 

                                    PerfilBE = new PerfilBE();
                                    PerfilBE.IDPERFIL = Convert.ToString(objDRConsulta["IDPERFIL"]);
                                    PerfilBE.DESCRIPCION = Convert.ToString(objDRConsulta["DESCRIPCION"]);

                                    LISTA.Add(PerfilBE);
                                }
                                objDRConsulta.Close();
                            }

                            NewConexion.retClose();
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            LISTA = new List<PerfilBE>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("SP_LISTAR_PERFIL", ex.Message.ToString(),"SIGREI");
                throw ex;
            }
            return LISTA;
        }
        //ESIGREI
        public String SP_INSERTAR_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_INSERTAR_USUARIO_EO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                   // string GGG = ConfigurationManager.AppSettings["Aplicacion"].ToString();
                    //int g = GGG.Length;
                    //int gg = GGG.Trim().Length;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, "SIGREI"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, string.IsNullOrEmpty(UsuarioEOBE.IDUSUARIO) ? null : UsuarioEOBE.IDUSUARIO.Trim()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEmpresa", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDEMPRESA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDPerfil", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDPERFIL));                    
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.CLAVE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, string.IsNullOrEmpty(UsuarioEOBE.NOMBRE) ? null : UsuarioEOBE.NOMBRE.Trim()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApePat", OracleType.VarChar, ParameterDirection.Input, string.IsNullOrEmpty(UsuarioEOBE.APEPAT) ? null : UsuarioEOBE.APEPAT.Trim()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApeMat", OracleType.VarChar, ParameterDirection.Input, string.IsNullOrEmpty(UsuarioEOBE.APEMAT) ? null : UsuarioEOBE.APEMAT.Trim()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, string.IsNullOrEmpty(UsuarioEOBE.IDUSUARIO) ? null : UsuarioEOBE.IDUSUARIO.Trim()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_EmailEO", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.EMAILEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TelFij", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.TELFIJ));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TelMov", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.TELMOV));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Sexo", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.SEXO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoDoc", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.TIPODOC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroDoc", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.NRODOC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_EmailPers", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.EMAILPERS));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Area", OracleType.VarChar, ParameterDirection.Input, string.IsNullOrEmpty(UsuarioEOBE.AREA) ? null : UsuarioEOBE.AREA.Trim()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Cargo", OracleType.VarChar, ParameterDirection.Input, string.IsNullOrEmpty(UsuarioEOBE.CARGO) ? null : UsuarioEOBE.CARGO.Trim()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_InfAdic", OracleType.VarChar, ParameterDirection.Input, string.IsNullOrEmpty(UsuarioEOBE.INFOADIC) ? null : UsuarioEOBE.INFOADIC.Trim()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEstado", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDESTADO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecVig", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.FECVIG));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FlaCbioC", OracleType.VarChar, ParameterDirection.Input, "1"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Number));

                    OracleParameter parameter = new OracleParameter("po_Error", OracleType.VarChar);
                    parameter.IsNullable = true;
                    parameter.Direction = ParameterDirection.Output;
                    parameter.Size = 200;
                    cmd.Parameters.Add(parameter);
                    
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        resp_operacion = NewConexion.ejecutaSQL(cmd);
                        var xxx = Convert.ToString(cmd.Parameters["po_Error"].Value);
                        resp = Convert.ToString(cmd.Parameters["po_Retorno"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
            
        }
        //ESIGREI
        public UsuarioEOBE SP_OBTENER_USUARIO_EO(ref UsuarioEOBE P_UsuarioEOBE)
        {
            UsuarioEOBE UsuarioEOBE = new UsuarioEOBE();
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_OBTENER_USUARIO_EO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, "SIGREI"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, P_UsuarioEOBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            UsuarioEOBE = new UsuarioEOBE();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    //                                    ParseLibrery ParseLibrery = new 

                                    UsuarioEOBE = new UsuarioEOBE();
                                    UsuarioEOBE.IDEMPRESA = Convert.ToString(objDRConsulta["IDEMPRESA"]);
                                    //UsuarioEOBE.CONTRASENIA = Convert.ToString(objDRConsulta["CONTRASENIA"]);
                                    UsuarioEOBE.NOMBRE = Convert.ToString(objDRConsulta["NOMBRE"]);
                                    UsuarioEOBE.APEPAT = Convert.ToString(objDRConsulta["APEPAT"]);
                                    UsuarioEOBE.APEMAT = Convert.ToString(objDRConsulta["APEMAT"]);
                                    UsuarioEOBE.USUARIO = Convert.ToString(objDRConsulta["USUARIO"]);
                                    UsuarioEOBE.IDUSUARIO = Convert.ToString(objDRConsulta["IDUSUARIO"]);
                                    UsuarioEOBE.EMAILEO = Convert.ToString(objDRConsulta["EMAILEO"]);
                                    UsuarioEOBE.TELFIJ = Convert.ToString(objDRConsulta["TELFIJ"]);
                                    UsuarioEOBE.TELMOV = Convert.ToString(objDRConsulta["TELMOV"]);
                                    UsuarioEOBE.SEXO = Convert.ToString(objDRConsulta["SEXO"]);
                                    UsuarioEOBE.TIPODOC = Convert.ToString(objDRConsulta["TIPODOC"]);
                                    UsuarioEOBE.NRODOC = Convert.ToString(objDRConsulta["NRODOC"]);
                                    UsuarioEOBE.EMAILPERS = Convert.ToString(objDRConsulta["EMAILPERS"]);
                                    UsuarioEOBE.AREA = Convert.ToString(objDRConsulta["AREA"]);
                                    UsuarioEOBE.CARGO = Convert.ToString(objDRConsulta["CARGO"]);
                                    UsuarioEOBE.INFOADIC = Convert.ToString(objDRConsulta["INFOADIC"]);
                                    UsuarioEOBE.IDESTADO = Convert.ToString(objDRConsulta["IDESTADO"]);
                                    UsuarioEOBE.FECVIG = Convert.ToString(objDRConsulta["FECVIG"]);
                                    UsuarioEOBE.IDPERFIL = Convert.ToString(objDRConsulta["IDPERFIL"]);
                                    //UsuarioEOBE.FLACBIOCNIA = Convert.ToString(objDRConsulta["FLACBIOCNIA"]);
                                    //UsuarioEOBE.FECCBIOCNIA = Convert.ToString(objDRConsulta["FECCBIOCNIA"]);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                        }
                        catch (Exception ex)
                        {
                            string error = ex.ToString();
                            UsuarioEOBE = new UsuarioEOBE();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("SP_OBTENER_USUARIO_EO", "", ex.Message.ToString());
                throw ex;
            }
            return UsuarioEOBE;
        }
        //ESIGREI
        public String SP_ACTUALIZAR_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_ACTUALIZAR_USUARIO_EO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, "SIGREI"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEmpresa", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDEMPRESA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDPerfil", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDPERFIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.NOMBRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApePat", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.APEPAT));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApeMat", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.APEMAT));
                    //cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.USUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_EmailEO", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.EMAILEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TelFij", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.TELFIJ));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TelMov", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.TELMOV));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Sexo", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.SEXO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipoDoc", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.TIPODOC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroDoc", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.NRODOC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_EmailPers", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.EMAILPERS));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Area", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.AREA));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Cargo", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.CARGO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_InfAdic", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.INFOADIC));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEstado", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDESTADO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecVig", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.FECVIG));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FlaCbioC", OracleType.VarChar, ParameterDirection.Input, "1"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Number));
                    //cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.VarChar));
                    OracleParameter parameter = new OracleParameter("po_Error", OracleType.VarChar);
                    parameter.IsNullable = true;
                    parameter.Direction = ParameterDirection.Output;
                    parameter.Size = 200;
                    cmd.Parameters.Add(parameter);

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        resp_operacion = NewConexion.ejecutaSQL(cmd);
                        var xxx = Convert.ToString(cmd.Parameters["po_Error"].Value);
                        resp = Convert.ToString(cmd.Parameters["po_Retorno"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }

        public int SP_EXISTE_PERFIL_USUARIO(UsuarioEOBE UsuarioEOBE)
        {
            int resp = 0;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_EXISTE_PERFIL_USUARIO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    //cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, ConfigurationManager.AppSettings["Aplicacion"].ToString()));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        resp_operacion = NewConexion.ejecutaSQL(cmd);
                        resp = Convert.ToInt32(cmd.Parameters["po_Retorno"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }
        //ESIGREI
        public String SP_CAMBIAR_ESTADO_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_CAMBIAR_ESTADO_USUARIO_EO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, "SIGREI"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEstado", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDESTADO));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Number));
                    
                    //cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.VarChar));
                    OracleParameter parameter = new OracleParameter("po_Error", OracleType.VarChar);
                    parameter.IsNullable = true;
                    parameter.Direction = ParameterDirection.Output;
                    parameter.Size = 200;
                    cmd.Parameters.Add(parameter);

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        resp_operacion = NewConexion.ejecutaSQL(cmd);
                        resp = Convert.ToString(cmd.Parameters["po_Retorno"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }
        //ESIGREI
        public String SP_CAMBIAR_CLAVE_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_CAMBIAR_CLAVE_USUARIO_EO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, "SIGREI"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Idusuario", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.CLAVE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.USUCRE));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Number));

                    OracleParameter parameter = new OracleParameter("po_Error", OracleType.VarChar);
                    parameter.IsNullable = true;
                    parameter.Direction = ParameterDirection.Output;
                    parameter.Size = 200;
                    cmd.Parameters.Add(parameter);

                    //cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.VarChar));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        resp_operacion = NewConexion.ejecutaSQL(cmd);
                        resp = Convert.ToString(cmd.Parameters["po_Retorno"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }
        //ESIGREI
        public String SP_VALIDAR_USUARIO_EO(UsuarioEOBE UsuarioEOBE, string TIPOBUSKDA)
        {
            string resp = string.Empty;
            int resp_operacion = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "ACCESO.PKG_SEGURIDAD.SP_VALIDAR_USUARIO_EO";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Aplicacion", OracleType.VarChar, ParameterDirection.Input, "SIGREI"));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDUsuario", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.IDUSUARIO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Contrasenia", OracleType.VarChar, ParameterDirection.Input, UsuarioEOBE.CLAVE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Tipo", OracleType.VarChar, ParameterDirection.Input, TIPOBUSKDA));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        resp_operacion = NewConexion.ejecutaSQL(cmd);
                        resp = Convert.ToString(cmd.Parameters["po_Retorno"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;

        }

    }
}
