﻿using Osiptel.SIGREI.BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;

namespace Osiptel.SIGREI.DL
{
    public class DL_REQUERIMIENTO_IMEI
    {
        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_IMEI> ListaMOVIL(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_LISTAR_MOVIL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.idRequerimientoImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroRequerimiento = Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.estadoImei = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);
                                    objConsultaBE.ultimoDigito = Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.nroSisDocAnteriores = Convert.ToString(objDRConsulta["NROSISDOCANTERIOR"]);
                                    objConsultaBE.usuSisDocAnterior = Convert.ToString(objDRConsulta["USUSISDOCANTERIOR"]);
                                    objConsultaBE.fecSisDocAnterior = Convert.ToString(objDRConsulta["FECSISDOCANTERIOR"]);
                                    objConsultaBE.nroServicio = Convert.ToString(objDRConsulta["NROSERVICIO"]);
                                    objConsultaBE.nombreTitular = Convert.ToString(objDRConsulta["NOMBRETITULAR"]);
                                    objConsultaBE.apPaternoTitular = Convert.ToString(objDRConsulta["APPATERNOTITULAR"]);
                                    objConsultaBE.apMaternoTitular = Convert.ToString(objDRConsulta["APMATERNOTITULAR"]);
                                    objConsultaBE.nombreCompletoTitular = Convert.ToString(objDRConsulta["NOMBRECOMPLETOTITULAR"]);
                                    objConsultaBE.idTipoDocumento = Convert.ToString(objDRConsulta["IDTIPODOCUMENTO"]);
                                    objConsultaBE.descTipoDocumento = Convert.ToString(objDRConsulta["DESCDOCUMENTO"]);
                                    objConsultaBE.nroDocumento = Convert.ToString(objDRConsulta["NRODOCUMENTO"]);
                                    objConsultaBE.usuarioEnvio = Convert.ToString(objDRConsulta["USUENVIO"]);
                                    objConsultaBE.fechaEnvio = Convert.ToString(objDRConsulta["FECENVIO"]);
                                    objConsultaBE.usuarioDevolucion = Convert.ToString(objDRConsulta["USUDEVOLUCION"]);
                                    objConsultaBE.fechaDevolucion = Convert.ToString(objDRConsulta["FECDEVOLUCION"]);
                                    objConsultaBE.usuarioAtencion = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtencion = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.idTipoMotivo = Convert.ToInt32(objDRConsulta["IDTIPOMOTIVO"]);
                                    objConsultaBE.descTipoMotivo = Convert.ToString(objDRConsulta["DESCTIPOMOTIVO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.estadoLuhn = Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.descDocumentoAdjunto = Convert.ToString(objDRConsulta["DESCDOCUMENTO_DESCARTE"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BE_REQUERIMIENTO_IMEI> ListaIMEI_EXCEPCION(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_LISTAR_IMEI_EXCEPCION";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.idRequerimientoImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroRequerimiento = Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.estadoImei = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);
                                    objConsultaBE.ultimoDigito = Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.nroSisDocAnteriores = Convert.ToString(objDRConsulta["NROSISDOCANTERIOR"]);
                                    objConsultaBE.usuSisDocAnterior = Convert.ToString(objDRConsulta["USUSISDOCANTERIOR"]);
                                    objConsultaBE.fecSisDocAnterior = Convert.ToString(objDRConsulta["FECSISDOCANTERIOR"]);
                                    objConsultaBE.nroServicio = Convert.ToString(objDRConsulta["NROSERVICIO"]);
                                    objConsultaBE.nombreTitular = Convert.ToString(objDRConsulta["NOMBRETITULAR"]);
                                    objConsultaBE.apPaternoTitular = Convert.ToString(objDRConsulta["APPATERNOTITULAR"]);
                                    objConsultaBE.apMaternoTitular = Convert.ToString(objDRConsulta["APMATERNOTITULAR"]);
                                    objConsultaBE.nombreCompletoTitular = Convert.ToString(objDRConsulta["NOMBRECOMPLETOTITULAR"]);
                                    objConsultaBE.idTipoDocumento = Convert.ToString(objDRConsulta["IDTIPODOCUMENTO"]);
                                    objConsultaBE.descTipoDocumento = Convert.ToString(objDRConsulta["DESCDOCUMENTO"]);
                                    objConsultaBE.nroDocumento = Convert.ToString(objDRConsulta["NRODOCUMENTO"]);
                                    objConsultaBE.usuarioEnvio = Convert.ToString(objDRConsulta["USUENVIO"]);
                                    objConsultaBE.fechaEnvio = Convert.ToString(objDRConsulta["FECENVIO"]);
                                    objConsultaBE.usuarioDevolucion = Convert.ToString(objDRConsulta["USUDEVOLUCION"]);
                                    objConsultaBE.fechaDevolucion = Convert.ToString(objDRConsulta["FECDEVOLUCION"]);
                                    objConsultaBE.usuarioAtencion = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtencion = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.idTipoMotivo = Convert.ToInt32(objDRConsulta["IDTIPOMOTIVO"]);
                                    objConsultaBE.descTipoMotivo = Convert.ToString(objDRConsulta["DESCTIPOMOTIVO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.estadoLuhn = Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.descDocumentoAdjunto = Convert.ToString(objDRConsulta["DESCDOCUMENTO_DESCARTE"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BE_REQUERIMIENTO_IMEI> ListaIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_LISTAR_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.idRequerimientoImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroRequerimiento = Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.estadoImei = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);
                                    objConsultaBE.ultimoDigito = Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.nroSisDocAnteriores = Convert.ToString(objDRConsulta["NROSISDOCANTERIOR"]);
                                    objConsultaBE.usuSisDocAnterior = Convert.ToString(objDRConsulta["USUSISDOCANTERIOR"]);
                                    objConsultaBE.fecSisDocAnterior = Convert.ToString(objDRConsulta["FECSISDOCANTERIOR"]);
                                    objConsultaBE.nroServicio = Convert.ToString(objDRConsulta["NROSERVICIO"]);
                                    objConsultaBE.nombreTitular = Convert.ToString(objDRConsulta["NOMBRETITULAR"]);
                                    objConsultaBE.apPaternoTitular = Convert.ToString(objDRConsulta["APPATERNOTITULAR"]);
                                    objConsultaBE.apMaternoTitular = Convert.ToString(objDRConsulta["APMATERNOTITULAR"]);
                                    objConsultaBE.nombreCompletoTitular = Convert.ToString(objDRConsulta["NOMBRECOMPLETOTITULAR"]);
                                    objConsultaBE.idTipoDocumento = Convert.ToString(objDRConsulta["IDTIPODOCUMENTO"]);
                                    objConsultaBE.descTipoDocumento = Convert.ToString(objDRConsulta["DESCDOCUMENTO"]);
                                    objConsultaBE.nroDocumento = Convert.ToString(objDRConsulta["NRODOCUMENTO"]);
                                    objConsultaBE.usuarioEnvio = Convert.ToString(objDRConsulta["USUENVIO"]);
                                    objConsultaBE.fechaEnvio = Convert.ToString(objDRConsulta["FECENVIO"]);
                                    objConsultaBE.usuarioDevolucion = Convert.ToString(objDRConsulta["USUDEVOLUCION"]);
                                    objConsultaBE.fechaDevolucion = Convert.ToString(objDRConsulta["FECDEVOLUCION"]);
                                    objConsultaBE.usuarioAtencion = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtencion = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.idTipoMotivo = Convert.ToInt32(objDRConsulta["IDTIPOMOTIVO"]);
                                    objConsultaBE.descTipoMotivo = Convert.ToString(objDRConsulta["DESCTIPOMOTIVO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.estadoLuhn = Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.descDocumentoAdjunto = Convert.ToString(objDRConsulta["DESCDOCUMENTO_DESCARTE"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BE_REQUERIMIENTO_IMEI ConsultarIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            try
            {
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_CONSULTAR_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                if (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;
                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimientoImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.estadoImei = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.nroServicio = Convert.ToString(objDRConsulta["NROSERVICIO"]);
                                    objConsultaBE.nombreCompletoTitular = Convert.ToString(objDRConsulta["NOMBRETITULAR"]);
                                    objConsultaBE.descTipoDocumento = Convert.ToString(objDRConsulta["DESCDOCUMENTO"]);
                                    objConsultaBE.nroDocumento = Convert.ToString(objDRConsulta["NRODOCUMENTO"]);
                                    objConsultaBE.descTipoMotivo = objDRConsulta["DESCMOTIVO"]==null ? "": Convert.ToString(objDRConsulta["DESCMOTIVO"]);
                                    objConsultaBE.descTipoMotivoAtencion = objDRConsulta["DESCMOTIVO_ATENCION"] == null ? "" : Convert.ToString(objDRConsulta["DESCMOTIVO_ATENCION"]);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                            return objConsultaBE;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO_IMEI> ListaIMEIAnalizar(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_LISTAR_IMEI_ANALIZAR";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    cmd.CommandTimeout = 480;

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.idRequerimientoImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroRequerimiento = Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.estadoImei = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);
                                    objConsultaBE.ultimoDigito = Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.longitudImei = Convert.ToInt32(objDRConsulta["LONGITUDIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.nroSisDocAnteriores = Convert.ToString(objDRConsulta["NROSISDOCANTERIOR"]);
                                    objConsultaBE.usuSisDocAnterior = Convert.ToString(objDRConsulta["USUSISDOCANTERIOR"]);
                                    objConsultaBE.fecSisDocAnterior = Convert.ToString(objDRConsulta["FECSISDOCANTERIOR"]);
                                    objConsultaBE.nroServicio = Convert.ToString(objDRConsulta["NROSERVICIO"]);
                                    objConsultaBE.nombreTitular = Convert.ToString(objDRConsulta["NOMBRETITULAR"]);
                                    objConsultaBE.apPaternoTitular = Convert.ToString(objDRConsulta["APPATERNOTITULAR"]);
                                    objConsultaBE.apMaternoTitular = Convert.ToString(objDRConsulta["APMATERNOTITULAR"]);
                                    objConsultaBE.nombreCompletoTitular = Convert.ToString(objDRConsulta["NOMBRECOMPLETOTITULAR"]);
                                    objConsultaBE.idTipoDocumento = Convert.ToString(objDRConsulta["IDTIPODOCUMENTO"]);
                                    objConsultaBE.descTipoDocumento = Convert.ToString(objDRConsulta["DESCDOCUMENTO"]);
                                    objConsultaBE.nroDocumento = Convert.ToString(objDRConsulta["NRODOCUMENTO"]);
                                    objConsultaBE.usuarioEnvio = Convert.ToString(objDRConsulta["USUENVIO"]);
                                    objConsultaBE.fechaEnvio = Convert.ToString(objDRConsulta["FECENVIO"]);
                                    objConsultaBE.usuarioDevolucion = Convert.ToString(objDRConsulta["USUDEVOLUCION"]);
                                    objConsultaBE.fechaDevolucion = Convert.ToString(objDRConsulta["FECDEVOLUCION"]);
                                    objConsultaBE.usuarioAtencion = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtencion = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.idTipoMotivo = Convert.ToInt32(objDRConsulta["IDTIPOMOTIVO"]);
                                    objConsultaBE.descTipoMotivo = Convert.ToString(objDRConsulta["DESCTIPOMOTIVO"]);

                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);

                                    objConsultaBE.estadoLuhn = Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.idEventoSeleccionado = Convert.ToInt32(objDRConsulta["IDVENTOSELECCIONADO"]);
                                    objConsultaBE.indDevolver = Convert.ToInt32(objDRConsulta["INDDEVOLVER"]);
                                    objConsultaBE.indProcesado = Convert.ToInt32(objDRConsulta["INDPROCESADO"]);
                                    objConsultaBE.descMarcaModelo = objDRConsulta["MARCAMODELO"] == null ? "" : Convert.ToString(objDRConsulta["MARCAMODELO"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO_IMEI> ListaIMEIAsignado(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_LISTAR_IMEI_ASIGNADO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.idRequerimientoImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroRequerimiento = Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.fechaAsignacion = Convert.ToString(objDRConsulta["FECASIGNACION"]);
                                    objConsultaBE.usuarioAsignacion = Convert.ToString(objDRConsulta["USUASIGNACION"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ListaIMEI_EO_DT(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            try
            {
                DataTable dtResultado = new DataTable();


                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_LISTAR_IMEI_EO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEmpresaEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idEmpresaEO));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                dtResultado.Load(objDRConsulta);

                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return dtResultado;
                        }
                        catch (Exception ex)
                        {
                            dtResultado = new DataTable();
                            return dtResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_EVENTO_IMEI> ListarEventosIMEI(BE_EVENTO_IMEI objetoParametro)
        {
            try
            {
                List<BE_EVENTO_IMEI> listaResultado = new List<BE_EVENTO_IMEI>();
                BE_EVENTO_IMEI objConsultaBE = new BE_EVENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UTIL.SP_LISTAR_OCURRENCIA_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IMEI", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_EVENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idTermMovi = Convert.ToInt64(objDRConsulta["IDTERMMOV"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["IMEI"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["OPERADORL"]);
                                    objConsultaBE.nomArchivo = Convert.ToString(objDRConsulta["ARCHIVO"]);
                                    objConsultaBE.fechaHora = Convert.ToString(objDRConsulta["FECHAHORA"]);
                                    objConsultaBE.estadoReporte = Convert.ToString(objDRConsulta["REPORTE"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_EVENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Transaccionales"

        public Int64 InsertarSisDocImeiInterno(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_INSERT_IMEI_SISDOC_INTERNO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_MovSgd", OracleType.Number, ParameterDirection.Input, objetoParametro.idMovSgd));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estadoIMEI", OracleType.VarChar, ParameterDirection.Input, objetoParametro.estadoIMEI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_fecReporte", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fecReporte));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecArchivo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaArchivo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pais", OracleType.VarChar, ParameterDirection.Input, objetoParametro.pais));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdEmpresa_EO", OracleType.VarChar, ParameterDirection.Input, objetoParametro.idEmpresaEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IndLogico", OracleType.VarChar, ParameterDirection.Input, objetoParametro.indLogico));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_validacionFormato", OracleType.VarChar, ParameterDirection.Input, objetoParametro.validacionFormato));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CantEvento", OracleType.Number, ParameterDirection.Input, objetoParametro.cantEventos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estadoLuhn", OracleType.VarChar, ParameterDirection.Input, objetoParametro.estadoLuhn));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ultimoDigito", OracleType.Number, ParameterDirection.Input, objetoParametro.ultimoDigito));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroSisDosAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroSisDocAnteriores));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuSisDocAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuSisDocAnterior));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecSisDocAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fecSisDocAnterior));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImei", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 ActualizarInformacionIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_ACTUALIZAR_INFORMACIONIMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroServicio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroServicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NombreTitular", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreTitular));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApPaterno", OracleType.VarChar, ParameterDirection.Input, objetoParametro.apPaternoTitular));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApMaterno", OracleType.VarChar, ParameterDirection.Input, objetoParametro.apMaternoTitular));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoDocumento", OracleType.VarChar, ParameterDirection.Input, objetoParametro.idTipoDocumento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroDocumento", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroDocumento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImei", OracleType.Number));

                    //IN SIGREI_IMEI_EO.APPATERNOTITULAR%TYPE,
                    //IN SIGREI_IMEI_EO.APMATERNOTITULAR%TYPE,

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Aprobar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 InsertarImeiExterno(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_INSERTAR_IMEI_EXTERNO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImei", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 ActualizarEstadoIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_ACTUALIZAR_ESTADOIMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idEvento", OracleType.Number, ParameterDirection.Input, objetoParametro.idEventoSeleccionado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoMotivo", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoMotivo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idEstado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idImei", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Aprobar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 AtenderIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_ATENDER_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoMotivo", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoMotivo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoSol", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idEstado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idImei", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Aprobar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 DevolverIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_DEVOLVER_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoSol", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoMotivo", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoMotivo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.observacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idAdjunto", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjunto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idEstado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idImei", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Aprobar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 ProcesaIMEISAR(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_IMEI.SP_PROCESAR_SAR_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImei", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        #endregion


        public Int64 InsertarImeiInterno(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_INSERT_IMEI_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_MovSgd", OracleType.Number, ParameterDirection.Input, objetoParametro.idMovSgd));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    //cmd.Parameters.Add(DL_HELPER.getParam("pi_estadoIMEI", OracleType.VarChar, ParameterDirection.Input, objetoParametro.MOTIVOREPORTE));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estadoIMEI", OracleType.VarChar, ParameterDirection.Input, objetoParametro.estadoIMEI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_fecReporte", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fecReporte));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecArchivo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaArchivo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pais", OracleType.VarChar, ParameterDirection.Input, objetoParametro.pais));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdEmpresa_EO", OracleType.VarChar, ParameterDirection.Input, objetoParametro.idEmpresaEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IndLogico", OracleType.VarChar, ParameterDirection.Input, objetoParametro.indLogico));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_validacionFormato", OracleType.VarChar, ParameterDirection.Input, objetoParametro.validacionFormato));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CantEvento", OracleType.Number, ParameterDirection.Input, objetoParametro.cantEventos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estadoLuhn", OracleType.VarChar, ParameterDirection.Input, objetoParametro.estadoLuhn));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ultimoDigito", OracleType.Number, ParameterDirection.Input, objetoParametro.ultimoDigito));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroSisDosAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroSisDocAnteriores));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuSisDocAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuSisDocAnterior));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecSisDocAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fecSisDocAnterior));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImei", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 InsertarTelefonoInterno(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_INSERT_TELEFONO_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_MovSgd", OracleType.Number, ParameterDirection.Input, objetoParametro.idMovSgd));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroTelefono", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImeiDos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estadoTelefono", OracleType.VarChar, ParameterDirection.Input, objetoParametro.estadoIMEI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_fecReporte", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fecReporte));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecArchivo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaArchivo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Pais", OracleType.VarChar, ParameterDirection.Input, objetoParametro.pais));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdEmpresa_EO", OracleType.VarChar, ParameterDirection.Input, objetoParametro.idEmpresaEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IndLogico", OracleType.VarChar, ParameterDirection.Input, objetoParametro.indLogico));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_validacionFormato", OracleType.VarChar, ParameterDirection.Input, objetoParametro.validacionFormato));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CantEvento", OracleType.Number, ParameterDirection.Input, objetoParametro.cantEventos));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estadoLuhn", OracleType.VarChar, ParameterDirection.Input, objetoParametro.estadoLuhn));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ultimoDigito", OracleType.Number, ParameterDirection.Input, objetoParametro.ultimoDigito));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroSisDosAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroSisDocAnteriores));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuSisDocAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuSisDocAnterior));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecSisDocAnterior", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fecSisDocAnterior));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDTelefono", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDTelefono"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO_IMEI> ConsultarImeiPendietes(string codigo)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_LISTAR_IMEI_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, codigo));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idImei = Convert.ToInt32(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.fechaArchivo = Convert.ToString(objDRConsulta["FECARCHIVO"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.estadoLuhn = objDRConsulta["ESTADOLUHN"] == null ? "" : Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.ultimoDigito = objDRConsulta["ULTIMODIGITO"] == null || objDRConsulta["ULTIMODIGITO"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);

                                   

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO_IMEI> ConsultarMovilPendietes(string codigo)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_LISTAR_MOVIL_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, codigo));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idImei = Convert.ToInt32(objDRConsulta["IDTELEFONO"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROTELEFONO"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOTELEFONO"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.fechaArchivo = Convert.ToString(objDRConsulta["FECARCHIVO"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.estadoLuhn = objDRConsulta["ESTADOLUHN"] == null ? "" : Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.ultimoDigito = objDRConsulta["ULTIMODIGITO"] == null || objDRConsulta["ULTIMODIGITO"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);



                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

