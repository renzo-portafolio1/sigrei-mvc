﻿using Osiptel.SIGREI.BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
//using System.OracleBulkCopy;
//using Oracle.DataAccess.Client;
//using System.Data.SqlClient;

namespace Osiptel.SIGREI.DL
{
    public class DL_REQUERIMIENTO
    {
        #region "No Transaccionales"

        public BE_REQUERIMIENTO ConsultaSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            try
            {
                Int32 numerador = 0;
                BE_REQUERIMIENTO objConsultaBE = new BE_REQUERIMIENTO(); ;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_CONSULTAR_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                if (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimiento = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.nroRequerimiento = Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    objConsultaBE.descTipoSolicitante = Convert.ToString(objDRConsulta["DESC_TIPOSOLICITANTE"]);
                                    objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nombreSolicitante = Convert.ToString(objDRConsulta["NOMSOLICITANTE"]);
                                    objConsultaBE.cargo = Convert.ToString(objDRConsulta["CARGO"]);
                                    objConsultaBE.idUbigeo = Convert.ToString(objDRConsulta["IDUBIGEO"]);
                                    objConsultaBE.departamento = Convert.ToString(objDRConsulta["DEPARTAMENTO"]);
                                    objConsultaBE.direccion = Convert.ToString(objDRConsulta["DIRECCION"]);
                                    objConsultaBE.nroSisDoc = Convert.ToString(objDRConsulta["NROSISDOC"]);
                                    objConsultaBE.correo = Convert.ToString(objDRConsulta["CORREO"]);
                                    objConsultaBE.contacto = Convert.ToString(objDRConsulta["NROCONTACTO"]);
                                    objConsultaBE.nroOficio = Convert.ToString(objDRConsulta["NROOFICIO"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.cantidadRegistrado = Convert.ToInt32(objDRConsulta["CANTIDAD_IMEI"]);
                                    objConsultaBE.cantidadValido = Convert.ToInt32(objDRConsulta["CANTIDAD_VALIDO"]);
                                    objConsultaBE.cantidadInvalido = Convert.ToInt32(objDRConsulta["CANTIDAD_INVALIDO"]);
                                    objConsultaBE.cantidadAtendido = Convert.ToInt32(objDRConsulta["CANTIDAD_ATENDIDO"]);
                                    objConsultaBE.cantidadPorAtender = Convert.ToInt32(objDRConsulta["CANTIDAD_ATENDER"]);
                                    objConsultaBE.cantidadEnviado = Convert.ToInt32(objDRConsulta["CANTIDAD_ENVIADO"]);
                                    objConsultaBE.cantidadPorDevolver = Convert.ToInt32(objDRConsulta["CANTIDAD_DEVOLVER"]);
                                    objConsultaBE.nombreArchivoAdjuntoOficio = Convert.ToString(objDRConsulta["DOCUMENTO_OFICIO"]);
                                    objConsultaBE.nombreArchivoAdjuntoIMEI = Convert.ToString(objDRConsulta["DOCUMENTO_IMEI"]);
                                    objConsultaBE.nroDocReferenciado = Convert.ToString(objDRConsulta["NRODOCREFENCIADO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);

                                    objConsultaBE.usuarioRegistro = Convert.ToString(objDRConsulta["USUCRE"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.usuarioAsignacion = Convert.ToString(objDRConsulta["USUASIGNACION"]);
                                    objConsultaBE.fechaAsignacion = Convert.ToString(objDRConsulta["FECASIGNACION"]);
                                    objConsultaBE.usuarioDerivacion = Convert.ToString(objDRConsulta["USUDERIVACION"]);
                                    objConsultaBE.fechaDerivacion = Convert.ToString(objDRConsulta["FECDERIVACION"]);
                                    objConsultaBE.usuarioFinalizacion = Convert.ToString(objDRConsulta["USUFINALIZACION"]);
                                    objConsultaBE.fechaFinalizacion = Convert.ToString(objDRConsulta["FECFINALIZACION"]);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO();
                            return objConsultaBE;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO_IMEI> ConsultaEstadoAbonExcepc(string nroImei, string TIPO_DOC_LEGAL, string NUMERO_DOC_LEGAL,string NROSERVICIOMOVIL)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_VALIDAR_IMEI_BY_ABONADOS";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.Number, ParameterDirection.Input, nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipDoc", OracleType.VarChar, ParameterDirection.Input, TIPO_DOC_LEGAL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDocumento", OracleType.VarChar, ParameterDirection.Input, NUMERO_DOC_LEGAL));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumMovil", OracleType.VarChar, ParameterDirection.Input, NROSERVICIOMOVIL));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.IMEI = Convert.ToString(objDRConsulta["IMEI"]);
                                    objConsultaBE.FUENTE_REPORTE = Convert.ToString(objDRConsulta["FUENTE_REPORTE"]);
                                    objConsultaBE.MOTIVO_REPORTE = Convert.ToString(objDRConsulta["MOTIVO_REPORTE"]);
                                    objConsultaBE.FECHA_BLOQUEO = Convert.ToString(objDRConsulta["FECHA_BLOQUEO"]);
                                    objConsultaBE.CONCESIONARIO = Convert.ToString(objDRConsulta["CONCESIONARIO"]);
                                    objConsultaBE.NOM_ABONADO = Convert.ToString(objDRConsulta["NOM_ABONADO"]);
                                    objConsultaBE.TIPO_DOC_LEGAL = Convert.ToString(objDRConsulta["TIPO_DOC_LEGAL"]);
                                    objConsultaBE.NUMERO_DOC_LEGAL = Convert.ToString(objDRConsulta["NUMERO_DOC_LEGAL"]);
                                    objConsultaBE.NUMERO_SERVICIO_MOVIL = Convert.ToString(objDRConsulta["NUMERO_SERVICIO_MOVIL"]);
 
                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO> ActualizarCabeExcepcion(long idRequerimiento, string cod_requerimiento, long idRequerimientoIMEi, string nombre)
        {
            try
            {
                List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
                BE_REQUERIMIENTO objConsultaBE = new BE_REQUERIMIENTO();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_ACTUALIZAR_EXCEPCION_REQ_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroRequerimiento", OracleType.VarChar, ParameterDirection.Input, cod_requerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idReqImei", OracleType.Number, ParameterDirection.Input, idRequerimientoIMEi));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Nombre", OracleType.VarChar, ParameterDirection.Input, nombre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    
                                    objConsultaBE.HOSTNAME = Convert.ToString(objDRConsulta["HOSTNAME"]);
                                    objConsultaBE.PUERTO = Convert.ToString(objDRConsulta["PUERTO"]);
                                    objConsultaBE.DE_PARTE = Convert.ToString(objDRConsulta["DE_PARTE"]);
                                    objConsultaBE.CLAVE_PARTE = Convert.ToString(objDRConsulta["CLAVE_PARTE"]);
                                    objConsultaBE.ASUNTO = Convert.ToString(objDRConsulta["ASUNTO"]);
                                    objConsultaBE.MENSAJE = Convert.ToString(objDRConsulta["MENSAJE"]);
                                    if (Convert.ToString(objDRConsulta["COPIA"]) == "" || objDRConsulta["COPIA"] is null)
                                    {
                                        objConsultaBE.COPIA = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.COPIA = DL_HELPER.getString(objDRConsulta, "COPIA");
                                    }



                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO> ListaSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
                BE_REQUERIMIENTO objConsultaBE = new BE_REQUERIMIENTO();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_LISTAR_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroRequerimiento", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroRequerimientoI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NomInstitucion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreInstitucion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaInicio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaFin", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NomSolicitante", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroOficio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroOficio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroSisDoc", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroSisDoc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_usuAsignado", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuarioAsignacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdTipoSolicitante", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdTipoOrigen", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoOrigen));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroRequerimientoF", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroRequerimientoF));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Opcion", OracleType.Number, ParameterDirection.Input, objetoParametro.opcion));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimiento = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.nroRequerimiento = Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    objConsultaBE.descTipoSolicitante = Convert.ToString(objDRConsulta["DESC_TIPOSOLICITANTE"]);
                                    objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nombreSolicitante = Convert.ToString(objDRConsulta["NOMSOLICITANTE"]);
                                    objConsultaBE.cargo = Convert.ToString(objDRConsulta["CARGO"]);
                                    objConsultaBE.idUbigeo = Convert.ToString(objDRConsulta["IDUBIGEO"]);
                                    objConsultaBE.departamento = Convert.ToString(objDRConsulta["DEPARTAMENTO"]);
                                    objConsultaBE.direccion = Convert.ToString(objDRConsulta["DIRECCION"]);
                                    objConsultaBE.nroSisDoc = Convert.ToString(objDRConsulta["NROSISDOC"]);
                                    objConsultaBE.correo = Convert.ToString(objDRConsulta["CORREO"]);
                                    objConsultaBE.contacto = Convert.ToString(objDRConsulta["NROCONTACTO"]);
                                    objConsultaBE.nroOficio = Convert.ToString(objDRConsulta["NROOFICIO"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.cantidadRegistrado = Convert.ToInt32(objDRConsulta["CANTIDAD_IMEI"]);
                                    objConsultaBE.cantidadValido = Convert.ToInt32(objDRConsulta["CANTIDAD_VALIDO"]);
                                    objConsultaBE.cantidadInvalido = Convert.ToInt32(objDRConsulta["CANTIDAD_INVALIDO"]);
                                    objConsultaBE.cantidadAtendido = Convert.ToInt32(objDRConsulta["CANTIDAD_ATENDIDO"]);
                                    objConsultaBE.cantidadPorAtender = Convert.ToInt32(objDRConsulta["CANTIDAD_ATENDER"]);
                                    objConsultaBE.cantidadEnviado = Convert.ToInt32(objDRConsulta["CANTIDAD_ENVIADO"]);
                                    objConsultaBE.cantidadPorDevolver = Convert.ToInt32(objDRConsulta["CANTIDAD_DEVOLVER"]);
                                    objConsultaBE.imeiAsignado = Convert.ToInt32(objDRConsulta["IMEI_ASIGNADO"]);

                                    objConsultaBE.nombreArchivoAdjuntoOficio = Convert.ToString(objDRConsulta["DOCUMENTO_OFICIO"]);
                                    objConsultaBE.nombreArchivoAdjuntoIMEI = Convert.ToString(objDRConsulta["DOCUMENTO_IMEI"]);
                                    objConsultaBE.nroDocReferenciado = Convert.ToString(objDRConsulta["NRODOCREFENCIADO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);

                                    objConsultaBE.usuarioRegistro = Convert.ToString(objDRConsulta["USUCRE"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.usuarioAsignacion = Convert.ToString(objDRConsulta["USUASIGNACION"]);
                                    objConsultaBE.fechaAsignacion = Convert.ToString(objDRConsulta["FECASIGNACION"]);
                                    objConsultaBE.usuarioDerivacion = Convert.ToString(objDRConsulta["USUDERIVACION"]);
                                    objConsultaBE.fechaDerivacion = Convert.ToString(objDRConsulta["FECDERIVACION"]);
                                    objConsultaBE.usuarioFinalizacion = Convert.ToString(objDRConsulta["USUFINALIZACION"]);
                                    objConsultaBE.fechaFinalizacion = Convert.ToString(objDRConsulta["FECFINALIZACION"]);
                                    objConsultaBE.IND_EXCEPCION = Convert.ToString(objDRConsulta["IND_EXCEPCION"]);
                                    objConsultaBE.DES_EXCEPCION = Convert.ToString(objDRConsulta["DES_EXCEPCION"]);
                                    objConsultaBE.CANTIDAD_MOVIL = Convert.ToString(objDRConsulta["CANTIDAD_MOVIL"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Transaccionales"

        public BE_REQUERIMIENTO ObtenerDatosIMEI(BE_REQUERIMIENTO objetoParametro)
        {

            BE_REQUERIMIENTO objConsultaBE = new BE_REQUERIMIENTO();

            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_OBTENER_INFO_SGD_SIGEM_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroSisDoc", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroSisDoc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FlgLogico", OracleType.VarChar, ParameterDirection.Input, objetoParametro.indLogico));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO(); ;
                                    numerador++;
                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idMovSgd = Convert.ToInt32(objDRConsulta["IDTERMMOV"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.nroSisDoc = Convert.ToString(objDRConsulta["NROSISDOC"]);
                                    objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nombreSolicitante = Convert.ToString(objDRConsulta["NOMSOLICITANTE"]);
                                    objConsultaBE.cargo = Convert.ToString(objDRConsulta["CARGO"]);
                                    objConsultaBE.direccion = Convert.ToString(objDRConsulta["DIRECCION"]);
                                    objConsultaBE.nroOficio = Convert.ToString(objDRConsulta["NROOFICIO"]);
                                    objConsultaBE.longitudImei = Convert.ToInt32(objDRConsulta["LONGITUDIMEI"]);
                                    objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.indLogico = Convert.ToString(objDRConsulta["FLGLOGICO"]);
                                    objConsultaBE.fechaArchivo = Convert.ToString(objDRConsulta["FECARCHIVO"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.idEmpresaEO = Convert.ToInt32(objDRConsulta["IDEMP_EO"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);
                                    objConsultaBE.evento = Convert.ToString(objDRConsulta["EVENTO"]);
                                    objConsultaBE.estadoLuhn = Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.ultimoDigito = Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.nroSisDocAnteriores = Convert.ToString(objDRConsulta["NROSISDOCANT"]);
                                    objConsultaBE.usuSisDocAnterior = Convert.ToString(objDRConsulta["USUSISDOCANT"]);
                                    objConsultaBE.fecSisDocAnterior = Convert.ToString(objDRConsulta["FECSISDOCANT"]);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();

                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO();
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }

                objConsultaBE.idTipoSolicitante = objetoParametro.idTipoSolicitante;
                if (objConsultaBE.nroItem == 0)
                {
                    objConsultaBE.nroImei = objetoParametro.nroImei;
                    objConsultaBE.nroSisDoc = objetoParametro.nroSisDoc;
                    objConsultaBE.indLogico = objetoParametro.indLogico;
                }


                return objConsultaBE;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Obtener Informacion de imei del sar", ex.ToString(), objetoParametro.usuCre);
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO> ObtenerDatosIMEIXML(String cadenaInicial, String cadenaUno, String cadenaDos, String cadenaTres, String cadenaCuatro, String cadenaCinco, String cadenaSeis, String cadenaSiete, String cadenaOcho, String cadenaNueve, String cadenaDiez, String cadenaFinal, BE_REQUERIMIENTO objetoParametro)
        {

            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
            BE_REQUERIMIENTO objConsultaBE = new BE_REQUERIMIENTO();




            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_OBTENER_INFO_IMEI_XML";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CadenaInicial", OracleType.VarChar, ParameterDirection.Input, cadenaInicial));

                    OracleParameter p_data = new OracleParameter("pi_ImeiClobUno", OracleType.Clob);
                    p_data.Direction = ParameterDirection.Input;
                    p_data.Value = cadenaUno;
                    p_data.Size = 40000;
                    cmd.Parameters.Add(p_data);

                    OracleParameter p_dataDos = new OracleParameter("pi_ImeiClobDos", OracleType.Clob);
                    p_dataDos.Direction = ParameterDirection.Input;
                    p_dataDos.Value = cadenaDos;
                    p_dataDos.Size = 40000;
                    cmd.Parameters.Add(p_dataDos);

                    OracleParameter p_dataTres = new OracleParameter("pi_ImeiClobTres", OracleType.Clob);
                    p_dataTres.Direction = ParameterDirection.Input;
                    p_dataTres.Value = cadenaTres;
                    p_dataTres.Size = 40000;
                    cmd.Parameters.Add(p_dataTres);

                    OracleParameter p_dataCuatro = new OracleParameter("pi_ImeiClobCuatro", OracleType.Clob);
                    p_dataCuatro.Direction = ParameterDirection.Input;
                    p_dataCuatro.Value = cadenaCuatro;
                    p_dataCuatro.Size = 40000;
                    cmd.Parameters.Add(p_dataCuatro);


                    OracleParameter p_dataCinco = new OracleParameter("pi_ImeiClobCinco", OracleType.Clob);
                    p_dataCinco.Direction = ParameterDirection.Input;
                    p_dataCinco.Value = cadenaCinco;
                    p_dataCinco.Size = 40000;
                    cmd.Parameters.Add(p_dataCinco);

                    OracleParameter p_dataSeis = new OracleParameter("pi_ImeiClobSeis", OracleType.Clob);
                    p_dataSeis.Direction = ParameterDirection.Input;
                    p_dataSeis.Value = cadenaSeis;
                    p_dataSeis.Size = 40000;
                    cmd.Parameters.Add(p_dataSeis);

                    OracleParameter p_dataSiete = new OracleParameter("pi_ImeiClobSiete", OracleType.Clob);
                    p_dataSiete.Direction = ParameterDirection.Input;
                    p_dataSiete.Value = cadenaSiete;
                    p_dataSiete.Size = 40000;
                    cmd.Parameters.Add(p_dataSiete);

                    OracleParameter p_dataOcho = new OracleParameter("pi_ImeiClobOcho", OracleType.Clob);
                    p_dataOcho.Direction = ParameterDirection.Input;
                    p_dataOcho.Value = cadenaOcho;
                    p_dataOcho.Size = 40000;
                    cmd.Parameters.Add(p_dataOcho);

                    OracleParameter p_dataNueve = new OracleParameter("pi_ImeiClobNueve", OracleType.Clob);
                    p_dataNueve.Direction = ParameterDirection.Input;
                    p_dataNueve.Value = cadenaNueve;
                    p_dataNueve.Size = 40000;
                    cmd.Parameters.Add(p_dataNueve);

                    OracleParameter p_dataDiez = new OracleParameter("pi_ImeiClobDiez", OracleType.Clob);
                    p_dataDiez.Direction = ParameterDirection.Input;
                    p_dataDiez.Value = cadenaDiez;
                    p_dataDiez.Size = 40000;
                    cmd.Parameters.Add(p_dataDiez);

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CadenaFinal", OracleType.VarChar, ParameterDirection.Input, cadenaFinal));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    cmd.CommandTimeout = 480;

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO(); ;
                                    numerador++;
                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idMovSgd = Convert.ToInt32(objDRConsulta["IDTERMMOV"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.nroSisDoc = Convert.ToString(objDRConsulta["NROSISDOC"]);
                                    objConsultaBE.tipoSolicitante = Convert.ToString(objDRConsulta["TIPOSOLICITANTE"]);
                                    objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nombreSolicitante = Convert.ToString(objDRConsulta["NOMSOLICITANTE"]);
                                    objConsultaBE.cargo = Convert.ToString(objDRConsulta["CARGO"]);
                                    objConsultaBE.direccion = Convert.ToString(objDRConsulta["DIRECCION"]);
                                    objConsultaBE.nroOficio = Convert.ToString(objDRConsulta["NROOFICIO"]);
                                    objConsultaBE.longitudImei = Convert.ToInt32(objDRConsulta["LONGITUDIMEI"]);
                                    objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.indLogico = Convert.ToString(objDRConsulta["FLGLOGICO"]);
                                    objConsultaBE.fechaArchivo = Convert.ToString(objDRConsulta["FECARCHIVO"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.idEmpresaEO = Convert.ToInt32(objDRConsulta["IDEMP_EO"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);
                                    objConsultaBE.evento = objDRConsulta["EVENTO"] == null ? "" : Convert.ToString(objDRConsulta["EVENTO"]);
                                    objConsultaBE.estadoLuhn = objDRConsulta["ESTADOLUHN"] == null ? "" : Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.ultimoDigito = objDRConsulta["ULTIMODIGITO"] == null || objDRConsulta["ULTIMODIGITO"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.nroSisDocAnteriores = objDRConsulta["NROSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["NROSISDOCANT"]);
                                    objConsultaBE.usuSisDocAnterior = objDRConsulta["USUSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["USUSISDOCANT"]);
                                    objConsultaBE.fecSisDocAnterior = objDRConsulta["FECSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["FECSISDOCANT"]);

                                    listaResultado.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();

                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO();
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }

                //objConsultaBE.idTipoSolicitante = objetoParametro.idTipoSolicitante;
                //if (objConsultaBE.nroItem == 0)
                //{
                //    objConsultaBE.nroImei = objetoParametro.nroImei;
                //    objConsultaBE.nroSisDoc = objetoParametro.nroSisDoc;
                //    objConsultaBE.indLogico = objetoParametro.indLogico;
                //}


                return listaResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Obtener Informacion de imei del sar", ex.ToString(), objetoParametro.usuCre);
                throw ex;
            }
        }



        public Int64 Insertar(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_INSERTAR_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoSolicitante", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nomInstitucion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreInstitucion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nomSolicitante", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_cargo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.cargo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idUbigeo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.idUbigeo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_direccion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.direccion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_correo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.correo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroContacto", OracleType.VarChar, ParameterDirection.Input, objetoParametro.contacto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroOficio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroOficio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroDocReferencia", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroDocReferenciado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_asunto", OracleType.VarChar, ParameterDirection.Input, objetoParametro.asunto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idAdjuntoOficio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjuntoOficio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idAdjuntoImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjuntoIMEI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 InsertarSiSDoc(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_INSERTAR_REQ_NROSISDOC";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoSolicitante", OracleType.Number, ParameterDirection.Input, Int32.Parse(objetoParametro.tipoSolicitante)));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroSisDoc", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroSisDoc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nomInstitucion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreInstitucion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nomSolicitante", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_cargo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.cargo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_direccion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.direccion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_correo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.correo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroContacto", OracleType.VarChar, ParameterDirection.Input, objetoParametro.contacto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroOficio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroOficio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroDocReferencia", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroDocReferenciado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_asunto", OracleType.VarChar, ParameterDirection.Input, objetoParametro.asunto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idAdjuntoOficio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjuntoOficio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idAdjuntoImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjuntoIMEI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public long InsertarReqEP(BE_REQUERIMIENTO objetoParametro)
        {
            long idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_INSERTAR_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoSolicitante", OracleType.Number, ParameterDirection.Input, objetoParametro.ent_publica));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroSisDoc", OracleType.VarChar, ParameterDirection.Input, " "));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nomInstitucion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreInstitucion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nomSolicitante", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_cargo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.tipoSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_direccion", OracleType.VarChar, ParameterDirection.Input, " "));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_correo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.correo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroContacto", OracleType.VarChar, ParameterDirection.Input, " "));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroOficio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.num_denuncia));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroDocReferencia", OracleType.VarChar, ParameterDirection.Input, objetoParametro.num_denuncia));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_asunto", OracleType.VarChar, ParameterDirection.Input, objetoParametro.delito));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idAdjuntoOficio", OracleType.Number, ParameterDirection.Input, 0));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idAdjuntoImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjuntoIMEI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.motivo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuarioRegistro));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UnidadEP", OracleType.Number, ParameterDirection.Input, objetoParametro.uni_ent_publica));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NumDocIde", OracleType.Number, ParameterDirection.Input, objetoParametro.descTipoSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_TipCarga", OracleType.Number, ParameterDirection.Input, objetoParametro.tipo_carga));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Dato", OracleType.Number, ParameterDirection.Input, objetoParametro.tipo_dato));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ubigeo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.idUbigeo));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        NewConexion.ejecutaSQL(cmd);
                        long pvalor = Convert.ToInt64(cmd.Parameters["po_IDRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public string ConsultarCodGeneradoReqEP(long idRequerimiento)
        {
            string idResultado = "";
            try
            {
                BE_REQUERIMIENTO objConsultaBE;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_CONSULTAR_CODIGO_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    cmd.CommandTimeout = 480;

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO(); ;
                                    numerador++;
                                    objConsultaBE.nroItem = numerador;

                                    //objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOIMEI"]);

                                    idResultado = Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    
                                   
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();

                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO();
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(),"");
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO> ListarRequerimientoEP(String Estado, String nCodRequerimiento, String dFecInicio, String dFecFin,String cPerUser)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_BUSCAR_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.VarChar, ParameterDirection.Input, Estado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CodRequerimiento", OracleType.VarChar, ParameterDirection.Input, nCodRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecInicio", OracleType.VarChar, ParameterDirection.Input, dFecInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecFin", OracleType.VarChar, ParameterDirection.Input, dFecFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, cPerUser));
                  

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_REQUERIMIENTO objConsultaBE;
                            List<BE_REQUERIMIENTO> llstConsultaBE = new List<BE_REQUERIMIENTO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BE_REQUERIMIENTO();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idRequerimiento = DL_HELPER.getInt32(objDRConsulta, "IDREQUERIMIENTO");
                                    objConsultaBE.nroRequerimiento = DL_HELPER.getString(objDRConsulta, "NROREQUERIMIENTO");
                                    objConsultaBE.nombreInstitucion = DL_HELPER.getString(objDRConsulta, "NOMINSTITUCION");
                                    objConsultaBE.fecRegistro = DL_HELPER.getString(objDRConsulta, "FECREGISTRO");

                                    objConsultaBE.idEstado = DL_HELPER.getInt32(objDRConsulta, "IDESTADO");
                                    objConsultaBE.descEstado = DL_HELPER.getString(objDRConsulta, "DES_ESTADO");
                                    objConsultaBE.tipo_dato = DL_HELPER.getString(objDRConsulta, "TIP_DATO");
                                    objConsultaBE.descTipoOrigen = DL_HELPER.getString(objDRConsulta, "DES_TIP_DATO");
                                    objConsultaBE.nroImei = DL_HELPER.getString(objDRConsulta, "CANT_IMEI");
                                    objConsultaBE.nroImeiDos = DL_HELPER.getString(objDRConsulta, "CANT_TELE");
                                    
                                    //objConsultaBE.FECMOD = DL_HELPER.getString(objDRConsulta, "FECMOD");


                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_REQUERIMIENTO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BE_REQUERIMIENTO> ListarDetalleRequerimientoEP(String codigo)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_DETALLE_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, codigo));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            BE_REQUERIMIENTO objConsultaBE;
                            List<BE_REQUERIMIENTO> llstConsultaBE = new List<BE_REQUERIMIENTO>();
                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                int num = 1;
                                while (objDRConsulta.Read())
                                {

                                    objConsultaBE = new BE_REQUERIMIENTO();

                                    objConsultaBE.nroItem = num;
                                    objConsultaBE.idRequerimiento = DL_HELPER.getInt32(objDRConsulta, "IDREQUERIMIENTO");
                                    objConsultaBE.nroRequerimiento = DL_HELPER.getString(objDRConsulta, "NROREQUERIMIENTO");
                                    objConsultaBE.nombreInstitucion = DL_HELPER.getString(objDRConsulta, "NOMINSTITUCION");
                                    objConsultaBE.fecRegistro = DL_HELPER.getString(objDRConsulta, "FECREGISTRO");

                                    objConsultaBE.idEstado = DL_HELPER.getInt32(objDRConsulta, "IDESTADO");
                                    objConsultaBE.descEstado = DL_HELPER.getString(objDRConsulta, "DES_ESTADO");
                                    objConsultaBE.tipo_dato = DL_HELPER.getString(objDRConsulta, "TIP_DATO");
                                    objConsultaBE.descTipoOrigen = DL_HELPER.getString(objDRConsulta, "DES_TIP_DATO");
                                    objConsultaBE.nroImei = DL_HELPER.getString(objDRConsulta, "CANT_IMEI");
                                    objConsultaBE.nroImeiDos = DL_HELPER.getString(objDRConsulta, "CANT_TELE");
                                    objConsultaBE.nombreSolicitante = DL_HELPER.getString(objDRConsulta, "NOM_COMPLETO");
                                    objConsultaBE.NOMBRES = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.APELLIDOS = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.EXPED = DL_HELPER.getString(objDRConsulta, "EXPED");
                                    objConsultaBE.DELITO = DL_HELPER.getString(objDRConsulta, "DELITO");
                                    objConsultaBE.MOTIVO = DL_HELPER.getString(objDRConsulta, "MOTIVO");

                                    //objConsultaBE.FECMOD = DL_HELPER.getString(objDRConsulta, "FECMOD");


                                    llstConsultaBE.Add(objConsultaBE);
                                    num++;
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return llstConsultaBE;
                        }
                        catch (Exception)
                        {

                            return new List<BE_REQUERIMIENTO>();
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public Int64 AprobarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_APROBAR_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDTipoMotivoAprobacion", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoMotivoAprobacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroSisDoc", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroSisDoc));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ComentarioAprobacion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentarioAprobacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Aprobar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 RechazarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_RECHAZO_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoMotivoRechazo", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoMotivoRechazo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ComentarioRechazo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentarioRechazo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Rechazar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 EnviarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_ENVIAR_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    //cmd.Parameters.Add(DL_HELPER.getParam("pi_ComentarioEnvio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentarioEnvio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Enviar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 AtenderSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_ATENDER_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Atender la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 FinalizarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_FINALIZAR_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentarioFinalizacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Finalizar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 AsignarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_ASIGNAR_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuAsignado", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuarioAsignacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Asignar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 DevolverSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_DEVOLVER_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Devolver la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 ValidarClaveSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_VALIDAR_CLAVE_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Clave", OracleType.VarChar, ParameterDirection.Input, objetoParametro.claveSolicitud));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Aprobar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 InsertarRequerimientoInicial(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_INSERT_REQUERIMIEN_INICIAL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdTipoSolicitante", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NomInstitucion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreInstitucion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NomSolicitante", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreSolicitante));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_cargo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.cargo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_direccion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.direccion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Correo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.correo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroContacto", OracleType.VarChar, ParameterDirection.Input, objetoParametro.contacto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroOficio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroOficio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroDocReferencia", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroDocReferenciado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Asunto", OracleType.VarChar, ParameterDirection.Input, objetoParametro.asunto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdAdjuntoOficio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjuntoOficio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdAdjuntoImei", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjuntoIMEI));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Requerimiento de información", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        #endregion


        public List<BE_REQUERIMIENTO> ConsultaEstadoImei(String imei,String cUsuario)
        {

            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
            BE_REQUERIMIENTO objConsultaBE = new BE_REQUERIMIENTO();
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_VALIDAR_IMEI_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Imei", OracleType.VarChar, ParameterDirection.Input, imei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, cUsuario));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    cmd.CommandTimeout = 480;

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO(); ;
                                    numerador++;
                                    objConsultaBE.nroItem = numerador;

                                    //objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOIMEI"]);

                                    objConsultaBE.idMovSgd = Convert.ToInt32(objDRConsulta["IDTERMMOV"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.nroSisDoc = Convert.ToString(objDRConsulta["NROSISDOC"]);
                                    objConsultaBE.tipoSolicitante = Convert.ToString(objDRConsulta["TIPOSOLICITANTE"]);
                                    objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nombreSolicitante = Convert.ToString(objDRConsulta["NOMSOLICITANTE"]);
                                    objConsultaBE.cargo = Convert.ToString(objDRConsulta["CARGO"]);
                                    objConsultaBE.direccion = Convert.ToString(objDRConsulta["DIRECCION"]);
                                    objConsultaBE.nroOficio = Convert.ToString(objDRConsulta["NROOFICIO"]);
                                    objConsultaBE.longitudImei = Convert.ToInt32(objDRConsulta["LONGITUDIMEI"]);
                                    objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.indLogico = Convert.ToString(objDRConsulta["FLGLOGICO"]);
                                    objConsultaBE.fechaArchivo = Convert.ToString(objDRConsulta["FECARCHIVO"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.idEmpresaEO = Convert.ToInt32(objDRConsulta["IDEMP_EO"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);
                                    objConsultaBE.evento = objDRConsulta["EVENTO"] == null ? "" : Convert.ToString(objDRConsulta["EVENTO"]);
                                    objConsultaBE.estadoLuhn = objDRConsulta["ESTADOLUHN"] == null ? "" : Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.ultimoDigito = objDRConsulta["ULTIMODIGITO"] == null || objDRConsulta["ULTIMODIGITO"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.nroSisDocAnteriores = objDRConsulta["NROSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["NROSISDOCANT"]);
                                    objConsultaBE.usuSisDocAnterior = objDRConsulta["USUSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["USUSISDOCANT"]);
                                    objConsultaBE.fecSisDocAnterior = objDRConsulta["FECSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["FECSISDOCANT"]);
                                    objConsultaBE.NROSERVICIOMOVIL = objDRConsulta["NROSERVICIOMOVIL"] == null ? "" : Convert.ToString(objDRConsulta["NROSERVICIOMOVIL"]);
                                    objConsultaBE.MOTIVOREPORTE = objDRConsulta["MOTIVOREPORTE"] == null ? "" : Convert.ToString(objDRConsulta["MOTIVOREPORTE"]);
                                    objConsultaBE.FUENTEREPORTE = objDRConsulta["FUENTEREPORTE"] == null ? "" : Convert.ToString(objDRConsulta["FUENTEREPORTE"]);
                                    objConsultaBE.TIPO_DOC_LEGAL = objDRConsulta["TIPO_DOC_LEGAL"] == null ? "" : Convert.ToString(objDRConsulta["TIPO_DOC_LEGAL"]);
                                    objConsultaBE.NUMERO_DOC_LEGAL = objDRConsulta["NUMERO_DOC_LEGAL"] == null ? "" : Convert.ToString(objDRConsulta["NUMERO_DOC_LEGAL"]);
                                    listaResultado.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();

                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO();
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }


                return listaResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Obtener Informacion de imei del sar", ex.ToString(),"");
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO> ConsultaEstadoTelefono(String imei, String cUsuario)
        {

            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
            BE_REQUERIMIENTO objConsultaBE = new BE_REQUERIMIENTO();
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_VALIDAR_TELEFONO_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Telefono", OracleType.VarChar, ParameterDirection.Input, imei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, cUsuario));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    cmd.CommandTimeout = 480;

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO(); ;
                                    numerador++;
                                    objConsultaBE.nroItem = numerador;

                                    //objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOIMEI"]);

                                    objConsultaBE.idMovSgd = Convert.ToInt32(objDRConsulta["IDTERMMOV"]);
                                    objConsultaBE.nroImeiDos = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.nroSisDoc = Convert.ToString(objDRConsulta["NROSISDOC"]);
                                    objConsultaBE.tipoSolicitante = Convert.ToString(objDRConsulta["TIPOSOLICITANTE"]);
                                    objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    //objConsultaBE.nombreInstitucion = Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nombreSolicitante = Convert.ToString(objDRConsulta["NOMSOLICITANTE"]);
                                    objConsultaBE.cargo = Convert.ToString(objDRConsulta["CARGO"]);
                                    objConsultaBE.direccion = Convert.ToString(objDRConsulta["DIRECCION"]);
                                    objConsultaBE.nroOficio = Convert.ToString(objDRConsulta["NROOFICIO"]);
                                    objConsultaBE.longitudImei = Convert.ToInt32(objDRConsulta["LONGITUDIMEI"]);
                                    objConsultaBE.estadoIMEI = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECREPORTE"]);
                                    objConsultaBE.validacionFormato = Convert.ToString(objDRConsulta["VALIDACIONFORMATO"]);
                                    objConsultaBE.indLogico = Convert.ToString(objDRConsulta["FLGLOGICO"]);
                                    objConsultaBE.fechaArchivo = Convert.ToString(objDRConsulta["FECARCHIVO"]);
                                    objConsultaBE.pais = Convert.ToString(objDRConsulta["PAIS"]);
                                    objConsultaBE.idEmpresaEO = Convert.ToInt32(objDRConsulta["IDEMP_EO"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.cantEventos = Convert.ToInt32(objDRConsulta["CANTEVENTO"]);
                                    objConsultaBE.evento = objDRConsulta["EVENTO"] == null ? "" : Convert.ToString(objDRConsulta["EVENTO"]);
                                    objConsultaBE.estadoLuhn = objDRConsulta["ESTADOLUHN"] == null ? "" : Convert.ToString(objDRConsulta["ESTADOLUHN"]);
                                    objConsultaBE.ultimoDigito = objDRConsulta["ULTIMODIGITO"] == null || objDRConsulta["ULTIMODIGITO"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["ULTIMODIGITO"]);
                                    objConsultaBE.nroSisDocAnteriores = objDRConsulta["NROSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["NROSISDOCANT"]);
                                    objConsultaBE.usuSisDocAnterior = objDRConsulta["USUSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["USUSISDOCANT"]);
                                    objConsultaBE.fecSisDocAnterior = objDRConsulta["FECSISDOCANT"] == null ? "" : Convert.ToString(objDRConsulta["FECSISDOCANT"]);
                                    listaResultado.Add(objConsultaBE);
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();

                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO();
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }


                return listaResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Obtener Informacion de imei del sar", ex.ToString(), "");
                throw ex;
            }
        }

        public BeUsuarioEP ConsultarCabeceraPendiente(String CodigoPer)
        {

            List<BeUsuarioEP> listaResultado = new List<BeUsuarioEP>();
            BeUsuarioEP objConsultaBE = new BeUsuarioEP();
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_CONSULTAR_REQUERIMIENTO_EP_PENDIENTE";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Usuario", OracleType.VarChar, ParameterDirection.Input, CodigoPer));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    cmd.CommandTimeout = 480;

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BeUsuarioEP();

                                    //objConsultaBE.nroItem = num;

                                    objConsultaBE.IDPERFIL = DL_HELPER.getInt32(objDRConsulta, "IDPERFIL");
                                    objConsultaBE.IDMODULO = DL_HELPER.getInt32(objDRConsulta, "IDMODULO");
                                    objConsultaBE.IDENTPUBLICA = DL_HELPER.getInt32(objDRConsulta, "IDENTPUBLICA");
                                    objConsultaBE.IDUNIDADEP = DL_HELPER.getInt32(objDRConsulta, "IDUNIDADEP");
                                    objConsultaBE.IND_SEXO = DL_HELPER.getString(objDRConsulta, "IND_SEXO");
                                    objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    objConsultaBE.DES_APEMAT = DL_HELPER.getString(objDRConsulta, "DES_APEMAT");
                                    objConsultaBE.COD_DOCIDE = DL_HELPER.getString(objDRConsulta, "COD_DOCIDE");

                                    objConsultaBE.DES_NOMBRE = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE");
                                    objConsultaBE.DES_NOmBRE_LAST = DL_HELPER.getString(objDRConsulta, "DES_NOMBRE_LAST");
                                    objConsultaBE.DES_APEPAT = DL_HELPER.getString(objDRConsulta, "DES_APEPAT");
                                    objConsultaBE.DES_TDOC = DL_HELPER.getString(objDRConsulta, "DES_TDOC");
                                    objConsultaBE.NUM_DOCIDE = DL_HELPER.getString(objDRConsulta, "NUM_DOCIDE");
                                    objConsultaBE.nomEntPublica = DL_HELPER.getString(objDRConsulta, "IDUNIDADEP") + "//" + "@" + DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    objConsultaBE.EXTCORREO = DL_HELPER.getString(objDRConsulta, "EXTCORREO");
                                    objConsultaBE.CORREO_COMPL = DL_HELPER.getString(objDRConsulta, "DES_CORELE") + "@" + DL_HELPER.getString(objDRConsulta, "EXTCORREO");

                                    objConsultaBE.DES_TLFFIJ = DL_HELPER.getString(objDRConsulta, "DES_TLFFIJ");
                                    objConsultaBE.DES_TLFMVL = DL_HELPER.getString(objDRConsulta, "DES_TLFMVL");
                                    objConsultaBE.DIRECCION = DL_HELPER.getString(objDRConsulta, "DIRECCION");
                                    objConsultaBE.DES_MODU = DL_HELPER.getString(objDRConsulta, "DES_MODU");
                                    objConsultaBE.DES_PERFIL = DL_HELPER.getString(objDRConsulta, "DES_PERFIL");
                                    objConsultaBE.DES_ENTPUB = DL_HELPER.getString(objDRConsulta, "DES_ENTPUB");
                                    objConsultaBE.NOMBREUNIDADP = DL_HELPER.getString(objDRConsulta, "NOMBREUNIDADP");
                                    objConsultaBE.NUM_RUC = DL_HELPER.getString(objDRConsulta, "NUM_RUC");
                                    objConsultaBE.IDUSUARIO = DL_HELPER.getString(objDRConsulta, "IDUSUARIO");
                                    objConsultaBE.IND_ESTADO = DL_HELPER.getString(objDRConsulta, "IND_ESTADO");
                                    objConsultaBE.DES_PUESTO = DL_HELPER.getString(objDRConsulta, "IDREQUERIMIENTO");
                                    objConsultaBE.Lista_Tip_Dato = DL_HELPER.getString(objDRConsulta, "TIP_DATO");
                                    objConsultaBE.Lista_Tip_Carga = DL_HELPER.getString(objDRConsulta, "TIP_CARGA");
                                    objConsultaBE.MOTIVO = DL_HELPER.getString(objDRConsulta, "MOTIVO");
                                    objConsultaBE.NRODOCREF = DL_HELPER.getString(objDRConsulta, "NRODOCREF");
                                    objConsultaBE.DELITO = DL_HELPER.getString(objDRConsulta, "DELITO");
                                    objConsultaBE.DES_UUOO = DL_HELPER.getString(objDRConsulta, "NROREQUERIMIENTO");
                                    objConsultaBE.IDUBIGEO = DL_HELPER.getString(objDRConsulta, "IDUBIGEO");
                                    objConsultaBE.UBIGEO = DL_HELPER.getString(objDRConsulta, "UBIGEO");
                                    objConsultaBE.CARGO = DL_HELPER.getString(objDRConsulta, "CARGO");

                                    //objConsultaBE.FECMOD = DL_HELPER.getString(objDRConsulta, "FECMOD");

                                    if (Convert.ToString(objDRConsulta["DES_CORELE"]) == "" || objDRConsulta["DES_CORELE"] is null)
                                    {
                                        objConsultaBE.DES_CORELE = "";
                                    }
                                    else
                                    {
                                        objConsultaBE.DES_CORELE = DL_HELPER.getString(objDRConsulta, "DES_CORELE");
                                    }


                                    if (Convert.ToString(objDRConsulta["IND_ESTADO"]) == "1")
                                    {
                                        objConsultaBE.desActivo = "Activo";
                                    }
                                    else
                                    {
                                        objConsultaBE.desActivo = "Inactivo";
                                    }
                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();

                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BeUsuarioEP();
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }


                return objConsultaBE;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Obtener Informacion de imei del sar", ex.ToString(), "");
                throw ex;
            }
        }

        public Int16 ConfirmarRegistroReqEP(int codigo)
        {
            Int16 resp = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EP.SP_CONFIRMAR_REQUERIMIENTO_EP";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IdRequerimiento", OracleType.VarChar, ParameterDirection.Input, codigo));
                   
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Retorno", OracleType.VarChar, ParameterDirection.Output, ""));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Error", OracleType.Int16, ParameterDirection.Output, ""));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                             NewConexion.ejecutaSQL(cmd);
                            resp = 1;
                        }
                        catch (Exception exp)
                        {
                            throw new Exception("Mensaje de Error: ", exp);
                            //resp = 0;

                        }

                    }
                }
                return resp;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Confirmacion de Requerimiento", ex.ToString(),"");
                throw ex;
            }
        }

    }
}

