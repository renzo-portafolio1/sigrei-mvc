﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_MOVREQUERIMIENTO
    {
        #region "No Transaccionales"
   
        
        public List<BE_MOVREQUERIMIENTO> ListaMovSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            try
            {
                List<BE_MOVREQUERIMIENTO> listaResultado = new List<BE_MOVREQUERIMIENTO>();
                BE_MOVREQUERIMIENTO objConsultaBE = new BE_MOVREQUERIMIENTO();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO.SP_LISTAR_MOVSOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));                    
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_MOVREQUERIMIENTO();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idMovSolicitud = Convert.ToInt64(objDRConsulta["IDMOVSOLICITUD"]);
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);                                   
                                    objConsultaBE.descMotivo = Convert.ToString(objDRConsulta["MOTIVO"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.usuCre = Convert.ToString(objDRConsulta["USUCRE"]);                                    

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_MOVREQUERIMIENTO>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }     
        #endregion

    

    }
}

