﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_REQUERIMIENTO_SOL
    {
        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_SOL> Listar(BE_REQUERIMIENTO_SOL objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_SOL> listaResultado = new List<BE_REQUERIMIENTO_SOL>();
                BE_REQUERIMIENTO_SOL objConsultaBE = new BE_REQUERIMIENTO_SOL(); ;
                
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_SOL.SP_LISTAR_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDRequerimientoSol", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroRequerimientoSol", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaInicio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaFin", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_SOL(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimientoSol = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTOSOL"]);
                                    objConsultaBE.nroRequerimientoSol = Convert.ToString(objDRConsulta["NROREQUERIMIENTOSOL"]);
                                    objConsultaBE.idRequerimiento = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.idTipoAtencion = Convert.ToInt32(objDRConsulta["IDTIPOATENCION"]);
                                    objConsultaBE.usuarioAtendido = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtendido = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.fechaRecibido = Convert.ToString(objDRConsulta["FECRECEPCION"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);                                    
                                    objConsultaBE.auditoria.usuMod = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.auditoria.fecMod = Convert.ToDateTime(objDRConsulta["FECHA"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_SOL>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BE_REQUERIMIENTO_SOL Consultar(BE_REQUERIMIENTO_SOL objetoParametro)
        {
            try
            {
                Int32 numerador = 0;
                BE_REQUERIMIENTO_SOL objConsultaBE = new BE_REQUERIMIENTO_SOL(); ;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_SOL.SP_CONSULTAR_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoSol", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroRequerimientoSol", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                if (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_SOL(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimientoSol = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTOSOL"]);
                                    objConsultaBE.nroRequerimientoSol = Convert.ToString(objDRConsulta["NROREQUERIMIENTOSOL"]);
                                    objConsultaBE.idRequerimiento = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.idTipoAtencion = Convert.ToInt32(objDRConsulta["IDTIPOATENCION"]);
                                    objConsultaBE.usuarioAtendido = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtendido = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.fechaRecibido = Convert.ToString(objDRConsulta["FECRECEPCION"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.auditoria.usuMod = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.auditoria.fecMod = Convert.ToDateTime(objDRConsulta["FECHA"]);
                                    objConsultaBE.descDocumentoAdjunto = Convert.ToString(objDRConsulta["DESDOCADJUNTO"]);
                                }
                                
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO_SOL();
                            return objConsultaBE;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO_IMEI> ListaIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                BE_REQUERIMIENTO_IMEI objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_SOL.SP_LISTAR_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoSol", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroRequerimientoSol", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.idRequerimientoSol = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTOSOL"]);
                                    objConsultaBE.idImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.estadoImei = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECHAREPORTE"]);
                                    objConsultaBE.nroServicio = Convert.ToString(objDRConsulta["NROSERVICIO"]);
                                    objConsultaBE.nombreTitular = Convert.ToString(objDRConsulta["NOMBRETITULAR"]);
                                    objConsultaBE.apPaternoTitular = Convert.ToString(objDRConsulta["APPATERNOTITULAR"]);
                                    objConsultaBE.apMaternoTitular = Convert.ToString(objDRConsulta["APMATERNOTITULAR"]);
                                    objConsultaBE.nombreCompletoTitular= Convert.ToString(objDRConsulta["NOMBRECOMPLETOTITULAR"]);
                                    objConsultaBE.idTipoDocumento = Convert.ToString(objDRConsulta["IDTIPODOCUMENTO"]);
                                    objConsultaBE.descTipoDocumento = Convert.ToString(objDRConsulta["DESCDOCUMENTO"]);
                                    objConsultaBE.nroDocumento = Convert.ToString(objDRConsulta["NRODOCUMENTO"]);
                                    objConsultaBE.usuarioAtencion = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtencion = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.idTipoMotivo = Convert.ToInt32(DL_HELPER.getInt32(objDRConsulta, "IDTIPOMOTIVO"));
                                    objConsultaBE.descTipoMotivo = Convert.ToString(objDRConsulta["DESCTIPOMOTIVO"]);
                                    objConsultaBE.observacion = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.usuMod = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.fecMod = Convert.ToDateTime(objDRConsulta["FECHA"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
        #endregion

        #region "Transaccionales"

        public Int64 Insertar(BE_REQUERIMIENTO_SOL objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_SOL.SP_INSERTAR_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoAtencion", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoAtencion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_DocumentoAdjunto", OracleType.Number, ParameterDirection.Input, objetoParametro.idDocumentoAdjunto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_CantidadPorAtender", OracleType.Number, ParameterDirection.Input, objetoParametro.cantidadAtendido));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimientoSol", OracleType.Number));
                  
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimientoSol"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Requerimiento", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public BE_REQUERIMIENTO_SOL Recibir(BE_REQUERIMIENTO_SOL objetoParametro)
        {
            try
            {
                Int32 numerador = 0;
                BE_REQUERIMIENTO_SOL objConsultaBE = new BE_REQUERIMIENTO_SOL(); ;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_SOL.SP_PROCESAR_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoSol", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroRequerimientoSol", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroRequerimientoSol));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                if (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_SOL(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimientoSol = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTOSOL"]);
                                    objConsultaBE.nroRequerimientoSol = Convert.ToString(objDRConsulta["NROREQUERIMIENTOSOL"]);
                                    objConsultaBE.idRequerimiento = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.idTipoAtencion = Convert.ToInt32(objDRConsulta["IDTIPOATENCION"]);
                                    objConsultaBE.usuarioAtendido = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtendido = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.fechaRecibido = Convert.ToString(objDRConsulta["FECRECEPCION"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.auditoria.usuMod = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.auditoria.fecMod = Convert.ToDateTime(objDRConsulta["FECHA"]);
                                }

                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO_SOL();
                            return objConsultaBE;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}

