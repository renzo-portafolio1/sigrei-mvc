﻿
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BE.General;
using Osiptel.SIGREI.BE.Reporte.Req;
using Osiptel.SIGREI.BE.Reporte;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;

namespace Osiptel.SIGREI.DL
{
    public class DL_REPORTE
    {
        #region "No Transaccionales"

        public List<BE_COMBO> ComboUbigeo()
        {
            try
            {
                List<BE_COMBO> listaCombo = new List<BE_COMBO>();
                BE_COMBO objC;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UBIGEO.SP_LISTAR_UBIGEO_DEPARTAMENTO_INEI";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("oListado", OracleType.Cursor).Direction = ParameterDirection.Output;
                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();
                            using (OracleDataReader obj = cmd.ExecuteReader())
                            {
                                while (obj.Read())
                                {
                                    objC = new BE_COMBO();
                                    objC.id = Convert.ToString(obj["IDDD"]);
                                    objC.descripcion = Convert.ToString(obj["DEPARTAMENTO"]);
                                    listaCombo.Add(objC);
                                }
                                obj.Close();
                            }
                            con.retClose();
                            return listaCombo;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            listaCombo = new List<BE_COMBO>();
                            return listaCombo;
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<BE_COMBO> CombosMes()
        {
            try
            {
                List<BE_COMBO> listaCombo = new List<BE_COMBO>();
                BE_COMBO objC;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UTIL_COMUN.SP_OBTENER_MES";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("oListado", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();
                            using (OracleDataReader obj = cmd.ExecuteReader())
                            {
                                while (obj.Read())
                                {
                                    objC = new BE_COMBO();
                                    objC.id = Convert.ToString(obj["NUM_MES"]);
                                    objC.descripcion = Convert.ToString(obj["DES_MES"]);
                                    listaCombo.Add(objC);
                                }
                                obj.Close();
                            }
                            con.retClose();
                            return listaCombo;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            listaCombo = new List<BE_COMBO>();
                            return listaCombo;
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<BE_COMBO> CombosEO()
        {
            try
            {
                List<BE_COMBO> listaCombo = new List<BE_COMBO>();
                BE_COMBO objC;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_UTIL.SP_LISTAR_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("oListado", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();
                            using (OracleDataReader obj = cmd.ExecuteReader())
                            {
                                while (obj.Read())
                                {
                                    objC = new BE_COMBO();
                                    objC.id = Convert.ToString(obj["IDOPE"]);
                                    objC.descripcion = Convert.ToString(obj["RAZON_SOCIAL"]);
                                    listaCombo.Add(objC);
                                }
                                obj.Close();
                            }
                            con.retClose();
                            return listaCombo;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            listaCombo = new List<BE_COMBO>();
                            return listaCombo;
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public DataTable ListarReporteRegion(BE_REPORTE_REGION objRegion)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_REGION_GRAFICO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objRegion.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_mes", Convert.ToInt16(objRegion.periodo)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_ubigeo", objRegion.region).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_institucion", Convert.ToInt16(objRegion.institucion)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_desde", Convert.ToString(objRegion.desde)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_hasta", Convert.ToString(objRegion.hasta)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objRegion.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_lista", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            DataTable dt = new DataTable();
                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            con.retClose();
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public DataTable TablaReporteRegion(BE_REPORTE_REGION objRegion)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_REGION_TABLA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objRegion.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_mes", Convert.ToInt16(objRegion.periodo)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_ubigeo", objRegion.region).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_institucion", Convert.ToInt16(objRegion.institucion)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_desde", Convert.ToString(objRegion.desde)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_hasta", Convert.ToString(objRegion.hasta)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objRegion.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_lista", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            DataTable dt = new DataTable();
                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            con.retClose();
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public DataTable ListarReporteEntidad(BE_REPORTE_ENTIDAD objEntidad)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_ENTIDAD_GRAFICO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objEntidad.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_mes", Convert.ToInt16(objEntidad.periodo)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_institucion", Convert.ToInt16(objEntidad.institucion)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_desde", Convert.ToString(objEntidad.desde)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_hasta", Convert.ToString(objEntidad.hasta)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objEntidad.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_lista", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            DataTable dt = new DataTable();
                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            con.retClose();
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public DataTable TablaReporteEntidad(BE_REPORTE_ENTIDAD objEntidad)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_ENTIDAD_TABLA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objEntidad.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_mes", Convert.ToInt16(objEntidad.periodo)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_institucion", Convert.ToInt16(objEntidad.institucion)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_desde", Convert.ToString(objEntidad.desde)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_hasta", Convert.ToString(objEntidad.hasta)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objEntidad.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_lista", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            DataTable dt = new DataTable();
                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            con.retClose();
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public DataTable ListarReporteEO(BE_REPORTE_EO objEO)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_EO_GRAFICO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objEO.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_periodo", Convert.ToInt16(objEO.mes)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_eo", Convert.ToInt16(objEO.eo)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_desde", Convert.ToString(objEO.desde)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_hasta", Convert.ToString(objEO.hasta)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objEO.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_listado", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            DataTable dt = new DataTable();
                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            con.retClose();
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public DataTable TablaReporteEO(BE_REPORTE_EO objEO)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_EO_TABLA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objEO.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_periodo", Convert.ToInt16(objEO.mes)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_eo", Convert.ToInt16(objEO.eo)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_desde", Convert.ToString(objEO.desde)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_hasta", Convert.ToString(objEO.hasta)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objEO.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_listado", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            DataTable dt = new DataTable();
                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            con.retClose();
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public DataTable GraficoGerencialesEO(BE_GERENCIAL_EO objEO)
        {
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_GERENCIAL_EO_GRAFICO";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objEO.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_mes", Convert.ToString(objEO.mes)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objEO.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("P_INS", Convert.ToInt16(objEO.institucion)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_eo", Convert.ToInt16(objEO.empresa)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_listado", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            DataTable dt = new DataTable();
                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            con.retClose();
                            return dt;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<BE_TABLA_EO> TablaGerencialesEO(BE_GERENCIAL_EO objEO)
        {

            List<BE_TABLA_EO> lst = new List<BE_TABLA_EO>();

            try
            {
                using (OracleConnection conexion = new OracleConnection())
                {
                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandText = "PKG_REPORTE.SP_GERENCIAL_EO_TABLA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objEO.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_mes", Convert.ToString(objEO.mes)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_eo", Convert.ToInt16(objEO.empresa)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objEO.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_listado", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        con.retOpen();
                        cmd.Connection = con.retConexion();

                        OracleDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lst.Add(new BE_TABLA_EO()
                            {
                                empresa = reader.IsDBNull(reader.GetOrdinal("empresa")) ? "-" : reader.GetString(reader.GetOrdinal("empresa")),
                                atencion = reader.IsDBNull(reader.GetOrdinal("atencion")) ? "-" : reader.GetString(reader.GetOrdinal("atencion")),
                                estado = reader.IsDBNull(reader.GetOrdinal("estado")) ? "-" : reader.GetString(reader.GetOrdinal("estado")),
                                tiposervicio = reader.IsDBNull(reader.GetOrdinal("tiposervicio")) ? "-" : reader.GetString(reader.GetOrdinal("tiposervicio")),
                                idMes = reader.IsDBNull(reader.GetOrdinal("idMes")) ? 0 : reader.GetInt32(reader.GetOrdinal("idMes")),
                                mes = reader.IsDBNull(reader.GetOrdinal("mes")) ? "-" : reader.GetString(reader.GetOrdinal("mes")),
                                dia = reader.IsDBNull(reader.GetOrdinal("dia")) ? "-" : reader.GetString(reader.GetOrdinal("dia")),
                                cantidad = reader.IsDBNull(reader.GetOrdinal("cantidad")) ? "0" : reader.GetDecimal(reader.GetOrdinal("cantidad")).ToString()
                            });
                        }
                        con.retClose();
                        return lst;
                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex;
                return lst;
            }
        }


        public List<BE_TABLA_INSTITUCION> TablaGerencialesIns(BE_GERENCIAL_EO objEO)
        {
            List<BE_TABLA_INSTITUCION> lst = new List<BE_TABLA_INSTITUCION>();

            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_GERENCIAL_INS_TABLA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", Convert.ToInt16(objEO.anio)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_mes", Convert.ToString(objEO.mes)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_ins", Convert.ToInt16(objEO.institucion)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_servicio", Convert.ToString(objEO.requerimiento)).Direction = ParameterDirection.Input;
                    cmd.Parameters.Add("p_listado", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            OracleDataReader reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                lst.Add(new BE_TABLA_INSTITUCION()
                                {
                                    estado = reader.IsDBNull(reader.GetOrdinal("estado")) ? "-" : reader.GetString(reader.GetOrdinal("estado")),
                                    entidad = reader.IsDBNull(reader.GetOrdinal("entidad")) ? "-" : reader.GetString(reader.GetOrdinal("entidad")),
                                    externa = reader.IsDBNull(reader.GetOrdinal("externa")) ? "-" : reader.GetString(reader.GetOrdinal("externa")),
                                    unidad = reader.IsDBNull(reader.GetOrdinal("unidad")) ? "-" : reader.GetString(reader.GetOrdinal("unidad")),
                                    ruc = reader.IsDBNull(reader.GetOrdinal("ruc")) ? "-" : reader.GetString(reader.GetOrdinal("ruc")),
                                    idMes = reader.IsDBNull(reader.GetOrdinal("idMes")) ? 0 : reader.GetInt32(reader.GetOrdinal("idMes")),
                                    mes = reader.IsDBNull(reader.GetOrdinal("mes")) ? "-" : reader.GetString(reader.GetOrdinal("mes")),
                                    dia = reader.IsDBNull(reader.GetOrdinal("dia")) ? "-" : reader.GetString(reader.GetOrdinal("dia")),
                                    cantidad = reader.IsDBNull(reader.GetOrdinal("cantidad")) ? "0" : reader.GetDecimal(reader.GetOrdinal("cantidad")).ToString()
                                });
                            }

                            con.retClose();
                            return lst;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public DataTable ExportarReporteGeneral(BE_GERENCIAL_EO objEO)
        {

            int anio = Convert.ToInt32(objEO.anio);
            int eo = Convert.ToInt32(objEO.empresa);
            int ins = Convert.ToInt32(objEO.institucion);
            DataTable dt = new DataTable();

            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_GENERAL";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("p_anio", anio).Direction = ParameterDirection.Input;//numerico
                    cmd.Parameters.Add("p_servicio", objEO.requerimiento).Direction = ParameterDirection.Input;//varchar
                    cmd.Parameters.Add("p_eo", eo).Direction = ParameterDirection.Input;//numero
                    cmd.Parameters.Add("p_ins", ins).Direction = ParameterDirection.Input;//numero
                    cmd.Parameters.Add("p_mes", objEO.mes).Direction = ParameterDirection.Input;//varchar
                    cmd.Parameters.Add("po_Listado", OracleType.Cursor).Direction = ParameterDirection.Output;

                    using (DL_CONEXION con = new DL_CONEXION())
                    {
                        try
                        {
                            con.retOpen();
                            cmd.Connection = con.retConexion();

                            OracleDataAdapter dap = new OracleDataAdapter(cmd);
                            dap.Fill(dt);

                            con.retClose();
                            return dt;
                        }
                        catch (OracleException ex)
                        {    
                            Console.WriteLine(ex);
                            throw;
                        }
                        finally
                        {
                            if (con.conexion.State == ConnectionState.Open)
                            {
                                con.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Exportar Gerencial", ex.ToString(), "");
                throw;
            }
        }

        public List<BE_REPORTE_IMEI> ListaReporte(BE_REPORTE_IMEI objetoParametro)
        {
            try
            {
                List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objConsultaBE = new BE_REPORTE_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_empresa", OracleType.Number, ParameterDirection.Input, objetoParametro.idEmpresaEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecIni", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FecFin", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NomInstitucion", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreInstitucion));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REPORTE_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.departamento = objDRConsulta["DEPARTAMENTO"] == null ? "" : Convert.ToString(objDRConsulta["DEPARTAMENTO"]);
                                    objConsultaBE.nombreInstitucion = objDRConsulta["NOMINSTITUCION"] == null ? "" : Convert.ToString(objDRConsulta["NOMINSTITUCION"]);
                                    objConsultaBE.nroRequerimiento = objDRConsulta["NROREQUERIMIENTO"] == null ? "" : Convert.ToString(objDRConsulta["NROREQUERIMIENTO"]);
                                    objConsultaBE.nroImei = objDRConsulta["NROIMEI"] == null ? "" : Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.estadoImei = objDRConsulta["ESTADOIMEI"] == null ? "" : Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fechaRegistro = objDRConsulta["FECREGISTRO"] == null ? "" : Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.fechaEnvio = objDRConsulta["FECENVIO"] == null ? "" : Convert.ToString(objDRConsulta["FECENVIO"]);
                                    objConsultaBE.empresaOperadora = objDRConsulta["EMPRESA_OPERADORA"] == null ? "" : Convert.ToString(objDRConsulta["EMPRESA_OPERADORA"]);
                                    objConsultaBE.fecPlazo = objDRConsulta["FECPLAZO"] == null ? "" : Convert.ToString(objDRConsulta["FECPLAZO"]);
                                    objConsultaBE.cumplioPlazo = objDRConsulta["CUMPLIO"] == null ? "" : Convert.ToString(objDRConsulta["CUMPLIO"]);
                                    objConsultaBE.usuarioAtendido = objDRConsulta["USUATENCION"] == null ? "" : Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtendido = objDRConsulta["FECATENCION"] == null ? "" : Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.usuRespuesta = objDRConsulta["USURESPUESTA"] == null ? "" : Convert.ToString(objDRConsulta["USURESPUESTA"]);
                                    objConsultaBE.fecRespuesta = objDRConsulta["FECRESPUESTA"] == null ? "" : Convert.ToString(objDRConsulta["FECRESPUESTA"]);


                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REPORTE_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REPORTE_IMEI> ListaReporteEntidad(BE_REPORTE_IMEI objetoParametro)
        {
            try
            {
                List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objConsultaBE = new BE_REPORTE_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_REQUERIMIENTO_ENTIDAD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_anio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAnio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REPORTE_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    //objConsultaBE.idTipoSolicitante = objDRConsulta["IDTIPOSOLICITANTE"] == null || objDRConsulta["IDTIPOSOLICITANTE"].ToString()=="" ? 0 : Convert.ToInt32(objDRConsulta["IDTIPOSOLICITANTE"]);
                                    objConsultaBE.descripcion = objDRConsulta["DESC_TIPOSOLICITANTE"] == null ? "" : Convert.ToString(objDRConsulta["DESC_TIPOSOLICITANTE"]);
                                    objConsultaBE.enero = objDRConsulta["1"] == null || objDRConsulta["1"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["1"]);
                                    objConsultaBE.febrero = objDRConsulta["2"] == null || objDRConsulta["2"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["2"]);
                                    objConsultaBE.marzo = objDRConsulta["3"] == null || objDRConsulta["3"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["3"]);
                                    objConsultaBE.abril = objDRConsulta["4"] == null || objDRConsulta["4"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["4"]);
                                    objConsultaBE.mayo = objDRConsulta["5"] == null || objDRConsulta["5"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["5"]);
                                    objConsultaBE.junio = objDRConsulta["6"] == null || objDRConsulta["6"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["6"]);
                                    objConsultaBE.julio = objDRConsulta["7"] == null || objDRConsulta["7"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["7"]);
                                    objConsultaBE.agosto = objDRConsulta["8"] == null || objDRConsulta["8"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["8"]);
                                    objConsultaBE.setiembre = objDRConsulta["9"] == null || objDRConsulta["9"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["9"]);
                                    objConsultaBE.octubre = objDRConsulta["10"] == null || objDRConsulta["10"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["10"]);
                                    objConsultaBE.noviembre = objDRConsulta["11"] == null || objDRConsulta["11"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["11"]);
                                    objConsultaBE.diciembre = objDRConsulta["12"] == null || objDRConsulta["12"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["12"]);

                                    objConsultaBE.totalFila = objConsultaBE.enero + objConsultaBE.febrero + objConsultaBE.marzo + objConsultaBE.abril + objConsultaBE.mayo + objConsultaBE.junio +
                                                        objConsultaBE.julio + objConsultaBE.agosto + objConsultaBE.setiembre + objConsultaBE.octubre + objConsultaBE.noviembre + objConsultaBE.diciembre;

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REPORTE_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REPORTE_IMEI> ListaReporteDepartamento(BE_REPORTE_IMEI objetoParametro)
        {
            try
            {
                List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objConsultaBE = new BE_REPORTE_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_REQUERIMIENTO_DPTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_anio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAnio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REPORTE_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    //objConsultaBE.idUbigeo = objDRConsulta["IDUBIGEO"] == null || objDRConsulta["IDUBIGEO"].ToString()=="" ? 0 : Convert.ToInt32(objDRConsulta["IDUBIGEO"]);
                                    objConsultaBE.descripcion = objDRConsulta["DEPARTAMENTO"] == null ? "" : Convert.ToString(objDRConsulta["DEPARTAMENTO"]);
                                    objConsultaBE.participacion = objDRConsulta["PARTICIPACION"] == null || objDRConsulta["PARTICIPACION"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["PARTICIPACION"]);
                                    objConsultaBE.enero = objDRConsulta["1"] == null || objDRConsulta["1"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["1"]);
                                    objConsultaBE.febrero = objDRConsulta["2"] == null || objDRConsulta["2"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["2"]);
                                    objConsultaBE.marzo = objDRConsulta["3"] == null || objDRConsulta["3"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["3"]);
                                    objConsultaBE.abril = objDRConsulta["4"] == null || objDRConsulta["4"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["4"]);
                                    objConsultaBE.mayo = objDRConsulta["5"] == null || objDRConsulta["5"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["5"]);
                                    objConsultaBE.junio = objDRConsulta["6"] == null || objDRConsulta["6"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["6"]);
                                    objConsultaBE.julio = objDRConsulta["7"] == null || objDRConsulta["7"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["7"]);
                                    objConsultaBE.agosto = objDRConsulta["8"] == null || objDRConsulta["8"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["8"]);
                                    objConsultaBE.setiembre = objDRConsulta["9"] == null || objDRConsulta["9"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["9"]);
                                    objConsultaBE.octubre = objDRConsulta["10"] == null || objDRConsulta["10"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["10"]);
                                    objConsultaBE.noviembre = objDRConsulta["11"] == null || objDRConsulta["11"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["11"]);
                                    objConsultaBE.diciembre = objDRConsulta["12"] == null || objDRConsulta["12"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["12"]);
                                    objConsultaBE.totalFila = objConsultaBE.enero + objConsultaBE.febrero + objConsultaBE.marzo + objConsultaBE.abril + objConsultaBE.mayo + objConsultaBE.junio +
                                                     objConsultaBE.julio + objConsultaBE.agosto + objConsultaBE.setiembre + objConsultaBE.octubre + objConsultaBE.noviembre + objConsultaBE.diciembre;
                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REPORTE_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REPORTE_IMEI> ListaReporteEmpresaOperadora(BE_REPORTE_IMEI objetoParametro)
        {
            try
            {
                List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objConsultaBE = new BE_REPORTE_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_REQUERIMIENTO_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_anio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAnio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REPORTE_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    //objConsultaBE.idEmpresaEO = objDRConsulta["IDEMPRESAEO"] == null || objDRConsulta["IDEMPRESAEO"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["IDEMPRESAEO"]);
                                    objConsultaBE.descripcion = objDRConsulta["NOMEMPRESAEO"] == null ? "" : Convert.ToString(objDRConsulta["NOMEMPRESAEO"]);
                                    objConsultaBE.enero = objDRConsulta["1"] == null || objDRConsulta["1"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["1"]);
                                    objConsultaBE.febrero = objDRConsulta["2"] == null || objDRConsulta["2"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["2"]);
                                    objConsultaBE.marzo = objDRConsulta["3"] == null || objDRConsulta["3"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["3"]);
                                    objConsultaBE.abril = objDRConsulta["4"] == null || objDRConsulta["4"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["4"]);
                                    objConsultaBE.mayo = objDRConsulta["5"] == null || objDRConsulta["5"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["5"]);
                                    objConsultaBE.junio = objDRConsulta["6"] == null || objDRConsulta["6"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["6"]);
                                    objConsultaBE.julio = objDRConsulta["7"] == null || objDRConsulta["7"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["7"]);
                                    objConsultaBE.agosto = objDRConsulta["8"] == null || objDRConsulta["8"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["8"]);
                                    objConsultaBE.setiembre = objDRConsulta["9"] == null || objDRConsulta["9"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["9"]);
                                    objConsultaBE.octubre = objDRConsulta["10"] == null || objDRConsulta["10"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["10"]);
                                    objConsultaBE.noviembre = objDRConsulta["11"] == null || objDRConsulta["11"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["11"]);
                                    objConsultaBE.diciembre = objDRConsulta["12"] == null || objDRConsulta["12"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["12"]);
                                    objConsultaBE.totalFila = objConsultaBE.enero + objConsultaBE.febrero + objConsultaBE.marzo + objConsultaBE.abril + objConsultaBE.mayo + objConsultaBE.junio +
                                                     objConsultaBE.julio + objConsultaBE.agosto + objConsultaBE.setiembre + objConsultaBE.octubre + objConsultaBE.noviembre + objConsultaBE.diciembre;
                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REPORTE_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Listados de Estadisticos Gerenciales"

        public List<BE_REPORTE_IMEI> ListaPromedioAtencionEO(BE_REPORTE_IMEI objetoParametro)
        {
            try
            {
                List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objConsultaBE = new BE_REPORTE_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_PROMATENCION_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_anio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAnio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REPORTE_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    //objConsultaBE.idUbigeo = objDRConsulta["IDUBIGEO"] == null ? 0 : Convert.ToInt32(objDRConsulta["IDUBIGEO"]);
                                    objConsultaBE.descripcion = objDRConsulta["DESCRIPCION"] == null ? "" : Convert.ToString(objDRConsulta["DESCRIPCION"]);
                                    objConsultaBE.enero = objDRConsulta["1"] == null || objDRConsulta["1"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["1"]);
                                    objConsultaBE.febrero = objDRConsulta["2"] == null || objDRConsulta["2"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["2"]);
                                    objConsultaBE.marzo = objDRConsulta["3"] == null || objDRConsulta["3"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["3"]);
                                    objConsultaBE.abril = objDRConsulta["4"] == null || objDRConsulta["4"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["4"]);
                                    objConsultaBE.mayo = objDRConsulta["5"] == null || objDRConsulta["5"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["5"]);
                                    objConsultaBE.junio = objDRConsulta["6"] == null || objDRConsulta["6"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["6"]);
                                    objConsultaBE.julio = objDRConsulta["7"] == null || objDRConsulta["7"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["7"]);
                                    objConsultaBE.agosto = objDRConsulta["8"] == null || objDRConsulta["8"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["8"]);
                                    objConsultaBE.setiembre = objDRConsulta["9"] == null || objDRConsulta["9"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["9"]);
                                    objConsultaBE.octubre = objDRConsulta["10"] == null || objDRConsulta["10"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["10"]);
                                    objConsultaBE.noviembre = objDRConsulta["11"] == null || objDRConsulta["11"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["11"]);
                                    objConsultaBE.diciembre = objDRConsulta["12"] == null || objDRConsulta["12"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["12"]);
                                    objConsultaBE.totalFila = objConsultaBE.enero + objConsultaBE.febrero + objConsultaBE.marzo + objConsultaBE.abril + objConsultaBE.mayo + objConsultaBE.junio +
                                                     objConsultaBE.julio + objConsultaBE.agosto + objConsultaBE.setiembre + objConsultaBE.octubre + objConsultaBE.noviembre + objConsultaBE.diciembre;
                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REPORTE_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BE_REPORTE_IMEI> ListaPromedioAtencionOsiptel(BE_REPORTE_IMEI objetoParametro)
        {
            try
            {
                List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objConsultaBE = new BE_REPORTE_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_PROMATENCION_OSIPTEL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_anio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAnio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REPORTE_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    //objConsultaBE.idUbigeo = objDRConsulta["IDUBIGEO"] == null ? 0 : Convert.ToInt32(objDRConsulta["IDUBIGEO"]);
                                    objConsultaBE.descripcion = objDRConsulta["DESCRIPCION"] == null ? "" : Convert.ToString(objDRConsulta["DESCRIPCION"]);
                                    objConsultaBE.enero = objDRConsulta["1"] == null || objDRConsulta["1"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["1"]);
                                    objConsultaBE.febrero = objDRConsulta["2"] == null || objDRConsulta["2"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["2"]);
                                    objConsultaBE.marzo = objDRConsulta["3"] == null || objDRConsulta["3"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["3"]);
                                    objConsultaBE.abril = objDRConsulta["4"] == null || objDRConsulta["4"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["4"]);
                                    objConsultaBE.mayo = objDRConsulta["5"] == null || objDRConsulta["5"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["5"]);
                                    objConsultaBE.junio = objDRConsulta["6"] == null || objDRConsulta["6"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["6"]);
                                    objConsultaBE.julio = objDRConsulta["7"] == null || objDRConsulta["7"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["7"]);
                                    objConsultaBE.agosto = objDRConsulta["8"] == null || objDRConsulta["8"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["8"]);
                                    objConsultaBE.setiembre = objDRConsulta["9"] == null || objDRConsulta["9"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["9"]);
                                    objConsultaBE.octubre = objDRConsulta["10"] == null || objDRConsulta["10"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["10"]);
                                    objConsultaBE.noviembre = objDRConsulta["11"] == null || objDRConsulta["11"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["11"]);
                                    objConsultaBE.diciembre = objDRConsulta["12"] == null || objDRConsulta["12"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["12"]);
                                    objConsultaBE.totalFila = objConsultaBE.enero + objConsultaBE.febrero + objConsultaBE.marzo + objConsultaBE.abril + objConsultaBE.mayo + objConsultaBE.junio +
                                                     objConsultaBE.julio + objConsultaBE.agosto + objConsultaBE.setiembre + objConsultaBE.octubre + objConsultaBE.noviembre + objConsultaBE.diciembre;
                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REPORTE_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<BE_REPORTE_IMEI> ListaRatioDiasAtencionOsiptel(BE_REPORTE_IMEI objetoParametro)
        {
            try
            {
                List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objConsultaBE = new BE_REPORTE_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_RATIODIASATENCION_OSIPTEL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_anio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAnio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REPORTE_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    //objConsultaBE.idUbigeo = objDRConsulta["IDUBIGEO"] == null ? 0 : Convert.ToInt32(objDRConsulta["IDUBIGEO"]);
                                    objConsultaBE.descripcion = objDRConsulta["DESCRIPCION"] == null ? "" : Convert.ToString(objDRConsulta["DESCRIPCION"]);
                                    objConsultaBE.enero = objDRConsulta["1"] == null || objDRConsulta["1"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["1"]);
                                    objConsultaBE.febrero = objDRConsulta["2"] == null || objDRConsulta["2"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["2"]);
                                    objConsultaBE.marzo = objDRConsulta["3"] == null || objDRConsulta["3"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["3"]);
                                    objConsultaBE.abril = objDRConsulta["4"] == null || objDRConsulta["4"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["4"]);
                                    objConsultaBE.mayo = objDRConsulta["5"] == null || objDRConsulta["5"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["5"]);
                                    objConsultaBE.junio = objDRConsulta["6"] == null || objDRConsulta["6"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["6"]);
                                    objConsultaBE.julio = objDRConsulta["7"] == null || objDRConsulta["7"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["7"]);
                                    objConsultaBE.agosto = objDRConsulta["8"] == null || objDRConsulta["8"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["8"]);
                                    objConsultaBE.setiembre = objDRConsulta["9"] == null || objDRConsulta["9"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["9"]);
                                    objConsultaBE.octubre = objDRConsulta["10"] == null || objDRConsulta["10"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["10"]);
                                    objConsultaBE.noviembre = objDRConsulta["11"] == null || objDRConsulta["11"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["11"]);
                                    objConsultaBE.diciembre = objDRConsulta["12"] == null || objDRConsulta["12"].ToString() == "" ? 0 : Convert.ToInt32(objDRConsulta["12"]);
                                    objConsultaBE.totalFila = objConsultaBE.enero + objConsultaBE.febrero + objConsultaBE.marzo + objConsultaBE.abril + objConsultaBE.mayo + objConsultaBE.junio +
                                                     objConsultaBE.julio + objConsultaBE.agosto + objConsultaBE.setiembre + objConsultaBE.octubre + objConsultaBE.noviembre + objConsultaBE.diciembre;
                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REPORTE_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REPORTE_IMEI> ListaRatioPorcentajeAtencionOsiptel(BE_REPORTE_IMEI objetoParametro)
        {
            try
            {
                List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objConsultaBE = new BE_REPORTE_IMEI();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REPORTE.SP_REPORTE_RATIOPROCATENCION_OSIPTEL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_anio", OracleType.Number, ParameterDirection.Input, objetoParametro.idAnio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));

                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REPORTE_IMEI(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    //objConsultaBE.idUbigeo = objDRConsulta["IDUBIGEO"] == null ? 0 : Convert.ToInt32(objDRConsulta["IDUBIGEO"]);
                                    objConsultaBE.descripcion = objDRConsulta["DESCRIPCION"] == null ? "" : Convert.ToString(objDRConsulta["DESCRIPCION"]);
                                    objConsultaBE.enero = objDRConsulta["1"] == null || objDRConsulta["1"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["1"]);
                                    objConsultaBE.febrero = objDRConsulta["2"] == null || objDRConsulta["2"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["2"]);
                                    objConsultaBE.marzo = objDRConsulta["3"] == null || objDRConsulta["3"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["3"]);
                                    objConsultaBE.abril = objDRConsulta["4"] == null || objDRConsulta["4"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["4"]);
                                    objConsultaBE.mayo = objDRConsulta["5"] == null || objDRConsulta["5"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["5"]);
                                    objConsultaBE.junio = objDRConsulta["6"] == null || objDRConsulta["6"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["6"]);
                                    objConsultaBE.julio = objDRConsulta["7"] == null || objDRConsulta["7"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["7"]);
                                    objConsultaBE.agosto = objDRConsulta["8"] == null || objDRConsulta["8"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["8"]);
                                    objConsultaBE.setiembre = objDRConsulta["9"] == null || objDRConsulta["9"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["9"]);
                                    objConsultaBE.octubre = objDRConsulta["10"] == null || objDRConsulta["10"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["10"]);
                                    objConsultaBE.noviembre = objDRConsulta["11"] == null || objDRConsulta["11"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["11"]);
                                    objConsultaBE.diciembre = objDRConsulta["12"] == null || objDRConsulta["12"].ToString() == "" ? 0 : Convert.ToDecimal(objDRConsulta["12"]);
                                    objConsultaBE.totalFila = objConsultaBE.enero + objConsultaBE.febrero + objConsultaBE.marzo + objConsultaBE.abril + objConsultaBE.mayo + objConsultaBE.junio +
                                                     objConsultaBE.julio + objConsultaBE.agosto + objConsultaBE.setiembre + objConsultaBE.octubre + objConsultaBE.noviembre + objConsultaBE.diciembre;
                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REPORTE_IMEI>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open)
                            {
                                NewConexion.retClose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion



    }
}

