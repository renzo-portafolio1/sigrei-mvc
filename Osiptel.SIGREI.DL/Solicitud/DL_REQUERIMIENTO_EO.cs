﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_REQUERIMIENTO_EO
    {
        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_EO> ListaSolicitudEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_EO> listaResultado = new List<BE_REQUERIMIENTO_EO>();
                BE_REQUERIMIENTO_EO objConsultaBE = new BE_REQUERIMIENTO_EO(); ;
                
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_LISTAR_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEmpresaEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idEmpresaEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroRequerimientoEO", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NomEmpresaEO", OracleType.VarChar, ParameterDirection.Input, objetoParametro.empresaOperadora));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaInicio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaInicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_FechaFin", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaFin));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_EO(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimientoEO = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTOEO"]);
                                    objConsultaBE.nroRequerimientoEO = Convert.ToString(objDRConsulta["NROREQUERIMIENTOEO"]);
                                    objConsultaBE.idEmpresaEO = Convert.ToInt32(objDRConsulta["IDEMPRESAEO"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["NOMEMPRESAEO"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.fechaPlazo = Convert.ToString(objDRConsulta["FECPLAZO"]);
                                    objConsultaBE.usuarioAtendido = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtendido = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.cantidadRegistrado = Convert.ToInt32(objDRConsulta["CANTIDAD_IMEI"]);
                                    objConsultaBE.cantidadAtendido = Convert.ToInt32(objDRConsulta["CANTIDAD_ATENDIDO"]);
                                    objConsultaBE.cantidadPorAtender = Convert.ToInt32(objDRConsulta["CANTIDAD_ATENDER"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);                                    
                                    objConsultaBE.auditoria.usuMod = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.auditoria.fecMod = Convert.ToDateTime(objDRConsulta["FECHA"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_EO>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BE_REQUERIMIENTO_EO ConsultaSolicitudEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            try
            {
                Int32 numerador = 0;
                BE_REQUERIMIENTO_EO objConsultaBE = new BE_REQUERIMIENTO_EO(); ;
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_CONSULTAR_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                if (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_EO(); ;
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimientoEO = Convert.ToInt32(objDRConsulta["IDREQUERIMIENTOEO"]);
                                    objConsultaBE.nroRequerimientoEO = Convert.ToString(objDRConsulta["NROREQUERIMIENTOEO"]);
                                    objConsultaBE.idEmpresaEO = Convert.ToInt32(objDRConsulta["IDEMPRESAEO"]);
                                    objConsultaBE.empresaOperadora = Convert.ToString(objDRConsulta["NOMEMPRESAEO"]);
                                    objConsultaBE.fecRegistro = Convert.ToString(objDRConsulta["FECREGISTRO"]);
                                    objConsultaBE.fechaPlazo = Convert.ToString(objDRConsulta["FECPLAZO"]);
                                    objConsultaBE.cantidadRegistrado = Convert.ToInt32(objDRConsulta["CANTIDAD_IMEI"]);
                                    objConsultaBE.cantidadAtendido = Convert.ToInt32(objDRConsulta["CANTIDAD_ATENDIDO"]);
                                    objConsultaBE.cantidadPorAtender = Convert.ToInt32(objDRConsulta["CANTIDAD_ATENDER"]);
                                    objConsultaBE.usuarioAtendido = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtendido = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.auditoria.usuMod = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.auditoria.fecMod = Convert.ToDateTime(objDRConsulta["FECHA"]);
                                }
                                
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return objConsultaBE;
                        }
                        catch (Exception ex)
                        {
                            objConsultaBE = new BE_REQUERIMIENTO_EO();
                            return objConsultaBE;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BE_REQUERIMIENTO_IMEI_EO> ListaIMEI_EO(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI_EO> listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
                BE_REQUERIMIENTO_IMEI_EO objConsultaBE = new BE_REQUERIMIENTO_IMEI_EO();
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_LISTAR_IMEI_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI_EO();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idRequerimientoEO = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTOEO"]);
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.idImeiEO = Convert.ToInt64(objDRConsulta["IDIMEIEO"]);
                                    objConsultaBE.idImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.idTipoReporte = Convert.ToInt32(objDRConsulta["IDTIPOREPORTE"]);
                                    objConsultaBE.estadoImei = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECHAREPORTE"]);
                                    objConsultaBE.nroServicio = Convert.ToString(objDRConsulta["NROSERVICIO"]);
                                    objConsultaBE.nombreTitular = Convert.ToString(objDRConsulta["NOMBRETITULAR"]);
                                    objConsultaBE.apPaternoTitular = Convert.ToString(objDRConsulta["APPATERNOTITULAR"]);
                                    objConsultaBE.apMaternoTitular = Convert.ToString(objDRConsulta["APMATERNOTITULAR"]);
                                    objConsultaBE.nombreCompletoTitular= Convert.ToString(objDRConsulta["NOMBRECOMPLETOTITULAR"]);
                                    objConsultaBE.idTipoDocumento = Convert.ToString(objDRConsulta["IDTIPODOCUMENTO"]);
                                    objConsultaBE.descTipoDocumento = Convert.ToString(objDRConsulta["DESCDOCUMENTO"]);
                                    objConsultaBE.nroDocumento = Convert.ToString(objDRConsulta["NRODOCUMENTO"]);
                                    objConsultaBE.usuarioAtencion = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtencion = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.idTipoObservacion = Convert.ToInt32(DL_HELPER.getInt32(objDRConsulta,"IDTIPOMOTIVO"));
                                    objConsultaBE.descTipoObservacion = Convert.ToString(objDRConsulta["DESCTIPOMOTIVO"]);
                                    objConsultaBE.comentario = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.usuMod = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.fecMod = Convert.ToDateTime(objDRConsulta["FECHA"]);
                                    objConsultaBE.documentoAdjunto = Convert.ToString(objDRConsulta["DOCUMENTOADJUNTO"]);
                                    
                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
        #endregion

        #region "Transaccionales"

        public Int64 InsertarEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_INSERTAR_REQUERIMIENTO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idEmpresaEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idEmpresaEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.comentario));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimientoEO", OracleType.Number));
                  
                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimientoEO"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 AtenderEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_ATENDER_EO_SOLICITUD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_IDEmpresaEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idEmpresaEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Rechazar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 ActualizarVencimiento(BE_REQUERIMIENTO_EO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_ACTUALIZAR_VENCIMIENTO_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoVencimiento", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoMotivoVencimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_fecPlazo", OracleType.VarChar, ParameterDirection.Input, objetoParametro.fechaPlazo));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_idRequerimiento", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_idRequerimiento"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Actualizar Fecha de Vencimiento", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        #endregion

    }
}

