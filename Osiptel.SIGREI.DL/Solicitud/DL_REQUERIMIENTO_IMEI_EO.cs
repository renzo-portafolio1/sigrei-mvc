﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using Osiptel.SIGREI.BE;

namespace Osiptel.SIGREI.DL
{
    public class DL_REQUERIMIENTO_IMEI_EO
    {
        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_IMEI_EO> ListaIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            try
            {
                List<BE_REQUERIMIENTO_IMEI_EO> listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
                BE_REQUERIMIENTO_IMEI_EO objConsultaBE = new BE_REQUERIMIENTO_IMEI_EO();

                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_LISTAR_IMEI_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimiento));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_Listado", OracleType.Cursor));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        try
                        {
                            NewConexion.retOpen();
                            cmd.Connection = NewConexion.retConexion();
                            Int32 numerador = 0;

                            using (OracleDataReader objDRConsulta = cmd.ExecuteReader())
                            {
                                while (objDRConsulta.Read())
                                {
                                    objConsultaBE = new BE_REQUERIMIENTO_IMEI_EO();
                                    numerador++;

                                    objConsultaBE.nroItem = numerador;
                                    objConsultaBE.idImeiEO = Convert.ToInt64(objDRConsulta["IDIMEIEO"]);
                                    objConsultaBE.idRequerimientoEO = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTOEO"]);
                                    objConsultaBE.idRequerimiento = Convert.ToInt64(objDRConsulta["IDREQUERIMIENTO"]);
                                    objConsultaBE.idImei = Convert.ToInt64(objDRConsulta["IDIMEI"]);
                                    objConsultaBE.nroImei = Convert.ToString(objDRConsulta["NROIMEI"]);
                                    objConsultaBE.estadoImei = Convert.ToString(objDRConsulta["ESTADOIMEI"]);
                                    objConsultaBE.fecReporte = Convert.ToString(objDRConsulta["FECHAREPORTE"]);
                                    objConsultaBE.nroServicio = Convert.ToString(objDRConsulta["NROSERVICIO"]);
                                    objConsultaBE.nombreTitular = Convert.ToString(objDRConsulta["NOMBRETITULAR"]);
                                    objConsultaBE.apPaternoTitular = Convert.ToString(objDRConsulta["APPATERNOTITULAR"]);
                                    objConsultaBE.apMaternoTitular = Convert.ToString(objDRConsulta["APMATERNOTITULAR"]);
                                    objConsultaBE.nombreCompletoTitular = Convert.ToString(objDRConsulta["NOMBRECOMPLETOTITULAR"]);
                                    objConsultaBE.idTipoDocumento = Convert.ToString(objDRConsulta["IDTIPODOCUMENTO"]);
                                    objConsultaBE.descTipoDocumento = Convert.ToString(objDRConsulta["DESCDOCUMENTO"]);
                                    objConsultaBE.nroDocumento = Convert.ToString(objDRConsulta["NRODOCUMENTO"]);
                                    objConsultaBE.usuarioAtencion = Convert.ToString(objDRConsulta["USUATENCION"]);
                                    objConsultaBE.fechaAtencion = Convert.ToString(objDRConsulta["FECATENCION"]);
                                    objConsultaBE.idTipoObservacion = Convert.ToInt32(DL_HELPER.getInt32(objDRConsulta, "IDTIPOMOTIVO"));
                                    objConsultaBE.descTipoObservacion = Convert.ToString(objDRConsulta["DESCTIPOMOTIVO"]);
                                    objConsultaBE.observacion = Convert.ToString(objDRConsulta["COMENTARIO"]);
                                    objConsultaBE.idEstado = Convert.ToInt32(objDRConsulta["IDESTADO"]);
                                    objConsultaBE.descEstado = Convert.ToString(objDRConsulta["DESC_ESTADO"]);
                                    objConsultaBE.usuMod = Convert.ToString(objDRConsulta["USUARIO"]);
                                    objConsultaBE.fecMod = Convert.ToDateTime(objDRConsulta["FECHA"]);

                                    listaResultado.Add(objConsultaBE);

                                }
                                objDRConsulta.Close();
                            }
                            NewConexion.retClose();
                            return listaResultado;
                        }
                        catch (Exception ex)
                        {
                            listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
                            return listaResultado;
                            throw ex;
                        }
                        finally
                        {
                            if (NewConexion.conexion.State == ConnectionState.Open) NewConexion.retClose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 ActualizarIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_ACTUALIZAR_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImeiEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idImeiEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NroServicio", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroServicio));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_NombreTitular", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nombreTitular));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApPaterno", OracleType.VarChar, ParameterDirection.Input, objetoParametro.apPaternoTitular));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_ApMaterno", OracleType.VarChar, ParameterDirection.Input, objetoParametro.apMaternoTitular));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoDocumento", OracleType.VarChar, ParameterDirection.Input, objetoParametro.idTipoDocumento));                    
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroDocumento", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroDocumento));                    
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImeiEO", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImeiEO"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Aprobar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 InsertarIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_INSERTAR_IMEI";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImeiEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idImeiEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_nroImei", OracleType.VarChar, ParameterDirection.Input, objetoParametro.nroImei));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuCre", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuCre));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImei", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImei"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Insertar Reporte Consulta", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 ActualizarEstadoIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_ACTUALIZAR_ESTADOIMEI_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImeiEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idImeiEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoMotivo", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoObservacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.observacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idEstado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImeiEO", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImeiEO"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Aprobar la solicitud", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 DescartarIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_DESCARTAR_IMEI_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImeiEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idImeiEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoMotivo", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoObservacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.observacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idAdjunto", OracleType.Number, ParameterDirection.Input, objetoParametro.idAdjunto));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImeiEO", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImeiEO"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Descartar IMEI", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        public Int64 AtenderIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 idResultado = 0;
            try
            {
                using (OracleCommand cmd = new OracleCommand())
                {
                    cmd.CommandText = "PKG_REQUERIMIENTO_EO.SP_ATENDER_IMEI_EO";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idRequerimientoEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idRequerimientoEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idImeiEO", OracleType.Number, ParameterDirection.Input, objetoParametro.idImeiEO));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_idTipoMotivo", OracleType.Number, ParameterDirection.Input, objetoParametro.idTipoObservacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Comentario", OracleType.VarChar, ParameterDirection.Input, objetoParametro.observacion));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_Estado", OracleType.Number, ParameterDirection.Input, objetoParametro.idEstado));
                    cmd.Parameters.Add(DL_HELPER.getParam("pi_UsuMod", OracleType.VarChar, ParameterDirection.Input, objetoParametro.usuMod));
                    cmd.Parameters.Add(DL_HELPER.getParam("po_IDImeiEO", OracleType.Number));

                    using (DL_CONEXION NewConexion = new DL_CONEXION())
                    {
                        int ret = NewConexion.ejecutaSQL(cmd);
                        Int64 pvalor = Convert.ToInt64(cmd.Parameters["po_IDImeiEO"].Value);
                        idResultado = pvalor;
                    }
                }
                return idResultado;

            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("Atender IMEI", ex.ToString(), objetoParametro.usuMod);
                throw ex;
            }
        }

        #endregion

    }
}

