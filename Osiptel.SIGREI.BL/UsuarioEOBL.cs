﻿
using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;


namespace Osiptel.SIGREI.BL 
{
    public class UsuarioEOBL
    {
        //public List<TicketBE> SP_CONSULTAR_TICKET_EP(TicketBE P_TicketBE)
        //{
        //    return new TicketDL().SP_CONSULTAR_TICKET_EP(P_TicketBE);
        //}

        public List<UsuarioEOBE> SP_CONSULTAR_TICKET_EP(UsuarioEOBE P_UsuarioEOBE)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            //pFiltroBusquedaMesaPartesBE.Total = 0;
            List<UsuarioEOBE> LISTA;

            try
            {
                //if (pFiltroBusquedaMesaPartesBE.EsBusqueda)
                //    lstExpedientes = objDatos.ObtenerListarExpedientesMesaPartes(pFiltroBusquedaMesaPartesBE);
                //else
                //    lstExpedientes = new List<ExpedienteBE>();
                LISTA = UsuEODL.SP_LISTAR_USUARIO_EO_X_APLIC(ref P_UsuarioEOBE);

                //return new PaginacionBE<TicketBE>(pFiltroBusquedaMesaPartesBE.Pagina, pFiltroBusquedaMesaPartesBE.RegistrosxPagina, pFiltroBusquedaMesaPartesBE.Total, LISTA);
                return LISTA;// new PaginacionBE<UsuarioEOBE>(P_UsuarioEOBE.Pagina, P_UsuarioEOBE.RegistrosxPagina, P_UsuarioEOBE.Total, LISTA);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SP_LISTAR_USUARIO_EO_X_APLIC_EXCEL(UsuarioEOBE P_UsuarioEOBE)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();


            try
            {
             
                return UsuEODL.SP_LISTAR_USUARIO_EO_X_APLIC_EXCEL(ref P_UsuarioEOBE);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PerfilBE> SP_LISTAR_PERFIL(string TIPO_VACIO, string PERFIL)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            List<PerfilBE> LISTA;
            PerfilBE PerfilBE = new PerfilBE();
            try
            {
                LISTA = UsuEODL.SP_LISTAR_PERFIL(PERFIL);
                //if (TIPO_VACIO.CompareTo("S") == 0)
                //{
                //    PerfilBE.IDPERFIL = "S";
                //    PerfilBE.DESCRIPCION = "SELECCION";
                //    LISTA.Insert(0, PerfilBE);
                //}
                //else if (TIPO_VACIO.CompareTo("T") == 0)
                //{
                //    PerfilBE.IDPERFIL = "T";
                //    PerfilBE.DESCRIPCION = "TODOS";
                //    LISTA.Insert(0, PerfilBE);
                //}

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return LISTA;
        }

        public string SP_INSERTAR_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            try
            {
                return UsuEODL.SP_INSERTAR_USUARIO_EO(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public UsuarioEOBE SP_OBTENER_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            try
            {
                return UsuEODL.SP_OBTENER_USUARIO_EO(ref UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SP_ACTUALIZAR_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            try
            {
                return UsuEODL.SP_ACTUALIZAR_USUARIO_EO(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SP_EXISTE_PERFIL_USUARIO(UsuarioEOBE UsuarioEOBE)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            try
            {
                return UsuEODL.SP_EXISTE_PERFIL_USUARIO(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SP_CAMBIAR_ESTADO_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            try
            {
                GenericoBE objGenericoBE = new GenericoBE();
                UsuarioEOBE.IDESTADO = objGenericoBE.ESTADO_CONTRARIO(UsuarioEOBE.IDESTADO);
                return UsuEODL.SP_CAMBIAR_ESTADO_USUARIO_EO(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SP_CAMBIAR_CLAVE_USUARIO_EO(UsuarioEOBE UsuarioEOBE)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            try
            {
                GenericoBE GenericoBE = new GenericoBE();
                return UsuEODL.SP_CAMBIAR_CLAVE_USUARIO_EO(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SP_VALIDAR_USUARIO_EO(UsuarioEOBE UsuarioEOBE, string TIPOBUSKDA)
        {
            UsuarioEODL UsuEODL = new UsuarioEODL();
            try
            {                
                return UsuEODL.SP_VALIDAR_USUARIO_EO(UsuarioEOBE, TIPOBUSKDA);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public string SP_CONSULTAR_TICKET_EP_EXCEL(TicketBE P_TicketBE)
        //{
        //    try
        //    {
        //        TicketDL TicketDL = new TicketDL();
        //        return TicketDL.SP_CONSULTAR_TICKET_EP_EXCEL(P_TicketBE);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

    }
}
