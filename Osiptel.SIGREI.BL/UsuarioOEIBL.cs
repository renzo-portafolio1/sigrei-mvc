﻿
using System;

using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;
using Osiptel.SIGREI.BL;


namespace Osiptel.SIGREI.BL 
{
    public class UsuarioOEIBL
    {
        public UsuarioOEIBE SP_OBTENER_USUARIO_OEI(UsuarioOEIBE P_UsuOEIBE)
        {
            UsuarioOEIDL UsuOEIDL = new UsuarioOEIDL();
            try
            {
                return UsuOEIDL.SP_OBTENER_USUARIO_OEI(ref P_UsuOEIBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SP_VALIDAR_USUARIO_OEI(UsuarioOEIBE P_UsuOEIBE, string TIPOBUSKDA)
        {
            UsuarioOEIDL UsuOEIDL = new UsuarioOEIDL();
            try
            {
                GenericoBE GenericoBE = new GenericoBE();
                return UsuOEIDL.SP_VALIDAR_USUARIO_OEI(P_UsuOEIBE, TIPOBUSKDA);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SP_VALIDAR_USUARIO_IE(BeUsuarioEP P_UsuOEIBE)
        {
            UsuarioOEIDL UsuOEIDL = new UsuarioOEIDL();
            try
            {
                GenericoBE GenericoBE = new GenericoBE();
                return UsuOEIDL.SP_VALIDAR_USUARIO_IE(P_UsuOEIBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BeUsuarioEP SP_OBTENER_USUARIO_IE(BeUsuarioEP P_UsuOEIBE)
        {
            UsuarioOEIDL UsuOEIDL = new UsuarioOEIDL();
            try
            {
                return UsuOEIDL.SP_OBTENER_USUARIO_IE(ref P_UsuOEIBE);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
