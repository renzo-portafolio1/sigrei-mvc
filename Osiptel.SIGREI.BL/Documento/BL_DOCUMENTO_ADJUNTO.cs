﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;

using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_DOCUMENTO_ADJUNTO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_DOCUMENTO_ADJUNTO objData = new DL_DOCUMENTO_ADJUNTO();

        #endregion

        #region "No Transaccionales"

        //public List<BE_ALERTA_CONSULTA> ListarBuzon(BE_ALERTA_CONSULTA objetoParametro)
        //{
        //    return objData.ListarBuzon(objetoParametro);
        //}

        //public BE_ALERTA_CONSULTA ConsultarBuzon(BE_ALERTA_CONSULTA objetoParametro)
        //{
        //    return objData.ConsultarBuzon(objetoParametro);
        //}

        //public DataTable ListarBuzonDt(BE_ALERTA_CONSULTA objetoParametro)
        //{
        //    return objData.ListarBuzonDt(objetoParametro);
        //}

        #endregion

        #region "Transaccionales"

        public Int64 Insertar(BE_DOCUMENTO_ADJUNTO objeto)
        {
            Int64 objResultado = objData.Insertar(objeto);
            return objResultado;
        }

        //public Int64 Modificar(BE_ALERTA_CONSULTA objeto)
        //{
        //    Int64 objResultado = objData.Modificar(objeto);
        //    return objResultado;
        //}

        //public Int64 Modificar_Estado(BE_ALERTA_CONSULTA objeto)
        //{
        //    Int64 objResultado = objData.Modificar_Estado(objeto);
        //    return objResultado;
        //}

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion
    }
}
