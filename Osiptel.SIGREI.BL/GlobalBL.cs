﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class GlobalBL : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

            public String Obtener(String pAplicacion, String pGlobal)
            {
                GlobalDL objGlobalDALC = new GlobalDL();
                try
                {
                    return objGlobalDALC.Obtener(pAplicacion, pGlobal);
                }catch (Exception ex){
                    throw ex;
                }finally{
                    objGlobalDALC = null;
                }
            }

        #region IDisposable Members

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            private void Dispose(bool disposing)
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        component.Dispose();
                    }
                    disposed = true;

                }
            }

        #endregion
    }
}
