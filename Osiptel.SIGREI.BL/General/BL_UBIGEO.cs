﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_UBIGEO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"

        public List<BE_UBIGEO> Listar(string pUbigeo)
        {
            DL_UBIGEO objParametroDL = new DL_UBIGEO();
            BE_UBIGEO pFiltros = new BE_UBIGEO();
            pFiltros.idUbigeo= pUbigeo;
            List<BE_UBIGEO> lista;

            try
            {
                lista = objParametroDL.Listar(pFiltros);
             //   lista.Insert(0, new BE_UBIGEO() { idUbigeo = pInicialVal, descUbigeo = pInicialText });
                
                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
        
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
