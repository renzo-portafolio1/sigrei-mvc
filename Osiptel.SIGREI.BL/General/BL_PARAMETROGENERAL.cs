﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Osiptel.SIGREI.DL;
using Osiptel.SIGREI.BE;
using System.Collections;
using System.Web;

namespace Osiptel.SIGREI.BL
{
    public class BL_PARAMETROGENERAL
    {
        public List<BE_PARAMETROGENERAL> ListarParametro(BE_PARAMETROGENERAL objObjeto)
        {
            return new DL_PARAMETROGENERAL().listaParametro(objObjeto);
        }

        public List<BE_VARIABLEPARAMETRO> obtenerValoresParametroDocumento(BE_REQUERIMIENTO_IMEI objObjeto)
        {
            return new DL_PARAMETROGENERAL().obtenerValoresParametroDocumento(objObjeto);
        }
        public BE_VARIABLEPARAMETRO valoresNotificarSigrei(BE_VARIABLEPARAMETRO objObjeto)
        {
            return new DL_PARAMETROGENERAL().valoresNotificarSigrei(objObjeto);
        }
        public BE_VARIABLEPARAMETRO valoresNotificarSigreiMovil(BE_VARIABLEPARAMETRO objObjeto)
        {
            return new DL_PARAMETROGENERAL().valoresNotificarSigreiMovil(objObjeto);
        }
    }
}
