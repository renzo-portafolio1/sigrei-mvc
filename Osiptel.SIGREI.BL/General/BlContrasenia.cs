﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;


namespace Osiptel.SIGREI.BL
{
    public class BlContrasenia : IDisposable
    {
        private Component component = new Component();
        private bool disposed = false;
        public string SP_ACTUALIZAR_CONTRASENIA_UEP(BeContrasenia UsuarioEOBE)
        {
            DlContrasenia UsuEODL = new DlContrasenia();
            try
            {
                return UsuEODL.SP_ACTUALIZAR_CONTRASENIA_UEP(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }
    }
}
