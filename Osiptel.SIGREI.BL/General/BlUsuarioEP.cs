﻿using System;
using System.Collections.Generic;

using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BlUsuarioEP
    {
        /* public string SP_INSERTAR_USUARIO_ENTIDAD_PUBLICA(BeUsuarioEP UsuarioEP)
         {
             DlUsuarioEP UnidadEPDL = new DlUsuarioEP();
             try
             {
                 return UnidadEPDL.SP_INSERTAR_USUARIO_ENTIDAD_PUBLICA(UsuarioEP);
             }
             catch (Exception ex)
             {
                 throw new Exception("Mensaje de Error: ", ex);
             }
         }*/
        public BeUsuarioEP SP_VALIDAR_USUARIO_EP_BY_DNI(string dni)
        {
            DlUsuarioEP ParametroEPBE = new DlUsuarioEP();
            BeUsuarioEP LISTA;
            try
            {
                LISTA = ParametroEPBE.SP_VALIDAR_USUARIO_EP_BY_DNI(new BeUsuarioEP()
                {
                    NUM_DOCIDE = dni
                });

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        /*public string SP_REGISTRAR_MASIVO_USUARIO_EP(BeUsuarioEP UsuarioEP)
        {
            DlUsuarioEP UnidadEPDL = new DlUsuarioEP();
            try
            {
                return UnidadEPDL.SP_REGISTRAR_MASIVO_USUARIO_EP(UsuarioEP);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }*/
        public List<BeUsuarioEP> SP_REGISTRAR_MASIVO_USUARIO_EP(BeUsuarioEP UsuarioEP)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_REGISTRAR_MASIVO_USUARIO_EP(UsuarioEP);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }
        public List<BeUsuarioEP> ActualizarDatosTemporalUser(string dni,string DES_APEMAT,string DES_NOMBRE,string DES_NOmBRE_LAST,
            string DES_TLFFIJ,string COD_DOCIDE,string DES_TLFMVL,string NUM_DOCIDE,string DIRECCION,string DES_APEPAT,string DES_CORELE,string IND_SEXO,string CARGO)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.ActualizarDatosTemporalUser(dni, DES_APEMAT, DES_NOMBRE, DES_NOmBRE_LAST,
                      DES_TLFFIJ,  COD_DOCIDE,  DES_TLFMVL,  NUM_DOCIDE,  DIRECCION,  DES_APEPAT,  DES_CORELE,  IND_SEXO,CARGO);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUsuarioEP> EliminarDatosTemporalUser(string dni)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.EliminarDatosTemporalUser(dni);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public BeUsuarioEP SP_OBTENER_USUARIO_EP_TEMPORAL(string dni)
        {
            DlUsuarioEP ParametroEPBE = new DlUsuarioEP();
            BeUsuarioEP LISTA;
            try
            {
                LISTA = ParametroEPBE.SP_OBTENER_USUARIO_EP_TEMPORAL(new BeUsuarioEP()
                {
                    NUM_DOCIDE = dni
                });

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public BeUsuarioEP SP_OBTENER_USUARIO_EP(string cPerCodigo)
        {
            DlUsuarioEP ParametroEPBE = new DlUsuarioEP();
            BeUsuarioEP LISTA;
            try
            {
                LISTA = ParametroEPBE.SP_OBTENER_USUARIO_EP(new BeUsuarioEP()
                {
                    IDUSUARIO = cPerCodigo
                });

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public string SP_ACTUALIZAR_VIGENCIA_USUARIO(BeUsuarioEP UsuarioEP)
        {
            DlUsuarioEP UnidadEPDL = new DlUsuarioEP();
            try
            {
                return UnidadEPDL.SP_ACTUALIZAR_VIGENCIA_USUARIO(UsuarioEP);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public string InsertarDatosTemporalUser(string cadena_tip_doc, string cadena_num_doc, string cadena_num_tlf,string cadena_num_cel, string cadena_tip_sexo,string cadena_correo,
                                                string cadena_prim_nombre, string cadena_sec_nombre, string cadena_pri_apellido,string  cadena_sec_apellido,string cadena_sec_direccion,
                                                string cadena_cargo,string cadena_contrasenia,string cadena_contrasenia_encry)
        {
            DlUsuarioEP UnidadEPDL = new DlUsuarioEP();
            try
            {
                return UnidadEPDL.InsertarDatosTemporalUser(cadena_tip_doc, cadena_num_doc, cadena_num_tlf, cadena_num_cel, cadena_tip_sexo, cadena_correo,
                            cadena_prim_nombre, cadena_sec_nombre, cadena_pri_apellido, cadena_sec_apellido, cadena_sec_direccion, cadena_cargo, cadena_contrasenia, cadena_contrasenia_encry);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BeUsuarioEP> SP_INSERTAR_USUARIO_ENTIDAD_PUBLICA(BeUsuarioEP obj)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_INSERTAR_USUARIO_ENTIDAD_PUBLICA(obj);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUsuarioEP> SP_ACTUALIZAR_USUARIO_ENTIDAD_PUBLICA(BeUsuarioEP obj)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_ACTUALIZAR_USUARIO_ENTIDAD_PUBLICA(obj);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }


        

        public List<BeUsuarioEP> ListarUsuariosEP(BeUsuarioEP obj)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.ListarUsuariosEP(obj);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUsuarioEP> SP_VALIDAR_DATOS_USER_AFTER(string usuario,string contrasenia)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_VALIDAR_DATOS_USER_AFTER(usuario, contrasenia);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUsuarioEP> SP_VALIDAR_CAMBIO_CONTRASENIA(string usuario)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_VALIDAR_CAMBIO_CONTRASENIA(usuario);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUsuarioEP> SP_VALIDAR_CORREO_USER_EP(string correo,string tip_documento,string usuario)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_VALIDAR_CORREO_USER_EP(correo,tip_documento,usuario);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUsuarioEP> SP_VALIDAR_CORREO_CAMBIO_CLAVE_USER_EP(string correo,string tip_doc,string num_doc)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_VALIDAR_CORREO_CAMBIO_CLAVE_USER_EP(correo,tip_doc,num_doc);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUsuarioEP> SP_GENERAR_NUEVO_ENLACE_UEP(string correo)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_GENERAR_NUEVO_ENLACE_UEP(correo);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUsuarioEP> SP_VALIDAR_EXPIRACION_CONTRASENIA_ENLACE(string usuario)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_VALIDAR_EXPIRACION_CONTRASENIA_ENLACE(usuario);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }
        public List<BeUsuarioEP> SP_GENERAR_NUEVO_CLAVE_TEMPORAL_UEP(string idusuario,string contra_enc,string contra)
        {
            List<BeUsuarioEP> lista;
            DlUsuarioEP objUnidadEPDL = new DlUsuarioEP();

            try
            {
                lista = objUnidadEPDL.SP_GENERAR_NUEVO_CLAVE_TEMPORAL_UEP(idusuario, contra_enc, contra);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }
    }
}
