﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_ABONADO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"

        public List<BE_ABONADO> BuscarAbonados(ref BE_PAGINACION objPaginacion, BE_ABONADO objParametro)
        {
            DL_ABONADO objParametroDL = new DL_ABONADO();

            List<BE_ABONADO> lista;

            try
            {
                lista = objParametroDL.BuscarAbonados(ref objPaginacion, objParametro);


                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }


        public List<BE_ABONADO> BuscarListaNegra(ref BE_PAGINACION objPaginacion, BE_ABONADO objParametro)
        {
            DL_ABONADO objParametroDL = new DL_ABONADO();

            List<BE_ABONADO> lista;

            try
            {
                lista = objParametroDL.BuscarListaNegra(ref objPaginacion, objParametro);


                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }

        public DataTable ExportarAbonados_DT(BE_ABONADO objParametro)
        {
            DL_ABONADO objParametroDL = new DL_ABONADO();

            DataTable lista;

            try
            {
                lista = objParametroDL.ExportarAbonados_DT(objParametro);


                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
        public DataTable ExportarListaNegra_DT(BE_ABONADO objParametro)
        {
            DL_ABONADO objParametroDL = new DL_ABONADO();

            DataTable lista;

            try
            {
                lista = objParametroDL.ExportarListaNegra_DT(objParametro);


                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
