﻿using System;
using System.Collections.Generic;

using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;


namespace Osiptel.SIGREI.BL
{
    public class BlPerfil : IDisposable
    {
        private readonly Component component = new Component();
        private bool disposed = false;

        public string SP_INSERTAR_PERFIL(BePerfil UsuarioEOBE)
        {
            DlPerfil UsuEODL = new DlPerfil();
            try
            {
                return UsuEODL.SP_INSERTAR_PERFIL(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public string SP_ACTUALIZAR_PERFIL(BePerfil UsuarioEOBE)
        {
            DlPerfil UsuEODL = new DlPerfil();
            try
            {
                return UsuEODL.SP_ACTUALIZAR_PERFIL(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }


        public BePerfil SP_OBTENER_PERFIL_BY_CODIGO(int pvalor,string descripcion)
        {
            DlPerfil ParametroEPBE = new DlPerfil();
            BePerfil LISTA;
            try
            {
                LISTA = ParametroEPBE.SP_OBTENER_PERFIL_BY_CODIGO(new BePerfil()
                {
                    idPerfil = pvalor,
                    descripcion = descripcion
                });

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BePerfil> ValidarAsignacionPerfilMod(string pAplicacion,int codigo,int modulo)
        {
            List<BePerfil> lista;
            DlPerfil objParametroDL = new DlPerfil();
            lista = objParametroDL.ValidarAsignacionPerfilMod(new BePerfil()
            {
                descripcion = pAplicacion,
                idPerfil = codigo,
                idModulo = modulo
            });



            return lista;
        }
        public List<BePerfil> ListarPerfil(string pAplicacion)
        {
            List<BePerfil> lista;
            DlPerfil objParametroDL = new DlPerfil();
            lista = objParametroDL.ListarPerfilByTodo(new BePerfil()
            {
                descripcion = pAplicacion
            });

           

            return lista;
        }

        public List<BePerfil> ValidarPerfilDuplicado(string pAplicacion,string descripcion, int codigo)
        {
            List<BePerfil> lista;
            DlPerfil objParametroDL = new DlPerfil();
            lista = objParametroDL.ValidarPerfilDuplicado(new BePerfil()
            {
                aplicacion = pAplicacion,
                descripcion = descripcion,
                idPerfil = codigo
            });



            return lista;
        }
        public List<BePerfil> ListarPerfilByEstado_filtro(string aplicacion)
        {
            DlPerfil objParametroDL = new DlPerfil();

            try
            {
                List<BePerfil> lista;
                lista = objParametroDL.ListarPerfilByEstado_filtro(new BePerfil()
                {
                    descripcion = aplicacion
                });

                return lista;
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }


        public List<BePerfil> ListarPerfilByEstado(string aplicacion)
        {
            DlPerfil objParametroDL = new DlPerfil();

            try
            {
                List<BePerfil> lista;
                lista = objParametroDL.ListarPerfilByEstado(new BePerfil()
                {
                    descripcion = aplicacion
                });

                return lista;
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }

        public List<BePerfil> ListarPerfilByUsuario(string idUsuario,string aplicacion)
        {
            DlPerfil objParametroDL = new DlPerfil();

            try
            {
                List<BePerfil> lista;
                lista = objParametroDL.ListarPerfilByUsuario(new BePerfil()
                {
                    descripcion = aplicacion,
                    usuCre = idUsuario
                });

                return lista;
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }


    }
}
