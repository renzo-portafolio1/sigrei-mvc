﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_GSMA : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"

        public List<BE_GSMA> Listar(ref BE_PAGINACION objPaginacion, BE_GSMA objParametro)
        {
            DL_GSMA objParametroDL = new DL_GSMA();

            List<BE_GSMA> lista;

            try
            {
                lista = objParametroDL.Listar(ref objPaginacion, objParametro);


                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }

        public DataTable ListarConsultaGSMA_DT(BE_GSMA objParametro)
        {
            DL_GSMA objParametroDL = new DL_GSMA();

            DataTable lista;

            try
            {
                lista = objParametroDL.ListarConsultaGSMA_DT(objParametro);


                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
