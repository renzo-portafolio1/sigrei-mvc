﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BlParametro : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"


        public string SP_INSERTAR_PARAMETRO(BE_PARAMETRO UsuarioEOBE)
        {
            DlParametro UsuEODL = new DlParametro();
            try
            {
                return UsuEODL.SP_INSERTAR_PARAMETRO(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public string SP_ACTUALIZAR_PARAMETRO(BE_PARAMETRO UsuarioEOBE)
        {
            DlParametro UsuEODL = new DlParametro();
            try
            {
                return UsuEODL.SP_ACTUALIZAR_PARAMETRO(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BE_PARAMETRO> ListarProByDepartamento(string codDepartamento)
        {

            List<BE_PARAMETRO> lista;
            DlParametro objUnidadEPDL = new DlParametro();

            try
            {
                lista = objUnidadEPDL.ListarProByDepartamento(codDepartamento);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;


        }

        public List<BE_PARAMETRO> ListarDisByProvincia(string codProvincia)
        {

            List<BE_PARAMETRO> lista;
            DlParametro objUnidadEPDL = new DlParametro();

            try
            {
                lista = objUnidadEPDL.ListarDisByProvincia(codProvincia);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;


        }

        public List<BE_PARAMETRO> ValidarDuplicadoParametro(string pAplicacion, string pTabla, string pColumna,string descripcion,string valor)
        {

            List<BE_PARAMETRO> lista;
            DlParametro objUnidadEPDL = new DlParametro();

            try
            {
                lista = objUnidadEPDL.ValidarDuplicadoParametro(new BE_PARAMETRO()
                {
                    aplicacion = pAplicacion,
                    tabla = pTabla,
                    columna = pColumna,
                    descripcion = descripcion,
                    valor = valor
                });
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;

          
        }

        public string Obtener_Codigo_Parametro_By_Registro(string pAplicacion, string pTabla, string pColumna)
        {
            DlParametro ParametroEPBE = new DlParametro();
            try
            {
                return ParametroEPBE.Obtener_Codigo_Parametro_By_Registro(new BE_PARAMETRO()
                {
                    aplicacion = pAplicacion,
                    tabla = pTabla,
                    columna = pColumna
                });
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public BE_PARAMETRO SP_OBTENER_PARAMETRO_BY_CODIGO(string pvalor,string pAplicacion, string pTabla, string pColumna)
        {
            DlParametro ParametroEPBE = new DlParametro();
            BE_PARAMETRO LISTA;
            try
            {
                LISTA =  ParametroEPBE.SP_OBTENER_PARAMETRO_BY_CODIGO(new BE_PARAMETRO()
                {
                    valor = pvalor,
                    aplicacion = pAplicacion,
                    tabla = pTabla,
                    columna = pColumna
                });

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }


       

        public List<BE_PARAMETRO> SP_PARAMETRO_GET_DATOS(BE_PARAMETRO P_UsuarioEOBE)
        {
            DlParametro UsuEODL = new DlParametro();
            List<BE_PARAMETRO> LISTA;

            try
            {

                LISTA = UsuEODL.SP_PARAMETRO_GET_DATOS(ref P_UsuarioEOBE);

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public List<BE_PARAMETRO> ListarEntidadPByUsuario(string idUsuario, string pAplicacion, string pTabla, string pColumna)
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarEntidadPByUsuario(new BE_PARAMETRO()
            {
                usuCre = idUsuario,
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });



            return lista;
        }
        public List<BE_PARAMETRO> ListarModuloByUsuario(string idUsuario, string pAplicacion, string pTabla, string pColumna)
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarModuloByUsuario(new BE_PARAMETRO()
            {
                usuCre = idUsuario,
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });



            return lista;
        }
        
        public List<BE_PARAMETRO> ListarValoresTipDocumento(string pAplicacion, string pTabla, string pColumna)
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarValoresTipDocumento(new BE_PARAMETRO()
            {
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });



            return lista;
        }

        public List<BE_PARAMETRO> ListarValores_sexo(string pAplicacion, string pTabla, string pColumna)
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarValores_sexo(new BE_PARAMETRO()
            {
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });



            return lista;
        }

        public List<BE_PARAMETRO> ListarValores(string pAplicacion, string pTabla, string pColumna )
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarValores(new BE_PARAMETRO()
            {
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });

           

            return lista;
        }

        public List<BE_PARAMETRO> ListarValoresContrasenia(string pAplicacion, string pTabla, string pColumna)
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarValoresContrasenia(new BE_PARAMETRO()
            {
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });



            return lista;
        }

        public List<BE_PARAMETRO> ListarModuloByPerfil(string nPerfil,int nEstado)
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarModuloByPerfil(nPerfil,nEstado);



            return lista;
        }

        public List<BE_PARAMETRO> ListarValores_By_Todo(string pAplicacion, string pTabla, string pColumna)
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarValores_By_Todo(new BE_PARAMETRO()
            {
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });

           
            return lista;
        }

        public List<BE_PARAMETRO> ListarValores_By_Todo_Global(string pAplicacion, string pTabla, string pColumna)
        {
            List<BE_PARAMETRO> lista;
            DlParametro objParametroDL = new DlParametro();
            lista = objParametroDL.ListarValores_By_Todo_Global(new BE_PARAMETRO()
            {
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });


            return lista;
        }


        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
