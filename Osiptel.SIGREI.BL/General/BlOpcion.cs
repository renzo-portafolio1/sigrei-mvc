﻿using System;
using System.Collections.Generic;

using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;


namespace Osiptel.SIGREI.BL
{
    public class BlOpcion
    {

        public List<BeAccesoOpcion> ValidarMenuDuplicado(string aplicacion,string descripcion, int idModulo)
        {
            List<BeAccesoOpcion> lista;
            DlOpcion objUnidadEPDL = new DlOpcion();

            try
            {
                lista = objUnidadEPDL.ValidarMenuDuplicado(aplicacion, descripcion, idModulo);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeModuloOpcion> ListarModulo_Opcion_By_Modulo(string aplicacion,int idModulo)
        {
            List<BeModuloOpcion> lista;
            DlOpcion objUnidadEPDL = new DlOpcion();

            try
            {
                lista = objUnidadEPDL.ListarModulo_Opcion_By_Modulo(aplicacion, idModulo);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BePerfilModuloOpcion> ListarModulo_Opcion_By_Modulo_By_Perfil(string aplicacion,int idPerfil, int idModulo)
        {
            List<BePerfilModuloOpcion> lista;
            DlOpcion objUnidadEPDL = new DlOpcion();

            try
            {
                lista = objUnidadEPDL.ListarModulo_Opcion_By_Modulo_By_Perfil(aplicacion, idPerfil, idModulo);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeAccesoOpcion> ListarOpcionByTodo(string aplicacion)
        {
            List<BeAccesoOpcion> lista;
            DlOpcion objUnidadEPDL = new DlOpcion();

            try
            {
                lista = objUnidadEPDL.ListarOpcionByTodo(aplicacion);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public BeAccesoOpcion SP_OBTENER_MENU_BY_CODIGO(string pvalor, string pAplicacion)
        {
            DlOpcion ParametroEPBE = new DlOpcion();
            BeAccesoOpcion LISTA;
            try
            {
                LISTA = ParametroEPBE.SP_OBTENER_MENU_BY_CODIGO(new BeAccesoOpcion()
                {
                    IDOPCION = pvalor,
                    APLICACION = pAplicacion
                });

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }


        public List<BeAccesoOpcion> ListarMenuPrincipales(string aplicacion)
        {
            List<BeAccesoOpcion> lista;
            DlOpcion objUnidadEPDL = new DlOpcion();

            try
            {
                lista = objUnidadEPDL.ListarMenuPrincipales(aplicacion);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public string SP_INSERTAR_MENU(BeAccesoOpcion UsuarioEOBE)
        {
            DlOpcion UsuEODL = new DlOpcion();
            try
            {
                return UsuEODL.SP_INSERTAR_MENU(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public string SP_ACTUALIZAR_MENU(BeAccesoOpcion UsuarioEOBE)
        {
            DlOpcion UsuEODL = new DlOpcion();
            try
            {
                return UsuEODL.SP_ACTUALIZAR_MENU(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public string Eliminar_Registro_Modulos(string aplicacion,int idModulo,string usuAct)
        {
            DlOpcion UsuEODL = new DlOpcion();
            try
            {
                return UsuEODL.Eliminar_Registro_Modulos(aplicacion, idModulo, usuAct);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public string SP_INSERTAR_MODULO_OPCION(BeModuloOpcion UsuarioEOBE)
        {
            DlOpcion UsuEODL = new DlOpcion();
            try
            {
                return UsuEODL.SP_INSERTAR_MODULO_OPCION(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public string Eliminar_Registro_Modulos_Perfil_Opcion(string aplicacion, int idModulo,int idPerfil,string usuMod)
        {
            DlOpcion UsuEODL = new DlOpcion();
            try
            {
                return UsuEODL.Eliminar_Registro_Modulos_Perfil_Opcion(aplicacion, idModulo, idPerfil,usuMod);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public string SP_INSERTAR_MODULO_OPCION_PERFIL(BePerfilModuloOpcion UsuarioEOBE)
        {
            DlOpcion UsuEODL = new DlOpcion();
            try
            {
                return UsuEODL.SP_INSERTAR_MODULO_OPCION_PERFIL(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

    }
}
