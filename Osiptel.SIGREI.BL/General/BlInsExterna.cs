﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
   public class BlInsExterna : IDisposable
    {
        private Component component = new Component();
        private bool disposed = false;
        public string SP_INERTAR_INSTITUCION_EXTERNA(BeInsExterna UsuarioEOBE)
        {
            DlInsExterna UsuEODL = new DlInsExterna();
            try
            {
                return UsuEODL.SP_INERTAR_INSTITUCION_EXTERNA(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public string SP_INSERTAR_REGISTRO_CONSULTA_WS(String Aplicacion, String idOpcion, String usuario, String num_ruc, String origen, String proceso, String respuesta, String localIP, String mac)
        {
            DlInsExterna UsuEODL = new DlInsExterna();
            try
            {
                return UsuEODL.SP_INSERTAR_REGISTRO_CONSULTA_WS(Aplicacion, idOpcion, usuario, num_ruc, origen, proceso, respuesta, localIP, mac);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BeInsExterna> ListarInstituciones_Externas_By_Todo()
        {
            List<BeInsExterna> lista;
            DlInsExterna objParametroDL = new DlInsExterna();
            lista = objParametroDL.ListarInstituciones_Externas_By_Todo();



            return lista;
        }

        public List<BeInsExterna> ListarInsExternasByEP(int nCodEP,int estado)
        {
            List<BeInsExterna> lista;
            DlInsExterna objParametroDL = new DlInsExterna();
            lista = objParametroDL.ListarInsExternasByEP(nCodEP,estado);



            return lista;
        }

        public BeInsExterna SP_OBTENER_INST_EXTERNA_BY_RUC(string pRuc,string cUser)
        {
            DlInsExterna ParametroEPBE = new DlInsExterna();
            BeInsExterna LISTA;
            try
            {
                LISTA = ParametroEPBE.SP_OBTENER_INST_EXTERNA_BY_RUC(new BeInsExterna()
                {
                    NUM_RUC = pRuc,
                    USUCRE = cUser
                });

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }


        public string SP_ACTUALIZAR_INSTITUCION_EXTERNA(BeInsExterna UsuarioEOBE)
        {
            DlInsExterna UsuEODL = new DlInsExterna();
            try
            {
                return UsuEODL.SP_ACTUALIZAR_INSTITUCION_EXTERNA(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }
    }
}
