﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_EIR : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"      

        public List<BE_EIR> BuscarEIR(ref BE_PAGINACION objPaginacion, BE_EIR objParametro)
        {
            DL_EIR objParametroDL = new DL_EIR();
            List<BE_EIR> lista;

            try
            {
                lista = objParametroDL.BuscarEIR(ref objPaginacion, objParametro);
                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }


        public DataTable ExportarEIR_DT(BE_EIR objParametro)
        {
            DL_EIR objParametroDL = new DL_EIR();
            DataTable lista;
            try
            {
                lista = objParametroDL.ExportarEIR_DT(objParametro);
                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
