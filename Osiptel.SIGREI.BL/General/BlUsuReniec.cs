﻿using System;
using System.Collections.Generic;

using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;
namespace Osiptel.SIGREI.BL
{
    public class BlUsuReniec
    {
        public List<BeParReniec> ExtrerUsuarioReniec(string usuario,int estado)
        {
            List<BeParReniec> lista;
            DlUsuReniec objUnidadEPDL = new DlUsuReniec();

            try
            {
                lista = objUnidadEPDL.ExtrerUsuarioReniec(usuario, estado);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

    }
}
