﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_USUARIOGPSU : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"

        public List<BE_USUARIOGPSU> Listar(string pUnidad, int opcion)
        {
            DL_USUARIOGPSU objParametroDL = new DL_USUARIOGPSU();
            BE_USUARIOGPSU pFiltros = new BE_USUARIOGPSU();
            pFiltros.unidad = pUnidad;
            pFiltros.opcion = opcion;
            List<BE_USUARIOGPSU> lista;

            try
            {
                lista = objParametroDL.Listar(pFiltros);
                //lista.Insert(0, new BE_USUARIOGPSU() { codigo = pInicialVal, nombreCompleto = pInicialText });
                
                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
        
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
