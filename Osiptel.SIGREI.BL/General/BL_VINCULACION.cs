﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_VINCULACION : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"

        public List<BE_VINCULACION> BuscarVinculacion(ref BE_PAGINACION objPaginacion, BE_VINCULACION objParametro)
        {
            DL_VINCULACION objParametroDL = new DL_VINCULACION();

            List<BE_VINCULACION> lista;

            try
            {
                lista = objParametroDL.BuscarVinculacion(ref objPaginacion, objParametro);


                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
    
        public DataTable ExportarVinculacion_DT(BE_VINCULACION objParametro)
        {
            DL_VINCULACION objParametroDL = new DL_VINCULACION();

            DataTable lista;

            try
            {
                lista = objParametroDL.ExportarVinculacion_DT(objParametro);


                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
    
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
