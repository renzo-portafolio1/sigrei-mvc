﻿using System;
using System.Collections.Generic;

using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BlUnidadEp
    {
        #region Variables

        private Component component = new Component();
      


        #endregion




        public string SP_INSERTAR_UNIDAD_ENTIDAD_PUBLICA(BeUnidadEp UnidadEP)
        {
            DlUnidadEp UnidadEPDL = new DlUnidadEp();
            try
            {
                return UnidadEPDL.SP_INSERTAR_UNIDAD_ENTIDAD_PUBLICA(UnidadEP);
            }
            catch (Exception ex)
            {
                DL_LOG.Insertar("SP_INSERTAR_UNIDAD_ENTIDAD_PUBLICA", "", ex.StackTrace);
                throw ex;
            }
        }

        public List<BeUnidadEp> ValidarRegistroUnidadEP(int idUnidadEP,string nombreUEP)
        {
            List<BeUnidadEp> lista;
            DlUnidadEp objUnidadEPDL = new DlUnidadEp();

            try
            {
                lista = objUnidadEPDL.ValidarRegistroUnidadEP(idUnidadEP, nombreUEP);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public List<BeUnidadEp> ListarUnidadEP_By_Todo(string aplicacion, string tabla, string columna)
        {
            List<BeUnidadEp> lista;
            DlUnidadEp objUnidadEPDL = new DlUnidadEp();

            try
            {
                lista = objUnidadEPDL.ListarUnidadEP_By_Todo(aplicacion, tabla, columna);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }

        public string SP_ACTUALIZAR_UNIDAD_ENTIDAD_PUBLICA(BeUnidadEp UsuarioEOBE)
        {
            DlUnidadEp UsuEODL = new DlUnidadEp();
            try
            {
                return UsuEODL.SP_ACTUALIZAR_UNIDAD_ENTIDAD_PUBLICA(UsuarioEOBE);
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public BeUnidadEp SP_OBTENER_UNIDAD_EP_BY_CODIGO(int pvalor)
        {
            DlUnidadEp ParametroEPBE = new DlUnidadEp();
            BeUnidadEp LISTA;
            try
            {
                LISTA = ParametroEPBE.SP_OBTENER_UNIDAD_EP_BY_CODIGO(new BeUnidadEp()
                {
                    idUnidadEP = pvalor
                });

                return LISTA;
            }
            catch (Exception ex)
            {
                throw new Exception("Mensaje de Error: ", ex);
            }
        }

        public List<BeUnidadEp> ListaUnidadEPByEstado(string nEntidadP,int estado)
        {
            List<BeUnidadEp> lista;
            DlUnidadEp objUnidadEPDL = new DlUnidadEp();

            try
            {
                lista = objUnidadEPDL.ListaUnidadEPByEstado(nEntidadP,estado);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objUnidadEPDL = null;
            }

            return lista;
        }


    }
}
