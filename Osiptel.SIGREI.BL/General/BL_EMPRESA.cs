﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_EMPRESA : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"

        public List<BE_EMPRESA> Listar(long pEmpresa)
        {
            DL_EMPRESA objParametroDL = new DL_EMPRESA();
            BE_EMPRESA pFiltros = new BE_EMPRESA();
            pFiltros.idEmpresa = pEmpresa;
            List<BE_EMPRESA> lista;

            try
            {
                lista = objParametroDL.Listar(pFiltros);
                //lista.Insert(0, new BE_EMPRESA() { idEmpresa = pInicialVal, empresaOperadora = pInicialText });
                
                return lista;

            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
        
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
