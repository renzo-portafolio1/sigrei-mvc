﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_PARAMETRO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;

        #endregion

        #region "No Transaccionales"

        public List<BE_PARAMETRO> ListarValores(string pAplicacion, string pTabla, string pColumna)
        {
            List<BE_PARAMETRO> lista;
            lista = ListarValores(new BE_PARAMETRO()
            {
                aplicacion = pAplicacion,
                tabla = pTabla,
                columna = pColumna
            });

            //lista.Insert(0, new BE_PARAMETRO() { valor = pInicialVal, descripcion = pInicialText });

            return lista;
        }

        public List<BE_PARAMETRO> ListarValores(BE_PARAMETRO filtros)
        {
            DL_PARAMETRO objParametroDL = new DL_PARAMETRO();

            try
            {
                return objParametroDL.ListarValores(filtros);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                objParametroDL = null;
            }
        }
    
        
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
