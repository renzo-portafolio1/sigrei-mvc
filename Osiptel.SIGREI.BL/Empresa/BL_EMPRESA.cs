﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SISDEA.BE;
using Osiptel.SISDEA.DL;

namespace Osiptel.SISDEA.BL
{
    public class BL_EMPRESA : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_EMPRESA objData = new DL_EMPRESA();

        #endregion

        #region "No Transaccionales"

        //public List<BE_EMPRESA> ListarBuzon(BE_EMPRESA objetoParametro)
        //{
        //    return objData.ListarBuzon(objetoParametro);
        //}

        public List<BE_EMPRESA> ListarValoresEmpresa(Int32 opcion, string pInicialVal, string pInicialText)
        {
            List<BE_EMPRESA> lista;
            BE_EMPRESA param = new BE_EMPRESA();

            lista = objData.ListarEmpresa();

            if (opcion == 1)
            {
                lista.Insert(0, new BE_EMPRESA() { Codigo = pInicialVal, RazonSocial = pInicialText });
            }
            else {
                lista.Insert(0, new BE_EMPRESA() { Codigo = pInicialVal, NombreComercial = pInicialText });
            }

            return lista;
        }

        #endregion

        #region "Transaccionales"
            

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion
    }
}
