﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SISDEA.BE;
using Osiptel.SISDEA.DL;

namespace Osiptel.SISDEA.BL
{
    public class BL_ARCHIVO_ADJUNTO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_ARCHIVO_ADJUNTO objData = new DL_ARCHIVO_ADJUNTO();
        #endregion

        #region "No Transaccionales"

        public List<BE_ARCHIVO_ADJUNTO> ListaDocumentos(BE_ARCHIVO_ADJUNTO objetoParametro)
        {
            return objData.ListaDocumentos(objetoParametro);
        }
      

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 Insertar(BE_ARCHIVO_ADJUNTO objetoParametro)
        {
            Int64 objResultado = objData.Insertar(objetoParametro);
            return objResultado;
        }       
       

        #endregion

    }
}
