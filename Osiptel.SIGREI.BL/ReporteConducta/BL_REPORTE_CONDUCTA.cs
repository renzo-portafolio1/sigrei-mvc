﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SISDEA.BE;
using Osiptel.SISDEA.DL;

namespace Osiptel.SISDEA.BL
{
    public class BL_REPORTE_CONDUCTA : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_REPORTE_CONDUCTA objData = new DL_REPORTE_CONDUCTA();
        #endregion

        #region "No Transaccionales"

        public List<BE_REPORTE_CONDUCTA> ListaRepConducta(BE_REPORTE_CONDUCTA objetoParametro)
        {
            return objData.ListaRepConducta(objetoParametro);
        }

        public BE_REPORTE_CONDUCTA ConsultaRepConducta(BE_REPORTE_CONDUCTA objetoParametro)
        {
            return objData.ConsultaRepConducta(objetoParametro);
        }
        public DataTable ListaRepConductaDt(BE_REPORTE_CONDUCTA objetoParametro)
        {
            return objData.ListaRepConductaDt(objetoParametro);
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 Insertar(BE_REPORTE_CONDUCTA objetoParametro)
        {
            Int64 objResultado = objData.Insertar(objetoParametro);
            return objResultado;
        }

        public Int64 RevisarReporteConducta(BE_REPORTE_CONDUCTA objetoParametro)
        {
            Int64 objResultado = objData.RevisarReporteConducta(objetoParametro);
            return objResultado;
        }

        public Int64 ConcluirReporteConducta(BE_REPORTE_CONDUCTA objetoParametro)
        {
            Int64 objResultado = objData.ConcluirReporteConducta(objetoParametro);
            return objResultado;
        }
        /*
        public Int64 Modificar(BE_DECLARACION_JURADA objeto)
        {
            Int64 objResultado = objData.Modificar(objeto);
            return objResultado;
        }

        public Int32 ActualizarEstado(BE_DECLARACION_JURADA objeto)
        {
            Int32 objResultado = objData.ActualizarEstado(objeto);
            return objResultado;
        }*/

       

        #endregion

    }
}
