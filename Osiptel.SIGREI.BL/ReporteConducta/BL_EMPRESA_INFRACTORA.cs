﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SISDEA.BE;
using Osiptel.SISDEA.DL;

namespace Osiptel.SISDEA.BL
{
    public class BL_EMPRESA_INFRACTORA : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_EMPRESA_INFRACTORA objData = new DL_EMPRESA_INFRACTORA();
        #endregion

        #region "No Transaccionales"

        public List<BE_EMPRESA_INFRACTORA> ListaEmpresasInfractoras(BE_EMPRESA_INFRACTORA objetoParametro)
        {
            return objData.ListaEmpresasInfractoras(objetoParametro);
        }
      

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 Insertar(BE_EMPRESA_INFRACTORA objetoParametro)
        {
            Int64 objResultado = objData.Insertar(objetoParametro);
            return objResultado;
        }       
       

        #endregion

    }
}
