﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class UsuarioBL : IDisposable
    {
        #region Variables

            private Component component = new Component();
            private bool disposed = false;

        #endregion

        public string FModificaClave(UsuarioBE objUsuario_BE)
        {
            UsuarioDL objDaUsua = new UsuarioDL();
            return objDaUsua.FModificaClave(objUsuario_BE);
        }

        public DataTable ObtenerUsuEO(String pUsuario, String aplicacion)
        {
            UsuarioDL oDLUsuario = new UsuarioDL();
            try
            {
                return oDLUsuario.ObtenerUsuEO(pUsuario, aplicacion);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                oDLUsuario = null;
            }

        }

        public String ValidarUsuEO(String pUsuario, String PContrasenia,String aplicacion)
        {
            UsuarioDL oDLUsuario = new UsuarioDL();
            try
            {
                return oDLUsuario.ValidarUsuEO(pUsuario, PContrasenia, aplicacion);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                oDLUsuario = null;
            }
        }

        public String Validar(String pUsuario)
        {
            UsuarioDL oUsuarioDL = new UsuarioDL();
            try
            {
                return oUsuarioDL.Validar(pUsuario);
            }catch (Exception lexcError){
                throw new Exception(lexcError.Message);
            }finally{
                oUsuarioDL = null;
            }
        }

        public UsuarioBE Obtener(String pUsuario)
        {
            UsuarioDL oUsuarioDL = new UsuarioDL();
            try
            {
                return oUsuarioDL.Obtener(pUsuario);
            }catch (Exception lexcError){
                throw new Exception(lexcError.Message);
            }finally{
                oUsuarioDL = null;
            }
        }

        public DataTable Listar()
        {
            UsuarioDL oUsuarioDL = new UsuarioDL();
            try
            {
                return oUsuarioDL.Listar();
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                oUsuarioDL = null;
            }
        }

        public Int32 ValidarPermisos(String pAplicacion, String pUsuario)
        {
            UsuarioDL oUsuarioDL = new UsuarioDL();
            try
            {
                return oUsuarioDL.ValidarPermisos(pAplicacion, pUsuario);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                oUsuarioDL = null;
            }
        }

        public DataTable ObtenerIdPerfil(String pAplicacion, String pUsuario)
        {
            UsuarioDL oUsuarioDL = new UsuarioDL();
            try
            {
                return oUsuarioDL.ObtenerIdPerfil(pAplicacion, pUsuario);
            }catch (Exception lexcError){
                throw new Exception(lexcError.Message);
            }finally{
                oUsuarioDL = null;
            }
        }

        public List<BeAccesoOpcion> obtenerListaOpcionPerfil(string idPerfil)
        {
            return new UsuarioDL().obtenerListaOpcionPerfil(idPerfil);
        }

        public List<UsuarioBE> FListarUsuario_Osiptel(UsuarioBE UsuarioBE)
        {
            return new UsuarioDL().FListarUsuario_Osiptel(UsuarioBE);
        }

        public List<UsuarioBE> FListarUsuario(UsuarioBE UsuarioBE)
        {
            return new UsuarioDL().FListarUsuario(UsuarioBE);
        }

        public string FActualiza_Acceso_x_Usuario(UsuarioBE objUsuario_BE)
        {
            UsuarioDL objDaUsua = new UsuarioDL();
            return objDaUsua.FActualiza_Acceso_x_Usuario(objUsuario_BE);
        }

        public string FEstadoUsuario(UsuarioBE objUsuario_BE)
        {
            UsuarioDL objDaUsua = new UsuarioDL();
            return objDaUsua.FEstadoUsuario(objUsuario_BE);
        }

        public string FDesactivar_Acceso_x_Usuario(UsuarioBE objUsuario_BE)
        {
            UsuarioDL objDaUsua = new UsuarioDL();
            return objDaUsua.FDesactivar_Acceso_x_Usuario(objUsuario_BE);
        }

        public string FInsertaUsuario(UsuarioBE objUsuario_BE)
        {
            UsuarioDL objDaUsua = new UsuarioDL();
            return objDaUsua.FInsertaUsuario(objUsuario_BE);
        }


        public string FActualizaUsuario(UsuarioBE objUsuario_BE)
        {
            UsuarioDL objDaUsua = new UsuarioDL();
            return objDaUsua.FActualizaUsuario(objUsuario_BE);
        }


        

        public string FInsertaPerfil_x_Usuario(UsuarioBE objUsuario_BE)
        {
            UsuarioDL objDaUsua = new UsuarioDL();
            return objDaUsua.FInsertaPerfil_x_Usuario(objUsuario_BE);
        }

        public string FActualizaPerfil_x_Usuario(UsuarioBE objUsuario_BE)
        {
            UsuarioDL objDaUsua = new UsuarioDL();
            return objDaUsua.FActualizaPerfil_x_Usuario(objUsuario_BE);
        }
        public UsuarioBE ObtenerUsuarioSolicitante(String pUsuario)
        {
            UsuarioDL oUsuarioDL = new UsuarioDL();
            try
            {
                return oUsuarioDL.ObtenerUsuarioSolicitante(pUsuario);
            }
            catch (Exception lexcError)
            {
                throw new Exception(lexcError.Message);
            }
            finally
            {
                oUsuarioDL = null;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion
    }
}
