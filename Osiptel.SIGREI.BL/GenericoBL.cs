﻿
using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Text;

using System.Security.Cryptography;
using System.IO;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;
using Osiptel.SIGREI.BL;

namespace Osiptel.SIGREI.BL 
{
    public class GenericoBL
    {
        public List<EstadoBE> LISTAR_ESTADOS(string TIPO)
        {
            GenericoBE GenericoBE = new GenericoBE();
            List<EstadoBE> LISTA = GenericoBE.ESTADOS;
            EstadoBE EstadoBE = new EstadoBE();
            //if (TIPO.CompareTo("S") == 0)
            //{
            //    EstadoBE.IDESTADO = "S";
            //    EstadoBE.ESTADO = "Seleccione";
            //    LISTA.Insert(0, EstadoBE);
            //}
            //else if (TIPO.CompareTo("T") == 0)
            //{
            //    EstadoBE.IDESTADO = "T";
            //    EstadoBE.ESTADO = "TODOS";
            //    LISTA.Insert(0, EstadoBE);
            //}
            return LISTA;
        }

        public List<EstadoBE> ESTADOSREQEP(string TIPO)
        {
            GenericoBE GenericoBE = new GenericoBE();
            List<EstadoBE> LISTA = GenericoBE.ESTADOSREQEP;
            EstadoBE EstadoBE = new EstadoBE();
           
            return LISTA;
        }

        public List<TipoMenuBE> LISTAR_TIPO_MENU()
        {
            GenericoBE GenericoBE = new GenericoBE();
            List<TipoMenuBE> LISTA = GenericoBE.LISTAR_TIPO_MENU;
           
          
            return LISTA;
        }

        public List<SexoBE> LISTAR_SEXOS(string TIPO)
        {
            GenericoBE GenericoBE = new GenericoBE();
            List<SexoBE> LISTA = GenericoBE.SEXOS;
            SexoBE SexoBE = new SexoBE();
            //if (TIPO.CompareTo("S") == 0) {
            //    SexoBE.IDSEXO = "S";
            //    SexoBE.SEXO = "Seleccione";
            //    LISTA.Insert(0, SexoBE);
            //}
            //else if (TIPO.CompareTo("T") == 0)
            //{
            //    SexoBE.IDSEXO = "T";
            //    SexoBE.SEXO = "TODOS";
            //    LISTA.Insert(0, SexoBE);
            //}
            return LISTA;
        }

        public List<TipoDocumentoBE> LISTAR_TIPODOCUMENTOS(string TIPO)
        {
            GenericoBE GenericoBE = new GenericoBE();
            List<TipoDocumentoBE> LISTA = GenericoBE.TIPODOCUMENTOS;
            TipoDocumentoBE TipoDocumentoBE = new TipoDocumentoBE();
            //if (TIPO.CompareTo("S") == 0)
            //{
            //    TipoDocumentoBE.IDTIPODOCUMENTO = "S";
            //    TipoDocumentoBE.TIPODOCUMENTO = "Seleccione";
            //    LISTA.Insert(0, TipoDocumentoBE);
            //}
            //else if (TIPO.CompareTo("T") == 0)
            //{
            //    TipoDocumentoBE.IDTIPODOCUMENTO = "T";
            //    TipoDocumentoBE.TIPODOCUMENTO = "TODOS";
            //    LISTA.Insert(0, TipoDocumentoBE);
            //}
            return LISTA;
        }
    }
}
