﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_REQUERIMIENTO_IMEI_EO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_REQUERIMIENTO_IMEI_EO objData = new DL_REQUERIMIENTO_IMEI_EO();

        #endregion

        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_IMEI_EO> ListaIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            return objData.ListaIMEI(objetoParametro);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;
            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 InsertarImei(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 objResultado = objData.InsertarIMEI(objetoParametro);
            return objResultado;
        }

        public Int64 ActualizarInformacionIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 objResultado = objData.ActualizarIMEI(objetoParametro);
            return objResultado;
        }

        public Int64 ActualizarEstadoIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 objResultado = objData.ActualizarEstadoIMEI(objetoParametro);
            return objResultado;
        }

        public Int64 DescartarIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 objResultado = objData.DescartarIMEI(objetoParametro);
            return objResultado;
        }

        public Int64 AtenderIMEI(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            Int64 objResultado = objData.AtenderIMEI(objetoParametro);
            return objResultado;
        }
                                  
        #endregion

    }
}
