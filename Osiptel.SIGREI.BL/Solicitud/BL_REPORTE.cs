﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;
using Osiptel.SIGREI.BE.General;
using Osiptel.SIGREI.BE.Reporte;

namespace Osiptel.SIGREI.BL
{
    public class BL_REPORTE : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_REPORTE objData = new DL_REPORTE();

        #endregion

        #region "No Transaccionales"
        public List<BE_COMBO> ListaComboUbigeo()
        {
            return objData.ComboUbigeo();
        }
        public List<BE_COMBO> ListaComboMes()
        {
            return objData.CombosMes();
        }
        public List<BE_COMBO> ListaComboEO()
        {
            return objData.CombosEO();
        }
        public DataTable ListarReporteRegion(BE.Reporte.Req.BE_REPORTE_REGION objRegion)
        {
            return objData.ListarReporteRegion(objRegion);
        }
        public DataTable TablaReporteRegion(BE.Reporte.Req.BE_REPORTE_REGION objRegion)
        {
            return objData.TablaReporteRegion(objRegion);
        }
        public DataTable ListarReporteEntidad(BE.Reporte.Req.BE_REPORTE_ENTIDAD objEntidad)
        {
            return objData.ListarReporteEntidad(objEntidad);
        }
        public DataTable TablaReporteEntidad(BE.Reporte.Req.BE_REPORTE_ENTIDAD objEntidad)
        {
            return objData.TablaReporteEntidad(objEntidad);
        }
        public DataTable ListarReporteEO(BE.Reporte.Req.BE_REPORTE_EO objEO)
        {
            return objData.ListarReporteEO(objEO);
        }
        public DataTable TablaReporteEO(BE.Reporte.Req.BE_REPORTE_EO objEO)
        {
            return objData.TablaReporteEO(objEO);
        }
        public DataTable GraficaGerencialEO(BE.Reporte.Req.BE_GERENCIAL_EO objEO)
        {
            return objData.GraficoGerencialesEO(objEO);
        }
        public List<BE_TABLA_EO> TablaGerencialEO(BE.Reporte.Req.BE_GERENCIAL_EO objEO)
        {
            return objData.TablaGerencialesEO(objEO);
        }
        public List<BE_TABLA_INSTITUCION> TablaGerencialIns(BE.Reporte.Req.BE_GERENCIAL_EO objEO)
        {
            return objData.TablaGerencialesIns(objEO);
        }

        public DataTable ExportarReporteGeneral(BE.Reporte.Req.BE_GERENCIAL_EO objEO)
        {
            return objData.ExportarReporteGeneral(objEO);
        }

        //
        public List<BE_REPORTE_IMEI> ListaReporte(BE_REPORTE_IMEI objetoParametro)
        {
            return objData.ListaReporte(objetoParametro);
        }
        public List<BE_REPORTE_IMEI> ListaReporteEntidad(BE_REPORTE_IMEI objetoParametro)
        {
            return objData.ListaReporteEntidad(objetoParametro);
        }
        public List<BE_REPORTE_IMEI> ListaReporteDepartamento(BE_REPORTE_IMEI objetoParametro)
        {
            return objData.ListaReporteDepartamento(objetoParametro);
        }
        public List<BE_REPORTE_IMEI> ListaReporteEmpresaOperadora(BE_REPORTE_IMEI objetoParametro)
        {
            return objData.ListaReporteEmpresaOperadora(objetoParametro);
        }

        public List<BE_REPORTE_IMEI> ListaPromedioAtencionEO(BE_REPORTE_IMEI objetoParametro)
        {
            return objData.ListaPromedioAtencionEO(objetoParametro);
        }

        public List<BE_REPORTE_IMEI> ListaPromedioAtencionOsiptel(BE_REPORTE_IMEI objetoParametro)
        {
            return objData.ListaPromedioAtencionOsiptel(objetoParametro);
        }

        public List<BE_REPORTE_IMEI> ListaRatioDiasAtencionOsiptel(BE_REPORTE_IMEI objetoParametro)
        {
            return objData.ListaRatioDiasAtencionOsiptel(objetoParametro);
        }

        public List<BE_REPORTE_IMEI> ListaRatioPorcentajeAtencionOsiptel(BE_REPORTE_IMEI objetoParametro)
        {
            return objData.ListaRatioPorcentajeAtencionOsiptel(objetoParametro);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion


    }
}
