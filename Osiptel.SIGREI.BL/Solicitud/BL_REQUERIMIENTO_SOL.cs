﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_REQUERIMIENTO_SOL : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_REQUERIMIENTO_SOL objData = new DL_REQUERIMIENTO_SOL();

        #endregion

        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_SOL> Listar(BE_REQUERIMIENTO_SOL objetoParametro)
        {
            return objData.Listar(objetoParametro);
        }

        public BE_REQUERIMIENTO_SOL Consultar(BE_REQUERIMIENTO_SOL objetoParametro)
        {
            return objData.Consultar(objetoParametro);
        }

        public List<BE_REQUERIMIENTO_IMEI> ListaIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            return objData.ListaIMEI(objetoParametro);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;
            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 Insertar(BE_REQUERIMIENTO_SOL objetoParametro)
        {
            Int64 objResultado = objData.Insertar(objetoParametro);
            return objResultado;
        }

        public BE_REQUERIMIENTO_SOL Recibir(BE_REQUERIMIENTO_SOL objetoParametro)
        {
            return objData.Recibir(objetoParametro);
        }

        #endregion

    }
}
