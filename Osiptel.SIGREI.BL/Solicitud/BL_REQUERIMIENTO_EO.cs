﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_REQUERIMIENTO_EO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_REQUERIMIENTO_EO objData = new DL_REQUERIMIENTO_EO();

        #endregion

        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_EO> ListaSolicitudEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            return objData.ListaSolicitudEO(objetoParametro);
        }

        public BE_REQUERIMIENTO_EO ConsultaSolicitudEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            return objData.ConsultaSolicitudEO(objetoParametro);
        }

        public List<BE_REQUERIMIENTO_IMEI_EO> ListaIMEI_EO(BE_REQUERIMIENTO_IMEI_EO objetoParametro)
        {
            return objData.ListaIMEI_EO(objetoParametro);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 InsertarEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            Int64 objResultado = objData.InsertarEO(objetoParametro);
            return objResultado;
        }

        public Int64 AtenderEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            Int64 objResultado = objData.AtenderEO(objetoParametro);
            return objResultado;
        }

        public Int64 ActualizarVencimientoEO(BE_REQUERIMIENTO_EO objetoParametro)
        {
            Int64 objResultado = objData.ActualizarVencimiento(objetoParametro);
            return objResultado;
        }

        #endregion

    }
}
