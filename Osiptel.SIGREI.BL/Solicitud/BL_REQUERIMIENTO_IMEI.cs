﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_REQUERIMIENTO_IMEI : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_REQUERIMIENTO_IMEI objData = new DL_REQUERIMIENTO_IMEI();

        #endregion

        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_IMEI> ListaIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            return objData.ListaIMEI(objetoParametro);
        }

        public List<BE_REQUERIMIENTO_IMEI> ListaIMEI_EXCEPCION(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            return objData.ListaIMEI_EXCEPCION(objetoParametro);
        }

        public List<BE_REQUERIMIENTO_IMEI> ListaMOVIL(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            return objData.ListaMOVIL(objetoParametro);
        }

        public List<BE_REQUERIMIENTO_IMEI> ListaIMEIAnalizar(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            return objData.ListaIMEIAnalizar(objetoParametro);
        }

        public List<BE_REQUERIMIENTO_IMEI> ListaIMEIAsignado(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            return objData.ListaIMEIAsignado(objetoParametro);
        }

        public List<BE_REQUERIMIENTO_IMEI> ConsultarImeiPendietes(string codigo)
        {
            return objData.ConsultarImeiPendietes(codigo);
        }

        public List<BE_REQUERIMIENTO_IMEI> ConsultarMovilPendietes(string codigo)
        {
            return objData.ConsultarMovilPendietes(codigo);
        }

        public DataTable ListaIMEI_EO_DT(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            return objData.ListaIMEI_EO_DT(objetoParametro);
        }
        public List<BE_EVENTO_IMEI> ListarEventosIMEI(BE_EVENTO_IMEI objetoParametro)
        {
            return objData.ListarEventosIMEI(objetoParametro);
        }

        public BE_REQUERIMIENTO_IMEI ConsultarIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            return objData.ConsultarIMEI(objetoParametro);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;
            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 InsertarImeiExterno(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 objResultado = objData.InsertarImeiExterno(objetoParametro);
            return objResultado;
        }

        public Int64 ActualizarInformacionIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 objResultado = objData.ActualizarInformacionIMEI(objetoParametro);
            return objResultado;
        }

        public Int64 InsertarSisDocImeiInterno(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.InsertarSisDocImeiInterno(objetoParametro);
            return objResultado;
        }

        public Int64 InsertarImeiInterno(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.InsertarImeiInterno(objetoParametro);
            return objResultado;
        }

        public Int64 InsertarTelefonoInterno(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.InsertarTelefonoInterno(objetoParametro);
            return objResultado;
        }

        public Int64 ActualizarEstadoIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 objResultado = objData.ActualizarEstadoIMEI(objetoParametro);
            return objResultado;
        }

        public Int64 AtenderIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 objResultado = objData.AtenderIMEI(objetoParametro);
            return objResultado;
        }

        public Int64 DevolverIMEI(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 objResultado = objData.DevolverIMEI(objetoParametro);
            return objResultado;
        }
        public Int64 ProcesaIMEISAR(BE_REQUERIMIENTO_IMEI objetoParametro)
        {
            Int64 objResultado = objData.ProcesaIMEISAR(objetoParametro);
            return objResultado;
        }
                                
        #endregion

    }
}
