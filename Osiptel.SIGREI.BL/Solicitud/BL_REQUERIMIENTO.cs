﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;
using System.Xml;

namespace Osiptel.SIGREI.BL
{
    public class BL_REQUERIMIENTO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_REQUERIMIENTO objData = new DL_REQUERIMIENTO();

        #endregion

        #region "No Transaccionales"

        public List<BE_REQUERIMIENTO_IMEI> ConsultaEstadoAbonExcepc(string nroImei, string TIPO_DOC_LEGAL, string NUMERO_DOC_LEGAL,string NROSERVICIOMOVIL)
        {
            return objData.ConsultaEstadoAbonExcepc(nroImei,TIPO_DOC_LEGAL,NUMERO_DOC_LEGAL, NROSERVICIOMOVIL);
        }
        public List<BE_REQUERIMIENTO> ActualizarCabeExcepcion(long idRequerimiento,string  cod_requerimiento,long idRequerimientoIMEi,string nombre)
        {
            return objData.ActualizarCabeExcepcion( idRequerimiento,   cod_requerimiento,  idRequerimientoIMEi,  nombre);
        }

        public List<BE_REQUERIMIENTO> ListaSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            return objData.ListaSolicitud(objetoParametro);
        }

        public BE_REQUERIMIENTO ConsultaSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            return objData.ConsultaSolicitud(objetoParametro);
        }

        public Int64 ValidarClaveSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.ValidarClaveSolicitud(objetoParametro);
            return objResultado;
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

        #region "Transaccionales"

        public Int64 Insertar(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.Insertar(objetoParametro);
            return objResultado;
        }

        public Int16 ConfirmarRegistroReqEP(int codigo)
        {
            Int16 objResultado = objData.ConfirmarRegistroReqEP(codigo);
            return objResultado;
        }

        public Int64 InsertarSiSDoc(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.InsertarSiSDoc(objetoParametro);
            return objResultado;
        }

        public long InsertarReqEP(BE_REQUERIMIENTO objetoParametro)
        {
            long objResultado = objData.InsertarReqEP(objetoParametro);
            return objResultado;
        }

        public string ConsultarCodGeneradoReqEP(long idRequerimiento)
        {
            string objResultado = objData.ConsultarCodGeneradoReqEP(idRequerimiento);
            return objResultado;
        }

        public BE_REQUERIMIENTO ObtenerDatosIMEI(BE_REQUERIMIENTO objetoParametro)
        {
            BE_REQUERIMIENTO objResultado = objData.ObtenerDatosIMEI(objetoParametro);
            return objResultado;
        }

        public List<BE_REQUERIMIENTO> ListarRequerimientoEP(String Estado,String nCodRequerimiento,String dFecInicio,String dFecFin,String cPerUser )
        {
            return objData.ListarRequerimientoEP(Estado, nCodRequerimiento, dFecInicio, dFecFin,cPerUser);
        }

        public List<BE_REQUERIMIENTO> ListarDetalleRequerimientoEP(String codigo)
        {
            return objData.ListarDetalleRequerimientoEP(codigo);
        }

        public List<BE_REQUERIMIENTO> ObtenerDatosIMEIXML(String cadenaInicial, String cadenaUno, String cadenaDos, String cadenaTres, String cadenaCuatro, String cadenaCinco, String cadenaSeis, String cadenaSiete, String cadenaOcho, String cadenaNueve, String cadenaDiez, String cadenaFinal, BE_REQUERIMIENTO objetoParametro)
        {            
            return objData.ObtenerDatosIMEIXML(cadenaInicial,cadenaUno, cadenaDos, cadenaTres, cadenaCuatro, cadenaCinco, cadenaSeis, cadenaSiete, cadenaOcho, cadenaNueve, cadenaDiez, cadenaFinal, objetoParametro); 
        }


        public Int64 AprobarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.AprobarSolicitud(objetoParametro);
            return objResultado;
        }

        public Int64 RechazarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.RechazarSolicitud(objetoParametro);
            return objResultado;
        }

        public Int64 EnviarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.EnviarSolicitud(objetoParametro);
            return objResultado;
        }

        public Int64 AtenderSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.AtenderSolicitud(objetoParametro);
            return objResultado;
        }

        public Int64 FinalizarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.FinalizarSolicitud(objetoParametro);
            return objResultado;
        }

        public Int64 AsignarSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.AsignarSolicitud(objetoParametro);
            return objResultado;
        }

        public Int64 DevolverSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            Int64 objResultado = objData.DevolverSolicitud(objetoParametro);
            return objResultado;
        }


        public List<BE_REQUERIMIENTO> ConsultaEstadoImei(String imei,String cUsuario)
        {
            return objData.ConsultaEstadoImei(imei, cUsuario);
        }

        public List<BE_REQUERIMIENTO> ConsultaEstadoTelefono(String imei, String cUsuario)
        {
            return objData.ConsultaEstadoTelefono(imei, cUsuario);
        }

        public BeUsuarioEP ConsultarCabeceraPendiente(String CodigoPer)
        {
            return objData.ConsultarCabeceraPendiente(CodigoPer);
        }
        #endregion

    }
}
