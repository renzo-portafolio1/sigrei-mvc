﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.DL;

namespace Osiptel.SIGREI.BL
{
    public class BL_MOVREQUERIMIENTO : IDisposable
    {
        #region Variables

        private Component component = new Component();
        private bool disposed = false;
        DL_MOVREQUERIMIENTO objData = new DL_MOVREQUERIMIENTO();

        #endregion

        #region "No Transaccionales"

        public List<BE_MOVREQUERIMIENTO> ListaMovSolicitud(BE_REQUERIMIENTO objetoParametro)
        {
            return objData.ListaMovSolicitud(objetoParametro);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
                disposed = true;

            }
        }

        #endregion

    }
}
