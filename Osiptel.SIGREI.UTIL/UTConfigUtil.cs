﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Osiptel.SISDEA.UTIL
{
    public static class ConfigUtil
    {
        public static string readValue(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

        public static string readConnectionString(string connectionString)
        {
            return ConfigurationManager.ConnectionStrings[connectionString].ConnectionString.ToString();
        }
    }
}
