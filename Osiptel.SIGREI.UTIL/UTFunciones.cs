﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;
using Osiptel.SISDEA.BL;

namespace Osiptel.SISDEA.UTIL
{

    public class UTFunciones
    {
        public static string SHA1Encrypt(string password)
        {
            HashAlgorithm hashValue = new SHA1CryptoServiceProvider();
            byte[] bytes = Encoding.UTF8.GetBytes(password);
            byte[] byteHash = hashValue.ComputeHash(bytes);
            hashValue.Clear();
            return (Convert.ToBase64String(byteHash));
        }

        public static object EvaluarNulo(object oValor)
        {
            if (oValor == null)
                return null;

            switch (oValor.GetType().Name)
            {
                case "String":
                    if (oValor.ToString().Trim() == "")
                        return null;
                    break;
                case "Int32":
                    if (System.Convert.ToInt32(oValor) == -1)
                        return null;
                    break;
                case "DateTime":
                    if (System.Convert.ToDateTime(oValor).ToString("dd/MM/yyyy") == "01/01/1900")
                        return null;
                    break;
                case "Char":
                    if (oValor.ToString() == "\0")
                        return null;
                    break;
                case "Double":
                    if (System.Convert.ToInt32(oValor) == -1)
                        return null;
                    break;
            }
            return oValor;
        }

        public static byte[] ConvertirCadenaByte(string[] cadena)
        {
            int count = cadena.Length;
            byte[] cadenaByte = new byte[count];
            for (int i = 0; i < count; i++)
            {
                cadenaByte[i] = Byte.Parse(cadena[i].Trim());
            }

            return cadenaByte;
        }

        public static string ReemplazarSaltoLinea(string sMensajesError)
        {
            sMensajesError = sMensajesError.Replace("\r", "");
            sMensajesError = sMensajesError.Replace("\n", " ");
            return sMensajesError;
        }

        public static bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format. 
            return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        public static bool IsValidText(string strIn)
        {
            // Return true if strIn is in valid e-mail format. 
            return Regex.IsMatch(strIn, @"(\w*[\s]*\w*[ Ñ, ñ, á, é, í, ó, ú, Á, É, Í, Ó, Ú]*)*");
        }

        public static bool IsValidAlfanumerico(string strIn)
        {
            // Return true if strIn is in valid e-mail format. 
            return Regex.IsMatch(strIn, @"^[0-9a-zA-Z'.\s]");
        }

        public static bool IsValidNumerico(string strIn)
        {
            // Return true if strIn is in valid e-mail format. 
            return Regex.IsMatch(strIn, @"^[0-9]");
        }

        public static bool ValidarCorreo(string sCorreo)
        {
            int intPosicion;

            intPosicion = sCorreo.IndexOf("@", 1, sCorreo.Length - 2);
            if (intPosicion <= 0)
                return false;

            intPosicion = sCorreo.IndexOf(".", intPosicion + 2, sCorreo.Length - intPosicion - 3);
            if (intPosicion <= 0)
                return false;
            return true;
        }

        public static bool IsValidDate(string FechaText)
        {
            try
            {
                DateTime Fecha = Convert.ToDateTime(FechaText);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string evaluaNuloCelda(string valor)
        {
            return valor.Equals("&nbsp;") ? "" : valor;
        }

        public static void LoadInformationCulture()
        {
            CultureInfo MiCultura = new CultureInfo("es-PE", false);
            CultureInfo CultureSystem = (CultureInfo)MiCultura.Clone();

            CultureSystem.DateTimeFormat.ShortDatePattern = "dd/MM/yyyy";
            CultureSystem.DateTimeFormat.ShortTimePattern = "HH:mm";

            Thread.CurrentThread.CurrentCulture = CultureSystem;

        }

        public static string ObtenerValorxCadena(string cadena, int accion)
        {
            string retorno = "";

            string[] field = cadena.ToString().Split(("/").ToCharArray());
            if (accion == 1)
                retorno = field[0].ToString();
            else
                retorno = field[1].ToString();

            return retorno;
        }

        public String ObtenerLDAP()
        {
            String lsLDAP = String.Empty;

            try
            {
                GlobalBL oGlobalBL = new GlobalBL();
                lsLDAP = oGlobalBL.Obtener("COMUN", "ADDRESS_LDAP");
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return lsLDAP;
        }

        public void RegExcepcion(System.Web.UI.Page PageActual, string sExcepcion)
        {
            string PopUpWindowPage = "../Excepcion/Excepcion.aspx?Prm=" + "2" + ";";

            StringBuilder Script = new StringBuilder();
            Script.Append("\n<script language=JavaScript id='PopupWindow'>\n");
            Script.Append("var confirmWin = null; \n");
            Script.Append("confirmWin = window.showModalDialog('" + PopUpWindowPage + "','anycontent','width=300,height=100,left=600,top=400,status'); \n");

            Script.Append("if((confirmWin != null) && (confirmWin.opener==null)){ \n");
            Script.Append("  confirmWin.opener = self; \n");
            Script.Append("}\n");
            Script.Append("</script>");
  

            PageActual.ClientScript.RegisterStartupScript(this.GetType(), "PopupWindow", Script.ToString(), false);
        }

        public String DevuelveMes(String Mes)
        {
            String sMes = String.Empty;

            switch (Mes)
            {
                case "JAN":
                    sMes = "ENERO";
                    break;
                case "FEB":
                    sMes = "FEBRERO";
                    break;
                case "MAR":
                    sMes = "MARZO";
                    break;
                case "APR":
                    sMes = "ABRIL";
                    break;
                case "MAY":
                    sMes = "MAYO";
                    break;
                case "JUN":
                    sMes = "JUNIO";
                    break;
                case "JUL":
                    sMes = "JULIO";
                    break;
                case "AUG":
                    sMes = "AGOSTO";
                    break;
                case "SEP":
                    sMes = "SETIEMBRE";
                    break;
                case "OCT":
                    sMes = "OCTUBRE";
                    break;
                case "NOV":
                    sMes = "NOVIEMBRE";
                    break;
                default:
                    sMes = "DICIEMBRE";
                    break;
            }
            return sMes;
        }
    }

}
