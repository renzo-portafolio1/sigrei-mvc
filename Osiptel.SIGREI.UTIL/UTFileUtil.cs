﻿
using System;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Osiptel.SISDEA.UTIL
{

    public class FileUtil
    {
        #region Atributos

        public System.String FilePath { get; set; }

        #endregion

        #region Constructor

        public FileUtil()
        {

        }
        public FileUtil(string filePath)
        {
            this.FilePath = filePath;
        }

        #endregion

        #region Metodos

        #region TXT

  
        public string getFileContent(string filePath)
        {
            return System.IO.File.ReadAllText(filePath);
        }

        public ArrayList getLines(StreamReader streamReader)
        {
            ArrayList alLines = new ArrayList();
            string sLine = "";
            while (sLine != null)
            {
                sLine = streamReader.ReadLine();
                if (sLine != null && sLine.Trim() != "")
                    alLines.Add(sLine);
            }
            streamReader.Close();
            return alLines;

        }

        public string getValue(object line)
        {
            return getValue(line.ToString(), new char[] { '\t' }, 1);
        }

        public string getValue(string line, char[] separator, int index)
        {
            string[] values = line.Split(separator);
            string returnValue;
            try
            {
                returnValue = values[index];
            }
            catch (Exception)
            {
                returnValue = "";
            }
            return returnValue;
        }

        public int getRowOfString(ArrayList arrayList, string FindValue)
        {
            int index = 0;
            for (int i = 0; i < arrayList.Count; i++)
            {
                string line = arrayList[i].ToString().Trim();
                if (line.IndexOf(FindValue) >= 0 || line.StartsWith(FindValue))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        private string formatLine(string line)
        {
            string newLine = line;
            for (int i = 0; i < newLine.Length; i++)
            {
                newLine = newLine[i].ToString().Replace(UTConstantes.TILDES.A, UTConstantes.TILDESASCII.A);
                newLine = newLine[i].ToString().Replace(UTConstantes.TILDES.E, UTConstantes.TILDESASCII.E);
                newLine = newLine[i].ToString().Replace(UTConstantes.TILDES.I, UTConstantes.TILDESASCII.I);
                newLine = newLine[i].ToString().Replace(UTConstantes.TILDES.O, UTConstantes.TILDESASCII.O);
                newLine = newLine[i].ToString().Replace(UTConstantes.TILDES.U, UTConstantes.TILDESASCII.U);
            }
            return newLine;
        }

        #endregion

        #region Excel

        public System.Data.DataTable leerExcel(string ExcelFile)
        {
            return this.leerExcel(ExcelFile, this.getFirstSheet(ExcelFile));
        }

        public System.Data.DataTable leerExcel(string ExcelFile, string sheet)
        {
            System.Data.DataTable dtExcel = new System.Data.DataTable();
            DataSet dsExcel = new DataSet();
            OleDbConnection lcoOleDbConnection = new OleDbConnection();
            try
            {
                string connectionString = "";
                if (ExcelFile.EndsWith("xls"))
                {
                    connectionString = string.Format(ConfigUtil.readConnectionString("EXCEL_CS"), ExcelFile);
                }
                else
                {
                    connectionString = string.Format(ConfigUtil.readConnectionString("EXCEL_CS_2007"), ExcelFile);
                }
                lcoOleDbConnection = new OleDbConnection(connectionString);
                lcoOleDbConnection.Open();
                OleDbDataAdapter lcoOleDbDataAdapter = new OleDbDataAdapter("SELECT * FROM [" + sheet + "] ", lcoOleDbConnection);
                lcoOleDbDataAdapter.Fill(dsExcel);
                lcoOleDbConnection.Close();
                int flag = 0;
                DataTable dtData = new DataTable();
                if (dsExcel.Tables.Count > 0)
                {
                    dtData = dsExcel.Tables[0];
                    dtExcel = dtData.Clone();
                    for (int numfil = 0; numfil < dtData.Rows.Count; numfil++)
                    {
                        flag = 0;
                        for (int numcol = 0; numcol < dtData.Columns.Count; numcol++)
                        {
                            if (dtData.Rows[numfil][numcol].ToString().Trim() != "")
                            {
                                flag = 1;
                            }
                        }

                        if (flag == 1)
                        {
                            dtExcel.Rows.Add(dtData.Rows[numfil].ItemArray);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                lcoOleDbConnection.Close();
            }
            catch (Exception ex)
            {
                if (lcoOleDbConnection.State == ConnectionState.Open)
                    lcoOleDbConnection.Close();
                throw ex;//UTConstantes.MENSAJE.ERROR_LECTURA);
            }
            return dtExcel;
        }

        public string getFirstSheet(string ExcelFile)
        {
            string sheetName = "";
            OleDbConnection lcoOleDbConnection = null;
            System.Data.DataTable dt = null;
            try
            {
                string connectionString = "";
                if (ExcelFile.Trim().ToUpper().EndsWith("XLS"))
                {
                    connectionString = string.Format(ConfigUtil.readConnectionString("EXCEL_CS"), ExcelFile);
                }
                else
                {
                    connectionString = string.Format(ConfigUtil.readConnectionString("EXCEL_CS_2007"), ExcelFile);
                }
                lcoOleDbConnection = new OleDbConnection(connectionString);
                lcoOleDbConnection.Open();
                dt = lcoOleDbConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dt == null)
                {
                    return null;
                }
                foreach (DataRow row in dt.Rows)
                {
                    sheetName = row["TABLE_NAME"].ToString();
                    break;
                }
                lcoOleDbConnection.Close();
            }
            catch (Exception)
            {
                if (lcoOleDbConnection.State == ConnectionState.Open)
                    lcoOleDbConnection.Close();
            }
            return sheetName;
        }

        public void ExportarExcelWeb(WebControl wc, HttpResponse Response, HttpRequest Request, HttpServerUtility Server)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                Page page = new Page();

                HtmlForm form = new HtmlForm();
                wc.EnableViewState = false;
                page.EnableEventValidation = false;
                page.DesignerInitialize();

                HtmlLink link = new HtmlLink();
                string urlEstilos = this.AbsolutePath(Request, ConfigUtil.readValue("cssExcel"));
                link.Attributes.Add("href", urlEstilos);
                link.Attributes.Add("type", "text/css");
                link.Attributes.Add("rel", "stylesheet");
                page.Controls.Add(link);
                page.Controls.Add(form);
                form.Controls.Add(wc);

                page.RenderControl(htw);
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=ArchivoExportarExcel.xls");
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                Response.Write(sb.ToString());
                Response.End();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected string AbsolutePath(HttpRequest Request, String file)
        {
            String end = (Request.ApplicationPath.EndsWith("/")) ? "" : "/";
            String path = Request.ApplicationPath + end;
            String url = Request.Url.Authority;
            return String.Format("http://{0}{1}{2}", url, path, file);
        }

        #endregion

        #endregion
    }

}
