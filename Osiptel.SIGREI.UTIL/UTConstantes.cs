﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Osiptel.SISDEA.UTIL
{    
    public static class UTConstantes
    {
        public static string APLICACION = "SISDEA";

        public struct TILDES
        {
            public static string A = "Á";
            public static string E = "É";
            public static string I = "Í";
            public static string O = "Ó";
            public static string U = "Ú";
        }

        public struct TILDESASCII
        {
            public static string A = "&#193;";
            public static string E = "&#201;";
            public static string I = "&#205;";
            public static string O = "&#211;";
            public static string U = "&#218;";
        }

       public struct APLICACION_TABLA
        {
           public static string TIPOPRESENTACION = "TIPOPRESENTACION";
           public static string TIPOMATERIA = "TIPOMATERIA";        
           public static string TIPOESTADO = "TIPOESTADO";
           public static string TIPORESULTADO = "TIPORESULTADO";
           public static string TIPORESPUESTA = "TIPORESPUESTA";
           public static string TIPORESPUESTAGEN = "TIPORESPUESTAGEN";
        }

        public struct APLICACION_COLUMNA
        {
            public static string IDTIPOPRESENTACION = "IDTIPOPRESENTACION";
            public static string IDTIPOMATERIA = "IDTIPOMATERIA";                        
            public static string IDTIPOESTADO = "IDTIPOESTADO";
            public static string IDTIPORESULTADO = "IDTIPORESULTADO";            
            public static string IDTIPORESPUESTA = "IDTIPORESPUESTA";
            public static string IDTIPORESPUESTAGEN = "IDTIPORESPUESTAGEN";            
        }

        public static string COMBO_SINVALOR = "";
        public static string COMBO_VALORSELECCIONE = "0";
        public static string COMBO_SELECCIONE = "-Seleccionar-";
        public static string COMBO_VALORINICIAL = "-1";
        public static string COMBO_TODOS = "-Todos-";
        public static string COMBO_NINGUNO = "-Ninguno-";
        public static string COMBO_SEL = "-SEL-";

        public static Int32 ID_TIPO_MATERIA_LIBRE_COMPETENCIA = 1;
        public static Int32 ID_TIPO_MATERIA_COMPETENCIA_DESLEAL = 2;

        public static Int32 ID_TIPO_PRESENTACION_ANONINO = 1;
        public static Int32 ID_TIPO_PRESENTACION_NOANONINO = 2;

  
      
        public static Int32 ID_ESTADO_REGISTRADO= 1;
        public static Int32 ID_ESTADO_RECIBIDO = 2;
        public static Int32 ID_ESTADO_ENREVISION = 3;
        public static Int32 ID_ESTADO_CONCLUIDO = 4;
        public static Int32 ID_ESTADO_ANULADO = 5;
        

        public static string ESTADO_REGISTRADO = "REGISTRADO";
        public static string ESTADO_RECIBIDO = "RECIBIDO";
        public static string ESTADO_ENREVISION = "ENREVISION";
        public static string ESTADO_CONCLUIDO = "CONCLUIDO";
        public static string ESTADO_ANULADO = "ANULADO";
               

        public static Int32 ID_TIPORESPUESTA_SI = 1;
        public static Int32 ID_TIPORESPUESTA_NO = 2;
        public static Int32 ID_TIPORESPUESTA_NO_APLICA = 3;

        public static Int32 VALOR_CERO = 0;
        public static Int32 VALOR_UNO = 1;
        public static Int32 VALOR_DOS = 2;
        public static Int32 TAMANIO_UN_KB= 1000;
        public static Int32 TAMANIO_CARGA_DOCUMENTO = 20000000;

        public static string FORMATO_FECHA = "dd/MM/yyyy";
        public static string RANGO_ALTITUD_MIN = "0";
        public static string RANGO_ALTITUD_MAX = "8000";
        public static string RANGO_LATITUD_MIN = "-19";
        public static string RANGO_LATITUD_MAX = "0";
        public static string RANGO_LONGITUD_MIN = "-82";
        public static string RANGO_LONGITUD_MAX = "-68";

        public static string MSG_ERROR_OPERACION = "Ocurrió un error en la operación";
        public static string MSG_ERROR_REGISTRO = "Ocurrió un error al registrar";
        public static string MSG_NOEXISTE_REGISTRO = "No se encontraron registros";
        public static string MSG_EXISTE_REGISTRO = "Existe un registro";
        public static string MSG_ERROR_UBIGEO = "Debe ingresar 10 números en el Ubigeo";
        public static string MSG_ERROR_ARCHIVO = "¡Error al grabar! Nombre de archivo duplicado. Cambiar el nombre del archivo ";
        public static string MSG_REGISTRAR_OK = "Se registró correctamente.";
        public static string MSG_ELIMINAR_OK = "Se eliminó correctamente.";
        public static string MSG_MODIFICAR_OK = "Se modificó correctamente.";
        public static string MSG_ANULAR_OK = "Se anuló correctamente.";
        public static string MSG_PROCESAR_OK = "Se proceso correctamente.";
        public static string MSG_ENVIAR_OK = "Se envió correctamente.";
        public static string MSG_VALIDAR_OK = "Se validó correctamente";
        public static string MSG_INACTIVAR_OK = "Se inactivó correctamente";
        public static string MSG_EXPORTAR_OK = "Se exportó correctamente";
        public static string MSG_APROBAR_OK = "Se aprobó correctamente";
        public static string MSG_DESAPROBAR_OK = "Se desaprobó correctamente";
        public static string MSG_DESCARGAR_OK = "Se descargó correctamente";
        public static string MSG_GENERAR_OK = "Se generó correctamente";
        public static string MSG_ACTIVAR_OK = "Se activó correctamente";
        public static string MSG_DEVOLVER_OK = "Se devolvió correctamente";
        public static string MSG_AGREGAR_OK = "¡Se agrego correctamente a la empresa!";
        public static string MSG_ERROR_EXISTE_EMPRESA = "¡Ya existe la empresa que intentó agregar!";
        public static string MSG_AGREGAR_DOCUMENTO_OK = "¡Se agrego correctamente el documento!";
        public static string MSG_ERROR_EXISTE_DOCUMENTO = "¡Ya existe el documento intentó agregar!";
        public static string MSG_ERROR_TAMANIO_MAXIMO_EXCEDIDO= "¡Se ha excedido el tamaño máximo de carga de documentos!";

        public static string MSG_REVISADO_OK = "Se ha revisado correctamente.";
        public static string MSG_CONCLUIDO_OK = "Se ha concluido correctamente.";

        public static String MSG_REGISTRAR_CONFIRM = "¿Desea registrar?";
        public static String MSG_ELIMINAR_CONFIRM = "¿Desea eliminar el registro seleccionado?";
        public static String MSG_MODIFICAR_CONFIRM = "¿Desea actualizar los cambios realizados?";
        public static String MSG_ANULAR_CONFIRM = "¿Desea anular el registro seleccionado?";
        public static String MSG_COMENTAR_CONFIRM = "¿Desea comentar referente a : {0} ?";
        public static String MSG_AGREGAR_CONFIRM = "¿Desea agregar a la empresa infractora?";
        public static String MSG_AGREGAR_DOCUMENTO_CONFIRM = "¿Desea agregar el documento seleccionado?";

        
        public static string MSG_PROCESAR_CONFIRM = "Desea procesar";
        public static string MSG_ENVIAR_CONFIRM = "Desea enviar";
        public static string MSG_VALIDAR_CONFIRM = "Desea Validar";
        public static string MSG_INACTIVAR_CONFIRM = "Desea inactivar";
        public static string MSG_GENERAR_CONFIRM = "Desea generar";
        public static string MSG_EXPORTAR_CONFIRM = "Desea exportar";
        public static string MSG_APROBAR_CONFIRM = "Desea aprobar";
        public static string MSG_DESAPROBAR_CONFIRM = "Desea desaprobar";
        public static string MSG_CERRAR_CONFIRM = "Desea cerrar";
        public static string MSG_DESCARGAR_CONFIRM = "Desea descargar";
        public static string MSG_ACTIVAR_CONFIRM = "Desea activar";

        
        public static String PREGUNTA_REGISTRAR_ATENCION_REPORTE_CONDUCTA= "¿Desea revisar la denuncia de conducta?";
        public static String PREGUNTA_REGISTRAR_CONCLUIR_REPORTE_CONDUCTA = "¿Desea concluir la denuncia de conducta?";        

        public static String MENSAJE_VALIDACION= "¡Mensaje de validación!";
        

    }

}
