﻿using System;
using System.Collections.Generic;


using Microsoft.AspNetCore.Mvc;

using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;


using docPacking = DocumentFormat.OpenXml.Packaging;
using docWodProcesing = DocumentFormat.OpenXml.Wordprocessing;

using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

using System.Text;
using Microsoft.AspNetCore.Http;
using ClienteSIGREI.Util;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Threading.Tasks;
//using Microsoft.Office.Interop;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Style.XmlAccess;
using System.Xml;
using System.Linq;
using System.Net.Mail;
using NETCore.Encrypt;
using System.Collections;
using System.Drawing;
using Newtonsoft.Json.Linq;
using iTextSharp.text.html;
using iTextSharp.text.pdf.draw;
using Rectangle = iTextSharp.text.Rectangle;
using System.Globalization;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    [Authorize]
    public class RequerimientoEPController : Controller
    {

        public IList<AuthenticationScheme> OtherLogins { get; set; }

        private readonly IConfiguration configuration;

        public RequerimientoEPController(IConfiguration config)
        {
            this.configuration = config;
        }
        public IActionResult Index()
        {
            List<EstadoBE> listaEstado = new GenericoBL().ESTADOSREQEP("S");
            ViewBag.Lista_Estado = listaEstado;
            return View();
        }
        public IActionResult NuevoRequerimientoEP()
        {
            HttpContext.Session.SetString("ListaIMEIREQEP", "");
            HttpContext.Session.SetString("ListaTELEFREQEP", "");
            BeUsuarioEP listaUserEdit;
            BlUsuarioEP objParametro = new BlUsuarioEP();

            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOREQUERIMIENTO, UTConstantes.APLICACION_COLUMNA.IDTIPOREQUERIMIENTO);
            ViewBag.Lista_Tip_Dato = listaTipoDocumento;

            //List<BE_PARAMETRO> listaTipoCarga = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOREQCARGA, UTConstantes.APLICACION_COLUMNA.IDTIPOREQCARGA);
            List<BE_PARAMETRO> listaTipoCarga = ObjLogica.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOREQCARGA, UTConstantes.APLICACION_COLUMNA.IDTIPOREQCARGA);
            ViewBag.Lista_Tip_Carga = listaTipoCarga;

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("S");
            ViewBag.Lista_Estado = listaEstado;

            BL_REPORTE objLogica = new BL_REPORTE();
            ViewBag.ListaRegion = objLogica.ListaComboUbigeo();

            //CONSULTANDO SI TIENE ALGUN REGISTRO PENDIENTE
            BeUsuarioEP listaResultado_Pendiente;
            BL_REQUERIMIENTO objBe = new BL_REQUERIMIENTO();
            listaResultado_Pendiente = objBe.ConsultarCabeceraPendiente(HttpContext.Session.GetString("IdUsuario"));
            if (listaResultado_Pendiente.IDUSUARIO is null || listaResultado_Pendiente.IDUSUARIO == "")
            {
                listaUserEdit = objParametro.SP_OBTENER_USUARIO_EP(HttpContext.Session.GetString("IdUsuario"));
                listaUserEdit.DES_NOMBRE = listaUserEdit.DES_NOMBRE + ' ' + listaUserEdit.DES_NOmBRE_LAST;
                return View(listaUserEdit);
            }
            else
            {
                return View(listaResultado_Pendiente);
            }
            //
           
        }
        [HttpPost]
        public ActionResult ConsultarMovilPendiente(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacionPendienteMovil(formulario);


                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        private KeyValuePair<string, string> CargarInformacionPendienteMovil(IFormCollection formulario)
        {
            String mensaje = "";
            int cont_correcta = 0;
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            //CONSULTAR VALORES DE TELEFONO E IMEI
            BL_REQUERIMIENTO_IMEI objIMEI = new BL_REQUERIMIENTO_IMEI();
            List<BE_REQUERIMIENTO_IMEI> listaResultadoMovil = new List<BE_REQUERIMIENTO_IMEI>();

            listaResultadoMovil = objIMEI.ConsultarMovilPendietes(formulario["DES_PUESTO"].ToString());
            string cadena_html_user = "";
            if (formulario["Lista_Tip_Carga"].ToString() == "2")
            {

                cadena_html_user += "<div class='tabla-content-result'>";
                cadena_html_user += "<table id='dtResultadCarMasMovilEP' class='tabla-osiptel' style='width: 100%'>";
                cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_html_user += " <thead>";
                cadena_html_user += "<tr>";
                cadena_html_user += "<th scope='col'>Nº</th>";
                cadena_html_user += "<th scope='col'>Dato</th>";
                cadena_html_user += "<th scope='col'>Revisión Preliminar</th>";
                cadena_html_user += "<th scope='col'>¿Procede a Cargar?</th>";
                //cadena_html_user += "<th scope='col'>Sustento</th>";
                cadena_html_user += "</tr>";
                cadena_html_user += " </thead>";
                cadena_html_user += "<tbody>";
                for (int i = 0; i < listaResultadoMovil.Count; i++)
                {
                    cadena_html_user += " <tr>";
                    cadena_html_user += "<td> " + listaResultadoMovil[i].nroItem.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaResultadoMovil[i].nroImei.ToString() + "</td>";
                    string des_pre = listaResultadoMovil[i].validacionFormato.ToString() + " - " + listaResultadoMovil[i].estadoLuhn;
                    string proc_cargar = "No procede";
                    if (listaResultadoMovil[i].validacionFormato.Trim() == "" )
                    {
                        des_pre = "Correcto";
                        proc_cargar = "Si";
                    }
                    int pos_cortar = 15;
                    if (des_pre.ToString().Length < 16)
                    {
                        pos_cortar = des_pre.ToString().Length;
                    }
                    if (proc_cargar.Trim() == "No procede")
                    {
                        cadena_html_user += "<td style='color: red;'> " + des_pre.ToString().Substring(0, pos_cortar) + "...</td>";

                        cadena_html_user += "<td style='color: red;'> " + proc_cargar.ToString() + "</td>";
                    }
                    else
                    {
                        cadena_html_user += "<td> " + des_pre.ToString().Substring(0, pos_cortar) + "...</td>";

                        cadena_html_user += "<td> " + proc_cargar.ToString() + "</td>";
                    }
                    //cadena_html_user += "<td> " + listaResultadoMovil[i].validacionFormato.ToString() + " - " + listaResultadoMovil[i].estadoLuhn + "</td>";
                    cadena_html_user += "</tr>";



                }

                cadena_html_user += " </tbody>";
                cadena_html_user += "   </table>";
                cadena_html_user += "</div>";
            }
            else
            {
                for (int i = 0; i < listaResultadoMovil.Count; i++)
                {
                    string des_pre = "Incorrecto";
                    string proc_cargar = "No procede";
                    if (listaResultadoMovil[i].validacionFormato.Trim() == "" )
                    {
                        des_pre = "Correcto";
                        proc_cargar = "Sí procede";
                    }
                    cadena_html_user += des_pre.ToString() + "    -    " + proc_cargar.ToString() + "   -    " + listaResultadoMovil[i].validacionFormato.ToString() + " / " + listaResultadoMovil[i].estadoLuhn;



                }
            }
            //
            keyValueError = new KeyValuePair<string, string>("1", "Carga Completa" + "//" + cadena_html_user);

            return keyValueError;
        }

        [HttpPost]
        public ActionResult ConsultarImeiPendiente(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacionPendienteImei(formulario);


                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        private KeyValuePair<string, string> CargarInformacionPendienteImei(IFormCollection formulario)
        {
            String mensaje = "";
            int cont_correcta = 0;
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            //CONSULTAR VALORES DE TELEFONO E IMEI
            BL_REQUERIMIENTO_IMEI objIMEI = new BL_REQUERIMIENTO_IMEI();
            List<BE_REQUERIMIENTO_IMEI> listaResultadoImei = new List<BE_REQUERIMIENTO_IMEI>();

            listaResultadoImei = objIMEI.ConsultarImeiPendietes(formulario["DES_PUESTO"].ToString());
            string cadena_html_user = "";
            if (formulario["Lista_Tip_Carga"].ToString() == "2")
            {
               
                cadena_html_user += "<div class='tabla-content-result'>";
                cadena_html_user += "<table id='dtResultadCarMasImeiEP' class='tabla-osiptel' style='width: 100%'>";
                cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_html_user += " <thead>";
                cadena_html_user += "<tr>";
                cadena_html_user += "<th scope='col'>Nº</th>";
                cadena_html_user += "<th scope='col'>Dato</th>";
                cadena_html_user += "<th scope='col'>Revisión Preliminar</th>";
                cadena_html_user += "<th scope='col'>¿Procede a Cargar?</th>";
                //cadena_html_user += "<th scope='col'>Sustento</th>";
                cadena_html_user += "</tr>";
                cadena_html_user += " </thead>";
                cadena_html_user += "<tbody>";
                for (int i = 0; i < listaResultadoImei.Count; i++)
                {
                    cadena_html_user += " <tr>";
                    cadena_html_user += "<td> " + listaResultadoImei[i].nroItem.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaResultadoImei[i].nroImei.ToString() + "</td>";
                    string des_pre = listaResultadoImei[i].validacionFormato.ToString() + " - " + listaResultadoImei[i].estadoLuhn;
                    string proc_cargar = "No procede";
                    if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].estadoLuhn.Trim() == "Lunh válido")
                    //if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].ultimoDigito != 0)
                    {
                        des_pre = "Correcto";
                        proc_cargar = "Si";
                    }
                    int pos_cortar = 15;
                    if (des_pre.ToString().Length < 16)
                    {
                        pos_cortar = des_pre.ToString().Length;
                    }
                    if (proc_cargar.Trim() == "No procede")
                    {
                        cadena_html_user += "<td style='color: red;'> " + des_pre.ToString().Substring(0, pos_cortar) + "...</td>";

                        cadena_html_user += "<td style='color: red;'> " + proc_cargar.ToString() + "</td>";
                    }
                    else
                    {
                        cadena_html_user += "<td> " + des_pre.ToString().Substring(0, pos_cortar) + "...</td>";

                        cadena_html_user += "<td> " + proc_cargar.ToString() + "</td>";
                    }
                    //cadena_html_user += "<td> " + listaResultadoImei[i].validacionFormato.ToString() + " - " + listaResultadoImei[i].estadoLuhn + "</td>";
                    cadena_html_user += "</tr>";



                }

                cadena_html_user += " </tbody>";
                cadena_html_user += "   </table>";
                cadena_html_user += "</div>";
            }
            else
            {
                for (int i = 0; i < listaResultadoImei.Count; i++)
                {
                    string des_pre = "Incorrecto";
                    string proc_cargar = "No procede";
                    if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].estadoLuhn.Trim() == "Lunh válido")
                    //if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].ultimoDigito != 0)
                    {
                        des_pre = "Correcto";
                        proc_cargar = "Sí procede";
                    }
                    cadena_html_user += des_pre.ToString() + "    -    " + proc_cargar.ToString() + "   -    " + listaResultadoImei[i].validacionFormato.ToString() + " / " + listaResultadoImei[i].estadoLuhn;



                }
            }
            //
            keyValueError = new KeyValuePair<string, string>("1", "Carga Completa" + "//" + cadena_html_user);

            return keyValueError;
        }

        public FileResult DescargarPlantillaTelef()
        {
            //var rutaManual = configuration["CredencialUsuario"];
            var rutaManual = configuration["PlantillasSIGREI"];
            //String nombrePlantilla = configuration["PlantillasSIGREI"] + "\\PlantillaReqMas.xlsx";
            String nombrePlantilla = "FORMATO CARGA MASIVA.xlsx";
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public FileResult DescargarPlantillaImei()
        {
            //var rutaManual = configuration["CredencialUsuario"];
            var rutaManual = configuration["PlantillasSIGREI"];
            //String nombrePlantilla = configuration["PlantillasSIGREI"] + "\\PlantillaReqMas.xlsx";
            String nombrePlantilla = "FORMATO CARGA MASIVA.xlsx";
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult CargarInformacionMasivaImei(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacion(formulario);
 

                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }
        int validar_error_general = 0;
        private KeyValuePair<string, string> CargarInformacion(IFormCollection formulario)
        {
            String mensaje = "";
            int cont_correcta = 0;
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            Boolean existeRegistro = false;

            /*List<ErrorCarMasUser> listaErrors = new List<ErrorCarMasUser>();
            var listaErrorJSON = JsonConvert.SerializeObject(listaErrors);
            HttpContext.Session.SetString("listaErrorCargaMasivaImei", listaErrorJSON);*/

            string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileCargaMasivaImei"].FileName);

            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            //if (archivo.ContentLength / 1024 < 2000)//2MB
            IFormFile archivo = formulario.Files["fileCargaMasivaImei"];
            if (archivo.Length / 1024 < TamFileGrande)//2MB
            {
                if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                {
                    keyValueError = new KeyValuePair<string, string>("", "");
                }
                else
                {
                    mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                    keyValueError = new KeyValuePair<string, string>("0", mensaje);
                }
            }
            else
            {
                keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);

            }

            if (keyValueError.Key == "")
            {
                List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
                BE_REQUERIMIENTO objParametroDos = new BE_REQUERIMIENTO();

                #region "Copiar a una carpeta temporal para revisar si hay registro que registrar"


                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                IFormFile fileArchivoImei = formulario.Files["fileCargaMasivaImei"];
                var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                string nombreArchivoImei = Path.GetFileName(formulario.Files["fileCargaMasivaImei"].FileName);

                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                var ubicacionArchivoTemporal = "";

                if (fileArchivoImei.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileArchivoImei.CopyTo(fileStream);
                    }
                    ubicacionArchivoTemporal = filePath;
                }
                FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                HttpContext.Session.SetString("nombreArchivoCarMasImei", nombreArchivoImei);

                using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                {
                    try
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets["Plantilla Carga Masiva"];
                        int totalRows = workSheet.Dimension.Rows;

                        if (totalRows < 2)
                        {
                            return keyValueError = new KeyValuePair<string, string>("2", "No existe información en el archivo adjunto");
                        }
                        else
                        {
                            try
                            {
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    objParametroDos = new BE_REQUERIMIENTO();
                                    objParametroDos.nroItem = i;
                                    objParametroDos.nroImei = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim().Replace(",", ".") : "";
                               
                                    listaResultado.Add(objParametroDos);

                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "CargarInformacion=> error al cargar la data ");
                                return keyValueError = new KeyValuePair<string, string>("0", "Error en la obtención de los datos");
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarErrorMetodo(ex, "CargarInformacion=> error al cargar la data ");
                        return keyValueError = new KeyValuePair<string, string>("0", UTConstantes.PLANTILLA_EXCEL_NO_EXISTE_HOJA_DATA);
                    }
                }
                #endregion

                if (listaResultado == null)
                {
                    existeRegistro = false;
                }
                else if (listaResultado.Count == 0)
                {
                    existeRegistro = false;
                }
                else
                {
                    existeRegistro = true;
                }
                string cadena_imei_general = "";
                if (existeRegistro)
                {

                    //listaResultado = listaResultado.OrderBy(o => o.nroImei).ToList();

                    if (listaResultado != null)
                    {
                        if (listaResultado.Count > 0)
                        {
                            //int validar = 0;
                            for (int i = 0; i < listaResultado.Count; i++)
                            {
                                validar_error_general = 0;
                                //listaResultado[i] = validarDocumentoExcel(listaResultado[i], listaResultado);
                                if (listaResultado[i].nroImei != "")
                                {
                                    cadena_imei_general += listaResultado[i].nroImei + "//";
                                }
                                
                                if (validar_error_general == 1)
                                {
                                    cont_correcta++;
                                }
                            }
                        }
                    }
                    HttpContext.Session.SetString("ListaIMEIREQEP", "");
                    //CONSULTAR DE FORMA GENERAL VALIDACIONES DE IMEI
                    BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
                    listaResultado = ObjLogica.ConsultaEstadoImei(cadena_imei_general, HttpContext.Session.GetString("IdUsuario"));
                    //
                    var listaSesionJSONEP = JsonConvert.SerializeObject(listaResultado);
                    HttpContext.Session.SetString("ListaIMEIREQEP", listaSesionJSONEP);
                    /*BeUsuarioEP ObjUserEP = new BeUsuarioEP();
                    List<BeUsuarioEP> listaUEP_Grilla = new List<BeUsuarioEP>();*/

                    string cadena_html_user = "";
                    cadena_html_user += "<div class='tabla-content-result'>";
                    cadena_html_user += "<table id='dtResultadCarMasImeiEP' class='tabla-osiptel' style='width: 100%'>";
                    cadena_html_user += "<caption style='visibility: hidden;'>Detalle Parametro</caption>";
                    cadena_html_user += "<thead>";
                    cadena_html_user += "<tr>";
                   
                    cadena_html_user += "<th scope='col'>Nº</th>";
                    cadena_html_user += "<th scope='col'>Dato</th>";
                    cadena_html_user += "<th scope='col'>Revisión Preliminar</th>";
                    cadena_html_user += "<th scope='col'>¿Procede a Cargar?</th>";
                    cadena_html_user += "<th scope='col'>Accion</th>";
                    cadena_html_user += "<th scope='col' style='display: none;'>Tipo</th>";
                    cadena_html_user += "</tr>";
                    cadena_html_user += "</thead>";
                    cadena_html_user += "<tbody>";

                    string cadena_tip_doc = "";
                    string cadena_num_doc = "";
                    string cadena_num_tlf = "";
                    string cadena_num_cel = "";
                    string cadena_tip_sexo = "";
                    string cadena_correo = "";
                    string cadena_prim_nombre = "";
                    string cadena_sec_nombre = "";
                    string cadena_pri_apellido = "";
                    string cadena_sec_apellido = "";
                    string cadena_sec_direccion = "";

                    for (int i = 0; i < listaResultado.Count; i++)
                    {
                        cadena_html_user += " <tr>";
                        
                        cadena_html_user += "<td>" + listaResultado[i].nroItem.ToString() + "</td>";
                        cadena_html_user += "<td>" + listaResultado[i].nroImei.ToString() + "</td>";

                        if (IsNumeric(listaResultado[i].nroImei.ToString()) && listaResultado[i].nroImei.ToString().Contains(" ") != true)
                        {
                            if (listaResultado[i].nroImei.ToString().Length == 15)
                            {
                                if(listaResultado[i].idMovSgd == 99)
                                {
                                    cadena_html_user += "<td style='color: red;'>IMEI no reportado como SUSTRAÍDO o PERDIDO.</td>";
                                    cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                }
                                else
                                {
                                    if (listaResultado[i].estadoLuhn.Trim() == "Lunh valido" || listaResultado[i].estadoLuhn.Trim() == "Lunh válido")
                                    {
                                        if (listaResultado[i].MOTIVOREPORTE.Trim() == "Sustraído" || listaResultado[i].MOTIVOREPORTE.Trim() == "Perdido")
                                        {
                                            cadena_html_user += "<td>Correcto</td>";
                                            cadena_html_user += "<td>Sí Procede</td>";
                                        }
                                        else if (listaResultado[i].MOTIVOREPORTE.Trim() == "Recuperado")
                                        {
                                            cadena_html_user += "<td style='color: red;'>IMEI reportado como RECUPERADO.</td>";
                                            cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                        }
                                        else
                                        {
                                            if (listaResultado[i].nombreSolicitante.Trim() == "I")
                                            {
                                                cadena_html_user += "<td style='color: red;'>IMEI reportado como CAMPAÑA BLOQUEO INVÁLIDO</td>";
                                                cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                            }
                                            else
                                            {
                                                cadena_html_user += "<td style='color: red;'>IMEI no reportado como SUSTRAÍDO o PERDIDO.</td>";
                                                cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                            }
                                            
                                        }
                                    }
                                    else
                                    {
                                        cadena_html_user += "<td style='color: red;'>IMEI no cumple algoritmo de verificación.</td>";
                                        cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                    }
                                }
                                
                            }
                            else
                            {
                                cadena_html_user += "<td style='color: red;'>No tiene 15 dígitos.</td>";
                                cadena_html_user += "<td style='color: red;'>No Procede</td>";
                            }
                        }
                        else
                        {
                            cadena_html_user += "<td style='color: red;'>Tiene caracteres no numéricos o espacios.</td>";
                            cadena_html_user += "<td style='color: red;'>No Procede</td>";
                        }
                        
                        //cadena_html_user += "<td> " + listaResultado[i].validacionFormato.ToString() +" - "+ listaResultado[i].estadoLuhn+"</td>";
                        cadena_html_user += "<td style='text-align: center'><a onclick='btnBorrarFilaImei();' ><i class='fa fa-trash fa-lg borrar' aria-hidden='true'></i></a></td>";
                        cadena_html_user += "<td style='display: none !important;'>" + listaResultado[i].nombreSolicitante.Trim() + "</td>";
                        cadena_html_user += "</tr>";
                    }

                    cadena_html_user += "</tbody>";
                    cadena_html_user += "</table>";
                    cadena_html_user += "</div>";

                    keyValueError = new KeyValuePair<string, string>("1", "Carga Completa" + "//" + cadena_html_user );
                }
            }

            return keyValueError;
        }

        private BE_REQUERIMIENTO validarDocumentoExcel(BE_REQUERIMIENTO objParametro, List<BE_REQUERIMIENTO> lista)
        {
            /*List<ErrorImei> listaErrors = new List<ErrorImei>();
            listaErrors = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("listaErrorImeiMasReqEP"));

            if (listaErrors == null) { listaErrors = new List<ErrorImei>(); }*/


            String error = "";
            ErrorImei objError = new ErrorImei();
            objError.nroImei = objParametro.nroImei;
            //VALIDANDO LONGITUD Y ALFANUMERICO
            if (objParametro.nroImei.Length != 15)
            {
                error += "el número de IMEI debe tener una longitud de 15 caracteres";
                error += ", ";
            }
            else
            {
                long numservicio = 0;
                bool isNumero = long.TryParse(objParametro.nroImei, out numservicio);
                if (!isNumero)
                {
                    error += "El número de IMEI contiene caracteres que no es numérico";
                    error += ", ";
                }
            }
            //VALIDANDO ALGORITMO DE LUHN, LISTA NEGRA,BLOQUEO DE CAMPAÑA
            if (error =="")
            {
                BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
                ///List<BE_REQUERIMIENTO> LïstaConsultImei = ObjLogica.ConsultaEstadoImei(objParametro.nroImei);
                //error += LïstaConsultImei[0].estadoLuhn+", ";
            }
           
            //

            objParametro.estadoIMEI = error;
            objParametro.isRevisado = true;
            return objParametro;
        }

        [HttpPost]
        public ActionResult CargarInformacionMasivaMovil(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacionMovil(formulario);


                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        private KeyValuePair<string, string> CargarInformacionMovil(IFormCollection formulario)
        {
            String mensaje = "";
            int cont_correcta = 0;
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            Boolean existeRegistro = false;

            /*List<ErrorCarMasUser> listaErrors = new List<ErrorCarMasUser>();
            var listaErrorJSON = JsonConvert.SerializeObject(listaErrors);
            HttpContext.Session.SetString("listaErrorCargaMasivaImei", listaErrorJSON);*/

            string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileCargaMasivaMovil"].FileName);

            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            //if (archivo.ContentLength / 1024 < 2000)//2MB
            IFormFile archivo = formulario.Files["fileCargaMasivaMovil"];
            if (archivo.Length / 1024 < TamFileGrande)//2MB
            {
                if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                {
                    keyValueError = new KeyValuePair<string, string>("", "");
                }
                else
                {
                    mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                    keyValueError = new KeyValuePair<string, string>("0", mensaje);
                }
            }
            else
            {
                keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);

            }

            if (keyValueError.Key == "")
            {
                List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
                BE_REQUERIMIENTO objParametroDos = new BE_REQUERIMIENTO();

                #region "Copiar a una carpeta temporal para revisar si hay registro que registrar"


                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                IFormFile fileArchivoImei = formulario.Files["fileCargaMasivaMovil"];
                var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                string nombreArchivoImei = Path.GetFileName(formulario.Files["fileCargaMasivaMovil"].FileName);

                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                var ubicacionArchivoTemporal = "";

                if (fileArchivoImei.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileArchivoImei.CopyTo(fileStream);
                    }
                    ubicacionArchivoTemporal = filePath;
                }
                FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                HttpContext.Session.SetString("nombreArchivoCarMasMovil", nombreArchivoImei);

                using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                {
                    try
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets["Plantilla Carga Masiva"];
                        int totalRows = workSheet.Dimension.Rows;

                        if (totalRows < 2)
                        {
                            return keyValueError = new KeyValuePair<string, string>("2", "No existe información en el archivo adjunto");
                        }
                        else
                        {
                            try
                            {
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    objParametroDos = new BE_REQUERIMIENTO();
                                    objParametroDos.nroItem = i;
                                    objParametroDos.nroImeiDos = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim().Replace(",", ".") : "";

                                    listaResultado.Add(objParametroDos);

                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "CargarInformacion=> error al cargar la data ");
                                return keyValueError = new KeyValuePair<string, string>("0", "Error en la obtención de los datos");
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarErrorMetodo(ex, "CargarInformacion=> error al cargar la data ");
                        return keyValueError = new KeyValuePair<string, string>("0", UTConstantes.PLANTILLA_EXCEL_NO_EXISTE_HOJA_DATA);
                    }
                }
                #endregion

                if (listaResultado == null)
                {
                    existeRegistro = false;
                }
                else if (listaResultado.Count == 0)
                {
                    existeRegistro = false;
                }
                else
                {
                    existeRegistro = true;
                }
                String cadena_telefe_general = "";
                if (existeRegistro)
                {

                    //listaResultado = listaResultado.OrderBy(o => o.nroImeiDos).ToList();

                    if (listaResultado != null)
                    {
                        if (listaResultado.Count > 0)
                        {
                            //int validar = 0;
                            for (int i = 0; i < listaResultado.Count; i++)
                            {
                                validar_error_general = 0;
                                //listaResultado[i] = validarDocumentoExcelMovil(listaResultado[i], listaResultado);
                                if (listaResultado[i].nroImeiDos != "")
                                {
                                    cadena_telefe_general += listaResultado[i].nroImeiDos + "//";
                                }
                                
                                if (validar_error_general == 1)
                                {
                                    cont_correcta++;
                                }
                            }
                        }
                    }

                    HttpContext.Session.SetString("ListaTELEFREQEP", "");
                    //CONSULTAR DE FORMA GENERAL VALIDACIONES DE IMEI
                    BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
                    listaResultado = ObjLogica.ConsultaEstadoTelefono(cadena_telefe_general, HttpContext.Session.GetString("IdUsuario"));
                    //
                    

                    /*BeUsuarioEP ObjUserEP = new BeUsuarioEP();
                    List<BeUsuarioEP> listaUEP_Grilla = new List<BeUsuarioEP>();*/

                    string cadena_html_user = "";
                    cadena_html_user += "<div class='tabla-content-result'>";
                    cadena_html_user += "<table id='dtResultadCarMasMovilEP' class='tabla-osiptel' style='width: 100%'>";
                    cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                    cadena_html_user += " <thead>";
                    cadena_html_user += "<tr>";
                    cadena_html_user += "<th scope='col'>Nº</th>";
                    cadena_html_user += "<th scope='col'>Dato</th>";
                    cadena_html_user += "<th scope='col'>Revisión Preliminar</th>";
                    cadena_html_user += "<th scope='col'>¿Procede a Cargar?</th>";
                    cadena_html_user += "<th scope='col'>Accion</th>";
                    cadena_html_user += "</tr>";
                    cadena_html_user += " </thead>";
                    cadena_html_user += "<tbody>";

                    string cadena_tip_doc = "";
                    string cadena_num_doc = "";
                    string cadena_num_tlf = "";
                    string cadena_num_cel = "";
                    string cadena_tip_sexo = "";
                    string cadena_correo = "";
                    string cadena_prim_nombre = "";
                    string cadena_sec_nombre = "";
                    string cadena_pri_apellido = "";
                    string cadena_sec_apellido = "";
                    string cadena_sec_direccion = "";

                    for (int i = 0; i < listaResultado.Count; i++)
                    {
                        cadena_html_user += " <tr>";
                        cadena_html_user += "<td>" + listaResultado[i].nroItem.ToString() + "</td>";
                        cadena_html_user += "<td>" + listaResultado[i].nroImeiDos.ToString() + "</td>";
                        if (IsNumeric(listaResultado[i].nroImeiDos.ToString()) && listaResultado[i].nroImeiDos.ToString().Contains(" ") != true)
                        {
                            if (listaResultado[i].nroImeiDos.ToString().Length == 9)
                            {
                                if (listaResultado[i].validacionFormato.Trim() == "")
                                {
                                    cadena_html_user += "<td>Correcto</td>";
                                    cadena_html_user += "<td>Sí procede</td>";
                                    listaResultado[i].validacionFormato = "";
                                }
                                else
                                {
                                    cadena_html_user += "<td style='color: red;'>No se encuentra en el Registro de Abonados</td>";
                                    cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                    listaResultado[i].validacionFormato = "No se encuentra en el Registro de Abonados";
                                }
                            }
                            else
                            {
                                cadena_html_user += "<td style='color: red;'>No tiene 9 dígitos.</td>";
                                cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                listaResultado[i].validacionFormato = "No tiene 9 dígitos.";
                            }
                        }
                        else
                        {
                            cadena_html_user += "<td style='color: red;'>Tiene caracteres no numéricos o espacios.</td>";
                            cadena_html_user += "<td style='color: red;'>No Procede</td>";
                            listaResultado[i].validacionFormato = "Tiene caracteres no numéricos o espacios.";
                        }
                        cadena_html_user += "<td style='text-align: center'><a onclick='btnBorrarFila();' ><i class='fa fa-trash fa-lg borrar' aria-hidden='true'></i></a></td>";
                        cadena_html_user += "</tr>";
                    }

                    cadena_html_user += "</tbody>";
                    cadena_html_user += "</table>";
                    cadena_html_user += "</div>";
                    var listaSesionJSONEP = JsonConvert.SerializeObject(listaResultado);
                    HttpContext.Session.SetString("ListaTELEFREQEP", listaSesionJSONEP);
                    /*BlUsuarioEP objLogicaSolicitud = new BlUsuarioEP();
                    var respuesta_con = "";

                    respuesta_con = objLogicaSolicitud.InsertarDatosTemporalUser(cadena_tip_doc, cadena_num_doc, cadena_num_tlf, cadena_num_cel, cadena_tip_sexo, cadena_correo,
                            cadena_prim_nombre, cadena_sec_nombre, cadena_pri_apellido, cadena_sec_apellido, cadena_sec_direccion);*/

                    keyValueError = new KeyValuePair<string, string>("1", "Carga Completa" + "//" + cadena_html_user);
                }
            }

            return keyValueError;
        }
        private BE_REQUERIMIENTO validarDocumentoExcelMovil(BE_REQUERIMIENTO objParametro, List<BE_REQUERIMIENTO> lista)
        {
            /*List<ErrorImei> listaErrors = new List<ErrorImei>();
            listaErrors = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("listaErrorImeiMasReqEP"));

            if (listaErrors == null) { listaErrors = new List<ErrorImei>(); }*/


            String error = "";
            ErrorImei objError = new ErrorImei();
            objError.nroImei = objParametro.nroImeiDos;
            //VALIDANDO LONGITUD Y ALFANUMERICO
            if (objParametro.nroImeiDos.Length != 9)
            {
                error += "el número de IMEI debe tener una longitud de 9 caracteres";
                error += ", ";
            }
            else
            {
                long numservicio = 0;
                bool isNumero = long.TryParse(objParametro.nroImeiDos, out numservicio);
                if (!isNumero)
                {
                    error += "El número de IMEI contiene caracteres que no es numérico";
                    error += ", ";
                }
            }
         
            //
            /* if (error != "")
             {
                 error = error.Substring(0, error.Length - 2);
                 objError.nroItem = objParametro.nroItem;
                 objError.codError = String.Format("La fila {0} tiene el siguiente error: ", objParametro.nroItem) + error;
                 listaErrors.Add(objError);

                 var listaErrorJSON = JsonConvert.SerializeObject(listaErrors);
                 HttpContext.Session.SetString("listaErrorGPSSisDoc", listaErrorJSON);

             }*/
            objParametro.estadoIMEI = error;
            objParametro.isRevisado = true;
            return objParametro;
        }

        [HttpPost]
        public ActionResult CargarInformacionImei(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacionImeiUnic(formulario);


                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        private KeyValuePair<string, string> CargarInformacionImeiUnic(IFormCollection formulario)
        {
            String mensaje = "";
            int cont_correcta = 0;
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            Boolean existeRegistro = false;



            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
            BE_REQUERIMIENTO objParametroDos = new BE_REQUERIMIENTO();

            objParametroDos = new BE_REQUERIMIENTO();
            objParametroDos.nroItem = 1;
            objParametroDos.nroImei = formulario["txtImeiIndiv"].ToString();

            listaResultado.Add(objParametroDos);

            if (listaResultado == null)
            {
                existeRegistro = false;
            }
            else if (listaResultado.Count == 0)
            {
                existeRegistro = false;
            }
            else
            {
                existeRegistro = true;
            }
            string cadena_imei_general = "";
            if (existeRegistro)
            {

                //listaResultado = listaResultado.OrderBy(o => o.nroImei).ToList();

                if (listaResultado != null)
                {
                    if (listaResultado.Count > 0)
                    {
                        //listaResultado[0] = validarDocumentoExcel(listaResultado[0], listaResultado);
                        cadena_imei_general += listaResultado[0].nroImei + "//";
                    }
                }
                HttpContext.Session.SetString("ListaIMEIREQEP", "");
                //CONSULTAR DE FORMA GENERAL VALIDACIONES DE IMEI
                BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
                listaResultado = ObjLogica.ConsultaEstadoImei(cadena_imei_general, HttpContext.Session.GetString("IdUsuario"));
                //
                var listaSesionJSONEP = JsonConvert.SerializeObject(listaResultado);
                HttpContext.Session.SetString("ListaIMEIREQEP", listaSesionJSONEP);
                /*BeUsuarioEP ObjUserEP = new BeUsuarioEP();
                List<BeUsuarioEP> listaUEP_Grilla = new List<BeUsuarioEP>();*/
                string cadena_html_user = "";
                cadena_html_user += "<div class='tabla-content-result'>";
                cadena_html_user += "<table id='dtResultadCarMasImeiEP' class='tabla-osiptel' style='width: 100%'>";
                cadena_html_user += "<caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_html_user += "<thead>";
                cadena_html_user += "<tr>";
                
                cadena_html_user += "<th scope='col'>Nº</th>";
                cadena_html_user += "<th scope='col'>Dato</th>";
                cadena_html_user += "<th scope='col'>Revisión Preliminar</th>";
                cadena_html_user += "<th scope='col'>¿Procede a Cargar?</th>";
                cadena_html_user += "<th scope='col'>Accion</th>";
                cadena_html_user += "<th scope='col' style='display: none;'>Tipo</th>";
                cadena_html_user += "</tr>";
                cadena_html_user += "</thead>";
                cadena_html_user += "<tbody>";

                if (listaResultado.Count > 0)
                {
                    for (int i = 0; i < listaResultado.Count; i++)
                    {
                        cadena_html_user += "<tr>";
                       
                        cadena_html_user += "<td>" + listaResultado[i].nroItem.ToString() + "</td>";
                        cadena_html_user += "<td>" + listaResultado[i].nroImei.ToString() + "</td>";

                        if (IsNumeric(listaResultado[i].nroImei.ToString()) && listaResultado[i].nroImei.ToString().Contains(" ") != true)
                        {
                            if (listaResultado[i].nroImei.ToString().Length == 15)
                            {
                                if (listaResultado[i].estadoLuhn.Trim() == "Lunh valido" || listaResultado[i].estadoLuhn.Trim() == "Lunh válido")
                                {
                                    if (listaResultado[i].MOTIVOREPORTE.Trim() == "Sustraído" || listaResultado[i].MOTIVOREPORTE.Trim() == "Perdido")
                                    {
                                        cadena_html_user += "<td>Correcto</td>";
                                        cadena_html_user += "<td>Sí Procede</td>";
                                    }
                                    else if (listaResultado[i].MOTIVOREPORTE.Trim() == "Recuperado")
                                    {
                                        cadena_html_user += "<td style='color: red;'>IMEI reportado como RECUPERADO.</td>";
                                        cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                    }
                                    else
                                    {
                                        if (listaResultado[i].nombreSolicitante.Trim() == "I")
                                        {
                                            cadena_html_user += "<td style='color: red;'>IMEI reportado como CAMPAÑA BLOQUEO INVÁLIDO</td>";
                                            cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                        }
                                        else
                                        {
                                            cadena_html_user += "<td style='color: red;'>IMEI no reportado como SUSTRAÍDO o PERDIDO.</td>";
                                            cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    cadena_html_user += "<td style='color: red;'>IMEI no cumple algoritmo de verificación.</td>";
                                    cadena_html_user += "<td style='color: red;'>No Procede</td>";
                                }
                            }
                            else
                            {
                                cadena_html_user += "<td style='color: red;'>No tiene 15 dígitos.</td>";
                                cadena_html_user += "<td style='color: red;'>No Procede</td>";
                            }
                        }
                        else
                        {
                            cadena_html_user += "<td style='color: red;'>Tiene caracteres no numéricos o espacios.</td>";
                            cadena_html_user += "<td style='color: red;'>No Procede</td>";
                        }

                        cadena_html_user += "<td style='text-align: center'><a onclick='btnBorrarFilaImei();' ><i class='fa fa-trash fa-lg borrar' aria-hidden='true'></i></a></td>";
                        cadena_html_user += "<td style='display: none;'>" + listaResultado[i].nombreSolicitante.Trim() + "</td>";
                        cadena_html_user += "</tr>";
                    }
                }
                else
                {
                    cadena_html_user += "<tr>";
                    cadena_html_user += "<td>1</td>";
                    cadena_html_user += "<td>" + formulario["txtImeiIndiv"].ToString() + "</td>";
                    cadena_html_user += "<td style='color: red;'>IMEI no reportado como SUSTRAÍDO o PERDIDO.</td>";
                    cadena_html_user += "<td style='color: red;'>No Procede</td>";
                    cadena_html_user += "<td style='text-align: center'><a onclick='btnBorrarFilaImei();' ><i class='fa fa-trash fa-lg borrar' aria-hidden='true'></i></a></td>";
                    cadena_html_user += "</tr>";
                }
                
                cadena_html_user += "</tbody>";
                cadena_html_user += "</table>";
                
                keyValueError = new KeyValuePair<string, string>("1", "Carga Completa" + "//" + cadena_html_user);
            }


            return keyValueError;
        }

        public FileResult DescargarSustento()
        {
            //var rutaManual = configuration["CredencialUsuario"];
            var rutaManual = HttpContext.Session.GetString("SustenoRequerimiento");
            //String nombrePlantilla = HttpContext.Session.GetString("SustenoRequerimiento");
            String nombrePlantilla = "SUSTENTO REVISION PRELIMINAR.xlsx";
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            System.IO.File.Delete(rutaManual+nombrePlantilla);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        private bool IsNumeric(string number)
        {
            Regex pattern = new Regex("[^0-9]");
            return !pattern.IsMatch(number);
        }
        [HttpPost]
        public ActionResult CargarInformacionServicioMovil(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacionServMovilUnic(formulario);


                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", ex.Message);

                return Json(result);
            }
        }

        private KeyValuePair<string, string> CargarInformacionServMovilUnic(IFormCollection formulario)
        {
            String mensaje = "";
            int cont_correcta = 0;
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            Boolean existeRegistro = false;



            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
            BE_REQUERIMIENTO objParametroDos = new BE_REQUERIMIENTO();

            objParametroDos = new BE_REQUERIMIENTO();
            objParametroDos.nroItem = 1;
            objParametroDos.nroImeiDos = formulario["txtNumMovIndiv"].ToString();

            listaResultado.Add(objParametroDos);


            if (listaResultado == null)
            {
                existeRegistro = false;
            }
            else if (listaResultado.Count == 0)
            {
                existeRegistro = false;
            }
            else
            {
                existeRegistro = true;
            }
            String cadena_telefo_general = "";
            if (existeRegistro)
            {

                //listaResultado = listaResultado.OrderBy(o => o.nroImeiDos).ToList();

                if (listaResultado != null)
                {
                    if (listaResultado.Count > 0)
                    {
                        //listaResultado[0] = validarDocumentoExcelMovil(listaResultado[0], listaResultado);
                        cadena_telefo_general += listaResultado[0].nroImeiDos + "//";
                    }
                }
                HttpContext.Session.SetString("ListaTELEFREQEP", "");
                //CONSULTAR DE FORMA GENERAL VALIDACIONES DE IMEI
                BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
                listaResultado = ObjLogica.ConsultaEstadoTelefono(cadena_telefo_general, HttpContext.Session.GetString("IdUsuario"));
                //
                var listaSesionJSONEP = JsonConvert.SerializeObject(listaResultado);
                HttpContext.Session.SetString("ListaTELEFREQEP", listaSesionJSONEP);

                /*BeUsuarioEP ObjUserEP = new BeUsuarioEP();
                List<BeUsuarioEP> listaUEP_Grilla = new List<BeUsuarioEP>();*/
                string cadena_html_user = "";
                cadena_html_user += "<div class='tabla-content-result'>";
                cadena_html_user += "<table id='dtResultadCarMasMovilEP' class='tabla-osiptel' style='width: 100%'>";
                cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_html_user += " <thead>";
                cadena_html_user += "<tr>";
                cadena_html_user += "<th scope='col'>Nº</th>";
                cadena_html_user += "<th scope='col'>Dato</th>";
                cadena_html_user += "<th scope='col'>Revisión Preliminar</th>";
                cadena_html_user += "<th scope='col'>¿Procede a Cargar?</th>";
                cadena_html_user += "<th scope='col'>Accion</th>";
                cadena_html_user += "</tr>";
                cadena_html_user += " </thead>";
                cadena_html_user += "<tbody>";
                for (int i = 0; i < listaResultado.Count; i++)
                {
                    cadena_html_user += " <tr>";
                    cadena_html_user += "<td>" + listaResultado[i].nroItem.ToString() + "</td>";
                    cadena_html_user += "<td>" + listaResultado[i].nroImeiDos.ToString() + "</td>";

                    //if (int.TryParse(listaResultado[i].nroImeiDos.ToString(), out int valor) && listaResultado[i].nroImeiDos.ToString().Contains(" ") != true)
                    if (IsNumeric(listaResultado[i].nroImeiDos.ToString()) && listaResultado[i].nroImeiDos.ToString().Contains(" ") != true)
                    {
                        if (listaResultado[i].nroImeiDos.ToString().Length == 9)
                        {
                            if (listaResultado[i].validacionFormato.Trim() == "")
                            {
                                cadena_html_user += "<td>Correcto</td>";
                                cadena_html_user += "<td>Sí procede</td>";
                            }
                            else
                            {
                                cadena_html_user += "<td style='color: red;'>No se encuentra en el Registro de Abonados</td>";
                                cadena_html_user += "<td style='color: red;'>No Procede</td>";
                            }
                        }
                        else
                        {
                            cadena_html_user += "<td style='color: red;'>No tiene 9 dígitos.</td>";
                            cadena_html_user += "<td style='color: red;'>No Procede</td>";
                        }
                    }
                    else
                    {
                        cadena_html_user += "<td style='color: red;'>Tiene caracteres no numéricos o espacios.</td>";
                        cadena_html_user += "<td style='color: red;'>No Procede</td>";
                    }
                    cadena_html_user += "<td style='text-align: center'><a onclick='btnBorrarFila();'><i class='fa fa-trash fa-lg borrar' aria-hidden='true'></i></a></td>";
                    cadena_html_user += "</tr>";

                }
                cadena_html_user += "</tbody>";
                cadena_html_user += "</table>";
                cadena_html_user += "</div>";

                keyValueError = new KeyValuePair<string, string>("1", "Carga Completa" + "//" + cadena_html_user);
            }


            return keyValueError;
        }

        [HttpPost]
        public void DescargarSustentoImei(IFormCollection formulario)
        {
            var datos = JsonConvert.DeserializeObject<List<BE_SUSTENTO>>(formulario["datos"]);

            //GENERANDO SUSTENTO
            if (datos.Count > 0)
            {
                FileStream fs = new FileStream(configuration["CarpetaTemporal"] + "\\SUSTENTO REVISION PRELIMINAR.xlsx", FileMode.Create);
                ExcelPackage Excel = new ExcelPackage(fs);

                /* Creación del estilo. */
                Excel.Workbook.Styles.CreateNamedStyle("Moneda");
                ExcelNamedStyleXml moneda = Excel.Workbook.Styles.NamedStyles[1];// 0 = Normal, 1 (El que acabamos de agregar).

                /* Creación de hoja de trabajo. */
                Excel.Workbook.Worksheets.Add("Sustento IMEI Fisico");
                ExcelWorksheet hoja = Excel.Workbook.Worksheets["Sustento IMEI Fisico"];
                Color colorDeCelda = ColorTranslator.FromHtml("#00AAE4");



                /* Num Caracteres + 1.29 de Margen.
                Los índices de columna empiezan desde 1. */
                hoja.Column(1).Width = 11.29f;

                ExcelRange rangoAAAA = hoja.Cells["A1"];

                rangoAAAA.Value = "Nº";
                rangoAAAA.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rangoAAAA.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rangoAAAA.Style.Font.Bold = true;
                rangoAAAA.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rangoAAAA.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rangoAAAA.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rangoAAAA.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rangoAAAA.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rangoAAAA.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                rangoAAAA.AutoFitColumns();

                ExcelRange rango = hoja.Cells["B1"];

                rango.Value = "Dato";
                rango.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rango.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rango.Style.Font.Bold = true;
                rango.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rango.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rango.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rango.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rango.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rango.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                rango.AutoFitColumns();

                ExcelRange rangoB = hoja.Cells["C1"];

                rangoB.Value = "Revisión Preliminar";
                rangoB.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rangoB.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rangoB.Style.Font.Bold = true;
                rangoB.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rangoB.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rangoB.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rangoB.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rangoB.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rangoB.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                hoja.Column(3).Width = 30;
                ExcelRange rangoC = hoja.Cells["D1"];

                rangoC.Value = "¿Procede a Cargar?";
                rangoC.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rangoC.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rangoC.Style.Font.Bold = true;
                rangoC.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rangoC.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rangoC.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rangoC.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rangoC.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rangoC.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                hoja.Column(4).Width = 30;
                ExcelRange rangoD = hoja.Cells["E1"];

                rangoD.Value = "Sustento";
                rangoD.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rangoD.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rangoD.Style.Font.Bold = true;
                rangoD.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rangoD.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rangoD.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rangoD.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rangoD.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rangoD.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                hoja.Column(5).Width = 110;
                //rangoD.AutoFitColumns();

                int contador_celdas = 2;
                for (var i = 0; i < datos.Count; i++)
                {

                    ExcelRange rangoAAA = hoja.Cells["A" + contador_celdas];

                    rangoAAA.Value = datos[i].nroitem.ToString();
                    rangoAAA.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoAAA.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoAAA.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoAAA.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoAAA.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoAAA.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rangoAAA.AutoFitColumns();

                    ExcelRange rangoAA = hoja.Cells["B" + contador_celdas];

                    rangoAA.Value = datos[i].dato.ToString();
                    rangoAA.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoAA.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoAA.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoAA.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoAA.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoAA.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rangoAA.AutoFitColumns();

                    ExcelRange rangoBB = hoja.Cells["C" + contador_celdas];

                    rangoBB.Value = datos[i].revi.ToString();
                    rangoBB.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoBB.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoBB.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoBB.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoBB.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoBB.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //rangoBB.AutoFitColumns();

                    ExcelRange rangoCC = hoja.Cells["D" + contador_celdas];

                    rangoCC.Value = datos[i].proce.ToString();
                    rangoCC.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoCC.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoCC.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoCC.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoCC.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoCC.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //rangoCC.AutoFitColumns();

                    ExcelRange rangoDD = hoja.Cells["E" + contador_celdas];

                    if (IsNumeric(datos[i].dato.ToString()) && datos[i].dato.ToString().Contains(" ") != true)
                    {
                        if (datos[i].dato.ToString().Length == 15)
                        {
                            if (datos[i].revi.ToString() == "Correcto" && datos[i].proce.ToString() == "Sí Procede")
                            {
                                rangoDD.Value = "-";
                            }
                            else if (datos[i].revi.ToString() == "IMEI no cumple algoritmo de verificación.")
                            {
                                rangoDD.Value = "El IMEI consultado no se ajusta a los estándares de la GSMA (Global System for Mobile Communications Association), esto debido a que los primeros catorce (14) dígitos del referido número de IMEI no guardan relación con el decimoquinto dígito del mismo (dígito que verifica la consistencia de los primeros catorce).";
                            }
                            else if (datos[i].revi.ToString() == "IMEI no reportado como SUSTRAÍDO o PERDIDO.")
                            {
                                rangoDD.Value = "El IMEI consultado no se encuentra reportado como sustraído o perdido ante ninguna empresa operadora. Así, dada la facilidad que existe en el mercado para intercambiar el uso del equipo en diferentes servicios móviles, no resultaría posible identificar apropiadamente a los titulares de los equipos terminales móviles no reportados como robados o perdidos, en tanto que dicha información se encuentra relacionada con el servicio de telefonía móvil.";
                            }
                            else if (datos[i].revi.ToString() == "IMEI reportado como RECUPERADO.")
                            {
                                rangoDD.Value = "El IMEI consultado figura como recuperado, por lo que hacemos de su conocimiento que un equipo se reporta como “recuperado” cuando el abonado ubica o toma posesión nuevamente de un equipo que había sido reportado como “sustraído” o “perdido”. De esta manera, este hecho es consignado ante la empresa operadora que le brinda el servicio con la finalidad de que se elimine el bloqueo por robo o pérdida, habilitando su uso en las redes de telecomunicaciones.";
                            }
                            else if (datos[i].indi.ToString() == "I")
                            {
                                rangoDD.Value = "El IMEI consultado sería un IMEI inválido, debido a que su numeración no permite identificar al equipo terminal móvil en base a los estándares de la GSMA (Global System for Mobile Communications Association), por lo que se puede presumir que se trataría de un IMEI lógico, el cual sería factible de ser alterado.";
                            }
                        }
                        else
                        {
                            rangoDD.Value = "El número de dígitos del código IMEI consultado es diferente a quince (15), con lo cual no resulta posible verificar si el equipo terminal en cuestión fue reportado como robado o perdido ante alguna de las empresas operadoras que operan a nivel nacional.";
                        }
                    }
                    else
                    {
                        rangoDD.Value = "El código consultado contiene letras, espacios u otros caracteres no numéricos. Cabe precisar que el IMEI es un código que identifica al equipo móvil de forma exclusiva a nivel mundial y cuenta con quince (15) dígitos.";
                    }

                    rangoDD.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoDD.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoDD.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoDD.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoDD.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoDD.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rangoDD.Style.WrapText = true;
                    //rangoDD.AutoFitColumns();

                    contador_celdas++;
                    //DescargarUsuarioCredenciales();
                }
                Excel.Save();
                // No estoy seguro de ésta parte, pero mejor cerramos el stream de archivo.
                fs.Close();
                fs.Dispose();

                Excel.Dispose();
                //var rutaPlantilla = configuration["CarpetaTemporal"];
                var rutaPlantilla = "";

                rutaPlantilla = Path.Combine(rutaPlantilla, configuration["CarpetaTemporal"] + "\\SUSTENTO REVISION PRELIMINAR.xlsx");

                HttpContext.Session.SetString("SustenoRequerimiento", configuration["CarpetaTemporal"]);
            }
        }


        [HttpPost]
        public void DescargarSustentoMovil(IFormCollection formulario)
        {
            var datos = JsonConvert.DeserializeObject<List<BE_SUSTENTO>>(formulario["datos"]);

            //GENERANDO SUSTENTO
            if (datos.Count > 0)
            {
                FileStream fs = new FileStream(configuration["CarpetaTemporal"] + "\\SUSTENTO REVISION PRELIMINAR.xlsx", FileMode.Create);
                ExcelPackage Excel = new ExcelPackage(fs);

                /* Creación del estilo. */
                Excel.Workbook.Styles.CreateNamedStyle("Moneda");
                ExcelNamedStyleXml moneda = Excel.Workbook.Styles.NamedStyles[1];// 0 = Normal, 1 (El que acabamos de agregar).

                /* Creación de hoja de trabajo. */
                Excel.Workbook.Worksheets.Add("Sustento Servicio Movil");
                ExcelWorksheet hoja = Excel.Workbook.Worksheets["Sustento Servicio Movil"];
                Color colorDeCelda = ColorTranslator.FromHtml("#00AAE4");



                /* Num Caracteres + 1.29 de Margen.
                Los índices de columna empiezan desde 1. */
                hoja.Column(1).Width = 11.29f;

                ExcelRange rangoAAA = hoja.Cells["A1"];

                rangoAAA.Value = "Nº";
                rangoAAA.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rangoAAA.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rangoAAA.Style.Font.Bold = true;
                rangoAAA.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rangoAAA.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rangoAAA.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rangoAAA.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rangoAAA.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rangoAAA.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                ExcelRange rango = hoja.Cells["B1"];

                rango.Value = "Dato";
                rango.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rango.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rango.Style.Font.Bold = true;
                rango.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rango.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rango.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rango.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rango.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rango.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                rango.AutoFitColumns();

                ExcelRange rangoB = hoja.Cells["C1"];

                rangoB.Value = "Revisión Preliminar";
                rangoB.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rangoB.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rangoB.Style.Font.Bold = true;
                rangoB.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rangoB.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rangoB.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rangoB.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rangoB.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rangoB.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                hoja.Column(3).Width = 40;
                ExcelRange rangoC = hoja.Cells["D1"];

                rangoC.Value = "¿Procede a Cargar?";
                rangoC.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rangoC.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rangoC.Style.Font.Bold = true;
                rangoC.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rangoC.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rangoC.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rangoC.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rangoC.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rangoC.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                hoja.Column(4).Width = 30;
                ExcelRange rangoD = hoja.Cells["E1"];

                rangoD.Value = "Sustento";
                rangoD.Style.Fill.PatternType = ExcelFillStyle.Solid;
                rangoD.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
                rangoD.Style.Font.Bold = true;
                rangoD.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                rangoD.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                rangoD.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                rangoD.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                rangoD.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                rangoD.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                hoja.Column(5).Width = 130;
                //rangoD.AutoFitColumns();

                int contador_celdas = 2;
                for (var i = 0; i < datos.Count; i++)
                {

                    ExcelRange rangoAAAA = hoja.Cells["A" + contador_celdas];

                    rangoAAAA.Value = datos[i].nroitem.ToString();
                    rangoAAAA.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoAAAA.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoAAAA.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoAAAA.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoAAAA.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoAAAA.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rangoAAAA.AutoFitColumns();

                    ExcelRange rangoAA = hoja.Cells["B" + contador_celdas];

                    rangoAA.Value = datos[i].dato.ToString();
                    rangoAA.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoAA.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoAA.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoAA.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoAA.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoAA.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rangoAA.AutoFitColumns();

                    ExcelRange rangoBB = hoja.Cells["C" + contador_celdas];

                    rangoBB.Value = datos[i].revi.ToString();
                    rangoBB.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoBB.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoBB.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoBB.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoBB.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoBB.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    //rangoBB.AutoFitColumns();

                    ExcelRange rangoCC = hoja.Cells["D" + contador_celdas];

                    rangoCC.Value = datos[i].proce.ToString();
                    rangoCC.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoCC.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoCC.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoCC.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoCC.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoCC.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rangoCC.Style.WrapText = true;
                    //rangoCC.AutoFitColumns();

                    ExcelRange rangoDD = hoja.Cells["E" + contador_celdas];

                    if (IsNumeric(datos[i].dato.ToString()) && datos[i].dato.ToString().Contains(" ") != true)
                    {
                        if (datos[i].dato.ToString().Length == 9)
                        {
                            if (datos[i].revi.Trim() == "Correcto")
                            {
                                rangoDD.Value = "-";
                            }
                            else
                            {
                                rangoDD.Value = "El número de servicio móvil consultado no se encuentra en el Registro de Abonados del RENTESEG. Ninguna empresa operadora del servicio público móvil ha reportado información al RENTESEG respecto del servicio consultado.";
                            }
                        }
                        else
                        {
                            rangoDD.Value = "El número de dígitos del servicio consultado es diferente a nueve (9). Cabe precisar que un número de servicio público móvil cuenta con nueve (9) dígitos e inicia con 9.";
                        }
                    }
                    else
                    {
                        rangoDD.Value = "El código consultado contiene letras, espacios u otros caracteres no numéricos.Cabe precisar que un número del servicio público móvil cuenta con nueve(9) dígitos.";
                    }

                    rangoDD.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rangoDD.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rangoDD.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    rangoDD.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    rangoDD.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    rangoDD.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    rangoDD.Style.WrapText = true;
                    //rangoDD.AutoFitColumns();

                    contador_celdas++;
                    //DescargarUsuarioCredenciales();
                }
                Excel.Save();
                // No estoy seguro de ésta parte, pero mejor cerramos el stream de archivo.
                fs.Close();
                fs.Dispose();

                Excel.Dispose();
                //var rutaPlantilla = configuration["CredencialUsuario"];
                var rutaPlantilla = "";

                rutaPlantilla = Path.Combine(rutaPlantilla, configuration["CarpetaTemporal"] + "\\SUSTENTO REVISION PRELIMINAR.xlsx");

                HttpContext.Session.SetString("SustenoRequerimiento", configuration["CarpetaTemporal"] );
            }
        }

        [HttpPost]
        public ActionResult GuardarRequeimientoEP(IFormCollection formulario)
        {
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();
            try
            {
                var objMovil = JsonConvert.DeserializeObject<List<BE_SUSTENTO>>(formulario["dataMovil"].ToString());
                var objImei = JsonConvert.DeserializeObject<List<BE_SUSTENTO>>(formulario["dataImei"].ToString());

                var dataObtenidaMovil = new List<BE_SUSTENTO>();
                var dataObtenidaImei = new List<BE_SUSTENTO>();

                List<BE_REQUERIMIENTO> datosGrillaMovil = new List<BE_REQUERIMIENTO>();
                List<BE_REQUERIMIENTO> datosGrillaImei = new List<BE_REQUERIMIENTO>();

                if (objMovil.Count > 0 && (formulario["lstDato"].ToString() == "1" || formulario["lstDato"].ToString() == "3"))
                {
                    List<BE_REQUERIMIENTO> listaResultadoMovi = new List<BE_REQUERIMIENTO>();
                    listaResultadoMovi = JsonConvert.DeserializeObject<List<BE_REQUERIMIENTO>>(HttpContext.Session.GetString("ListaTELEFREQEP"));

                    /*foreach (var itemMovil in objMovil)
                    {
                        foreach (var itemTotal in listaResultadoMovi)
                        {
                            if (itemMovil.dato == itemTotal.nroImeiDos)
                            {
                                datosGrillaMovil.Add(itemTotal);
                                dataObtenidaMovil.Add(itemMovil);
                            }
                        }
                    }*/
                    foreach (var itemMovil in objMovil)
                    {
                        dataObtenidaMovil.Add(itemMovil);
                    }
                    foreach (var itemTotal in listaResultadoMovi)
                    {
                        datosGrillaMovil.Add(itemTotal);
                    }
                }

                if (objImei.Count > 0 && (formulario["lstDato"].ToString() == "2" || formulario["lstDato"].ToString() == "3"))
                {
                    List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
                    listaResultado = JsonConvert.DeserializeObject<List<BE_REQUERIMIENTO>>(HttpContext.Session.GetString("ListaIMEIREQEP"));

                    /*foreach (var item in objImei)
                    {
                        foreach (var itemTotal in listaResultado)
                        {
                            if (item.dato == itemTotal.nroImei)
                            {
                                datosGrillaImei.Add(itemTotal);
                                dataObtenidaImei.Add(item);
                            }
                        }
                    }*/
                    foreach (var item in objImei)
                    {
                        dataObtenidaImei.Add(item);
                    }
                    foreach (var itemTotal in listaResultado)
                    {
                        datosGrillaImei.Add(itemTotal);
                    }
                }

                if (datosGrillaMovil.Count > 0 || datosGrillaImei.Count > 0)
                {    
                    Int64 idAdjunto = 0;
                    Int64 idAdjuntoTelf = 0;

                    List<BE_REQUERIMIENTO> listaRequerimiento = new List<BE_REQUERIMIENTO>();
                    List<BE_REQUERIMIENTO> listaRequerimientoTelef = new List<BE_REQUERIMIENTO>();

                    //Registro documentacion por carga masiva
                    if (formulario["lstCarga"].ToString() == "2")
                    {
                        String nombreArchivoImei = HttpContext.Session.GetString("nombreArchivoCarMasImei");
                        String nombreArchivoTelef = HttpContext.Session.GetString("nombreArchivoCarMasMovil");

                        BL_DOCUMENTO_ADJUNTO objLogicaArchivoAdjunto = new BL_DOCUMENTO_ADJUNTO();
                        BE_DOCUMENTO_ADJUNTO objAdjunto = new BE_DOCUMENTO_ADJUNTO();
                        BE_DOCUMENTO_ADJUNTO objAdjuntoTelf = new BE_DOCUMENTO_ADJUNTO();

                        if (formulario["lstDato"].ToString() == "2")
                        {
                            objAdjunto.nombreDocumento = nombreArchivoImei;
                            objAdjunto.descDocumento = "Registro de Sisdoc, Archivo adjuntado al registrar de forma masiva Imei - Requerimiento EP";
                            objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");

                            idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);
                        }
                        else if (formulario["lstDato"].ToString() == "1")
                        {
                            objAdjunto.nombreDocumento = nombreArchivoTelef;
                            objAdjunto.descDocumento = "Registro de Sisdoc, Archivo adjuntado al registrar de forma masiva Telefono - Requerimiento EP";
                            objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");

                            idAdjuntoTelf = objLogicaArchivoAdjunto.Insertar(objAdjunto);
                        }
                        else
                        {
                            objAdjunto.nombreDocumento = nombreArchivoImei;
                            objAdjunto.descDocumento = "Registro de Sisdoc, Archivo adjuntado al registrar de forma masiva Imei - Requerimiento EP";
                            objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");

                            idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);

                            objAdjuntoTelf.nombreDocumento = nombreArchivoTelef;
                            objAdjuntoTelf.descDocumento = "Registro de Sisdoc, Archivo adjuntado al registrar de forma masiva Telefono - Requerimiento EP";
                            objAdjuntoTelf.usuCre = HttpContext.Session.GetString("IdUsuario");

                            idAdjuntoTelf = objLogicaArchivoAdjunto.Insertar(objAdjunto);
                        }

                    }

                    //REGISTRANDO IMEI

                    for (var i = 0; i<datosGrillaImei.Count; i++)
                    {
                        var item = datosGrillaImei[i];
                        item.listaIMEI = new List<BE_REQUERIMIENTO>();
                        item.listaIMEI.Add(item);
                        item.idAdjuntoIMEI = idAdjunto;
                        listaRequerimiento.Add(item);
                    }

                    //REGISTRANDO TELEFONO

                    for (var i = 0; i < datosGrillaMovil.Count; i++)
                    {
                        var item = datosGrillaMovil[i];
                        item.listaIMEI = new List<BE_REQUERIMIENTO>();
                        item.listaIMEI.Add(item);
                        item.idAdjuntoIMEI = idAdjuntoTelf;
                        listaRequerimientoTelef.Add(item);
                    }


                    BL_REQUERIMIENTO objLogicaRequerimiento = new BL_REQUERIMIENTO();
                    BL_REQUERIMIENTO_IMEI objLogicaIMEI = new BL_REQUERIMIENTO_IMEI();

                    long idRequerimiento = 0;
                    long idRequerimientoIMEi = 0;
                    //INSERTANDO CABECERA
                    BE_REQUERIMIENTO objReq = new BE_REQUERIMIENTO();
                    objReq.usuarioRegistro = HttpContext.Session.GetString("IdUsuario");
                    objReq.ent_publica = Convert.ToInt16(formulario["ent_publica"].ToString());
                    objReq.uni_ent_publica = Convert.ToInt16(formulario["uni_ent_publica"].ToString());
                    objReq.tipoSolicitante = formulario["cargo"].ToString();
                    objReq.descTipoSolicitante = formulario["descTipoSolicitante"].ToString();
                    objReq.fecRegistro = formulario["fecRegistro"].ToString();
                    objReq.correo = formulario["correo"].ToString();
                    objReq.delito = formulario["delito"].ToString();
                    objReq.motivo = formulario["motivo"].ToString();
                    objReq.num_denuncia = formulario["num_denuncia"].ToString();
                    objReq.tipo_dato = formulario["lstDato"].ToString();
                    objReq.tipo_carga = formulario["lstCarga"].ToString();
                    objReq.nombreSolicitante = formulario["nombreSolicitante"].ToString();
                    objReq.nombreInstitucion = formulario["nombreInstitucion"].ToString();
                    objReq.idUbigeo = formulario["ubigeo"].ToString();
                    objReq.idAdjuntoIMEI =Convert.ToInt64( idAdjunto);

                    
                    idRequerimiento = 0;
                    idRequerimiento = objLogicaRequerimiento.InsertarReqEP(objReq);

                    foreach (var item in listaRequerimiento)
                    {
                        item.usuCre = HttpContext.Session.GetString("IdUsuario");
                        foreach (var itemImei in item.listaIMEI)
                        {
                            itemImei.idRequerimiento = idRequerimiento;
                            itemImei.usuCre = HttpContext.Session.GetString("IdUsuario"); ;
                            idRequerimientoIMEi = objLogicaIMEI.InsertarImeiInterno(itemImei);
                        }
                    }

                    foreach (var item in listaRequerimientoTelef)
                    {
                        item.usuCre = HttpContext.Session.GetString("IdUsuario");
                        foreach (var itemImei in item.listaIMEI)
                        {
                            itemImei.idRequerimiento = idRequerimiento;
                            itemImei.usuCre = HttpContext.Session.GetString("IdUsuario"); ;
                            idRequerimientoIMEi = objLogicaIMEI.InsertarTelefonoInterno(itemImei);
                        }
                    }
                    HttpContext.Session.SetString("ListaIMEIREQEP", "");
                    HttpContext.Session.SetString("ListaTELEFREQEP", "");
                    string cod_requerimiento = "";
                    cod_requerimiento = objLogicaRequerimiento.ConsultarCodGeneradoReqEP(idRequerimiento);

                    //Generar PDF

                    var doc = new iTextSharp.text.Document();
                    
                    var output = new FileStream(configuration["DocumentosEntidadPublica"] + "FichaRequerimientoEP.pdf", FileMode.Create);
                    
                    var write = iTextSharp.text.pdf.PdfWriter.GetInstance(doc, output);
                    
                    doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
                    doc.Open();

                    // Creamos la imagen y le ajustamos el tamaño
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(configuration["DocumentosEntidadPublica"] + "LogoPortal.png");
                    imagen.BorderWidth = 0;
                    imagen.Alignment = Element.ALIGN_RIGHT;
                    float percentage = 0.0f;
                    percentage = 150 / imagen.Width;
                    imagen.ScalePercent(percentage * 100);

                    // Insertamos la imagen en el documento
                    doc.Add(imagen);

                    Paragraph lineSeparatorB = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
                    Paragraph lineSeparatorW = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, iTextSharp.text.BaseColor.WHITE, Element.ALIGN_LEFT, 1)));

                    PdfPTable table = new PdfPTable(9);
                    table.WidthPercentage = 100.0F;

                    table.AddCell(cellFormat("SOLICITUD DE REQUERIMIENTO DE INFORMACIÓN", 9, 1));
                    table.AddCell(cellFormat("Registro: "+ cod_requerimiento.ToString(), 9, 1));
                    table.AddCell(cellFormat(" ", 9, 1));

                    table.AddCell(cellFormat("Apellidos:", 2, 0));
                    table.AddCell(cellFormat(formulario["apellidos"].ToString(), 2, 0));
                    table.AddCell(cellFormat(" ", 1, 0));
                    /*table.AddCell(cellFormat("Región Solicitante:", 2, 0));
                    table.AddCell(cellFormat(formulario["ubigeoText"].ToString(), 2, 0));*/
                    table.AddCell(cellFormat("Institución Solicitante:", 2, 0));
                    table.AddCell(cellFormat(formulario["nombreInstitucion"].ToString(), 2, 0));

                    table.AddCell(cellFormat("Nombres:", 2, 0));
                    table.AddCell(cellFormat(formulario["nombres"].ToString(), 2, 0));
                    table.AddCell(cellFormat(" ", 1, 0));
                    table.AddCell(cellFormat("Oficina Solicitante:", 2, 0));
                    table.AddCell(cellFormat(formulario["nombreSolicitante"].ToString(), 2, 0));

                    table.AddCell(cellFormat("Tipo de Documento:", 2, 0));
                    table.AddCell(cellFormat(formulario["tipoDoc"].ToString(), 2, 0));
                    table.AddCell(cellFormat(" ", 1, 0));
                    /*table.AddCell(cellFormat("Fecha de Registro:", 2, 0));
                    table.AddCell(cellFormat(formulario["fecRegistro"].ToString(), 2, 0));*/
                    table.AddCell(cellFormat("Cargo:", 2, 0));
                    table.AddCell(cellFormat(formulario["cargo"].ToString(), 2, 0));

                    table.AddCell(cellFormat("Numero de Documento:", 2, 0));
                    table.AddCell(cellFormat(formulario["descTipoSolicitante"].ToString(), 2, 0));
                    table.AddCell(cellFormat(" ", 1, 0));
                    /*table.AddCell(cellFormat("Codigo de Registro:", 2, 0));
                    table.AddCell(cellFormat(cod_requerimiento.ToString(), 2, 0));*/
                    table.AddCell(cellFormat("Fecha de Registro:", 2, 0));
                    table.AddCell(cellFormat(formulario["fecRegistro"].ToString(), 2, 0));

                    table.AddCell(cellFormat("Correo Electronico:", 2, 0));
                    table.AddCell(cellFormat(formulario["correo"].ToString(), 6, 0));

                    doc.Add(table);

                    doc.Add(lineSeparatorB);
                    doc.Add(lineSeparatorW);

                    table = new PdfPTable(4);
                    table.WidthPercentage = 100.0F;

                    table.AddCell(cellFormat("Delito Investigado: ", 2, 0));
                    table.AddCell(cellFormat(formulario["delito"].ToString(), 2, 0));
                    table.AddCell(cellFormat("Motivo del Requerimiento:", 2, 0));
                    table.AddCell(cellFormat(formulario["motivo"].ToString(), 2, 0));
                    table.AddCell(cellFormat("N° Denuncia/ Caso/ Carpeta/ Expediente:", 2, 0));
                    table.AddCell(cellFormat(formulario["num_denuncia"].ToString(), 2, 0));

                    doc.Add(table);

                    doc.Add(lineSeparatorB);
                    doc.Add(lineSeparatorW);

                     table = new PdfPTable(12);
                     table.WidthPercentage = 100.0F;
                     table.AddCell(cellFormat("Se ha registrado el siguiente requerimiento de información:  ", 12, 0));
                     table.AddCell(cellFormat(" ", 12, 0));
                    table.AddCell(cellFormat(" ", 12, 0));
                    doc.Add(table);


                    table = new PdfPTable(12);
                   
                    table.WidthPercentage = 100.0F;
                    /*table.AddCell(cellFormat("Se ha registrado el siguiente requerimiento de información:  ", 12, 0));
                    table.AddCell(cellFormat(" ", 12, 0));
                    table.AddCell(cellFormat(" ", 12, 0));*/
                    table.SetWidths(new float[] { 2f, 4f, 3f, 5f, 6f, 4f, 6f, 6f, 4f, 6f, 4f, 4f });
                    table.AddCell(cellCabecera("Nº", 1));
                    table.AddCell(cellCabecera("Tipo de Dato", 2));
                    table.AddCell(cellCabecera("Dato", 2));
                    table.AddCell(cellCabecera("Revisión Preliminar", 2));
                    //table.AddCell(cellCabecera("¿Procede Cargar?", 2));
                    table.AddCell(cellCabecera("Sustento Preliminar", 5));
                    //table.GetHeader();
                    table.HeaderRows = 1;
                    //table.SkipFirstHeader = true;
                    foreach (var item in dataObtenidaImei)
                    {
                        table.AddCell(cellDatos(item.nroitem.ToString(), 1));
                        table.AddCell(cellDatos("IMEI FISICO", 2));
                        table.AddCell(cellDatos(item.dato.ToString(), 2));
                        table.AddCell(cellDatos(item.revi.ToString(), 2));
                        //table.AddCell(cellDatos(item.proce.ToString(), 2));
                        table.AddCell(cellDatos(validacionSustentoImei(item), 5));
                    }
                    doc.Add(table);

                    doc.Add(lineSeparatorW);
                    doc.Add(lineSeparatorW);
                    //doc.Add(lineSeparatorW);

                    table = new PdfPTable(12);

                    table.WidthPercentage = 100.0F;
                    /*table.AddCell(cellFormat("Se ha registrado el siguiente requerimiento de información:  ", 12, 0));
                    table.AddCell(cellFormat(" ", 12, 0));
                    table.AddCell(cellFormat(" ", 12, 0));*/
                    table.SetWidths(new float[] { 2f, 4f, 3f, 5f, 6f, 4f, 6f, 6f, 4f, 6f, 4f, 4f });
                    table.AddCell(cellCabecera("Nº", 1));
                    table.AddCell(cellCabecera("Tipo de Dato", 2));
                    table.AddCell(cellCabecera("Dato", 2));
                    table.AddCell(cellCabecera("Revisión Preliminar", 2));
                    //table.AddCell(cellCabecera("¿Procede Cargar?", 2));
                    table.AddCell(cellCabecera("Sustento Preliminar", 5));
                    //table.GetHeader();
                    table.HeaderRows = 1;
                    foreach (var item in dataObtenidaMovil)
                    {
                        table.AddCell(cellDatos(item.nroitem.ToString(), 1));
                        table.AddCell(cellDatos("SERVICIO MOVIL", 2));
                        table.AddCell(cellDatos(item.dato, 2));
                        table.AddCell(cellDatos(item.revi, 2));
                        //table.AddCell(cellDatos(item.proce, 2));
                        table.AddCell(cellDatos(validacionSustentoMovil(item), 5));
                    }

                    doc.Add(table);

                    write.CloseStream = false;
                    doc.Close();
                    output.Dispose();

                    //VALIDANDO EXCEPCIONES - IMEI
                    if ((formulario["lstDato"].ToString() == "2"))
                    {
                        if (listaRequerimiento.Count == 1)
                        {
                            foreach (var item in listaRequerimiento)
                            {
                                if (item.MOTIVOREPORTE.Trim() == "Sustraído" || item.MOTIVOREPORTE.Trim() == "Perdido")
                                {
                                    if (item.FUENTEREPORTE.Trim() == "Abonado")
                                    {
                                        //valiando si existe en registro de abonados
                                        List<BE_REQUERIMIENTO_IMEI> listaResultadoExep = new List<BE_REQUERIMIENTO_IMEI>();
                                        BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
                                        listaResultadoExep = ObjLogica.ConsultaEstadoAbonExcepc(item.nroImei, item.TIPO_DOC_LEGAL, item.NUMERO_DOC_LEGAL,item.NROSERVICIOMOVIL);
                                        //ACTUALIZAR ESTADO DE CABECERA Y REGISTRO EN TABLA IMEI SI HAY RESULTADOS
                                        if (listaResultadoExep.Count > 0)
                                        {
                                            List<BE_REQUERIMIENTO> listaResultadoUpdCabecera = new List<BE_REQUERIMIENTO>();
                                            //BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
                                            listaResultadoUpdCabecera = ObjLogica.ActualizarCabeExcepcion(idRequerimiento, cod_requerimiento, idRequerimientoIMEi,(formulario["nombres"].ToString() + " "+ formulario["apellidos"].ToString()));
                                            if (listaResultadoUpdCabecera.Count > 0)
                                            {
                                                //GENERAR PDF
                                                string cadena_imei = "";
                                                string cadena_final = "";
                                                if (listaResultadoExep.Count > 0)
                                                {
                                                    cadena_imei += "<br><br><br><h4 style='text-align: center; text-align: center;page-break-before:always;'><strong>ANEXO</strong></h4><br>";
                                                    cadena_imei += "<div class='tabla-content-result'>";
                                                    cadena_imei += "<table id='dtResultadCarMasImeiEP' border='1' class='tabla-osiptel' style='width: 100%;font-size: 7px;'>";
                                                    cadena_imei += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                                                    cadena_imei += " <thead>";
                                                    cadena_imei += "<tr>";
                                                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>IMEI</strong></th>";
                                                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>ESTADO</strong></th>";
                                                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>FECHA DE BLOQUEO</strong></th>";
                                                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>EMPRESA OPERADORA</strong></th>";
                                                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>NOMBRES Y APELLIDOS DEL PROPIETARIO</strong></th>";
                                                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>TIPO DE DOCUMENTO</strong></th>";
                                                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>NÚMERO DE DOCUMENTO</strong></th>";
                                                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Nº SERVICIO MOVIL</strong></th>";
                                                    cadena_imei += "</tr>";
                                                    cadena_imei += " </thead>";
                                                    cadena_imei += "<tbody>";
                                                    for (int i = 0; i < listaResultadoExep.Count; i++)
                                                    {
                                                        cadena_imei += " <tr>";
                                                        cadena_imei += "<td> " + listaResultadoExep[i].IMEI.ToString() + "</td>";
                                                        
                                                        cadena_imei += "<td> " + listaResultadoExep[i].MOTIVO_REPORTE.ToString() + "</td>";

                                                        cadena_imei += "<td> " + listaResultadoExep[i].FECHA_BLOQUEO.ToString() + "</td>";
                                                        cadena_imei += "<td> " + listaResultadoExep[i].CONCESIONARIO.ToString() + "</td>";
                                                        cadena_imei += "<td> " + listaResultadoExep[i].NOM_ABONADO.ToString() + "</td>";
                                                        cadena_imei += "<td> " + listaResultadoExep[i].TIPO_DOC_LEGAL.ToString() + "</td>";
                                                        cadena_imei += "<td> " + listaResultadoExep[i].NUMERO_DOC_LEGAL.ToString() + "</td>";
                                                        cadena_imei += "<td> " + listaResultadoExep[i].NUMERO_SERVICIO_MOVIL.ToString() + "</td>";
                                                        cadena_imei += "</tr>";



                                                    }

                                                    cadena_imei += " </tbody>";
                                                    cadena_imei += "   </table>";
                                                    cadena_imei += "  <br><p style='font-size: 7px;text-align: center;'>Fuente: Información reportada por las empresas operadoras al RENTESEG.</p>";
                                                    cadena_imei += "</div>";
                                                }
                                                //
                                                //CONCATENADO CADENAS
                                                CultureInfo ci = new CultureInfo("es-ES");

                                                DateTime fecha = DateTime.Now;

                                                string FechaFormateada = fecha.ToString("dd") +" de "+ fecha.ToString("MMMM", ci) + " del " + fecha.ToString("yyyy");
                                                //int cant = Convert.ToInt16(lista[0].nroImei) + Convert.ToInt16(lista[0].nroImeiDos);
                                                cadena_final += "<br><h4 style='font-size: 11px; '><u><strong>RESPUESTA Nº " + cod_requerimiento + "</strong></u></h4><div class='row'>";
                                                cadena_final += " <div class='col-md-3' style='text-align: right; text-align: right;'>";
                                                cadena_final += "<p style='font-size: 11px; '>";
                                                cadena_final += " Lima, " + FechaFormateada;
                                                cadena_final += "</p>";
                                                cadena_final += "  </div>";
                                           
                                                cadena_final += "</div>";
                                                cadena_final += "<div class='row'>";
                                                cadena_final += " <div class='col-md-12'>";
                                                cadena_final += "<p style='font-size: 11px; '>";
                                                cadena_final += " <strong>Señor(a) </strong><br>" + (formulario["nombres"].ToString() + " " + formulario["apellidos"].ToString());
                                                cadena_final += "</p>";
                                                cadena_final += "  </div>";

                                                cadena_final += "</div>";
                                                cadena_final += "<div class='row'>";
                                                cadena_final += " <div class='col-md-12'>";
                                                cadena_final += "<p style='font-size: 11px; '>";
                                                cadena_final +=  formulario["cargo"].ToString() ;
                                                cadena_final += "</p>";
                                                cadena_final += "  </div>";

                                                cadena_final += "</div>";
                                                cadena_final += "<div class='row'>";
                                                cadena_final += " <div class='col-md-12'>";
                                                cadena_final += "<p style='font-size: 11px; '>";
                                                cadena_final += "<strong>"+ formulario["nombreSolicitante"].ToString()  + " </strong> ";
                                                cadena_final += "</p>";
                                                cadena_final += "  </div>";
                                        
                                                cadena_final += "</div>";
                                                cadena_final += "<div class='row'>";
                                                cadena_final += " <div class='col-md-12'>";
                                                cadena_final += "<p style='font-size: 11px; '>";
                                                cadena_final +=  formulario["correo"].ToString() ;
                                                cadena_final += "</p>";
                                                cadena_final += "  </div>";

                                                cadena_final += "</div>";

                                                cadena_final += "<div class='row'>";
                                                cadena_final += " <div class='col-md-3'>";
                                                cadena_final += "<p style='font-size: 11px; '>";
                                                cadena_final += "<strong><u>Presente.-  </u></strong> ";
                                                cadena_final += "</p>";
                                                cadena_final += "  </div>";

                                                cadena_final += "</div>";
                                                cadena_final += "<div class='row'>";
                                                cadena_final += " <div class='col-md-12'>";
                                                cadena_final += "<p style='font-size: 11px; '>";
                                                cadena_final += "<strong>Referencia: </strong><u> Solicitud Nº  "+ cod_requerimiento +" de fecha " + formulario["fecRegistro"].ToString()+"</u><br>";
                                                cadena_final += "</p>";
                                                cadena_final += "<p style='font-size: 11px;text-indent: 25px; '>";
                                                cadena_final += "<strong style='color: #FFFFFF;'>Referencia: </strong>            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Delito Investigado:   " + formulario["delito"].ToString() + "</u><br>";
                                                cadena_final += "</p>";
                                                cadena_final += "<p style='font-size: 11px;text-indent: 25px; '>";
                                                cadena_final += " <strong style='color: #FFFFFF;'>Referencia: </strong>             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Denuncia/Caso/Carpeta/Exp:   " + formulario["num_denuncia"].ToString() + "</u><br>";
                                                cadena_final += "</p>";
                                                cadena_final += "  </div>";

                                                cadena_final += "</div>";
                                                cadena_final += "<br>";

                                                cadena_final += "<div class='row'>";
                                                cadena_final += " <div class='col-md-12'>";
                                                cadena_final += "<p style='font-size: 11px; '>";
                                                cadena_final += "De nuestra consideración: ";
                                                cadena_final += "</p><br>";
                                                cadena_final += "<p style='font-size: 11px;text-align: justify; line-height:100%'>";
                                                cadena_final += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tengo el agrado de dirigirme a usted con la finalidad de saludarle y hacer mención ";
                                                cadena_final += "a la solicitud de la referencia, mediante la cual su despacho solicitó a través del Sistema ";
                                                cadena_final += "de Registro de Requerimiento de Información de IMEIs - SIGREI información relacionada ";
                                                cadena_final += "a la titularidad del equipo terminal móvil con IMEI N° " + listaResultadoExep[0].IMEI.ToString().Trim()+".";
                                                cadena_final += "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sobre el particular, debemos precisar que este Organismo, a través de su página ";
                                                cadena_final += "web, ha implementado la herramienta informática denominada “Consulta IMEI”, la misma ";
                                                cadena_final += "que permite verificar los equipos terminales móviles reportados como sustraídos, perdidos ";
                                                cadena_final += "o recuperados<sup style='font-size: 7px;'>1</sup>, para ello resulta necesario contar con el código IMEI, el cual es un código ";
                                                cadena_final += "que identifica al equipo móvil de forma exclusiva a nivel mundial y cuenta con quince(15) ";
                                                cadena_final += "dígitos numéricos. Dicho código suele estar impreso en la parte posterior interna del ";
                                                cadena_final += "equipo terminal o en la bandeja de soporte de la SIM Card<sup style='font-size: 7px;'>2</sup> (IMEI físico); o se puede ";
                                                cadena_final += "visualizar en la pantalla del equipo marcando las teclas *#06# (IMEI lógico)<sup style='font-size: 7px;'>3</sup>. ";
                                                cadena_final += "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Asimismo, es importante señalar que este Organismo cuenta con la atribución de ";
                                                cadena_final += "administrar el RENTESEG<sup style='font-size: 7px;'>4</sup>; el mismo que a la fecha viene siendo implementado ";
                                                cadena_final += "progresivamente y que contiene la información de la lista negra, la cual está conformada, móviles reportados como sustraídos y perdidos en el Perú<sup style='font-size: 7px;'>5</sup>.";
                                                //cadena_final += "entre otros, por el registro de los equipos terminales <br><br> </p>";
                                                //cadena_final += "<p style='font-size: 9px;text-align: justify; line-height:85%'>";
                                                cadena_final += "<p style='font-size: 9px;text-align: justify; line-height:100%'>";
                                                cadena_final += "___________________________________________________________ <br>";
                                                cadena_final += " &nbsp;&nbsp;<br>1 Para mayor información ingresar a la página web del OSIPTEL, opción “Servicios en Línea”, luego acceder a";
                                                cadena_final += "&nbsp;“Consulta IMEI” y proceder a digitar el código de IMEI del equipo terminal móvil <br>";
                                                cadena_final += "(http://www.osiptel.gob.pe/sistemas/sigem.html). <br>";
                                                cadena_final += " 2 Para el caso de los Smartphone con batería integrada.<br>";
                                                cadena_final += "3 En los casos en que el IMEI físico y lógico no coincidan se trataría de un equipo móvil adulterado, por lo que ";
                                                cadena_final += "para la adecuada identificación del posible propietario deberá remitirnos el código de IMEI físico. <br>";
                                                cadena_final += "4 	Artículo 27° del Reglamento del Decreto Legislativo N° 1338, aprobado por el Decreto Supremo N° 007-2019-IN ";
                                                cadena_final += "<br><strong>“Articulo 27.- Atribuciones del OSIPTEL </strong><br>";
                                                cadena_final += "El OSIPTEL tiene las siguientes atribuciones: <br>";
                                                cadena_final += "a) Implementar y administrar el RENTESEG. El OSIPTEL puede designar a un tercero para la implementación ";
                                                cadena_final += "y administración de dicho registro. <br>";
                                                cadena_final += "(…)” ";
                                                cadena_final += "</p><br><br><br><br><br>";
                                                //cadena_final += "<p style='font-size: 11px;text-align: justify; line-height:100%'>móviles reportados como sustraídos ";
                                                //cadena_final += "y perdidos en el Perú<sup style='font-size: 7px;'>5</sup>. ";
                                                cadena_final += "<p style='font-size: 11px;text-align: justify; line-height:100%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;En ese sentido, a fin de atender su requerimiento, este Organismo, a través de la ";
                                                cadena_final += "Dirección de Atención y Protección del Usuario, ha procedido a realizar la consulta del ";
                                                cadena_final += "código IMEI N° "+ listaResultadoExep[0].IMEI.ToString() + " en el RENTESEG. Por tal motivo, se adjunta en el ";
                                                cadena_final += "Anexo de la presente, la información referida a los datos personales del titular del equipo ";
                                                cadena_final += "terminal móvil en cuestión, para los fines que correspondan. ";
                                                cadena_final += "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cabe mencionar que, la información proporcionada a su despacho es considerada ";
                                                cadena_final += "como confidencial, de conformidad con lo dispuesto por la Ley N° 29733 - Ley de ";
                                                cadena_final += "Protección de Datos Personales, así como lo señalado en la Lista Enunciativa de ";
                                                cadena_final += "Información Pública y Reservada, aprobada por este Organismo. ";
                                                cadena_final += "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Finalmente, en la información que se adjunta a la presente comunicación, se ";
                                                cadena_final += "detalla la empresa operadora del servicio antes descrito, a fin que su despacho, en ";
                                                cadena_final += "atención a las facultades que le otorga el ordenamiento jurídico vigente, pueda realizar ";
                                                cadena_final += "cualquier otro requerimiento que no se enmarque dentro de las obligaciones de este ";
                                                cadena_final += "organismo. ";
                                                cadena_final += "<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sin otro particular, quedamos de usted. ";
                                                cadena_final += "</p>";
                                                
                                                cadena_final += "  </div>";
                                                cadena_final += "</div>";
                                                //
                                                //cadena_final += cadena_html_user;
                                                cadena_final += "<br><br><br><br>";

                                                cadena_final += " <div class='col-md-3' style='text-align: center; text-align: center;'>";
                                                cadena_final += "<label class='etiqueta' style='font-size: 11px; '>";
                                                cadena_final += "<strong>DIRECCIÓN DE ATENCIÓN Y PROTECCIÓN DEL USUARIO</strong> ";
                                                cadena_final += "</label>";
                                                cadena_final += "  </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>";

                                                cadena_final += "<div class='row'>";
                                                cadena_final += " <div class='col-md-12'>";
                                                cadena_final += "<p style='font-size: 9px;text-align: justify; line-height:100%'>____________________________________________________________________ <br>";
                                                cadena_final += " <br>5 Reglamento del RENTESEG aprobado mediante Decreto Supremo N° 007-2019-IN:  <br>";
                                                cadena_final += "<strong>“Artículo 7.- Lista Negra </strong><br>";
                                                cadena_final += "7.1. La Lista Negra está conformada por: <br>";
                                                cadena_final += "a) El registro de los equipos terminales móviles reportados como sustraídos, perdidos, e inoperativos en el ";
                                                cadena_final += "Perú. <br>";
                                                cadena_final += "Perú. <br>";
                                                cadena_final += "(…)” ";
                                                cadena_final += "</p>";
                                                cadena_final += "  </div>";
                                                cadena_final += "</div>";
                                                cadena_final += "</div>";
                                                cadena_final += cadena_imei;



                                                Document doc2 = new Document();
                                                string ruta_archivo_excepcion = "";
                                                ruta_archivo_excepcion =  "ADJUNTO REQUERIMIENTO " + cod_requerimiento.Trim() + ".pdf";
                                                HttpContext.Session.SetString("RutaArchivoExcepcion", ruta_archivo_excepcion);
                                                PdfWriter.GetInstance(doc2, new FileStream(configuration["DocumentosEntidadPublica"] + ruta_archivo_excepcion, FileMode.Create));
                                                doc2.Open();
                                                Paragraph title = new Paragraph();
                                                // Creamos la imagen y le ajustamos el tamaño
                                                iTextSharp.text.Image imagen2 = iTextSharp.text.Image.GetInstance(configuration["DocumentosEntidadPublica"] + "LogoPortal.png");
                                                imagen2.BorderWidth = 0;
                                                imagen2.Alignment = Element.ALIGN_RIGHT;
                                                float percentage2 = 0.0f;
                                                percentage2 = 150 / imagen2.Width;
                                                imagen2.ScalePercent(percentage2 * 100);

                                                // Insertamos la imagen en el documento
                                                doc2.Add(imagen2);
                                                //title.Font = FontFactory.GetFont(FontFactory.TIMES, 18f, BaseColor.BLUE);

                                                //title.Add("Sustento de Requerimiento EP!");
                                                //doc.Add(title);
                                                //doc.Add(new Paragraph(""));
                                                List<IElement> htmlarraylist = HTMLWorker.ParseToList(new StringReader(cadena_final), null);
                                                //add the collection to the document
                                                for (int k = 0; k < htmlarraylist.Count; k++)
                                                {
                                                    doc2.Add((IElement)htmlarraylist[k]);
                                                }

                                                doc2.Close();

                                                //
                                                //ENVIANDO CORREO AL USUARIO
                                                bool res_correo = false;
                                                
                                                var seEnvia = configuration["enviarConCertificado"].ToString();
                                                /*res_correo = EnviarCorreoSinCertificado(
                                                     listaResultadoUpdCabecera[0].HOSTNAME,
                                                     listaResultadoUpdCabecera[0].PUERTO,
                                                     listaResultadoUpdCabecera[0].DE_PARTE,
                                                     listaResultadoUpdCabecera[0].CLAVE_PARTE,
                                                     formulario["correo"].ToString(),
                                                     listaResultadoUpdCabecera[0].ASUNTO,
                                                     listaResultadoUpdCabecera[0].MENSAJE,
                                                     listaResultadoUpdCabecera[0].COPIA);*/
                                                if (seEnvia.Equals("1"))
                                                {
                                                    res_correo = EnviarCorreoConCertificado(
                                                listaResultadoUpdCabecera[0].HOSTNAME,
                                                listaResultadoUpdCabecera[0].PUERTO,
                                                listaResultadoUpdCabecera[0].DE_PARTE,
                                                listaResultadoUpdCabecera[0].CLAVE_PARTE,
                                                formulario["correo"].ToString(),
                                                listaResultadoUpdCabecera[0].ASUNTO,
                                                listaResultadoUpdCabecera[0].MENSAJE,
                                                listaResultadoUpdCabecera[0].COPIA);
                                                }
                                                else
                                                {
                                                    res_correo = EnviarCorreoSinCertificado(
                                                     listaResultadoUpdCabecera[0].HOSTNAME,
                                                     listaResultadoUpdCabecera[0].PUERTO,
                                                     listaResultadoUpdCabecera[0].DE_PARTE,
                                                     listaResultadoUpdCabecera[0].CLAVE_PARTE,
                                                     formulario["correo"].ToString(),
                                                     listaResultadoUpdCabecera[0].ASUNTO,
                                                     listaResultadoUpdCabecera[0].MENSAJE,
                                                     listaResultadoUpdCabecera[0].COPIA);
                                                }


                                                //
                                            }
                                        }
                                        //
                                        //
                                    }
                                }
                                /*item.usuCre = HttpContext.Session.GetString("IdUsuario");
                                foreach (var itemImei in item.listaIMEI)
                                {
                                    itemImei.idRequerimiento = idRequerimiento;
                                    itemImei.usuCre = HttpContext.Session.GetString("IdUsuario"); ;
                                    idRequerimientoIMEi = objLogicaIMEI.InsertarImeiInterno(itemImei);
                                }*/
                            }
                        }
                    }
                    //
                    keyValueError = new KeyValuePair<string, string>("1", "El proceso concluyó de forma exitosa, el número de Requerimiento Generado es: " + cod_requerimiento + "//" + idRequerimiento + "//" + cod_requerimiento);


                }
                else
                {
                    keyValueError = new KeyValuePair<string, string>("1", "Usted debe ingresar Imei o Servicio Movil..");
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "GrabarInformacion");
                keyValueError = new KeyValuePair<string, string>("0", "Error al guardar los requerimientos de IMEI");
            }
            return Json(keyValueError);
        }
        public Boolean EnviarCorreoConCertificado(string HOSTNAME, string PUERTO, string DE_PARTE, string CLAVE_PARTE, string PARA, string ASUNTO, string MENSAJE, string COPIA)
        {
            Boolean EnviadoCorreo = false;

            try
            {
                MailMessage message = new MailMessage();

                StringBuilder Message_sec = new StringBuilder();
                Message_sec.AppendLine();
                //message.From = new MailAddress("info@chromoperu.com", "SIGREI", System.Text.Encoding.UTF8);
                message.From = new MailAddress(DE_PARTE);
                //message.HeadersEncoding = System.Text.Encoding.UTF8; 
                message.Subject = ASUNTO;

                var contentType = new ContentType("application/pkcs7-mime; smime-type=enveloped-data; name=smime.p7m");
                string attachmentFilename = configuration["DocumentosEntidadPublica"] + HttpContext.Session.GetString("RutaArchivoExcepcion");
                Attachment attachment = new Attachment(attachmentFilename, contentType);
                attachment.ContentDisposition.FileName = "smime.p7m";
                attachment.TransferEncoding = TransferEncoding.Base64;
                //ContentDisposition disposition = attachment.ContentDisposition;

                attachment.ContentDisposition.FileName = Path.GetFileName(attachmentFilename);
                //disposition.Size = new FileInfo(attachmentFilename).Length;
                //disposition.DispositionType = DispositionTypeNames.Attachment;
                //message.Attachments.Add(attachment);



                StringBuilder Message = new StringBuilder();

                Message.AppendLine("content-type: multipart/mixed;");

                Message.AppendLine(" boundary=\"----=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX\"");

                Message.AppendLine();

                Message.AppendLine("This is a multi-part message in MIME format.");

                Message.AppendLine();

                Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX");

                Message.AppendLine("content-type: text/html; charset=\"us-ascii\"");

                Message.AppendLine("content-transfer-encoding: 7bit");

                Message.AppendLine();

                Message.AppendLine(MENSAJE);

                Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX");

                Message.AppendLine("content-disposition: attachment; filename=\"" + attachment.ContentDisposition.FileName + "\"");

                Message.AppendLine("content-transfer-encoding: base64");

                Message.AppendLine("content-type: application/octet;");

                Message.AppendLine(" name=\"" + attachment.ContentDisposition.FileName + "\"");

                Message.AppendLine();

                //string data = System.IO.File.ReadAllText(@""+attachmentFilename);
                string data = System.IO.File.ReadAllText(@"" + attachmentFilename);
                String path = @"" + attachmentFilename;
                //List<String> base64Strings = GetBase64Strings(path, 1);
                //Message.AppendLine(Convert.ToBase64String(Encoding.ASCII.GetBytes(GetBase64String(path))));
                Message.AppendLine(GetBase64String(path));


                Message.AppendLine("------=_NextPart_414_0c36_f93e4f59.6ee76195_.MIX--");

                message.To.Add(new MailAddress(PARA));
                if (COPIA == null)
                {
                    COPIA = "";
                }
                var conCopia = COPIA.Split(";");
                if (conCopia.Length > 0)
                {
                    foreach (var item in conCopia)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }
                

                var certificado = configuration["RutaCertificado"];
                var claveCertificado = configuration["ClaveCertificado"];
                X509Certificate2 objCert = new X509Certificate2(certificado, claveCertificado, X509KeyStorageFlags.MachineKeySet
                             | X509KeyStorageFlags.PersistKeySet
                             | X509KeyStorageFlags.Exportable);

                //Creamos el ContentInfo  
                ContentInfo objContent = new ContentInfo(Encoding.ASCII.GetBytes(Message.ToString()));
                //ContentInfo objContent = new ContentInfo(Encoding.ASCII.GetBytes("Content-Type: text/html; charset=ISO-8859-1;Content-Transfer-Encoding: 7bit \r\n\r\n" + Message));
                //Creamos el objeto que representa los datos firmados  
                SignedCms objSignedData = new SignedCms(objContent);
                //Creamos el "firmante"  
                CmsSigner objSigner = new CmsSigner(objCert);
                objSigner.IncludeOption = X509IncludeOption.EndCertOnly;
                //Firmamos los datos  
                //mensaje("Antes de firmar los datos");
                objSignedData.ComputeSignature(objSigner);
                //Obtenemos el resultado  
                byte[] bytSigned = objSignedData.Encode();

                CmsRecipientCollection toCollection = new CmsRecipientCollection();

                //foreach (string address in to)


                X509Certificate2 certificate = new X509Certificate2(certificado, claveCertificado, X509KeyStorageFlags.MachineKeySet
                     | X509KeyStorageFlags.PersistKeySet
                     | X509KeyStorageFlags.Exportable);

                CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                toCollection.Add(recipient);

                MemoryStream stream = new MemoryStream(bytSigned);
                //AlternateView view = new AlternateView(stream, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");
                AlternateView view = new AlternateView(stream, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m; content-transfer-encoding=Base64; content-disposition=attachment; fileName=smime.p7m;");

                message.AlternateViews.Add(view);

                //message.Body = MENSAJE;
                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient(HOSTNAME, Int32.Parse(PUERTO));
                //SmtpClient client = new SmtpClient("mail.chromoperu.com", Int32.Parse("25"));
                var UsuarioRemitente = configuration["UsuarioRemitente"];
                var ClaveRemitente = configuration["ClaveRemitente"];
                var Dominio = configuration["Dominio"];

                client.Credentials = new System.Net.NetworkCredential(UsuarioRemitente, ClaveRemitente, Dominio);
                //client.Credentials = new System.Net.NetworkCredential("info@chromoperu.com", "gHMljBTgK1hN", Dominio);

                client.EnableSsl = false;
                client.Send(message);
                EnviadoCorreo = true;
            }
            catch (Exception ex)
            {
                EnviadoCorreo = false;
                LogError.RegistrarErrorMetodo(ex, "EnviarCorreoConCertificado");
            }

            return EnviadoCorreo;
        }
        static String GetBase64String(String path)
        {
            Byte[] bytes = System.IO.File.ReadAllBytes(path);
            String base64String = Convert.ToBase64String(bytes);

            return base64String;
        }
        private Boolean EnviarCorreoSinCertificado(string HOSTNAME, string PUERTO, string DE_PARTE, string CLAVE_PARTE, string PARA, string ASUNTO, string MENSAJE,string COPIA)
        {
            Boolean isEnviado = false;
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(DE_PARTE);
                message.To.Add(new MailAddress(PARA));
                byte[] bytes = Encoding.Default.GetBytes(ASUNTO);
                string myString = Encoding.UTF8.GetString(bytes);
                message.Subject = myString;
                //message.From = new MailAddress("info@chromoperu.com", "SIGREI", System.Text.Encoding.UTF8);
                message.HeadersEncoding = System.Text.Encoding.UTF8;
               
                //message.SubjectEncoding = UTF8Encoding.UTF8;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                //message.Attachments.Add(new Attachment(configuration["DocumentosEntidadPublica"] + HttpContext.Session.GetString("RutaArchivoExcepcion")));
                string attachmentFilename = configuration["DocumentosEntidadPublica"] + HttpContext.Session.GetString("RutaArchivoExcepcion");
                Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                ContentDisposition disposition = attachment.ContentDisposition;
                //disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                //disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                //disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                disposition.FileName = Path.GetFileName(attachmentFilename);
                disposition.Size = new FileInfo(attachmentFilename).Length;
                disposition.DispositionType = DispositionTypeNames.Attachment;
                message.Attachments.Add(attachment);

                if (COPIA == null)
                {
                    COPIA = "";
                }

                var conCopia = COPIA.Split(";");
                if (conCopia.Length > 0)
                {
                    foreach (var item in conCopia)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }

                message.Body = MENSAJE;

                //message.BodyEncoding = UTF8Encoding.UTF8;
                message.BodyEncoding = System.Text.Encoding.UTF8;
               
                /*message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.BodyEncoding = Encoding.GetEncoding("utf-8");
                message.SubjectEncoding = Encoding.GetEncoding("utf-8");*/
                message.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient(HOSTNAME, Int32.Parse(PUERTO));
                //SmtpClient smtpMail = new SmtpClient("mail.chromoperu.com", Int32.Parse("25"));

                var UsuarioRemitente = configuration["UsuarioRemitente"];
                var ClaveRemitente = configuration["ClaveRemitente"];
                var Dominio = configuration["Dominio"];

                smtpMail.UseDefaultCredentials = false;
                smtpMail.Credentials = new System.Net.NetworkCredential(UsuarioRemitente, ClaveRemitente, Dominio);
               // smtpMail.Credentials = new System.Net.NetworkCredential("info@chromoperu.com", "gHMljBTgK1hN");
                smtpMail.EnableSsl = true;
                //smtpMail.
                smtpMail.Send(message);
                isEnviado = true;

               

            }
            catch (Exception ex)
            {
                isEnviado = false;
                LogError.RegistrarErrorMetodo(ex, "EnviarCorreoSinCertificado");

            }
            return isEnviado;
        }

        [HttpPost]
        public ActionResult ConfirmarRequeimientoEP(IFormCollection formulario)
        {
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();
            try
            {

                String cadena_final = "";

                BL_REQUERIMIENTO objLogicaRequerimiento = new BL_REQUERIMIENTO();
                int respuesta = 0;
                respuesta = objLogicaRequerimiento.ConfirmarRegistroReqEP(Convert.ToInt16( formulario["DES_PUESTO"].ToString()));

                //CONSULTANDO DATOS GENERALES
                List<BE_REQUERIMIENTO> lista;
                BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
                lista = ObjLogica.ListarDetalleRequerimientoEP(formulario["DES_PUESTO"].ToString());

                //CONSULTABDO DATOS IMEI
                BL_REQUERIMIENTO_IMEI objIMEI = new BL_REQUERIMIENTO_IMEI();
                List<BE_REQUERIMIENTO_IMEI> listaResultadoMovil = new List<BE_REQUERIMIENTO_IMEI>();

                listaResultadoMovil = objIMEI.ConsultarMovilPendietes(formulario["DES_PUESTO"].ToString());
                string cadena_html_user = "";
                if (listaResultadoMovil.Count > 0)
                {
                    cadena_html_user += "<h4>DETALLE MOVIL</h4><br>";
                    cadena_html_user += "<div class='tabla-content-result'>";
                    cadena_html_user += "<table id='dtResultadCarMasMovilEP' border='1' class='tabla-osiptel' style='width: 100%'>";
                    cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                    cadena_html_user += " <thead>";
                    cadena_html_user += "<tr>";
                    cadena_html_user += "<th scope='col' style=' background-color:#9c9c9c;text-align:center;'><strong>Nº</strong></th>";
                    cadena_html_user += "<th scope='col' style=' background-color:#9c9c9c;text-align:center;'><strong>Dato</strong></th>";
                    cadena_html_user += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Revisión Preliminar</strong></th>";
                    cadena_html_user += "<th scope='col' style=' background-color:#9c9c9c;text-align:center;'><strong>¿Procede a Cargar?</strong></th>";
                    cadena_html_user += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Sustento</strong></th>";
                    cadena_html_user += "</tr>";
                    cadena_html_user += " </thead>";
                    cadena_html_user += "<tbody>";
                    for (int i = 0; i < listaResultadoMovil.Count; i++)
                    {
                        cadena_html_user += " <tr>";
                        cadena_html_user += "<td> " + listaResultadoMovil[i].nroItem.ToString() + "</td>";
                        cadena_html_user += "<td> " + listaResultadoMovil[i].nroImei.ToString() + "</td>";
                        string des_pre = "Incorrecto";
                        string proc_cargar = "No procede";
                        if (listaResultadoMovil[i].validacionFormato.Trim() == "")
                        {
                            des_pre = "Correcto";
                            proc_cargar = "Si";
                        }
                        cadena_html_user += "<td> " + des_pre.ToString() + "</td>";

                        cadena_html_user += "<td> " + proc_cargar.ToString() + "</td>";
                        cadena_html_user += "<td> " + listaResultadoMovil[i].validacionFormato.ToString() + " - " + listaResultadoMovil[i].estadoLuhn + "</td>";
                        cadena_html_user += "</tr>";



                    }

                    cadena_html_user += " </tbody>";
                    cadena_html_user += "   </table>";
                    cadena_html_user += "</div>";

                }


                //
                //CONSULTANDO SI EXISTEN REGISTROS DE TELEFONO
                //BL_REQUERIMIENTO_IMEI objIMEI = new BL_REQUERIMIENTO_IMEI();
                List<BE_REQUERIMIENTO_IMEI> listaResultadoImei = new List<BE_REQUERIMIENTO_IMEI>();

                listaResultadoImei = objIMEI.ConsultarImeiPendietes(formulario["DES_PUESTO"].ToString());
                string cadena_imei = "";
                if (listaResultadoImei.Count > 0)
                {
                    cadena_imei += "<h4>DETALLE IMEI</h4><br>";
                    cadena_imei += "<div class='tabla-content-result'>";
                    cadena_imei += "<table id='dtResultadCarMasImeiEP' border='1' class='tabla-osiptel' style='width: 100%;'>";
                    cadena_imei += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                    cadena_imei += " <thead>";
                    cadena_imei += "<tr>";
                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Nª</strong></th>";
                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Dato</strong></th>";
                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Revisión Preliminar</strong></th>";
                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>¿Procede a Cargar?</strong></th>";
                    cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Sustento</strong></th>";
                    cadena_imei += "</tr>";
                    cadena_imei += " </thead>";
                    cadena_imei += "<tbody>";
                    for (int i = 0; i < listaResultadoImei.Count; i++)
                    {
                        cadena_imei += " <tr>";
                        cadena_imei += "<td> " + listaResultadoImei[i].nroItem.ToString() + "</td>";
                        cadena_imei += "<td> " + listaResultadoImei[i].nroImei.ToString() + "</td>";
                        string des_pre = "Incorrecto";
                        string proc_cargar = "No procede";
                        if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].estadoLuhn.Trim() == "Lunh válido")
                        //if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].ultimoDigito != 0)
                        {
                            des_pre = "Correcto";
                            proc_cargar = "Si";
                        }
                        cadena_imei += "<td> " + des_pre.ToString() + "</td>";

                        cadena_imei += "<td> " + proc_cargar.ToString() + "</td>";
                        cadena_imei += "<td> " + listaResultadoImei[i].validacionFormato.ToString() + " - " + listaResultadoImei[i].estadoLuhn + "</td>";
                        cadena_imei += "</tr>";



                    }

                    cadena_imei += " </tbody>";
                    cadena_imei += "   </table>";
                    cadena_imei += "</div>";
                }
                //
                //CONCATENADO CADENAS
                int cant = Convert.ToInt16(lista[0].nroImei) + Convert.ToInt16(lista[0].nroImeiDos);
                cadena_final += "<br><h4><strong>Sustento Requerimiento EP</strong></h4><br><div class='row'>";
                cadena_final += " <div class='col-md-3'>";
                cadena_final += "<label class='etiqueta'>";
                cadena_final += " <strong>Nº de Requerimiento : <strong>" + lista[0].nroRequerimiento;
                cadena_final += "</label>";
                cadena_final += "  </div>";
                /*cadena_final += " <div class='col-md-3'>";
                cadena_final +=   lista[0].nroRequerimiento ;

                cadena_final += "  </div>";*/
                cadena_final += "</div>";
                cadena_final += "<div class='row'>";
                cadena_final += " <div class='col-md-3'>";
                cadena_final += "<label class='etiqueta'>";
                cadena_final += " <strong>Usuario Solicitante : </strong>" + lista[0].nombreSolicitante;
                cadena_final += "</label>";
                cadena_final += "  </div>";
                /*cadena_final += " <div class='col-md-3'>";
                cadena_final += lista[0].nombreSolicitante ;
                cadena_final += "  </div>";*/
                cadena_final += "</div>";
                cadena_final += "<div class='row'>";
                cadena_final += " <div class='col-md-3'>";
                cadena_final += "<label class='etiqueta'>";
                cadena_final += "<strong> Total de Data Procesada : </strong> " + cant;
                cadena_final += "</label>";
                cadena_final += "  </div>";
                /*cadena_final += " <div class='col-md-3'>";
                cadena_final +=  cant ;

                cadena_final += "  </div>";*/
                cadena_final += "</div>";
                cadena_final += "<br><br>";
                //
                cadena_final += cadena_html_user;
                cadena_final += cadena_imei;

                /*StringBuilder sb = new StringBuilder();
                sb.Append(cadena_final);

                StringReader sr = new StringReader(sb.ToString());

                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    pdfDoc.Open();

                    htmlparser.Parse(sr);
                    //pdfDoc.Close();

                    byte[] bytes = memoryStream.ToArray();
                    memoryStream.Close();
                }*/

                Document doc = new Document();
                PdfWriter.GetInstance(doc, new FileStream(configuration["DocumentosEntidadPublica"] + "SustentoReqEP.pdf", FileMode.Create));
                doc.Open();
                Paragraph title = new Paragraph();
                // Creamos la imagen y le ajustamos el tamaño
                iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(configuration["DocumentosEntidadPublica"] + "LogoPortal.png");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_RIGHT;
                float percentage = 0.0f;
                percentage = 150 / imagen.Width;
                imagen.ScalePercent(percentage * 100);

                // Insertamos la imagen en el documento
                doc.Add(imagen);
                //title.Font = FontFactory.GetFont(FontFactory.TIMES, 18f, BaseColor.BLUE);

                //title.Add("Sustento de Requerimiento EP!");
                //doc.Add(title);
                //doc.Add(new Paragraph(""));
                List<IElement> htmlarraylist = HTMLWorker.ParseToList(new StringReader(cadena_final), null);
                //add the collection to the document
                for (int k = 0; k < htmlarraylist.Count; k++)
                {
                    doc.Add((IElement)htmlarraylist[k]);
                }

                doc.Close();



                keyValueError = new KeyValuePair<string, string>("1", "Se registró correctamente el requerimiento" );
                  



            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "GrabarInformacion");
                keyValueError = new KeyValuePair<string, string>("0", "Error al guardar los requerimientos de IMEI");
            }
            return Json(keyValueError);
        }

        [HttpPost]
        public ActionResult BuscarRequerimientoEP(IFormCollection formulario)
        {

            String nom_vista = "";
            List<BE_REQUERIMIENTO> lista;

            BE_REQUERIMIENTO ParametroEPBE = new BE_REQUERIMIENTO();

            //VALIDANDO CAMPOS QUE SE ENCIAN PARA BUSQUEDA
            string dFecInicio = " ";
            string dFecFin = " ";
            string nCodRequerimiento = " ";
            string Estado = " ";

            if (formulario["Lista_Estado"].ToString() != null && formulario["Lista_Estado"].ToString() != "")
            {
                Estado = formulario["Lista_Estado"].ToString();
            }
            if (formulario["txtNroRequerimiento"].ToString() != null && formulario["txtNroRequerimiento"].ToString() != "")
            {
                nCodRequerimiento = formulario["txtNroRequerimiento"].ToString();
            }
            if (formulario["txtPerDesde"].ToString() != null && formulario["txtPerDesde"].ToString() != "")
            {
                /*var arr = formulario["txtPerDesde"].ToString().Split("-");
                
                dFecInicio = arr[2] + "/" + arr[1] + "/" + arr[0];*/

                dFecInicio = formulario["txtPerDesde2"].ToString();
            }
            if (formulario["txtPerHasta"].ToString() != null && formulario["txtPerHasta"].ToString() != "")
            {
                /*var arr = formulario["txtPerHasta"].ToString().Split("-");
                
                dFecFin = arr[2] + "/" + arr[1] + "/" + arr[0];*/

                dFecFin = formulario["txtPerHasta2"].ToString();
            }

            //




            BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
            lista = ObjLogica.ListarRequerimientoEP(Estado,nCodRequerimiento,dFecInicio,dFecFin, HttpContext.Session.GetString("IdUsuario"));

            nom_vista = "_DetalleRequerimientoEP";

            var listaResultadoJSON = JsonConvert.SerializeObject(lista);
            HttpContext.Session.SetString("ListaBusquedaReqEP", listaResultadoJSON);

            return PartialView(nom_vista, lista);





        }

        [HttpPost]
        public ActionResult DetalleRequerimientoEP(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = DetalleRequerimientoEP_Cargar(formulario);


                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        private KeyValuePair<string, string> DetalleRequerimientoEP_Cargar(IFormCollection formulario)
        {
            string cadena_final = "";
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            //CONSULTANDO DATOS GENERALES
            List<BE_REQUERIMIENTO> lista;
            BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
            lista = ObjLogica.ListarDetalleRequerimientoEP(formulario["hiidValorReqEP"].ToString());

            //CONSULTABDO DATOS IMEI
            BL_REQUERIMIENTO_IMEI objIMEI = new BL_REQUERIMIENTO_IMEI();
            List<BE_REQUERIMIENTO_IMEI> listaResultadoMovil = new List<BE_REQUERIMIENTO_IMEI>();

            listaResultadoMovil = objIMEI.ConsultarMovilPendietes(formulario["hiidValorReqEP"].ToString());
            string cadena_html_user = "";
            if (listaResultadoMovil.Count > 0)
            {
                cadena_html_user += "<div class='tabla-content-result'>";
                cadena_html_user += "<table id='dtResultadCarMasMovilEP' class='tabla-osiptel' style='width: 100%'>";
                cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_html_user += " <thead>";
                cadena_html_user += "<tr>";
                cadena_html_user += "<th scope='col'>Nº</th>";
                cadena_html_user += "<th scope='col'>Dato</th>";
                cadena_html_user += "<th scope='col'>Revisión Preliminar</th>";
                //cadena_html_user += "<th scope='col'>¿Procede a Cargar?</th>";
                //cadena_html_user += "<th scope='col'>Sustento</th>";
                cadena_html_user += "</tr>";
                cadena_html_user += " </thead>";
                cadena_html_user += "<tbody>";
                for (int i = 0; i < listaResultadoMovil.Count; i++)
                {
                    cadena_html_user += " <tr>";
                    cadena_html_user += "<td>" + listaResultadoMovil[i].nroItem.ToString() + "</td>";
                    cadena_html_user += "<td>" + listaResultadoMovil[i].nroImei.ToString() + "</td>";

                    if (decimal.TryParse(listaResultadoMovil[i].nroImei.ToString(), out decimal valor) && listaResultadoMovil[i].nroImei.ToString().Contains(" ") != true)
                    {
                        if (listaResultadoMovil[i].nroImei.ToString().Length == 9)
                        {
                            if (listaResultadoMovil[i].validacionFormato.Trim() == "")
                            {
                                cadena_html_user += "<td>Correcto</td>";
                                //cadena_html_user += "<td>Sí procede</td>";
                            }
                            else
                            {
                                cadena_html_user += "<td style='color: red;'>No se encuentra en el Registro de Abonados</td>";
                                //cadena_html_user += "<td style='color: red;'>No Procede</td>";
                            }
                        }
                        else
                        {
                            cadena_html_user += "<td style='color: red;'>No tiene 9 dígitos.</td>";
                            //cadena_html_user += "<td style='color: red;'>No Procede</td>";
                        }
                    }
                    else
                    {
                        cadena_html_user += "<td style='color: red;'>Tiene caracteres no numéricos o espacios.</td>";
                        //cadena_html_user += "<td style='color: red;'>No Procede</td>";
                    }
                    cadena_html_user += "</tr>";

                }
                /*for (int i = 0; i < listaResultadoMovil.Count; i++)
                {
                    cadena_html_user += " <tr>";
                    cadena_html_user += "<td> " + listaResultadoMovil[i].nroItem.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaResultadoMovil[i].nroImei.ToString() + "</td>";
                    string des_pre = listaResultadoMovil[i].validacionFormato.ToString() + " - " + listaResultadoMovil[i].estadoLuhn;
                    string proc_cargar = "No procede";
                    if (listaResultadoMovil[i].validacionFormato.Trim() == "")
                    {
                        des_pre = "Correcto";
                        proc_cargar = "Si";
                    }
                    int pos_cortar = 15;
                    if (des_pre.ToString().Length < 16)
                    {
                        pos_cortar = des_pre.ToString().Length;
                    }
                    if (proc_cargar.Trim() == "No procede")
                    {
                        //cadena_html_user += "<td style='color: red;'> " + des_pre.ToString().Substring(0, pos_cortar) + "...</td>";
                        cadena_html_user += "<td style='color: red; text-align: center;'> " + des_pre.ToString()+ "...</td>";

                        //cadena_html_user += "<td style='color: red;'> " + proc_cargar.ToString() + "</td>";
                    }
                    else
                    {
                        //cadena_html_user += "<td> " + des_pre.ToString().Substring(0, pos_cortar) + "...</td>";
                        cadena_html_user += "<td style='text-align: center;'> " + des_pre.ToString() + "...</td>";

                        //cadena_html_user += "<td> " + proc_cargar.ToString() + "</td>";
                    }
                    //cadena_html_user += "<td> " + listaResultadoMovil[i].validacionFormato.ToString() + " - " + listaResultadoMovil[i].estadoLuhn + "</td>";
                    cadena_html_user += "</tr>";



                }*/

                cadena_html_user += " </tbody>";
                cadena_html_user += "   </table>";
                cadena_html_user += "</div>";

            }


            //
            //CONSULTANDO SI EXISTEN REGISTROS DE TELEFONO
            //BL_REQUERIMIENTO_IMEI objIMEI = new BL_REQUERIMIENTO_IMEI();
            List<BE_REQUERIMIENTO_IMEI> listaResultadoImei = new List<BE_REQUERIMIENTO_IMEI>();

            listaResultadoImei = objIMEI.ConsultarImeiPendietes(formulario["hiidValorReqEP"].ToString());
            string cadena_imei = "";
            if (listaResultadoImei.Count > 0)
            {

                cadena_imei += "<div class='tabla-content-result'>";
                cadena_imei += "<table id='dtResultadCarMasImeiEP' class='tabla-osiptel' style='width: 100%'>";
                cadena_imei += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_imei += " <thead>";
                cadena_imei += "<tr>";
                cadena_imei += "<th scope='col'>Nº</th>";
                cadena_imei += "<th scope='col'>Dato</th>";
                cadena_imei += "<th scope='col'>Revisión Preliminar</th>";
                //cadena_imei += "<th scope='col'>¿Procede a Cargar?</th>";
                //cadena_imei += "<th scope='col'>Sustento</th>";
                cadena_imei += "</tr>";
                cadena_imei += " </thead>";
                cadena_imei += "<tbody>";
                /*for (int i = 0; i < listaResultadoImei.Count; i++)
                 {
                     cadena_imei += " <tr>";
                     cadena_imei += "<td> " + listaResultadoImei[i].nroItem.ToString() + "</td>";
                     cadena_imei += "<td> " + listaResultadoImei[i].nroImei.ToString() + "</td>";
                     string des_pre = listaResultadoImei[i].validacionFormato.ToString() + " - " + listaResultadoImei[i].estadoLuhn;
                     string proc_cargar = "No procede";
                     if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].estadoLuhn.Trim() == "Lunh válido")
                     //if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].ultimoDigito != 0)
                     {
                         des_pre = "Correcto";
                         proc_cargar = "Si";
                     }
                     cadena_imei += "<td> " + des_pre.ToString().Substring(0,15) + "...</td>";

                     cadena_imei += "<td> " + proc_cargar.ToString() + "</td>";
                     //cadena_imei += "<td> " + listaResultadoImei[i].validacionFormato.ToString() + " - " + listaResultadoImei[i].estadoLuhn + "</td>";
                     cadena_imei += "</tr>";



                 }*/
                for (int i = 0; i < listaResultadoImei.Count; i++)
                {
                    cadena_imei += "<tr>";
                    cadena_imei += "<td>" + listaResultadoImei[i].nroItem.ToString() + "</td>";
                    cadena_imei += "<td>" + listaResultadoImei[i].nroImei.ToString() + "</td>";

                    if (Decimal.TryParse(listaResultadoImei[i].nroImei.ToString(), out Decimal valor) && listaResultadoImei[i].nroImei.ToString().Contains(" ") != true)
                    {
                        if (listaResultadoImei[i].nroImei.ToString().Length == 15)
                        {
                            if (listaResultadoImei[i].estadoLuhn.Trim() == "Lunh valido" || listaResultadoImei[i].estadoLuhn.Trim() == "Lunh válido")
                            {
                                if (listaResultadoImei[i].estadoIMEI.Trim() == "Sustraído" || listaResultadoImei[i].estadoIMEI.Trim() == "Perdido")
                                {
                                    cadena_imei += "<td>Correcto</td>";
                                    //cadena_imei += "<td>Sí Procede</td>";
                                }
                                else if (listaResultadoImei[i].estadoIMEI.Trim() == "Recuperado")
                                {
                                    cadena_imei += "<td style='color: red;'>IMEI reportado como RECUPERADO.</td>";
                                    //cadena_imei += "<td style='color: red;'>No Procede</td>";
                                }
                                else
                                {
                                    //cadena_imei += "<td style='color: red;'>IMEI no reportado como SUSTRAÍDO o PERDIDO.</td>";
                                    //cadena_imei += "<td style='color: red;'>No Procede</td>";
                                    if (listaResultadoImei[i].estadoIMEI.Trim() == "Bloqueado Inválido")
                                    {
                                        //sust.revi = "IMEI reportado como CAMPAÑA BLOQUEO INVÁLIDO";
                                        cadena_imei += "<td style='color: red;'>IMEI reportado como CAMPAÑA BLOQUEO INVÁLIDO</td>";

                                    }
                                    else
                                    {
                                        cadena_imei += "<td style='color: red;'>IMEI no reportado como SUSTRAÍDO o PERDIDO.</td>";
                                        
                                    }
                                }
                            }
                            else
                            {
                                cadena_imei += "<td style='color: red;'>IMEI no cumple algoritmo de verificación.</td>";
                                //cadena_imei += "<td style='color: red;'>No Procede</td>";
                            }
                        }
                        else
                        {
                            cadena_imei += "<td style='color: red;'>No tiene 15 dígitos.</td>";
                            //cadena_imei += "<td style='color: red;'>No Procede</td>";
                        }
                    }
                    else
                    {
                        cadena_imei += "<td style='color: red;'>Tiene caracteres no numéricos o espacios.</td>";
                        //cadena_imei += "<td style='color: red;'>No Procede</td>";
                    }
                    cadena_imei += "</tr>";
                }
                cadena_imei += " </tbody>";
                cadena_imei += "   </table>";
                cadena_imei += "</div>";
            }
            //
            //CONCATENADO CADENAS
            int cant = Convert.ToInt16(lista[0].nroImei) + Convert.ToInt16(lista[0].nroImeiDos);
            cadena_final += "<div class='row'>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<label class='etiqueta'>";
            cadena_final += " Nº de Requerimiento";
            cadena_final += "</label>";
            cadena_final += "  </div>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<input id='txtCodRegistro' value='" + lista[0].nroRequerimiento + "' class='input-content' type='text' style='width:100%' disabled>";

            cadena_final += "  </div>";
            cadena_final += "</div>";
            cadena_final += "<div class='row'>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<label class='etiqueta'>";
            cadena_final += " Usuario Solicitante";
            cadena_final += "</label>";
            cadena_final += "  </div>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<input id='txtCodRegistro2' value='" + lista[0].nombreSolicitante + "' class='input-content' type='text' style='width:100%' disabled>";
            cadena_final += "  </div>";
            cadena_final += "</div>";
            cadena_final += "<div class='row'>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<label class='etiqueta'>";
            cadena_final += " Total de Data Procesada";
            cadena_final += "</label>";
            cadena_final += "  </div>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<input id='txtCodRegistro3' value='" + cant + "' class='input-content' type='text' style='width:100%' disabled>";
            
            cadena_final += "  </div>";
            cadena_final += "</div>";
            cadena_final += "<br><br>";
            //
            cadena_final += cadena_html_user;
            cadena_final += cadena_imei;
            keyValueError = new KeyValuePair<string, string>("1", cadena_final);
           
            return keyValueError;
        }

        public FileResult DescargarExcelBusqueda()
        {
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");
            string horas = hoy.ToString("HHmmss");
            List<BE_REQUERIMIENTO> lista = new List<BE_REQUERIMIENTO>();

            lista = JsonConvert.DeserializeObject<List<BE_REQUERIMIENTO>>(HttpContext.Session.GetString("ListaBusquedaReqEP"));
            

            string nom_archivo = "Busqueda de Requerimiento" + fecha + "-" + horas + ".xlsx";
            HttpContext.Session.SetString("NombreArchBusquedaEP", nom_archivo);
            FileStream fs = new FileStream(configuration["CarpetaTemporal"] +"\\"+ nom_archivo, FileMode.Create);
            ExcelPackage Excel = new ExcelPackage(fs);

            /* Creación del estilo. */
            Excel.Workbook.Styles.CreateNamedStyle("Moneda");
            ExcelNamedStyleXml moneda = Excel.Workbook.Styles.NamedStyles[1];// 0 = Normal, 1 (El que acabamos de agregar).

            Excel.Workbook.Worksheets.Add("Busqueda de Requerimiento");
            ExcelWorksheet hoja = Excel.Workbook.Worksheets["Busqueda de Requerimiento"];

            /* Num Caracteres + 1.29 de Margen.
            Los índices de columna empiezan desde 1. */
            hoja.Column(1).Width = 11.29f;

            ExcelRange rango = hoja.Cells["A1"];
            rango.Value = "Nº";

            ExcelRange rangoB = hoja.Cells["B1"];
            rangoB.Value = "Nombre de Institución";

            ExcelRange rangoC = hoja.Cells["C1"];
            rangoC.Value = "Nº DE Requerimiento";

            ExcelRange rangoD = hoja.Cells["D1"];
            rangoD.Value = "Fecha de Registro";

            ExcelRange rangoE = hoja.Cells["E1"];
            rangoE.Value = "Estado";

            ExcelRange rangoF = hoja.Cells["F1"];
            rangoF.Value = "Tipo de Dato";

            ExcelRange rangoG = hoja.Cells["G1"];
            rangoG.Value = "Total de IMEI";

            ExcelRange rangoH = hoja.Cells["H1"];
            rangoH.Value = "Total de MOVIL";



            int contador = 2;
            for (var i = 0; i < lista.Count; i++)
            {
                ExcelRange rangoAA = hoja.Cells["A" + contador];
                rangoAA.Value = lista[i].nroItem;

                ExcelRange rangoBB = hoja.Cells["B" + contador];
                rangoBB.Value = lista[i].nombreInstitucion;

                ExcelRange rangoCC = hoja.Cells["C" + contador];
                rangoCC.Value = lista[i].nroRequerimiento;

                ExcelRange rangoDD = hoja.Cells["D" + contador];
                rangoDD.Value = lista[i].fecRegistro;

                ExcelRange rangoEE = hoja.Cells["E" + contador];
                rangoEE.Value = lista[i].descEstado;

                ExcelRange rangoFF = hoja.Cells["F" + contador];
                rangoFF.Value = lista[i].descTipoOrigen;

                ExcelRange rangoGG = hoja.Cells["G" + contador];
                rangoGG.Value = lista[i].nroImei;

                ExcelRange rangoHH = hoja.Cells["H" + contador];
                rangoHH.Value = lista[i].nroImeiDos;



                contador++;
            }




            Excel.Save();

            // No estoy seguro de ésta parte, pero mejor cerramos el stream de archivo.
            fs.Close();
            fs.Dispose();

            Excel.Dispose();
            //var rutaPlantilla = configuration["CredencialUsuario"];
            var rutaPlantilla = "";

            rutaPlantilla = Path.Combine(rutaPlantilla, configuration["CarpetaTemporal"] + nom_archivo);

            HttpContext.Session.SetString("BusquedaRequerimientoEP", configuration["CarpetaTemporal"]);

            //var rutaManual = configuration["CredencialUsuario"];
            var rutaManual = configuration["CarpetaTemporal"];
            String nombrePlantilla = nom_archivo;
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            System.IO.File.Delete(rutaManual);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult GenerarDocRequerimientoEP(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = GenerarDocRequerimientoEP_Sec(formulario);


                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        private KeyValuePair<string, string> GenerarDocRequerimientoEP_Sec(IFormCollection formulario)
        {
            string cadena_final = "";
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            //CONSULTANDO DATOS GENERALES
            List<BE_REQUERIMIENTO> lista;
            BL_REQUERIMIENTO ObjLogica = new BL_REQUERIMIENTO();
            lista = ObjLogica.ListarDetalleRequerimientoEP(formulario["hiidValorReqEP"].ToString());

            //CONSULTABDO DATOS IMEI
            BL_REQUERIMIENTO_IMEI objIMEI = new BL_REQUERIMIENTO_IMEI();
            List<BE_REQUERIMIENTO_IMEI> listaResultadoMovil = new List<BE_REQUERIMIENTO_IMEI>();

            List<BE_REQUERIMIENTO_IMEI> listaResultadoImei = new List<BE_REQUERIMIENTO_IMEI>();

            listaResultadoImei = objIMEI.ConsultarImeiPendietes(formulario["hiidValorReqEP"].ToString());
            listaResultadoMovil = objIMEI.ConsultarMovilPendietes(formulario["hiidValorReqEP"].ToString());

            BeUsuarioEP listaUserEdit;
            BlUsuarioEP objParametro = new BlUsuarioEP();
            listaUserEdit = objParametro.SP_OBTENER_USUARIO_EP(HttpContext.Session.GetString("IdUsuario"));

            //Generar PDF

            var doc = new Document();
            doc.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            var output = new FileStream(configuration["DocumentosEntidadPublica"] + "FichaRequerimientoEP.pdf", FileMode.Create);
            var write = PdfWriter.GetInstance(doc, output);
            doc.Open();

            // Creamos la imagen y le ajustamos el tamaño
            iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(configuration["DocumentosEntidadPublica"] + "LogoPortal.png");
            imagen.BorderWidth = 0;
            imagen.Alignment = Element.ALIGN_RIGHT;
            float percentage = 0.0f;
            percentage = 150 / imagen.Width;
            imagen.ScalePercent(percentage * 100);

            // Insertamos la imagen en el documento
            doc.Add(imagen);

            Paragraph lineSeparatorB = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, iTextSharp.text.BaseColor.BLACK, Element.ALIGN_LEFT, 1)));
            Paragraph lineSeparatorW = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(0.0F, 100.0F, iTextSharp.text.BaseColor.WHITE, Element.ALIGN_LEFT, 1)));

            PdfPTable table = new PdfPTable(9);
            table.WidthPercentage = 100.0F;

            table.AddCell(cellFormat("SOLICITUD DE REQUERIMIENTO DE INFORMACIÓN", 9, 1));
            table.AddCell(cellFormat("Registro: " + lista[0].nroRequerimiento.ToString(), 9, 1));
            table.AddCell(cellFormat(" ", 9, 1));

            table.AddCell(cellFormat("Apellidos:", 2, 0));
            table.AddCell(cellFormat(lista[0].APELLIDOS.ToString(), 2, 0));
            table.AddCell(cellFormat(" ", 1, 0));
            /*table.AddCell(cellFormat("Región Solicitante:", 2, 0));
            table.AddCell(cellFormat(formulario["ubigeoText"].ToString(), 2, 0));*/
            table.AddCell(cellFormat("Institución Solicitante:", 2, 0));
            table.AddCell(cellFormat(lista[0].nombreInstitucion.ToString(), 2, 0));

            table.AddCell(cellFormat("Nombres:", 2, 0));
            table.AddCell(cellFormat(lista[0].NOMBRES.ToString(), 2, 0));
            table.AddCell(cellFormat(" ", 1, 0));
            table.AddCell(cellFormat("Oficina Solicitante:", 2, 0));
            table.AddCell(cellFormat(listaUserEdit.NOMBREUNIDADP.ToString(), 2, 0));

            table.AddCell(cellFormat("Tipo de Documento:", 2, 0));
            table.AddCell(cellFormat(listaUserEdit.DES_TDOC.ToString(), 2, 0));
            table.AddCell(cellFormat(" ", 1, 0));
            /*table.AddCell(cellFormat("Fecha de Registro:", 2, 0));
            table.AddCell(cellFormat(formulario["fecRegistro"].ToString(), 2, 0));*/
            table.AddCell(cellFormat("Cargo:", 2, 0));
            table.AddCell(cellFormat(listaUserEdit.CARGO.ToString(), 2, 0));

            table.AddCell(cellFormat("Numero de Documento:", 2, 0));
            table.AddCell(cellFormat(listaUserEdit.NUM_DOCIDE.ToString(), 2, 0));
            table.AddCell(cellFormat(" ", 1, 0));
            /*table.AddCell(cellFormat("Codigo de Registro:", 2, 0));
            table.AddCell(cellFormat(cod_requerimiento.ToString(), 2, 0));*/
            table.AddCell(cellFormat("Fecha de Registro:", 2, 0));
            //table.AddCell(cellFormat(lista[0].fecRegistro.ToString(), 2, 0));
            table.AddCell(cellFormat(Convert.ToDateTime(lista[0].fecRegistro).ToString("dd/MM/yyyy"), 2, 0));

            table.AddCell(cellFormat("Correo Electronico:", 2, 0));
            table.AddCell(cellFormat(listaUserEdit.CORREO_COMPL.ToString(), 6, 0));

            doc.Add(table);

            doc.Add(lineSeparatorB);
            doc.Add(lineSeparatorW);

            table = new PdfPTable(4);
            table.WidthPercentage = 100.0F;

            table.AddCell(cellFormat("Delito Investigado: ", 2, 0));
            table.AddCell(cellFormat(lista[0].DELITO.ToString(), 2, 0));
            table.AddCell(cellFormat("Motivo del Requerimiento:", 2, 0));
            table.AddCell(cellFormat(lista[0].MOTIVO.ToString(), 2, 0));
            table.AddCell(cellFormat("N° Denuncia/ Caso/ Carpeta/ Expediente:", 2, 0));
            table.AddCell(cellFormat(lista[0].EXPED.ToString(), 2, 0));

            doc.Add(table);

            doc.Add(lineSeparatorB);
            doc.Add(lineSeparatorW);

            table = new PdfPTable(12);
            table.WidthPercentage = 100.0F;
            table.AddCell(cellFormat("Se ha registrado el siguiente requerimiento de información:  ", 12, 0));
            table.AddCell(cellFormat(" ", 12, 0));
            table.AddCell(cellFormat(" ", 12, 0));
            doc.Add(table);


            table = new PdfPTable(12);

            table.WidthPercentage = 100.0F;
            /*table.AddCell(cellFormat("Se ha registrado el siguiente requerimiento de información:  ", 12, 0));
            table.AddCell(cellFormat(" ", 12, 0));
            table.AddCell(cellFormat(" ", 12, 0));*/
            table.SetWidths(new float[] { 2f, 4f, 3f, 5f, 6f, 4f, 6f, 6f, 4f, 6f, 4f, 4f });
            table.AddCell(cellCabecera("Nº", 1));
            table.AddCell(cellCabecera("Tipo de Dato", 2));
            table.AddCell(cellCabecera("Dato", 2));
            table.AddCell(cellCabecera("Revisión Preliminar", 2));
            //table.AddCell(cellCabecera("¿Procede Cargar?", 2));
            table.AddCell(cellCabecera("Sustento Preliminar", 5));
            //table.GetHeader();
            table.HeaderRows = 1;
            //table.SkipFirstHeader = true;
            foreach (var item in listaResultadoImei)
            {
                BE_SUSTENTO sust = new BE_SUSTENTO();
                sust.dato = item.nroImei.ToString();
                table.AddCell(cellDatos(item.nroItem.ToString(), 1));
                table.AddCell(cellDatos("IMEI FISICO", 2));
                table.AddCell(cellDatos(item.nroImei.ToString(), 2));
                if (Decimal.TryParse(item.nroImei.ToString(), out Decimal valor) && item.nroImei.ToString().Contains(" ") != true)
                {
                    if (item.nroImei.ToString().Length == 15)
                    {
                        if (item.estadoLuhn.Trim() == "Lunh valido" || item.estadoLuhn.Trim() == "Lunh válido")
                        {
                            if (item.estadoIMEI.Trim() == "Sustraído" || item.estadoIMEI.Trim() == "Perdido")
                            {
                                sust.revi = "Correcto";
                                table.AddCell(cellDatos("Correcto", 2));
                                
                                sust.proce = "Sí Procede";
                               
                            }
                            else if (item.estadoIMEI.Trim() == "Recuperado")
                            {
                                sust.revi = "IMEI reportado como RECUPERADO.";
                                table.AddCell(cellDatos("IMEI reportado como RECUPERADO.", 2));
                                sust.proce = "No Procede";
                                
                            }
                            
                            else
                            {
                                if (item.estadoIMEI.Trim() == "Bloqueado Inválido")
                                {
                                    //sust.revi = "IMEI reportado como CAMPAÑA BLOQUEO INVÁLIDO";
                                    sust.revi = "Bloqueado Invalido";
                                    table.AddCell(cellDatos("IMEI reportado como CAMPAÑA BLOQUEO INVÁLIDO", 2));
                                    sust.proce = "No Procede";
                                }
                                else
                                {
                                    sust.revi = "IMEI no reportado como SUSTRAÍDO o PERDIDO.";
                                    table.AddCell(cellDatos("IMEI no reportado como SUSTRAÍDO o PERDIDO.", 2));
                                    sust.proce = "No Procede";
                                }
        

                            }
                        }
                        else
                        {
                            sust.revi = "IMEI no cumple algoritmo de verificación.";
                            table.AddCell(cellDatos("IMEI no cumple algoritmo de verificación.", 2));
                            sust.proce = "No Procede";
                           
                        }
                    }
                    else
                    {
                        sust.revi = "No tiene 15 dígitos.";
                        table.AddCell(cellDatos("No tiene 15 dígitos.", 2));
                        sust.proce = "No Procede";
                        
                    }
                }
                else
                {
                    sust.revi = "Tiene caracteres no numéricos o espacios.";
                    table.AddCell(cellDatos("Tiene caracteres no numéricos o espacios.", 2));
                    sust.proce = "No Procede";
                   
                }
                
                //table.AddCell(cellDatos(item.proce.ToString(), 2));
                table.AddCell(cellDatos(validacionSustentoImei(sust), 5));
            }


            doc.Add(table);

            doc.Add(lineSeparatorW);
            doc.Add(lineSeparatorW);
            //doc.Add(lineSeparatorW);

            table = new PdfPTable(12);

            table.WidthPercentage = 100.0F;
            /*table.AddCell(cellFormat("Se ha registrado el siguiente requerimiento de información:  ", 12, 0));
            table.AddCell(cellFormat(" ", 12, 0));
            table.AddCell(cellFormat(" ", 12, 0));*/
            table.SetWidths(new float[] { 2f, 4f, 3f, 5f, 6f, 4f, 6f, 6f, 4f, 6f, 4f, 4f });
            table.AddCell(cellCabecera("Nº", 1));
            table.AddCell(cellCabecera("Tipo de Dato", 2));
            table.AddCell(cellCabecera("Dato", 2));
            table.AddCell(cellCabecera("Revisión Preliminar", 2));
            //table.AddCell(cellCabecera("¿Procede Cargar?", 2));
            table.AddCell(cellCabecera("Sustento Preliminar", 5));
            //table.GetHeader();
            table.HeaderRows = 1;


            foreach (var item in listaResultadoMovil)
            {
                BE_SUSTENTO sut = new BE_SUSTENTO();
                table.AddCell(cellDatos(item.nroItem.ToString(), 1));
                table.AddCell(cellDatos("SERVICIO MOVIL", 2));
                table.AddCell(cellDatos(item.nroImei, 2));
                sut.dato = item.nroImei;
                if (int.TryParse(item.nroImei.ToString(), out int valor) && item.nroImei.ToString().Contains(" ") != true)
                {
                    if (item.nroImei.ToString().Length == 9)
                    {
                        if (item.validacionFormato.Trim() == "")
                        {
                            
                            table.AddCell(cellDatos("Correcto", 2));
                            sut.revi = "Correcto";
                            sut.proce = "Sí procede";
                            
                        }
                        else
                        {
                            sut.revi = "No se encuentra en el Registro de Abonados";
                            table.AddCell(cellDatos("No se encuentra en el Registro de Abonados", 2));
                            sut.proce = "No Procede";
                            
                        }
                    }
                    else
                    {
                        
                        sut.revi = "No tiene 9 dígitos.";
                        table.AddCell(cellDatos("No tiene 9 dígitos.", 2));
                        sut.proce = "No Procede";
                        
                    }
                }
                else
                {
                    
                    sut.revi = "Tiene caracteres no numéricos o espacios.";
                    table.AddCell(cellDatos("Tiene caracteres no numéricos o espacios.", 2));
                    sut.proce = "No Procede";
                }
                
                table.AddCell(cellDatos(validacionSustentoMovil(sut), 5));
            }

            doc.Add(table);

            write.CloseStream = false;
            doc.Close();
            output.Dispose();

            /*listaResultadoMovil = objIMEI.ConsultarMovilPendietes(formulario["hiidValorReqEP"].ToString());
            string cadena_html_user = "";
            if (listaResultadoMovil.Count > 0)
            {
                cadena_html_user += "<h4>DETALLE MOVIL</h4><br>";
                cadena_html_user += "<div class='tabla-content-result'>";
                cadena_html_user += "<table id='dtResultadCarMasMovilEP' border='1' class='tabla-osiptel' style='width: 100%'>";
                cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_html_user += " <thead>";
                cadena_html_user += "<tr>";
                cadena_html_user += "<th scope='col' style=' background-color:#9c9c9c;text-align:center;'><strong>Nº</strong></th>";
                cadena_html_user += "<th scope='col' style=' background-color:#9c9c9c;text-align:center;'><strong>Dato</strong></th>";
                cadena_html_user += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Revisión Preliminar</strong></th>";
                cadena_html_user += "<th scope='col' style=' background-color:#9c9c9c;text-align:center;'><strong>¿Procede a Cargar?</strong></th>";
                cadena_html_user += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Sustento</strong></th>";
                cadena_html_user += "</tr>";
                cadena_html_user += " </thead>";
                cadena_html_user += "<tbody>";
                for (int i = 0; i < listaResultadoMovil.Count; i++)
                {
                    cadena_html_user += " <tr>";
                    cadena_html_user += "<td> " + listaResultadoMovil[i].nroItem.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaResultadoMovil[i].nroImei.ToString() + "</td>";
                    string des_pre = "Incorrecto";
                    string proc_cargar = "No procede";
                    if (listaResultadoMovil[i].validacionFormato.Trim() == "")
                    {
                        des_pre = "Correcto";
                        proc_cargar = "Si";
                    }
                    cadena_html_user += "<td> " + des_pre.ToString() + "</td>";

                    cadena_html_user += "<td> " + proc_cargar.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaResultadoMovil[i].validacionFormato.ToString() + " - " + listaResultadoMovil[i].estadoLuhn + "</td>";
                    cadena_html_user += "</tr>";



                }

                cadena_html_user += " </tbody>";
                cadena_html_user += "   </table>";
                cadena_html_user += "</div>";

            }


            //
            //CONSULTANDO SI EXISTEN REGISTROS DE TELEFONO
            //BL_REQUERIMIENTO_IMEI objIMEI = new BL_REQUERIMIENTO_IMEI();
            List<BE_REQUERIMIENTO_IMEI> listaResultadoImei = new List<BE_REQUERIMIENTO_IMEI>();

            listaResultadoImei = objIMEI.ConsultarImeiPendietes(formulario["hiidValorReqEP"].ToString());
            string cadena_imei = "";
            if (listaResultadoImei.Count > 0)
            {
                cadena_imei += "<h4>DETALLE IMEI</h4><br>";
                cadena_imei += "<div class='tabla-content-result'>";
                cadena_imei += "<table id='dtResultadCarMasImeiEP' border='1' class='tabla-osiptel' style='width: 100%;'>";
                cadena_imei += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_imei += " <thead>";
                cadena_imei += "<tr>";
                cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Nº</strong></th>";
                cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Dato</strong></th>";
                cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Revisión Preliminar</strong></th>";
                cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>¿Procede a Cargar?</strong></th>";
                cadena_imei += "<th scope='col' style=' background-color: #9c9c9c;text-align:center;'><strong>Sustento</strong></th>";
                cadena_imei += "</tr>";
                cadena_imei += " </thead>";
                cadena_imei += "<tbody>";
                for (int i = 0; i < listaResultadoImei.Count; i++)
                {
                    cadena_imei += " <tr>";
                    cadena_imei += "<td> " + listaResultadoImei[i].nroItem.ToString() + "</td>";
                    cadena_imei += "<td> " + listaResultadoImei[i].nroImei.ToString() + "</td>";
                    string des_pre = "Incorrecto";
                    string proc_cargar = "No procede";
                    if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].estadoLuhn.Trim() == "Lunh válido")
                    //if (listaResultadoImei[i].validacionFormato.Trim() == "" && listaResultadoImei[i].ultimoDigito != 0)
                    {
                        des_pre = "Correcto";
                        proc_cargar = "Si";
                    }
                    cadena_imei += "<td> " + des_pre.ToString() + "</td>";

                    cadena_imei += "<td> " + proc_cargar.ToString() + "</td>";
                    cadena_imei += "<td> " + listaResultadoImei[i].validacionFormato.ToString() + " - " + listaResultadoImei[i].estadoLuhn + "</td>";
                    cadena_imei += "</tr>";



                }

                cadena_imei += " </tbody>";
                cadena_imei += "   </table>";
                cadena_imei += "</div>";
            }
            //
            //CONCATENADO CADENAS
            int cant = Convert.ToInt16(lista[0].nroImei) + Convert.ToInt16(lista[0].nroImeiDos);
            cadena_final += "<br><h4><strong>Sustento Requerimiento EP</strong></h4><br><div class='row'>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<label class='etiqueta'>";
            cadena_final += " <strong>Nº de Requerimiento : <strong>"+ lista[0].nroRequerimiento;
            cadena_final += "</label>";
            cadena_final += "  </div>";
           
            cadena_final += "</div>";
            cadena_final += "<div class='row'>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<label class='etiqueta'>";
            cadena_final += " <strong>Usuario Solicitante : </strong>" + lista[0].nombreSolicitante;
            cadena_final += "</label>";
            cadena_final += "  </div>";
          
            cadena_final += "</div>";
            cadena_final += "<div class='row'>";
            cadena_final += " <div class='col-md-3'>";
            cadena_final += "<label class='etiqueta'>";
            cadena_final += "<strong> Total de Data Procesada : </strong> "+ cant;
            cadena_final += "</label>";
            cadena_final += "  </div>";
           
            cadena_final += "</div>";
            cadena_final += "<br><br>";
            //
            cadena_final += cadena_html_user;
            cadena_final += cadena_imei;

            
            Document doc = new Document();
            PdfWriter.GetInstance(doc, new FileStream("ArchivoExcelUser/SustentoReqEP.pdf", FileMode.Create));
            doc.Open();
            Paragraph title = new Paragraph();
            // Creamos la imagen y le ajustamos el tamaño
            iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("ArchivoExcelUser/LogoPortal.png");
            imagen.BorderWidth = 0;
            imagen.Alignment = Element.ALIGN_RIGHT;
            float percentage = 0.0f;
            percentage = 150 / imagen.Width;
            imagen.ScalePercent(percentage * 100);

            // Insertamos la imagen en el documento
            doc.Add(imagen);
            //title.Font = FontFactory.GetFont(FontFactory.TIMES, 18f, BaseColor.BLUE);

            //title.Add("Sustento de Requerimiento EP!");
            //doc.Add(title);
            //doc.Add(new Paragraph(""));
            List<IElement> htmlarraylist = HTMLWorker.ParseToList(new StringReader(cadena_final), null);
            //add the collection to the document
            for (int k = 0; k < htmlarraylist.Count; k++)
            {
                doc.Add((IElement)htmlarraylist[k]);
            }

            doc.Close();*/

            keyValueError = new KeyValuePair<string, string>("1", "Documento Generado");



            return keyValueError;
        }

        public FileResult DescargarDocSustentoPdf()
        {

            //var rutaManual = configuration["CredencialUsuario"];
            var rutaManual = configuration["DocumentosEntidadPublica"];
            //String nombrePlantilla = configuration["DocumentosEntidadPublica"] + "FichaRequerimientoEP.pdf";
            String nombrePlantilla =  "FichaRequerimientoEP.pdf";
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public PdfPCell cellFormat(string dato, int colspan, int align)
        {
            PdfPCell cell = new PdfPCell(new Phrase(dato));
            cell.Colspan = colspan;
            cell.HorizontalAlignment = align;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = 0;
            return cell;
        }

        public PdfPCell cellCabecera(string dato, int colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(dato));
            cell.Colspan = colspan;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
            return cell;
        }
        public PdfPCell cellCabeceraNum(string dato, int colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(dato));
            cell.Colspan = colspan;
            cell.Width = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
            return cell;
        }
        

        public PdfPCell cellDatos(string dato, int colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(dato));
            cell.Colspan = colspan;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
            return cell;
        }
        public PdfPCell cellDatosNum(string dato, int colspan)
        {
            PdfPCell cell = new PdfPCell(new Phrase(dato));
            cell.Colspan = colspan;
            cell.Width = 5f;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = Rectangle.BOTTOM_BORDER | Rectangle.TOP_BORDER;
            return cell;
        }
        
        public String validacionSustentoImei(BE_SUSTENTO item)
        {
            String msg = "";
            if (IsNumeric(item.dato) && item.dato.Contains(" ") != true)
            {
                if (item.dato.Length == 15)
                {
                    if (item.revi == "Correcto" && item.proce == "Sí Procede")
                    {
                        msg = "-";
                    }
                    else if (item.revi.ToString() == "IMEI no cumple algoritmo de verificación.")
                    {
                        msg = "El IMEI consultado no se ajusta a los estándares de la GSMA (Global System for Mobile Communications Association), esto debido a que los primeros catorce (14) dígitos del referido número de IMEI no guardan relación con el decimoquinto dígito del mismo (dígito que verifica la consistencia de los primeros catorce).";
                    }
                    else if (item.revi.ToString() == "IMEI no reportado como SUSTRAÍDO o PERDIDO.")
                    {
                        msg = "El IMEI consultado no se encuentra reportado como sustraído o perdido ante ninguna empresa operadora. Así, dada la facilidad que existe en el mercado para intercambiar el uso del equipo en diferentes servicios móviles, no resultaría posible identificar apropiadamente a los titulares de los equipos terminales móviles no reportados como robados o perdidos, en tanto que dicha información se encuentra relacionada con el servicio de telefonía móvil.";
                    }
                    else if (item.revi.ToString() == "IMEI reportado como RECUPERADO.")
                    {
                        msg = "El IMEI consultado figura como recuperado, por lo que hacemos de su conocimiento que un equipo se reporta como “recuperado” cuando el abonado ubica o toma posesión nuevamente de un equipo que había sido reportado como “sustraído” o “perdido”. De esta manera, este hecho es consignado ante la empresa operadora que le brinda el servicio con la finalidad de que se elimine el bloqueo por robo o pérdida, habilitando su uso en las redes de telecomunicaciones.";
                    }
                    //else if (item.revi.ToString() == "IMEI reportado como CAMPAÑA BLOQUEO INVÁLIDO")
                    else 
                    {
                        msg = "El IMEI consultado sería un IMEI inválido, debido a que su numeración no permite identificar al equipo terminal móvil en base a los estándares de la GSMA (Global System for Mobile Communications Association), por lo que se puede presumir que se trataría de un IMEI lógico, el cual sería factible de ser alterado.";
                    }
                }
                else
                {
                    msg = "El número de dígitos del código IMEI consultado es diferente a quince (15), con lo cual no resulta posible verificar si el equipo terminal en cuestión fue reportado como robado o perdido ante alguna de las empresas operadoras que operan a nivel nacional.";
                }
            }
            else
            {
                msg = "El código consultado contiene letras, espacios u otros caracteres no numéricos. Cabe precisar que el IMEI es un código que identifica al equipo móvil de forma exclusiva a nivel mundial y cuenta con quince (15) dígitos.";
            }

            return msg;
        }

        public String validacionSustentoMovil(BE_SUSTENTO item)
        {
            String msg = "";

            if (IsNumeric(item.dato) && item.dato.Contains(" ") != true)
            {
                if (item.dato.Length == 9)
                {
                    if (item.revi == "Correcto")
                    {
                        msg = "-";
                    }
                    else
                    {
                        msg = "El número de servicio móvil consultado no se encuentra en el Registro de Abonados del RENTESEG. Ninguna empresa operadora del servicio público móvil ha reportado información al RENTESEG respecto del servicio consultado.";
                    }
                }
                else
                {
                    msg = "El número de dígitos del servicio consultado es diferente a nueve (9). Cabe precisar que un número de servicio público móvil cuenta con nueve (9) dígitos e inicia con 9.";
                }
            }
            else
            {
                msg = "El código consultado contiene letras, espacios u otros caracteres no numéricos.Cabe precisar que un número del servicio público móvil cuenta con nueve(9) dígitos.";
            }

            return msg;
        }

        public String dataTabla()
        {
            return "Ok";
        }
    }
}