﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using reCAPTCHA.AspNetCore;
using System;
using System.Collections.Generic;

using System.Net;
using System.Net.Security;
using System.Net.Http;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;

namespace ClienteSIGREI.Controllers
{
    public class LoginEPController : Controller
    {
        // GET: Login
        private readonly IConfiguration configuration;


        public LoginEPController(IConfiguration config)
        {
            this.configuration = config;
        }

        protected static string ReCaptcha_Key = "6LfJPwATAAAAAEneZUzSrBmBStil1YRMsRL3Akum";
        protected static string ReCaptcha_Secret = "6LfJPwATAAAAAFmb19YL_SQxvyhGTAgYD9c-p7N5";

        public ActionResult Index()
        {
            var cadena = HttpContext.Session.GetString("NumDocIdeEP");
            //HttpContext.Session.SetString("NumDocIdeEP", "");
            //HttpContext.Session.SetString("CodDocIdeEP", "");
            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOIDENTIDAD, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOIDENTIDAD);
            ViewBag.ddlTipoDocumento = listaTipoDocumento;
            HttpContext.Session = null;

            return View();
        }

        public RecaptchaResponse ValidateCaptcha(string response)
        {
            //string secret = System.Web.Configuration.WebConfigurationManager.AppSettings["6LfJPwATAAAAAFmb19YL_SQxvyhGTAgYD9c-p7N5"];
            RecaptchaResponse resultado = new RecaptchaResponse();
            string secret = "6LfJPwATAAAAAFmb19YL_SQxvyhGTAgYD9c-p7N5";

            var proxyTres = new WebProxy("srvproxy01.osiptel.gob.pe", 3128)
            {
                UseDefaultCredentials = false,
                Credentials = CredentialCache.DefaultCredentials
            };

            var client = new WebClient();

            var usarProxy = configuration["usarProxy"];
            var jsonResult = "";
            if (usarProxy.Equals("1"))
            {
                client.Proxy = proxyTres;
            }
            try
            {
                jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
                resultado = JsonConvert.DeserializeObject<RecaptchaResponse>(jsonResult.ToString());
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidateCaptcha");
                resultado.success = false;
            }

            client.Dispose();
            return resultado;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ValidarUsuario(IFormCollection collection)
        {
            RecaptchaResponse response = ValidateCaptcha(Request.Form["g-recaptcha-response"]);
            var captcha = response.success;
            //if (true)
            if (captcha)
            {
                try
            {
                
                String resultado = validarUsuarioBD(collection);
                String metodo = "", Controlador = "";
                if (resultado == "0")
                {
                    metodo = "Index";
                    Controlador = "Principal";
                   
                }
                else if (resultado == "2")
                {
                    metodo = "GestionContrasenia";
                    Controlador = "GestorContrasenia";
                }
                else
                {
                    TempData["Mensaje"] = resultado;

                    metodo = "Index";
                    Controlador = "LoginEp";
                }
                return RedirectToAction(metodo, Controlador);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarUsuario");
                TempData["Mensaje"] = ex.Message.ToString();
                return RedirectToAction(nameof(Index));
            }
            }
            else
            {
                ViewBag.if_error = true;
                ViewBag.msg_error = "Captcha no válido, vuelva a ingresar";
                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOIDENTIDAD, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOIDENTIDAD);
                ViewBag.ddlTipoDocumento = listaTipoDocumento;
                return View("Index");
            }
        }

        protected string validarUsuarioBD(IFormCollection formulario)
        {
            String TipUsuario = formulario["ddlTipoDocumento"].ToString().Trim();
            String strUsuario = formulario["txtUsuario"].ToString().Trim();
            String strContrasenia = formulario["txtClave"].ToString().Trim();
            
            
            String lsIDUsuario = String.Empty;

            UsuarioOEIBL objBLUsuario = new UsuarioOEIBL();

            BeUsuarioEP objBEUsuario = new BeUsuarioEP();
            Constantes cont = new Constantes();
            Encriptador encry = new Encriptador();

            objBEUsuario.COD_DOCIDE = TipUsuario;
            objBEUsuario.IDUSUARIO = strUsuario;

            
            var claveSha = encry.EncryptStringToBytes_Aes(strContrasenia, cont.Key256, cont.IVAES256);
            objBEUsuario.CONTRASENIA = claveSha;
            lsIDUsuario = objBLUsuario.SP_VALIDAR_USUARIO_IE(objBEUsuario).ToString();
            if (lsIDUsuario == "0")
            {
                return "Usuario o contraseña incorrecta.";
            }
            objBEUsuario = objBLUsuario.SP_OBTENER_USUARIO_IE(objBEUsuario);
            if (objBEUsuario.IDUSUARIO == null)
            {
                return "Usuario Vencido o Contraseña no actualizada";
            }

            if (objBEUsuario.INDCAMBIOCNIA == 2)
            {
                
                return "2";
            }
            try
            {

                var idUsuario = Convert.ToString(objBEUsuario.IDUSUARIO);
                HttpContext.Session.SetString("IdUsuario", idUsuario);
                HttpContext.Session.SetString("CodDocIdeEP", Convert.ToString(objBEUsuario.COD_DOCIDE));
                HttpContext.Session.SetString("NumDocIdeEP", Convert.ToString(objBEUsuario.NUM_DOCIDE));
                HttpContext.Session.SetString("ContraWS", formulario["txtClave"].ToString().Trim());

                var claims = new List<Claim>
                {
                   new Claim(ClaimTypes.Name, "UsuarioEP"+Convert.ToString(objBEUsuario.NUM_DOCIDE)),
                   new Claim("UsuarioEP"+Convert.ToString(objBEUsuario.NUM_DOCIDE), Convert.ToString(objBEUsuario.NUM_DOCIDE)),
                    new Claim(ClaimTypes.Email, idUsuario)
                };

                var userIdentity = new ClaimsIdentity(claims, "login");

                ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                HttpContext.SignInAsync(principal,
                    new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(20)
                    });
                /*var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, "UsuarioEP"+Convert.ToString(objBEUsuario.COD_DOCIDE)),
                    new Claim(ClaimTypes.Name, idUsuario)
                };

                // create identity
                ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

                // create principal
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);


                HttpContext.SignInAsync(
                    scheme: "FiverSecurityCookie",
                    principal: principal);*/

                //var cook = HttpContext.Request.Cookies["0"];
                //generando nueva cookie
                //FormsAuthentication.SetAuthCookie(UserName, IsPersistent)
                //FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                //User,
                //DateTime.Now,
                //DateTime.Now.AddMinutes(30),
                //IsPersistent,
                //Convert.ToString(U.cPerCodigo),
                //FormsAuthentication.FormsCookiePath
                //);

                string caracter = "";
                if (strUsuario.Length > 2)
                {
                    caracter = strUsuario.Substring(0, 3).ToLower();
                }
                if (caracter == "eo_")
                {
                    HttpContext.Session.SetString("DesUsuario", Convert.ToString(objBEUsuario.IDUSUARIO));
                }
                else
                {
                    HttpContext.Session.SetString("DesUsuario", Convert.ToString(objBEUsuario.NUM_DOCIDE));
                }
                    

                HttpContext.Session.SetString("PerfilUsuario", Convert.ToString(objBEUsuario.IDPERFIL));
                HttpContext.Session.SetString("DescripcionPerfil", Convert.ToString(objBEUsuario.DES_PERFIL));

                return "0";
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "validarUsuarioBD");
                return "Los datos ingresados son incorrectos.";
            }
        }
    }
}