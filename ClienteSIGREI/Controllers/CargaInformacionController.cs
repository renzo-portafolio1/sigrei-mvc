﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;
using System.IO;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class CargaInformacionController : Controller
    {

        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment hosting;
        public CargaInformacionController(IConfiguration config, IHostingEnvironment host)
        {
            this.configuration = config;
            this.hosting = host;
        }

        public IActionResult Index()
        {
            List<EstadoBE> listaEstado = new List<EstadoBE>();
            EstadoBE objEstado = new EstadoBE();
            objEstado.IDESTADO = "1";
            objEstado.ESTADO = "Registrado";
            listaEstado.Add(objEstado);

            objEstado = new EstadoBE();
            objEstado.IDESTADO = "4";
            objEstado.ESTADO = "Atendido";
            listaEstado.Add(objEstado);

            ViewBag.Lista_Estado = listaEstado;

            return View();
        }

        public ActionResult RecargarPaginaCarga()
        {

            return RedirectToAction("Index", "SistemaDocumentario");
        }

        [HttpPost]
        public ActionResult CargaInicial(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_EO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO_EO>();
            BE_REQUERIMIENTO_EO objParametro = obtenerCriterios(formulario);
            BL_REQUERIMIENTO_EO objLogica = new BL_REQUERIMIENTO_EO();

            objParametro.idEmpresaEO = Int64.Parse(HttpContext.Session.GetString("cod_Empresa"));
            listaBE_REQUERIMIENTO = objLogica.ListaSolicitudEO(objParametro);
            return PartialView("_DetalleCargaInformacion", listaBE_REQUERIMIENTO);
        }

        [HttpPost]
        public ActionResult BucarSolicitudes(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_EO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO_EO>();
            BE_REQUERIMIENTO_EO objParametro = obtenerCriterios(formulario);
            BL_REQUERIMIENTO_EO objLogica = new BL_REQUERIMIENTO_EO();

            objParametro.idEmpresaEO = Int64.Parse(HttpContext.Session.GetString("cod_Empresa"));
            listaBE_REQUERIMIENTO = objLogica.ListaSolicitudEO(objParametro);
            return PartialView("_DetalleCargaInformacion", listaBE_REQUERIMIENTO);
        }

        [HttpPost]
        public ActionResult LimpiarCriteriosBandeja(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_EO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO_EO>();

            return PartialView("_DetalleCargaInformacion", listaBE_REQUERIMIENTO);
        }


        [HttpPost]
        public ActionResult AtenderRequerimientoEO(IFormCollection formulario)
        {
            var ID = formulario["ihrequerimientoeo"];

            BE_REQUERIMIENTO_EO objBE_REQUERIMIENTOEO = new BE_REQUERIMIENTO_EO();
            BL_REQUERIMIENTO_EO objLogicaRequerimientoEO = new BL_REQUERIMIENTO_EO();
            Int64 idRetorno = 0;
            try
            {
                objBE_REQUERIMIENTOEO.idRequerimientoEO = Int64.Parse(ID);
                objBE_REQUERIMIENTOEO.usuMod = HttpContext.Session.GetString("IdUsuario");

                idRetorno = objLogicaRequerimientoEO.AtenderEO(objBE_REQUERIMIENTOEO);

                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                if (idRetorno > 0)
                {
                    result = new KeyValuePair<string, string>("1", "¡Se atendió correctamente el requerimiento de información!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al intentar atender el requerimiento de información");
                }
                return Json(result);
            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al intentar atender el requerimiento de información");
                return Json(result);
            }
        }


        [HttpPost]
        public ActionResult ConsultarIMEIPendientes(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            var ID = formulario["ihrequerimientoeo"];

            BE_REQUERIMIENTO_EO objBE_REQUERIMIENTOEO = new BE_REQUERIMIENTO_EO();
            BL_REQUERIMIENTO_EO objLogica = new BL_REQUERIMIENTO_EO();
            try
            {
                objBE_REQUERIMIENTOEO.idRequerimientoEO = Int64.Parse(ID);

                objBE_REQUERIMIENTOEO = objLogica.ConsultaSolicitudEO(objBE_REQUERIMIENTOEO);


                if (objBE_REQUERIMIENTOEO.cantidadPorAtender > UTConstantes.VALOR_CERO)
                {
                    result = new KeyValuePair<string, string>("2", "¡No se puede atender el Requerimiento porque tiene IMEIs por atender!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("1", "¡Se prosigue a atender!");
                }
                return Json(result);
            }
            catch (Exception)
            {
                result = new KeyValuePair<string, string>("0", "Error al intentar atender la solicitud de la empresa operadora");

                return Json(result);
            }
        }


        public FileResult DescargarDocumentoIMEI(IFormCollection formulario)
        {
            BL_REQUERIMIENTO_EO objLogica = new BL_REQUERIMIENTO_EO();
            BE_REQUERIMIENTO_IMEI_EO objBE_REQUERIMIENTO_IMEI = new BE_REQUERIMIENTO_IMEI_EO();

            objBE_REQUERIMIENTO_IMEI.idRequerimientoEO = Int64.Parse(formulario["ihrequerimientoeoDes"].ToString());

            byte[] FileExcel = null;
            ExportExcel objExportExcel = new ExportExcel();
            FileExcel = objExportExcel.generarIMEI_EO(objLogica.ListaIMEI_EO(objBE_REQUERIMIENTO_IMEI), "IMEI");

            return File(FileExcel, System.Net.Mime.MediaTypeNames.Application.Octet, formulario["ihrequerimientoeoDes"].ToString() + "_IMEI_EO.xlsx");
        }



        [HttpPost]
        public ActionResult ConsultaRequerimientoSolicitud(IFormCollection formulario)
        {
            BE_REQUERIMIENTO_EO objRequerimientoEO = new BE_REQUERIMIENTO_EO();
            BE_REQUERIMIENTO_IMEI_EO objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI_EO();

            try
            {
                BL_REQUERIMIENTO_EO objLogicaRequerimiento = new BL_REQUERIMIENTO_EO();


                Int64 idRequerimientoEO = Int64.Parse(formulario["ihrequerimientoeo"].ToString());
                objRequerimientoEO.idRequerimientoEO = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoEO = objLogicaRequerimiento.ConsultaSolicitudEO(objRequerimientoEO);

                objRequerimientoImeiEo.idRequerimientoEO = Convert.ToInt64(idRequerimientoEO);

                objRequerimientoEO.listaRequerimientoImeiEO = objLogicaRequerimiento.ListaIMEI_EO(objRequerimientoImeiEo);

            }
            catch (Exception)
            {
                objRequerimientoEO = new BE_REQUERIMIENTO_EO();
                objRequerimientoEO.listaRequerimientoImeiEO = new List<BE_REQUERIMIENTO_IMEI_EO>();

            }
            return PartialView("_PartialConsultaSolicitud", objRequerimientoEO);
        }


        [HttpPost]
        public ActionResult AbrirCargaInfoSolicitud(IFormCollection formulario)
        {
            BE_REQUERIMIENTO_EO objRequerimientoEO = new BE_REQUERIMIENTO_EO();
            BE_REQUERIMIENTO_IMEI_EO objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI_EO();

            try
            {
                BL_REQUERIMIENTO_EO objLogicaRequerimiento = new BL_REQUERIMIENTO_EO();


                Int64 idRequerimientoEO = Int64.Parse(formulario["ihrequerimientoeo"].ToString());
                objRequerimientoEO.idRequerimientoEO = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoEO = objLogicaRequerimiento.ConsultaSolicitudEO(objRequerimientoEO);


                var irTab = HttpContext.Session.GetString("IrTabCargarInformacion");

                if (irTab == null)
                {
                    irTab = "0";
                }
                else if (irTab == "1")
                {
                    HttpContext.Session.SetString("IrTabCargarInformacion", "0");
                }
                else
                {
                }

                ViewBag.IrTabPorAtender = irTab;

            }
            catch (Exception)
            {
                objRequerimientoEO = new BE_REQUERIMIENTO_EO();
                objRequerimientoEO.listaRequerimientoImeiEO = new List<BE_REQUERIMIENTO_IMEI_EO>();

            }
            return PartialView("_PartialCargaInformacion", objRequerimientoEO);
        }

        [HttpPost]
        public ActionResult verTabsAtendidas(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_IMEI_EO> listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();

            var listaResultadoJSON = JsonConvert.SerializeObject(listaResultado);
            HttpContext.Session.SetString("ListaImeiPorAtender", listaResultadoJSON);
            try
            {
                BL_REQUERIMIENTO_EO objLogicaRequerimiento = new BL_REQUERIMIENTO_EO();
                BE_REQUERIMIENTO_IMEI_EO objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI_EO();
                Int64 idRequerimientoEO = Int64.Parse(formulario["ihrequerimientoeo"].ToString());
                objRequerimientoImeiEo.idRequerimientoEO = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoImeiEo.idEstado = UTConstantes.ID_ESTADO_ATENDIDO;
                listaResultado = objLogicaRequerimiento.ListaIMEI_EO(objRequerimientoImeiEo);
            }
            catch (Exception)
            {
                listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
            }
            return PartialView("_PartialImeiAtendidas", listaResultado);
        }

        [HttpPost]
        public ActionResult verTabsPorAtender(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_IMEI_EO> listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
            try
            {
                BL_REQUERIMIENTO_EO objLogicaRequerimiento = new BL_REQUERIMIENTO_EO();
                BE_REQUERIMIENTO_IMEI_EO objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI_EO();
                Int64 idRequerimientoEO = Int64.Parse(formulario["ihrequerimientoeo"].ToString());
                objRequerimientoImeiEo.idRequerimientoEO = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoImeiEo.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;

                listaResultado = JsonConvert.DeserializeObject<List<BE_REQUERIMIENTO_IMEI_EO>>(HttpContext.Session.GetString("ListaImeiPorAtender"));

                if (listaResultado == null)
                {
                    listaResultado = objLogicaRequerimiento.ListaIMEI_EO(objRequerimientoImeiEo);
                    ViewBag.Total_Actualizar = 0;
                }
                else if (listaResultado.Count == UTConstantes.VALOR_CERO)
                {
                    ViewBag.Total_Actualizar = 0;
                    listaResultado = objLogicaRequerimiento.ListaIMEI_EO(objRequerimientoImeiEo);
                }
                else
                {
                    ViewBag.Total_Actualizar = listaResultado.Count;
                }


                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOOBSERVACION, UTConstantes.APLICACION_COLUMNA.IDTIPOOBSERVACION);
                ViewBag.Lista_Motivo = lista;
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "verTabsPorAtender");
                listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
            }
            return PartialView("_PartialImeiPorAtender", listaResultado);
        }

        [HttpPost]
        public ActionResult verTabsPorAtenderNew(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_IMEI_EO> listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
            try
            {
                BL_REQUERIMIENTO_EO objLogicaRequerimiento = new BL_REQUERIMIENTO_EO();
                BE_REQUERIMIENTO_IMEI_EO objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI_EO();
                Int64 idRequerimientoEO = Int64.Parse(formulario["ihrequerimientoeo"].ToString());
                objRequerimientoImeiEo.idRequerimientoEO = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoImeiEo.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;

                listaResultado = objLogicaRequerimiento.ListaIMEI_EO(objRequerimientoImeiEo);
                ViewBag.Total_Actualizar = 0;
                List<BE_REQUERIMIENTO_IMEI_EO> listaTemporal = new List<BE_REQUERIMIENTO_IMEI_EO>();
                var listaTemporalJSON = JsonConvert.SerializeObject(listaTemporal);
                HttpContext.Session.SetString("ListaImeiPorAtender", listaTemporalJSON);

                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOOBSERVACION, UTConstantes.APLICACION_COLUMNA.IDTIPOOBSERVACION);

                ViewBag.Lista_Motivo = lista;
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "verTabsPorAtenderNew");
                listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
            }
            return PartialView("_PartialImeiPorAtender", listaResultado);
        }

        [HttpPost]
        public ActionResult CargarInformacionExcel(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacion(formulario);

                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionExcel");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");
                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult ActualizarInformacionImei(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                BL_REQUERIMIENTO_IMEI_EO objLogica = new BL_REQUERIMIENTO_IMEI_EO();
                Int64 idResultado = 0;
                try
                {
                    List<BE_REQUERIMIENTO_IMEI_EO> listadoActualizar = new List<BE_REQUERIMIENTO_IMEI_EO>();

                    listadoActualizar = JsonConvert.DeserializeObject<List<BE_REQUERIMIENTO_IMEI_EO>>(HttpContext.Session.GetString("ListaImeiPorAtender"));

                    if (listadoActualizar == null)
                    {
                        listadoActualizar = new List<BE_REQUERIMIENTO_IMEI_EO>();
                    }

                    foreach (var item in listadoActualizar)
                    {
                        if (item.indImeiEncontrado)
                        {
                            item.usuMod = HttpContext.Session.GetString("IdUsuario");
                            idResultado = objLogica.ActualizarInformacionIMEI(item);
                        }
                    }
                }
                catch (Exception e)
                {
                    LogError.RegistrarErrorMetodo(e, "ActualizarInformacionImei=> Actualizar imei");
                    result = new KeyValuePair<string, string>("0", "Error al grabar la información");
                }
                if (idResultado > 0)
                {
                    HttpContext.Session.SetString("IrTabCargarInformacion", "1");
                    result = new KeyValuePair<string, string>("1", "¡Se ha cargado la información de IMEI correctamente!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al actualizar la información");
                }
                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ActualizarInformacionImei");
                result = new KeyValuePair<string, string>("0", "Error al actualizar la información");
                return Json(result);
            }
        }
        public FileResult DescargarError_ImeiEO()
        {
            var rutaDocumentosPlantilla = configuration["CarpetaTemporal"];
            var archivoError = Path.Combine(rutaDocumentosPlantilla, "ErrorEO.txt");
            if (System.IO.File.Exists(archivoError))
            {
                System.IO.File.Delete(archivoError);
            }
            List<ErrorImei> listaError = new List<ErrorImei>();

            listaError = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("ListaErroresEOCarga"));

            List<ErrorImei> listaErrorErrores = new List<ErrorImei>();
            var listaErrror = JsonConvert.SerializeObject(listaErrorErrores);
            HttpContext.Session.SetString("ListaErroresEOCarga", listaErrror);

            if (listaError != null)
            {
                if (listaError.Count > 0)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(archivoError, true))
                    {
                        file.WriteLine("------ERRORES ENCONTRADOS EN EL DOCUMENTO CARGADO----");
                        foreach (var item in listaError)
                        {
                            file.WriteLine(item.nroImei + ": Error==>" + item.codError);
                        }
                        file.Dispose();
                    }
                }
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoError);
            string fileName = "ErrorEO.txt";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }


        [HttpPost]
        public ActionResult DescartarInformacionImei(IFormCollection formulario)
        {
            KeyValuePair<string, string> keyResultError = new KeyValuePair<string, string>();

            try
            {


             
                IFormFile archivoImei = formulario.Files["fileDocDescartar"];

                String mensaje = "";

                keyResultError = new KeyValuePair<string, string>("", "");

                if (archivoImei != null)
                {
                    if (archivoImei.Length > UTConstantes.VALOR_CERO)
                    {
                        var nombreArchivo = formulario.Files["fileDocDescartar"].FileName;
                        if (nombreArchivo == null) { nombreArchivo = ""; }
                        string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileDocDescartar"].FileName);

                        int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
                        string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
                        //if (archivo.ContentLength / 1024 < 2000)//2MB

                        if (archivoImei.Length / 1024 < TamFileGrande)//2MB
                        {
                            if (extensionArchivo == ".pdf" || extensionArchivo == ".xls" || extensionArchivo == ".xlsx" || extensionArchivo == ".doc" || extensionArchivo == ".docx")
                            {
                                keyResultError = new KeyValuePair<string, string>("", "");
                            }
                            else
                            {
                                mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de archivo.";
                                keyResultError = new KeyValuePair<string, string>("0", mensaje);
                            }
                        }
                        else
                        {
                            keyResultError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                        }
                    }
                }

                if (keyResultError.Key == "")
                {
                    Int64 idResultado = 0;
                    try
                    {
                        BE_REQUERIMIENTO_IMEI_EO objEntidad = new BE_REQUERIMIENTO_IMEI_EO();
                        BL_REQUERIMIENTO_IMEI_EO objLogica = new BL_REQUERIMIENTO_IMEI_EO();

                        long idAdjunto = adjuntarArchivoDescartar(formulario);

                        objEntidad.idImeiEO = Convert.ToInt64(formulario["ihidImeiEO"].ToString());
                        objEntidad.idTipoObservacion = formulario["ddlMotivo"] == "" ? 0 : Convert.ToInt32(formulario["ddlMotivo"].ToString());
                        objEntidad.observacion = formulario["comentariosDescarte"].ToString();
                        objEntidad.idAdjunto = idAdjunto;
                        objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario"); ;
                        objEntidad.idEstado = UTConstantes.ID_ESTADO_ATENDIDO;

                        idResultado = objLogica.DescartarIMEI(objEntidad);
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarErrorMetodo(ex, "Al registrar el descarte de imei=>DescartarInformacionImei");
                        keyResultError = new KeyValuePair<string, string>("0", "Error al actualizar la información");
                        return Json(keyResultError);
                    }
                    if (idResultado > 0)
                    {
                        HttpContext.Session.SetString("IrTabCargarInformacion", "1");
                        keyResultError = new KeyValuePair<string, string>("1", "¡Se ha descartado el IMEI correctamente!");
                        return Json(keyResultError);
                    }
                    else
                    {
                        keyResultError = new KeyValuePair<string, string>("0", "Error al descartar la información del IMEI");
                        return Json(keyResultError);
                    }
                }
                else
                {
                    return Json(keyResultError);
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "Error al descartar la información=>DescartarInformacionImei ");
                keyResultError = new KeyValuePair<string, string>("0", "Error al descartar la información");
                return Json(keyResultError);
            }
        }




        #region "Metodos"

        private long adjuntarArchivoDescartar(IFormCollection formulario)
        {
            long idAdjunto = 0;
            try
            {

                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");

                IFormFile fileDocDevolver = formulario.Files["fileDocDescartar"];
                var rutaCarpetaSIGREI = configuration["RutaDocumentos"];
                var nombreArchivoImei = Path.GetFileName(formulario.Files["fileDocDescartar"].FileName);
                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");


                BL_DOCUMENTO_ADJUNTO objLogicaArchivoAdjunto = new BL_DOCUMENTO_ADJUNTO();
                BE_DOCUMENTO_ADJUNTO objAdjunto = new BE_DOCUMENTO_ADJUNTO();

                if (fileDocDevolver.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaSIGREI, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileDocDevolver.CopyTo(fileStream);
                    }
                    objAdjunto.nombreDocumento = nombreArchivoImei;
                    objAdjunto.descDocumento = "Empresa Operadora, Archivo adjuntado al descartar un archivo imei";

                    objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");
                    idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);

                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "Registrado documento adjunto al devolver imei=>FrmRequerimientoDerivarIMEI ");
                idAdjunto = 0;
            }
            return idAdjunto;
        }

        public BE_REQUERIMIENTO_EO obtenerCriterios(IFormCollection formulario)
        {

            BE_REQUERIMIENTO_EO objResultado = new BE_REQUERIMIENTO_EO();
            objResultado.idEmpresaEO = Int64.Parse(HttpContext.Session.GetString("cod_Empresa"));
            objResultado.fechaInicio = formulario["txtFechaInicio"].ToString();
            objResultado.fechaFin = formulario["txtFechaFin"].ToString();
            objResultado.nroImei = formulario["txtNroImei"].ToString();
            objResultado.idEstado = formulario["ddlEstado"].ToString() == null || formulario["ddlEstado"].ToString() == "" ? 0 : Convert.ToInt32(formulario["ddlEstado"].ToString());

            return objResultado;
        }


        private KeyValuePair<string, string> CargarInformacion(IFormCollection formulario)
        {
            List<ErrorImei> listaErrorErrores = new List<ErrorImei>();
            var listaErrror = JsonConvert.SerializeObject(listaErrorErrores);
            HttpContext.Session.SetString("ListaErroresEOCarga", listaErrror);

            String mensaje = "";
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileArchivoImei"].FileName);

            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            //if (archivo.ContentLength / 1024 < 2000)//2MB
            IFormFile archivoImei = formulario.Files["fileArchivoImei"];
            if (archivoImei.Length / 1024 < TamFileGrande)//2MB
            {
                if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                {
                    keyValueError = new KeyValuePair<string, string>("", "");
                }
                else
                {
                    mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                    keyValueError = new KeyValuePair<string, string>("0", "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.");
                }
            }
            else
            {
                keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
            }


            if (keyValueError.Key == "")
            {

                BE_REQUERIMIENTO objParametroDos = new BE_REQUERIMIENTO();

                #region "Copiar a una carpeta temporal para revisar si hay registro que registrar"

                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                IFormFile fileArchivoImei = formulario.Files["fileArchivoImei"];
                var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                string nombreArchivoImei = Path.GetFileName(formulario.Files["fileArchivoImei"].FileName);

                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                var ubicacionArchivoTemporal = "";

                if (fileArchivoImei.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileArchivoImei.CopyTo(fileStream);
                    }
                    ubicacionArchivoTemporal = filePath;
                }
                FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                BE_REQUERIMIENTO_IMEI_EO objParametro = new BE_REQUERIMIENTO_IMEI_EO();
                List<BE_REQUERIMIENTO_IMEI_EO> listaExcel = new List<BE_REQUERIMIENTO_IMEI_EO>();
                ErrorImei objError = new ErrorImei();
                using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                {
                    try
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets["IMEI"];
                        int totalRows = workSheet.Dimension.Rows;

                        if (totalRows < 2)
                        {
                            return keyValueError = new KeyValuePair<string, string>("2", "No existe información en el archivo adjunto de IMEI");
                        }
                        else
                        {
                            try
                            {
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    objError = new ErrorImei();
                                    objParametro = new BE_REQUERIMIENTO_IMEI_EO();
                                    objParametro.nroItem = i;

                                    objParametro.nroImei = workSheet.Cells[i, 2].Value != null ? workSheet.Cells[i, 2].Value.ToString().Trim() : "";
                                    objError.nroImei = workSheet.Cells[i, 2].Value != null ? workSheet.Cells[i, 2].Value.ToString().Trim() : "";
                                    objParametro.descTipoReporte = workSheet.Cells[i, 3].Value != null ? workSheet.Cells[i, 3].Value.ToString().Trim() : "";
                                    objParametro.fecReporte = workSheet.Cells[i, 4].Value != null ? workSheet.Cells[i, 4].Value.ToString().Trim() : "";
                                    objParametro.nroServicio = workSheet.Cells[i, 5].Value != null ? workSheet.Cells[i, 5].Value.ToString().Trim() : "";
                                    objParametro.apPaternoTitular = workSheet.Cells[i, 6].Value != null ? workSheet.Cells[i, 6].Value.ToString().Trim() : "";
                                    objParametro.apMaternoTitular = workSheet.Cells[i, 7].Value != null ? workSheet.Cells[i, 7].Value.ToString().Trim() : "";
                                    objParametro.nombreTitular = workSheet.Cells[i, 8].Value != null ? workSheet.Cells[i, 8].Value.ToString().Trim() : "";
                                    objParametro.idTipoDocumento = workSheet.Cells[i, 9].Value != null ? workSheet.Cells[i, 9].Value.ToString().Trim() : "";
                                    objParametro.nroDocumento = workSheet.Cells[i, 10].Value != null ? workSheet.Cells[i, 10].Value.ToString().Trim() : "";
                                    if (objParametro.idTipoDocumento.Length == 1)
                                    {
                                        objParametro.idTipoDocumento = "0" + objParametro.idTipoDocumento.ToString();
                                    }

                                    if (objParametro.nroServicio.Length == 0 && objParametro.nombreTitular.Length == 0
                                        && objParametro.apPaternoTitular.Length == 0 && objParametro.apMaternoTitular.Length == 0
                                        && objParametro.idTipoDocumento.Length == 0 && objParametro.nroDocumento.Length == 0)
                                    {
                                        //objError.codError = String.Format("La fila {0} no contiene información del imei", i);
                                        //listaError.Add(objError);
                                    }
                                    else
                                    {
                                        String numeroSericio = objParametro.nroServicio;
                                        objParametro.nroServicio = numeroSericio.Trim();
                                        objParametro.nombreTitular = objParametro.nombreTitular.Trim();

                                        objParametro = ValidarRegistroImeiEO(objParametro, i);
                                        if (!objParametro.conError)
                                        {
                                            listaExcel.Add(objParametro);
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                return keyValueError = new KeyValuePair<string, string>("0", "Error en la obtención de los datos");
                            }
                        }

                    }
                    catch (Exception)
                    {
                        return keyValueError = new KeyValuePair<string, string>("0", UTConstantes.PLANTILLA_EXCEL_NO_EXISTE_HOJA);
                    }


                }
                #endregion


                List<ErrorImei> listaError = new List<ErrorImei>();
                listaError = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("ListaErroresEOCarga"));

                if (listaError == null) { listaError = new List<ErrorImei>(); }
                if (listaError.Count > UTConstantes.VALOR_CERO)
                {
                    listaError = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("ListaErroresEOCarga"));
                    if (listaError == null)
                    {
                        listaError = new List<ErrorImei>();
                    }

                    if (listaError == null)
                    {
                        listaError = new List<ErrorImei>();
                    }
                    if (listaError.Count > 0)
                    {
                        return keyValueError = new KeyValuePair<string, string>("3", "Se ha encontrado errores en el archivo IMEI");
                    }
                }
                else
                {
                    List<BE_REQUERIMIENTO_IMEI_EO> listaResultado = new List<BE_REQUERIMIENTO_IMEI_EO>();
                    List<BE_REQUERIMIENTO_IMEI_EO> listadoActualizar = new List<BE_REQUERIMIENTO_IMEI_EO>();
                    objParametro = new BE_REQUERIMIENTO_IMEI_EO();

                    objParametro.idRequerimientoEO = Int64.Parse(formulario["ihrequerimientoeo"].ToString());
                    objParametro.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;
                    BL_REQUERIMIENTO_EO objLogicaEO = new BL_REQUERIMIENTO_EO();

                    listaResultado = objLogicaEO.ListaIMEI_EO(objParametro);

                    if (listaExcel != null)
                    {
                        if (listaExcel.Count > UTConstantes.VALOR_CERO)
                        {
                            foreach (var item in listaExcel)
                            {

                                if (listaResultado.Count > UTConstantes.VALOR_CERO)
                                {
                                    foreach (var itemDos in listaResultado)
                                    {
                                        if (item.nroImei.Equals(itemDos.nroImei))
                                        {
                                            //total = total + 1;
                                            itemDos.nroServicio = item.nroServicio;
                                            itemDos.apPaternoTitular = item.apPaternoTitular;
                                            itemDos.apMaternoTitular = item.apMaternoTitular;
                                            itemDos.nombreTitular = item.nombreTitular;
                                            itemDos.nombreCompletoTitular = item.apPaternoTitular + " " + item.apMaternoTitular + " " + item.nombreTitular;
                                            itemDos.idTipoDocumento = item.idTipoDocumento;
                                            itemDos.descTipoDocumento = item.descTipoDocumento;
                                            itemDos.nroDocumento = item.nroDocumento;
                                            itemDos.indImeiEncontrado = true;
                                            listadoActualizar.Add(itemDos);

                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    var listaResultadoJSON = JsonConvert.SerializeObject(listadoActualizar);
                    HttpContext.Session.SetString("ListaImeiPorAtender", listaResultadoJSON);
                    return keyValueError = new KeyValuePair<string, string>("1", "Se ha cargado la información correctamente");
                }
            }
            else
            {
                return keyValueError = new KeyValuePair<string, string>("0", UTConstantes.MSG_DOCUMENTO_EXCEL_REGISTRO_PUBLICO_VACIO);
            }
            return keyValueError;
        }



        private BE_REQUERIMIENTO_IMEI_EO ValidarRegistroImeiEO(BE_REQUERIMIENTO_IMEI_EO objParametro, Int32 fila)
        {

            List<ErrorImei> listaError = new List<ErrorImei>();

            listaError = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("ListaErroresEOCarga"));
            if (listaError == null)
            {
                listaError = new List<ErrorImei>();
            }

            ErrorImei objError = new ErrorImei();
            objError.nroImei = objParametro.nroImei;
            String errores = "";
            if (objParametro.nroImei == "")
            {
                errores += "No contiene el número de IMEI";
                errores += ", ";

            }
            if (objParametro.nroServicio == "")
            {
                errores += "No contiene el número de servicio";
                errores += ", ";

            }
            else
            {
                long numservicio = 0;
                bool isNumero = long.TryParse(objParametro.nroServicio, out numservicio);
                if (!isNumero)
                {
                    errores += "El valor del número de servicio contiene caracteres que no es numerico";
                    errores += ", ";
                }
                else if (objParametro.nroServicio.Length != 9)
                {
                    errores += "El número de servicio no contiene 9 dígitos";
                    errores += ",";
                }
                else if (objParametro.nroServicio.Substring(0, 1).ToString() != "9")
                {
                    errores += "El número de servicio no empieza con el dígito 9";
                    errores += ", ";
                }
            }

            String tipoDocumento = objParametro.idTipoDocumento.ToString();

            if (tipoDocumento.Equals("01") || tipoDocumento.Equals("03") ||
                tipoDocumento.Equals("04") || tipoDocumento.Equals("05"))
            {

                /**Nombes del Titular*/
                if (objParametro.nombreTitular == "")
                {
                    errores += "No contiene el nombre del posible titular";
                    errores += ", ";
                }
                else
                {
                    Boolean existeNumero = contieneNumero(objParametro.nombreTitular);
                    if (existeNumero)
                    {
                        errores += "En el nombre del posible titular contiene números";
                        errores += ", ";
                    }
                }

                /**Nombes del Titular*/
                if (objParametro.apPaternoTitular == "")
                {
                    errores += "No contiene el apellido paterno del posible titular";
                    errores += ", ";
                }
                else
                {
                    Boolean existeNumero = contieneNumero(objParametro.apPaternoTitular);
                    if (existeNumero)
                    {
                        errores += "En el apellido paterno del posible titular contiene números";
                        errores += ", ";

                    }
                }
                if (objParametro.apMaternoTitular == "")
                {
                    errores += "No contiene el apellido materno del posible titular";
                    errores += ", ";
                }
                else
                {
                    Boolean existeNumero = contieneNumero(objParametro.apMaternoTitular);
                    if (existeNumero)
                    {
                        errores += "En el apellido materno del posible titular contiene números";
                        errores += ", ";

                    }
                }
                if (objParametro.nroDocumento.Length == 0)
                {
                    errores += "No ha ingresado el número de documento de identidad";
                    errores += ", ";
                }
                else
                {
                    long numservicio = 0;
                    bool isNumero = long.TryParse(objParametro.nroDocumento, out numservicio);
                    if (!isNumero)
                    {
                        errores += "El número de documento de identidad contiene letras";
                        errores += ", ";
                    }
                    if (tipoDocumento.Equals("01"))
                    {
                        if (objParametro.nroDocumento.Length != 8)
                        {
                            errores += "La longitud de número de DNI es incorrecto";
                            errores += ", ";
                        }
                    }
                    else if (tipoDocumento.Equals("03") || tipoDocumento.Equals("04") || tipoDocumento.Equals("05"))
                    {
                        if (objParametro.nroDocumento.Length > 20)
                        {
                            errores += "La longitud de número de documento de identidad es incorrecto";
                            errores += ", ";
                        }
                    }
                }
                if (errores == "")
                {
                    if (tipoDocumento.Equals("01")) { objParametro.descTipoDocumento = UTConstantes.DESCTIPODOCIDENTIDAD_DNI; }
                    if (tipoDocumento.Equals("03")) { objParametro.descTipoDocumento = UTConstantes.DESCTIPODOCIDENTIDAD_CARNET; }
                    if (tipoDocumento.Equals("04")) { objParametro.descTipoDocumento = UTConstantes.DESCTIPODOCIDENTIDAD_PASAPORTE; }
                    if (tipoDocumento.Equals("05")) { objParametro.descTipoDocumento = UTConstantes.DESCTIPODOCIDENTIDAD_DOCUMENTO_LEGAL; }
                }

            }
            else if (tipoDocumento.Equals("02"))
            {
                /**Nombes del Titular*/
                if (objParametro.nombreTitular == "")
                {
                    errores += "No tiene la razón social";
                    errores += ", ";
                }
                if (objParametro.nroDocumento.Length == 0)
                {
                    errores += "No tiene el número de RUC";
                    errores += ", ";
                }
                else if (objParametro.nroDocumento.Length != 11)
                {
                    errores += "La longitud de número de RUC es incorrecto";
                    errores += ", ";
                }
                else
                {
                    long numservicio = 0;
                    bool isNumero = long.TryParse(objParametro.nroDocumento, out numservicio);
                    if (!isNumero)
                    {
                        errores += "El número de RUC contiene letras";
                        errores += ", ";
                    }
                }
                objParametro.apMaternoTitular = "";
                objParametro.apPaternoTitular = "";
                if (errores == "")
                {
                    objParametro.descTipoDocumento = UTConstantes.DESCTIPODOCIDENTIDAD_RUC;
                }
            }
            else
            {
                errores += "El número de tipo de documento no es correcto";
                errores += ", ";

                if (objParametro.nombreTitular == "")
                {
                    errores += "No tiene el nombre del posible titular";
                    errores += ", ";
                }
                if (objParametro.nroDocumento.Length == 0)
                {
                    errores += "No tiene el número de documento legal";
                    errores += ", ";
                }

            }
            if (errores != "")
            {
                errores = errores.Substring(0, errores.Length - 2);
                objError.codError = String.Format("La fila {0} tiene el siguiente error: ", fila) + errores;
                listaError.Add(objError);

                var listaErrorCargaJSONFinal = JsonConvert.SerializeObject(listaError);
                HttpContext.Session.SetString("ListaErroresEOCarga", listaErrorCargaJSONFinal);

                objParametro.conError = true;
            }
            return objParametro;
        }

        private bool contieneNumero(String cadena)
        {
            bool isNum = false;
            foreach (char c in cadena)
            {
                if (char.IsNumber(c) == true)
                {
                    isNum = true;
                    break;
                }
            }
            return isNum;
        }

        #endregion
    }
}