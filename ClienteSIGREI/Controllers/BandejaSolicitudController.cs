﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class BandejaSolicitudController : Controller
    {
        public IActionResult Index()
        {

            BL_EMPRESA ObjLogica = new BL_EMPRESA();
            List<BE_EMPRESA> lista = ObjLogica.Listar(0);

            ViewBag.Lista_EmpresaEO = lista;

            List<EstadoBE> listaEstado = new List<EstadoBE>();
            EstadoBE objEstado = new EstadoBE();
            objEstado.IDESTADO = "1";
            objEstado.ESTADO = "Registrado";

            listaEstado.Add(objEstado);


            objEstado = new EstadoBE();
            objEstado.IDESTADO = "4";
            objEstado.ESTADO = "Atendido";
            listaEstado.Add(objEstado);

            ViewBag.Lista_Estado = listaEstado;

            BlParametro ObjLogicaP = new BlParametro();
            List<BE_PARAMETRO> listaP = ObjLogicaP.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOMOTIVOVENCIMIENTO, UTConstantes.APLICACION_COLUMNA.IDTIPOMOTIVOVENCIMIENTO);
            ViewBag.Lista_Vencimiento = listaP;

            return View();
        }

        [HttpPost]
        public ActionResult BuscarSolicitudes(IFormCollection formulario)
        {

            List<BE_REQUERIMIENTO_EO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO_EO>();
            BE_REQUERIMIENTO_EO objParametro = new BE_REQUERIMIENTO_EO();
            BL_REQUERIMIENTO_EO objLogica = new BL_REQUERIMIENTO_EO();

            objParametro = obtenerCriterios(formulario);

            listaBE_REQUERIMIENTO = objLogica.ListaSolicitudEO(objParametro);

            return PartialView("_DetalleSolicitud", listaBE_REQUERIMIENTO);
        }

        [HttpPost]
        public ActionResult LimpiarCriteriosBandeja(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_EO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO_EO>();

            return PartialView("_DetalleSolicitud", listaBE_REQUERIMIENTO);
        }


        public BE_REQUERIMIENTO_EO obtenerCriterios(IFormCollection formulario)
        {
            BE_REQUERIMIENTO_EO objResultado = new BE_REQUERIMIENTO_EO();
            objResultado.idEmpresaEO = formulario["ddlEmpresaOperadora"].ToString() == null || formulario["ddlEmpresaOperadora"].ToString() == "" ? 0 : Convert.ToInt32(formulario["ddlEmpresaOperadora"].ToString());
            objResultado.fechaInicio = formulario["txtFechaInicio"].ToString();
            objResultado.fechaFin = formulario["txtFechaFin"].ToString();
            objResultado.nroImei = formulario["txtNroImei"].ToString();
            objResultado.idEstado = formulario["ddlEstado"].ToString() == null || formulario["ddlEstado"].ToString() == "" ? 0 : Convert.ToInt32(formulario["ddlEstado"].ToString());
            return objResultado;
        }




        [HttpPost]
        public ActionResult CargaInicial(IFormCollection formulario)
        {

            List<BE_REQUERIMIENTO_EO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO_EO>();
            return PartialView("_DetalleSolicitud", listaBE_REQUERIMIENTO);
        }

        [HttpPost]
        public ActionResult AmpliarVencimientoPlazo(IFormCollection formulario)
        {
            Int64 idResultado = 0;
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {


                BE_REQUERIMIENTO_EO objEntidad = new BE_REQUERIMIENTO_EO();
                BL_REQUERIMIENTO_EO objLogica = new BL_REQUERIMIENTO_EO();
                objEntidad.idRequerimientoEO = Convert.ToInt64(formulario["ihrequerimientoeo"]);
                objEntidad.idTipoMotivoVencimiento = formulario["ddlMotivo"] == "" ? 0 : Convert.ToInt32(formulario["ddlMotivo"].ToString());
                objEntidad.fechaPlazo = formulario["txtFechaVencimiento"].ToString();
                objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario");

                idResultado = objLogica.ActualizarVencimientoEO(objEntidad);
                if (idResultado > 0)
                {
                    HttpContext.Session.SetString("IrTabCargarInformacion", "1");
                    result = new KeyValuePair<string, string>("1", "¡Se ha ampliado la fecha de vencimiento  correctamente!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al ampliar la fecha de vencimiento");
                }                
                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "AmpliarVencimientoPlazo");
                result = new KeyValuePair<string, string>("0", "Error al ampliar la fecha de vencimiento");
                
                return Json(result);
            }
        }

    }
}