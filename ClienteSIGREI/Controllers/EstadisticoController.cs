﻿using Osiptel.Clases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;
using Osiptel.SIGREI.BE.General;
using Osiptel.SIGREI.BE.Reporte.Req;
using System.Data;
using System.Linq;
using Osiptel.SIGREI.BE.Reporte;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using System.Drawing;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class EstadisticoController : Controller
    {

        private readonly IConfiguration configuration;
        public EstadisticoController(IConfiguration config)
        {
            configuration = config;
        }

        public IActionResult Index()
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            ViewBag.ListaAnio = CargaComboAnio();
            //ViewBag.ListTabs = CargarCombo(UTConstantes.APLICACION_TABLA.TIPOREPORTE,UTConstantes.APLICACION_COLUMNA.IDTIPOREPORTE);
            ViewBag.ListaRequerimiento = CargarCombo(UTConstantes.APLICACION_TABLA.TIPOREQUERIMIENTO, UTConstantes.APLICACION_COLUMNA.IDTIPOREQUERIMIENTO);
            //ViewBag.ListaRequerimiento = CargarComboRequerimiento();
            ViewBag.ListaInstitucion = CargarCombo(UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
            ViewBag.ListaPeriodo = objLogica.ListaComboMes();
            ViewBag.ListaRegion = objLogica.ListaComboUbigeo();
            ViewBag.ListaEO = objLogica.ListaComboEO();
            return View();
        }

        public List<BE_COMBO> CargaComboAnio()
        {
            Funciones objFunciones = new Funciones();
            List<BE_COMBO> lstAnio = new List<BE_COMBO>();
            BE_COMBO objAnio;

            String anioMinimo = objFunciones.ObtenerAnio();
            if (anioMinimo != "")
            {
                int anioactual = DateTime.Now.Year;
                Int32 anioInicio = Int32.Parse(anioMinimo.ToString());

                for (int i = anioactual; i >= anioInicio; i--)
                {
                    objAnio = new BE_COMBO();
                    objAnio.id = i.ToString();
                    objAnio.descripcion = i.ToString();

                    lstAnio.Add(objAnio);
                }
            }
            return lstAnio;
        }

        public List<BE_COMBO> CargarCombo(String tabla, String columna)
        {
            List<BE_COMBO> lstCombo = new List<BE_COMBO>();
            BE_COMBO objR;
            BlParametro objParametro = new BlParametro();
            List<BE_PARAMETRO> lstParametro = objParametro.ListarValores(UTConstantes.APLICACION, tabla, columna);
            foreach (var item in lstParametro)
            {
                objR = new BE_COMBO();
                objR.id = item.valor;
                objR.descripcion = item.descripcion;
                lstCombo.Add(objR);
            }
            return lstCombo;
        }

        public List<BE_COMBO> CargarComboRequerimiento()
        {
            List<BE_COMBO> lstCombo = new List<BE_COMBO>();
            BE_COMBO objR;
            objR = new BE_COMBO();
            objR.id = "1";
            objR.descripcion = "SERVICIO MÓVIL";
            lstCombo.Add(objR);
            objR = new BE_COMBO();
            objR.id = "2";
            objR.descripcion = "IMEI FÍSICO";
            lstCombo.Add(objR);
            objR = new BE_COMBO();
            objR.id = "3";
            objR.descripcion = "SERVICIO MÓVIL - IMEI FÍSICO";
            lstCombo.Add(objR);
            return lstCombo;
        }

        [HttpPost]
        public ActionResult GraficarRegion(IFormCollection formulario)
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            BE_REPORTE_REGION objRegion = new BE_REPORTE_REGION();
            objRegion.anio = formulario["anio"];
            objRegion.requerimiento = formulario["requerimiento"];
            objRegion.region = formulario["region"];
            objRegion.institucion = formulario["institucion"];
            objRegion.periodo = formulario["periodo"];
            objRegion.desde = formulario["desde"];
            objRegion.hasta = formulario["hasta"];

            List<BE_GRAFICA_FORMATO> formatoList = new List<BE_GRAFICA_FORMATO>();

            DataTable obj = objLogica.ListarReporteRegion(objRegion);

            var parseData = (from u in obj.Select().AsEnumerable()
                             select new BE_GRAFICA_DATABASE()
                             {
                                 idubigeo = Convert.ToInt32(u["idubigeo"].ToString()),
                                 departament = u["departamento"].ToString(),
                                 servicio = u["servicio"].ToString(),
                                 cantidad = Convert.ToInt32(u["cantidad"].ToString())
                             }).ToList();


            var validarData = (from u in parseData
                               group u by new { u.idubigeo, u.departament, u.servicio } into g
                               orderby g.Key.idubigeo
                               select new
                               {
                                   idubigeo = g.Key.idubigeo,
                                   depa = g.Key.departament,
                                   serv = g.Key.servicio,
                                   cant = g.Sum(x => x.cantidad)
                               }).ToList();

            var departamentos = (from u in validarData
                                 group u by new { u.idubigeo, u.depa } into g
                                 orderby g.Key.idubigeo
                                 select new
                                 {
                                     id = g.Key.idubigeo,
                                     nombre = g.Key.depa
                                 }).ToList();

            var servicios = (from u in validarData
                             group u by u.serv into g
                             orderby g.Key
                             select new BE_GRAFICA_FORMATO
                             {
                                 label = g.Key,
                                 data = new List<int>(),
                             }).ToList();

            var colores = new List<string>()
            {
                "rgb(255, 99, 132)",
                "rgb(54, 162, 235)",
                "rgb(60, 186, 159)"
            };

            if (objRegion.requerimiento == "0")
            {
                for (var i = 0; i < servicios.Count; i++)
                {
                    servicios[i].backgroundColor = colores[i];
                }
            }
            else
            {
                servicios[0].backgroundColor = colores[Convert.ToInt32(objRegion.requerimiento) - 1];
            }




            BE_GRAFICA objGrafica = new BE_GRAFICA();
            objGrafica.labels = new List<string>();
            objGrafica.datasets = servicios;

            foreach (var itemD in departamentos)
            {
                objGrafica.labels.Add(itemD.nombre);
                foreach (var itemS in objGrafica.datasets)
                {
                    var filtro = (from u in validarData
                                  where u.idubigeo == itemD.id &&
                                        u.depa == itemD.nombre &&
                                        u.serv == itemS.label
                                  select u.cant).FirstOrDefault();
                    itemS.data.Add(filtro);
                }
            }

            return Json(objGrafica);
        }

        [HttpPost]
        public ActionResult TablaRegion(IFormCollection formulario)
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            BE_REPORTE_REGION objRegion = new BE_REPORTE_REGION();
            objRegion.anio = formulario["anio"];
            objRegion.requerimiento = formulario["requerimiento"];
            objRegion.region = formulario["region"];
            objRegion.institucion = formulario["institucion"];
            objRegion.periodo = formulario["periodo"];
            objRegion.desde = formulario["desde"];
            objRegion.hasta = formulario["hasta"];

            DataTable obj = objLogica.TablaReporteRegion(objRegion);

            List<List<String>> dataSeteada = new List<List<String>>();

            List<BE_TABLA_REGION> dataParse = (from d in obj.Select().AsEnumerable()
                                               select new BE_TABLA_REGION
                                               {
                                                   idUbigeo = d.Table.Columns.Contains("idubigeo") ? Convert.ToInt32(d["idubigeo"].ToString()) : 0,
                                                   departamento = d.Table.Columns.Contains("departamento") ? d["departamento"].ToString() : "",
                                                   entidad = d.Table.Columns.Contains("entidad") ? d["entidad"].ToString() : "",
                                                   externa = d.Table.Columns.Contains("externa") ? d["externa"].ToString() : "",
                                                   unidad = d.Table.Columns.Contains("unidad") ? d["unidad"].ToString() : "",
                                                   ruc = d.Table.Columns.Contains("ruc") ? d["ruc"].ToString() : "",
                                                   servicio = d.Table.Columns.Contains("servicio") ? d["servicio"].ToString() : "",
                                                   anio = d.Table.Columns.Contains("anio") ? d["anio"].ToString() : "",
                                                   idMes = d.Table.Columns.Contains("idmes") ? Convert.ToInt32(d["idmes"].ToString()) : 0,
                                                   mes = d.Table.Columns.Contains("mes") ? d["mes"].ToString() : "",
                                                   dia = d.Table.Columns.Contains("dia") ? d["dia"].ToString() : "",
                                                   cantidad = d.Table.Columns.Contains("cantidad") ? Convert.ToInt32(d["cantidad"].ToString()) : 0
                                               }).ToList();


            List<BE_TABLA_FORMATO_FECHA> parseFechas = (from u in dataParse
                                                        group u by u.anio into gAnio
                                                        orderby gAnio.Key
                                                        select new BE_TABLA_FORMATO_FECHA()
                                                        {
                                                            anio = gAnio.Key,
                                                            meses = (from u in dataParse
                                                                     where u.anio == gAnio.Key
                                                                     group u by new { u.idMes, u.mes } into gMes
                                                                     orderby gMes.Key.idMes
                                                                     select new BE_TABLA_MES_ANIO
                                                                     {
                                                                         idMes = gMes.Key.idMes,
                                                                         mes = gMes.Key.mes,
                                                                         dias = (from u in dataParse
                                                                                 where u.anio == gAnio.Key && u.idMes == gMes.Key.idMes
                                                                                 group u by u.dia into gDia
                                                                                 orderby gDia.Key
                                                                                 select gDia.Key).ToList()
                                                                     }).ToList()

                                                        }).ToList();


            if (objRegion.periodo == "0" && objRegion.desde == "0" && objRegion.hasta == "0")
            {
                var parseGroup = (from u in dataParse
                                  group u by new { u.idUbigeo, u.departamento, u.entidad, u.externa, u.unidad, u.ruc, u.servicio } into g
                                  orderby g.Key.idUbigeo, g.Key.entidad, g.Key.externa, g.Key.unidad, g.Key.ruc, g.Key.servicio
                                  select new BE_TABLA_REGION()
                                  {
                                      idUbigeo = g.Key.idUbigeo,
                                      departamento = g.Key.departamento,
                                      entidad = g.Key.entidad,
                                      externa = g.Key.externa,
                                      unidad = g.Key.unidad,
                                      ruc = g.Key.ruc,
                                      servicio = g.Key.servicio
                                  }).ToList();

                foreach (var item in parseGroup)
                {
                    List<String> nuevaEntidad = new List<String>();
                    List<int> calcularTotal = new List<int>();
                    nuevaEntidad.Add(item.departamento);
                    nuevaEntidad.Add(item.entidad);
                    nuevaEntidad.Add(item.externa);
                    nuevaEntidad.Add(item.unidad);
                    //nuevaEntidad.Add(item.ruc);
                    nuevaEntidad.Add(item.servicio);

                    var anios = parseFechas.Select(x => x).ToList();

                    foreach (var item2 in anios)
                    {
                        var buscarEntidad = dataParse.Where(x => x.idUbigeo == item.idUbigeo
                                                       && x.departamento == item.departamento
                                                       && x.entidad == item.entidad
                                                       && x.externa == item.externa
                                                       && x.unidad == item.unidad
                                                       && x.ruc == item.ruc
                                                       && x.servicio == item.servicio
                                                       && x.anio == item2.anio).Select(x => x).ToList();
                        foreach (var item3 in item2.meses)
                        {
                            var mesVal = buscarEntidad.Where(x => x.idMes == item3.idMes).Select(x => x.cantidad == null ? 0 : Convert.ToInt32(x.cantidad)).Sum();

                            nuevaEntidad.Add(mesVal.ToString());
                            calcularTotal.Add(mesVal);
                        }
                    }
                    nuevaEntidad.Add(calcularTotal.Sum().ToString());
                    dataSeteada.Add(nuevaEntidad);
                }
            }
            else
            {

                var parseGroup = (from u in dataParse
                                  group u by new { u.idUbigeo, u.departamento, u.entidad, u.externa, u.unidad, u.ruc, u.servicio } into g
                                  orderby g.Key.idUbigeo, g.Key.entidad, g.Key.externa, g.Key.unidad, g.Key.ruc, g.Key.servicio
                                  select new BE_TABLA_REGION()
                                  {
                                      idUbigeo = g.Key.idUbigeo,
                                      departamento = g.Key.departamento,
                                      entidad = g.Key.entidad,
                                      externa = g.Key.externa,
                                      unidad = g.Key.unidad,
                                      ruc = g.Key.ruc,
                                      servicio = g.Key.servicio
                                  }).ToList();

                foreach (var item in parseGroup)
                {
                    List<String> nuevaEntidad = new List<String>();
                    List<int> calcularTotal = new List<int>();
                    nuevaEntidad.Add(item.departamento);
                    nuevaEntidad.Add(item.entidad);
                    nuevaEntidad.Add(item.externa);
                    nuevaEntidad.Add(item.unidad);
                    //nuevaEntidad.Add(item.ruc);
                    nuevaEntidad.Add(item.servicio);

                    var anios = parseFechas.Select(x => x).ToList();

                    foreach (var item2 in anios)
                    {
                        foreach (var item3 in item2.meses)
                        {
                            var buscarEntidad = dataParse.Where(x => x.idUbigeo == item.idUbigeo
                                                        && x.departamento == item.departamento
                                                        && x.entidad == item.entidad
                                                        && x.externa == item.externa
                                                        && x.unidad == item.unidad
                                                        && x.ruc == item.ruc
                                                        && x.servicio == item.servicio
                                                        && x.anio == item2.anio
                                                        && x.mes == item3.mes).Select(x => x).ToList();
                            foreach (var item4 in item3.dias)
                            {
                                var diaVal = buscarEntidad.Where(x => x.dia == item4).Select(x => x.cantidad == null ? 0 : Convert.ToInt32(x.cantidad)).Sum();
                                nuevaEntidad.Add(diaVal.ToString());
                                calcularTotal.Add(diaVal);
                            }
                        }
                    }
                    nuevaEntidad.Add(calcularTotal.Sum().ToString());
                    dataSeteada.Add(nuevaEntidad);
                }
            }

            var oR = new
            {
                data = dataSeteada,
                parseFechas
            };

            return Json(oR);
        }

        [HttpPost]
        public ActionResult GraficarEntidad(IFormCollection formulario)
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            BE_REPORTE_ENTIDAD objEntidad = new BE_REPORTE_ENTIDAD();
            objEntidad.anio = formulario["anio"];
            objEntidad.requerimiento = formulario["requerimiento"];
            objEntidad.institucion = formulario["institucion"];
            objEntidad.periodo = formulario["periodo"];
            objEntidad.desde = formulario["desde"];
            objEntidad.hasta = formulario["hasta"];

            DataTable obj = objLogica.ListarReporteEntidad(objEntidad);

            var dataParse = (from u in obj.Select().AsEnumerable()
                             select new
                             {
                                 mes = u["MES"].ToString(),
                                 entidad = u["ENTIDAD"].ToString(),
                                 servicio = u["SERVICIO"].ToString(),
                                 cantidad = Convert.ToInt32(u["CANTIDAD"].ToString())
                             }).ToList();


            var validarData = (from u in dataParse
                               group u by new { u.mes, u.entidad, u.servicio } into g
                               select new
                               {
                                   mes = g.Key.mes,
                                   entidad = g.Key.entidad,
                                   servicio = g.Key.servicio,
                                   cantidad = g.Sum(x => x.cantidad)
                               }).ToList();


            List<String> paseDatosColores = new List<string>()
                {
                    "#3e95cd",
                    "#8e5ea2",
                    "#3cba9f",
                    "#3e95cd",
                    "#8e5ea2",
                    "#3cba9f",
                    "#3e95cd",
                    "#8e5ea2",
                    "#3cba9f"
                };


            var entidades = (from u in validarData
                             group u by u.entidad into g
                             orderby g.Key
                             select g.Key).ToList();



            BE_PIE_GRAFICA_FORMATO oPie = new BE_PIE_GRAFICA_FORMATO();
            oPie.label = "DataSets 1";
            oPie.backgroundColor = paseDatosColores;
            oPie.data = (from u in validarData
                         group u by u.entidad into g
                         orderby g.Key
                         select g.Sum(x => x.cantidad)).ToList();

            var objFormatoPie = new List<BE_PIE_GRAFICA_FORMATO>();
            objFormatoPie.Add(oPie);

            BE_GRAFICA_PIE pie = new BE_GRAFICA_PIE();
            pie.labels = entidades;
            pie.datasets = objFormatoPie;



            BE_GRAFICA barra = new BE_GRAFICA();
            barra.labels = (from u in validarData
                            group u by u.mes into g
                            select g.Key).ToList();
            barra.datasets = new List<BE_GRAFICA_FORMATO>();

            var index = 0;

            foreach (var itemE in entidades)
            {
                BE_GRAFICA_FORMATO oFor = new BE_GRAFICA_FORMATO();
                oFor.label = itemE;
                oFor.backgroundColor = paseDatosColores[index];
                oFor.data = new List<int>();

                foreach (var itemM in barra.labels)
                {
                    var cant = (from u in validarData
                                where u.mes == itemM && u.entidad == itemE
                                group u by new { u.mes, u.entidad } into g
                                select g.Sum(x => x.cantidad)).FirstOrDefault();
                    oFor.data.Add(cant);
                }

                barra.datasets.Add(oFor);
                index++;
            }


            var servicios = (from u in validarData
                             group u by u.servicio into g
                             select g.Key).ToList();


            List<Object> toolTip = new List<Object>();

            foreach (var itemM in barra.labels)
            {
                List<object> listTE = new List<object>();
                foreach (var itemE in entidades)
                {
                    List<object> listS = new List<object>();
                    foreach (var itemS in servicios)
                    {
                        var cant = (from u in validarData
                                    where u.mes == itemM &&
                                          u.entidad == itemE &&
                                          u.servicio == itemS
                                    group u by new { u.mes, u.entidad, u.servicio } into g
                                    select g.Sum(x => x.cantidad)).FirstOrDefault();
                        object s = new
                        {
                            servicio = itemS,
                            valor = cant
                        };

                        listS.Add(s);
                    }

                    object enti = new
                    {
                        enti = itemE,
                        servicios = listS
                    };

                    listTE.Add(enti);
                }
                object lim = new
                {
                    mes = itemM,
                    entidades = listTE
                };
                toolTip.Add(lim);
            }


            List<Object> toolTipPorcentaje = new List<Object>();

            foreach (var itemE in entidades)
            {
                var cantEnti = (from u in validarData
                            where u.entidad == itemE 
                            group u by u.entidad  into g
                            select g.Sum(x => x.cantidad)).FirstOrDefault();

                List<object> listS = new List<object>();

                foreach (var itemS in servicios)
                {
                    var cant = (from u in validarData
                                where u.entidad == itemE && u.servicio == itemS
                                group u by new { u.entidad, u.servicio } into g
                                select g.Sum(x => x.cantidad)).FirstOrDefault();

                    var doubleCant = Convert.ToDouble(cant);
                    var doubleCantEnti = Convert.ToDouble(cantEnti);

                    var porcentaje = (doubleCant / doubleCantEnti) * 100;
                    var formart = Convert.ToString(Math.Round(porcentaje, 2)) + " %";

                    object s = new
                    {
                        servicio = itemS,
                        valor = formart
                    };
                    listS.Add(s);
                }

                object e = new
                {
                    enti = itemE,
                    servicios = listS
                };

                toolTipPorcentaje.Add(e);
            }




            //List<Object> toolTip = new List<Object>();
            //List<Object> toolTipPorcentaje = new List<Object>();

            //else
            //{
            //    var datosMeses = (from d in obj.Select().AsEnumerable() select d["mes"].ToString()).Distinct().ToList();
            //    var datosEntidad = (from d in obj.Select().AsEnumerable() select d["entidad"].ToString()).Distinct().ToList();

            //    var index = 0;

            //    List<BE_GRAFICA_FORMATO> objFormato = new List<BE_GRAFICA_FORMATO>();
            //    List<BE_PIE_GRAFICA_FORMATO> objFormatoPie = new List<BE_PIE_GRAFICA_FORMATO>();
            //    List<int> cantidadPie = new List<int>();

            //    foreach (var item in datosEntidad)
            //    {

            //        var cantidadIMEI = (from d in obj.Select().AsEnumerable()
            //                            where d["entidad"].ToString() == item && d["servicio"].ToString() == "IMEI"
            //                            select new
            //                            {
            //                                mes = d["mes"].ToString(),
            //                                cantidad = Convert.ToInt32(d["cantidad"])
            //                            }).ToList();

            //        var cantidadMOVIL = (from d in obj.Select().AsEnumerable()
            //                             where d["entidad"].ToString() == item && d["servicio"].ToString() != "IMEI"
            //                             select new
            //                             {
            //                                 mes = d["mes"].ToString(),
            //                                 cantidad = Convert.ToInt32(d["cantidad"])
            //                             }).ToList();

            //        var cPie = (from d in obj.Select().AsEnumerable()
            //                    where d["entidad"].ToString() == item
            //                    select Convert.ToInt32(d["cantidad"])).Sum();

            //        cantidadPie.Add(cPie);

            //        double porcentajeImei = ((Convert.ToDouble(cantidadIMEI.Select(x => x.cantidad).Sum()) / Convert.ToDouble(cPie)) * 100);
            //        double porcentajeMovil = ((Convert.ToDouble(cantidadMOVIL.Select(x => x.cantidad).Sum()) / Convert.ToDouble(cPie)) * 100);

            //        string formatoPorcentajeImei = Convert.ToString(Math.Round(porcentajeImei, 2)) + " %";
            //        string formatoPorcentajeMovil = Convert.ToString(Math.Round(porcentajeMovil, 2)) + " %";

            //        var toolP = new
            //        {
            //            imei = formatoPorcentajeImei,
            //            movil = formatoPorcentajeMovil
            //        };

            //        toolTipPorcentaje.Add(toolP);

            //        List<Int32> parseImei = new List<Int32>();
            //        List<Int32> parseMovil = new List<Int32>();

            //        foreach (var m in datosMeses)
            //        {
            //            var daI = cantidadIMEI.SingleOrDefault(x => x.mes == m);
            //            var daM = cantidadMOVIL.SingleOrDefault(x => x.mes == m);

            //            parseImei.Add(daI == null ? 0 : daI.cantidad);
            //            parseMovil.Add(daM == null ? 0 : daM.cantidad);
            //        }

            //        var sumatoria = parseImei.Zip(parseMovil, (a, b) => (a + b)).ToList();

            //        BE_GRAFICA_FORMATO oFor = new BE_GRAFICA_FORMATO();
            //        oFor.label = item;
            //        oFor.data = sumatoria;
            //        oFor.backgroundColor = paseDatosColores[index];
            //        objFormato.Add(oFor);
            //        index++;
            //    }

            //    foreach (var item in datosMeses)
            //    {
            //        List<Object> parseMes = new List<Object>();

            //        var cantidadIMEI = (from d in obj.Select().AsEnumerable()
            //                            where d["mes"].ToString() == item && d["servicio"].ToString() == "IMEI"
            //                            select new
            //                            {
            //                                entidad = d["entidad"].ToString(),
            //                                cantidad = Convert.ToInt32(d["cantidad"])
            //                            }).ToList();

            //        var cantidadMOVIL = (from d in obj.Select().AsEnumerable()
            //                             where d["mes"].ToString() == item && d["servicio"].ToString() != "IMEI"
            //                             select new
            //                             {
            //                                 entidad = d["entidad"].ToString(),
            //                                 cantidad = Convert.ToInt32(d["cantidad"])
            //                             }).ToList();

            //        foreach (var e in datosEntidad)
            //        {
            //            var daI = cantidadIMEI.SingleOrDefault(x => x.entidad == e);
            //            var daM = cantidadMOVIL.SingleOrDefault(x => x.entidad == e);

            //            var forTooltip = new
            //            {
            //                imei = daI == null ? 0 : daI.cantidad,
            //                movil = daM == null ? 0 : daM.cantidad
            //            };

            //            parseMes.Add(forTooltip);
            //        }

            //        toolTip.Add(parseMes);
            //    }

            //    barra.labels = datosMeses;
            //    barra.datasets = objFormato;

            //    BE_PIE_GRAFICA_FORMATO oPie = new BE_PIE_GRAFICA_FORMATO();
            //    oPie.label = "DataSets 1";
            //    oPie.data = cantidadPie;
            //    oPie.backgroundColor = paseDatosColores;

            //    objFormatoPie.Add(oPie);

            //    pie.labels = datosEntidad;
            //    pie.datasets = objFormatoPie;
            //}


            var resultado = new { barra, pie, toolTip, toolTipPorcentaje };

            return Json(resultado);
        }

        [HttpPost]
        public ActionResult TablaEntidad(IFormCollection formulario)
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            BE_REPORTE_ENTIDAD objEntidad = new BE_REPORTE_ENTIDAD();
            objEntidad.anio = formulario["anio"];
            objEntidad.requerimiento = formulario["requerimiento"];
            objEntidad.institucion = formulario["institucion"];
            objEntidad.periodo = formulario["periodo"];
            objEntidad.desde = formulario["desde"];
            objEntidad.hasta = formulario["hasta"];

            DataTable obj = objLogica.TablaReporteEntidad(objEntidad);

            List<BE_TABLA_INSTITUCION> dataParse = (from d in obj.Select().AsEnumerable()
                                                    select new BE_TABLA_INSTITUCION
                                                    {
                                                        entidad = d.Table.Columns.Contains("entidad") ? d["entidad"].ToString() : "",
                                                        externa = d.Table.Columns.Contains("externa") ? d["externa"].ToString() : "",
                                                        unidad = d.Table.Columns.Contains("unidad") ? d["unidad"].ToString() : "",
                                                        ruc = d.Table.Columns.Contains("ruc") ? d["ruc"].ToString() : "",
                                                        servicio = d.Table.Columns.Contains("servicio") ? d["servicio"].ToString() : "",
                                                        anio = d.Table.Columns.Contains("anio") ? d["anio"].ToString() : "",
                                                        idMes = d.Table.Columns.Contains("IDMES") ? Convert.ToInt32(d["IDMES"].ToString()) : 0,
                                                        mes = d.Table.Columns.Contains("mes") ? d["mes"].ToString() : "",
                                                        dia = d.Table.Columns.Contains("dia") ? d["dia"].ToString() : "",
                                                        cantidad = d.Table.Columns.Contains("cantidad") ? d["cantidad"].ToString() : ""
                                                    }).ToList();


            List<List<String>> dataSeteada = new List<List<String>>();
            List<string> parseAnios = new List<string>();
            List<string> parseMeses = new List<string>();
            List<List<string>> parseDiasFront = new List<List<string>>();

            if (objEntidad.periodo == "0" && objEntidad.desde == "0" && objEntidad.hasta == "0")
            {
                var parseGroup = dataParse.GroupBy(x => new { x.entidad, x.externa, x.unidad, x.ruc, x.servicio }).Select(x => x.First()).ToList();
                parseAnios = dataParse.Select(x => x.anio).Distinct().ToList();
                var meses = dataParse.OrderBy(x => x.idMes).Select(x => new { x.idMes, x.mes }).Distinct().ToList();
                parseMeses = meses.OrderBy(x => x.idMes).Select(x => x.mes).Distinct().ToList();

                foreach (var item in parseGroup)
                {
                    List<String> nuevaEntidad = new List<String>();
                    List<int> calcularTotal = new List<int>();
                    nuevaEntidad.Add(item.entidad);
                    nuevaEntidad.Add(item.externa);
                    nuevaEntidad.Add(item.unidad);
                    //nuevaEntidad.Add(item.ruc);
                    nuevaEntidad.Add(item.servicio);
                    foreach (var item2 in parseAnios)
                    {
                        var buscarEntidad = dataParse.Where(x => x.entidad == item.entidad
                                                        && x.externa == item.externa
                                                        && x.unidad == item.unidad
                                                        && x.ruc == item.ruc
                                                        && x.servicio == item.servicio
                                                        && x.anio == item2).Select(x => x).ToList();
                        foreach (var item3 in meses)
                        {
                            var mesVal = buscarEntidad.Where(x => x.idMes == item3.idMes).Select(x => x.cantidad == null ? 0 : Convert.ToInt32(x.cantidad)).Sum();
                            nuevaEntidad.Add(mesVal.ToString());
                            calcularTotal.Add(mesVal);
                        }
                    }
                    nuevaEntidad.Add(calcularTotal.Sum().ToString());
                    dataSeteada.Add(nuevaEntidad);
                }
            }
            else
            {
                var parseGroup = dataParse.GroupBy(x => new { x.entidad, x.externa, x.unidad, x.ruc, x.servicio }).Select(x => x.First()).ToList();
                parseAnios = dataParse.Select(x => x.anio).Distinct().ToList();
                var parseMesesDia = dataParse.GroupBy(x => new { x.mes, x.dia }).Select(x => x.First()).ToList();
                parseMeses = parseMesesDia.Select(x => x.mes).Distinct().ToList();
                var parseDias = parseMeses.Count() == 1 ? parseMesesDia.OrderBy(x => x.dia).Select(x => x.dia).ToList() : parseMesesDia.Select(x => x.dia).ToList();

                foreach (var item in parseMeses)
                {
                    var objM = parseMesesDia.OrderBy(x => x.dia).Where(x => x.mes == item).Select(x => x.dia).ToList();
                    parseDiasFront.Add(objM);
                }

                foreach (var item in parseGroup)
                {
                    List<String> nuevaEntidad = new List<String>();
                    List<int> calcularTotal = new List<int>();
                    nuevaEntidad.Add(item.entidad);
                    nuevaEntidad.Add(item.externa);
                    nuevaEntidad.Add(item.unidad);
                    //nuevaEntidad.Add(item.ruc);
                    nuevaEntidad.Add(item.servicio);
                    foreach (var item2 in parseAnios)
                    {
                        for (var i = 0; i < parseMeses.Count(); i++)
                        {
                            var buscarEntidad = dataParse.Where(x => x.entidad == item.entidad
                                                        && x.externa == item.externa
                                                        && x.unidad == item.unidad
                                                        && x.ruc == item.ruc
                                                        && x.servicio == item.servicio
                                                        && x.anio == item2
                                                        && x.mes == parseMeses[i]).Select(x => x).ToList();
                            foreach (var item4 in parseDiasFront[i])
                            {
                                var diaVal = buscarEntidad.Where(x => x.dia == item4).Select(x => x.cantidad == null ? 0 : Convert.ToInt32(x.cantidad)).Sum();
                                nuevaEntidad.Add(diaVal.ToString());
                                calcularTotal.Add(diaVal);
                            }
                        }
                    }
                    nuevaEntidad.Add(calcularTotal.Sum().ToString());
                    dataSeteada.Add(nuevaEntidad);
                }
            }

            var oR = new
            {
                data = dataSeteada,
                dias = parseDiasFront,
                meses = parseMeses,
                anios = parseAnios
            };

            return Json(oR);
        }

        [HttpPost]
        public ActionResult GraficarEmpOpe(IFormCollection formulario)
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            BE_REPORTE_EO objEO = new BE_REPORTE_EO();
            objEO.anio = formulario["anio"];
            objEO.mes = formulario["mes"];
            objEO.eo = formulario["empresa"];
            objEO.desde = formulario["desde"];
            objEO.hasta = formulario["hasta"];
            objEO.requerimiento = formulario["requerimiento"];
            DataTable obj = objLogica.ListarReporteEO(objEO);

            var label = new List<String>()
            {
                "EN PLAZO","FUERA DE PLAZO"
            };

            List<String> paseDatosColores = new List<string>()
                {
                    "#3e95cd",
                    "#8e5ea2",
                    "#3cba9f",
                    "#0000FF",
                    "#8A2BE2",
                    "#A52A2A"
                };

            BE_GRAFICA barraAtendido = new BE_GRAFICA();
            barraAtendido.labels = label;

            BE_GRAFICA barraPendiente = new BE_GRAFICA();
            barraPendiente.labels = label;

            BE_GRAFICA_PIE pieGrafica = new BE_GRAFICA_PIE();

            List<BE_GRAFICA_FORMATO> atendidoFormato = new List<BE_GRAFICA_FORMATO>();
            List<BE_GRAFICA_FORMATO> pendienteFormato = new List<BE_GRAFICA_FORMATO>();
            List<BE_PIE_GRAFICA_FORMATO> objFormatoPie = new List<BE_PIE_GRAFICA_FORMATO>();
            List<int> cantidadPie = new List<int>();

            var empresa = (from d in obj.Select().AsEnumerable() select d["empresa"].ToString()).Distinct().ToList();
            var indexEO = 0;

            if (indexEO > 6)
            {
                indexEO = 0;
            }

            foreach (var item in empresa)
            {
                List<int> cantA = new List<int>();
                List<int> cantP = new List<int>();
                List<int> cantPieA = new List<int>();
                List<int> cantPieP = new List<int>();
                foreach (var itemL in label)
                {
                    var cantidadA = obj.Select().SingleOrDefault(x => x["empresa"].ToString() == item && x["atendido"].ToString() == itemL);
                    var cantidadP = obj.Select().SingleOrDefault(x => x["empresa"].ToString() == item && x["pendiente"].ToString() == itemL);
                    cantA.Add(cantidadA == null ? 0 : Convert.ToInt32(cantidadA["cantatendidos"]));
                    cantP.Add(cantidadP == null ? 0 : Convert.ToInt32(cantidadP["cantpendientes"]));

                    cantPieA.Add(cantidadA == null ? 0 : Convert.ToInt32(cantidadA["cantatendidos"]));
                    cantPieP.Add(cantidadP == null ? 0 : Convert.ToInt32(cantidadP["cantpendientes"]));
                }

                var sumatoria = cantPieA.Sum() + cantPieP.Sum();

                cantidadPie.Add(sumatoria);

                BE_GRAFICA_FORMATO aF = new BE_GRAFICA_FORMATO();
                aF.label = item;
                aF.data = cantA;
                aF.backgroundColor = paseDatosColores[indexEO];
                atendidoFormato.Add(aF);

                BE_GRAFICA_FORMATO pF = new BE_GRAFICA_FORMATO();
                pF.label = item;
                pF.data = cantP;
                pF.backgroundColor = paseDatosColores[indexEO];
                pendienteFormato.Add(pF);
                indexEO++;
            }

            barraAtendido.datasets = atendidoFormato;
            barraPendiente.datasets = pendienteFormato;

            BE_PIE_GRAFICA_FORMATO oPie = new BE_PIE_GRAFICA_FORMATO();
            oPie.label = "DataSets 1";
            oPie.data = cantidadPie;
            oPie.backgroundColor = paseDatosColores;

            objFormatoPie.Add(oPie);

            pieGrafica.labels = empresa;
            pieGrafica.datasets = objFormatoPie;

            var resultado = new { barraAtendido, barraPendiente, pieGrafica };
            return Json(resultado);
        }

        [HttpPost]
        public ActionResult TablaEmpOpe(IFormCollection formulario)
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            BE_REPORTE_EO objEO = new BE_REPORTE_EO();
            objEO.anio = formulario["anio"];
            objEO.mes = formulario["mes"];
            objEO.eo = formulario["empresa"];
            objEO.desde = formulario["desde"];
            objEO.hasta = formulario["hasta"];
            objEO.requerimiento = formulario["requerimiento"];

            DataTable obj = objLogica.TablaReporteEO(objEO);

            List<BE_TABLA_EO> dataParse = new List<BE_TABLA_EO>();
            List<List<String>> dataSeteada = new List<List<String>>();
            List<string> parseAnios = new List<string>();
            List<string> parseMeses = new List<string>();
            List<List<string>> parseDiasFront = new List<List<string>>();

            if (objEO.mes == "0" && objEO.desde == "0" && objEO.hasta == "0")
            {
                dataParse = (from d in obj.Select().AsEnumerable()
                             select new BE_TABLA_EO
                             {
                                 empresa = d["empresa"].ToString(),
                                 atencion = d["atencion"].ToString(),
                                 estado = d["estado"].ToString(),
                                 tiposervicio = d["tiposervicio"].ToString(),
                                 anio = d["anio"].ToString(),
                                 mes = d["mes"].ToString(),
                                 cantidad = d["cantidad"].ToString()
                             }).ToList();

                var parseGroup = dataParse.GroupBy(x => new { x.empresa, x.tiposervicio, x.estado, x.atencion }).Select(x => x.First()).ToList();
                parseAnios = dataParse.Select(x => x.anio).Distinct().ToList();
                parseMeses = dataParse.Select(x => x.mes).Distinct().ToList();

                foreach (var item in parseGroup)
                {
                    List<String> nuevaEntidad = new List<String>();
                    List<int> calcularTotal = new List<int>();
                    nuevaEntidad.Add(item.empresa);
                    nuevaEntidad.Add(item.tiposervicio);
                    nuevaEntidad.Add(item.estado);
                    nuevaEntidad.Add(item.atencion);
                    foreach (var item2 in parseAnios)
                    {
                        var buscarEntidad = dataParse.Where(x => x.empresa == item.empresa
                                                        && x.tiposervicio == item.tiposervicio
                                                        && x.estado == item.estado
                                                        && x.atencion == item.atencion
                                                        && x.anio == item2).Select(x => x).ToList();
                        foreach (var item3 in parseMeses)
                        {
                            var mesVal = buscarEntidad.SingleOrDefault(x => x.mes == item3);

                            nuevaEntidad.Add(mesVal == null ? "0" : mesVal.cantidad);
                            calcularTotal.Add(mesVal == null ? 0 : Convert.ToInt32(mesVal.cantidad));
                        }
                    }
                    nuevaEntidad.Add(calcularTotal.Sum().ToString());
                    dataSeteada.Add(nuevaEntidad);
                }
            }
            else
            {
                dataParse = (from d in obj.Select().AsEnumerable()
                             select new BE_TABLA_EO
                             {
                                 empresa = d["empresa"].ToString(),
                                 atencion = d["atencion"].ToString(),
                                 estado = d["estado"].ToString(),
                                 tiposervicio = d["tiposervicio"].ToString(),
                                 anio = d["anio"].ToString(),
                                 mes = d["mes"].ToString(),
                                 dia = d["dia"].ToString(),
                                 cantidad = d["cantidad"].ToString()
                             }).ToList();

                var parseGroup = dataParse.GroupBy(x => new { x.empresa, x.tiposervicio, x.estado, x.atencion }).Select(x => x.First()).ToList();
                parseAnios = dataParse.Select(x => x.anio).Distinct().ToList();
                var parseMesesDia = dataParse.GroupBy(x => new { x.mes, x.dia }).Select(x => x.First()).ToList();
                parseMeses = parseMesesDia.Select(x => x.mes).Distinct().ToList();
                var parseDias = parseMeses.Count() == 1 ? parseMesesDia.OrderBy(x => x.dia).Select(x => x.dia).ToList() : parseMesesDia.Select(x => x.dia).ToList();

                foreach (var item in parseMeses)
                {
                    var objM = parseMesesDia.OrderBy(x => x.dia).Where(x => x.mes == item).Select(x => x.dia).ToList();
                    parseDiasFront.Add(objM);
                }

                foreach (var item in parseGroup)
                {
                    List<String> nuevaEntidad = new List<String>();
                    List<int> calcularTotal = new List<int>();
                    nuevaEntidad.Add(item.empresa);
                    nuevaEntidad.Add(item.tiposervicio);
                    nuevaEntidad.Add(item.estado);
                    nuevaEntidad.Add(item.atencion);
                    foreach (var item2 in parseAnios)
                    {
                        for (var i = 0; i < parseMeses.Count(); i++)
                        {
                            var buscarEntidad = dataParse.Where(x => x.empresa == item.empresa
                                                        && x.tiposervicio == item.tiposervicio
                                                        && x.estado == item.estado
                                                        && x.atencion == item.atencion
                                                        && x.anio == item2
                                                        && x.mes == parseMeses[i]).Select(x => x).ToList();
                            foreach (var item4 in parseDiasFront[i])
                            {
                                var diaVal = buscarEntidad.SingleOrDefault(x => x.dia == item4);

                                nuevaEntidad.Add(diaVal == null ? "0" : diaVal.cantidad);
                                calcularTotal.Add(diaVal == null ? 0 : Convert.ToInt32(diaVal.cantidad));
                            }
                        }
                    }
                    nuevaEntidad.Add(calcularTotal.Sum().ToString());
                    dataSeteada.Add(nuevaEntidad);
                }
            }

            var oR = new
            {
                data = dataSeteada,
                dias = parseDiasFront,
                meses = parseMeses,
                anios = parseAnios
            };

            return Json(oR);
        }

        [HttpPost]
        public ActionResult ExportarTablaRegio(string anio, string requerimiento, string region, string institucion, string periodo, string desde, string hasta)
        {
            try
            {
                BL_REPORTE objLogica = new BL_REPORTE();
                BE_REPORTE_REGION objRegion = new BE_REPORTE_REGION();
                objRegion.anio = anio;
                objRegion.requerimiento = requerimiento;
                objRegion.region = region;
                objRegion.institucion = institucion;
                objRegion.periodo = periodo;
                objRegion.desde = desde;
                objRegion.hasta = hasta;

                DataTable obj = objLogica.TablaReporteRegion(objRegion);

                List<BE_TABLA_REGION> dataParse = (from d in obj.Select().AsEnumerable()
                                                   select new BE_TABLA_REGION
                                                   {
                                                       idUbigeo = d.Table.Columns.Contains("idubigeo") ? Convert.ToInt32(d["idubigeo"].ToString()) : 0,
                                                       departamento = d.Table.Columns.Contains("departamento") ? d["departamento"].ToString() : "",
                                                       entidad = d.Table.Columns.Contains("entidad") ? d["entidad"].ToString() : "",
                                                       externa = d.Table.Columns.Contains("externa") ? d["externa"].ToString() : "",
                                                       unidad = d.Table.Columns.Contains("unidad") ? d["unidad"].ToString() : "",
                                                       ruc = d.Table.Columns.Contains("ruc") ? d["ruc"].ToString() : "",
                                                       servicio = d.Table.Columns.Contains("servicio") ? d["servicio"].ToString() : "",
                                                       anio = d.Table.Columns.Contains("anio") ? d["anio"].ToString() : "",
                                                       idMes = d.Table.Columns.Contains("idmes") ? Convert.ToInt32(d["idmes"].ToString()) : 0,
                                                       mes = d.Table.Columns.Contains("mes") ? d["mes"].ToString() : "",
                                                       dia = d.Table.Columns.Contains("dia") ? d["dia"].ToString() : "",
                                                       cantidad = d.Table.Columns.Contains("cantidad") ? Convert.ToInt32(d["cantidad"].ToString()) : 0
                                                   }).ToList();


                var fechas = (from u in dataParse
                              group u by u.anio into a
                              orderby a.Key
                              select new BE_TABLA_FORMATO_FECHA
                              {
                                  anio = a.Key,
                                  meses = (from b in dataParse
                                           where b.anio == a.Key
                                           group b by new { b.idMes, b.mes } into m
                                           orderby m.Key.idMes
                                           select new BE_TABLA_MES_ANIO
                                           {
                                               idMes = m.Key.idMes,
                                               mes = m.Key.mes,
                                               dias = (from c in dataParse
                                                       where c.anio == a.Key && c.idMes == m.Key.idMes
                                                       group c by c.dia into d
                                                       orderby d.Key
                                                       select d.Key).ToList()
                                           }).ToList()
                              }).ToList();


                var validarData = new List<BE_TABLA_REGION>();
                var calculoTotal = new List<BE_TABLA_REGION>();

                if (periodo == "0" && desde == "0" && hasta == "0")
                {
                    validarData = (from u in dataParse
                                   group u by new { u.idUbigeo, u.departamento, u.entidad, u.externa, u.unidad, u.anio, u.idMes, u.mes, u.servicio } into g
                                   orderby g.Key.idUbigeo
                                   select new BE_TABLA_REGION
                                   {
                                       idUbigeo = g.Key.idUbigeo,
                                       departamento = g.Key.departamento,
                                       entidad = g.Key.entidad,
                                       externa = g.Key.externa,
                                       unidad = g.Key.unidad,
                                       anio = g.Key.anio,
                                       idMes = g.Key.idMes,
                                       mes = g.Key.mes,
                                       servicio = g.Key.servicio,
                                       cantidad = g.Sum(x => Convert.ToInt32(x.cantidad))
                                   }).ToList();

                    calculoTotal = (from u in validarData
                                    group u by new { u.idUbigeo, u.departamento, u.entidad, u.externa, u.unidad, u.servicio } into g
                                    orderby g.Key.idUbigeo
                                    select new BE_TABLA_REGION
                                    {
                                        idUbigeo = g.Key.idUbigeo,
                                        departamento = g.Key.departamento,
                                        entidad = g.Key.entidad,
                                        externa = g.Key.externa,
                                        unidad = g.Key.unidad,
                                        meses = new List<int>(),
                                        servicio = g.Key.servicio,
                                        total = g.Sum(x => Convert.ToInt32(x.cantidad))
                                    }).ToList();

                    foreach (var itemCT in calculoTotal)
                    {
                        foreach (var itemM in fechas[0].meses)
                        {
                            var mes = validarData.Where(x => x.idUbigeo == itemCT.idUbigeo &&
                                                             x.departamento == itemCT.departamento &&
                                                             x.entidad == itemCT.entidad &&
                                                             x.externa == itemCT.externa &&
                                                             x.unidad == itemCT.unidad &&
                                                             x.idMes == itemM.idMes &&
                                                             x.servicio == itemCT.servicio)
                                                 .Select(x => Convert.ToInt32(x.cantidad)).FirstOrDefault();
                            itemCT.meses.Add(mes);
                        }
                    }

                }
                else
                {
                    validarData = (from u in dataParse
                                   group u by new { u.idUbigeo, u.departamento, u.entidad, u.externa, u.unidad, u.anio, u.idMes, u.mes, u.dia, u.servicio } into g
                                   orderby g.Key.idUbigeo
                                   select new BE_TABLA_REGION
                                   {
                                       idUbigeo = g.Key.idUbigeo,
                                       departamento = g.Key.departamento,
                                       entidad = g.Key.entidad,
                                       externa = g.Key.externa,
                                       unidad = g.Key.unidad,
                                       anio = g.Key.anio,
                                       idMes = g.Key.idMes,
                                       mes = g.Key.mes,
                                       dia = g.Key.dia,
                                       servicio = g.Key.servicio,
                                       cantidad = g.Sum(x => Convert.ToInt32(x.cantidad))
                                   }).ToList();

                    calculoTotal = (from u in validarData
                                    group u by new { u.idUbigeo, u.departamento, u.entidad, u.externa, u.unidad, u.servicio } into g
                                    orderby g.Key.idUbigeo
                                    select new BE_TABLA_REGION
                                    {
                                        idUbigeo = g.Key.idUbigeo,
                                        departamento = g.Key.departamento,
                                        entidad = g.Key.entidad,
                                        externa = g.Key.externa,
                                        unidad = g.Key.unidad,
                                        servicio = g.Key.servicio,
                                        dias = new List<int>(),
                                        total = g.Sum(x => Convert.ToInt32(x.cantidad))
                                    }).ToList();

                    foreach (var itemCT in calculoTotal)
                    {
                        foreach (var itemA in fechas)
                        {
                            foreach (var itemM in itemA.meses)
                            {
                                foreach (var itemD in itemM.dias)
                                {
                                    var dia = validarData.Where(x => x.idUbigeo == itemCT.idUbigeo &&
                                                             x.departamento == itemCT.departamento &&
                                                             x.entidad == itemCT.entidad &&
                                                             x.externa == itemCT.externa &&
                                                             x.unidad == itemCT.unidad &&
                                                             x.anio == itemA.anio &&
                                                             x.idMes == itemM.idMes &&
                                                             x.dia == itemD &&
                                                             x.servicio == itemCT.servicio)
                                                 .Select(x => Convert.ToInt32(x.cantidad)).FirstOrDefault();
                                    itemCT.dias.Add(dia);
                                }

                            }
                        }
                    }

                }

                var stream = new MemoryStream();
                using (ExcelPackage p = new ExcelPackage(stream))
                {
                    var hoja = p.Workbook.Worksheets.Add("Detalle");

                    if (periodo == "0" && desde == "0" && hasta == "0")
                    {
                        var index = 6;

                        hoja.Cells[1, 1].Value = "REGION";
                        hoja.Cells[1, 2].Value = "ENTIDAD PÚBLICA";
                        hoja.Cells[1, 3].Value = "UNIDAD ENTIDAD PÚBLICA";
                        hoja.Cells[1, 4].Value = "SUB UNIDAD ENTIDAD PÚBLICA";
                        hoja.Cells[1, 5].Value = "TIPO REQUERIMIENTO";
                        hoja.Cells[1, 6].Value = objRegion.anio;

                        foreach (var item in fechas[0].meses)
                        {
                            hoja.Cells[2, index].Value = item.mes;
                            index++;
                        }

                        hoja.Cells[1, index == 6 ? index + 1 : index].Value = "TOTAL";

                        hoja.Cells["A1:A2"].Merge = true;
                        hoja.Cells["B1:B2"].Merge = true;
                        hoja.Cells["C1:C2"].Merge = true;
                        hoja.Cells["D1:D2"].Merge = true;
                        hoja.Cells["E1:E2"].Merge = true;
                        hoja.Cells["F1:" + GetExcelColumnName(index == 6 ? index : index - 1) + "1"].Merge = true;

                        var letraColumna = GetExcelColumnName(index == 6 ? index + 1 : index);
                        hoja.Cells[letraColumna + "1:" + letraColumna + "2"].Merge = true;

                        hoja.Cells["A1:" + letraColumna + "2"].Style.Font.Color.SetColor(Color.White);
                        hoja.Cells["A1:" + letraColumna + "2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hoja.Cells["A1:" + letraColumna + "2"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4472C4"));
                        hoja.Cells["A1:" + letraColumna + "2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["A1:" + letraColumna + "2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        var indexData = 3;
                        var indexMes = 6;

                        foreach (var itemDa in calculoTotal)
                        {
                            hoja.Cells[indexData, 1].Value = itemDa.departamento;
                            hoja.Cells[indexData, 2].Value = itemDa.entidad;
                            hoja.Cells[indexData, 3].Value = itemDa.externa;
                            hoja.Cells[indexData, 4].Value = itemDa.unidad;
                            hoja.Cells[indexData, 5].Value = itemDa.servicio;

                            for (var i = 0; i < itemDa.meses.Count(); i++)
                            {
                                hoja.Cells[indexData, indexMes + i].Value = itemDa.meses[i];
                            }

                            hoja.Cells[indexData, (indexMes + itemDa.meses.Count())].Value = itemDa.total;
                            indexData++;
                        }

                        if (calculoTotal.Count() > 0)
                        {
                            hoja.Cells.AutoFitColumns(0);
                            hoja.Cells["A3:" + letraColumna + "" + indexData].Sort(0);
                        }
                    }
                    else
                    {
                        var index = 6;

                        hoja.Cells[1, 1].Value = "REGION";
                        hoja.Cells[1, 2].Value = "ENTIDAD PÚBLICA";
                        hoja.Cells[1, 3].Value = "UNIDAD ENTIDAD PÚBLICA";
                        hoja.Cells[1, 4].Value = "SUB UNIDAD ENTIDAD PÚBLICA";
                        hoja.Cells[1, 5].Value = "TIPO REQUERIMIENTO";
                        foreach (var item in fechas)
                        {
                            hoja.Cells[1, index].Value = item.anio;
                            for (var i = 0; i < item.meses.Count(); i++)
                            {
                                hoja.Cells[2, index].Value = item.meses[i].mes;
                                foreach (var item2 in item.meses[i].dias)
                                {
                                    hoja.Cells[3, index].Value = item2;
                                    index++;
                                }
                            }
                        }
                        hoja.Cells[1, index == 6 ? index + 1 : index].Value = "TOTAL";

                        hoja.Cells["A1:A3"].Merge = true;
                        hoja.Cells["B1:B3"].Merge = true;
                        hoja.Cells["C1:C3"].Merge = true;
                        hoja.Cells["D1:D3"].Merge = true;
                        hoja.Cells["E1:E3"].Merge = true;

                        var indexMerge = 6;

                        foreach (var item in fechas)
                        {
                            var colM = indexMerge;
                            foreach (var itemM in item.meses)
                            {
                                hoja.Cells[GetExcelColumnName(colM) + "2:" + GetExcelColumnName(colM + itemM.dias.Count() - 1) + "2"].Merge = true;
                                colM += itemM.dias.Count();
                            }
                            hoja.Cells[GetExcelColumnName(indexMerge) + "1:" + GetExcelColumnName(colM - 1) + "1"].Merge = true;
                            indexMerge = colM;
                        }

                        var letraColumna = GetExcelColumnName(index == 6 ? index + 1 : index);
                        hoja.Cells[letraColumna + "1:" + letraColumna + "3"].Merge = true;

                        hoja.Cells["A1:" + letraColumna + "3"].Style.Font.Color.SetColor(Color.White);
                        hoja.Cells["A1:" + letraColumna + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hoja.Cells["A1:" + letraColumna + "3"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4472C4"));
                        hoja.Cells["A1:" + letraColumna + "3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["A1:" + letraColumna + "3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        var indexData = 4;
                        var indexMes = 6;

                        foreach (var itemDa in calculoTotal)
                        {
                            hoja.Cells[indexData, 1].Value = itemDa.departamento;
                            hoja.Cells[indexData, 2].Value = itemDa.entidad;
                            hoja.Cells[indexData, 3].Value = itemDa.externa;
                            hoja.Cells[indexData, 4].Value = itemDa.unidad;
                            hoja.Cells[indexData, 5].Value = itemDa.servicio;

                            for (var i = 0; i < itemDa.dias.Count(); i++)
                            {
                                hoja.Cells[indexData, indexMes + i].Value = itemDa.dias[i];
                            }

                            hoja.Cells[indexData, (indexMes + itemDa.dias.Count())].Value = itemDa.total;
                            indexData++;
                        }

                        if (calculoTotal.Count() > 0)
                        {
                            hoja.Cells.AutoFitColumns(0);
                            hoja.Cells["A4:" + letraColumna + "" + indexData].Sort(0);
                        }
                    }

                    p.Save();


                    string handle = Guid.NewGuid().ToString();
                    stream.Position = 0;
                    //TempData[handle] = stream.ToArray();

                    HttpContext.Session.Set(handle, stream.ToArray());

                    //return File(stream.ToArray(), "application/octet-stream", "Reporte_Region.xlsx");

                    return new JsonResult(new
                    {
                        Data = new { FileGuid = handle, FileName = "Reporte_Region.xlsx" }
                    });

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {

            if (HttpContext.Session.Get(fileGuid) != null)
            {
                try
                {
                    byte[] data = HttpContext.Session.Get(fileGuid) as byte[];
                    return File(data, "application/vnd.ms-excel", fileName);
                }
                catch (Exception ex)
                {
                    return new EmptyResult();
                }
                finally
                {
                    HttpContext.Session.Remove(fileGuid);
                }

            }
            else
            {
                return new EmptyResult();
            }
        }

        [HttpPost]
        public ActionResult ExportarTablaInsSol(string anio, string requerimiento, string institucion, string periodo, string desde, string hasta)
        {
            try
            {
                BL_REPORTE objLogica = new BL_REPORTE();
                BE_REPORTE_ENTIDAD objEntidad = new BE_REPORTE_ENTIDAD();
                objEntidad.anio = anio;
                objEntidad.requerimiento = requerimiento;
                objEntidad.institucion = institucion;
                objEntidad.periodo = periodo;
                objEntidad.desde = desde;
                objEntidad.hasta = hasta;

                DataTable obj = objLogica.TablaReporteEntidad(objEntidad);
                List<BE_TABLA_INSTITUCION> dataParse = new List<BE_TABLA_INSTITUCION>();
                List<List<String>> dataSeteada = new List<List<String>>();
                List<string> parseAnios = new List<string>();
                List<string> parseMeses = new List<string>();
                List<List<string>> parseDiasFront = new List<List<string>>();

                var stream = new MemoryStream();
                using (ExcelPackage p = new ExcelPackage(stream))
                {

                    var hoja = p.Workbook.Worksheets.Add("Detalle");

                    if (periodo == "0" && desde == "0" && hasta == "0")
                    {

                        dataParse = (from d in obj.Select().AsEnumerable()
                                     select new BE_TABLA_INSTITUCION
                                     {
                                         entidad = d["entidad"].ToString(),
                                         externa = d["externa"].ToString(),
                                         unidad = d["unidad"].ToString(),
                                         ruc = d["ruc"].ToString(),
                                         servicio = d["servicio"].ToString(),
                                         anio = d["anio"].ToString(),
                                         mes = d["mes"].ToString(),
                                         cantidad = d["cantidad"].ToString()
                                     }).ToList();

                        var parseGroup = dataParse.GroupBy(x => new { x.entidad, x.externa, x.unidad, x.ruc, x.servicio }).Select(x => x.First()).ToList();
                        parseAnios = dataParse.Select(x => x.anio).Distinct().ToList();
                        parseMeses = dataParse.Select(x => x.mes).Distinct().ToList();

                        foreach (var item in parseGroup)
                        {
                            List<String> nuevaEntidad = new List<String>();
                            List<int> calcularTotal = new List<int>();
                            nuevaEntidad.Add(item.entidad);
                            nuevaEntidad.Add(item.externa);
                            nuevaEntidad.Add(item.unidad);
                            //nuevaEntidad.Add(item.ruc);
                            nuevaEntidad.Add(item.servicio);
                            foreach (var item2 in parseAnios)
                            {
                                var buscarEntidad = dataParse.Where(x => x.entidad == item.entidad
                                                                && x.externa == item.externa
                                                                && x.unidad == item.unidad
                                                                && x.ruc == item.ruc
                                                                && x.servicio == item.servicio
                                                                && x.anio == item2).Select(x => x).ToList();
                                foreach (var item3 in parseMeses)
                                {
                                    var mesVal = buscarEntidad.SingleOrDefault(x => x.mes == item3);

                                    nuevaEntidad.Add(mesVal == null ? "0" : mesVal.cantidad);
                                    calcularTotal.Add(mesVal == null ? 0 : Convert.ToInt32(mesVal.cantidad));
                                }
                            }
                            nuevaEntidad.Add(calcularTotal.Sum().ToString());
                            dataSeteada.Add(nuevaEntidad);
                        }

                        var index = 5;

                        hoja.Cells[1, 1].Value = "ENTIDAD PÚBLICA";
                        hoja.Cells[1, 2].Value = "UNIDAD ENTIDAD PÚBLICA";
                        hoja.Cells[1, 3].Value = "SUB UNIDAD ENTIDAD PÚBLICA";
                        //hoja.Cells[1, 4].Value = "RUC";
                        hoja.Cells[1, 4].Value = "TIPO REQUERIMIENTO";
                        hoja.Cells[1, 5].Value = objEntidad.anio;
                        foreach (var item in parseMeses)
                        {
                            hoja.Cells[2, index].Value = item;
                            index++;
                        }
                        hoja.Cells[1, index == 5 ? index + 1 : index].Value = "TOTAL";

                        hoja.Cells["A1:A2"].Merge = true;
                        hoja.Cells["B1:B2"].Merge = true;
                        hoja.Cells["C1:C2"].Merge = true;
                        //hoja.Cells["D1:D2"].Merge = true;
                        hoja.Cells["D1:D2"].Merge = true;
                        hoja.Cells["E1:" + GetExcelColumnName(index == 5 ? index : index - 1) + "1"].Merge = true;


                        var letraColumna = GetExcelColumnName(index == 5 ? index + 1 : index);
                        hoja.Cells[letraColumna + "1:" + letraColumna + "2"].Merge = true;

                        hoja.Cells["A1:" + letraColumna + "2"].Style.Font.Color.SetColor(Color.White);
                        hoja.Cells["A1:" + letraColumna + "2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hoja.Cells["A1:" + letraColumna + "2"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4472C4"));
                        hoja.Cells["A1:" + letraColumna + "2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["A1:" + letraColumna + "2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        var indexData = 3;

                        foreach (var item in dataSeteada)
                        {
                            for (var i = 0; i < item.Count(); i++)
                            {
                                hoja.Cells[indexData, i + 1].Value = item[i];
                            }
                            indexData++;
                        }

                        if (dataParse.Count() > 0)
                        {
                            hoja.Cells.AutoFitColumns(0);
                            hoja.Cells["A3:" + letraColumna + "" + indexData].Sort(0);
                        }
                    }
                    else
                    {
                        dataParse = (from d in obj.Select().AsEnumerable()
                                     select new BE_TABLA_INSTITUCION
                                     {
                                         entidad = d["entidad"].ToString(),
                                         externa = d["externa"].ToString(),
                                         unidad = d["unidad"].ToString(),
                                         ruc = d["ruc"].ToString(),
                                         servicio = d["servicio"].ToString(),
                                         anio = d["anio"].ToString(),
                                         mes = d["mes"].ToString(),
                                         dia = d["dia"].ToString(),
                                         cantidad = d["cantidad"].ToString()
                                     }).ToList();

                        var parseGroup = dataParse.GroupBy(x => new { x.entidad, x.externa, x.unidad, x.ruc, x.servicio }).Select(x => x.First()).ToList();
                        parseAnios = dataParse.Select(x => x.anio).Distinct().ToList();
                        var parseMesesDia = dataParse.GroupBy(x => new { x.mes, x.dia }).Select(x => x.First()).ToList();
                        parseMeses = parseMesesDia.Select(x => x.mes).Distinct().ToList();
                        var parseDias = parseMeses.Count() == 1 ? parseMesesDia.OrderBy(x => x.dia).Select(x => x.dia).ToList() : parseMesesDia.Select(x => x.dia).ToList();

                        foreach (var item in parseMeses)
                        {
                            var objM = parseMesesDia.OrderBy(x => x.dia).Where(x => x.mes == item).Select(x => x.dia).ToList();
                            parseDiasFront.Add(objM);
                        }

                        foreach (var item in parseGroup)
                        {
                            List<String> nuevaEntidad = new List<String>();
                            List<int> calcularTotal = new List<int>();
                            nuevaEntidad.Add(item.entidad);
                            nuevaEntidad.Add(item.externa);
                            nuevaEntidad.Add(item.unidad);
                            //nuevaEntidad.Add(item.ruc);
                            nuevaEntidad.Add(item.servicio);
                            foreach (var item2 in parseAnios)
                            {
                                for (var i = 0; i < parseMeses.Count(); i++)
                                {
                                    var buscarEntidad = dataParse.Where(x => x.entidad == item.entidad
                                                                && x.externa == item.externa
                                                                && x.unidad == item.unidad
                                                                && x.ruc == item.ruc
                                                                && x.servicio == item.servicio
                                                                && x.anio == item2
                                                                && x.mes == parseMeses[i]).Select(x => x).ToList();
                                    foreach (var item4 in parseDiasFront[i])
                                    {
                                        var diaVal = buscarEntidad.SingleOrDefault(x => x.dia == item4);

                                        nuevaEntidad.Add(diaVal == null ? "0" : diaVal.cantidad);
                                        calcularTotal.Add(diaVal == null ? 0 : Convert.ToInt32(diaVal.cantidad));
                                    }
                                }
                            }
                            nuevaEntidad.Add(calcularTotal.Sum().ToString());
                            dataSeteada.Add(nuevaEntidad);
                        }

                        var index = 5;

                        hoja.Cells[1, 1].Value = "ENTIDAD PÚBLICA";
                        hoja.Cells[1, 2].Value = "UNIDAD ENTIDAD PÚBLICA";
                        hoja.Cells[1, 3].Value = "SUB UNIDAD ENTIDAD PÚBLICA";
                        //hoja.Cells[1, 4].Value = "RUC";
                        hoja.Cells[1, 4].Value = "TIPO REQUERIMIENTO";
                        foreach (var item in parseAnios)
                        {
                            hoja.Cells[1, index].Value = item;
                            for (var i = 0; i < parseMeses.Count(); i++)
                            {
                                hoja.Cells[2, index].Value = parseMeses[i];
                                foreach (var item2 in parseDiasFront[i])
                                {
                                    hoja.Cells[3, index].Value = item2;
                                    index++;
                                }
                            }
                        }
                        hoja.Cells[1, index == 5 ? index + 1 : index].Value = "TOTAL";

                        hoja.Cells["A1:A3"].Merge = true;
                        hoja.Cells["B1:B3"].Merge = true;
                        hoja.Cells["C1:C3"].Merge = true;
                        //hoja.Cells["D1:D3"].Merge = true;
                        hoja.Cells["D1:D3"].Merge = true;
                        hoja.Cells["E1:" + GetExcelColumnName(index == 5 ? index : index - 1) + "1"].Merge = true; //ANIO

                        var cantidadMeses = 0;
                        for (var i = 0; i < parseMeses.Count(); i++)
                        {
                            var ayuda = 5 + cantidadMeses;
                            //hoja.Cells[GetExcelColumnName(6 + cantidadMeses) + "2:" + GetExcelColumnName(5 + parseDiasFront[i].Count()) + "2"].Merge = true;
                            hoja.Cells[GetExcelColumnName(ayuda) + "2:" + GetExcelColumnName((ayuda + parseDiasFront[i].Count()) - 1) + "2"].Merge = true;
                            cantidadMeses += parseDiasFront[i].Count();
                        }

                        var letraColumna = GetExcelColumnName(index == 5 ? index + 1 : index);
                        hoja.Cells[letraColumna + "1:" + letraColumna + "3"].Merge = true;

                        hoja.Cells["A1:" + letraColumna + "3"].Style.Font.Color.SetColor(Color.White);
                        hoja.Cells["A1:" + letraColumna + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hoja.Cells["A1:" + letraColumna + "3"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4472C4"));
                        hoja.Cells["A1:" + letraColumna + "3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["A1:" + letraColumna + "3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        var indexData = 4;

                        foreach (var item in dataSeteada)
                        {
                            for (var i = 0; i < item.Count(); i++)
                            {
                                hoja.Cells[indexData, i + 1].Value = item[i];
                            }
                            indexData++;
                        }

                        if (dataParse.Count() > 0)
                        {
                            hoja.Cells.AutoFitColumns(0);
                            hoja.Cells["A4:" + letraColumna + "" + indexData].Sort(0);
                        }
                    }

                    p.Save();


                    string handle = Guid.NewGuid().ToString();
                    stream.Position = 0;
                    //TempData[handle] = stream.ToArray();

                    HttpContext.Session.Set(handle, stream.ToArray());

                    return new JsonResult(new
                    {
                        Data = new { FileGuid = handle, FileName = "Reporte_Institucion_Solicitante.xlsx" }
                    });

                    //return File(stream.ToArray(), "application/octet-stream", "Reporte_Institucion_Solicitante.xlsx");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult ExportarTablaEmpOpe(string anio, string mes, string empresa, string desde, string hasta, string requerimiento)
        {
            try
            {
                BL_REPORTE objLogica = new BL_REPORTE();
                BE_REPORTE_EO objEO = new BE_REPORTE_EO();
                objEO.anio = anio;
                objEO.mes = mes;
                objEO.eo = empresa;
                objEO.desde = desde;
                objEO.hasta = hasta;
                objEO.requerimiento = requerimiento;

                DataTable obj = objLogica.TablaReporteEO(objEO);

                List<BE_TABLA_EO> dataParse = new List<BE_TABLA_EO>();
                List<List<String>> dataSeteada = new List<List<String>>();
                List<string> parseAnios = new List<string>();
                List<string> parseMeses = new List<string>();
                List<List<string>> parseDiasFront = new List<List<string>>();

                var stream = new MemoryStream();
                using (ExcelPackage p = new ExcelPackage(stream))
                {
                    var hoja = p.Workbook.Worksheets.Add("Detalle");

                    if (objEO.mes == "0" && objEO.desde == "0" && objEO.hasta == "0")
                    {
                        dataParse = (from d in obj.Select().AsEnumerable()
                                     select new BE_TABLA_EO
                                     {
                                         empresa = d["empresa"].ToString(),
                                         atencion = d["atencion"].ToString(),
                                         estado = d["estado"].ToString(),
                                         tiposervicio = d["tiposervicio"].ToString(),
                                         anio = d["anio"].ToString(),
                                         mes = d["mes"].ToString(),
                                         cantidad = d["cantidad"].ToString()
                                     }).ToList();

                        var parseGroup = dataParse.GroupBy(x => new { x.empresa, x.tiposervicio, x.estado, x.atencion }).Select(x => x.First()).ToList();
                        parseAnios = dataParse.Select(x => x.anio).Distinct().ToList();
                        parseMeses = dataParse.Select(x => x.mes).Distinct().ToList();

                        foreach (var item in parseGroup)
                        {
                            List<String> nuevaEntidad = new List<String>();
                            List<int> calcularTotal = new List<int>();
                            nuevaEntidad.Add(item.empresa);
                            nuevaEntidad.Add(item.tiposervicio);
                            nuevaEntidad.Add(item.estado);
                            nuevaEntidad.Add(item.atencion);
                            foreach (var item2 in parseAnios)
                            {
                                var buscarEntidad = dataParse.Where(x => x.empresa == item.empresa
                                                                && x.tiposervicio == item.tiposervicio
                                                                && x.estado == item.estado
                                                                && x.atencion == item.atencion
                                                                && x.anio == item2).Select(x => x).ToList();
                                foreach (var item3 in parseMeses)
                                {
                                    var mesVal = buscarEntidad.SingleOrDefault(x => x.mes == item3);

                                    nuevaEntidad.Add(mesVal == null ? "0" : mesVal.cantidad);
                                    calcularTotal.Add(mesVal == null ? 0 : Convert.ToInt32(mesVal.cantidad));
                                }
                            }
                            nuevaEntidad.Add(calcularTotal.Sum().ToString());
                            dataSeteada.Add(nuevaEntidad);
                        }

                        var index = 5;

                        hoja.Cells[1, 1].Value = "EMPRESA OPERADORA";
                        hoja.Cells[1, 2].Value = "TIPO REQUERIMIENTO";
                        hoja.Cells[1, 3].Value = "ESTADO";
                        hoja.Cells[1, 4].Value = "TIPO DE ESTADO";
                        hoja.Cells[1, 5].Value = objEO.anio;
                        foreach (var item in parseMeses)
                        {
                            hoja.Cells[2, index].Value = item;
                            index++;
                        }
                        hoja.Cells[1, index == 5 ? index + 1 : index].Value = "TOTAL";

                        hoja.Cells["A1:A2"].Merge = true;
                        hoja.Cells["B1:B2"].Merge = true;
                        hoja.Cells["C1:C2"].Merge = true;
                        hoja.Cells["D1:D2"].Merge = true;
                        hoja.Cells["E1:" + GetExcelColumnName(index == 5 ? index : index - 1) + "1"].Merge = true;


                        var letraColumna = GetExcelColumnName(index == 5 ? index + 1 : index);
                        hoja.Cells[letraColumna + "1:" + letraColumna + "2"].Merge = true;

                        hoja.Cells["A1:" + letraColumna + "2"].Style.Font.Color.SetColor(Color.White);
                        hoja.Cells["A1:" + letraColumna + "2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hoja.Cells["A1:" + letraColumna + "2"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4472C4"));
                        hoja.Cells["A1:" + letraColumna + "2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["A1:" + letraColumna + "2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        var indexData = 3;

                        foreach (var item in dataSeteada)
                        {
                            for (var i = 0; i < item.Count(); i++)
                            {
                                hoja.Cells[indexData, i + 1].Value = item[i];
                            }
                            indexData++;
                        }

                        if (dataParse.Count() > 0)
                        {
                            hoja.Cells["A1:" + letraColumna + "" + indexData].AutoFitColumns(0);
                            hoja.Cells["A3:" + letraColumna + "" + indexData].Sort(0);
                        }
                    }
                    else
                    {
                        dataParse = (from d in obj.Select().AsEnumerable()
                                     select new BE_TABLA_EO
                                     {
                                         empresa = d["empresa"].ToString(),
                                         atencion = d["atencion"].ToString(),
                                         estado = d["estado"].ToString(),
                                         tiposervicio = d["tiposervicio"].ToString(),
                                         anio = d["anio"].ToString(),
                                         mes = d["mes"].ToString(),
                                         dia = d["dia"].ToString(),
                                         cantidad = d["cantidad"].ToString()
                                     }).ToList();

                        var parseGroup = dataParse.GroupBy(x => new { x.empresa, x.tiposervicio, x.estado, x.atencion }).Select(x => x.First()).ToList();
                        parseAnios = dataParse.Select(x => x.anio).Distinct().ToList();
                        var parseMesesDia = dataParse.GroupBy(x => new { x.mes, x.dia }).Select(x => x.First()).ToList();
                        parseMeses = parseMesesDia.Select(x => x.mes).Distinct().ToList();
                        var parseDias = parseMeses.Count() == 1 ? parseMesesDia.OrderBy(x => x.dia).Select(x => x.dia).ToList() : parseMesesDia.Select(x => x.dia).ToList();

                        foreach (var item in parseMeses)
                        {
                            var objM = parseMesesDia.OrderBy(x => x.dia).Where(x => x.mes == item).Select(x => x.dia).ToList();
                            parseDiasFront.Add(objM);
                        }

                        foreach (var item in parseGroup)
                        {
                            List<String> nuevaEntidad = new List<String>();
                            List<int> calcularTotal = new List<int>();
                            nuevaEntidad.Add(item.empresa);
                            nuevaEntidad.Add(item.tiposervicio);
                            nuevaEntidad.Add(item.estado);
                            nuevaEntidad.Add(item.atencion);
                            foreach (var item2 in parseAnios)
                            {
                                for (var i = 0; i < parseMeses.Count(); i++)
                                {
                                    var buscarEntidad = dataParse.Where(x => x.empresa == item.empresa
                                                                && x.tiposervicio == item.tiposervicio
                                                                && x.estado == item.estado
                                                                && x.atencion == item.atencion
                                                                && x.anio == item2
                                                                && x.mes == parseMeses[i]).Select(x => x).ToList();
                                    foreach (var item4 in parseDiasFront[i])
                                    {
                                        var diaVal = buscarEntidad.SingleOrDefault(x => x.dia == item4);

                                        nuevaEntidad.Add(diaVal == null ? "0" : diaVal.cantidad);
                                        calcularTotal.Add(diaVal == null ? 0 : Convert.ToInt32(diaVal.cantidad));
                                    }
                                }
                            }
                            nuevaEntidad.Add(calcularTotal.Sum().ToString());
                            dataSeteada.Add(nuevaEntidad);
                        }

                        var index = 5;

                        hoja.Cells[1, 1].Value = "EMPRESA OPERADORA";
                        hoja.Cells[1, 2].Value = "TIPO REQUERIMIENTO";
                        hoja.Cells[1, 3].Value = "ESTADO";
                        hoja.Cells[1, 4].Value = "TIPO DE ESTADO";
                        foreach (var item in parseAnios)
                        {
                            hoja.Cells[1, index].Value = item;
                            for (var i = 0; i < parseMeses.Count(); i++)
                            {
                                hoja.Cells[2, index].Value = parseMeses[i];
                                foreach (var item2 in parseDiasFront[i])
                                {
                                    hoja.Cells[3, index].Value = item2;
                                    index++;
                                }
                            }
                        }
                        hoja.Cells[1, index == 5 ? index + 1 : index].Value = "TOTAL";

                        hoja.Cells["A1:A3"].Merge = true;
                        hoja.Cells["B1:B3"].Merge = true;
                        hoja.Cells["C1:C3"].Merge = true;
                        hoja.Cells["D1:D3"].Merge = true;
                        hoja.Cells["E1:" + GetExcelColumnName(index == 5 ? index : index - 1) + "1"].Merge = true; //ANIO

                        var cantidadMeses = 0;
                        for (var i = 0; i < parseMeses.Count(); i++)
                        {
                            var ayuda = 5 + cantidadMeses;
                            //hoja.Cells[GetExcelColumnName(5 + cantidadMeses) + "2:" + GetExcelColumnName(4 + parseDiasFront[i].Count()) + "2"].Merge = true;
                            hoja.Cells[GetExcelColumnName(ayuda) + "2:" + GetExcelColumnName((ayuda + parseDiasFront[i].Count()) - 1) + "2"].Merge = true;
                            cantidadMeses += parseDiasFront[i].Count();
                        }

                        var letraColumna = GetExcelColumnName(index == 5 ? index + 1 : index);
                        hoja.Cells[letraColumna + "1:" + letraColumna + "3"].Merge = true;

                        hoja.Cells["A1:" + letraColumna + "3"].Style.Font.Color.SetColor(Color.White);
                        hoja.Cells["A1:" + letraColumna + "3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hoja.Cells["A1:" + letraColumna + "3"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4472C4"));
                        hoja.Cells["A1:" + letraColumna + "3"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        hoja.Cells["A1:" + letraColumna + "3"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        var indexData = 4;

                        foreach (var item in dataSeteada)
                        {
                            for (var i = 0; i < item.Count(); i++)
                            {
                                hoja.Cells[indexData, i + 1].Value = item[i];
                            }
                            indexData++;
                        }

                        if (dataParse.Count() > 0)
                        {
                            hoja.Cells["A1:" + letraColumna + "" + indexData].AutoFitColumns(0);
                            hoja.Cells["A4:" + letraColumna + "" + indexData].Sort(0);
                        }
                    }

                    p.Save();

                    string handle = Guid.NewGuid().ToString();
                    stream.Position = 0;
                    //TempData[handle] = stream.ToArray();

                    HttpContext.Session.Set(handle, stream.ToArray());

                    return new JsonResult(new
                    {
                        Data = new { FileGuid = handle, FileName = "Reporte_Empresa_Operadora.xlsx" }
                    });


                    //return File(stream.ToArray(), "application/octet-stream", "Reporte_Empresa_Operadora.xlsx");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public ActionResult BuscarInformacion(IFormCollection formulario)
        {
            var anio = formulario["ddlAnio"];
            var tipoReporte = formulario["ddlTipoReporte"];
            BE_REPORTE_IMEI objReporte = new BE_REPORTE_IMEI();
            BL_REPORTE objLogica = new BL_REPORTE();
            objReporte.idAnio = anio == "" ? 0 : Convert.ToInt32(anio);
            List<BE_REPORTE_IMEI> lista = new List<BE_REPORTE_IMEI>();

            String partial = "";
            if (tipoReporte == "1")
            {
                lista = objLogica.ListaReporteEntidad(objReporte);
                ViewBag.TipoReporte = "Institución Solicitante";
                partial = "_DetalleIndex";
            }
            else if (tipoReporte == "2")
            {
                lista = objLogica.ListaReporteDepartamento(objReporte);
                ViewBag.TipoReporte = "Región";
                partial = "_DetalleDepartamento";
            }
            else if (tipoReporte == "3")
            {
                lista = objLogica.ListaReporteEmpresaOperadora(objReporte);
                ViewBag.TipoReporte = "Empresa Operadora";
                partial = "_DetalleIndex";
            }

            if (lista != null)
            {
                if (lista.Count > 0)
                {
                    BE_REPORTE_IMEI objTotalFila = new BE_REPORTE_IMEI();
                    decimal totalEnero = 0, totalFebrero = 0, totalMarzo = 0, totalAbril = 0, totalMayo = 0, totalJunio = 0,
                        totalJulio = 0, totalAgosto = 0, totalSetiembre = 0, totalOctubre = 0, totalNoviembre = 0, totalDiciembre = 0;

                    foreach (var item in lista)
                    {
                        totalEnero = totalEnero + item.enero;
                        totalFebrero = totalFebrero + item.febrero;
                        totalMarzo = totalMarzo + item.marzo;
                        totalAbril = totalAbril + item.abril;
                        totalMayo = totalMayo + item.mayo;
                        totalJunio = totalJunio + item.junio;
                        totalJulio = totalJulio + item.julio;
                        totalAgosto = totalAgosto + item.agosto;
                        totalSetiembre = totalSetiembre + item.setiembre;
                        totalOctubre = totalOctubre + item.octubre;
                        totalNoviembre = totalNoviembre + item.noviembre;
                        totalDiciembre = totalDiciembre + item.diciembre;
                    }
                    objTotalFila.descripcion = "Total";
                    objTotalFila.enero = totalEnero;
                    objTotalFila.febrero = totalFebrero;
                    objTotalFila.marzo = totalMarzo;
                    objTotalFila.abril = totalAbril;
                    objTotalFila.mayo = totalMayo;
                    objTotalFila.junio = totalJunio;
                    objTotalFila.julio = totalJulio;
                    objTotalFila.agosto = totalAgosto;
                    objTotalFila.setiembre = totalSetiembre;
                    objTotalFila.octubre = totalOctubre;
                    objTotalFila.noviembre = totalNoviembre;
                    objTotalFila.diciembre = totalDiciembre;
                    lista.Add(objTotalFila);
                }
            }

            return PartialView(partial, lista);
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

    }
}