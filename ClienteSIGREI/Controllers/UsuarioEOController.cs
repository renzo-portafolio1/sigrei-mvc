﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NETCore.Encrypt;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class UsuarioEOController : Controller
    {
        public IActionResult Index()
        {

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("T");


            ViewBag.Listado_Estado = listaEstado;
            BL_EMPRESA ObjLogica = new BL_EMPRESA();

            List<BE_EMPRESA> listaEmpresa = ObjLogica.Listar(0);

            ViewBag.Lista_EmpresaEO = listaEmpresa;
            //var group = _groupsService.Update(model.ToServiceModel());
            //return NotFound();

            return View();
        }

        [HttpPost]
        public ActionResult BandejaInicial(IFormCollection formulario)
        {
            List<UsuarioEOBE> lista = new List<UsuarioEOBE>();

            return PartialView("_DetalleBandeja", lista);
        }

        [HttpPost]
        public ActionResult BuscarUsuariosEO(IFormCollection formulario)
        {
            UsuarioEOBL UsuEOBL = new UsuarioEOBL();
            List<UsuarioEOBE> lista = new List<UsuarioEOBE>();

            UsuarioEOBE objUsuarioEOBE = new UsuarioEOBE();
            objUsuarioEOBE = obtenerCriterios(formulario);

            lista = UsuEOBL.SP_CONSULTAR_TICKET_EP(objUsuarioEOBE);

            return PartialView("_DetalleBandeja", lista);
        }

        [HttpPost]
        public ActionResult NuevoUsuario(IFormCollection formulario)
        {

            UsuarioEOBE objUsuarioEO = new UsuarioEOBE();

            BL_USUARIOGPSU ObjLogicaUsuario = new BL_USUARIOGPSU();
            BL_EMPRESA ObjLogica = new BL_EMPRESA();

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("S");
            ViewBag.Lista_Estado = listaEstado;

            List<PerfilBE> listaPerfil = new List<PerfilBE>();
            listaPerfil = new UsuarioEOBL().SP_LISTAR_PERFIL("S", "PERFIL_EO");

            ViewBag.Lista_Perfil = listaPerfil;

            List<SexoBE> listaSexo = new GenericoBL().LISTAR_SEXOS("S");
            ViewBag.Lista_Sexo = listaSexo;

            List<TipoDocumentoBE> listaTipoDocumento = new GenericoBL().LISTAR_TIPODOCUMENTOS("S");
            ViewBag.Lista_Documento = listaTipoDocumento;

            List<BE_EMPRESA> lista = ObjLogica.Listar(0);
            ViewBag.Lista_EmpresaEO = lista;


            return PartialView("_NuevoUsuario", objUsuarioEO);
        }

        [HttpPost]
        public ActionResult EditarUsuario(IFormCollection formulario)
        {

            UsuarioEOBE objUsuarioEO = new UsuarioEOBE();

            BL_USUARIOGPSU ObjLogicaUsuario = new BL_USUARIOGPSU();
            BL_EMPRESA ObjLogica = new BL_EMPRESA();

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("S");
            ViewBag.Lista_Estado = listaEstado;

            objUsuarioEO.IDUSUARIO = formulario["hiidUsuEO"];
            UsuarioEOBL objLogicaUsuarioEO = new UsuarioEOBL();
            objUsuarioEO = objLogicaUsuarioEO.SP_OBTENER_USUARIO_EO(objUsuarioEO);


            List<PerfilBE> listaPerfil = new List<PerfilBE>();
            listaPerfil = new UsuarioEOBL().SP_LISTAR_PERFIL("S", "PERFIL_EO");

            ViewBag.Lista_Perfil = listaPerfil;

            List<SexoBE> listaSexo = new GenericoBL().LISTAR_SEXOS("S");
            ViewBag.Lista_Sexo = listaSexo;

            List<TipoDocumentoBE> listaTipoDocumento = new GenericoBL().LISTAR_TIPODOCUMENTOS("S");
            ViewBag.Lista_Documento = listaTipoDocumento;

            List<BE_EMPRESA> lista = ObjLogica.Listar(0);
            ViewBag.Lista_EmpresaEO = lista;


            return PartialView("_EditarUsuario", objUsuarioEO);
        }


        [HttpPost]
        public ActionResult CambiarClaveUsuario(IFormCollection formulario)
        {
            UsuarioEOBE objUsuarioEO = new UsuarioEOBE();
            objUsuarioEO.IDUSUARIO = formulario["hiidUsuEO"];
            UsuarioEOBL objLogicaUsuarioEO = new UsuarioEOBL();
            objUsuarioEO = objLogicaUsuarioEO.SP_OBTENER_USUARIO_EO(objUsuarioEO);

            return PartialView("_CambioClave", objUsuarioEO);
        }



        [HttpPost]
        public ActionResult CambiarEstado(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                UsuarioEOBE objParametro = new UsuarioEOBE();
                objParametro.IDUSUARIO = formulario["hiidUsuEO"];
                objParametro.USUCRE = HttpContext.Session.GetString("IdUsuario");
                objParametro.IDESTADO = formulario["hiidEstado"];

                UsuarioEOBL objUsuEOBL = new UsuarioEOBL();
                String resultado = objUsuEOBL.SP_CAMBIAR_ESTADO_USUARIO_EO(objParametro);

                if (resultado.Equals("1"))
                {
                    result = new KeyValuePair<string, string>("1", "¡Se actualizó correctamente el estado del usuario operador!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al actualizar la información ingresada, consulte con el administrador del sistema");
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "UsuarioEO==>CambiarEstado");
                result = new KeyValuePair<string, string>("0", "Error al intentar cambiar de estado");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult GrabarUsuarioEO(IFormCollection formulario, UsuarioEOBE usuarioEOBE)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                Boolean existeUsuario = ValidarUsuario(usuarioEOBE);
                if (!existeUsuario)
                {
                    usuarioEOBE.IDESTADO = formulario["ddlEstadoNuevo"] == "" ? "0" : formulario["ddlEstadoNuevo"].ToString();
                    usuarioEOBE.IDPERFIL = formulario["ddlPerfil"] == "" ? "0" : formulario["ddlPerfil"].ToString();
                    usuarioEOBE.SEXO = formulario["ddlSexo"] == "" ? "0" : formulario["ddlSexo"].ToString();
                    usuarioEOBE.TIPODOC = formulario["ddlTipoDocumento"] == "" ? "0" : formulario["ddlTipoDocumento"].ToString();
                    usuarioEOBE.IDEMPRESA = formulario["ddlEmpresaNuevo"] == "" ? "0" : formulario["ddlEmpresaNuevo"].ToString();
                    usuarioEOBE.CLAVE = EncryptProvider.Sha1(usuarioEOBE.CLAVE);
                    usuarioEOBE.USUCRE = HttpContext.Session.GetString("IdUsuario");

                    UsuarioEOBL objUsuaroLogica = new UsuarioEOBL();
                    try
                    {
                        String resultado = objUsuaroLogica.SP_INSERTAR_USUARIO_EO(usuarioEOBE);
                        if (resultado.Equals("1"))
                        {
                            result = new KeyValuePair<string, string>("1", "¡Se ha registrar el usuario operador correctamente!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al registrar el usuario operador, consulte con el administrador del sistema");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al registrar el usuario operador, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("2", "Ya existe un usuario con ese nombre, ingrese uno diferente");
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrio un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ActualizarUsuarioEO(IFormCollection formulario, UsuarioEOBE usuarioEOBE)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                var idUsuario = formulario["hiidUsuEO"].ToString();
                usuarioEOBE.IDUSUARIO = idUsuario;               
                usuarioEOBE.USUCRE = HttpContext.Session.GetString("IdUsuario");

                UsuarioEOBL objUsuaroLogica = new UsuarioEOBL();
                try
                {
                    String resultado = objUsuaroLogica.SP_ACTUALIZAR_USUARIO_EO(usuarioEOBE);
                    if (resultado.Equals("1"))
                    {
                        result = new KeyValuePair<string, string>("1", "¡Se ha actualizado el usuario operador correctamente!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al actualizar al usuario operador, consulte con el administrador del sistema");
                    }
                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al actualizar el usuario operador, consulte con el administrador del sistema");
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrio un error al obtener la información del usuario operador");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ActualizarClaveUsuarioEO(IFormCollection formulario, UsuarioEOBE usuarioEOBE)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {                
                usuarioEOBE.CLAVE = EncryptProvider.Sha1(usuarioEOBE.CLAVE);

                var idUsuario = formulario["hiidUsuEO"].ToString();
                usuarioEOBE.IDUSUARIO = idUsuario;
                usuarioEOBE.USUCRE = HttpContext.Session.GetString("IdUsuario");

                UsuarioEOBL objUsuaroLogica = new UsuarioEOBL();
                try
                {
                    String resultado = objUsuaroLogica.SP_CAMBIAR_CLAVE_USUARIO_EO(usuarioEOBE);
                    if (resultado.Equals("1"))
                    {
                        result = new KeyValuePair<string, string>("1", "¡Se actualizó correctamente la clave!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al actualizar la clave del usuario operador, consulte con el administrador del sistema");
                    }
                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al actualizar el usuario operador, consulte con el administrador del sistema");
                }

            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrio un error al obtener la información del usuario operador");
            }
            return Json(result);
        }



        private Boolean ValidarUsuario(UsuarioEOBE objParametro)
        {
            Boolean existeUsuario = false;

            objParametro.USUCRE = HttpContext.Session.GetString("IdUsuario");

            var claveSha = EncryptProvider.Sha1(objParametro.CLAVE);
            objParametro.CLAVE = claveSha;
     
            UsuarioEOBL objUsuaroLogica = new UsuarioEOBL();

            String resultado = objUsuaroLogica.SP_VALIDAR_USUARIO_EO(objParametro, "T");

            if (resultado.Equals("0"))
            {
                existeUsuario = false;
            }
            else
            {
                existeUsuario = true;
            }
            return existeUsuario;
        }

        public UsuarioEOBE obtenerCriterios(IFormCollection formulario)
        {
            UsuarioEOBE objParametro = new UsuarioEOBE();
            objParametro.IDEMPRESA = "T";
            objParametro.IDUSUARIO = "";
            objParametro.NOMBRE = "";
            objParametro.IDESTADO = "T";
            objParametro.IDEMPRESA = formulario["ddlEmpresa"] == "" ? "0" : Convert.ToString(formulario["ddlEmpresa"]);
            objParametro.IDUSUARIO = formulario["txtUsuarioBusqueda"];
            objParametro.NOMBRE = formulario["txtNombres"];
            objParametro.Pagina = 1;
            objParametro.RegistrosxPagina = 1000;
            objParametro.Orden = "1 DESC";

            objParametro.IDESTADO = formulario["ddlEstado"] == "" ? "T" : Convert.ToString(formulario["ddlEstado"]);


            return objParametro;
        }



    }
}