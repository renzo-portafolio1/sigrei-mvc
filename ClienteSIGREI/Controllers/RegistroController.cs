﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.Data;


using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using System.IO;
using ExcelDataReader;
using OfficeOpenXml;
using Newtonsoft.Json;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using ClienteSIGREI.Util;


namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class RegistroController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment hosting;
        public RegistroController(IConfiguration config, IHostingEnvironment host)
        {
            this.configuration = config;
            this.hosting = host;
        }
        // GET: Registro
        public ActionResult Index()
        {
            BL_UBIGEO ObjLogica = new BL_UBIGEO();
            List<BE_UBIGEO> llstConsultaBE = new List<BE_UBIGEO>();
            ViewBag.Lista_Departamento =  ObjLogica.Listar("");

            ViewBag.Arroba = "@";
            ViewBag.TipoUsuario = HttpContext.Session.GetString("idTipoSolicitante");
            ViewBag.DescTipoUsuario = HttpContext.Session.GetString("descTipoSolicitante");

            return View();
        }

        public FileResult DescargarPlantillaImeiFisico()
        {

            var rutaDocumentosPlantilla = configuration["PlantillasSIGREI"];

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaDocumentosPlantilla + "\\PlantillaIMEIsFisico.xlsx");
            string fileName = "PlantillaIMEIsFisico.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        public ActionResult RecargarPaginaRegistro()
        {

            return   RedirectToAction("Index","Registro");
        }

        public FileResult DescargarError_Imei()
        {
            var rutaDocumentosPlantilla = configuration["CarpetaTemporal"];

            var archivoError=   Path.Combine(rutaDocumentosPlantilla, "Error.txt");

            if (System.IO.File.Exists(archivoError))
            {
                System.IO.File.Delete(archivoError);
            }
            List<ErrorImei> listaError = new List<ErrorImei>();

            listaError = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("ListaErroresCargaPublico"));          

            List<ErrorImei> listaErrorErrores = new List<ErrorImei>();
            var listaErrror= JsonConvert.SerializeObject(listaErrorErrores);
            HttpContext.Session.SetString("ListaErroresCargaPublico", listaErrror);

            if (listaError != null)
            {
                if (listaError.Count > 0)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(archivoError, true))
                    {
                        file.WriteLine("------ERRORES ENCONTRADOS EN EL DOCUMENTO CARGADO----");
                        foreach (var item in listaError)
                        {
                            file.WriteLine(item.nroImei + ": Error==>" + item.codError);
                        }
                        file.Dispose();
                    }                   
                }
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoError);
            string fileName = "Error.txt";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);           
        }

        private Boolean validarDocumentoExcel(List<BE_REQUERIMIENTO_IMEI> listaIMEI)
        {
            Boolean ArchivoConError = false;

            List<ErrorImei> listaErrors = new List<ErrorImei>();
            String error = "";
            ErrorImei objError = new ErrorImei();

            BE_REQUERIMIENTO_IMEI objParametro = new BE_REQUERIMIENTO_IMEI();
            BE_REQUERIMIENTO_IMEI objParametroDos = new BE_REQUERIMIENTO_IMEI();


            for (int i = 0; i < listaIMEI.Count; i++)
            {

                if (!listaIMEI[i].isRevisado)
                {
                    objParametro = new BE_REQUERIMIENTO_IMEI();
                    objParametroDos = new BE_REQUERIMIENTO_IMEI();
                    objError = new ErrorImei();

                    objParametro = listaIMEI[i];
                    error = "";
                    if (objParametro.nroImei.Length == 0)
                    {
                        error += "no tiene número de IMEI";
                        error += ", ";
                    }
                    if (objParametro.nroImei.Length != 15)
                    {
                        error += "el número de IMEI no tiene 15 digitos";
                        error += ", ";
                    }
                    long numImeiCon = 0;
                    bool isNumero = long.TryParse(objParametro.nroImei, out numImeiCon);
                    if (!isNumero)
                    {
                        error += "El número de Imei contiene caracteres que no es numerico";
                        error += ", ";
                    }
                    objParametro.isRevisado = true;
                    listaIMEI[i] = objParametro;

                    objParametroDos = listaIMEI.Find(x => x.filaImei == objParametro.filaImei && x.isRevisado == false);
                    if (objParametroDos != null)
                    {
                        int indice = listaIMEI.IndexOf(objParametroDos);
                        if (objParametroDos.nroImei.Length == 0)
                        {
                            error += "no tiene número de IMEI 2";
                            error += ", ";
                        }
                        if (objParametroDos.nroImei.Length != 15)
                        {
                            error += "el número de IMEI 2 no tiene 15 digitos";
                            error += ", ";
                        }
                        long numImeiConDos = 0;
                        bool isNumeroDos = long.TryParse(objParametroDos.nroImei, out numImeiConDos);
                        if (!isNumeroDos)
                        {
                            error += "El número de Imei 2 contiene caracteres que no es numerico";
                            error += ", ";
                        }
                        objParametroDos.isRevisado = true;
                        listaIMEI[indice] = objParametroDos;
                    }


                    if (error != "")
                    {
                        error = error.Substring(0, error.Length - 2);
                        objError.codError = String.Format("La fila {0} tiene el siguiente error: ", objParametro.nroItem) + error;
                        listaErrors.Add(objError);

                    }
                }
            }
            if (listaErrors != null)
            {
                if (listaErrors.Count > 0)
                {
                    //listaErrorCargaJSON = "";
                    var listaErrorCargaJSONFinal = JsonConvert.SerializeObject(listaErrors);
                    //  Session["ListaErroresCargaPublico"] = listaErrorCargaJSON;
                    HttpContext.Session.SetString("ListaErroresCargaPublico", listaErrorCargaJSONFinal);
                    ArchivoConError = true;
                }
                else
                {
                    ArchivoConError = false;
                }
            }
            else
            {
                ArchivoConError = false;
            }
            return ArchivoConError;
        }

        private static DataTable GetDataTableFromExcel(string path, bool hasHeader = true)
        {
            using (var pck = new OfficeOpenXml.ExcelPackage())
            {
                using (var stream = System.IO.File.OpenRead(path))
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                DataTable tbl = new DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = hasHeader ? 2 : 1;
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }
        }


        private KeyValuePair<string, string> Grabar(IFormCollection formulario)
        {
            String mensaje = "";
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();
            Boolean archivoConError = false;
            Boolean existeRegistro = false;
            Boolean continuarRegistro = false;
            var rutaDocumentosPlantilla = configuration["PlantillasSIGREI"];

            string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileArchivoImei"].FileName);

            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            //if (archivo.ContentLength / 1024 < 2000)//2MB
            IFormFile archivo = formulario.Files["fileArchivoImei"];
            if (archivo.Length / 1024 < TamFileGrande)//2MB
            {
                if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                {
                    continuarRegistro = true;
                }
                else
                {
                    mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                    keyValueError = new KeyValuePair<string, string>("0", "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.");                 
                }
            }
            else
            {             
                keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                
            }

            if (keyValueError.Key != "")
            {
                List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
                BE_REQUERIMIENTO objParametroDos = new BE_REQUERIMIENTO();

                #region "Copiar a una carpeta temporal para revisar si hay registro que registrar"


                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                IFormFile fileArchivoImei = formulario.Files["fileArchivoImei"];                
                var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                string nombreArchivoImei = Path.GetFileName(formulario.Files["fileArchivoImei"].FileName);
                
                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                var ubicacionArchivoTemporal = "";

                if (fileArchivoImei.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileArchivoImei.CopyTo(fileStream);
                    }
                    ubicacionArchivoTemporal = filePath;
                }
                FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);

                using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                {
                    try
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets["IMEI"];
                        int totalRows = workSheet.Dimension.Rows;
                        for (int i = 3; i <= totalRows; i++)
                        {
                            objParametroDos = new BE_REQUERIMIENTO();
                            objParametroDos.nroImei = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim() : "";
                            objParametroDos.nroImeiDos = workSheet.Cells[i, 2].Value != null ? workSheet.Cells[i, 2].Value.ToString().Trim().ToLower() : "";
                            listaResultado.Add(objParametroDos);
                        }
                    }
                    catch (Exception)
                    {
                        return keyValueError = new KeyValuePair<string, string>("0", UTConstantes.PLANTILLA_EXCEL_NO_EXISTE_HOJA);
                    }
                }

                #endregion

                if (listaResultado == null)
                {
                    existeRegistro = false;
                }
                else if (listaResultado.Count == 0)
                {
                    existeRegistro = false;
                }
                else
                {
                    existeRegistro = true;
                }
                if (existeRegistro)
                {
                    String nroImei2 = "";
                    //ErrorImei objImei = new ErrorImei();
                    BE_REQUERIMIENTO_IMEI objSolicitudIMEI = new BE_REQUERIMIENTO_IMEI();
                    List<BE_REQUERIMIENTO_IMEI> listaIMEI = new List<BE_REQUERIMIENTO_IMEI>();
                    int num = 2;
                    for (int i = 0; i < listaResultado.Count; i++)
                    {

                        nroImei2 = "";
                        objSolicitudIMEI = new BE_REQUERIMIENTO_IMEI();
                        objSolicitudIMEI.nroItem = num + 1;
                        objSolicitudIMEI.nroImei = listaResultado[i].nroImei.ToString();
                        objSolicitudIMEI.usuCre = HttpContext.Session.GetString("IdUsuario"); 
                        objSolicitudIMEI.filaImei = i;
                        listaIMEI.Add(objSolicitudIMEI);

                        nroImei2 = listaResultado[i].nroImeiDos.ToString();

                        if (nroImei2.Length > 0)
                        {
                            objSolicitudIMEI = new BE_REQUERIMIENTO_IMEI();
                            objSolicitudIMEI.nroItem = num + 1;
                            objSolicitudIMEI.nroImei = nroImei2;
                            objSolicitudIMEI.usuCre = HttpContext.Session.GetString("IdUsuario");
                            objSolicitudIMEI.filaImei = i;

                            listaIMEI.Add(objSolicitudIMEI);
                            nroImei2 = "";
                        }
                        num++;
                    }

                    archivoConError = validarDocumentoExcel(listaIMEI);
                    if (System.IO.File.Exists(ubicacionArchivoTemporal))
                    {
                        System.IO.File.Delete(ubicacionArchivoTemporal);
                    }
                    if (!archivoConError)
                    {
                        long idAdjunto = 0;
                        BL_DOCUMENTO_ADJUNTO objLogicaArchivoAdjunto = new BL_DOCUMENTO_ADJUNTO();
                        BE_DOCUMENTO_ADJUNTO objAdjunto = new BE_DOCUMENTO_ADJUNTO();
                        BE_REQUERIMIENTO objParametro = new BE_REQUERIMIENTO();
                        objParametro = new BE_REQUERIMIENTO();
                        idAdjunto = 0;


                        var rutaCarpetaSIGREI = configuration["RutaDocumentos"];
                        nombreArchivoImei = Path.GetFileName(formulario.Files["fileArchivoImei"].FileName);                        
                        nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");                        

                        if (fileArchivoImei.Length > 0)
                        {
                            var filePath = Path.Combine(rutaCarpetaSIGREI, nombreArchivoImei);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                fileArchivoImei.CopyTo(fileStream);
                            }
                            
                            objAdjunto.nombreDocumento = nombreArchivoImei;
                            objAdjunto.descDocumento = "Archivo IMEI ya la fiscalia, policia, ministerio del interior";
                            idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);

                        }
                        objParametro.idAdjuntoIMEI= idAdjunto;

                        idAdjunto = 0;
                        using (BL_REQUERIMIENTO objLogicaSolicitud = new BL_REQUERIMIENTO())
                        {


                            // Obtener el Tipo de Usuario de acuerdo al usuario
                            string idTipoSolicitante = HttpContext.Session.GetString("idTipoSolicitante"); 
                            objParametro.idTipoSolicitante = Convert.ToInt32(idTipoSolicitante);

                            objParametro.nombreInstitucion = formulario["nombreInstitucion"].ToString(); 
                            objParametro.nombreSolicitante = formulario["nombreSolicitante"].ToString();
                            objParametro.cargo = formulario["cargo"].ToString();
                            objParametro.idUbigeo = formulario["ddlUbigeo"].ToString();
                            objParametro.direccion = formulario["direccion"].ToString();
                            objParametro.correo = formulario["correo"].ToString();
                            objParametro.contacto = formulario["contacto"].ToString();
                            objParametro.nroOficio = formulario["nroOficio"].ToString();
                            objParametro.nroDocReferenciado = formulario["nroDocReferenciado"].ToString();
                            objParametro.comentario = formulario["comentario"].ToString();
                            objParametro.usuCre = HttpContext.Session.GetString("IdUsuario");

                            IFormFile fileOficio = formulario.Files["fileOficio"];

                            var nombreArchivoOficio = Path.GetFileName(formulario.Files["fileOficio"].FileName);

                            nombreArchivoOficio = tiempoActual + "" + nombreArchivoOficio.Replace(" ", "");

                            if (fileOficio.Length > 0)
                            {
                                var filePath = Path.Combine(rutaCarpetaSIGREI, nombreArchivoOficio);
                                using (var fileStream = new FileStream(filePath, FileMode.Create))
                                {
                                    fileOficio.CopyTo(fileStream);
                                }                                
                                objAdjunto.nombreDocumento = nombreArchivoOficio;
                                objAdjunto.descDocumento = "Oficio del usuario PNP, Fiscalia ó MinisterioInterior";
                                objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");
                                idAdjunto =objLogicaArchivoAdjunto.Insertar(objAdjunto);
                            }
                            num = 2;
                            listaIMEI = new List<BE_REQUERIMIENTO_IMEI>();
                            for (int i = 0; i < listaResultado.Count; i++)
                            {

                                nroImei2 = "";
                                objSolicitudIMEI = new BE_REQUERIMIENTO_IMEI();
                                objSolicitudIMEI.nroItem = num + 1;
                                objSolicitudIMEI.nroImei = listaResultado[i].nroImei.ToString();
                                objSolicitudIMEI.usuCre = HttpContext.Session.GetString("IdUsuario");
                                objSolicitudIMEI.filaImei = i;
                                listaIMEI.Add(objSolicitudIMEI);

                                nroImei2 = listaResultado[i].nroImeiDos.ToString();

                                if (nroImei2.Length > 0)
                                {
                                    objSolicitudIMEI = new BE_REQUERIMIENTO_IMEI();
                                    objSolicitudIMEI.nroItem = num + 1;
                                    objSolicitudIMEI.nroImei = nroImei2;
                                    objSolicitudIMEI.usuCre = HttpContext.Session.GetString("IdUsuario");
                                    objSolicitudIMEI.filaImei = i;

                                    listaIMEI.Add(objSolicitudIMEI);
                                    nroImei2 = "";
                                }
                                num++;
                            }

                            objParametro.idAdjuntoOficio= idAdjunto;
                            long idResultado = objLogicaSolicitud.Insertar(objParametro);
                            BL_REQUERIMIENTO_IMEI objLogicaIMEI = new BL_REQUERIMIENTO_IMEI();
                            if (listaIMEI != null)
                            {
                                if (listaIMEI.Count > UTConstantes.VALOR_CERO)
                                {
                                    foreach (var item in listaIMEI)
                                    {
                                        item.idRequerimiento = idResultado;
                                        objLogicaIMEI.InsertarImeiExterno(item);
                                    }
                                }
                            }
                        }

                        return keyValueError = new KeyValuePair<string, string>("1", UTConstantes.MSG_REGISTRO_REQUERIMIENTO_OK);                        

                    }
                    else
                    {
                        List<ErrorImei> listaError = new List<ErrorImei>();
                        listaError = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("ListaErroresCargaPublico"));
                        if (listaError == null) listaError = new List<ErrorImei>();

                        if (listaError == null)
                        {
                            listaError = new List<ErrorImei>();
                        }
                        if (listaError.Count > 0)
                        {
                            return keyValueError = new KeyValuePair<string, string>("2", "Se ha encontrado errores en el archivo IMEI");
                        }
                    }
                }
                else
                {
                    return keyValueError = new KeyValuePair<string, string>("0", UTConstantes.MSG_DOCUMENTO_EXCEL_REGISTRO_PUBLICO_VACIO);
                }

            }

            return keyValueError;
        }

        //public static void CerrarConexionExcelReader(IExcelDataReader excelReader)
        //{
        //    excelReader.Close();
        //    GC.Collect();
        //    GC.WaitForPendingFinalizers();
        //    GC.Collect();
        //}
        
        /// <summary>
        /// 0==> Error
        /// 1==> Correo
        /// 2==> Descargar Error
        /// </summary>
        /// <param name="formulario"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RegistrarRequerimiento(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = Grabar(formulario);
                //return Json(new { id = 2, mensaje = "" });
                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "EnviarAtencionIMEI> info");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al registrar la información");
                //return Json(new { id = 2, mensaje = "" });
                return Json(result);
            }
        }
    }
}