﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class AnalizarIMEIController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CargarImei(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_IMEI> listaIMEI = new List<BE_REQUERIMIENTO_IMEI>();
            List<BE_REQUERIMIENTO_IMEI> listaImeitemp = new List<BE_REQUERIMIENTO_IMEI>();
            var imeiSeleccionados = HttpContext.Session.GetString("AnalizReqImei");

            var listaImei = imeiSeleccionados.Split("-");

            BE_REQUERIMIENTO_IMEI obParametro = new BE_REQUERIMIENTO_IMEI();
            try
            {
                if (listaImei != null)
                {
                    if (listaImei.Length > 0)
                    {
                        foreach (var item in listaImei)
                        {
                            obParametro = new BE_REQUERIMIENTO_IMEI();
                            obParametro.idRequerimiento = Int64.Parse(item);
                            obParametro.usuMod = HttpContext.Session.GetString("IdUsuario");

                            using (BL_REQUERIMIENTO_IMEI objLogicaImei = new BL_REQUERIMIENTO_IMEI())
                            {
                                Int64 id = objLogicaImei.ProcesaIMEISAR(obParametro);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarImei==> Procesar SAR");
            }

            try
            {
                if (listaImei != null)
                {
                    if (listaImei.Length > 0)
                    {
                        foreach (var item in listaImei)
                        {
                            obParametro = new BE_REQUERIMIENTO_IMEI();
                            obParametro.idRequerimiento = Int64.Parse(item);
                            listaImeitemp = new List<BE_REQUERIMIENTO_IMEI>();
                            using (BL_REQUERIMIENTO_IMEI objLogicaImei = new BL_REQUERIMIENTO_IMEI())
                            {
                                listaImeitemp = objLogicaImei.ListaIMEIAnalizar(obParametro);
                            }
                            if (listaImeitemp != null)
                            {
                                foreach (var itemDos in listaImeitemp)
                                {
                                    listaIMEI.Add(itemDos);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                listaIMEI = new List<BE_REQUERIMIENTO_IMEI>();
                LogError.RegistrarErrorMetodo(ex, "CargarImei==> Consultar Información SAR");
            }


            return PartialView("_DetalleBandeja", listaIMEI);
        }

        [HttpPost]
        public ActionResult EnviarEmpresaOperadora(IFormCollection formulario)
        {
            var ID = formulario["ihidimei"];

            BE_REQUERIMIENTO_IMEI objBE_REQUERIMIENTO = new BE_REQUERIMIENTO_IMEI();
            BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI();
            Int64 idAsignacion = 0;
            try
            {
                objBE_REQUERIMIENTO.idImei = Convert.ToInt64(ID);
                objBE_REQUERIMIENTO.idEstado = UTConstantes.ID_ESTADO_PORENVIAR;
                objBE_REQUERIMIENTO.usuMod = HttpContext.Session.GetString("IdUsuario");
                objBE_REQUERIMIENTO.idEventoSeleccionado = 0;


                idAsignacion = objLogica.ActualizarEstadoIMEI(objBE_REQUERIMIENTO);
                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                if (idAsignacion > 0)
                {
                    result = new KeyValuePair<string, string>("1", "¡El IMEI se ha marcado para enviar a la empresa operadora!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al enviar a la empresa operadora");
                }
                return Json(result);
            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al intentar marcar para enviar la información a la empresa empresa operadora");

                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult DeshacerAccionPrevia(IFormCollection formulario)
        {
            var ID = formulario["ihidimei"];

            BE_REQUERIMIENTO_IMEI objBE_REQUERIMIENTO = new BE_REQUERIMIENTO_IMEI();
            BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI();

            Int64 idAsignacion = 0;
            try
            {
                objBE_REQUERIMIENTO.idImei = Convert.ToInt64(ID);
                objBE_REQUERIMIENTO.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;
                objBE_REQUERIMIENTO.usuMod = HttpContext.Session.GetString("IdUsuario");
                objBE_REQUERIMIENTO.idEventoSeleccionado = 0;


                idAsignacion = objLogica.ActualizarEstadoIMEI(objBE_REQUERIMIENTO);
                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                if (idAsignacion > 0)
                {
                    result = new KeyValuePair<string, string>("1", "¡Se ha revertido la acción!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al deshacer la acción");
                }
                return Json(result);
            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al intentar deshacer la acción");
                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult EnviarAtencionIMEI(IFormCollection formulario)
        {
            var ID = formulario["ihidimei"];

            BE_REQUERIMIENTO_IMEI objBE_REQUERIMIENTO = new BE_REQUERIMIENTO_IMEI();
            BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI();

            Int64 idAsignacion = 0;
            try
            {
                objBE_REQUERIMIENTO.idImei = Convert.ToInt64(ID);
                objBE_REQUERIMIENTO.idEstado = UTConstantes.ID_ESTADO_PORATENDER;
                objBE_REQUERIMIENTO.usuMod = HttpContext.Session.GetString("IdUsuario");
                objBE_REQUERIMIENTO.idEventoSeleccionado = 0;


                idAsignacion = objLogica.ActualizarEstadoIMEI(objBE_REQUERIMIENTO);
                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                if (idAsignacion > 0)
                {
                    result = new KeyValuePair<string, string>("1", "¡El IMEI se ha marcado para poder ser ya atendido!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al intentar marcar para que pueda ser atendido");
                }
                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "EnviarAtencionIMEI> info");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al intentar marcar para que pueda ser atendido");
                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult GrabarSeleccionEvento(IFormCollection formulario)
        {

            Int64 idResultado = 0;
            try
            {
                var ID = formulario["ihidimeiban"];
                var idEventoSeleccionado = HttpContext.Session.GetString("idEventoSeleccionado");
                BE_REQUERIMIENTO_IMEI objBE_REQUERIMIENTO = new BE_REQUERIMIENTO_IMEI();
                using (BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI())
                {
                    objBE_REQUERIMIENTO.idImei = Int64.Parse(ID);
                    objBE_REQUERIMIENTO.idEventoSeleccionado = Int64.Parse(idEventoSeleccionado);
                    objBE_REQUERIMIENTO.idEstado = UTConstantes.ID_ESTADO_PORENVIAR;
                    objBE_REQUERIMIENTO.usuMod = HttpContext.Session.GetString("IdUsuario");
                    idResultado = objLogica.ActualizarEstadoIMEI(objBE_REQUERIMIENTO);
                }
                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                if (idResultado > 0)
                {
                    result = new KeyValuePair<string, string>("1", "¡El IMEI se ha marcado para enviar a la empresa operadora!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al enviar a la empresa operadora");
                }
                return Json(result);
            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al intentar marcar para que pueda ser enviado a la empresa operadora");
                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult verDevolverImei(IFormCollection formulario)
        {
            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOMOTIVODEVOLVER, UTConstantes.APLICACION_COLUMNA.IDTIPOMOTIVODEVOLVER);

            List<BE_PARAMETRO> listaFinal = new List<BE_PARAMETRO>();

            if (lista != null)
            {
                if (lista.Count > 0)
                {
                    foreach (var item in lista)
                    {
                        if (item.valor == "1"|| item.valor == "2" || item.valor == "4" || item.valor == "5" || item.valor == "8" || item.valor == "9" || item.valor == "11" || item.valor == "14")
                        {
                            listaFinal.Add(item);
                        }

                    }
                    listaFinal= listaFinal.OrderBy(item=>item.descripcion).ToList();                   
                }
            }
            ViewBag.Lista_Motivo = listaFinal;


            return PartialView("_PartialDevolverImei");
        }

        [HttpPost]
        public ActionResult verSeleccionEvento(IFormCollection formulario)
        {
            String idEmei = formulario["ihni"];
            List<BE_EVENTO_IMEI> listaEventoImei = new List<BE_EVENTO_IMEI>();

            BE_EVENTO_IMEI eventoParametro = new BE_EVENTO_IMEI();
            eventoParametro.nroImei = idEmei;
            using (BL_REQUERIMIENTO_IMEI objLogicaImei = new BL_REQUERIMIENTO_IMEI())
            {
                listaEventoImei = objLogicaImei.ListarEventosIMEI(eventoParametro);
            }

            var listaEvento = JsonConvert.SerializeObject(listaEventoImei);

            HttpContext.Session.SetString("listaEventos", listaEvento);

            ViewBag.EventoSeleccionado = 0;

            return PartialView("_PartialSeleccionEvento", listaEventoImei);
        }

        [HttpPost]
        public ActionResult refrescarEventosSeleccionado(IFormCollection formulario)
        {

            List<BE_EVENTO_IMEI> listaEventoImei = JsonConvert.DeserializeObject<List<BE_EVENTO_IMEI>>(HttpContext.Session.GetString("listaEventos"));

            List<BE_EVENTO_IMEI> lista = listaEventoImei.FindAll(x => x.idTermMoviSeleccionado > 0);
            ViewBag.EventoSeleccionado = 0;
            if (lista != null)
            {
                if (lista.Count > 0)
                {
                    ViewBag.EventoSeleccionado = 1;
                }
            }
            return PartialView("_PartialSeleccionEvento", listaEventoImei);

        }

        [HttpPost]
        public ActionResult SeleccionEvento(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>("", "");
            String idEvento = formulario["ihidevent"];

            List<BE_EVENTO_IMEI> listaEventoImei = JsonConvert.DeserializeObject<List<BE_EVENTO_IMEI>>(HttpContext.Session.GetString("listaEventos"));

            var valorSeleccion = "";
            if (listaEventoImei != null)
            {
                foreach (var item in listaEventoImei)
                {
                    valorSeleccion = "";
                    valorSeleccion = item.idTermMovi.ToString();
                    if (valorSeleccion.Equals(idEvento))
                    {
                        HttpContext.Session.SetString("idEventoSeleccionado", idEvento.ToString());
                        item.idTermMoviSeleccionado = Int64.Parse(idEvento.ToString());
                        // registroEncontrado = true;                        
                    }
                    else
                    {
                        item.idTermMoviSeleccionado = 0;
                    }
                }
            }


            var listaEvento = JsonConvert.SerializeObject(listaEventoImei);
            HttpContext.Session.SetString("listaEventos", listaEvento);

            result = new KeyValuePair<string, string>("1", "Error al intentar marcar para que pueda ser atendido");

            return Json(result);
        }

        [HttpPost]
        public ActionResult DevolverImei(IFormCollection formulario)
        {
            var ID = formulario["ihidimeiDevolver"];

            BE_REQUERIMIENTO_IMEI objBE_REQUERIMIENTO = new BE_REQUERIMIENTO_IMEI();
            BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI();

            Int64 idAsignacion = 0;
            try
            {
                objBE_REQUERIMIENTO.idImei = Convert.ToInt64(ID);
                objBE_REQUERIMIENTO.idEstado = UTConstantes.ID_ESTADO_PORDEVOLVER;
                objBE_REQUERIMIENTO.idTipoMotivo = formulario["ddlMotivo"].ToString() == null || formulario["ddlMotivo"].ToString() == "" ? 0 : Convert.ToInt32(formulario["ddlMotivo"].ToString());

                objBE_REQUERIMIENTO.usuMod = HttpContext.Session.GetString("IdUsuario");
                objBE_REQUERIMIENTO.idEventoSeleccionado = 0;
                idAsignacion = objLogica.ActualizarEstadoIMEI(objBE_REQUERIMIENTO);

                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                if (idAsignacion > 0)
                {
                    result = new KeyValuePair<string, string>("1", "¡El IMEI se ha marcado para poder devolver !");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al intentar marcar para que pueda ser devuelto");
                }
                return Json(result);
            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al intentar marcar para que pueda ser devuelto");
                return Json(result);
            }
        }

        public ActionResult irBandejaRequerimiento(IFormCollection formulario)
        {
            return RedirectToAction("Index", "Requerimiento");
        }


    }
}