﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using OfficeOpenXml;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class SistemaDocumentarioController : Controller
    {
        private readonly IConfiguration configuration;

        public SistemaDocumentarioController(IConfiguration config)
        {
            this.configuration = config;
        }

        public IActionResult Index()
        {

            var opcionCarga = HttpContext.Session.GetString("opcionCargado");
            if (opcionCarga == null) { ViewBag.OpionCarga = "0"; } else { ViewBag.OpionCarga = opcionCarga; }
            HttpContext.Session.SetString("opcionCargado", "0");
            return View();
        }

        public FileResult DescargarPlantillaImeiSisDoc()
        {

            var rutaDocumentosPlantilla = configuration["PlantillasSIGREI"];

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaDocumentosPlantilla + "\\PlantillaSISDOCIMEI.xlsx");
            string fileName = "PlantillaSISDOCIMEI.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpGet]
        public ActionResult BandejaInicial(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
            List<ErrorImei> listaErrors = new List<ErrorImei>();
            var listaErrorJSON = JsonConvert.SerializeObject(listaErrors);
            HttpContext.Session.SetString("listaErrorGPSSisDoc", listaErrorJSON);

            listaResultado = new List<BE_REQUERIMIENTO>();

            var listaRequerientoJSON = JsonConvert.SerializeObject(new List<BE_REQUERIMIENTO>());
            HttpContext.Session.SetString("ListaIMEI", listaRequerientoJSON);

            return PartialView("_DetalleBandeja", listaResultado);
        }


        [HttpGet]
        public ActionResult CargarDetalleCargado(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
            List<ErrorImei> listaErrors = new List<ErrorImei>();
            var listaErrorJSON = JsonConvert.SerializeObject(listaErrors);
            HttpContext.Session.SetString("listaErrorGPSSisDoc", listaErrorJSON);

            listaResultado = new List<BE_REQUERIMIENTO>();

            listaResultado = JsonConvert.DeserializeObject<List<BE_REQUERIMIENTO>>(HttpContext.Session.GetString("ListaIMEI"));
            if (listaResultado == null) { listaResultado = new List<BE_REQUERIMIENTO>(); }

            return PartialView("_DetalleBandeja", listaResultado);
        }


        [HttpPost]
        public ActionResult CargarInformacionSisDoc(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacion(formulario);

                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionSisDoc=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }


        [HttpPost]
        public ActionResult GrabarInformacionSisDoc(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = GrabarInformacion(formulario);

                var listaRequerientoJSON = JsonConvert.SerializeObject(new List<BE_REQUERIMIENTO>());
                HttpContext.Session.SetString("ListaIMEI", listaRequerientoJSON);
                HttpContext.Session.SetString("RutaArchivoTemporal", "");
                HttpContext.Session.SetString("nombreArchivoImei", "");
                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionSisDoc=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        public FileResult DescargarErrorSisDoc()
        {
            var rutaDocumentosPlantilla = configuration["CarpetaTemporal"];
            var archivoError = Path.Combine(rutaDocumentosPlantilla, "Error.txt");
            if (System.IO.File.Exists(archivoError))
            {
                System.IO.File.Delete(archivoError);
            }
            List<ErrorImei> listaError = new List<ErrorImei>();
            listaError = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("listaErrorGPSSisDoc"));

            List<ErrorImei> listaErrorErrores = new List<ErrorImei>();
            var listaErrror = JsonConvert.SerializeObject(listaErrorErrores);
            HttpContext.Session.SetString("listaErrorGPSSisDoc", listaErrror);

            if (listaError != null)
            {
                if (listaError.Count > 0)
                {
                    listaError = listaError.OrderBy(m => m.nroItem).ToList();
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(archivoError, true))
                    {
                        file.WriteLine("------ERRORES ENCONTRADOS EN EL DOCUMENTO CARGADO----");
                        foreach (var item in listaError)
                        {
                            file.WriteLine("SISDOC " + item.nroImei + ": Error==>" + item.codError);
                        }
                        file.Dispose();
                    }
                }
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoError);
            string fileName = "Error.txt";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }




        #region "Metodos"



        private KeyValuePair<string, string> GrabarInformacion(IFormCollection formulario)
        {
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();
            try
            {
                List<BE_REQUERIMIENTO> listaResultado = JsonConvert.DeserializeObject<List<BE_REQUERIMIENTO>>(HttpContext.Session.GetString("ListaIMEI"));

                if (listaResultado == null) { listaResultado = new List<BE_REQUERIMIENTO>(); }

                String CodSisDoc = "";
                Int32 indice = -1;

                Int64 idAdjunto = 0;
                BE_REQUERIMIENTO_IMEI objIMEI = new BE_REQUERIMIENTO_IMEI();
                List<BE_REQUERIMIENTO> listaRequerimiento = new List<BE_REQUERIMIENTO>();

                #region "Registrando el IMEI con Sisdoc"
                String ubicacionArchivoTemporal = HttpContext.Session.GetString("RutaArchivoTemporal");
                String nombreArchivoImei = HttpContext.Session.GetString("nombreArchivoImei");

                var rutaCarpetaDocumentos = configuration["RutaDocumentos"];
                var rutaDestino = Path.Combine(rutaCarpetaDocumentos, nombreArchivoImei);
                System.IO.File.Copy(ubicacionArchivoTemporal, rutaDestino, true);

                BL_DOCUMENTO_ADJUNTO objLogicaArchivoAdjunto = new BL_DOCUMENTO_ADJUNTO();
                BE_DOCUMENTO_ADJUNTO objAdjunto = new BE_DOCUMENTO_ADJUNTO();

                objAdjunto.nombreDocumento = nombreArchivoImei;
                objAdjunto.descDocumento = "Registro de Sisdoc, Archivo adjuntado al registrar de forma masiva con sisdoc";
                objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");


                idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);

                #endregion
                foreach (var item in listaResultado)
                {
                    if (item.nroSisDoc != null)
                    {
                        if (!item.nroSisDoc.Equals(CodSisDoc))
                        {
                            item.listaIMEI = new List<BE_REQUERIMIENTO>();
                            item.listaIMEI.Add(item);
                            item.idAdjuntoIMEI = idAdjunto;
                            listaRequerimiento.Add(item);
                            CodSisDoc = item.nroSisDoc;
                            indice++;
                        }
                        else
                        {
                            listaRequerimiento[indice].listaIMEI.Add(item);
                        }
                    }
                }
                BL_REQUERIMIENTO objLogicaRequerimiento = new BL_REQUERIMIENTO();
                BL_REQUERIMIENTO_IMEI objLogicaIMEI = new BL_REQUERIMIENTO_IMEI();

                long idRequerimiento = 0;
                long idRequerimientoIMEi = 0;
                foreach (var item in listaRequerimiento)
                {
                    item.usuCre = HttpContext.Session.GetString("IdUsuario");
                    idRequerimiento = 0;
                    idRequerimiento = objLogicaRequerimiento.InsertarSiSDoc(item);
                    foreach (var itemImei in item.listaIMEI)
                    {
                        itemImei.idRequerimiento = idRequerimiento;
                        itemImei.usuCre = HttpContext.Session.GetString("IdUsuario"); ;
                        idRequerimientoIMEi = objLogicaIMEI.InsertarSisDocImeiInterno(itemImei);
                    }
                }
                keyValueError = new KeyValuePair<string, string>("1", "Se grabo correctamente los requerimientos");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "GrabarInformacion");
                keyValueError = new KeyValuePair<string, string>("0", "Error al guardar los requerimientos de IMEI");
            }
            return keyValueError;
        }

        private KeyValuePair<string, string> CargarInformacion(IFormCollection formulario)
        {
            String mensaje = "";
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            Boolean existeRegistro = false;


            string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileSisDoc"].FileName);

            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            //if (archivo.ContentLength / 1024 < 2000)//2MB
            IFormFile archivo = formulario.Files["fileSisDoc"];
            if (archivo.Length / 1024 < TamFileGrande)//2MB
            {
                if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                {
                    keyValueError = new KeyValuePair<string, string>("", "");
                }
                else
                {
                    mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                    keyValueError = new KeyValuePair<string, string>("0", mensaje);
                }
            }
            else
            {
                keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);

            }

            if (keyValueError.Key == "")
            {
                List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
                BE_REQUERIMIENTO objParametroDos = new BE_REQUERIMIENTO();

                #region "Copiar a una carpeta temporal para revisar si hay registro que registrar"


                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                IFormFile fileArchivoImei = formulario.Files["fileSisDoc"];
                var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                string nombreArchivoImei = Path.GetFileName(formulario.Files["fileSisDoc"].FileName);

                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                var ubicacionArchivoTemporal = "";

                if (fileArchivoImei.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileArchivoImei.CopyTo(fileStream);
                    }
                    ubicacionArchivoTemporal = filePath;
                }
                FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                HttpContext.Session.SetString("nombreArchivoImei", nombreArchivoImei);

                using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                {
                    try
                    {
                        ExcelWorksheet workSheet = package.Workbook.Worksheets["DATA"];
                        int totalRows = workSheet.Dimension.Rows;

                        if (totalRows < 2)
                        {
                            return keyValueError = new KeyValuePair<string, string>("2", "No existe información en el archivo adjunto");
                        }
                        else
                        {
                            try
                            {
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    objParametroDos = new BE_REQUERIMIENTO();
                                    objParametroDos.nroItem = i;
                                    objParametroDos.nroSisDoc = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim().Replace(",", ".") : "";
                                    objParametroDos.nroImei = workSheet.Cells[i, 2].Value != null ? workSheet.Cells[i, 2].Value.ToString().Trim().ToLower() : "";
                                    objParametroDos.indLogico = workSheet.Cells[i, 3].Value != null ? workSheet.Cells[i, 3].Value.ToString().Trim().ToLower() : "";
                                    objParametroDos.tipoSolicitante = workSheet.Cells[i, 4].Value != null ? workSheet.Cells[i, 4].Value.ToString().Trim().ToLower() : "";
                                    //if (objParametroDos.nroSisDoc.Length > 0 && objParametroDos.nroImei.Length > 0 && objParametroDos.tipoSolicitante.Length > UTConstantes.VALOR_CERO)
                                    //{

                                    //}
                                    listaResultado.Add(objParametroDos);

                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "CargarInformacion=> error al cargar la data ");
                                return keyValueError = new KeyValuePair<string, string>("0", "Error en la obtención de los datos");
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarErrorMetodo(ex, "CargarInformacion=> error al cargar la data ");
                        return keyValueError = new KeyValuePair<string, string>("0", UTConstantes.PLANTILLA_EXCEL_NO_EXISTE_HOJA_DATA);
                    }
                }
                #endregion

                if (listaResultado == null)
                {
                    existeRegistro = false;
                }
                else if (listaResultado.Count == 0)
                {
                    existeRegistro = false;
                }
                else
                {
                    existeRegistro = true;
                }
                if (existeRegistro)
                {

                    listaResultado = listaResultado.OrderBy(o => o.nroSisDoc).ToList();

                    if (listaResultado != null)
                    {
                        if (listaResultado.Count > 0)
                        {
                            for (int i = 0; i < listaResultado.Count; i++)
                            {
                                listaResultado[i] = validarDocumentoExcel(listaResultado[i], listaResultado);
                            }
                        }
                    }

                    List<ErrorImei> listaErrors = new List<ErrorImei>();
                    BE_REQUERIMIENTO objIMEI = new BE_REQUERIMIENTO();
                    listaErrors = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("listaErrorGPSSisDoc"));
                    if (listaErrors == null) { listaErrors = new List<ErrorImei>(); }
                    if (listaErrors.Count == 0)
                    {

                        XmlDocument objXMLIMEI = new XmlDocument();
                        XmlElement nodeUno = objXMLIMEI.CreateElement("ListaSisDoc");
                        objXMLIMEI.AppendChild(nodeUno);

                       for (int i = 0; i < listaResultado.Count; i++)
                        {
                            XmlElement registro = objXMLIMEI.CreateElement("Table");
                            nodeUno.AppendChild(registro);

                            XmlElement eFila= objXMLIMEI.CreateElement(string.Empty, "fila", string.Empty);
                            XmlText xTFila = objXMLIMEI.CreateTextNode((i+1).ToString());
                            eFila.AppendChild(xTFila);
                            registro.AppendChild(eFila);

                            XmlElement eNroSisdoc = objXMLIMEI.CreateElement(string.Empty, "pi_NroSisDoc", string.Empty);
                            XmlText xTSisDoc = objXMLIMEI.CreateTextNode(listaResultado[i].nroSisDoc.ToString());
                            eNroSisdoc.AppendChild(xTSisDoc);
                            registro.AppendChild(eNroSisdoc);

                            XmlElement eNroIMEI = objXMLIMEI.CreateElement(string.Empty, "pi_NroIMEI", string.Empty);
                            XmlText xtNroIMEI = objXMLIMEI.CreateTextNode(listaResultado[i].nroImei.ToString());
                            eNroIMEI.AppendChild(xtNroIMEI);
                            registro.AppendChild(eNroIMEI);

                            XmlElement eFlgLogico = objXMLIMEI.CreateElement(string.Empty, "pi_FlgLogico", string.Empty);
                            XmlText xTIndLogico = objXMLIMEI.CreateTextNode(listaResultado[i].indLogico.ToString());
                            eFlgLogico.AppendChild(xTIndLogico);
                            registro.AppendChild(eFlgLogico);

                            XmlElement eTipoInstitucion = objXMLIMEI.CreateElement(string.Empty, "tipoSolicitante", string.Empty);
                            XmlText xTTipoInstitucion = objXMLIMEI.CreateTextNode(listaResultado[i].tipoSolicitante.ToString());
                            eTipoInstitucion.AppendChild(xTTipoInstitucion);
                            registro.AppendChild(eTipoInstitucion);

                        }
                        String Cadena = objXMLIMEI.InnerXml;
                        Cadena = Cadena.Replace("<ListaSisDoc>", "");
                        Cadena = Cadena.Replace("</ListaSisDoc>", "");

                        String CadenaInicial = "<ListaSisDoc>";
                        String CadenaFinal = "</ListaSisDoc>";

                        Int32 indiceInicio = 0;
                        Int32 indiceFinal = Cadena.Length;
                        Int32 CantidadTomar = indiceFinal/10;

                        String CadenaUno = Cadena.Substring(indiceInicio,CantidadTomar);
                        indiceInicio = CantidadTomar;
                        String CadenaDos = Cadena.Substring(indiceInicio, CantidadTomar);
                        indiceInicio = indiceInicio + CantidadTomar;
                        String CadenaTres = Cadena.Substring(indiceInicio, CantidadTomar);
                        indiceInicio = indiceInicio  + CantidadTomar;
                        String CadenaCuatro = Cadena.Substring(indiceInicio, CantidadTomar);
                        indiceInicio = indiceInicio  + CantidadTomar;
                        String CadenaCinco = Cadena.Substring(indiceInicio, CantidadTomar);
                        indiceInicio = indiceInicio  + CantidadTomar;
                        String CadenaSeis = Cadena.Substring(indiceInicio, CantidadTomar);
                        indiceInicio = indiceInicio  + CantidadTomar;
                        String CadenaSiete = Cadena.Substring(indiceInicio, CantidadTomar);
                        indiceInicio = indiceInicio  + CantidadTomar;
                        String CadenaOcho = Cadena.Substring(indiceInicio, CantidadTomar);
                        indiceInicio = indiceInicio  + CantidadTomar;
                        String CadenaNueve = Cadena.Substring(indiceInicio, CantidadTomar);
                        indiceInicio = indiceInicio  + CantidadTomar;
                        CantidadTomar = indiceFinal - indiceInicio;
                        String CadenaDiez = Cadena.Substring(indiceInicio, CantidadTomar);



                        List<BE_REQUERIMIENTO> listaRequerimiento = new List<BE_REQUERIMIENTO>();

                        using (BL_REQUERIMIENTO objLogicaSolicitud = new BL_REQUERIMIENTO())
                        {
                            objIMEI = new BE_REQUERIMIENTO();
                            objIMEI.usuCre = "SIGREI";
                            listaRequerimiento= objLogicaSolicitud.ObtenerDatosIMEIXML(CadenaInicial, CadenaUno.TrimEnd(), CadenaDos.TrimEnd(), CadenaTres.TrimEnd(), CadenaCuatro.TrimEnd(), CadenaCinco.TrimEnd(), CadenaSeis.TrimEnd(), CadenaSiete.TrimEnd(), CadenaOcho.TrimEnd(), CadenaNueve.Trim(), CadenaDiez.TrimEnd(), CadenaFinal, objIMEI);
                        }

                        var listaSesionJSON = JsonConvert.SerializeObject(listaRequerimiento);
                        HttpContext.Session.SetString("ListaIMEI", listaSesionJSON);
                        HttpContext.Session.SetString("RutaArchivoTemporal", ubicacionArchivoTemporal);
                        HttpContext.Session.SetString("opcionCargado", "1");
                        keyValueError = new KeyValuePair<string, string>("1", "Se ha obtenido la siguiente información");
                    }
                    else
                    {
                        return keyValueError = new KeyValuePair<string, string>("2", "Se ha encontrado errores en el archivo IMEI");
                    }
                }
            }

            return keyValueError;
        }

        private BE_REQUERIMIENTO validarDocumentoExcel(BE_REQUERIMIENTO objParametro, List<BE_REQUERIMIENTO> lista)
        {
            List<ErrorImei> listaErrors = new List<ErrorImei>();
            listaErrors = JsonConvert.DeserializeObject<List<ErrorImei>>(HttpContext.Session.GetString("listaErrorGPSSisDoc"));

            if (listaErrors == null) { listaErrors = new List<ErrorImei>(); }


            String error = "";
            ErrorImei objError = new ErrorImei();
            objError.nroImei = objParametro.nroSisDoc;
            if (objParametro.nroSisDoc.Length == 0)
            {
                error += "no tiene número de sisdoc";
                error += ", ";
            }
            if (objParametro.nroImei.Length == 0)
            {
                error += "no tiene número de IMEI";
                error += ", ";
            }
            else
            {
                long numservicio = 0;
                bool isNumero = long.TryParse(objParametro.nroImei, out numservicio);
                if (!isNumero)
                {
                    error += "El número de IMEI contiene caracteres que no es numérico";
                    error += ", ";
                }
            }
            if (objParametro.indLogico != "" && objParametro.indLogico.ToLower() != "l")
            {
                error += "el flag es incorrecto";
                error += ", ";
            }

            if (objParametro.tipoSolicitante.Equals("1") || objParametro.tipoSolicitante.Equals("2") || objParametro.tipoSolicitante.Equals("3"))
            {
                List<BE_REQUERIMIENTO> listaTemporal = new List<BE_REQUERIMIENTO>();
                listaTemporal = lista.FindAll(x => x.nroSisDoc.Contains(objParametro.nroSisDoc));

                if (listaTemporal != null)
                {
                    if (listaTemporal.Count > 0)
                    {
                        foreach (var item in listaTemporal)
                        {
                            if (item.isRevisado = true && item.nroSisDoc.Equals(objParametro.nroSisDoc))
                            {

                                if (!objParametro.tipoSolicitante.Equals(item.tipoSolicitante))
                                {
                                    error += "el número de SISDOC tienen diferentes tipos de solicitante";
                                    error += ", ";
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                error += "el tipo de institución es incorrecto";
                error += ", ";
            }
            if (error != "")
            {
                error = error.Substring(0, error.Length - 2);
                objError.nroItem = objParametro.nroItem;
                objError.codError = String.Format("La fila {0} tiene el siguiente error: ", objParametro.nroItem) + error;
                listaErrors.Add(objError);

                var listaErrorJSON = JsonConvert.SerializeObject(listaErrors);
                HttpContext.Session.SetString("listaErrorGPSSisDoc", listaErrorJSON);

            }
            objParametro.isRevisado = true;
            return objParametro;
        }
        #endregion

        public ActionResult RecargarPaginaCarga()
        {

            return RedirectToAction("Index", "SistemaDocumentario");
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        public DataTable ConvertListToDataTable(List<string[]> list)
        {
            // New table.
            DataTable table = new DataTable();

            table.Columns.Add("NROSISDOC");
            table.Columns.Add("NROIMEI");
            table.Columns.Add("FLGLOGICO");
            //// Get max columns.
            //int columns = 0;
            //foreach (var array in list)
            //{
            //    if (array.Length > columns)
            //    {
            //        columns = array.Length;
            //    }
            //}

            //// Add columns.
            //for (int i = 0; i < columns; i++)
            //{
            //    table.Columns.Add("NROSISDOC");
            //}

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }

    }
}