﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;
using System.IO;
using docPacking = DocumentFormat.OpenXml.Packaging;
using docWodProcesing = DocumentFormat.OpenXml.Wordprocessing;

using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Net.Mail;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style.XmlAccess;
using OfficeOpenXml.Style;
using System.Drawing;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class AsignarController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly IHostingEnvironment hosting;
        public AsignarController(IConfiguration config, IHostingEnvironment host)
        {
            this.configuration = config;
            this.hosting = host;
        }

        public IActionResult Index()
        {

            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);

            ViewBag.Lista_TipoSolicitante = lista;

            return View();
        }

        [HttpGet]
        public ActionResult BandejaInicial(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();


            return PartialView("_DetalleBandeja", listaResultado);
        }

        [HttpPost]
        public ActionResult BuscarRequerimientos(IFormCollection formulario)
        {


            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();
            BE_REQUERIMIENTO objParametro = obtenerCriterios(formulario);
            BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();

            listaResultado = objLogica.ListaSolicitud(objParametro);

            FileStream fs = new FileStream(configuration["CarpetaTemporal"] + "\\" + "Busqueda de Asignar Req.xlsx", FileMode.Create);
            ExcelPackage Excel = new ExcelPackage(fs);

            /* Creación del estilo. */
            Excel.Workbook.Styles.CreateNamedStyle("Moneda");
            ExcelNamedStyleXml moneda = Excel.Workbook.Styles.NamedStyles[1];// 0 = Normal, 1 (El que acabamos de agregar).

            /*moneda.Style.Numberformat.Format = "_-$* #,##0.00_-;-$* #,##0.00_-;_-$* \"-\"??_-;_-@_-";
            moneda.Style.Fill.PatternType = ExcelFillStyle.Solid;
            moneda.Style.Fill.BackgroundColor.SetColor(Color.Yellow);*/

            /* Creación de hoja de trabajo. */
            Excel.Workbook.Worksheets.Add("Busqueda Asignar");
            ExcelWorksheet hoja = Excel.Workbook.Worksheets["Busqueda Asignar"];

            /* Num Caracteres + 1.29 de Margen.
            Los índices de columna empiezan desde 1. */
            //hoja.Column(1).Width = 11.29f;
            Color colorDeCelda = ColorTranslator.FromHtml("#00AAE4");

            ExcelRange rango = hoja.Cells["A1"];
            rango.Value = "Nº";
            rango.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rango.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rango.Style.Font.Bold = true;
            rango.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rango.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rango.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rango.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rango.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rango.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(1).Width = 10;

            ExcelRange rangoB = hoja.Cells["B1"];
            rangoB.Value = "Número de Registro";
            rangoB.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoB.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoB.Style.Font.Bold = true;
            rangoB.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoB.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoB.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoB.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoB.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoB.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(2).Width = 20;

            ExcelRange rangoC = hoja.Cells["C1"];
            rangoC.Value = "Nombre de Institución";
            rangoC.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoC.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoC.Style.Font.Bold = true;
            rangoC.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoC.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoC.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoC.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoC.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoC.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(3).Width = 30;

            ExcelRange rangoD = hoja.Cells["D1"];
            rangoD.Value = "Nombre Solicitante";
            rangoD.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoD.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoD.Style.Font.Bold = true;
            rangoD.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoD.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoD.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoD.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoD.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoD.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(4).Width = 30;

            ExcelRange rangoE = hoja.Cells["E1"];
            rangoE.Value = "Cargo Solicitante";
            rangoE.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoE.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoE.Style.Font.Bold = true;
            rangoE.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoE.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoE.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoE.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoE.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoE.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(5).Width = 30;

            ExcelRange rangoF = hoja.Cells["F1"];
            rangoF.Value = "Correo Electrónico";
            rangoF.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoF.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoF.Style.Font.Bold = true;
            rangoF.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoF.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoF.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoF.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoF.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoF.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(6).Width = 30;

            ExcelRange rangoG = hoja.Cells["G1"];
            rangoG.Value = "Nº Oficio";
            rangoG.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoG.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoG.Style.Font.Bold = true;
            rangoG.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoG.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoG.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoG.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoG.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoG.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(7).Width = 20;

            ExcelRange rangoH = hoja.Cells["H1"];
            rangoH.Value = "Fecha y hora de Ingreso";
            rangoH.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoH.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoH.Style.Font.Bold = true;
            rangoH.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoH.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoH.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoH.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoH.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoH.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(8).Width = 30;

            ExcelRange rangoI = hoja.Cells["I1"];
            rangoI.Value = "IMEIs Registrados";
            rangoI.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoI.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoI.Style.Font.Bold = true;
            rangoI.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoI.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoI.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoI.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoI.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoI.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(9).Width = 20;

            ExcelRange rangoJ = hoja.Cells["J1"];
            rangoJ.Value = "Movil Registrados";
            rangoJ.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoJ.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoJ.Style.Font.Bold = true;
            rangoJ.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoJ.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoJ.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoJ.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoJ.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoJ.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(10).Width = 20;

            ExcelRange rangoK = hoja.Cells["K1"];
            rangoK.Value = "Excepción";
            rangoK.Style.Fill.PatternType = ExcelFillStyle.Solid;
            rangoK.Style.Fill.BackgroundColor.SetColor(colorDeCelda);
            rangoK.Style.Font.Bold = true;
            rangoK.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            rangoK.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            rangoK.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            rangoK.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            rangoK.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            rangoK.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            hoja.Column(11).Width = 20;


            int contador = 2;
            for (var i = 0; i < listaResultado.Count; i++)
            {
                ExcelRange rangoAA = hoja.Cells["A" + contador];

                rangoAA.Value = listaResultado[i].nroItem;
                rangoAA.Style.WrapText = true;

                ExcelRange rangoBB = hoja.Cells["B" + contador];
                rangoBB.Value = listaResultado[i].nroRequerimiento;
                rangoBB.Style.WrapText = true;

                ExcelRange rangoCC = hoja.Cells["C" + contador];
                rangoCC.Value = listaResultado[i].nombreInstitucion;
                rangoCC.Style.WrapText = true;

                ExcelRange rangoDD = hoja.Cells["D" + contador];
                rangoDD.Value = listaResultado[i].nombreSolicitante;
                rangoDD.Style.WrapText = true;

                ExcelRange rangoEE = hoja.Cells["E" + contador];
                rangoEE.Value = listaResultado[i].cargo;
                rangoEE.Style.WrapText = true;

                ExcelRange rangoFF = hoja.Cells["F" + contador];
                rangoFF.Value = listaResultado[i].correo;
                rangoFF.Style.WrapText = true;

                ExcelRange rangoGG = hoja.Cells["G" + contador];
                rangoGG.Value = listaResultado[i].nroOficio;
                rangoGG.Style.WrapText = true;

                ExcelRange rangoHH = hoja.Cells["H" + contador];
                rangoHH.Value = listaResultado[i].fecRegistro;
                rangoHH.Style.WrapText = true;

                ExcelRange rangoII = hoja.Cells["I" + contador];
                rangoII.Value = listaResultado[i].cantidadRegistrado;
                rangoDD.Style.WrapText = true;

                ExcelRange rangoJJ = hoja.Cells["J" + contador];
                rangoJJ.Value = listaResultado[i].CANTIDAD_MOVIL;
                rangoJJ.Style.WrapText = true;

                ExcelRange rangoKK = hoja.Cells["K" + contador];
                rangoKK.Value = listaResultado[i].DES_EXCEPCION;
                rangoKK.Style.WrapText = true;



                contador++;
            }




            Excel.Save();

            // No estoy seguro de ésta parte, pero mejor cerramos el stream de archivo.
            fs.Close();
            fs.Dispose();

            Excel.Dispose();


            return PartialView("_DetalleBandeja", listaResultado);
        }
        public FileResult DescargarExcelBusqueda()
        {


            var rutaManual = configuration["CarpetaTemporal"] + "/" ;
            String nombrePlantilla = "Busqueda de Asignar Req.xlsx";
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public BE_REQUERIMIENTO obtenerCriterios(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objResultado = new BE_REQUERIMIENTO();

            objResultado.idTipoSolicitante = formulario["ddlTipoSolicitante"].ToString() == null || formulario["ddlTipoSolicitante"].ToString() == "" ? 0 : Convert.ToInt32(formulario["ddlTipoSolicitante"].ToString());
            objResultado.nombreInstitucion = formulario["txtNombreInstitucion"];
            objResultado.nombreSolicitante = formulario["txtnombreSolicitante"];
            objResultado.nroOficio = formulario["txtnroOficio"];
            objResultado.nroRequerimientoI = formulario["txtNroRequerimientoI"];
            objResultado.nroRequerimientoF = formulario["txtNroRequerimientoF"];
            objResultado.nroImei = formulario["txtNroImei"];

            objResultado.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;

            return objResultado;
        }

        [HttpPost]
        public ActionResult ConsultarImeiAsignados(IFormCollection formulario)
        {
            BE_REQUERIMIENTO_IMEI objSolicitud = new BE_REQUERIMIENTO_IMEI();
            BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI();
            List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            var ID = formulario["hdimei"];
            objSolicitud.idRequerimiento = Int64.Parse(formulario["hdimei"].ToString());

            listaResultado = objLogica.ListaIMEIAsignado(objSolicitud);

            return PartialView("_PartialImeiAsignados", listaResultado);
        }

        [HttpPost]
        public ActionResult AbrirAsignarRequerimiento(IFormCollection formulario)
        {
            BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();
            BE_REQUERIMIENTO objEntidad = new BE_REQUERIMIENTO();

            objEntidad.idRequerimiento = Int64.Parse(formulario["hdimei"].ToString());
            objEntidad = objLogica.ConsultaSolicitud(objEntidad);


            BL_USUARIOGPSU ObjLogicaUsuario = new BL_USUARIOGPSU();
            List<BE_USUARIOGPSU> listaUsuario = ObjLogicaUsuario.Listar(UTConstantes.APLICACION, UTConstantes.VALOR_UNO);

            ViewBag.Lista_Usuario = listaUsuario;


            return PartialView("_PartialAsignarRequerimiento", objEntidad);
        }
        [HttpPost]
        public ActionResult AsignarRequerimiento(IFormCollection formulario)
        {
            try
            {
                BE_REQUERIMIENTO objEntidad = new BE_REQUERIMIENTO();
                BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();

                objEntidad.idRequerimiento = Int64.Parse(formulario["hdimei"].ToString());
                objEntidad.usuarioAsignacion = formulario["ddlUsuario"].ToString() == "" ? "" : formulario["ddlUsuario"].ToString();
                objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario");
                objEntidad.idEstado = UTConstantes.ID_ESTADO_ASIGNADO;
                Int64 idAsignacion = 0;
                try
                {
                    idAsignacion = objLogica.AsignarSolicitud(objEntidad);
                    KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                    if (idAsignacion > 0)
                    {
                        result = new KeyValuePair<string, string>("1", "¡Requeriento Asignado correctamente!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Error al registrar la información");
                    }
                    return Json(result);
                }
                catch (Exception)
                {

                    KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al registrar la información");
                    return Json(result);
                }

            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al registrar la información");

                return Json(result);
            }
        }


        [HttpPost]
        public ActionResult RechazarRequerimiento(IFormCollection formulario)
        {
            try
            {
                BE_REQUERIMIENTO objEntidad = new BE_REQUERIMIENTO();
                BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();

                objEntidad.idRequerimiento = Int64.Parse(formulario["hdimei"].ToString());
                objEntidad.idTipoMotivoRechazo = UTConstantes.ID_TIPOFINALIZACION_INCOMPLETO;
                objEntidad.comentarioRechazo = formulario["comentariosRechazo"].ToString();// txtComentario.Text;
                objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario");
                objEntidad.idEstado = UTConstantes.ID_ESTADO_FINALIZADO;

                Int64 idAsignacion = 0;
                try
                {
                    idAsignacion = objLogica.RechazarSolicitud(objEntidad);
                    KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                    if (idAsignacion > 0)
                    {
                        result = new KeyValuePair<string, string>("1", "¡Requeriento Rechazado correctamente!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Error al actualizar la información");
                    }
                    return Json(result);
                }
                catch (Exception)
                {

                    KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al actualizar la información");
                    return Json(result);
                }
            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al actualizar la información");
                return Json(result);
            }
        }


        [HttpPost]
        public ActionResult AbrirDescargaDocumentos(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objSolicitud = new BE_REQUERIMIENTO();
            BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();
            try
            {
                objSolicitud.idRequerimiento = Int64.Parse(formulario["hdimei"].ToString()); ;
                objSolicitud = objLogica.ConsultaSolicitud(objSolicitud);
            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                objSolicitud = new BE_REQUERIMIENTO();
            }


            return Json(objSolicitud);
        }

        [HttpPost]
        public ActionResult ConsultaRequerimiento(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objSolicitud = new BE_REQUERIMIENTO();
            try
            {
                BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();
                objSolicitud.idRequerimiento = Int64.Parse(formulario["hdimei"].ToString());
                objSolicitud = objLogica.ConsultaSolicitud(objSolicitud);
            }
            catch (Exception ex)
            {
                 LogError.RegistrarError(ex);
                objSolicitud = new BE_REQUERIMIENTO();
            }
            return PartialView("_ConsultaRequerimiento", objSolicitud);
        }

        [HttpGet]
        public ActionResult ValidarDocumentoSIGREI(String nombreDoc)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            var rutaDocumentosPlantilla = configuration["RutaDocumentos"];

            String nombreDocumento = rutaDocumentosPlantilla + "\\" + nombreDoc;

            if (!System.IO.File.Exists(nombreDocumento))
            {
                result = new KeyValuePair<string, string>("0", "No se ha podido encontrador el documento solicitado, consulte con el administrador del sistema.");
            }
            else
            {
                HttpContext.Session.SetString("NombreArchivo", nombreDoc);
                result = new KeyValuePair<string, string>("1", "¡Documento encontrado correctamente!");

            }

            return Json(result);
        }

        public FileResult DescargarDocumento()
        {
            var rutaDocumentosPlantilla = configuration["RutaDocumentos"];
            String nombreDoc = HttpContext.Session.GetString("NombreArchivo");
            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaDocumentosPlantilla + "\\" + nombreDoc);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreDoc);
        }

        [HttpPost]
        public ActionResult VerAnalizarImei(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objSolicitud = new BE_REQUERIMIENTO();
            try
            {
                BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();
                objSolicitud.idRequerimiento = Int64.Parse(formulario["hdimei"].ToString());
                objSolicitud = objLogica.ConsultaSolicitud(objSolicitud);


                BE_REQUERIMIENTO_IMEI objParametroImei = new BE_REQUERIMIENTO_IMEI();
                List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
                objParametroImei.idRequerimiento = objSolicitud.idRequerimiento;
                BL_REQUERIMIENTO_IMEI objLogicaImei = new BL_REQUERIMIENTO_IMEI();

                objSolicitud.listaRequerimientoIMEI = objLogicaImei.ListaIMEI(objParametroImei);

            }
            catch (Exception ex)
            {
                 LogError.RegistrarError(ex);
                objSolicitud = new BE_REQUERIMIENTO();
            }
            return PartialView("_PartialAnalizarImei", objSolicitud);
        }

        [HttpPost]
        public ActionResult DescargarDocumentoImei(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            var rutaDocumentosPlantilla = configuration["PlantillasSIGREI"];

            var imeiss = formulario["hdimeiseleccion"].ToString();
            imeiss = imeiss.Substring(0, imeiss.Length - 1);
            var listaImei = imeiss.Split("-");
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");
            String nombreArchivoDoc = "";
            nombreArchivoDoc = fecha + "CartaRespuesta.docx";

            String rutaArchvioPlantilla = "";

            Int64 idRequerimientoImei = 0;

            if (listaImei.Length > 1)
            {
                rutaArchvioPlantilla = rutaDocumentosPlantilla + "\\" + UTConstantes.CARTA_DEVOLVER_VARIOS;
                idRequerimientoImei = Int64.Parse(listaImei[0]);
            }
            else
            {
                rutaArchvioPlantilla = rutaDocumentosPlantilla + "\\" + UTConstantes.CARTA_DEVOLVER_INDIVIDUAL;
                idRequerimientoImei = 0;
            }
            var rutaArchivoCopiado = configuration["CarpetaTemporal"];
            rutaArchivoCopiado = rutaArchivoCopiado + "\\" + nombreArchivoDoc;

            if (System.IO.File.Exists(rutaArchivoCopiado))
            {
                System.IO.File.Delete(rutaArchivoCopiado);
            }
            System.IO.File.Copy(rutaArchvioPlantilla, rutaArchivoCopiado);
            var hdimei = Int64.Parse(formulario["hdimei"]);
            Boolean convertido = fusionarDocumentoWord(rutaArchivoCopiado, hdimei, idRequerimientoImei);

            if (convertido)
            {

                if (!System.IO.File.Exists(rutaArchivoCopiado))
                {
                    result = new KeyValuePair<string, string>("0", "No se ha podido encontrador el documento solicitado, consulte con el administrador del sistema.");
                }
                else
                {
                    HttpContext.Session.SetString("NombreArchivo", nombreArchivoDoc);
                    result = new KeyValuePair<string, string>("1", "¡Documento encontrado correctamente!");
                }
            }
            else
            {
                result = new KeyValuePair<string, string>("0", "No se ha podido fusionar el documento plantilla solicitado, consulte con el administrador del sistema.");
            }

            return Json(result);
        }




        public FileResult DescargarDocumentoTemporal()
        {
            var rutaDocumentoTemporal = configuration["CarpetaTemporal"];
            String nombreDoc = HttpContext.Session.GetString("NombreArchivo");
            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaDocumentoTemporal + "\\" + nombreDoc);

            if (System.IO.File.Exists(nombreDoc))
            {
                System.IO.File.Delete(nombreDoc);
            }

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreDoc);
        }

        public Boolean fusionarDocumentoWord(string rutaArchivo, Int64 idRequerimiento, Int64 idRequerimientoImei)
        {

            Boolean convertido = true;

            List<BE_VARIABLEPARAMETRO> listaParametros = new List<BE_VARIABLEPARAMETRO>();

            BE_REQUERIMIENTO_IMEI objParametro = new BE_REQUERIMIENTO_IMEI();
            objParametro.idRequerimiento = idRequerimiento;
            objParametro.idRequerimientoImei = idRequerimientoImei;

            BL_PARAMETROGENERAL obj = new BL_PARAMETROGENERAL();
            listaParametros = obj.obtenerValoresParametroDocumento(objParametro);

            try
            {
                using (docPacking.WordprocessingDocument wordDocument = docPacking.WordprocessingDocument.Open(rutaArchivo, true))
                {
                    var body = wordDocument.MainDocumentPart.Document.Body;
                    var paras = body.Elements<docWodProcesing.Paragraph>();                    
                    foreach (var para in paras)
                    {
                        foreach (var run in para.Elements<docWodProcesing.Run>())
                        {
                            foreach (var item in listaParametros)
                            {
                                foreach (var text in run.Elements<docWodProcesing.Text>())
                                {

                                    if (item.variable.Equals(text.Text.ToString()))
                                    {
                                        text.Text = text.Text.Replace(item.variable, item.valorVariable);
                                    }
                                }
                            }
                        }
                    }
                    wordDocument.MainDocumentPart.Document.Save();
                }
            }
            catch (Exception ex)
            {
                 LogError.RegistrarError(ex);
            }
            return convertido;
        }

        [HttpPost]
        public ActionResult RegistrarImeiIndividual(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            Int64 idImeiInsertado = 0;
            try
            {


                BL_REQUERIMIENTO_IMEI objLogicaIMEI = new BL_REQUERIMIENTO_IMEI();
                BE_REQUERIMIENTO_IMEI objSolicitudIMEI = new BE_REQUERIMIENTO_IMEI();

                objSolicitudIMEI.nroImei = formulario["NroImeiAgregar"].ToString();
                objSolicitudIMEI.usuCre = HttpContext.Session.GetString("IdUsuario");

                objSolicitudIMEI.idRequerimiento = Convert.ToInt64(formulario["hdimei"].ToString());
                idImeiInsertado = objLogicaIMEI.InsertarImeiExterno(objSolicitudIMEI);

                if (idImeiInsertado > 0)
                {
                    result = new KeyValuePair<string, string>("1", "IMEI registrado correctamente");
                }

                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al registrar el IMEI ingreado");
                }
                return Json(result);
            }
            catch (Exception)
            {
                result = new KeyValuePair<string, string>("0", "Error al registrar la información");
                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult VerAsignarRequerimientoVarios(IFormCollection formulario)
        {

            BL_USUARIOGPSU ObjLogicaUsuario = new BL_USUARIOGPSU();
            List<BE_USUARIOGPSU> listaUsuario = ObjLogicaUsuario.Listar(UTConstantes.APLICACION, UTConstantes.VALOR_UNO);

            ViewBag.Lista_Usuario = listaUsuario; ;

            return PartialView("_PartialAsignarRequerimientoVarios");
        }

        [HttpPost]
        public ActionResult AsignarRequerimientoVarios(IFormCollection formulario)
        {
            try
            {
                BE_REQUERIMIENTO objEntidad = new BE_REQUERIMIENTO();
                BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();



                var imeiSeleccionados = formulario["hdreqeele"].ToString();
                String usuarioAsignado = formulario["ddlUsuarioVarios"].ToString();
                imeiSeleccionados = imeiSeleccionados.Substring(0, imeiSeleccionados.Length - 1);
                var listaImeiSeleccionado = imeiSeleccionados.Split("-");
                Int64 idAsignacion = 0;
                try
                {
                    if (listaImeiSeleccionado != null)
                    {
                        foreach (var item in listaImeiSeleccionado)
                        {
                            objEntidad.idRequerimiento = Convert.ToInt64(item);
                            objEntidad.usuarioAsignacion = usuarioAsignado;
                            objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario");
                            objEntidad.idEstado = UTConstantes.ID_ESTADO_ASIGNADO;
                            idAsignacion = objLogica.AsignarSolicitud(objEntidad);
                        }
                    }
                    KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                    if (idAsignacion > 0)
                    {
                        result = new KeyValuePair<string, string>("1", "¡Requeriento(s) Asignado(s) correctamente!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Error al registrar la información");
                    }
                    return Json(result);
                }
                catch (Exception)
                {

                    KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al registrar la información");
                    return Json(result);
                }

            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al registrar la información");

                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult DevolverImeiRequerimiento(IFormCollection formulario)
        {
            try
            {
                String comentario = formulario["comentariosDevolver"].ToString();

                BE_REQUERIMIENTO_SOL objEntidadSol = new BE_REQUERIMIENTO_SOL();
                BL_REQUERIMIENTO_SOL objLogicaSol = new BL_REQUERIMIENTO_SOL();

                objEntidadSol.idRequerimiento = Convert.ToInt64(formulario["hdimei"].ToString());
                objEntidadSol.idTipoAtencion = UTConstantes.ID_NOEXISTE_EN_OFICIO;
                objEntidadSol.comentario = comentario;
                objEntidadSol.usuCre = HttpContext.Session.GetString("IdUsuario");
                objEntidadSol.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;
                long idAdjunto = adjuntarArchivoDevolver(formulario);
                objEntidadSol.idDocumentoAdjunto = idAdjunto;


                long idSol = objLogicaSol.Insertar(objEntidadSol);

                BE_REQUERIMIENTO_IMEI objEntidad = new BE_REQUERIMIENTO_IMEI();
                BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI();
                var imeiSeleccionados = formulario["hdimeiseleccion"].ToString();

                imeiSeleccionados = imeiSeleccionados.Substring(0, imeiSeleccionados.Length - 1);
                var listaImeiSeleccionado = imeiSeleccionados.Split("-");


                Int64 idAsignacion = 0;
                try
                {
                    if (listaImeiSeleccionado != null)
                    {
                        foreach (var item in listaImeiSeleccionado)
                        {
                            objEntidad.idRequerimiento = Convert.ToInt64(formulario["hdimei"].ToString());
                            objEntidad.idRequerimientoSol = idSol;

                            objEntidad.idImei = Int32.Parse(item);
                            objEntidad.idTipoMotivo = UTConstantes.ID_TIPOMOTIVODEVOLVER_NOOFICIO;
                            objEntidad.observacion = comentario;
                            objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario");
                            objEntidad.idEstado = UTConstantes.ID_ESTADO_ATENDIDO;

                            idAsignacion = objLogica.DevolverIMEI(objEntidad);
                        }
                    }
                    /*** enviando el correo con el certificado */
                    BL_PARAMETROGENERAL objLogicaParametroCorreo = new BL_PARAMETROGENERAL();
                    BE_VARIABLEPARAMETRO objParametroCorreo = new BE_VARIABLEPARAMETRO();
                    objParametroCorreo.idRequerimientoSol = idSol;
                    objParametroCorreo = objLogicaParametroCorreo.valoresNotificarSigrei(objParametroCorreo);


                    Boolean EnviadoCorreo = false;
                    var seEnvia = configuration["enviarConCertificado"].ToString();
                    if (seEnvia.Equals("1"))
                    {
                        EnviadoCorreo = EnviarCorreoConCertificado(objParametroCorreo);
                    }
                    else
                    {
                        EnviadoCorreo = EnviarCorreoSinCertificado(objParametroCorreo);
                    }

                    KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                    if (idAsignacion > 0)
                    {
                        result = new KeyValuePair<string, string>("1", "¡Devuelto correctamente!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Error al devolver la información");
                    }
                    return Json(result);
                }
                catch (Exception ex)
                {
                    LogError.RegistrarErrorMetodo(ex, "AsignarController > DevolverImeiRequerimiento");

                    KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al devolver la información");
                    return Json(result);
                }

            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "AsignarController > DevolverImeiRequerimiento");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al registrar la información");

                return Json(result);
            }
        }

        #region "Metodos"
        private long adjuntarArchivoDevolver(IFormCollection formulario)
        {
            long idAdjunto = 0;
            try
            {

                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");

                IFormFile fileDocDevolver = formulario.Files["fileDocDevolver"];
                var rutaCarpetaSIGREI = configuration["RutaDocumentos"];
                var nombreArchivoImei = Path.GetFileName(formulario.Files["fileDocDevolver"].FileName);
                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");


                BL_DOCUMENTO_ADJUNTO objLogicaArchivoAdjunto = new BL_DOCUMENTO_ADJUNTO();
                BE_DOCUMENTO_ADJUNTO objAdjunto = new BE_DOCUMENTO_ADJUNTO();

                if (fileDocDevolver.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaSIGREI, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileDocDevolver.CopyTo(fileStream);
                    }
                    objAdjunto.nombreDocumento = nombreArchivoImei;
                    objAdjunto.descDocumento = "Osiptel, Archivo adjuntado al devolver un imei";
                    objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");
                    idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "Registrado documento adjunto al devolver imei=>FrmRequerimientoDerivarIMEI ");
                idAdjunto = 0; 
            }
            return idAdjunto;
        }


        public Boolean EnviarCorreoConCertificado(BE_VARIABLEPARAMETRO objParametro)
        {
            Boolean EnviadoCorreo = false;

            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(objParametro.deParte);
                message.Subject = objParametro.asunto;
                //{
                var Correos = objParametro.para.Split(";");
                foreach (var item in Correos)
                {
                    message.To.Add(new MailAddress(item));
                }

                var conCopia = objParametro.conCopia.Split(";");
                if (conCopia.Length > 0)
                {
                    foreach (var item in conCopia)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }
                var conCopiaOculta = objParametro.conCopiaOculta.Split(";");
                if (conCopiaOculta.Length > 0)
                {
                    foreach (var item in conCopiaOculta)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }

                var certificado = configuration["RutaCertificado"];
                var claveCertificado = configuration["ClaveCertificado"];
                X509Certificate2 objCert = new X509Certificate2(certificado, claveCertificado, X509KeyStorageFlags.MachineKeySet
                             | X509KeyStorageFlags.PersistKeySet
                             | X509KeyStorageFlags.Exportable);

                //Creamos el ContentInfo  
                ContentInfo objContent = new ContentInfo(Encoding.ASCII.GetBytes("Content-Type: text/html; charset=ISO-8859-1;Content-Transfer-Encoding: 7bit \r\n\r\n" + objParametro.mensaje));
                //Creamos el objeto que representa los datos firmados  
                SignedCms objSignedData = new SignedCms(objContent);
                //Creamos el "firmante"  
                CmsSigner objSigner = new CmsSigner(objCert);
                objSigner.IncludeOption = X509IncludeOption.EndCertOnly;
                //Firmamos los datos  
                //mensaje("Antes de firmar los datos");
                objSignedData.ComputeSignature(objSigner);
                //Obtenemos el resultado  
                byte[] bytSigned = objSignedData.Encode();

                CmsRecipientCollection toCollection = new CmsRecipientCollection();

                //foreach (string address in to)


                X509Certificate2 certificate = new X509Certificate2(certificado, claveCertificado, X509KeyStorageFlags.MachineKeySet
                     | X509KeyStorageFlags.PersistKeySet
                     | X509KeyStorageFlags.Exportable);

                CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                toCollection.Add(recipient);
                //}

                MemoryStream stream = new MemoryStream(bytSigned);
                AlternateView view = new AlternateView(stream, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");

                message.AlternateViews.Add(view);

                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient(objParametro.hostname, Int32.Parse(objParametro.puerto));
                var UsuarioRemitente = configuration["UsuarioRemitente"];
                var ClaveRemitente = configuration["ClaveRemitente"];
                var Dominio = configuration["Dominio"];
                client.Credentials = new System.Net.NetworkCredential(UsuarioRemitente, ClaveRemitente, Dominio);


                client.EnableSsl = true;
                client.Send(message);
                EnviadoCorreo = true;
            }
            catch (Exception ex)
            {
                EnviadoCorreo = false;
                LogError.RegistrarErrorMetodo(ex, "EnviarCorreoConCertificado");
            }

            return EnviadoCorreo;
        }
        private Boolean EnviarCorreoSinCertificado(BE_VARIABLEPARAMETRO objParametro)
        {
            Boolean isEnviado = false;
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(objParametro.deParte);
                message.Subject = objParametro.asunto;             
                var Correos = objParametro.para.Split(";");
                foreach (var item in Correos)
                {
                    message.To.Add(new MailAddress(item));
                }

                var conCopia = objParametro.conCopia.Split(";");
                if (conCopia.Length > 0)
                {
                    foreach (var item in conCopia)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }
                var conCopiaOculta = objParametro.conCopiaOculta.Split(";");
                if (conCopiaOculta.Length > 0)
                {
                    foreach (var item in conCopiaOculta)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }
                message.Body = objParametro.mensaje;
                message.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient(objParametro.hostname, Int32.Parse(objParametro.puerto));

                var UsuarioRemitente = configuration["UsuarioRemitente"];
                var ClaveRemitente = configuration["ClaveRemitente"];
                var Dominio = configuration["Dominio"];

                smtpMail.UseDefaultCredentials = false;
                smtpMail.Credentials = new System.Net.NetworkCredential(UsuarioRemitente, ClaveRemitente, Dominio);
                smtpMail.EnableSsl = false;
                smtpMail.Send(message);
                isEnviado = true;



            }
            catch (Exception ex)
            {
                isEnviado = false;
                LogError.RegistrarErrorMetodo(ex, "EnviarCorreoSinCertificado");

            }
            return isEnviado;
        }
        #endregion
    }
}