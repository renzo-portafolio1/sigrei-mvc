﻿using System;
using System.Collections.Generic;


using Microsoft.AspNetCore.Mvc;

using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;


using docPacking = DocumentFormat.OpenXml.Packaging;
using docWodProcesing = DocumentFormat.OpenXml.Wordprocessing;


using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using ClienteSIGREI.Util;
using Newtonsoft.Json;

using System.Net;
using System.IO;
using System.Threading.Tasks;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Style.XmlAccess;
using System.Xml;
using System.Linq;
using System.Net.Mail;

using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class GestorUsuarioController : Controller
    {
        private readonly IConfiguration configuration;

        public GestorUsuarioController(IConfiguration config)
        {
            this.configuration = config;
        }
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult PermisoPerfil()
        {



            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.MODULO, UTConstantes.APLICACION_COLUMNA.IDMODULO);
            ViewBag.Lista_Modulo = lista;

            BlPerfil ObjPer = new BlPerfil();
            List<BePerfil> listaPerfil = ObjPer.ListarPerfilByEstado(UTConstantes.APLICACION);
            ViewBag.Lista_Perfil = listaPerfil;

            return View();
        }

        public IActionResult UsuarioEntidadP()
        {


            BlPerfil ObjLogicaper = new BlPerfil();
            BlParametro ObjLogica = new BlParametro();
            List<BePerfil> lista2;
            List<BE_PARAMETRO> listaMòdulo = new List<BE_PARAMETRO>();
            List<BE_PARAMETRO> lista;
            //CONSULTANDO PERFIL, MODULO Y ENTIDAD PUBLICA DE USUARIOS
            lista2 = ObjLogicaper.ListarPerfilByUsuario(HttpContext.Session.GetString("IdUsuario"), UTConstantes.APLICACION);
            if (lista2.Count == 0)
            {
                lista2 = ObjLogicaper.ListarPerfilByEstado_filtro(UTConstantes.APLICACION);
            }
            ViewBag.ddlTipoPerfil = lista2;


            if (listaMòdulo.Count == 0)
            {
                listaMòdulo = new List<BE_PARAMETRO>();
            }

            ViewBag.ddlModlulo = listaMòdulo;

            lista = ObjLogica.ListarEntidadPByUsuario(HttpContext.Session.GetString("IdUsuario"), UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);

            if (lista.Count == 0)
            {
                lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
            }

            ViewBag.ddlEntidadPublica = lista;
            //

            List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);
            ViewBag.ddlTipoDocumento = listaTipoDocumento;

            List<BeUnidadEp> listaUEPFinal = new List<BeUnidadEp>();
            ViewBag.ddlUnidadEP = listaUEPFinal;

            //List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
            List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores_sexo(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
            ViewBag.ddlSexo = listaSexo;

            List<BeInsExterna> Lista_InsExterna = new List<BeInsExterna>();
            ViewBag.Lista_InsExterna = Lista_InsExterna;

            return View();
        }

        public IActionResult CargaMasicaUsuarioEP()
        {
            List<BePerfil> listaPerfil;
            List<BE_PARAMETRO> listaModuloCM = new List<BE_PARAMETRO>();
            List<BE_PARAMETRO> listaEntidadPu;
            List<BE_PARAMETRO> listaTipoDocumentoCM;
            List<BeUnidadEp> listaUEPFinalCM = new List<BeUnidadEp>();
            List<BE_PARAMETRO> listaSexoCM;
            List<BeInsExterna> Lista_InsExternaCM;

            BlPerfil ObjPerfil = new BlPerfil();
            BlParametro ObjParametro = new BlParametro();

            //CONSULTANDO PERFIL, MODULO Y ENTIDAD PUBLICA DE USUARIOS
            listaPerfil = ObjPerfil.ListarPerfilByUsuario(HttpContext.Session.GetString("IdUsuario"), UTConstantes.APLICACION);
            if (listaPerfil.Count == 0)
            {
                listaPerfil = ObjPerfil.ListarPerfilByEstado_filtro(UTConstantes.APLICACION);
            }

            if (listaModuloCM.Count == 0)
            {
                listaModuloCM = new List<BE_PARAMETRO>();
            }

            listaEntidadPu = ObjParametro.ListarEntidadPByUsuario(HttpContext.Session.GetString("IdUsuario"), UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);

            if (listaEntidadPu.Count == 0)
            {
                listaEntidadPu = ObjParametro.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
            }

            listaTipoDocumentoCM = ObjParametro.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);

            //listaSexoCM = ObjParametro.ListarValores(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
            listaSexoCM = ObjParametro.ListarValores_sexo(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);

            Lista_InsExternaCM = new List<BeInsExterna>();

            ViewBag.ddlSexo = listaSexoCM;
            ViewBag.ddlUnidadEP = listaUEPFinalCM;
            ViewBag.ddlTipoDocumento = listaTipoDocumentoCM;
            ViewBag.ddlEntidadPublica = listaEntidadPu;
            ViewBag.ddlModlulo = listaModuloCM;
            ViewBag.ddlTipoPerfil = listaPerfil;
            ViewBag.Lista_InsExterna = Lista_InsExternaCM;

            return View();
        }

        [HttpPost]
        public ActionResult BuscarUsuariosEP(IFormCollection formulario)
        {

            String nom_vista = "";
            List<BeUsuarioEP> lista;
            List<BeUsuarioEP> listFinalUser;

            BeUsuarioEP ParametroEPBE = new BeUsuarioEP();

            //VALIDANDO CAMPOS QUE SE ENCIAN PARA BUSQUEDA
            int nPerfil = 0;
            string apellidos = "0";
            string nombres = "0";
            string num_doc = "0";
            string nTipDoc = "0";
            int nModulo = 0;
            int nEntPublica = 0;
            string nInsExte = "0";
            int nUniEP = 0;

            if (formulario["ddlTipoPerfil"].ToString() != "")
            {
                nPerfil = Convert.ToInt16(formulario["ddlTipoPerfil"].ToString());
            }
            if (formulario["txtApellidos"].ToString() != "")
            {
                apellidos = formulario["txtApellidos"].ToString();
            }
            if (formulario["txtNombres"].ToString() != "")
            {
                nombres = formulario["txtNombres"].ToString();
            }
            if ( formulario["txtNroDocumento"].ToString() != "")
            {
                num_doc = formulario["txtNroDocumento"].ToString();
            }
            if (formulario["ddlTipoDocumento"].ToString() != "")
            {
                nTipDoc = formulario["ddlTipoDocumento"].ToString();
            }
            if (formulario["ddlModulo"].ToString() != "")
            {
                nModulo = Convert.ToInt16(formulario["ddlModulo"].ToString());
            }
            if (formulario["ddlEPublica"].ToString() != "")
            {
                nEntPublica = Convert.ToInt16(formulario["ddlEPublica"].ToString());
            }
            if (formulario["ddlInsExternaUser"].ToString() != "")
            {
                nInsExte = formulario["ddlInsExternaUser"].ToString();
            }
            if (formulario["ddlUnidadEP"].ToString() != "")
            {
                var arr = formulario["ddlUnidadEP"].ToString().Split("//");
                nUniEP = Convert.ToInt16(arr[0]);
                
            }
            //

            ParametroEPBE.IDPERFIL = nPerfil;
            ParametroEPBE.DES_APEMAT = apellidos;
            ParametroEPBE.IDMODULO = nModulo;
            ParametroEPBE.DES_NOMBRE = nombres;

            ParametroEPBE.IDUNIDADEP = nUniEP;

            ParametroEPBE.COD_DOCIDE = Convert.ToString(nTipDoc);

            ParametroEPBE.NUM_DOCIDE = num_doc;
            ParametroEPBE.INS_EXT = nInsExte;
            ParametroEPBE.IDENTPUBLICA = nEntPublica;



            BlUsuarioEP ObjLogica = new BlUsuarioEP();
            lista = ObjLogica.ListarUsuariosEP(ParametroEPBE);
            listFinalUser = lista;
            nom_vista = "_DetalleUsuarioEP";

            var listaResultadoJSON = JsonConvert.SerializeObject(lista);
            HttpContext.Session.SetString("ListaBusquedaUsusario", listaResultadoJSON);

            

            return PartialView(nom_vista, lista);





        }
        public FileResult DescargarExcelBusqueda()
        {
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");
            string horas = hoy.ToString("HHmmss");

            List<BeUsuarioEP> listFinalUser = new List<BeUsuarioEP>() ;
            listFinalUser = JsonConvert.DeserializeObject<List<BeUsuarioEP>>(HttpContext.Session.GetString("ListaBusquedaUsusario"));

            FileStream fs = new FileStream(configuration["CarpetaTemporal"] + "\\Busqueda de Usuarios"+fecha+"-"+horas+".xlsx", FileMode.Create);
            ExcelPackage Excel = new ExcelPackage(fs);

            /* Creación del estilo. */
            Excel.Workbook.Styles.CreateNamedStyle("Moneda");
            

            /* Creación de hoja de trabajo. */
            Excel.Workbook.Worksheets.Add("Busqueda de Usuario");
            ExcelWorksheet hoja = Excel.Workbook.Worksheets["Busqueda de Usuario"];

            
            hoja.Column(1).Width = 11.29f;

            ExcelRange rango = hoja.Cells["A1"];
            rango.Value = "Nº";

            ExcelRange rangoB = hoja.Cells["B1"];
            rangoB.Value = "Apellidos";

            ExcelRange rangoC = hoja.Cells["C1"];
            rangoC.Value = "Nombres";

            ExcelRange rangoD = hoja.Cells["D1"];
            rangoD.Value = "Usuario";

            ExcelRange rangoE = hoja.Cells["E1"];
            rangoE.Value = "Tipo de Documento";

            ExcelRange rangoF = hoja.Cells["F1"];
            rangoF.Value = "Nº Documento";

            ExcelRange rangoG = hoja.Cells["G1"];
            rangoG.Value = "Teléfono";

            ExcelRange rangoH = hoja.Cells["H1"];
            rangoH.Value = "Celular";

            ExcelRange rangoI = hoja.Cells["I1"];
            rangoI.Value = "Dirección";

            ExcelRange rangoJ = hoja.Cells["J1"];
            rangoJ.Value = "Correo";

            ExcelRange rangoK = hoja.Cells["K1"];
            rangoK.Value = "Módulo";

            ExcelRange rangoL = hoja.Cells["L1"];
            rangoL.Value = "Perfil";

            ExcelRange rangoM = hoja.Cells["M1"];
            rangoM.Value = "Entidad Pública";

            ExcelRange rangoMMM = hoja.Cells["N1"];
            rangoMMM.Value = "Unidad EP";

            ExcelRange rangoN = hoja.Cells["O1"];
            rangoN.Value = "Sub Unidad EP";

            ExcelRange rangoO = hoja.Cells["P1"];
            rangoO.Value = "RUC Unidad EP";

            ExcelRange rangoOOO = hoja.Cells["Q1"];
            rangoOOO.Value = "Cargo";

            ExcelRange rangoP = hoja.Cells["R1"];
            rangoP.Value = "Fecha Inicio";

            ExcelRange rangoQ = hoja.Cells["S1"];
            rangoQ.Value = "Fecha Fin";

            ExcelRange rangoR = hoja.Cells["T1"];
            rangoR.Value = "Fecha de Creación";

            ExcelRange rangoS = hoja.Cells["U1"];
            rangoS.Value = "Usuario de Modificación";

            ExcelRange rangoT = hoja.Cells["V1"];
            rangoT.Value = "Estado";

            ExcelRange rangoU = hoja.Cells["W1"];
            rangoU.Value = "Usuario de Creación";

            int contador = 2;
            for (var i = 0; i < listFinalUser.Count; i++)
            {
                ExcelRange rangoAA = hoja.Cells["A" + contador];

                rangoAA.Value = listFinalUser[i].nroItem;

                ExcelRange rangoBB = hoja.Cells["B" + contador];
                rangoBB.Value = listFinalUser[i].DES_APEPAT;

                ExcelRange rangoCC = hoja.Cells["C" + contador];
                rangoCC.Value = listFinalUser[i].DES_NOMBRE;

                ExcelRange rangoDD = hoja.Cells["D" + contador];
                rangoDD.Value = listFinalUser[i].NUM_DOCIDE;

                ExcelRange rangoEE = hoja.Cells["E" + contador];
                rangoEE.Value = listFinalUser[i].DES_TDOC;

                ExcelRange rangoFF = hoja.Cells["F" + contador];
                rangoFF.Value = listFinalUser[i].NUM_DOCIDE;

                ExcelRange rangoGG = hoja.Cells["G" + contador];
                rangoGG.Value = listFinalUser[i].DES_TLFFIJ;

                ExcelRange rangoHH = hoja.Cells["H" + contador];
                rangoHH.Value = listFinalUser[i].DES_TLFMVL;

                ExcelRange rangoII = hoja.Cells["I" + contador];
                rangoII.Value = listFinalUser[i].DIRECCION;

                ExcelRange rangoJJ = hoja.Cells["J" + contador];
                rangoJJ.Value = listFinalUser[i].CORREO_COMPL;

                ExcelRange rangoKK = hoja.Cells["K" + contador];
                rangoKK.Value = listFinalUser[i].DES_MODU;

                ExcelRange rangoLL = hoja.Cells["L" + contador];
                rangoLL.Value = listFinalUser[i].DES_PERFIL;

                ExcelRange rangoMM = hoja.Cells["M" + contador];
                rangoMM.Value = listFinalUser[i].DES_ENTPUB;

                ExcelRange rangoMMMM = hoja.Cells["N" + contador];
                rangoMMMM.Value = listFinalUser[i].INS_EXT;

                ExcelRange rangoNN = hoja.Cells["O" + contador];
                rangoNN.Value = listFinalUser[i].NOMBREUNIDADP;

                ExcelRange rangoOO = hoja.Cells["P" + contador];
                rangoOO.Value = listFinalUser[i].NUM_RUC;

                ExcelRange rangoOOOO = hoja.Cells["Q" + contador];
                rangoOOOO.Value = listFinalUser[i].CARGO;

                ExcelRange rangoPP = hoja.Cells["R" + contador];
                rangoPP.Style.Numberformat.Format = "@";
                rangoPP.RichText.Text = listFinalUser[i].FECINI;

                ExcelRange rangoQQ = hoja.Cells["S" + contador];
                rangoQQ.Style.Numberformat.Format = "@";
                rangoQQ.RichText.Text = listFinalUser[i].FECEXPCONTRASENIA;

                ExcelRange rangoRR = hoja.Cells["T" + contador];
                rangoRR.Style.Numberformat.Format = "@";
                rangoRR.RichText.Text = listFinalUser[i].FECCRE;

                ExcelRange rangoSS = hoja.Cells["U" + contador];
                rangoSS.Value = listFinalUser[i].USUMOD;

                ExcelRange rangoTT = hoja.Cells["V" + contador];
                rangoTT.Value = listFinalUser[i].desActivo;

                ExcelRange rangoUU = hoja.Cells["W" + contador];
                rangoUU.Value = listFinalUser[i].USUCRE;

                contador++;
            }
            Excel.Save();

            // No estoy seguro de ésta parte, pero mejor cerramos el stream de archivo.
            fs.Close();
            fs.Dispose();

            Excel.Dispose();

            //HttpContext.Session.SetString("BusquedaUsuario", configuration["CarpetaTemporal"] + "\\Busqueda de Usuarios" + fecha + "-" + horas + ".xlsx");
            HttpContext.Session.SetString("BusquedaUsuario", "Busqueda de Usuarios" + fecha + "-" + horas + ".xlsx");

            //var rutaManual = configuration["CredencialUsuario"];
            var rutaManual = configuration["CarpetaTemporal"];
            String nombrePlantilla = HttpContext.Session.GetString("BusquedaUsuario");
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            System.IO.File.Delete(rutaManual);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public FileResult DescargarPlantillaCarga()
        {


            //var rutaManual = configuration["CredencialUsuario"];
            var rutaManual = configuration["PlantillasSIGREI"];
            String nombrePlantilla = "PlantillaCargaMasiva.xlsx";
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult GrabarFinUsuarioEP(IFormCollection formulario)
        {
            bool res_correo = false;
            BeUsuarioEP ParametroEPBE = new BeUsuarioEP();
            List<BeUsuarioEP> lista;
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            Constantes consta = new Constantes();
            Encriptador encry = new Encriptador();

            try
            {
                BlUsuarioEP objParametro = new BlUsuarioEP();

                ParametroEPBE.IDPERFIL = Convert.ToInt16(formulario["ddlTipoPerfil"].ToString());
                ParametroEPBE.DES_APEMAT = formulario["txtSegundoApellido"].ToString().ToUpper();
                ParametroEPBE.IDMODULO = Convert.ToInt16(formulario["ddlModulo"].ToString());
                ParametroEPBE.DES_NOMBRE = formulario["txtPrimNombre"].ToString().ToUpper();
                ParametroEPBE.DES_NOmBRE_LAST = formulario["txtSegundoNombre"].ToString().ToUpper();
                var arr = formulario["ddlUnidadEP"].ToString().Split("//");
                ParametroEPBE.IDUNIDADEP = Convert.ToInt16(arr[0]);
                ParametroEPBE.DES_TLFFIJ = formulario["txtTelefono"].ToString();
                ParametroEPBE.COD_DOCIDE = formulario["ddlTipoDocumento"].ToString();
                ParametroEPBE.DES_TLFMVL = formulario["txtCelular"].ToString();
                ParametroEPBE.NUM_DOCIDE = formulario["txtNroDocumento"].ToString();
                ParametroEPBE.DIRECCION = formulario["txtDireccion"].ToString().ToUpper();
                ParametroEPBE.DES_APEPAT = formulario["txtPrimerApellido"].ToString().ToUpper();
                ParametroEPBE.DES_CORELE = formulario["txtCorreoElectronico"].ToString();
                ParametroEPBE.IND_SEXO = formulario["ddlSexo"].ToString();
                ParametroEPBE.NUM_RUC = formulario["ddlInsExternaUser"].ToString();
                ParametroEPBE.USUCRE = HttpContext.Session.GetString("IdUsuario");
                String nueva_contrasenia = "UEP_" + CreatePassword(6);
                ParametroEPBE.CONTRASENIA = nueva_contrasenia;
               
                ParametroEPBE.CONTRASENIA_ENCR = encry.EncryptStringToBytes_Aes(nueva_contrasenia, consta.Key256, consta.IVAES256);
                ParametroEPBE.CARGO = formulario["txtCargo"].ToString().ToUpper();
                ParametroEPBE.APLICACION = UTConstantes.APLICACION.ToUpper();

                try
                {
                   
                    lista = objParametro.SP_INSERTAR_USUARIO_ENTIDAD_PUBLICA(ParametroEPBE);

                    //if (resultado.Equals("1"))
                    if (lista.Count > 0)
                    {
                        //ENVIANDO CORREO AL USUARIO

                        res_correo = EnviarCorreoSinCertificado(
                            lista[0].HOSTNAME,
                            lista[0].PUERTO,
                            lista[0].DE_PARTE,
                            lista[0].CLAVE_PARTE,
                            lista[0].PARA,
                            lista[0].ASUNTO,
                            lista[0].MENSAJE,
                            lista[0].COPIA);

                        //


                        FileStream fs = new FileStream(configuration["CarpetaTemporal"] + "\\Credenciales de Usuarios.xlsx", FileMode.Create);
                        ExcelPackage Excel = new ExcelPackage(fs);

                        /* Creación del estilo. */
                        Excel.Workbook.Styles.CreateNamedStyle("Moneda");

                        /* Creación de hoja de trabajo. */
                        Excel.Workbook.Worksheets.Add("Credenciales de Usuario");
                        ExcelWorksheet hoja = Excel.Workbook.Worksheets["Credenciales de Usuario"];

                        /* Num Caracteres + 1.29 de Margen.
                        Los índices de columna empiezan desde 1. */
                        hoja.Column(1).Width = 11.29f;

                        ExcelRange rango = hoja.Cells["A1"];

                        rango.Value = "Tipo de Documento";
                        ExcelRange rangoB = hoja.Cells["B1"];

                        rangoB.Value = "Nº de Documento";

                        ExcelRange rangoC = hoja.Cells["C1"];

                        rangoC.Value = "Apellidos";

                        ExcelRange rangoD = hoja.Cells["D1"];

                        rangoD.Value = "Nombres";

                        ExcelRange rangoE = hoja.Cells["E1"];

                        rangoE.Value = "Usuario";

                        ExcelRange rangoF = hoja.Cells["F1"];

                        rangoF.Value = "Clave";


                        ExcelRange rangoAA = hoja.Cells["A2"];

                        rangoAA.Value = lista[0].DES_TDOC;
                        ExcelRange rangoBB = hoja.Cells["B2"];

                        rangoBB.Value = lista[0].NUM_DOCIDE;

                        ExcelRange rangoCC = hoja.Cells["C2"];

                        rangoCC.Value = lista[0].DES_APEPAT.ToUpper();

                        ExcelRange rangoDD = hoja.Cells["D2"];

                        rangoDD.Value = lista[0].DES_NOMBRE.ToUpper();

                        ExcelRange rangoEE = hoja.Cells["E2"];

                        rangoEE.Value = lista[0].NUM_DOCIDE;

                        ExcelRange rangoFF = hoja.Cells["F2"];

                        rangoFF.Value = lista[0].CONTRASENIA;


                        Excel.Save();

                        // No estoy seguro de ésta parte, pero mejor cerramos el stream de archivo.
                        fs.Close();
                        fs.Dispose();

                        Excel.Dispose();

                        HttpContext.Session.SetString("CredencialUsuario", configuration["CarpetaTemporal"] + "\\Credenciales de Usuarios.xlsx");
                        

                        if (res_correo)
                        {
                            result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("1", "¡Registro de Usuario Correcto, Ocurrió un problema el enviar el mail !");
                        }
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                }






            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);

        }

        private Boolean EnviarCorreoSinCertificado(string HOSTNAME, string PUERTO, string DE_PARTE, string CLAVE_PARTE, string PARA, string ASUNTO, string MENSAJE,string COPIA)
        {
            Boolean isEnviado = false;
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(DE_PARTE);
                message.To.Add(new MailAddress(PARA));
                //message.From = new MailAddress("info@chromoperu.com", "SIGREI", System.Text.Encoding.UTF8);
                message.HeadersEncoding = System.Text.Encoding.UTF8;
                message.Subject = ASUNTO;
               
                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.CC.Add(new MailAddress("carlos.sistemas1314@gmail.com"));
                if (COPIA == null)
                {
                    COPIA = "";
                }
                var conCopia = COPIA.Split(";");
                if (conCopia.Length > 0)
                {
                    foreach (var item in conCopia)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }

                message.Body = MENSAJE;

               
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.BodyEncoding = Encoding.GetEncoding("utf-8");
                message.SubjectEncoding = Encoding.GetEncoding("utf-8");
                message.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient(HOSTNAME, Int32.Parse(PUERTO));
                //SmtpClient smtpMail = new SmtpClient("mail.chromoperu.com", Int32.Parse("25"));

                var UsuarioRemitente = configuration["UsuarioRemitente"];
                var ClaveRemitente = configuration["ClaveRemitente"];
                var Dominio = configuration["Dominio"];

                smtpMail.UseDefaultCredentials = false;
                smtpMail.Credentials = new System.Net.NetworkCredential(UsuarioRemitente, ClaveRemitente, Dominio);
                //smtpMail.Credentials = new System.Net.NetworkCredential("info@chromoperu.com", "gHMljBTgK1hN");
                smtpMail.EnableSsl = true;
                //smtpMail.
                smtpMail.Send(message);
                isEnviado = true;



            }
            catch (Exception ex)
            {
                isEnviado = false;
                LogError.RegistrarErrorMetodo(ex, "EnviarCorreoSinCertificado");

            }
            return isEnviado;
        }

        [HttpPost]
        public ActionResult ActualizarrFinUsuarioEP(IFormCollection formulario)
        {

            BeUsuarioEP ParametroEPBE = new BeUsuarioEP();
            List<BeUsuarioEP> lista ;
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            try
            {
                BlUsuarioEP objParametro = new BlUsuarioEP();

                ParametroEPBE.IDPERFIL = Convert.ToInt16(formulario["IDPERFIL"].ToString());
                ParametroEPBE.DES_APEMAT = formulario["DES_APEMAT"].ToString().ToUpper();
                ParametroEPBE.IDMODULO = Convert.ToInt16(formulario["IDMODULO"].ToString());
                ParametroEPBE.DES_NOMBRE = formulario["DES_NOMBRE"].ToString().ToUpper();
                ParametroEPBE.DES_NOmBRE_LAST = formulario["DES_NOmBRE_LAST"].ToString().ToUpper();
                var arr = formulario["nomEntPublica"].ToString().Split("//");
                ParametroEPBE.IDUNIDADEP = Convert.ToInt16(arr[0]);
                ParametroEPBE.DES_TLFFIJ = formulario["DES_TLFFIJ"].ToString();
                ParametroEPBE.COD_DOCIDE = formulario["COD_DOCIDE"].ToString();
                ParametroEPBE.DES_TLFMVL = formulario["DES_TLFMVL"].ToString();
                ParametroEPBE.NUM_DOCIDE = formulario["NUM_DOCIDE"].ToString();
                ParametroEPBE.DIRECCION = formulario["DIRECCION"].ToString().ToUpper();
                ParametroEPBE.DES_APEPAT = formulario["DES_APEPAT"].ToString().ToUpper();
                ParametroEPBE.DES_CORELE = formulario["DES_CORELE"].ToString();
                ParametroEPBE.IND_SEXO = formulario["IND_SEXO"].ToString();
                ParametroEPBE.NUM_RUC = formulario["NUM_RUC"].ToString();
                ParametroEPBE.IDUSUARIO = formulario["hiidValorUser"].ToString();
                ParametroEPBE.IND_ESTADO = formulario["IND_ESTADO"].ToString();
                ParametroEPBE.USUCRE = HttpContext.Session.GetString("IdUsuario");
                ParametroEPBE.CARGO = formulario["CARGO"].ToString().ToUpper();
                ParametroEPBE.APLICACION = UTConstantes.APLICACION.ToUpper();

                try
                {
                    
                    lista = objParametro.SP_ACTUALIZAR_USUARIO_ENTIDAD_PUBLICA(ParametroEPBE);
                    
                    if (lista.Count > 0)
                    {

                        FileStream fs = new FileStream(configuration["CarpetaTemporal"] + "\\Credenciales de Usuarios.xlsx", FileMode.Create);
                        ExcelPackage Excel = new ExcelPackage(fs);

                        /* Creación del estilo. */
                        Excel.Workbook.Styles.CreateNamedStyle("Moneda");
                       

                        /* Creación de hoja de trabajo. */
                        Excel.Workbook.Worksheets.Add("Credenciales de Usuario");
                        ExcelWorksheet hoja = Excel.Workbook.Worksheets["Credenciales de Usuario"];

                        /* Num Caracteres + 1.29 de Margen.
                        Los índices de columna empiezan desde 1. */
                        hoja.Column(1).Width = 11.29f;

                        ExcelRange rango = hoja.Cells["A1"];

                        rango.Value = "Tipo de Oocumento";
                        ExcelRange rangoB = hoja.Cells["B1"];

                        rangoB.Value = "Nº de Documento";

                        ExcelRange rangoC = hoja.Cells["C1"];

                        rangoC.Value = "Apellidos";

                        ExcelRange rangoD = hoja.Cells["D1"];

                        rangoD.Value = "Nombres";

                        ExcelRange rangoE = hoja.Cells["E1"];

                        rangoE.Value = "Usuario";

                        
                        ExcelRange rangoAA = hoja.Cells["A2"];

                        rangoAA.Value = lista[0].DES_TDOC;
                        ExcelRange rangoBB = hoja.Cells["B2"];

                        rangoBB.Value = lista[0].NUM_DOCIDE;

                        ExcelRange rangoCC = hoja.Cells["C2"];

                        rangoCC.Value = lista[0].DES_APEPAT;

                        ExcelRange rangoDD = hoja.Cells["D2"];

                        rangoDD.Value = lista[0].DES_NOMBRE;

                        ExcelRange rangoEE = hoja.Cells["E2"];

                        rangoEE.Value = lista[0].NUM_DOCIDE;

                      
                        Excel.Save();

                        // No estoy seguro de ésta parte, pero mejor cerramos el stream de archivo.
                        fs.Close();
                        fs.Dispose();

                        Excel.Dispose();

                        HttpContext.Session.SetString("CredencialUsuario", configuration["CarpetaTemporal"] + "\\Credenciales de Usuarios.xlsx");
                       

                        result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                }






            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);

        }

        public FileResult DescargarUsuarioCredenciales()
        {
            var rutaManual = configuration["CarpetaTemporal"];
            String nombrePlantilla = HttpContext.Session.GetString("CredencialUsuario");
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult ListarOpcionesPerMod(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlOpcion ObjModOp = new BlOpcion();
            List<BePerfilModuloOpcion> listaModulo_Opcion_Perfil = ObjModOp.ListarModulo_Opcion_By_Modulo_By_Perfil(UTConstantes.APLICACION, Convert.ToInt16(formulario["hiidValorPerfil"].ToString()), Convert.ToInt16(formulario["hiidValorModulo"].ToString()));

            //FORMATEANDO LA LISTA
            modulo.Append("<ul class='list-group'>");
            for (var i = 0; i < listaModulo_Opcion_Perfil.Count; i++)
            {
                if (listaModulo_Opcion_Perfil[i].IDPADRE == 0)
                {
                    modulo.Append("<li class='list-group-item'><strong>*" + listaModulo_Opcion_Perfil[i].DESCRIPCION + "</strong>");
                    modulo.Append("<ul class='list-group'>");
                    for (var j = 0; j < listaModulo_Opcion_Perfil.Count; j++)
                    {
                        if (listaModulo_Opcion_Perfil[i].IDOPCION == listaModulo_Opcion_Perfil[j].IDPADRE && listaModulo_Opcion_Perfil[j].ACTIVO == 1)
                        {
                            modulo.Append("<li class='list-group-item'>" + listaModulo_Opcion_Perfil[j].DESCRIPCION + "</li>");
                        }
                    }
                    modulo.Append("</ul>");
                    modulo.Append("</li>");
                }


            }

            modulo.Append("</ul>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult ListarInsExtByEntPubUser(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlInsExterna ObjUnEP = new BlInsExterna();
            List<BeInsExterna> listaUEP = ObjUnEP.ListarInsExternasByEP(Convert.ToInt16(formulario["ddlEPublica"].ToString()), 1);

            modulo.Append("<select class='input-content' id='ddlInsExternaUser' onchange='listarUEP();' name='ddlInsExternaUser' style='width:100%;'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + listaUEP[i].NUM_RUC.Trim() + "'>" + listaUEP[i].DES_RAZ_SOC + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult ListarProByDepartamentoEdit(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlParametro ObjUnEP = new BlParametro();
            List<BE_PARAMETRO> listaUEP = ObjUnEP.ListarProByDepartamento(formulario["iddepartamento"].ToString().Trim());

            modulo.Append("<select class='input-content' id='idprovincia' name='idprovincia' onchange='ListarPronviciaEdit();' style='width:100%;'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + listaUEP[i].IDPP.Trim() + "'>" + listaUEP[i].PROVINCIA + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult ListarProByDepartamento(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlParametro ObjUnEP = new BlParametro();
            List<BE_PARAMETRO> listaUEP = ObjUnEP.ListarProByDepartamento(formulario["ListaDepartamento"].ToString().Trim());

            modulo.Append("<select class='input-content' id='ListaProvincia' name='ListaProvincia' onchange='listarDistritos();' style='width:100%;'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + listaUEP[i].IDPP.Trim() + "'>" + listaUEP[i].PROVINCIA + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult ListarDisByProvincia(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlParametro ObjUnEP = new BlParametro();
            List<BE_PARAMETRO> listaUEP = ObjUnEP.ListarDisByProvincia(formulario["ListaDepartamento"].ToString().Trim() + formulario["ListaProvincia"].ToString().Trim());

            modulo.Append("<select class='input-content' id='ListaDistrito' name='ListaDistrito' style='width:100%;'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + listaUEP[i].IDDIS.Trim() + "'>" + listaUEP[i].DISTRITO + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult ListarDisByProvinciaEdit(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlParametro ObjUnEP = new BlParametro();
            List<BE_PARAMETRO> listaUEP = ObjUnEP.ListarDisByProvincia(formulario["iddepartamento"].ToString().Trim() + formulario["idprovincia"].ToString().Trim());

            modulo.Append("<select class='input-content' id='iddistrito' name='iddistrito' style='width:100%;'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + listaUEP[i].IDDIS.Trim() + "'>" + listaUEP[i].DISTRITO + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult ListarModuloByPerfil(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlParametro ObjUnEP = new BlParametro();
            List<BE_PARAMETRO> listaUEP = ObjUnEP.ListarModuloByPerfil(formulario["ddlTipoPerfil"].ToString().Trim(), 1);

            modulo.Append("<select class='input-content' id='ddlModulo' name='ddlModulo' style='width:100%;'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + listaUEP[i].valor.Trim() + "'>" + listaUEP[i].descripcion + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }


        [HttpPost]
        public ActionResult ConsultarRuc(IFormCollection formulario)
        {
            
            Task<String> respuesta = null;
            Task<String> respuesta_sunat = null;
            StringBuilder modulo = new StringBuilder();
            modulo.Append("");
            string reto_final = "";
            //CONSULTANDO A BASE DE DATOS ACCESOS PARA SERVICIO WEB

            if (formulario["NUM_RUC"].ToString().Trim() != "")
            {
                //respuesta = GenerarToken(configuration["UsernameToken"], configuration["Passsword"], configuration["Aplicacion"]);
                respuesta = GenerarToken(HttpContext.Session.GetString("IdUsuario"), HttpContext.Session.GetString("ContraWS"), configuration["Aplicacion"]);

                switch (respuesta.Result)
                {
                    case "2":
                        reto_final = "Contraseña o Usuario incorrectos para consultar los servicios de SUNAT//0";
                        break;
                    case "3":
                        reto_final = "Servicio de SUNAT no disponible//0";
                        break;
                    case "1":
                        if (HttpContext.Session.GetString("TokenWS") != "")
                        {
                            respuesta_sunat = ConsultarSunat(HttpContext.Session.GetString("TokenWS"), formulario["NUM_RUC"].ToString().Trim());
                            if (HttpContext.Session.GetString("WSSunatRAZ") != "" && HttpContext.Session.GetString("WSSunatRAZ") != null)
                            {
                                reto_final = HttpContext.Session.GetString("WSSunatRAZ") + "//1";
                            }
                            else
                            {
                                reto_final = "Ingrese un número de RUC correcto//0";
                            }
                        }
                        else
                        {
                            reto_final = "Ocurrió un error al generar la token//0";
                        }
                        break;
                    default:
                        reto_final = "Ocurrió un error al generar la token//0";
                        break;
                }
            }
            else
            {
                reto_final = "Usted debe ingresar un Número de RUC//0";
            }




            return Json(reto_final);

        }

        public static PhysicalAddress GetMacAddress()
        {
            var myInterfaceAddress = NetworkInterface.GetAllNetworkInterfaces()
                .Where(n => n.OperationalStatus == OperationalStatus.Up && n.NetworkInterfaceType != NetworkInterfaceType.Loopback)
                .OrderByDescending(n => n.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                .Select(n => n.GetPhysicalAddress())
                .FirstOrDefault();

            return myInterfaceAddress;
        }

        public async Task<String> ConsultarSunat(string Token, string NumRuc)
        {
            try
            {
                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> listaUrlSunat = ObjLogica.ListarValores_By_Todo_Global(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.URLSUNAT, UTConstantes.APLICACION_TABLA.URLSUNAT);

                //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://serviciospide.osiptel.gob.pe/WSSUNAT/wssunat/getDatosPrincipales");
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(""+ listaUrlSunat[0].valor);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                httpWebRequest.Headers["Authorization"] = "Bearer " + Token;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(new
                    { numruc = NumRuc.ToString() });
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {


                    var result = streamReader.ReadToEnd();
                    Newtonsoft.Json.Linq.JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(result);


                    string Aplicacion = UTConstantes.APLICACION;
                    string idOpcion = "26";
                    string usuario = HttpContext.Session.GetString("IdUsuario");
                    string num_ruc = NumRuc.ToString();
                    string origen = "2";
                    string proceso = "1";
                    string respuesta = (string)jObject["sunatdatprin"]["ddp_nombre"];
                    IPHostEntry host;
                    string localIP = "";
                    host = Dns.GetHostEntry(Dns.GetHostName());
                    foreach (IPAddress ip in host.AddressList)
                    {
                        if (ip.AddressFamily.ToString() == "InterNetwork")
                        {
                            localIP = ip.ToString();
                        }
                    }
                    string mac = Convert.ToString(GetMacAddress());

                    BlInsExterna obj = new BlInsExterna();
                    obj.SP_INSERTAR_REGISTRO_CONSULTA_WS(Aplicacion, idOpcion, usuario, num_ruc, origen, proceso, respuesta, localIP, mac);
                    if ((string)jObject["sunatdatprin"]["ddp_nombre"] != "")
                    {
                        HttpContext.Session.SetString("WSSunatRAZ", (String)jObject["sunatdatprin"]["ddp_nombre"]);
                        return "1";
                    }
                    else
                    {
                        HttpContext.Session.SetString("WSSunatRAZ", "");
                        return "0";
                    }
                }
            }
            catch (Exception )
            {
                //comentado para prueba en error es 0
                return "2";
            }


        }
        public async Task<String> GenerarToken(string Username, string Passsword, string Aplicacion)
        {
            try
            {

                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> listaUrlLogin = ObjLogica.ListarValores_By_Todo_Global(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.URLWSLOGIN, UTConstantes.APLICACION_TABLA.URLWSLOGIN);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(""+ listaUrlLogin[0].valor);
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create(" https://serviciospide.osiptel.gob.pe/WSAUTH/autorizacion/login");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var json = JsonConvert.SerializeObject(new
                    { UserName = Username.ToString(), Password = Passsword.ToString().Trim(), Aplicacion = Aplicacion.ToString().Trim() });
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Newtonsoft.Json.Linq.JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(result);

                    if ((string)jObject["user_iderror"] == "001")
                    {
                        HttpContext.Session.SetString("TokenWS", "");
                        return "2";
                    }
                    else if ((string)jObject["user_iderror"] == "000")
                    {
                        if ((string)jObject["access_token"] != "")
                        {
                            HttpContext.Session.SetString("TokenWS", (string)jObject["access_token"]);
                            return "1";
                        }
                        else
                        {
                            return "0";
                        }
                    }
                    else
                    {
                        HttpContext.Session.SetString("TokenWS", "");
                        return "3";
                    }


                }
            }
            catch (Exception ex)
            {
                //comentado para prueba en error es 0
                return "3";
            }
        }


        [HttpPost]
        public ActionResult ConsurmirServiceRen_EditarUser(IFormCollection formulario)
        {
            String reto_final = "";
            Task<String> respuesta = null;
            Task<String> respuesta_reniec = null;
            StringBuilder modulo = new StringBuilder();
            modulo.Append("");
            //CONSULTANDO A BASE DE DATOS ACCESOS PARA SERVICIO WEB
            BlUsuReniec ObjUnEP = new BlUsuReniec();
            List<BeParReniec> listaUEP = ObjUnEP.ExtrerUsuarioReniec(HttpContext.Session.GetString("IdUsuario"), 1);



            //
            if (listaUEP.Count > 0)
            {
                //respuesta = GenerarToken(configuration["UsernameToken"], configuration["Passsword"], configuration["Aplicacion"]);
                respuesta = GenerarToken(HttpContext.Session.GetString("IdUsuario"), HttpContext.Session.GetString("ContraWS"), configuration["Aplicacion"]);

                switch (respuesta.Result)
                {
                    case "2":
                        reto_final = "0//0//0//0//Contraseña o Usuario incorrectos para consultar los servicios de RENIEC//0";
                        break;
                    case "3":
                        reto_final = "0//0//0//0//Servicio de RENIEC no disponible//2";
                        break;
                    case "1":
                        if (HttpContext.Session.GetString("TokenWS") != "" && HttpContext.Session.GetString("TokenWS") != null)
                        {
                            respuesta_reniec = ServicioWeb(HttpContext.Session.GetString("TokenWS"), formulario["NUM_DOCIDE"].ToString(), listaUEP[0].nuDniUsuario, listaUEP[0].password);

                            switch (respuesta_reniec.Result)
                            {
                                case "1":
                                    if (HttpContext.Session.GetString("WSReniecR") != "" && HttpContext.Session.GetString("WSReniecR") != null)
                                    {
                                        reto_final = HttpContext.Session.GetString("WSReniecR") + "//1";
                                    }
                                    else
                                    {
                                        reto_final = "0//0//0//0//Ingrese un número de DNI correcto//0";
                                    }
                                    break;
                                case "0":
                                    reto_final = "0//0//0//0//" + HttpContext.Session.GetString("ErrorReniec") + "//0";
                                    break;
                                case "2":
                                    reto_final = "0//0//0//0//" + HttpContext.Session.GetString("ErrorReniec") + "//2";
                                    break;
                            }

                           
                        }
                        else
                        {
                            reto_final = "0//0//0//0//Ocurrió un error al generar la token//2";
                        }
                        break;
                    default:
                        reto_final = "0//0//0//0//Ocurrió un error al generar la token//2";
                        break;

                }

            }
            else
            {
                reto_final = "0//0//0//0//El Usuario no Cuenta con Permisos//2";
            }




            return Json(reto_final);

        }
        [HttpPost]
        public ActionResult ConsultarOtroDocumento(IFormCollection formulario)
        {
            String reto_final = "";
            StringBuilder modulo = new StringBuilder();
            modulo.Append("");


            //VALIDANDO SI EL USUARIO YA SE ENCUENTRA REGISTRADO CON EL DNI
            BeUsuarioEP listaUserEdit;
            BlUsuarioEP objParametroD = new BlUsuarioEP();

            listaUserEdit = objParametroD.SP_VALIDAR_USUARIO_EP_BY_DNI(formulario["txtNroDocumento"].ToString());

            if (listaUserEdit.IDUSUARIO != null && listaUserEdit.IDUSUARIO != "")
            {
                reto_final = "0//0//0//0//El número de Documento se encuentra registrado en la Base de Datos de Sigrei//0";
            }
            else
            {
                reto_final = "0//0//0//0//Validación Correcta, Ingrese datos manualmente//2";
            }

            return Json(reto_final);

        }
        [HttpPost]
        public ActionResult ConsurmirServiceRen(IFormCollection formulario)
        {
            String reto_final = "";
            Task<String> respuesta = null;
            Task<String> respuesta_reniec = null;
            StringBuilder modulo = new StringBuilder();
            modulo.Append("");


            //VALIDANDO SI EL USUARIO YA SE ENCUENTRA REGISTRADO CON EL DNI
            BeUsuarioEP listaUserEdit;
            BlUsuarioEP objParametroD = new BlUsuarioEP();

            listaUserEdit = objParametroD.SP_VALIDAR_USUARIO_EP_BY_DNI(formulario["txtNroDocumento"].ToString());

            if (listaUserEdit.IDUSUARIO != null && listaUserEdit.IDUSUARIO != "")
            {
                reto_final = "0//0//0//0//El número de Documento se encuentra registrado en la Base de Datos de Sigrei//0";
            }
            else
            {
                //CONSULTANDO A BASE DE DATOS ACCESOS PARA SERVICIO WEB
                BlUsuReniec ObjUnEP = new BlUsuReniec();
                List<BeParReniec> listaUEP = ObjUnEP.ExtrerUsuarioReniec(HttpContext.Session.GetString("IdUsuario"), 1);
                //
                if (listaUEP.Count > 0)
                {
                    //respuesta = GenerarToken(configuration["UsernameToken"], formulario["txtContrasenaSW"].ToString(), configuration["Aplicacion"]);
                    respuesta = GenerarToken(HttpContext.Session.GetString("IdUsuario"), HttpContext.Session.GetString("ContraWS"), configuration["Aplicacion"]);

                    switch (respuesta.Result)
                    {
                        case "2":
                            reto_final = "0//0//0//0//Contraseña o Usuario incorrectos para consultar los servicios de RENIEC//0";
                            break;
                        case "3":
                            reto_final = "0//0//0//0//Servicio de RENIEC no disponible, ingrese los datos manualmente//2";
                            break;
                        case "1":
                            if (HttpContext.Session.GetString("TokenWS") != "" && HttpContext.Session.GetString("TokenWS") != null)
                            {
                                respuesta_reniec = ServicioWeb(HttpContext.Session.GetString("TokenWS"), formulario["txtNroDocumento"].ToString(), listaUEP[0].nuDniUsuario, listaUEP[0].password);

                                switch (respuesta_reniec.Result)
                                {
                                    case "1":
                                        if (HttpContext.Session.GetString("WSReniecR") != "" && HttpContext.Session.GetString("WSReniecR") != null)
                                        {
                                            reto_final = HttpContext.Session.GetString("WSReniecR") + "//1";
                                        }
                                        else
                                        {
                                            reto_final = "0//0//0//0//Ingrese un número de DNI correcto//0";
                                        }
                                        break;
                                    case "0":
                                        reto_final = "0//0//0//0//" + HttpContext.Session.GetString("ErrorReniec") + "//0";
                                        break;
                                    case "2":
                                        reto_final = "0//0//0//0//" + HttpContext.Session.GetString("ErrorReniec") + "//2";
                                        break;
                                }
                            }
                            else
                            {
                                reto_final = "0//0//0//0//Ocurrió un error al generar la token//2";
                            }
                            break;
                        default:
                            reto_final = "0//0//0//0//Ocurrió un error al generar la token//0";
                            break;
                    }

                }
                else
                {
                    reto_final = "0//0//0//0//El Usuario no Cuenta con Permisos//2";
                }
            }
            return Json(reto_final);

        }

        public async Task<String> ServicioWeb(string Token, string dni, string user, string contrasena)
        {
            try
            {

                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> listaUrlReniec = ObjLogica.ListarValores_By_Todo_Global(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.URLRENIEC, UTConstantes.APLICACION_TABLA.URLRENIEC);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create("" + listaUrlReniec[0].valor);
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://serviciospide.osiptel.gob.pe/WSRENIEC/wsreniec/consultarDni");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                httpWebRequest.Headers["Authorization"] = "Bearer " + Token;

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                using (var streamWriter = new System.IO.StreamWriter(httpWebRequest.GetRequestStream()))
                {

                    var json = JsonConvert.SerializeObject(new
                    { nuDniConsulta = dni.ToString(), nuDniUsuario = user.ToString().Trim(), password = contrasena.ToString().Trim() });
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Newtonsoft.Json.Linq.JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(result);
                    if ((string)jObject["renieccondni"]["coResultado"] == "0000")
                    {
                        String nombre_compl = (String)jObject["renieccondni"]["datosPersona"]["prenombres"];
                        var names = nombre_compl.Split(' ');
                        if (names.Length > 2)
                        {
                            HttpContext.Session.SetString("WSReniecR", (String)jObject["renieccondni"]["datosPersona"]["apPrimer"] + "//" +
                            (String)jObject["renieccondni"]["datosPersona"]["apSegundo"]
                            + "//" + names[0]
                            + "//" + names[1]
                            + "//" + (String)jObject["renieccondni"]["datosPersona"]["direccion"]);
                        }
                        else
                        {
                            HttpContext.Session.SetString("WSReniecR", (String)jObject["renieccondni"]["datosPersona"]["apPrimer"] + "//" +
                            (String)jObject["renieccondni"]["datosPersona"]["apSegundo"]
                            + "//" + names[0]
                            + "//" + " "
                            + "//" + (String)jObject["renieccondni"]["datosPersona"]["direccion"]);
                        }
                        HttpContext.Session.SetString("WSReniecR", (String)jObject["renieccondni"]["datosPersona"]["apPrimer"] + "//" +
                            (String)jObject["renieccondni"]["datosPersona"]["apSegundo"]
                            + "//" + names[0]
                            + "//" + names[1]
                            + "//" + (String)jObject["renieccondni"]["datosPersona"]["direccion"]);
                        return "1";
                    }
                    else
                    {
                        HttpContext.Session.SetString("WSReniecR", "");
                        HttpContext.Session.SetString("ErrorReniec", (String)jObject["renieccondni"]["deResultado"]);
                        return "0";
                    }
                }
            }
            catch (Exception)
            {
                //comentado para prueba en error es 0
                HttpContext.Session.SetString("ErrorReniec", "Servicio de RENIEC no disponible, ingrese datos manualmente");
                return "2";
            }


        }

        [HttpPost]
        public ActionResult ListarInsExtByEntPub(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlInsExterna ObjUnEP = new BlInsExterna();
            List<BeInsExterna> listaUEP = ObjUnEP.ListarInsExternasByEP(Convert.ToInt16(formulario["ddlEPublica"].ToString()), 1);

            modulo.Append("<select class='input-content' id='ddlInsExterna' name='ddlInsExterna' style='width:100%;' title='Tipo de Solicitante'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + Convert.ToString(listaUEP[i].NUM_RUC) + "'>" + listaUEP[i].DES_RAZ_SOC + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult ListarInsExtByEntPubEdit(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlInsExterna ObjUnEP = new BlInsExterna();
            List<BeInsExterna> listaUEP = ObjUnEP.ListarInsExternasByEP(Convert.ToInt16(formulario["idEntidadP"].ToString()), 1);

            modulo.Append("<select class='input-content' id='idInsExterna' name='idInsExterna' style='width:100%;' title='Tipo de Solicitante'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + Convert.ToString(listaUEP[i].NUM_RUC) + "'>" + listaUEP[i].DES_RAZ_SOC + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult ListarInsExtByEntPubEdit_EditarUser(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlInsExterna ObjUnEP = new BlInsExterna();
            List<BeInsExterna> listaUEP = ObjUnEP.ListarInsExternasByEP(Convert.ToInt16(formulario["IDENTPUBLICA"].ToString()), 1);

            modulo.Append("<select class='input-content' id='NUM_RUC' name='NUM_RUC' onchange='numRucChange();' style='width:100%;' title='Tipo de Solicitante'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + Convert.ToString(listaUEP[i].NUM_RUC) + "'>" + listaUEP[i].DES_RAZ_SOC + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult CargarUnidadEPByEP(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlUnidadEp ObjUnEP = new BlUnidadEp();
            List<BeUnidadEp> listaUEP = ObjUnEP.ListaUnidadEPByEstado(formulario["ddlInsExternaUser"].ToString(), 1);

            modulo.Append("<select class='input-content' id='ddlUnidadEP' name='ddlUnidadEP' style='width:100%;' title='Tipo de Solicitante'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + Convert.ToString(listaUEP[i].idUnidadEP) + "//" + "@" + listaUEP[i].extCorreo + "'>" + listaUEP[i].nomUniEntPublica + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult CargarUnidadEPByEP_EditarUser(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            BlUnidadEp ObjUnEP = new BlUnidadEp();
            List<BeUnidadEp> listaUEP = ObjUnEP.ListaUnidadEPByEstado(formulario["NUM_RUC"].ToString(), 1);

            modulo.Append("<select class='input-content' id='nomEntPublica' name='nomEntPublica' style='width:100%;' title='Tipo de Solicitante'>");
            modulo.Append("<option value=''>Seleccione una Opción</option>");
            //FORMATEANDO LA LISTA
            for (var i = 0; i < listaUEP.Count; i++)
            {

                modulo.Append("<option value='" + Convert.ToString(listaUEP[i].idUnidadEP) + "//" + "@" + listaUEP[i].extCorreo + "'>" + listaUEP[i].nomUniEntPublica + "</option>");

            }

            modulo.Append("</select>");



            return Json(modulo.ToString());

        }


        public IActionResult GestionUsuarios()
        {
            BlPerfil ObjLogicaper = new BlPerfil();
            BlParametro ObjLogica = new BlParametro();
            List<BePerfil> lista2;
            List<BE_PARAMETRO> listaMòdulo = new List<BE_PARAMETRO>();
            List<BE_PARAMETRO> lista;
            //CONSULTANDO PERFIL, MODULO Y ENTIDAD PUBLICA DE USUARIOS
            lista2 = ObjLogicaper.ListarPerfilByUsuario(HttpContext.Session.GetString("IdUsuario"), UTConstantes.APLICACION);
            if (lista2.Count == 0)
            {
                lista2 = ObjLogicaper.ListarPerfilByEstado_filtro(UTConstantes.APLICACION);
            }
            ViewBag.ddlTipoPerfil = lista2;

            if (listaMòdulo.Count == 0)
            {
                listaMòdulo = new List<BE_PARAMETRO>();
            }

            ViewBag.ddlModlulo = listaMòdulo;

            lista = ObjLogica.ListarEntidadPByUsuario(HttpContext.Session.GetString("IdUsuario"), UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);

            if (lista.Count == 0)
            {
                lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
            }

            ViewBag.ddlEntidadPublica = lista;

            List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);
            ViewBag.ddlTipoDocumento = listaTipoDocumento;

            List<BeUnidadEp> listaUEP = new List<BeUnidadEp>(); 

            ViewBag.ddlUnidadEP = listaUEP;

            List<BeInsExterna> Lista_InsExterna = new List<BeInsExterna>();
            ViewBag.Lista_InsExterna = Lista_InsExterna;

            return View();
        }

        public IActionResult GestionGeneral()
        {

            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.GESTIONDEPERFIL, UTConstantes.APLICACION_COLUMNA.IDGESTIONDEPERFIL);

            ViewBag.ddlParametro = lista;

            return View();
        }

        public IActionResult Maestros()
        {

            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.GESTIONDEMAESTROS, UTConstantes.APLICACION_COLUMNA.IDGESTIONDEMAESTROS);

            ViewBag.ddlParametro = lista;

            return View();
        }
        [HttpPost]
        public ActionResult NuevoMaestros(IFormCollection formulario)
        {
            BE_PARAMETRO objUsuarioEO = new BE_PARAMETRO();

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("S");
            ViewBag.Lista_Estado = listaEstado;
            String nom_vista = "";
            if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 3)
            {
                nom_vista = "_NuevoParametro";
                return PartialView(nom_vista, objUsuarioEO);
            }
            
            else if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 2)
            {
                BePerfil objPerfil = new BePerfil();

                nom_vista = "_NuevoPerfil";
                return PartialView(nom_vista, objPerfil);
            }
            else if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 1)
            {

                List<TipoMenuBE> listaTipMenu = new GenericoBL().LISTAR_TIPO_MENU();
                ViewBag.Lista_Tip_Menu = listaTipMenu;

                BlOpcion ObjLogica = new BlOpcion();
                List<BeAccesoOpcion> listaMenuPrincipal = ObjLogica.ListarMenuPrincipales(UTConstantes.APLICACION);
                ViewBag.Lista_MPrincipal = listaMenuPrincipal;

                BeAccesoOpcion ParametroEPBE = new BeAccesoOpcion();

                nom_vista = "_NuevoModulo";
                return PartialView(nom_vista, ParametroEPBE);

            }
            
            else
            {
                return null;
            }


        }

        [HttpPost]
        public ActionResult NuevoParametro(IFormCollection formulario)
        {
            BE_PARAMETRO objUsuarioEO = new BE_PARAMETRO();

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("S");
            ViewBag.Lista_Estado = listaEstado;
            String nom_vista = "";
            if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 1 )
            {
                nom_vista = "_NuevoParametro";
                return PartialView(nom_vista, objUsuarioEO);
            }
            else if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 3)
            {
                BeUnidadEp objEntPublica = new BeUnidadEp();
                BL_REPORTE objLogica2 = new BL_REPORTE();
                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
                ViewBag.Lista_EPublica = lista;
                ViewBag.ListaDepartamento = objLogica2.ListaComboUbigeo();

                List<BE_PARAMETRO> Lista_Provincia = new List<BE_PARAMETRO>();
                ViewBag.ListaProvincia = Lista_Provincia;

                List<BE_PARAMETRO> Lista_Distrito = new List<BE_PARAMETRO>();
                ViewBag.ListaDistrito = Lista_Distrito;

                List<BeInsExterna> Lista_InsExterna = new List<BeInsExterna>();
                ViewBag.Lista_InsExterna = Lista_InsExterna;
                nom_vista = "_NuevoUnidadEP";
                return PartialView(nom_vista, objEntPublica);
            }
            
            else if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 2)
            {
                BlParametro ObjLogica = new BlParametro();

                List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION_ACCESO, UTConstantes.APLICACION_TABLA.INS_EXTERNA, UTConstantes.APLICACION_COLUMNA.TIP_INSTEXT);
                ViewBag.Lista_TInstitucion = lista;
                List<BE_PARAMETRO> listaEP = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
                ViewBag.Lista_EPublica = listaEP;
                BeInsExterna ParametroEPBE = new BeInsExterna();

                nom_vista = "_NuevaIEducativa";
                return PartialView(nom_vista, ParametroEPBE);

            }
            else
            {
                return null;
            }


        }

        public BE_PARAMETRO obtenerCriterios(IFormCollection formulario)
        {
            BE_PARAMETRO objParametro = new BE_PARAMETRO();
            objParametro.idParametro = Convert.ToInt32(formulario["ddlParametro"].ToString());
            objParametro.tipoParametro = formulario["tipoParametro"].ToString();




            return objParametro;
        }

        [HttpPost]
        public ActionResult GrabarInsExterna(IFormCollection formulario)
        {
            BeInsExterna ParametroEPBE = new BeInsExterna();

            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            try
            {
                BlInsExterna objParametro = new BlInsExterna();

                ParametroEPBE.NUM_RUC = formulario["NUM_RUC"].ToString();
                ParametroEPBE.DES_RAZ_SOC = formulario["DES_RAZ_SOC"].ToString();
                ParametroEPBE.TIP_INSTEXT = formulario["ddlTInstitucion"].ToString();
                ParametroEPBE.USUCRE = HttpContext.Session.GetString("IdUsuario");
                ParametroEPBE.IDENTPUBLICA = Convert.ToInt16(formulario["ddlEPublica"].ToString());



                //VALIDAR SI EL NUMERO DE RUC SE ENCUENTRA REGISTRADO
                BeInsExterna ValRuc = objParametro.SP_OBTENER_INST_EXTERNA_BY_RUC(formulario["NUM_RUC"].ToString(), HttpContext.Session.GetString("IdUsuario"));
                //

                try
                {
                    if (ValRuc.DES_RAZ_SOC == null || ValRuc.DES_RAZ_SOC == "")
                    {
                        String resultado = objParametro.SP_INERTAR_INSTITUCION_EXTERNA(ParametroEPBE);
                        if (resultado.Equals("1"))
                        {
                            result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                        }
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "El Número de RUC ingresado ya cuenta con registro...");
                    }

                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                }


            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }


        [HttpPost]
        public ActionResult GrabarParametro(IFormCollection formulario)
        {
            BE_PARAMETRO ParametroEPBE = new BE_PARAMETRO();
            KeyValuePair<string, string> result;
            string aplicacion = "";
            string tabla = "";
            string columna = "";

            try
            {
                BlParametro objParametro = new BlParametro();
                //VALIDAR DUPLICIDAD DE PARAMETRO
                List<BE_PARAMETRO> listaIgual;
                listaIgual = objParametro.ValidarDuplicadoParametro(aplicacion, tabla, columna, formulario["descripcion"].ToString(), "0");

                if (listaIgual.Count == 0)
                {
                    //CONSULTAR ULTIMO REGISTRO PARA EXTRAER VALOR DE PARAMETRO
                    String p_valor = "";

                    switch (Convert.ToInt32(formulario["ddlParametro"].ToString()))
                    {
                        case 3:
                            aplicacion = UTConstantes.APLICACION;
                            tabla = UTConstantes.APLICACION_TABLA.MODULO;
                            columna = UTConstantes.APLICACION_COLUMNA.IDMODULO;
                            p_valor = objParametro.Obtener_Codigo_Parametro_By_Registro(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.MODULO, UTConstantes.APLICACION_COLUMNA.IDMODULO);
                            if (p_valor == "")
                            {
                                p_valor = "0";
                            }
                            ParametroEPBE.valor = Convert.ToString((Convert.ToInt32(p_valor) + 1));
                            
                            ParametroEPBE.activo = "1";
                            ParametroEPBE.descripcion = formulario["descripcion"].ToString();
                            ParametroEPBE.abreviado = formulario["abreviado"].ToString();
                            ParametroEPBE.aplicacion = UTConstantes.APLICACION;
                            ParametroEPBE.tabla = UTConstantes.APLICACION_TABLA.MODULO;
                            ParametroEPBE.columna = UTConstantes.APLICACION_COLUMNA.IDMODULO;
                            ParametroEPBE.usuCre = HttpContext.Session.GetString("IdUsuario");
                            break;
                        default:
                            aplicacion = UTConstantes.APLICACION;
                            tabla = UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE;
                            columna = UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE;
                            p_valor = objParametro.Obtener_Codigo_Parametro_By_Registro(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
                            if (p_valor == "")
                            {
                                p_valor = "0";
                            }
                            ParametroEPBE.valor = Convert.ToString((Convert.ToInt32(p_valor) + 1));
                           
                            ParametroEPBE.activo = "1";
                            ParametroEPBE.descripcion = formulario["descripcion"].ToString();
                            ParametroEPBE.abreviado = formulario["abreviado"].ToString();
                            ParametroEPBE.aplicacion = UTConstantes.APLICACION;
                            ParametroEPBE.tabla = UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE;
                            ParametroEPBE.columna = UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE;
                            ParametroEPBE.usuCre = HttpContext.Session.GetString("IdUsuario");
                            break;
                    }

                   
                    try
                    {
                        String resultado = objParametro.SP_INSERTAR_PARAMETRO(ParametroEPBE);
                        if (resultado.Equals("1"))
                        {
                            result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }

                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "¡El Valor ingresado se encuentra registrado...!");
                }
                //



            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult GrabarMenu(IFormCollection formulario)
        {
            BeAccesoOpcion ParametroEPBE = new BeAccesoOpcion();
            KeyValuePair<string, string> result;
            try
            {

                BlOpcion objParametro = new BlOpcion();

                List<BeAccesoOpcion> listarMenu = objParametro.ValidarMenuDuplicado(UTConstantes.APLICACION, formulario["DESCRIPCION"].ToString(), 0);

                if (listarMenu.Count == 0)
                {
                    if (formulario["ddlTipoMenu"].ToString() == "1")
                    {
                        //PRINCIPAL
                        ParametroEPBE.APLICACION = UTConstantes.APLICACION;
                        ParametroEPBE.IDOPCION = "0";
                        ParametroEPBE.RUTA = "#";
                        ParametroEPBE.DESCRIPCION = formulario["DESCRIPCION"].ToString();
                        ParametroEPBE.IDPADRE = "0";
                        ParametroEPBE.ORDEN = "1";
                        ParametroEPBE.FLGRUTA = "0";
                        ParametroEPBE.ESTADO = "1";
                        ParametroEPBE.USUCRE = HttpContext.Session.GetString("IdUsuario");
                    }
                    else
                    {
                        //SECUNDARIO
                        ParametroEPBE.APLICACION = UTConstantes.APLICACION;
                        ParametroEPBE.IDOPCION = "0";
                        ParametroEPBE.RUTA = formulario["RUTA"].ToString();
                        ParametroEPBE.DESCRIPCION = formulario["DESCRIPCION"].ToString();
                        ParametroEPBE.IDPADRE = formulario["ddlMenprincipal"].ToString();
                        ParametroEPBE.ORDEN = "1";
                        ParametroEPBE.FLGRUTA = "1";
                        ParametroEPBE.ESTADO = "1";
                        ParametroEPBE.USUCRE = HttpContext.Session.GetString("IdUsuario");
                    }






                    try
                    {
                        String resultado = objParametro.SP_INSERTAR_MENU(ParametroEPBE);
                        if (resultado.Equals("1"))
                        {
                            result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Existe un Menú creado con la misma descripción...");
                }



            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult GrabarPerfil(IFormCollection formulario)
        {
            BePerfil ParametroEPBE = new BePerfil();
            KeyValuePair<string, string> result;
            try
            {

                BlPerfil objParametro = new BlPerfil();

                List<BePerfil> listaPerfil = objParametro.ValidarPerfilDuplicado(UTConstantes.APLICACION, formulario["descripcion"].ToString(), 0);

                if (listaPerfil.Count == 0)
                {
                    ParametroEPBE.descripcion = formulario["descripcion"].ToString();
                    
                    ParametroEPBE.activo = "1";
                    ParametroEPBE.aplicacion = UTConstantes.APLICACION;
                    ParametroEPBE.usuCre = HttpContext.Session.GetString("IdUsuario");

                    try
                    {
                        String resultado = objParametro.SP_INSERTAR_PERFIL(ParametroEPBE);
                        if (resultado.Equals("0"))
                        {
                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");

                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Existe un Perfil con el nombre ingresado...");
                }



            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult GrabarUnidadEP(IFormCollection formulario)
        {
            BeUnidadEp EntPublicaBE = new BeUnidadEp();
            KeyValuePair<string, string> result;
            try
            {
                BlUnidadEp objUndEntPublica = new BlUnidadEp();

                List<BeUnidadEp> listaUEP = objUndEntPublica.ValidarRegistroUnidadEP(0, formulario["nomUniEntPublica"].ToString());

                switch (listaUEP.Count)
                {
                    case 0:
                        //VALIDANDO CORREO ELECTRONICO
                        BlParametro ObjLogica = new BlParametro();
                        List<BE_PARAMETRO> listaCorreo = ObjLogica.ListarValores_By_Todo_Global(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.DOMINIONOPERMITIDO, UTConstantes.APLICACION_COLUMNA.IDDOMINIO);

                        int bandera = 0;
                        if (listaCorreo.Count > 0)
                        {
                            var arr = listaCorreo[0].descripcion.Split(";");
                            for (var i = 0; i < arr.Length; i++)
                            {
                                if (formulario["extCorreo"].ToString().ToUpper().Trim() == arr[i].ToString().ToUpper().Trim())
                                {
                                    bandera = 1;
                                }
                            }
                        }
                        //
                        switch (bandera)
                        {
                            case 0:
                                EntPublicaBE.idInsExterna = formulario["ddlInsExterna"].ToString();
                                EntPublicaBE.nomUniEntPublica = formulario["nomUniEntPublica"].ToString();
                                EntPublicaBE.telefono = formulario["telefono"].ToString();
                                EntPublicaBE.direccion = formulario["direccion"].ToString();
                                EntPublicaBE.extCorreo = formulario["extCorreo"].ToString();
                                
                                EntPublicaBE.activo = 1;
                                EntPublicaBE.usuCre = HttpContext.Session.GetString("IdUsuario");
                                EntPublicaBE.iddepartamento = Convert.ToString( formulario["ListaDepartamento"].ToString());
                                EntPublicaBE.idprovincia = Convert.ToString(formulario["ListaProvincia"].ToString());
                                EntPublicaBE.iddistrito = Convert.ToString(formulario["ListaDistrito"].ToString());




                                try
                                {
                                    string resultado = objUndEntPublica.SP_INSERTAR_UNIDAD_ENTIDAD_PUBLICA(EntPublicaBE);

                                    switch (resultado.Trim())
                                    {
                                        case "1":
                                            result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                                            break;
                                        default:
                                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                            break;
                                    }

                                }
                                catch (Exception ex)
                                {
                                    LogError.RegistrarError(ex);
                                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                }
                                break;
                            default:
                                result = new KeyValuePair<string, string>("0", "La extensión de correo no es permitida..");
                                break;
                        }
                        break;
                    default:
                        result = new KeyValuePair<string, string>("0", "Existe una Sub Unidad de Entidad Pública con el mismo nombre..");
                        break;
                }

            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ListarMaestros(IFormCollection formulario, BE_PARAMETRO usuarioEPBE)
        {
            String nom_vista = "";
            List<BE_PARAMETRO> lista;

            switch (Convert.ToInt32(formulario["ddlParametro"].ToString()))
            {


                case 2: //PERFILES
                    BlPerfil ObjLogica3 = new BlPerfil();
                    List<BePerfil> listaPerfil = ObjLogica3.ListarPerfil(UTConstantes.APLICACION);
                    nom_vista = "_DetallePerfil";
                    return PartialView(nom_vista, listaPerfil);

                case 1: //MENU
                    List<BeAccesoOpcion> listaModuloFinal = new List<BeAccesoOpcion>();

                    BlOpcion ObjLogica4 = new BlOpcion();
                    List<BeAccesoOpcion> listaModulo = ObjLogica4.ListarOpcionByTodo(UTConstantes.APLICACION);

                    //MODIFICANDO OBJETO
                    BeAccesoOpcion objConsultaBE;
                    var num = 1;
                    for (var i = 0; i < listaModulo.Count; i++)
                    {
                        if (listaModulo[i].IDPADRE == "0")
                        {
                            objConsultaBE = new BeAccesoOpcion();

                            objConsultaBE.NROITEM = num;
                            objConsultaBE.IDOPCION = listaModulo[i].IDOPCION;
                            objConsultaBE.APLICACION = listaModulo[i].APLICACION;
                            objConsultaBE.OBJETO = listaModulo[i].OBJETO;
                            objConsultaBE.RUTA = listaModulo[i].RUTA;

                            objConsultaBE.DESCRIPCION = listaModulo[i].DESCRIPCION;
                            objConsultaBE.IDPADRE = listaModulo[i].IDPADRE;
                            objConsultaBE.ORDEN = listaModulo[i].ORDEN;


                            objConsultaBE.ESTADO = listaModulo[i].ESTADO;



                            listaModuloFinal.Add(objConsultaBE);
                            num++;

                            //AGREGANDO HIJOS AL PADRE
                            for (var j = 0; j < listaModulo.Count; j++)
                            {
                                if (listaModulo[i].IDOPCION == listaModulo[j].IDPADRE)
                                {
                                    objConsultaBE = new BeAccesoOpcion();

                                    objConsultaBE.NROITEM = num;
                                    objConsultaBE.IDOPCION = listaModulo[j].IDOPCION;
                                    objConsultaBE.APLICACION = listaModulo[j].APLICACION;
                                    objConsultaBE.OBJETO = listaModulo[j].OBJETO;
                                    objConsultaBE.RUTA = listaModulo[j].RUTA;

                                    objConsultaBE.DESCRIPCION = listaModulo[j].DESCRIPCION;
                                    objConsultaBE.IDPADRE = listaModulo[j].IDPADRE;
                                    objConsultaBE.ORDEN = listaModulo[j].ORDEN;


                                    objConsultaBE.ESTADO = listaModulo[j].ESTADO;



                                    listaModuloFinal.Add(objConsultaBE);
                                    num++;
                                }
                            }
                        }
                    }

                    nom_vista = "_DetalleModulo";
                    return PartialView(nom_vista, listaModuloFinal);

                case 3://MODULO
                    BlParametro ObjLogica6 = new BlParametro();
                    lista = ObjLogica6.ListarValores_By_Todo(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.MODULO, UTConstantes.APLICACION_COLUMNA.IDMODULO);
                    nom_vista = "_DetalleMaeModulo";
                    return PartialView(nom_vista, lista);
                
                default:
                    return null;

            }




        }

        [HttpPost]
        public ActionResult ListarParametro(IFormCollection formulario, BE_PARAMETRO usuarioEPBE)
        {
            String nom_vista = "";
            List<BE_PARAMETRO> lista;

            switch (Convert.ToInt32(formulario["ddlParametro"].ToString()))
            {
                case 1: //ENTIDAD PUBLICA - BORRAR
                    BlParametro ObjLogica = new BlParametro();
                    lista = ObjLogica.ListarValores_By_Todo(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
                    nom_vista = "_DetalleParametro";
                    return PartialView(nom_vista, lista);

                case 3:
                    BlUnidadEp ObjLogica2 = new BlUnidadEp();
                    List<BeUnidadEp> listaUEP = ObjLogica2.ListarUnidadEP_By_Todo(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
                    ViewBag.Lista_EPublica = listaUEP;


                    nom_vista = "_DetalleUnidadEP";
                    return PartialView(nom_vista, listaUEP);

               
                case 2:
                    BlInsExterna ObjLogica7 = new BlInsExterna();
                    List<BeInsExterna> listaInsExterna;
                    listaInsExterna = ObjLogica7.ListarInstituciones_Externas_By_Todo();
                    nom_vista = "_DetalleInstExterna";
                    return PartialView(nom_vista, listaInsExterna);
                default:
                    return null;

            }




        }

        [HttpPost]
        public ActionResult EditarUsuarioEP(IFormCollection formulario)
        {

            BeUsuarioEP listaUserEdit;
            BlUsuarioEP objParametro = new BlUsuarioEP();

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("S");
            ViewBag.Lista_Estado = listaEstado;

            BlPerfil ObjLogicaper = new BlPerfil();
            BlParametro ObjLogica = new BlParametro();
            List<BePerfil> lista2;
            List<BE_PARAMETRO> listaMòdulo;
            List<BE_PARAMETRO> lista;
            //CONSULTANDO PERFIL, MODULO Y ENTIDAD PUBLICA DE USUARIOS
            lista2 = ObjLogicaper.ListarPerfilByUsuario(HttpContext.Session.GetString("IdUsuario"), UTConstantes.APLICACION);
            if (lista2.Count == 0)
            {
                lista2 = ObjLogicaper.ListarPerfilByEstado(UTConstantes.APLICACION);
            }
            ViewBag.ddlTipoPerfil = lista2;

            listaMòdulo = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.MODULO, UTConstantes.APLICACION_COLUMNA.IDMODULO);

            ViewBag.ddlModlulo = listaMòdulo;

            lista = ObjLogica.ListarEntidadPByUsuario(HttpContext.Session.GetString("IdUsuario"), UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);

            if (lista.Count == 0)
            {
                lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
            }

            ViewBag.ddlEntidadPublica = lista;
            //






            List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);
            ViewBag.ddlTipoDocumento = listaTipoDocumento;

            //List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
            List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores_sexo(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);

            ViewBag.ddlSexo = listaSexo;

            listaUserEdit = objParametro.SP_OBTENER_USUARIO_EP(formulario["hiidValorUser"]);

            BlInsExterna ObjUnEP = new BlInsExterna();
            List<BeInsExterna> listaInst_Externas = ObjUnEP.ListarInsExternasByEP(Convert.ToInt16(listaUserEdit.IDENTPUBLICA), 1);
            ViewBag.Lista_InsExterna = listaInst_Externas;

            BlUnidadEp ObjUnEP2 = new BlUnidadEp();
            List<BeUnidadEp> listaUEP = ObjUnEP2.ListaUnidadEPByEstado(listaUserEdit.NUM_RUC, 1);
            List<BeUnidadEp> listaUEPFinal = new List<BeUnidadEp>();
            BeUnidadEp obj;


            for (var i = 0; i < listaUEP.Count; i++)
            {
                obj = new BeUnidadEp();
                obj.nomEntPublica = Convert.ToString(listaUEP[i].idUnidadEP) + "//" + "@" + listaUEP[i].extCorreo;
                obj.nomUniEntPublica = listaUEP[i].nomUniEntPublica;
                listaUEPFinal.Add(obj);

            }
            ViewBag.ddlUnidadEP = listaUEPFinal;
            return PartialView("_EditarUsuarioEP", listaUserEdit);



        }
        [HttpPost]
        public ActionResult EditarMaestros(IFormCollection formulario)
        {

            BE_PARAMETRO lista;
            BlParametro objParametro = new BlParametro();

            BeUnidadEp listaUEP;
            BlUnidadEp objUnidadEP = new BlUnidadEp();

            BeAccesoOpcion listaOpcion;

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("S");
            ViewBag.Lista_Estado = listaEstado;

            if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 2)
            {
                BlPerfil objPerfil = new BlPerfil();

                BePerfil listaPerfil = objPerfil.SP_OBTENER_PERFIL_BY_CODIGO(Convert.ToInt16(formulario["hiidValor"]), UTConstantes.APLICACION);

                return PartialView("_EditarPerfil", listaPerfil);
            }
            else if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 1)
            {
                List<TipoMenuBE> listaTipMenu = new GenericoBL().LISTAR_TIPO_MENU();
                ViewBag.Lista_Tip_Menu = listaTipMenu;

                BlOpcion ObjLogica = new BlOpcion();
                List<BeAccesoOpcion> listaMenuPrincipal = ObjLogica.ListarMenuPrincipales(UTConstantes.APLICACION);
                ViewBag.Lista_MPrincipal = listaMenuPrincipal;



                listaOpcion = ObjLogica.SP_OBTENER_MENU_BY_CODIGO(formulario["hiidValor"], UTConstantes.APLICACION);


                return PartialView("_EditarModulo", listaOpcion);



            }
            else if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 3)
            {
                lista = objParametro.SP_OBTENER_PARAMETRO_BY_CODIGO(formulario["hiidValor"], UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.MODULO, UTConstantes.APLICACION_COLUMNA.IDMODULO);





                return PartialView("_EditarMaestros", lista);
            }
           
            else
            {
                return null;
            }


        }

        [HttpPost]
        public ActionResult EditarParametro(IFormCollection formulario)
        {

            BE_PARAMETRO lista;
            BlParametro objParametro = new BlParametro();

            BeUnidadEp listaUEP;
            BlUnidadEp objUnidadEP = new BlUnidadEp();

            BeAccesoOpcion listaOpcion;

            List<EstadoBE> listaEstado = new GenericoBL().LISTAR_ESTADOS("S");
            ViewBag.Lista_Estado = listaEstado;

            if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 1)
            {
                lista = objParametro.SP_OBTENER_PARAMETRO_BY_CODIGO(formulario["hiidValor"], UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);





                return PartialView("_EditarParametro", lista);
            }
            else if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 3)
            {
                List<BE_PARAMETRO> listaEP = objParametro.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
                ViewBag.Lista_EPublica = listaEP;
                BlInsExterna ObjLogica77 = new BlInsExterna();
                List<BeInsExterna> listaInsExterna77;
                listaInsExterna77 = ObjLogica77.ListarInstituciones_Externas_By_Todo();
                ViewBag.ddlListaExterna = listaInsExterna77;
                listaUEP = objUnidadEP.SP_OBTENER_UNIDAD_EP_BY_CODIGO(Convert.ToInt16(formulario["hiidValor"]));

                BlParametro ObjUnEP = new BlParametro();
                List<BE_PARAMETRO> listaProvincia = ObjUnEP.ListarProByDepartamento(listaUEP.iddepartamento);
                ViewBag.ListaProvincia = listaProvincia;

                List<BE_PARAMETRO> listaDistrito = ObjUnEP.ListarDisByProvincia(listaUEP.iddepartamento + listaUEP.idprovincia);
                ViewBag.ListaDistrito = listaDistrito;

                BL_REPORTE objLogica2 = new BL_REPORTE();

                ViewBag.ListaDepartamento = objLogica2.ListaComboUbigeo();
                return PartialView("_EditarUnidadEP", listaUEP);
            }
           
            else if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 2)
            {


                List<BE_PARAMETRO> listaTIns = objParametro.ListarValores(UTConstantes.APLICACION_ACCESO, UTConstantes.APLICACION_TABLA.INS_EXTERNA, UTConstantes.APLICACION_COLUMNA.TIP_INSTEXT);
                ViewBag.Lista_TInstitucion = listaTIns;
                List<BE_PARAMETRO> listaEP = objParametro.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
                ViewBag.Lista_EPublica = listaEP;

                BlInsExterna objInsExt = new BlInsExterna();

                BeInsExterna ListaIE = objInsExt.SP_OBTENER_INST_EXTERNA_BY_RUC(formulario["hiidValor"], HttpContext.Session.GetString("IdUsuario"));

                return PartialView("_EditarInsExterna", ListaIE);
            }
            else
            {
                return null;
            }


        }

        [HttpPost]
        public ActionResult ActualizarPerfil(IFormCollection formulario)
        {
            BePerfil ParametroEPBE = new BePerfil();
            KeyValuePair<string, string> result;
            try
            {

                BlPerfil objParametro = new BlPerfil();

                List<BePerfil> listaPerfil = objParametro.ValidarPerfilDuplicado(UTConstantes.APLICACION, formulario["descripcion"].ToString(), Convert.ToInt16(formulario["idPerfil"].ToString()));

                if (listaPerfil.Count == 0)
                {
                    ParametroEPBE.idPerfil = Convert.ToInt16(formulario["idPerfil"].ToString());
                    ParametroEPBE.descripcion = formulario["descripcion"].ToString();
                    ParametroEPBE.activo = formulario["activo"].ToString();

                    ParametroEPBE.aplicacion = UTConstantes.APLICACION;
                    ParametroEPBE.usuMod = HttpContext.Session.GetString("IdUsuario");





                    try
                    {
                        String resultado = objParametro.SP_ACTUALIZAR_PERFIL(ParametroEPBE);
                        if (resultado.Equals("1"))
                        {
                            result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Existe un Perfil con el nombre ingresado..");
                }


            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }


        [HttpPost]
        public ActionResult ActualizarInsExterna(IFormCollection formulario)
        {
            BeInsExterna ParametroEPBE = new BeInsExterna();
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            try
            {
                BlInsExterna objParametro = new BlInsExterna();
                ParametroEPBE.NUM_RUC_ACT = formulario["NUM_RUC_ACT"].ToString();
                ParametroEPBE.NUM_RUC = formulario["NUM_RUC"].ToString();
                ParametroEPBE.DES_RAZ_SOC = formulario["DES_RAZ_SOC"].ToString();
                ParametroEPBE.TIP_INSTEXT = formulario["TIP_INSTEXT"].ToString();
                ParametroEPBE.ACTIVO = formulario["ACTIVO"].ToString();
                ParametroEPBE.IDENTPUBLICA = Convert.ToInt16(formulario["IDENTPUBLICA"].ToString());
                ParametroEPBE.USUMOD = HttpContext.Session.GetString("IdUsuario");
                try
                {
                    String resultado = objParametro.SP_ACTUALIZAR_INSTITUCION_EXTERNA(ParametroEPBE);
                    if (resultado.Equals("1"))
                    {
                        result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                }



            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ActualizarMaestros(IFormCollection formulario)
        {
            BE_PARAMETRO ParametroEPBE = new BE_PARAMETRO();
            KeyValuePair<string, string> result;
            string aplicacion = "";
            string tabla = "";
            string columna = "";
            if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 3)
            {
                aplicacion = UTConstantes.APLICACION;
                tabla = UTConstantes.APLICACION_TABLA.MODULO;
                columna = UTConstantes.APLICACION_COLUMNA.IDMODULO;
            }
            else
            {
                aplicacion = UTConstantes.APLICACION;
                tabla = UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE;
                columna = UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE;
            }


            try
            {
                BlParametro objParametro = new BlParametro();
                List<BE_PARAMETRO> listaIgual;
                listaIgual = objParametro.ValidarDuplicadoParametro(aplicacion, tabla, columna, formulario["descripcion"].ToString(), formulario["valor"].ToString());

                if (listaIgual.Count == 0)
                {
                    if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 3)
                    {
                        ParametroEPBE.valor = formulario["valor"].ToString();
                        ParametroEPBE.activo = formulario["ESTADO"].ToString();
                        ParametroEPBE.descripcion = formulario["descripcion"].ToString();
                        ParametroEPBE.abreviado = formulario["abreviado"].ToString();
                        ParametroEPBE.aplicacion = UTConstantes.APLICACION;
                        ParametroEPBE.tabla = UTConstantes.APLICACION_TABLA.MODULO;
                        ParametroEPBE.columna = UTConstantes.APLICACION_COLUMNA.IDMODULO;
                        ParametroEPBE.usuMod = HttpContext.Session.GetString("IdUsuario");
                    }
                    else
                    {

                        ParametroEPBE.valor = formulario["valor"].ToString();
                        ParametroEPBE.activo = formulario["ESTADO"].ToString();
                        ParametroEPBE.descripcion = formulario["descripcion"].ToString();
                        ParametroEPBE.abreviado = formulario["abreviado"].ToString();
                        ParametroEPBE.aplicacion = UTConstantes.APLICACION;
                        ParametroEPBE.tabla = UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE;
                        ParametroEPBE.columna = UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE;
                        ParametroEPBE.usuMod = HttpContext.Session.GetString("IdUsuario");
                    }









                    try
                    {
                        String resultado = objParametro.SP_ACTUALIZAR_PARAMETRO(ParametroEPBE);
                        if (resultado.Equals("1"))
                        {
                            result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "¡El Valor ingresado se encuentra registrado...!");
                }


            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ActualizarParametro(IFormCollection formulario)
        {
            BE_PARAMETRO ParametroEPBE = new BE_PARAMETRO();
            KeyValuePair<string, string> result;
            string aplicacion = "";
            string tabla = "";
            string columna = "";
            if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 3)
            {
                aplicacion = UTConstantes.APLICACION;
                tabla = UTConstantes.APLICACION_TABLA.MODULO;
                columna = UTConstantes.APLICACION_COLUMNA.IDMODULO;
            }
            else
            {
                aplicacion = UTConstantes.APLICACION;
                tabla = UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE;
                columna = UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE;
            }


            try
            {
                BlParametro objParametro = new BlParametro();
                List<BE_PARAMETRO> listaIgual;
                listaIgual = objParametro.ValidarDuplicadoParametro(aplicacion, tabla, columna, formulario["descripcion"].ToString(), formulario["valor"].ToString());

                if (listaIgual.Count == 0)
                {
                    if (Convert.ToInt32(formulario["ddlParametro"].ToString()) == 3)
                    {
                        ParametroEPBE.valor = formulario["valor"].ToString();
                        ParametroEPBE.activo = formulario["ESTADO"].ToString();
                        ParametroEPBE.descripcion = formulario["descripcion"].ToString();
                        ParametroEPBE.abreviado = formulario["abreviado"].ToString();
                        ParametroEPBE.aplicacion = UTConstantes.APLICACION;
                        ParametroEPBE.tabla = UTConstantes.APLICACION_TABLA.MODULO;
                        ParametroEPBE.columna = UTConstantes.APLICACION_COLUMNA.IDMODULO;
                        ParametroEPBE.usuMod = HttpContext.Session.GetString("IdUsuario");
                    }
                    else
                    {

                        ParametroEPBE.valor = formulario["valor"].ToString();
                        ParametroEPBE.activo = formulario["ESTADO"].ToString();
                        ParametroEPBE.descripcion = formulario["descripcion"].ToString();
                        ParametroEPBE.abreviado = formulario["abreviado"].ToString();
                        ParametroEPBE.aplicacion = UTConstantes.APLICACION;
                        ParametroEPBE.tabla = UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE;
                        ParametroEPBE.columna = UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE;
                        ParametroEPBE.usuMod = HttpContext.Session.GetString("IdUsuario");
                    }


                    try
                    {
                        String resultado = objParametro.SP_ACTUALIZAR_PARAMETRO(ParametroEPBE);
                        if (resultado.Equals("1"))
                        {
                            result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "¡El Valor ingresado se encuentra registrado...!");
                }


            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ActualizarVigenciaUser(IFormCollection formulario)
        {
            BeUsuarioEP ParametroEPBE = new BeUsuarioEP();
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            try
            {
                BlUsuarioEP objParametro = new BlUsuarioEP();

                ParametroEPBE.IDUSUARIO = formulario["hiidValorUser"].ToString();
                ParametroEPBE.FECINI = formulario["fecIniUpd"].ToString();
                ParametroEPBE.FECEXPCONTRASENIA = formulario["fecFinUpd"].ToString();
                ParametroEPBE.USUMOD = HttpContext.Session.GetString("IdUsuario");
                try
                {
                    String resultado = objParametro.SP_ACTUALIZAR_VIGENCIA_USUARIO(ParametroEPBE);
                    if (resultado.Equals("1"))
                    {
                        result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                }



            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ActualizarUnidadEP(IFormCollection formulario)
        {
            BeUnidadEp EntPublicaBE = new BeUnidadEp();
            KeyValuePair<string, string> result;
            try
            {

                BlUnidadEp objParametro = new BlUnidadEp();

                List<BeUnidadEp> listaUEP = objParametro.ValidarRegistroUnidadEP(Convert.ToInt32(formulario["idUnidadEP"].ToString()), formulario["nomUniEntPublica"].ToString());

                switch (listaUEP.Count)
                {
                    case 0:
                        //VALIDANDO CORREO ELECTRONICO
                        BlParametro ObjLogica = new BlParametro();
                        List<BE_PARAMETRO> listaCorreo = ObjLogica.ListarValores_By_Todo_Global(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.DOMINIONOPERMITIDO, UTConstantes.APLICACION_COLUMNA.IDDOMINIO);

                        int bandera = 0;
                        if (listaCorreo.Count > 0)
                        {
                            var arr = listaCorreo[0].descripcion.Split(";");
                            for (var i = 0; i < arr.Length; i++)
                            {
                                if (formulario["extCorreo"].ToString().ToUpper().Trim() == arr[i].ToString().ToUpper().Trim())
                                {
                                    bandera = 1;
                                }
                            }
                        }
                        //
                        switch (bandera)
                        {
                            case 0:
                                EntPublicaBE.idUnidadEP = Convert.ToInt32(formulario["idUnidadEP"].ToString());
                                EntPublicaBE.idInsExterna = formulario["idInsExterna"].ToString();
                                EntPublicaBE.nomUniEntPublica = formulario["nomUniEntPublica"].ToString();
                                EntPublicaBE.telefono = formulario["telefono"].ToString();
                                EntPublicaBE.direccion = formulario["direccion"].ToString();
                                EntPublicaBE.extCorreo = formulario["extCorreo"].ToString();
                                EntPublicaBE.iddepartamento = formulario["iddepartamento"].ToString();
                                EntPublicaBE.idprovincia = formulario["idprovincia"].ToString();
                                EntPublicaBE.iddistrito = formulario["iddistrito"].ToString();
                                EntPublicaBE.activo = Convert.ToInt32(formulario["estado"].ToString());
                                EntPublicaBE.usuCre = HttpContext.Session.GetString("IdUsuario");

                                try
                                {
                                    String resultado = objParametro.SP_ACTUALIZAR_UNIDAD_ENTIDAD_PUBLICA(EntPublicaBE);
                                    switch (resultado.Trim())
                                    {
                                        case "1":
                                            result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                                            break;
                                        default:
                                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                            break;
                                    }
                                    
                                }
                                catch (Exception ex)
                                {
                                    LogError.RegistrarError(ex);
                                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                }
                                break;
                            default:
                                result = new KeyValuePair<string, string>("0", "La extensión de correo no es permitida..");
                                break;
                        }
                        
                        break;
                    default:
                        result = new KeyValuePair<string, string>("0", "Existe una Sub Unidad de Entidad Pública con el mismo nombre..");
                        break;
                }

                
            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ActualizarModulo(IFormCollection formulario)
        {
            BeAccesoOpcion ParametroEPBE = new BeAccesoOpcion();
            KeyValuePair<string, string> result;
            BlOpcion objParametro = new BlOpcion();

            List<BeAccesoOpcion> listarMenu = objParametro.ValidarMenuDuplicado(UTConstantes.APLICACION, formulario["DESCRIPCION"].ToString(), Convert.ToInt16(formulario["IDOPCION"].ToString()));

            if (listarMenu.Count == 0)
            {
                if (formulario["ddlTipoMenu"].ToString() == "1")
                {
                    //PRINCIPAL
                    ParametroEPBE.IDOPCION = formulario["IDOPCION"].ToString();
                    ParametroEPBE.APLICACION = UTConstantes.APLICACION;
                    ParametroEPBE.RUTA = "#";
                    ParametroEPBE.DESCRIPCION = formulario["DESCRIPCION"].ToString();
                    ParametroEPBE.IDPADRE = "0";
                    ParametroEPBE.ORDEN = "1";
                    ParametroEPBE.FLGRUTA = "0";
                    ParametroEPBE.ESTADO = "1";
                    ParametroEPBE.USUCRE = HttpContext.Session.GetString("IdUsuario");
                }
                else
                {
                    //SECUNDARIO
                    ParametroEPBE.IDOPCION = formulario["IDOPCION"].ToString();
                    ParametroEPBE.APLICACION = UTConstantes.APLICACION;
                    ParametroEPBE.RUTA = formulario["RUTA"].ToString();
                    ParametroEPBE.DESCRIPCION = formulario["DESCRIPCION"].ToString();
                    ParametroEPBE.IDPADRE = formulario["IDPADRE"].ToString();
                    ParametroEPBE.ORDEN = "1";
                    ParametroEPBE.FLGRUTA = "1";
                    ParametroEPBE.ESTADO = "1";
                    ParametroEPBE.USUCRE = HttpContext.Session.GetString("IdUsuario");
                }






                try
                {
                    String resultado = objParametro.SP_ACTUALIZAR_MENU(ParametroEPBE);
                    if (resultado.Equals("1"))
                    {
                        
                        result = new KeyValuePair<string, string>("1", "¡Actualización Correcto!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                }
            }
            else
            {
                result = new KeyValuePair<string, string>("0", "Existe un Menú registrado con la misma descripción...");
            }



            return Json(result);
        }

        [HttpPost]
        public ActionResult ListarOpcionesByModulo(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");

            //PRUEBA
            BlOpcion ObjMenu = new BlOpcion();
            List<BeAccesoOpcion> listaModulo = ObjMenu.ListarOpcionByTodo(UTConstantes.APLICACION);
            if (formulario["ddlModulo"].ToString() != "")
            {
                //Consultando si tiene registros en la BD

                BlOpcion ObjModOp = new BlOpcion();
                List<BeModuloOpcion> listaModulo_Opcion = ObjModOp.ListarModulo_Opcion_By_Modulo(UTConstantes.APLICACION, Convert.ToInt16(formulario["ddlModulo"].ToString()));


                if (listaModulo.Count > 0)
                {
                    int contador = 1;
                    for (var i = 0; i < listaModulo.Count; i++)
                    {
                        string conc_hijos = "";

                        switch (listaModulo[i].IDPADRE)
                        {
                            case "0":
                                conc_hijos = "";


                                modulo.Append("<div id='treeview_container' class='hummingbird-treeview well h-scroll-large' style='padding: 10px 0px 0px 15px;width: 60%;margin-bottom: 5px; '> ");
                                modulo.Append("<ul id='treeview" + contador + "' class='hummingbird-base'> ");

                                //VALIDANDO SI EL MENU TIENE HIJOS
                                int ayuda_padre = 0;
                                for (var j = 0; j < listaModulo.Count; j++)
                                {
                                    if (listaModulo[i].IDOPCION == listaModulo[j].IDPADRE)
                                    {
                                        ayuda_padre = 1;
                                    }
                                }
                                //

                                //VALIDANDO SI DEBE ESTAR CHEQUEADO
                                int chec_padre = 0;
                                for (var o = 0; o < listaModulo_Opcion.Count; o++)
                                {
                                    if (Convert.ToInt16(listaModulo[i].IDOPCION) == listaModulo_Opcion[o].IDOPCION && listaModulo_Opcion[o].ACTIVO == 1)
                                    {
                                        chec_padre = 1;
                                    }
                                }
                                //
                                switch (ayuda_padre)
                                {
                                    case 0:
                                        modulo.Append("<li> <label style='font-size: 1em;margin-left: 16px;'> <input id='MenOp" + listaModulo[i].IDOPCION + "' style='    margin-right: 10px;' name='MenOp" + listaModulo[i].IDOPCION + "' onclick='cambiarEstadoPadre(" + listaModulo[i].IDOPCION + ");'  type='checkbox'>");

                                        break;
                                    default:
                                        switch (chec_padre)
                                        {
                                            case 1:
                                                modulo.Append("<li><i class='fa fa-minus'></i> <label style='font-size: 1em;'> <input id='MenOp" + listaModulo[i].IDOPCION + "' style='    margin-right: 10px;' name='MenOp" + listaModulo[i].IDOPCION + "' onclick='cambiarEstadoPadre(" + listaModulo[i].IDOPCION + ");' value='1'  checked  type='checkbox'>");
                                                break;
                                            default:
                                                modulo.Append("<li><i class='fa fa-minus'></i> <label style='font-size: 1em;'> <input id='MenOp" + listaModulo[i].IDOPCION + "' style='    margin-right: 10px;' name='MenOp" + listaModulo[i].IDOPCION + "' onclick='cambiarEstadoPadre(" + listaModulo[i].IDOPCION + ");'   type='checkbox'>");

                                                break;
                                        }
                                        break;
                                }
                                modulo.Append(listaModulo[i].DESCRIPCION + " </label ><ul style = 'display: block;'>");
                                //AGREGANDO HIJOS AL PADRE
                                for (var j = 0; j < listaModulo.Count; j++)
                                {
                                    if (listaModulo[i].IDOPCION == listaModulo[j].IDPADRE)
                                    {
                                        //VALIDANDO SI DEBE ESTAR CHEQUEADO
                                        int chec_hijo = 0;
                                        for (var op = 0; op < listaModulo_Opcion.Count; op++)
                                        {
                                            if (Convert.ToInt16(listaModulo[j].IDOPCION) == listaModulo_Opcion[op].IDOPCION && listaModulo_Opcion[op].ACTIVO == 1)
                                            {
                                                chec_hijo = 1;
                                            }
                                        }
                                        //
                                        conc_hijos += listaModulo[j].IDOPCION + ",";
                                        switch (chec_hijo)
                                        {
                                            case 1:
                                                modulo.Append("<li><label style='font-size: .9em;margin-left: 40px;'> <input id='MenOp" + listaModulo[j].IDOPCION + "'  name='MenOp" + listaModulo[j].IDOPCION + "' onclick='cambiarEstado(" + listaModulo[j].IDOPCION + "," + listaModulo[j].IDPADRE + ");' value='1'  checked  type='checkbox'> " + listaModulo[j].DESCRIPCION + "</label></li> ");
                                                break;
                                            default:
                                                modulo.Append("<li><label style='font-size: .9em;margin-left: 40px;'> <input id='MenOp" + listaModulo[j].IDOPCION + "'  name='MenOp" + listaModulo[j].IDOPCION + "' onclick='cambiarEstado(" + listaModulo[j].IDOPCION + "," + listaModulo[j].IDPADRE + ");' type='checkbox'> " + listaModulo[j].DESCRIPCION + "</label></li> ");
                                                break;
                                        }
                                        
                                    }
                                }
                                modulo.Append("</ul></li></ul></div> ");
                                modulo.Append("<label id='concHij" + listaModulo[i].IDOPCION + "' style='display:none;'>" + conc_hijos + "</label>");
                                contador++;
                                break;

                        }

                    }
                    modulo.Append("<label id='cantTotal' style='display:none;'>" + contador + "</label>");
                }

                //



            }
           
            return Json(modulo.ToString());

        }

        [HttpPost]
        public ActionResult RegistrarModuloMenu(IFormCollection formulario)
        {
            
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                String resultado = "";

                BlOpcion objParametro = new BlOpcion();
                //ELIMINAR PREGUNTAS ASIGNADAS DE UN MODULO
                objParametro.Eliminar_Registro_Modulos(UTConstantes.APLICACION, Convert.ToInt16(formulario["ddlModulo"].ToString()), HttpContext.Session.GetString("IdUsuario"));

                ////////////////////////
                ///
                //INSERTAR REGISTROS
                try
                {

                    BlOpcion ObjMenu = new BlOpcion();
                    List<BeAccesoOpcion> listaModulo = ObjMenu.ListarOpcionByTodo(UTConstantes.APLICACION);

                    for (var i = 0; i < listaModulo.Count; i++)
                    {
                        string cod = "MenOp" + listaModulo[i].IDOPCION;

                        int activo = 0;
                        if (formulario[cod].ToString() == "1")
                        {
                            activo = 1;
                        }
                        else
                        {
                            activo = 0;
                        }
                        BeModuloOpcion ModOp = new BeModuloOpcion();

                        ModOp.IDMODULO = Convert.ToInt16(formulario["ddlModulo"].ToString());
                        ModOp.IDOPCION = Convert.ToInt16(listaModulo[i].IDOPCION);
                        ModOp.ACTIVO = activo;
                        ModOp.USUCRE = HttpContext.Session.GetString("IdUsuario");
                        ModOp.DESCRIPCION = UTConstantes.APLICACION;
                        ModOp.DESCRIPCION = UTConstantes.APLICACION;


                        resultado = objParametro.SP_INSERTAR_MODULO_OPCION(ModOp);
                    }

                    if (resultado.Equals("1"))
                    {
                        result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                catch (Exception ex)
                {
                    LogError.RegistrarError(ex);
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                }

                ////////



            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ListarOpcionesByModuloPer(IFormCollection formulario)
        {

            StringBuilder modulo = new StringBuilder();
            modulo.Append("");
            if (formulario["ddlModuloPer"].ToString() != "")
            {
                //Consultando si tiene registros en la BD
                BlOpcion ObjModOp = new BlOpcion();


                List<BePerfilModuloOpcion> listaModulo_Opcion_Perfil = ObjModOp.ListarModulo_Opcion_By_Modulo_By_Perfil(UTConstantes.APLICACION, Convert.ToInt16(formulario["ddlPerfilPer"].ToString()), Convert.ToInt16(formulario["ddlModuloPer"].ToString()));
                List<BeModuloOpcion> listaModulo_Opcion = ObjModOp.ListarModulo_Opcion_By_Modulo(UTConstantes.APLICACION, Convert.ToInt16(formulario["ddlModuloPer"].ToString()));

                int contador = 1;

                if (listaModulo_Opcion_Perfil.Count > 0)
                {
                    for (var i = 0; i < listaModulo_Opcion.Count; i++)
                    {
                        string conc_hijos = "";

                        switch (listaModulo_Opcion[i].IDPADRE)
                        {
                            case 0:
                                switch (listaModulo_Opcion[i].ACTIVO)
                                {
                                    case 1:
                                        conc_hijos = "";
                                        modulo.Append("<div id='treeview_container' class='hummingbird-treeview well h-scroll-large' style='padding: 10px 0px 0px 15px;width: 60%;margin-bottom: 5px;'> ");
                                        modulo.Append("<ul id='treeviewModP" + contador + "' class='hummingbird-base'> ");
                                        int banderaPadre = 0;
                                        for (var t = 0; t < listaModulo_Opcion_Perfil.Count; t++)
                                        {
                                            if (listaModulo_Opcion_Perfil[t].IDOPCION == listaModulo_Opcion[i].IDOPCION && listaModulo_Opcion_Perfil[t].ACTIVO == 1)
                                            {
                                                banderaPadre = 1;
                                            }
                                        }
                                        switch (banderaPadre)
                                        {
                                            case 0:
                                                modulo.Append("<li><i class='fa fa-minus'></i> <label style='font-size: 1em;'> <input id='MenOpModP" + listaModulo_Opcion[i].IDOPCION + "' style='    margin-right: 10px;'  name='MenOpModP" + listaModulo_Opcion[i].IDOPCION + "' onclick='cambiarEstadoPadreModP(" + listaModulo_Opcion[i].IDOPCION + ");'   type='checkbox'>");
                                                break;
                                            default:
                                                modulo.Append("<li><i class='fa fa-minus'></i> <label style='font-size: 1em;'> <input id='MenOpModP" + listaModulo_Opcion[i].IDOPCION + "' style='    margin-right: 10px;' value='1' checked name='MenOpModP" + listaModulo_Opcion[i].IDOPCION + "' onclick='cambiarEstadoPadreModP(" + listaModulo_Opcion[i].IDOPCION + ");'   type='checkbox'>");
                                                break;
                                        }
                                       
                                        modulo.Append(listaModulo_Opcion[i].DESCRIPCION + " </label ><ul style = 'display: block;'>");


                                        //AGREGANDO HIJOS AL PADRE
                                        for (var j = 0; j < listaModulo_Opcion.Count; j++)
                                        {
                                            if (listaModulo_Opcion[i].IDOPCION == listaModulo_Opcion[j].IDPADRE && listaModulo_Opcion[j].ACTIVO == 1)
                                            {
                                                conc_hijos += listaModulo_Opcion[j].IDOPCION + ",";
                                                int bandera = 0;
                                                for (var k = 0; k < listaModulo_Opcion_Perfil.Count; k++)
                                                {
                                                    if (listaModulo_Opcion_Perfil[k].IDOPCION == listaModulo_Opcion[j].IDOPCION && listaModulo_Opcion_Perfil[k].ACTIVO == 1)
                                                    {
                                                        bandera = 1;
                                                    }
                                                }
                                                switch (bandera)
                                                {
                                                    case 0:
                                                        modulo.Append("<li><label style='font-size: .9em;margin-left: 40px;'> <input id='MenOpModP" + listaModulo_Opcion[j].IDOPCION + "'  name='MenOpModP" + listaModulo_Opcion[j].IDOPCION + "' onclick='cambiarEstadoModP(" + listaModulo_Opcion[j].IDOPCION + "," + listaModulo_Opcion[j].IDPADRE + ");' type='checkbox'> " + listaModulo_Opcion[j].DESCRIPCION + "</label></li> ");
                                                        break;
                                                    default:
                                                        modulo.Append("<li><label style='font-size: .9em;margin-left: 40px;'> <input id='MenOpModP" + listaModulo_Opcion[j].IDOPCION + "'  name='MenOpModP" + listaModulo_Opcion[j].IDOPCION + "' value='1' checked onclick='cambiarEstadoModP(" + listaModulo_Opcion[j].IDOPCION + "," + listaModulo_Opcion[j].IDPADRE + ");' type='checkbox'> " + listaModulo_Opcion[j].DESCRIPCION + "</label></li> ");
                                                        break;
                                                }
                                             
                                            }
                                        }
                                        
                                        modulo.Append("</ul></li></ul></div> ");
                                        modulo.Append("<label id='concHijModP" + listaModulo_Opcion[i].IDOPCION + "' style='display:none;'>" + conc_hijos + "</label>");
                                        contador++;
                                        break;
                                }

                                break;

                        }
                    }
                }
                else
                {
                    for (var i = 0; i < listaModulo_Opcion.Count; i++)
                    {
                        string conc_hijos = "";

                        switch (listaModulo_Opcion[i].IDPADRE)
                        {
                            case 0:

                                switch (listaModulo_Opcion[i].ACTIVO)
                                {
                                    case 1:
                                        conc_hijos = "";
                                        modulo.Append("<div id='treeview_container' class='hummingbird-treeview well h-scroll-large' style='padding: 10px 0px 0px 15px;width: 60%;margin-bottom: 5px;'> ");
                                        modulo.Append("<ul id='treeviewModP" + contador + "' class='hummingbird-base'> ");
                                        modulo.Append("<li><i class='fa fa-minus'></i> <label style='font-size: 1em;'> <input id='MenOpModP" + listaModulo_Opcion[i].IDOPCION + "' style='    margin-right: 10px;' value='1' checked name='MenOpModP" + listaModulo_Opcion[i].IDOPCION + "' onclick='cambiarEstadoPadreModP(" + listaModulo_Opcion[i].IDOPCION + ");'   type='checkbox'>");

                                        modulo.Append(listaModulo_Opcion[i].DESCRIPCION + " </label ><ul style = 'display: block;'>");


                                        //AGREGANDO HIJOS AL PADRE
                                        for (var j = 0; j < listaModulo_Opcion.Count; j++)
                                        {
                                            if (listaModulo_Opcion[i].IDOPCION == listaModulo_Opcion[j].IDPADRE && listaModulo_Opcion[j].ACTIVO == 1)
                                            {
                                                conc_hijos += listaModulo_Opcion[j].IDOPCION + ",";
                                                modulo.Append("<li><label style='font-size: .9em;margin-left: 40px;'> <input id='MenOpModP" + listaModulo_Opcion[j].IDOPCION + "'  name='MenOpModP" + listaModulo_Opcion[j].IDOPCION + "' value='1' checked onclick='cambiarEstadoModP(" + listaModulo_Opcion[j].IDOPCION + "," + listaModulo_Opcion[j].IDPADRE + ");' type='checkbox'> " + listaModulo_Opcion[j].DESCRIPCION + "</label></li> ");


                                            }
                                        }
                                       
                                        modulo.Append("</ul></li></ul></div> ");
                                        modulo.Append("<label id='concHijModP" + listaModulo_Opcion[i].IDOPCION + "' style='display:none;'>" + conc_hijos + "</label>");
                                        contador++;
                                        break;
                                }
                                break;
                        }
                    }
                }

                modulo.Append("<label id='cantTotalModP' style='display:none;'>" + contador + "</label>");
            }

            return Json(modulo.ToString());


        }

        [HttpPost]
        public ActionResult RegistrarModuloMenuPerfil(IFormCollection formulario)
        {
            
            KeyValuePair<string, string> result ;
            try
            {
                String resultado = "";

                BlOpcion objParametro = new BlOpcion();
                BlPerfil ObjPerfil = new BlPerfil();

                //VALIDAR QUE EL PERFIL NO ESTE ASIGNADO A UN MODULO
                List<BePerfil> listaPerfAsig = ObjPerfil.ValidarAsignacionPerfilMod(UTConstantes.APLICACION, Convert.ToInt16(formulario["ddlPerfilPer"].ToString()),
                    Convert.ToInt16(formulario["ddlModuloPer"].ToString()));
                //

                if (listaPerfAsig.Count > 0)
                {
                    result = new KeyValuePair<string, string>("0", "El Perfil seleccionado cuenta con un Módulo asignado, seleccione otro perfil.");
                }
                else
                {

                    //ELIMINAR PREGUNTAS ASIGNADAS DE UN MODULO
                    objParametro.Eliminar_Registro_Modulos_Perfil_Opcion(UTConstantes.APLICACION, Convert.ToInt16(formulario["ddlModuloPer"].ToString()), Convert.ToInt16(formulario["ddlPerfilPer"].ToString()), HttpContext.Session.GetString("IdUsuario"));

                    ////////////////////////
                    ///
                    //INSERTAR REGISTROS
                    try
                    {

                        BlOpcion ObjMenu = new BlOpcion();
                        List<BeModuloOpcion> listaModulo = ObjMenu.ListarModulo_Opcion_By_Modulo(UTConstantes.APLICACION, Convert.ToInt16(formulario["ddlModuloPer"].ToString()));

                        for (var i = 0; i < listaModulo.Count; i++)
                        {
                            switch (listaModulo[i].ACTIVO)
                            {
                                case 1:
                                    string cod = "MenOpModP" + listaModulo[i].IDOPCION;

                                    int activo = 0;
                                    switch (formulario[cod].ToString())
                                    {
                                        case "1":
                                            activo = 1;
                                            break;
                                        default:
                                            activo = 0;
                                            break;
                                    }
                                  
                                    BePerfilModuloOpcion ModOp = new BePerfilModuloOpcion();

                                    ModOp.IDMODULO = Convert.ToInt16(formulario["ddlModuloPer"].ToString());
                                    ModOp.IDPERFIL = Convert.ToInt16(formulario["ddlPerfilPer"].ToString());
                                    ModOp.IDOPCION = Convert.ToInt16(listaModulo[i].IDOPCION);
                                    ModOp.ACTIVO = activo;
                                    ModOp.USUCRE = HttpContext.Session.GetString("IdUsuario");
                                    ModOp.DESCRIPCION = UTConstantes.APLICACION;


                                    resultado = objParametro.SP_INSERTAR_MODULO_OPCION_PERFIL(ModOp);
                                    break;
                            }

                        }

                        switch (resultado.Trim())
                        {
                            case "1":
                                result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                                break;
                                default:
                                result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                break;
                        }

                       
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }

                    ////////
                }




            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }


        public ActionResult RecargarPaginaCarga()
        {

            return RedirectToAction("CargaMasicaUsuarioEP", "GestorUsuario");
        }

        public FileResult DescargarError()
        {
            var rutaDocumentosPlantilla = configuration["CarpetaTemporal"];
            var archivoError = Path.Combine(rutaDocumentosPlantilla, "Error.txt");
            if (System.IO.File.Exists(archivoError))
            {
                System.IO.File.Delete(archivoError);
            }
            List<ErrorUser> listaError;
            listaError = JsonConvert.DeserializeObject<List<ErrorUser>>(HttpContext.Session.GetString("listaErrorCargaMasivaUser"));

            List<ErrorUser> listaErrorErrores = new List<ErrorUser>();
            var listaErrror = JsonConvert.SerializeObject(listaErrorErrores);
            HttpContext.Session.SetString("listaErrorCargaMasivaUser", listaErrror);

            if (listaError != null && listaError.Count > 0)
            {
                listaError = listaError.OrderBy(m => m.nroItem).ToList();
                using (System.IO.StreamWriter Archifile = new System.IO.StreamWriter(archivoError, true))
                {
                    Archifile.WriteLine("------ERRORES ENCONTRADOS EN EL DOCUMENTO CARGADO----");
                    foreach (var item in listaError)
                    {
                        Archifile.WriteLine("Fila  " + item.nroItem + ": Error==>" + item.error);
                    }
                    Archifile.Dispose();
                }
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoError);
            string fileName = "Error.txt";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult EliminarUsuarioCargaMasiva(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result;

                BlUsuarioEP obj = new BlUsuarioEP();
                List<BeUsuarioEP> listaUEPFinal = obj.EliminarDatosTemporalUser(formulario["hiidValorElimCar"].ToString());

                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);


                //List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
                List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores_sexo(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
                //ARMANDO NUEVO HTML
                string cadena_html_user = "";
                cadena_html_user += "<div class='tabla-content-result'>";
                cadena_html_user += "<table id='dtResultadUserCargaPrevia' class='tabla-osiptel' style='width: 100%'>";
                cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_html_user += " <thead>";
                cadena_html_user += "<tr>";
                cadena_html_user += "<th scope='col'>Tipo Documento</th>";
                cadena_html_user += "<th scope='col'>Nº Documento</th>";
                cadena_html_user += "<th scope='col'>Telefono</th>";
                cadena_html_user += "<th scope='col'>Celular</th>";
                cadena_html_user += "<th scope='col'>Correo</th>";
                cadena_html_user += "<th scope='col'>Sexo</th>";
                cadena_html_user += "<th scope='col'>Nombre</th>";
                cadena_html_user += "<th scope='col'>Segundo Nombre</th>";
                cadena_html_user += "<th scope='col'>Apellido Paterno</th>";
                cadena_html_user += "<th scope='col'>Apellido Materno</th>";
                cadena_html_user += "<th scope='col'>Dirección</th>";
                cadena_html_user += "<th scope='col'>Opciones</th>";
                cadena_html_user += "</tr>";
                cadena_html_user += " </thead>";
                cadena_html_user += "<tbody>";
                for (var i = 0; i < listaUEPFinal.Count; i++)
                {

                    cadena_html_user += " <tr>";
                    var nom_tip_doc = "";
                    for (var k = 0; k < listaTipoDocumento.Count; k++)
                    {
                        if (listaTipoDocumento[k].valor == listaUEPFinal[i].COD_DOCIDE.ToString())
                        {
                            nom_tip_doc = listaTipoDocumento[k].descripcion;
                        }
                    }

                    var nom_tip_sex = "";
                    for (var k = 0; k < listaSexo.Count; k++)
                    {
                        if (listaSexo[k].valor.ToUpper() == listaUEPFinal[i].IND_SEXO.ToString().ToUpper())
                        {
                            nom_tip_sex = listaSexo[k].descripcion;
                        }
                    }
                    cadena_html_user += "<td> " + nom_tip_doc + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].NUM_DOCIDE.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].DES_TLFFIJ.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].DES_TLFMVL.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].DES_CORELE.ToString() + "</td>";
                    cadena_html_user += "<td> " + nom_tip_sex + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].DES_NOMBRE.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].DES_NOmBRE_LAST.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].DES_APEPAT.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].DES_APEMAT.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEPFinal[i].DIRECCION.ToString() + "</td>";
                    cadena_html_user += "<td class='text-center'> <a href='#' onclick='fn_EditarUsuarioCargaM(\"" + listaUEPFinal[i].NUM_DOCIDE.ToString() + "\")' class='fa fa-edit fa-lg' title='Consultar'></a> <a href='#' onclick='fn_EliminarUsuarioCargaM(\"" + listaUEPFinal[i].NUM_DOCIDE.ToString() + "\")' class='fa fa-remove fa-lg' title='Consultar'></a></td>";
                    cadena_html_user += "</tr>";
                }
                cadena_html_user += " </tbody>";
                cadena_html_user += "   </table>";
                cadena_html_user += "</div>";
                ViewBag.listaUEP_Grilla_Final = listaUEPFinal;
                result = new KeyValuePair<string, string>("1", "Se Elimino el Usuario Seleccionado  //" + cadena_html_user);
                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult CargarInformacionMasivaUsuario(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result = CargarInformacion(formulario);

                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }
        int validar_error_general = 0;
        private KeyValuePair<string, string> CargarInformacion(IFormCollection formulario)
        {
            String mensaje = "";
            int cont_correcta = 0;
            KeyValuePair<string, string> keyValueError;

            Boolean existeRegistro = false;

            List<ErrorUser> listaErrors = new List<ErrorUser>();
            var listaErrorJSON = JsonConvert.SerializeObject(listaErrors);
            HttpContext.Session.SetString("listaErrorCargaMasivaUser", listaErrorJSON);

            string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileCargaMasiva"].FileName);

            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();

            IFormFile archivo = formulario.Files["fileCargaMasiva"];
            if (archivo.Length / 1024 < TamFileGrande)//2MB
            {
                if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                {
                    keyValueError = new KeyValuePair<string, string>("", "");
                }
                else
                {
                    mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                    keyValueError = new KeyValuePair<string, string>("0", mensaje);
                }
            }
            else
            {
                keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);

            }

            switch (keyValueError.Key.Trim())
            {
                case "":
                    List<BeUsuarioEP> listaResultado = new List<BeUsuarioEP>();
                    BeUsuarioEP objParametroDos;
                    Constantes consta = new Constantes();
                    Encriptador encry = new Encriptador();

                    #region "Copiar a una carpeta temporal para revisar si hay registro que registrar"


                    DateTime fechaDos = DateTime.Now;
                    string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                    IFormFile fileArchivoImei = formulario.Files["fileCargaMasiva"];
                    var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                    string nombreArchivoImei = Path.GetFileName(formulario.Files["fileCargaMasiva"].FileName);

                    nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                    var ubicacionArchivoTemporal = "";

                    if (fileArchivoImei.Length > 0)
                    {
                        var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            fileArchivoImei.CopyTo(fileStream);
                        }
                        ubicacionArchivoTemporal = filePath;
                    }
                    FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                    HttpContext.Session.SetString("nombreArchivoCarMasUsuario", nombreArchivoImei);

                    using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                    {
                        try
                        {
                            ExcelWorksheet workSheet = package.Workbook.Worksheets["Carga Masiva Usuario"];
                            int totalRows = workSheet.Dimension.Rows;

                            if (totalRows < 2)
                            {
                                return keyValueError = new KeyValuePair<string, string>("2", "No existe información en el archivo adjunto");
                            }
                            else
                            {
                                try
                                {
                                    for (int i = 2; i <= totalRows; i++)
                                    {
                                        objParametroDos = new BeUsuarioEP();
                                        objParametroDos.nroItem = i;
                                        objParametroDos.COD_DOCIDE = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim().Replace(",", ".") : "";
                                        objParametroDos.NUM_DOCIDE = workSheet.Cells[i, 2].Value != null ? workSheet.Cells[i, 2].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.DES_APEPAT = workSheet.Cells[i, 3].Value != null ? workSheet.Cells[i, 3].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.DES_APEMAT = workSheet.Cells[i, 4].Value != null ? workSheet.Cells[i, 4].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.DES_NOMBRE = workSheet.Cells[i, 5].Value != null ? workSheet.Cells[i, 5].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.DES_NOmBRE_LAST = workSheet.Cells[i, 6].Value != null ? workSheet.Cells[i, 6].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.DES_TLFFIJ = workSheet.Cells[i, 7].Value != null ? workSheet.Cells[i, 7].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.DES_TLFMVL = workSheet.Cells[i, 8].Value != null ? workSheet.Cells[i, 8].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.DIRECCION = workSheet.Cells[i, 9].Value != null ? workSheet.Cells[i, 9].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.IND_SEXO = workSheet.Cells[i, 10].Value != null ? workSheet.Cells[i, 10].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.DES_CORELE = workSheet.Cells[i, 11].Value != null ? workSheet.Cells[i, 11].Value.ToString().Trim().ToLower() : "";
                                        objParametroDos.CARGO = workSheet.Cells[i, 12].Value != null ? workSheet.Cells[i, 12].Value.ToString().Trim().ToLower() : "";
                                        String nueva_contrasenia = "UEP_" + CreatePassword(6);
                                        objParametroDos.CONTRASENIA = nueva_contrasenia;
                                       
                                        objParametroDos.CONTRASENIA_ENCR = encry.EncryptStringToBytes_Aes(nueva_contrasenia, consta.Key256, consta.IVAES256);
                                      
                                        listaResultado.Add(objParametroDos);

                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogError.RegistrarErrorMetodo(ex, "CargarInformacion=> error al cargar la data ");
                                    return keyValueError = new KeyValuePair<string, string>("0", "Error en la obtención de los datos");
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            LogError.RegistrarErrorMetodo(ex, "CargarInformacion=> error al cargar la data ");
                            return keyValueError = new KeyValuePair<string, string>("0", UTConstantes.PLANTILLA_EXCEL_NO_EXISTE_HOJA_DATA);
                        }
                    }
                    #endregion
                    switch (listaResultado.Count)
                    {
                        case 0:
                            existeRegistro = false;
                            break;
                        default:
                            existeRegistro = true;
                            break;
                    }
                    
                    if (existeRegistro)
                    {
                        listaResultado = listaResultado.OrderBy(o => o.NUM_DOCIDE).ToList();

                        for (int i = 0; i < listaResultado.Count; i++)
                        {
                            validar_error_general = 0;
                            listaResultado[i] = validarDocumentoExcel(listaResultado[i]);

                            switch (validar_error_general)
                            {
                                case 1:
                                    cont_correcta++;
                                    break;
                            }
                           
                        }

                        
                        
                        listaErrors = JsonConvert.DeserializeObject<List<ErrorUser>>(HttpContext.Session.GetString("listaErrorCargaMasivaUser"));
                        switch (listaErrors)
                        {
                            case null:
                                listaErrors = new List<ErrorUser>();
                                break;
                        }
                       
                        XmlDocument objXMLIMEI = new XmlDocument();
                        XmlElement nodeUno = objXMLIMEI.CreateElement("ListaCargaMasivaUser");
                        objXMLIMEI.AppendChild(nodeUno);

                        //BlUsuReniec ObjUnEP = new BlUsuReniec();
                        //ObjUnEP.ExtrerUsuarioReniec(HttpContext.Session.GetString("IdUsuario"), 1);

                        BeUsuarioEP ObjUserEP;
                        List<BeUsuarioEP> listaUEP_Grilla = new List<BeUsuarioEP>();

                        string cadena_html_user = "";
                        cadena_html_user += "<div class='tabla-content-result'>";
                        cadena_html_user += "<table id='dtResultadUserCargaPrevia' class='tabla-osiptel' style='width: 100%'>";
                        cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                        cadena_html_user += " <thead>";
                        cadena_html_user += "<tr>";
                        cadena_html_user += "<th scope='col'>Tipo Documento</th>";
                        cadena_html_user += "<th scope='col'>Nº Documento</th>";
                        cadena_html_user += "<th scope='col'>Apellido Paterno</th>";
                        cadena_html_user += "<th scope='col'>Apellido Materno</th>";
                        cadena_html_user += "<th scope='col'>Nombre</th>";
                        cadena_html_user += "<th scope='col'>Segundo Nombre</th>";
                        cadena_html_user += "<th scope='col'>Telefono</th>";
                        cadena_html_user += "<th scope='col'>Celular</th>";
                        cadena_html_user += "<th scope='col'>Dirección</th>";
                        cadena_html_user += "<th scope='col'>Sexo</th>";
                        cadena_html_user += "<th scope='col'>Correo</th>";
                        cadena_html_user += "<th scope='col'>Cargo</th>";




                        cadena_html_user += "<th scope='col'>Opciones</th>";
                        cadena_html_user += "</tr>";
                        cadena_html_user += " </thead>";
                        cadena_html_user += "<tbody>";


                        BlParametro ObjLogica = new BlParametro();
                        List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);


                        //List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
                        List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores_sexo(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
                        string cadena_tip_doc = "";
                        string cadena_num_doc = "";
                        string cadena_num_tlf = "";
                        string cadena_num_cel = "";
                        string cadena_tip_sexo = "";
                        string cadena_correo = "";
                        string cadena_prim_nombre = "";
                        string cadena_sec_nombre = "";
                        string cadena_pri_apellido = "";
                        string cadena_sec_apellido = "";
                        string cadena_sec_direccion = "";
                        string cadena_cargo = "";
                        string cadena_contrasenia = "";
                        string cadena_contrasenia_encry = "";

                        for (int i = 0; i < listaResultado.Count; i++)
                        {
                            if (listaResultado[i].NUM_DOCIDE != null && listaResultado[i].NUM_DOCIDE.ToString() != "")
                            {
                                
                                //VALIDANDO CON SERVICIO WEB DE REUNIEC
                                
                                String reto_final = "";
                                reto_final = "1";

                                switch (reto_final.Trim())
                                {
                                    case "1":
                                        ObjUserEP = new BeUsuarioEP();
                                        ObjUserEP.COD_DOCIDE = listaResultado[i].COD_DOCIDE.ToString();
                                        ObjUserEP.NUM_DOCIDE = listaResultado[i].NUM_DOCIDE.ToString();
                                        ObjUserEP.DES_TLFFIJ = listaResultado[i].DES_TLFFIJ.ToString();
                                        ObjUserEP.DES_TLFMVL = listaResultado[i].DES_TLFMVL.ToString();
                                        ObjUserEP.DES_CORELE = listaResultado[i].DES_CORELE.ToString();
                                        ObjUserEP.IND_SEXO = listaResultado[i].IND_SEXO.ToString();
                                        ObjUserEP.CARGO = listaResultado[i].CARGO.ToString();
                                        ObjUserEP.CONTRASENIA = listaResultado[i].CONTRASENIA.ToString();
                                        ObjUserEP.CONTRASENIA_ENCR = listaResultado[i].CONTRASENIA_ENCR.ToString();

                                        ObjUserEP.DES_NOMBRE = listaResultado[i].DES_NOMBRE.ToString().ToUpper();
                                        ObjUserEP.DES_NOmBRE_LAST = listaResultado[i].DES_NOmBRE_LAST.ToString().ToUpper();
                                        ObjUserEP.DES_APEPAT = listaResultado[i].DES_APEPAT.ToString().ToUpper();
                                        ObjUserEP.DES_APEMAT = listaResultado[i].DES_APEMAT.ToString().ToUpper();
                                        ObjUserEP.DIRECCION = listaResultado[i].DIRECCION.ToString().ToUpper();



                                        listaResultado[i].DES_NOMBRE = listaResultado[i].DES_NOMBRE.ToString().ToUpper();
                                        listaResultado[i].DES_NOmBRE_LAST = listaResultado[i].DES_NOmBRE_LAST.ToString().ToUpper();
                                        listaResultado[i].DES_APEPAT = listaResultado[i].DES_APEPAT.ToString().ToUpper();
                                        listaResultado[i].DES_APEMAT = listaResultado[i].DES_APEMAT.ToString().ToUpper();
                                        listaResultado[i].DIRECCION = listaResultado[i].DIRECCION.ToString().ToUpper();


                                        listaUEP_Grilla.Add(ObjUserEP);

                                        //CONCATENANDO A LA CADENA PARA ENVIAR A BD
                                        cadena_tip_doc += ObjUserEP.COD_DOCIDE + "//";
                                        cadena_num_doc += ObjUserEP.NUM_DOCIDE + "//";
                                        cadena_num_tlf += ObjUserEP.DES_TLFFIJ + "//";
                                        cadena_num_cel += ObjUserEP.DES_TLFMVL + "//";
                                        cadena_tip_sexo += ObjUserEP.IND_SEXO + "//";
                                        cadena_correo += ObjUserEP.DES_CORELE + "//";
                                        cadena_prim_nombre += ObjUserEP.DES_NOMBRE.ToUpper() + "//";
                                        cadena_sec_nombre += ObjUserEP.DES_NOmBRE_LAST.ToUpper() + "//";
                                        cadena_pri_apellido += ObjUserEP.DES_APEPAT.ToUpper() + "//";
                                        cadena_sec_apellido += ObjUserEP.DES_APEMAT.ToUpper() + "//";
                                        cadena_sec_direccion += ObjUserEP.DIRECCION.ToUpper() + "//";
                                        cadena_cargo += ObjUserEP.CARGO.ToUpper() + "//";
                                        cadena_contrasenia += ObjUserEP.CONTRASENIA + "//";
                                        cadena_contrasenia_encry += ObjUserEP.CONTRASENIA_ENCR + "//";


                                        //

                                        cadena_html_user += " <tr>";
                                        var nom_tip_doc = "";
                                        for (var k = 0; k < listaTipoDocumento.Count; k++)
                                        {
                                            if (listaTipoDocumento[k].valor == listaResultado[i].COD_DOCIDE.ToString())
                                            {
                                                nom_tip_doc = listaTipoDocumento[k].descripcion;
                                            }
                                        }

                                        var nom_tip_sex = "";
                                        for (var k = 0; k < listaSexo.Count; k++)
                                        {
                                            if (listaSexo[k].valor.ToUpper() == listaResultado[i].IND_SEXO.ToString().ToUpper())
                                            {
                                                nom_tip_sex = listaSexo[k].descripcion;
                                            }
                                        }
                                        var arrex = formulario["ddlUnidadEP"].ToString().Split("//");
                                        var extension = Convert.ToString(arrex[1]);
                                        cadena_html_user += "<td> " + nom_tip_doc + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].NUM_DOCIDE.ToString().ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].DES_APEPAT.ToString().ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].DES_APEMAT.ToString().ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].DES_NOMBRE.ToString().ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].DES_NOmBRE_LAST.ToString().ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].DES_TLFFIJ.ToString().ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].DES_TLFMVL.ToString().ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].DIRECCION.ToString().ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + nom_tip_sex.ToUpper() + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].DES_CORELE.ToString() + extension + "</td>";
                                        cadena_html_user += "<td> " + listaResultado[i].CARGO.ToString().ToUpper() + "</td>";




                                        cadena_html_user += "<td class='text-center'> <a href='#' onclick='fn_EditarUsuarioCargaM(\"" + listaResultado[i].NUM_DOCIDE.ToString() + "\")' class='fa fa-edit fa-lg' title='Editar'></a>  <a href='#' onclick='fn_EliminarUsuarioCargaM(\"" + listaResultado[i].NUM_DOCIDE.ToString() + "\")' class='fa fa-remove fa-lg' title='Eliminar'></a></td>";

                                        cadena_html_user += "</tr>";


                                        break;
                                }
                             
                            }



                            //


                        }
                        cadena_html_user += " </tbody>";
                        cadena_html_user += "   </table>";
                        cadena_html_user += "</div>";

                        BlUsuarioEP objLogicaSolicitud = new BlUsuarioEP();
                    
                        objLogicaSolicitud.InsertarDatosTemporalUser(cadena_tip_doc, cadena_num_doc, cadena_num_tlf, cadena_num_cel, cadena_tip_sexo, cadena_correo,
                                cadena_prim_nombre, cadena_sec_nombre, cadena_pri_apellido, cadena_sec_apellido, cadena_sec_direccion, cadena_cargo, cadena_contrasenia, cadena_contrasenia_encry);


                        keyValueError = new KeyValuePair<string, string>("1", "Se Cargaron: " + listaUEP_Grilla.Count + " registros de: " + listaResultado.Count + "//" + cadena_html_user + "//" + listaErrors.Count);

                    }
                    break;
            }

            return keyValueError;
        }
        private BeUsuarioEP validarDocumentoExcel(BeUsuarioEP objParametro)
        {
            List<ErrorUser> listaErrors;
            listaErrors = JsonConvert.DeserializeObject<List<ErrorUser>>(HttpContext.Session.GetString("listaErrorCargaMasivaUser"));

            if (listaErrors == null) { listaErrors = new List<ErrorUser>(); }


            String error = "";
            ErrorUser objError = new ErrorUser();
            objError.dni = objParametro.NUM_DOCIDE;

            switch (objParametro.COD_DOCIDE.Length)
            {
                case 0:
                    error += "no tiene Tipo de Documento";
                    error += ", ";
                    validar_error_general = 1;
                    break;
                default:
                    if (objParametro.COD_DOCIDE.ToUpper() != "01")
                    {
                        validar_error_general = 1;
                        error += "El tipo de documento es incorrecto";
                        error += ", ";
                    }
                    
                    break;
            }

            switch (objParametro.DES_APEPAT.Length)
            {
                case 0:
                    validar_error_general = 1;
                    error += "no apellido paterno";
                    error += ", ";
                    break;
            }
            switch (objParametro.DES_APEMAT.Length)
            {
                case 0:
                    validar_error_general = 1;
                    error += "no apellido materno";
                    error += ", ";
                    break;
            }
            switch (objParametro.DES_NOMBRE.Length)
            {
                case 0:
                    validar_error_general = 1;
                    error += "no tiene primer nombre";
                    error += ", ";
                    break;
            }
            switch (objParametro.DIRECCION.Length)
            {
                case 0:
                    validar_error_general = 1;
                    error += "no tiene dirección";
                    error += ", ";
                    break;
            }

            switch (objParametro.NUM_DOCIDE.Length)
            {
                case 0:
                    validar_error_general = 1;
                    error += "no tiene número de Dni";
                    error += ", ";
                    break;
                default:
                    long numservicio = 0;
                    bool isNumero = long.TryParse(objParametro.NUM_DOCIDE, out numservicio);

                    switch (isNumero)
                    {
                        case true:
                            //VALIDNADO SI EXISTE EL REGISTRO
                            BeUsuarioEP listaUserEdit;
                            BlUsuarioEP objParametroD = new BlUsuarioEP();

                            listaUserEdit = objParametroD.SP_VALIDAR_USUARIO_EP_BY_DNI(objParametro.NUM_DOCIDE);

                            if (listaUserEdit.IDUSUARIO != null && listaUserEdit.IDUSUARIO != "")
                            {
                                validar_error_general = 1;
                                error += "El número de Documento se encuentra registrado en la Base de Datos de Sigrei";
                                error += ", ";
                            }
                            else
                            {
                                if (objParametro.NUM_DOCIDE.Count() < 8)
                                {
                                    validar_error_general = 1;
                                    error += "El número de Documento debe contar con 8 digitos";
                                    error += ", ";
                                }
                            }

                            break;
                        default:
                            validar_error_general = 1;
                            error += "El número de Documento contiene caracteres que no es  numérico";
                            error += ", ";
                            break;
                    }
                    break;
            }

            if (!(objParametro.DES_TLFFIJ.Length == 9 || objParametro.DES_TLFFIJ.Length == 8))
            {
                validar_error_general = 1;
                error += "El número de teléfono debe contener 9 o 8 caracteres";
                error += ", ";
            }
            else
            {
                long numservicio = 0;
                bool isNumero = long.TryParse(objParametro.DES_TLFFIJ, out numservicio);

                switch (isNumero)
                {
                    case false:
                        validar_error_general = 1;
                        error += "El número de TELEFONO contiene caracteres que no es  numérico";
                        error += ", ";
                        break;
                }
            }

            switch (objParametro.DES_TLFMVL.Length)
            {
                case 9:
                    long numservicio = 0;
                    bool isNumero = long.TryParse(objParametro.DES_TLFMVL, out numservicio);

                    switch (isNumero)
                    {
                        case false:
                            validar_error_general = 1;
                            error += "El número de CELULAR contiene caracteres que no es  numérico";
                            error += ", ";
                            break;
                    }
                    
                    break;
                default:
                    validar_error_general = 1;
                    error += "El número de celular debe contener 9 caracteres";
                    error += ", ";
                    break;
            }

            switch (objParametro.DES_CORELE.Length)
            {
                case 0:
                    validar_error_general = 1;
                    error += "no tiene Correo Electronico";
                    error += ", ";
                    break;
            }
 
            if (objParametro.IND_SEXO.ToUpper() != "M" && objParametro.IND_SEXO.ToUpper() != "F")
            {
                validar_error_general = 1;
                error += "el Sexo ingresado es incorrecto";
                error += ", ";

            }
            
            switch (objParametro.CARGO.Length)
            {
                case 0:
                    validar_error_general = 1;
                    error += "no tiene cargo";
                    error += ", ";
                    break;
            }

            if (error != "")
            {
                error = error.Substring(0, error.Length - 2);
                objError.nroItem = objParametro.nroItem;
                objError.error = String.Format("La fila {0} tiene el siguiente error: ", objParametro.nroItem) + error;
                listaErrors.Add(objError);

                var listaErrorJSON = JsonConvert.SerializeObject(listaErrors);
                HttpContext.Session.SetString("listaErrorCargaMasivaUser", listaErrorJSON);
                return new BeUsuarioEP();

            }
           
            return objParametro;
        }

        [HttpPost]
        public ActionResult EditarUsuarioCargaMasiva(IFormCollection formulario)
        {

            BeUsuarioEP listaUserEdit;
            BlUsuarioEP objParametro = new BlUsuarioEP();


            BlParametro ObjLogica = new BlParametro();


            List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);
            ViewBag.ddlTipoDocumento = listaTipoDocumento;



            //List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
            List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores_sexo(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);

            ViewBag.ddlSexo = listaSexo;




            listaUserEdit = objParametro.SP_OBTENER_USUARIO_EP_TEMPORAL(formulario["hiidValorEditCar"]);


            return PartialView("_EditarUsuarioCarMasEP", listaUserEdit);



        }

        [HttpPost]
        public ActionResult ActualizarUsuarioMasivoEP(IFormCollection formulario)
        {
            try
            {
                KeyValuePair<string, string> result;
                

                BlUsuarioEP obj = new BlUsuarioEP();
                List<BeUsuarioEP> listaUEP_Grilla_Final = obj.ActualizarDatosTemporalUser(formulario["hiidValorEditCar"].ToString(),
                    formulario["DES_APEMAT"].ToString().ToUpper(),
                    formulario["DES_NOMBRE"].ToString().ToUpper(),
                    formulario["DES_NOmBRE_LAST"].ToString().ToUpper(),
                    formulario["DES_TLFFIJ"].ToString(),
                    formulario["COD_DOCIDE"].ToString(),
                    formulario["DES_TLFMVL"].ToString(),
                    formulario["NUM_DOCIDE"].ToString(),
                    formulario["DIRECCION"].ToString().ToUpper(),
                    formulario["DES_APEPAT"].ToString().ToUpper(),
                    formulario["DES_CORELE"].ToString(),
                    formulario["IND_SEXO"].ToString(),
                    formulario["CARGO"].ToString().ToUpper());

                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);


                //List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
                List<BE_PARAMETRO> listaSexo = ObjLogica.ListarValores_sexo(UTConstantes.APLICACION_COMUN, UTConstantes.APLICACION_TABLA.COMUN, UTConstantes.APLICACION_COLUMNA.IDSEXO);
                //ARMANDO NUEVO HTML
                string cadena_html_user = "";
                cadena_html_user += "<div class='tabla-content-result'>";
                cadena_html_user += "<table id='dtResultadUserCargaPrevia' class='tabla-osiptel' style='width: 100%'>";
                cadena_html_user += " <caption style='visibility: hidden;'>Detalle Parametro</caption>";
                cadena_html_user += " <thead>";
                cadena_html_user += "<tr>";
                cadena_html_user += "<th scope='col'>Tipo Documento</th>";
                cadena_html_user += "<th scope='col'>Nº Documento</th>";
                cadena_html_user += "<th scope='col'>Telefono</th>";
                cadena_html_user += "<th scope='col'>Celular</th>";
                cadena_html_user += "<th scope='col'>Correo</th>";
                cadena_html_user += "<th scope='col'>Sexo</th>";
                cadena_html_user += "<th scope='col'>Nombre</th>";
                cadena_html_user += "<th scope='col'>Segundo Nombre</th>";
                cadena_html_user += "<th scope='col'>Apellido Paterno</th>";
                cadena_html_user += "<th scope='col'>Apellido Materno</th>";
                cadena_html_user += "<th scope='col'>Dirección</th>";
                cadena_html_user += "<th scope='col'>Cargo</th>";
                cadena_html_user += "<th scope='col'>Opciones</th>";
                cadena_html_user += "</tr>";
                cadena_html_user += " </thead>";
                cadena_html_user += "<tbody>";
                for (var i = 0; i < listaUEP_Grilla_Final.Count; i++)
                {

                    cadena_html_user += " <tr>";
                    var nom_tip_doc = "";
                    for (var k = 0; k < listaTipoDocumento.Count; k++)
                    {
                        if (listaTipoDocumento[k].valor == listaUEP_Grilla_Final[i].COD_DOCIDE.ToString())
                        {
                            nom_tip_doc = listaTipoDocumento[k].descripcion;
                        }
                    }

                    var nom_tip_sex = "";
                    for (var k = 0; k < listaSexo.Count; k++)
                    {
                        if (listaSexo[k].valor.ToUpper() == listaUEP_Grilla_Final[i].IND_SEXO.ToString().ToUpper())
                        {
                            nom_tip_sex = listaSexo[k].descripcion;
                        }
                    }
                    var arr = formulario["ddlUnidadEP"].ToString().Split("//");
                    var extension = Convert.ToString(arr[1]);
                    cadena_html_user += "<td> " + nom_tip_doc + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].NUM_DOCIDE.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].DES_TLFFIJ.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].DES_TLFMVL.ToString() + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].DES_CORELE.ToString() + extension + "</td>";
                    cadena_html_user += "<td> " + nom_tip_sex + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].DES_NOMBRE.ToString().ToUpper() + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].DES_NOmBRE_LAST.ToString().ToUpper() + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].DES_APEPAT.ToString().ToUpper() + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].DES_APEMAT.ToString().ToUpper() + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].DIRECCION.ToString().ToUpper() + "</td>";
                    cadena_html_user += "<td> " + listaUEP_Grilla_Final[i].CARGO.ToString().ToUpper() + "</td>";
                    cadena_html_user += "<td class='text-center'> <a href='#' onclick='fn_EditarUsuarioCargaM(\"" + listaUEP_Grilla_Final[i].NUM_DOCIDE.ToString() + "\")' class='fa fa-edit fa-lg' title='Consultar'></a> <a href='#' onclick='fn_EliminarUsuarioCargaM(\"" + listaUEP_Grilla_Final[i].NUM_DOCIDE.ToString() + "\")' class='fa fa-remove fa-lg' title='Consultar'></a></td>";
                    cadena_html_user += "</tr>";
                }
                cadena_html_user += " </tbody>";
                cadena_html_user += "   </table>";
                cadena_html_user += "</div>";
                ViewBag.listaUEP_Grilla_Final = listaUEP_Grilla_Final;
                result = new KeyValuePair<string, string>("1", "Se Actualizó el Usuario Seleccionado  //" + cadena_html_user);
                return Json(result);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "CargarInformacionMasUser=>Carga de informacion");
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al cargar la información");

                return Json(result);
            }
        }

        [HttpPost]
        public ActionResult GrabarFinUsuarioMasivoEP(IFormCollection formulario)
        {
            bool res_correo = false;
            List<BeUsuarioEP> lista;
            BeUsuarioEP ParametroEPBE = new BeUsuarioEP();
            KeyValuePair<string, string> result;
            BlUsuarioEP objParametro = new BlUsuarioEP();
            Encriptador encry = new Encriptador();
            Constantes consta = new Constantes();

            //PRINCIPAL
            ParametroEPBE.IDPERFIL = Convert.ToInt16(formulario["ddlTipoPerfil"].ToString());
            ParametroEPBE.IDMODULO = Convert.ToInt16(formulario["ddlModulo"].ToString());
            ParametroEPBE.IDENTPUBLICA = Convert.ToInt16(formulario["ddlEPublica"].ToString());
            ParametroEPBE.NUM_RUC = formulario["ddlInsExternaUser"].ToString();
            var arr = formulario["ddlUnidadEP"].ToString().Split("//");
            ParametroEPBE.IDUNIDADEP = Convert.ToInt16(arr[0]);

            ParametroEPBE.USUCRE = HttpContext.Session.GetString("IdUsuario");
            String nueva_contrasenia = "UEP_" + CreatePassword(6);
            ParametroEPBE.CONTRASENIA = nueva_contrasenia;
            
            ParametroEPBE.CONTRASENIA_ENCR = encry.EncryptStringToBytes_Aes(nueva_contrasenia, consta.Key256, consta.IVAES256);
            ParametroEPBE.APLICACION = UTConstantes.APLICACION.ToUpper();



            try
            {
                
                lista = objParametro.SP_REGISTRAR_MASIVO_USUARIO_EP(ParametroEPBE);
                if (lista.Count > 0)
                {
                    FileStream fs = new FileStream(configuration["CarpetaTemporal"] + "\\Credenciales de Usuarios.xlsx", FileMode.Create);
                    ExcelPackage Excel = new ExcelPackage(fs);

                    /* Creación del estilo. */
                    Excel.Workbook.Styles.CreateNamedStyle("Moneda");
                    
                    /* Creación de hoja de trabajo. */
                    Excel.Workbook.Worksheets.Add("Credenciales de Usuario");
                    ExcelWorksheet hoja = Excel.Workbook.Worksheets["Credenciales de Usuario"];




                    /* Num Caracteres + 1.29 de Margen.
                    Los índices de columna empiezan desde 1. */
                    hoja.Column(1).Width = 11.29f;

                    ExcelRange rango = hoja.Cells["A1"];

                    rango.Value = "Tipo de Oocumento";
                    ExcelRange rangoB = hoja.Cells["B1"];

                    rangoB.Value = "Nº de Documento";

                    ExcelRange rangoC = hoja.Cells["C1"];

                    rangoC.Value = "Apellidos";

                    ExcelRange rangoD = hoja.Cells["D1"];

                    rangoD.Value = "Nombres";

                    ExcelRange rangoE = hoja.Cells["E1"];

                    rangoE.Value = "Usuario";

                    ExcelRange rangoF = hoja.Cells["F1"];

                    rangoF.Value = "Clave";
                    int contador_celdas = 2;
                    for (var i = 0; i < lista.Count; i++)
                    {
                        //ENVIANDO CORREO AL USUARIO

                        res_correo = EnviarCorreoSinCertificado(
                            lista[i].HOSTNAME,
                            lista[i].PUERTO,
                            lista[i].DE_PARTE,
                            lista[i].CLAVE_PARTE,
                            lista[i].PARA,
                            lista[i].ASUNTO,
                            lista[i].MENSAJE,
                             lista[i].COPIA);

                        //


                        ExcelRange rangoAA = hoja.Cells["A" + contador_celdas];

                        rangoAA.Value = lista[i].DES_TDOC;
                        ExcelRange rangoBB = hoja.Cells["B" + contador_celdas];

                        rangoBB.Value = lista[i].NUM_DOCIDE;

                        ExcelRange rangoCC = hoja.Cells["C" + contador_celdas];

                        rangoCC.Value = lista[i].DES_APEPAT;

                        ExcelRange rangoDD = hoja.Cells["D" + contador_celdas];

                        rangoDD.Value = lista[i].DES_NOMBRE;

                        ExcelRange rangoEE = hoja.Cells["E" + contador_celdas];

                        rangoEE.Value = lista[i].NUM_DOCIDE;

                        ExcelRange rangoFF = hoja.Cells["F" + contador_celdas];

                        rangoFF.Value = lista[i].CONTRASENIA;




                        contador_celdas++;
                        
                    }
                    Excel.Save();
                    // No estoy seguro de ésta parte, pero mejor cerramos el stream de archivo.
                    fs.Close();
                    fs.Dispose();

                    Excel.Dispose();


                    HttpContext.Session.SetString("CredencialUsuario", configuration["CarpetaTemporal"] + "\\Credenciales de Usuarios.xlsx");
                    if (res_correo)
                    {
                        result = new KeyValuePair<string, string>("1", "¡Registro Correcto!");
                    }
                    else
                    {
                        result = new KeyValuePair<string, string>("1", "¡Registro de Usuario Correcto, Ocurrió un problema el enviar el mail !");
                    }

                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                }
               
            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
            }

            return Json(result);
        }

        public IActionResult GestorContrasenia()
        {

            return View();
        }

        [HttpPost]
        public ActionResult ActualizarContraseniaUEP(IFormCollection formulario)
        {
            BeContrasenia ParametroEPBE = new BeContrasenia();
            KeyValuePair<string, string> result;
            Constantes cont = new Constantes();
            Encriptador encry = new Encriptador();
            try
            {
                BlContrasenia objParametro = new BlContrasenia();
                ParametroEPBE.USUARIO = formulario["txtUsuario"].ToString();
               
                ParametroEPBE.CONTRASENIA = encry.EncryptStringToBytes_Aes(formulario["txtClave"].ToString(), cont.Key256, cont.IVAES256);
               
                ParametroEPBE.NUEVA_CONTRASENIA = encry.EncryptStringToBytes_Aes(formulario["txtClaveNew"].ToString(), cont.Key256, cont.IVAES256);
                
                ParametroEPBE.NUEVA_CONTRASENIA_CONF = encry.EncryptStringToBytes_Aes(formulario["txtClaveNewConf"].ToString(), cont.Key256, cont.IVAES256);

                //VALIDANDO QUE LOS DATOS DE LA CONTRSEÑA ACTUAL Y USUARIO SEAN CORRECTOS
                BlUsuarioEP objParametroUEP = new BlUsuarioEP();
                List<BeUsuarioEP> listaUser;
                

                listaUser = objParametroUEP.SP_VALIDAR_DATOS_USER_AFTER(formulario["txtUsuario"].ToString(), encry.EncryptStringToBytes_Aes(formulario["txtClave"].ToString(), cont.Key256, cont.IVAES256));

                //CONULTAR PARAMETROS A BD
                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> listaParametros1 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRALMIN", "IDTIPCONTRASENIA");
                List<BE_PARAMETRO> listaParametros2 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRALMAY", "IDTIPCONTRASENIA");
                List<BE_PARAMETRO> listaParametros3 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRANCESP", "IDTIPCONTRASENIA");
                List<BE_PARAMETRO> listaParametros4 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRACMIN", "IDTIPCONTRASENIA");
                List<BE_PARAMETRO> listaParametros5 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRACMAX", "IDTIPCONTRASENIA");
                string minusculas = "";
                string mayusculas = "";
              
                string minimo = "";
                string maximo = "";
                for (var i = 0; i < listaParametros1.Count; i++)
                {
                    if (listaParametros1[i].activo == "1")
                    {
                        minusculas = "a-z";
                    }
                    if ( listaParametros2[i].activo == "1")
                    {
                        mayusculas = "A-Z";
                    }
                    if ( listaParametros4[i].activo == "1")
                    {
                        //minimo = Convert.ToString(listaParametros[i].orden);
                        List<BE_PARAMETRO> listaParametrosMin = ObjLogica.ListarValoresContrasenia("COMUN", "CONTRASENIAMINIMO", "CONTRASENIAMINIMO");
                        minimo = Convert.ToString(listaParametrosMin[0].orden);

                    }
                    if ( listaParametros5[i].activo == "1")
                    {
                        //maximo = Convert.ToString(listaParametros[i].orden);
                        List<BE_PARAMETRO> listaParametrosMax = ObjLogica.ListarValoresContrasenia("COMUN", "CONTRASENIAMAXIMO", "CONTRASENIAMAXIMO");
                        maximo = Convert.ToString(listaParametrosMax[0].orden);
                    }
                }
                //
                switch (listaUser.Count)
                {
                    case 0:
                        result = new KeyValuePair<string, string>("0", "Verificar su Usuario o Contraseña ya que son incorrectos.");
                        break;
                    default:
                        //VALIDANDO SI EL USUARIO YA ACTUALIZO SU CONTRASENIA

                        List<BeUsuarioEP> lista;
                        lista = objParametroUEP.SP_VALIDAR_CAMBIO_CONTRASENIA(formulario["txtUsuario"].ToString());

                        switch (lista.Count)
                        {
                            case 0:
                                result = new KeyValuePair<string, string>("0", "Usted ya realizo la actualización de su contraseña");
                                break;
                            default:

                                switch (lista[0].INDCAMBIOCNIA)
                                {
                                    case 2:
                                        //
                                        try
                                        {
                                           
                                            //VALIDANDO ESTRUCTURA DE CONTRASEÑA 
                                           
                                            Regex r = new Regex(@"^(?=.*\d)(?=.*[" + minusculas + "])(?=.*[" + mayusculas + "]).{" + minimo + "," + maximo + "}$");
                                            string clave = formulario["txtClaveNew"].ToString().Trim();
                                            if (r.IsMatch(clave))
                                            {
                                                String resultado = objParametro.SP_ACTUALIZAR_CONTRASENIA_UEP(ParametroEPBE);
                                                switch (resultado.Trim())
                                                {
                                                    case "1":
                                                        result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                                                        break;
                                                    default:
                                                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                                        break;
                                                }
                                                
                                            }
                                            else
                                            {
                                                result = new KeyValuePair<string, string>("0", "Sú contraseña debe contener al menos: <br> *1 letra minúscula <br> * 1 letra mayúscula <br> *1 Número o caracter especial <br> *Contener mínimo " + minimo + " caracteres <br> * Contener un máximo " + maximo + " caracteres");
                                            }
                                            //

                                        }
                                        catch (Exception ex)
                                        {
                                            LogError.RegistrarError(ex);
                                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                        }
                                        break;
                                    default:
                                        //VALIDANDO FECHA DE EXPIRACION DEL ENLACE
                                        List<BeUsuarioEP> listaExp;
                                        listaExp = objParametroUEP.SP_VALIDAR_EXPIRACION_CONTRASENIA_ENLACE(formulario["txtUsuario"].ToString());
                                        //
                                        switch (listaExp.Count)
                                        {
                                            case 0:
                                                result = new KeyValuePair<string, string>("0", "No se puede realizar el proceso de cambio de clave, ya que el enlace se encuentra expirado, debe generar un nuevo enlace.");
                                                break;
                                            default:
                                                //
                                                try
                                                {
                                                    //VALIDANDO ESTRUCTURA DE CONTRASEÑA 
                                                    
                                                    Regex r = new Regex(@"^(?=.*\d)(?=.*[" + minusculas + "])(?=.*[" + mayusculas + "]).{" + minimo + "," + maximo + "}$");
                                                    string clave = formulario["txtClaveNew"].ToString().Trim();
                                                    if (r.IsMatch(clave))
                                                    {
                                                        String resultado = objParametro.SP_ACTUALIZAR_CONTRASENIA_UEP(ParametroEPBE);
                                                        switch (resultado.Trim())
                                                        {
                                                            case "1":
                                                                result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                                                                break;
                                                            default:
                                                                result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                                                break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        result = new KeyValuePair<string, string>("0", "Sú contraseña debe contener al menos: <br> *1 letra minúscula <br> * 1 letra mayúscula <br> *1 Número o caracter especial <br> *Contener mínimo " + minimo + " caracteres <br> * Contener un máximo " + maximo + " caracteres");
                                                    }
                                                    //

                                                }
                                                catch (Exception ex)
                                                {
                                                    LogError.RegistrarError(ex);
                                                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                                }
                                                break;
                                        }
                                        break;
                                }

                                break;
                        }

                        break;
                }

            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        public IActionResult GenerarEnlace()
        {
            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);
            ViewBag.ddlTipoDocumento = listaTipoDocumento;
            return View();
        }

        [HttpPost]
        public ActionResult GenerarEnlaceUEP(IFormCollection formulario)
        {
            KeyValuePair<string, string> result;

            try
            {

                //VALIDANDO SI EXSTE EL CORREO
                BlUsuarioEP objParametroUEP = new BlUsuarioEP();
                List<BeUsuarioEP> listaUser;
                listaUser = objParametroUEP.SP_VALIDAR_CORREO_USER_EP(formulario["txtMail"].ToString(), formulario["ddlTipoDocumento"].ToString(), formulario["txtUsuario"].ToString());
                if (listaUser.Count > 0)
                {
                    try
                    {
                        List<BeUsuarioEP> listaUserEnlc;
                        listaUserEnlc = objParametroUEP.SP_GENERAR_NUEVO_ENLACE_UEP(listaUser[0].IDUSUARIO);

                        if (listaUserEnlc.Count > 0)
                        {
                            //ENVIANDO CORREO AL USUARIO
                           
                            EnviarCorreoSinCertificado(
                                listaUserEnlc[0].HOSTNAME,
                                listaUserEnlc[0].PUERTO,
                                listaUserEnlc[0].DE_PARTE,
                                listaUserEnlc[0].CLAVE_PARTE,
                                listaUserEnlc[0].PARA,
                                listaUserEnlc[0].ASUNTO,
                                listaUserEnlc[0].MENSAJE,
                                listaUserEnlc[0].COPIA);

                            //
                            result = new KeyValuePair<string, string>("1", "¡Enviamos el nuevo enlace a tu correo electronico por favor verificar!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "El Usuario ya cambio su clave generada.");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Los datos ingresados no son corectos o usted ya actualizó su contraseña");
                }

                //







            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        public IActionResult CambioClave()
        {
            BlParametro ObPara = new BlParametro();
            List<BE_PARAMETRO> listaTDocumentoUser = ObPara.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, 
                                                                                        UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);
            ViewBag.ddlTipoDocumento = listaTDocumentoUser;

            return View();
        }
        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890*";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--) { res.Append(valid[rnd.Next(valid.Length)]); }
            return res.ToString();
        }


        [HttpPost]
        public ActionResult GenerarNuevaClaveUEP(IFormCollection formulario)
        {
            KeyValuePair<string, string> result;

            try
            {

                //VALIDANDO SI EXSTE EL CORREO
                BlUsuarioEP objParametroUEP = new BlUsuarioEP();
                List<BeUsuarioEP> listaUser;
                Encriptador encry = new Encriptador();
                Constantes consta = new Constantes();
                Random rnd = new Random();
                String nueva_contrasenia = "SEP_" + CreatePassword(6);
                
                String nueva_contrasenia_enc = encry.EncryptStringToBytes_Aes(nueva_contrasenia, consta.Key256, consta.IVAES256);

                listaUser = objParametroUEP.SP_VALIDAR_CORREO_CAMBIO_CLAVE_USER_EP(formulario["txtMail"].ToString(), formulario["ddlTipoDocumento"].ToString(), formulario["txtUsuario"].ToString());
                if (listaUser.Count > 0)
                {
                    try
                    {
                        List<BeUsuarioEP> listaUserEnlc;
                        listaUserEnlc = objParametroUEP.SP_GENERAR_NUEVO_CLAVE_TEMPORAL_UEP(listaUser[0].IDUSUARIO, nueva_contrasenia_enc, nueva_contrasenia);

                        if (listaUserEnlc.Count > 0)
                        {
                            //ENVIANDO CORREO AL USUARIO
                            EnviarCorreoSinCertificado(
                                listaUserEnlc[0].HOSTNAME,
                                listaUserEnlc[0].PUERTO,
                                listaUserEnlc[0].DE_PARTE,
                                listaUserEnlc[0].CLAVE_PARTE,
                                listaUserEnlc[0].PARA,
                                listaUserEnlc[0].ASUNTO,
                                listaUserEnlc[0].MENSAJE,
                                listaUserEnlc[0].COPIA);

                            //
                            result = new KeyValuePair<string, string>("1", "¡Enviamos la nueva clave a tu correo electrónico por favor verificar!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "El Usuario ya cambio su clave generada.");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "El correo ingresado no es correcto.");
                }

                //







            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }
    }



}
