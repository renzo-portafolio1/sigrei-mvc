﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using docPacking = DocumentFormat.OpenXml.Packaging;
using docWodProcesing = DocumentFormat.OpenXml.Wordprocessing;

using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class RequerimientoController : Controller
    {

        private readonly IConfiguration configuration;
        public RequerimientoController(IConfiguration config)
        {
            this.configuration = config;

        }

        public IActionResult Index()
        {
            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> lista = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
            ViewBag.Lista_TipoSolicitante = lista;

            BL_USUARIOGPSU ObjLogicaUsuario = new BL_USUARIOGPSU();
            List<BE_USUARIOGPSU> listaUsuario = ObjLogicaUsuario.Listar(UTConstantes.APLICACION, UTConstantes.VALOR_UNO);

            ViewBag.Lista_UsuarioAsignado = listaUsuario;



            List<EstadoBE> listaEstado = new List<EstadoBE>();
            EstadoBE objEstado = new EstadoBE();
            objEstado.IDESTADO = "9";
            objEstado.ESTADO = "Asignado";
            listaEstado.Add(objEstado);

            objEstado = new EstadoBE();
            objEstado.IDESTADO = "10";
            objEstado.ESTADO = "Finalizado";
            listaEstado.Add(objEstado);

            ViewBag.Lista_Estado = listaEstado;

            return View();
        }

        [HttpGet]
        public ActionResult BandejaInicial(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO> listaResultado = new List<BE_REQUERIMIENTO>();

            return PartialView("_DetalleBandejaRequerimiento", listaResultado);
        }

        [HttpPost]
        public ActionResult BuscarRequerimientos(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO>();
            BE_REQUERIMIENTO objParametro = obtenerCriterios(formulario);
            BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();

            listaBE_REQUERIMIENTO = objLogica.ListaSolicitud(objParametro);

            return PartialView("_DetalleBandejaRequerimiento", listaBE_REQUERIMIENTO);
        }

        [HttpPost]
        public ActionResult LimpiarBandejaRequerimientos(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO>();
            return PartialView("_DetalleBandejaRequerimiento", listaBE_REQUERIMIENTO);
        }



        public ActionResult IrAnalizarImei(IFormCollection formulario)
        {
            var imei = formulario["ihimeianaliz"].ToString();
            imei = imei.Substring(0, imei.Length - 1);
            HttpContext.Session.SetString("AnalizReqImei", imei);
            return RedirectToAction("Index", "AnalizarIMEI");
        }


        [HttpPost]
        public ActionResult DevolverRequerimiento(IFormCollection formulario)
        {
            var ID = formulario["ihidrequeri"];
            BE_REQUERIMIENTO objEntidad = new BE_REQUERIMIENTO();
            BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();

            Int64 idDevuelto = 0;
            try
            {
                objEntidad.idRequerimiento = Convert.ToInt64(ID);
                objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario");
                objEntidad.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;
                idDevuelto = objLogica.DevolverSolicitud(objEntidad);

                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                if (idDevuelto > 0)
                {
                    result = new KeyValuePair<string, string>("1", "¡El Requerimiento se ha devuelto correctamente para su asignación!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al intentar devolver para su asignación");
                }
                return Json(result);
            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al intentar devolver para su asignación");
                return Json(result);
            }
        }
        [HttpPost]
        public ActionResult FinalizarRequerimiento(IFormCollection formulario)
        {
            var ID = formulario["ihidrequeri"];
            BE_REQUERIMIENTO objEntidad = new BE_REQUERIMIENTO();
            BL_REQUERIMIENTO objLogica = new BL_REQUERIMIENTO();

            Int64 idFinalizado = 0;
            try
            {
                objEntidad.idRequerimiento = Convert.ToInt64(ID);
                objEntidad.comentarioFinalizacion = formulario["comentariosFinalizar"].ToString();
                objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario");
                objEntidad.idEstado = UTConstantes.ID_ESTADO_FINALIZADO;
                idFinalizado = objLogica.FinalizarSolicitud(objEntidad);
                KeyValuePair<string, string> result = new KeyValuePair<string, string>();
                if (idFinalizado > 0)
                {
                    result = new KeyValuePair<string, string>("1", "¡El Requerimiento se ha finalizado correctamente!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Error al intentar finalizar el requerimiento");
                }
                return Json(result);
            }
            catch (Exception)
            {
                KeyValuePair<string, string> result = new KeyValuePair<string, string>("0", "Error al intentar finalizar el requerimiento");
                return Json(result);
            }
        }
        [HttpPost]
        public ActionResult ConsultaRequerimientoMovilPri(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objRequerimiento = new BE_REQUERIMIENTO();
            try
            {
                List<BE_REQUERIMIENTO> listaBE_REQUERIMIENTO;
                Int64 idRequerimiento = Int64.Parse(formulario["ihidrequeri"].ToString());

                BL_REQUERIMIENTO objLogicaRequerimiento = new BL_REQUERIMIENTO();
                objRequerimiento.idRequerimiento = idRequerimiento;
                objRequerimiento = objLogicaRequerimiento.ConsultaSolicitud(objRequerimiento);
                BE_REQUERIMIENTO_IMEI objRequerimientoImei = new BE_REQUERIMIENTO_IMEI();
                objRequerimientoImei.idRequerimiento = idRequerimiento;
                objRequerimientoImei.idEstado = UTConstantes.VALOR_CERO;
                BL_REQUERIMIENTO_IMEI objLogicaRequerimientoImei = new BL_REQUERIMIENTO_IMEI();
                objRequerimiento.listaRequerimientoIMEI = objLogicaRequerimientoImei.ConsultarMovilPendietes(Convert.ToString(idRequerimiento));
            }
            catch (Exception)
            {
                objRequerimiento = new BE_REQUERIMIENTO();
                objRequerimiento.listaRequerimientoIMEI = new List<BE_REQUERIMIENTO_IMEI>();

            }
            return PartialView("_PartialConsultaMovilPri", objRequerimiento);
        }


        [HttpPost]
        public ActionResult ConsultaRequerimientoMovil(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objRequerimiento = new BE_REQUERIMIENTO();
            try
            {
                List<BE_REQUERIMIENTO> listaBE_REQUERIMIENTO;
                Int64 idRequerimiento = Int64.Parse(formulario["ihidrequeri"].ToString());

                BL_REQUERIMIENTO objLogicaRequerimiento = new BL_REQUERIMIENTO();
                objRequerimiento.idRequerimiento = idRequerimiento;
                objRequerimiento = objLogicaRequerimiento.ConsultaSolicitud(objRequerimiento);
                BE_REQUERIMIENTO_IMEI objRequerimientoImei = new BE_REQUERIMIENTO_IMEI();
                objRequerimientoImei.idRequerimiento = idRequerimiento;
                objRequerimientoImei.idEstado = UTConstantes.VALOR_CERO;
                BL_REQUERIMIENTO_IMEI objLogicaRequerimientoImei = new BL_REQUERIMIENTO_IMEI();
                objRequerimiento.listaRequerimientoIMEI = objLogicaRequerimientoImei.ConsultarMovilPendietes(Convert.ToString(idRequerimiento));
            }
            catch (Exception)
            {
                objRequerimiento = new BE_REQUERIMIENTO();
                objRequerimiento.listaRequerimientoIMEI = new List<BE_REQUERIMIENTO_IMEI>();

            }
            return PartialView("_PartialConsultaMovil", objRequerimiento);
        }

        [HttpPost]
        public ActionResult ConsultaRequerimientoImei(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objRequerimiento = new BE_REQUERIMIENTO();

            try
            {
                List<BE_REQUERIMIENTO> listaBE_REQUERIMIENTO = new List<BE_REQUERIMIENTO>();
                Int64 idRequerimiento = Int64.Parse(formulario["ihidrequeri"].ToString());

                BL_REQUERIMIENTO objLogicaRequerimiento = new BL_REQUERIMIENTO();
                objRequerimiento.idRequerimiento = idRequerimiento;
                objRequerimiento = objLogicaRequerimiento.ConsultaSolicitud(objRequerimiento);
                BE_REQUERIMIENTO_IMEI objRequerimientoImei = new BE_REQUERIMIENTO_IMEI();
                objRequerimientoImei.idRequerimiento = idRequerimiento;
                objRequerimientoImei.idEstado = UTConstantes.VALOR_CERO;
                BL_REQUERIMIENTO_IMEI objLogicaRequerimientoImei = new BL_REQUERIMIENTO_IMEI();
                objRequerimiento.listaRequerimientoIMEI = objLogicaRequerimientoImei.ListaIMEI(objRequerimientoImei);
            }
            catch (Exception)
            {
                objRequerimiento = new BE_REQUERIMIENTO();
                objRequerimiento.listaRequerimientoIMEI = new List<BE_REQUERIMIENTO_IMEI>();

            }
            return PartialView("_PartialConsultaImei", objRequerimiento);
        }

        [HttpPost]
        public ActionResult AbrirSeguimiento(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objRequerimientoEO = new BE_REQUERIMIENTO();


            try
            {
                BL_REQUERIMIENTO objLogicaRequerimiento = new BL_REQUERIMIENTO();


                Int64 idRequerimientoEO = Int64.Parse(formulario["ihidrequeri"].ToString());
                objRequerimientoEO.idRequerimiento = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoEO = objLogicaRequerimiento.ConsultaSolicitud(objRequerimientoEO);


            }
            catch (Exception)
            {
                objRequerimientoEO = new BE_REQUERIMIENTO();


            }
            return PartialView("_PartialSeguimiento", objRequerimientoEO);
        }

        [HttpPost]
        public ActionResult verTabsRespondidas(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            try
            {
                BL_REQUERIMIENTO_IMEI objLogicaRequerimiento = new BL_REQUERIMIENTO_IMEI();
                BE_REQUERIMIENTO_IMEI objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI();
                Int64 idRequerimientoEO = Int64.Parse(formulario["ihidrequeri"].ToString());
                objRequerimientoImeiEo.idRequerimiento = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoImeiEo.idEstado = UTConstantes.ID_ESTADO_ATENDIDO;
                listaResultado = objLogicaRequerimiento.ListaIMEI(objRequerimientoImeiEo);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "verTabsRespondidas> info");
                listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            }

            return PartialView("_PartialTabRespondidas", listaResultado);
        }

        [HttpPost]
        public ActionResult verTabsPorResponder(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            try
            {
                BL_REQUERIMIENTO_IMEI objLogicaRequerimiento = new BL_REQUERIMIENTO_IMEI();
                BE_REQUERIMIENTO_IMEI objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI();
                Int64 idRequerimientoEO = Int64.Parse(formulario["ihidrequeri"].ToString());
                objRequerimientoImeiEo.idRequerimiento = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoImeiEo.idEstado = UTConstantes.ID_ESTADO_PORATENDER;
                listaResultado = objLogicaRequerimiento.ListaIMEI(objRequerimientoImeiEo);
                //VALIDANDO SI ES EXEPCION
                if (listaResultado.Count == 0)
                {
                    listaResultado = objLogicaRequerimiento.ListaIMEI_EXCEPCION(objRequerimientoImeiEo);
                }
                

                
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "verTabsPorResponder> info");
                listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            }

            return PartialView("_PartialTabPorResponder", listaResultado);
        }


        [HttpPost]
        public ActionResult verTabsEnviadaEO(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            try
            {
                BL_REQUERIMIENTO_IMEI objLogicaRequerimiento = new BL_REQUERIMIENTO_IMEI();
                BE_REQUERIMIENTO_IMEI objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI();
                Int64 idRequerimientoEO = Int64.Parse(formulario["ihidrequeri"].ToString());
                objRequerimientoImeiEo.idRequerimiento = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoImeiEo.idEstado = UTConstantes.ID_ESTADO_ENVIADO;
                listaResultado = objLogicaRequerimiento.ListaIMEI(objRequerimientoImeiEo);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "verTabsEnviadaEO> info");
                listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            }

            return PartialView("_PartialTabEnviadaEO", listaResultado);
        }


        [HttpPost]
        public ActionResult verTabsDevolverInstitucion(IFormCollection formulario)
        {
            List<BE_REQUERIMIENTO_IMEI> listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            try
            {
                BL_REQUERIMIENTO_IMEI objLogicaRequerimiento = new BL_REQUERIMIENTO_IMEI();
                BE_REQUERIMIENTO_IMEI objRequerimientoImeiEo = new BE_REQUERIMIENTO_IMEI();
                Int64 idRequerimientoEO = Int64.Parse(formulario["ihidrequeri"].ToString());
                objRequerimientoImeiEo.idRequerimiento = Convert.ToInt64(idRequerimientoEO);
                objRequerimientoImeiEo.idEstado = UTConstantes.ID_ESTADO_PORDEVOLVER;
                listaResultado = objLogicaRequerimiento.ListaIMEI(objRequerimientoImeiEo);
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "verTabsDevolverInstitucion> info");
                listaResultado = new List<BE_REQUERIMIENTO_IMEI>();
            }

            return PartialView("_PartialTabPorDevolver", listaResultado);
        }

        [HttpPost]
        public ActionResult ValidarDescargaImeisResponder(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {

                var imeisPorResponder = formulario["hdimeisPorResponder"].ToString();
                imeisPorResponder = imeisPorResponder.Substring(0, imeisPorResponder.Length - 1);

                List<BE_REQUERIMIENTO_IMEI> listadoIMEI = new List<BE_REQUERIMIENTO_IMEI>();

                var listaImei = imeisPorResponder.Split("-");

                var hdopcionResponder = formulario["hdopcionResponder"].ToString();
                var hdopcConServicio = formulario["hdopcConServicio"].ToString();

                var idRequerimiento = formulario["ihidrequeri"].ToString();

                BE_REQUERIMIENTO_IMEI objImei = new BE_REQUERIMIENTO_IMEI();
                BL_REQUERIMIENTO_IMEI objLogicaImei = new BL_REQUERIMIENTO_IMEI();
                if (listaImei != null)
                {
                    if (listaImei.Length > 0)
                    {
                        foreach (var item in listaImei)
                        {
                            objImei = new BE_REQUERIMIENTO_IMEI();
                            objImei.idRequerimientoImei = Int64.Parse(item);
                            objImei.idRequerimiento = Convert.ToInt64(idRequerimiento);
                            objImei = objLogicaImei.ConsultarIMEI(objImei);
                            listadoIMEI.Add(objImei);
                        }
                    }
                }

                byte[] FileExcel = null;

                ExportExcel objExportExcel = new ExportExcel();
                if (hdopcionResponder.Equals("1"))
                {
                    if (Int32.Parse(hdopcConServicio) == UTConstantes.ID_TIPOATENCION_CONSERVICIO)
                    {
                        FileExcel = objExportExcel.generarIMEI_SOL(listadoIMEI, "IMEI");
                    }
                    else
                    {
                        FileExcel = objExportExcel.generarIMEI_SOL_SS(listadoIMEI, "IMEI");
                    }
                }

                DateTime hoy = DateTime.Now;
                string fecha = hoy.ToString("ddMMyyyy");
                var rutaTemporal = configuration["CarpetaTemporal"];

                String archivoImei = Path.Combine(rutaTemporal, "IMEIS_SELECCIONADO.xlsx");
                if (System.IO.File.Exists(archivoImei))
                {
                    System.IO.File.Delete(archivoImei);
                }

                System.IO.File.WriteAllBytes(archivoImei, FileExcel);

                result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaImeisResponder");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los archivos de imeis seleccionados");
            }
            return Json(result);
        }



        public FileResult DescargarImeisSeleccionado()
        {
            var rutaTemporal = configuration["CarpetaTemporal"];
            var archivoImei = Path.Combine(rutaTemporal, "IMEIS_SELECCIONADO.xlsx");


            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoImei);
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");            
            String nombreImei = fecha + "_IMEIS.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreImei);
        }


        public FileResult DescargarDocumentoFusionado()
        {
            var rutaTemporal = configuration["CarpetaTemporal"];
            var archivoImei = Path.Combine(rutaTemporal, "CartaRespuesta.docx");


            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoImei);
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");            
            String nombreImei = fecha + "CartaRespuesta.docx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreImei);
        }


        [HttpPost]
        public ActionResult ValidarDescargaDocumentoFusionado(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {

                var imeisPorResponder = formulario["hdimeisPorResponder"].ToString();
                imeisPorResponder = imeisPorResponder.Substring(0, imeisPorResponder.Length - 1);

                List<BE_REQUERIMIENTO_IMEI> listadoIMEI = new List<BE_REQUERIMIENTO_IMEI>();

                var listaImei = imeisPorResponder.Split("-");

                var hdopcionResponder = formulario["hdopcionResponder"].ToString();
                var hdopcConServicio = formulario["hdopcConServicio"].ToString();

                var idRequerimiento = formulario["ihidrequeri"].ToString();

                Int64 idRequerimientoImei = 0;

                var rutaPlantillas = configuration["PlantillasSIGREI"];

                if (hdopcionResponder.Equals("1"))
                {
                    if (Int32.Parse(hdopcConServicio) == UTConstantes.ID_TIPOATENCION_CONSERVICIO)
                    {
                        if (listaImei.Length == 1)
                        {
                            rutaPlantillas = Path.Combine(rutaPlantillas, UTConstantes.CARTA_RESPONDER_CON_SERVICIO_INDIVIDUAL);
                            idRequerimientoImei = Int64.Parse(listaImei[0]);
                        }
                        else
                        {
                            rutaPlantillas = Path.Combine(rutaPlantillas, UTConstantes.CARTA_RESPONDER_VARIOS);
                            idRequerimientoImei = 0;
                        }
                    }
                    else
                    {
                        if (listaImei.Length == 1)
                        {
                            rutaPlantillas = Path.Combine(rutaPlantillas, UTConstantes.CARTA_RESPONDER_SIN_SERVICIO_INDIVIDUAL);
                            idRequerimientoImei = Int64.Parse(listaImei[0]);

                        }
                        else
                        {
                            rutaPlantillas = Path.Combine(rutaPlantillas, UTConstantes.CARTA_RESPONDER_VARIOS);
                            idRequerimientoImei = 0;
                        }
                    }
                }

                String nombreArchivoDoc = "";
                nombreArchivoDoc = "CartaRespuesta.docx";
                var rutaTemporal = configuration["CarpetaTemporal"];
                var rutaArchivoCopiado = Path.Combine(rutaTemporal, nombreArchivoDoc);
                if (System.IO.File.Exists(rutaArchivoCopiado))
                {
                    System.IO.File.Delete(rutaArchivoCopiado);
                }
                System.IO.File.Copy(rutaPlantillas, rutaArchivoCopiado);
                Boolean convertido = false;
                try
                {
                    convertido = fusionarDocumentoWord(rutaArchivoCopiado, Int64.Parse(idRequerimiento), idRequerimientoImei);
                }
                catch (Exception ex)
                {
                    LogError.RegistrarErrorMetodo(ex, "Convertir");
                    result = new KeyValuePair<string, string>("0", "Error al intentar fusionar el documento word con los datos del requerimiento imei");
                }
                if (convertido)
                {
                    result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "No se ha podido fusionar los datos del requerimiento con la plantilla");
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaImeisResponder");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los archivos de imeis seleccionados");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult GrabarRespuestaMovil(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            var nombreArchivo = formulario.Files["fileDocResponderMovil"].FileName;
            // Boolean continuarRegistro = false;
            String mensaje = "";

            result = new KeyValuePair<string, string>("", "");
            IFormFile archivoRespuestaPDF = formulario.Files["fileDocResponderMovil"];
            if (nombreArchivo == null) { nombreArchivo = ""; }
            if (nombreArchivo.Length > UTConstantes.VALOR_CERO)
            {
                string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileDocResponderMovil"].FileName);

                int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
                string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();

                if (archivoRespuestaPDF.Length / 1024 < TamFileGrande)//2MB
                {
                    if (extensionArchivo == ".pdf")
                    {
                        result = new KeyValuePair<string, string>("", "");
                    }
                    else
                    {
                        mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de archivo.";
                        result = new KeyValuePair<string, string>("0", mensaje);
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                }
            }
            if (result.Key == "")
            {
                if (archivoRespuestaPDF.Length > 0)
                {
                    DateTime hoy = DateTime.Now;
                    string fecha = hoy.ToString("ddMMyyyyHHmmss");
                    Int64 idAdjunto = 0;

                    idAdjunto = adjuntarArchivoResponderMovil(formulario);

                    BL_REQUERIMIENTO_SOL objLogicaSol = new BL_REQUERIMIENTO_SOL();
                    BE_REQUERIMIENTO_SOL objParametro = new BE_REQUERIMIENTO_SOL();
                    objParametro.idRequerimiento = Convert.ToInt64(formulario["ihidrequeri"].ToString());
                    objParametro.usuCre = HttpContext.Session.GetString("IdUsuario"); ;
                    objParametro.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;
                    objParametro.idTipoAtencion = 1;
                    objParametro.idDocumentoAdjunto = idAdjunto;
                    objParametro.cantidadAtendido = Int32.Parse(formulario["hdimeiseleccionCantMovil"].ToString());

                    long idSol = objLogicaSol.Insertar(objParametro);
                    /*** enviando el correo con el certificado */
                    BL_PARAMETROGENERAL objLogicaParametroCorreo = new BL_PARAMETROGENERAL();
                    BE_VARIABLEPARAMETRO objParametroCorreo = new BE_VARIABLEPARAMETRO();
                    objParametroCorreo.variable = Convert.ToString( idAdjunto);
                    //objParametroCorreo.idRequerimientoSol = Convert.ToInt32( formulario["hdimeiseleccionMovil"].ToString());
                    objParametroCorreo.idRequerimientoSol = idSol;
                    objParametroCorreo = objLogicaParametroCorreo.valoresNotificarSigreiMovil(objParametroCorreo);

                    Boolean EnviadoCorreo = false;
                    var seEnvia = configuration["enviarConCertificado"].ToString();
                    if (seEnvia.Equals("1"))
                    {
                        EnviadoCorreo = EnviarCorreoConCertificado(objParametroCorreo);
                    }
                    else
                    {
                        EnviadoCorreo = EnviarCorreoSinCertificado(objParametroCorreo);
                    }
                    result = new KeyValuePair<string, string>("1", "Móvil Respondido correctamente");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", " documento con información");
                }
            }
            return Json(result);

        }

        [HttpPost]
        public ActionResult GrabarRespuestaImei(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            var nombreArchivo = formulario.Files["fileDocResponder"].FileName;
            // Boolean continuarRegistro = false;
            String mensaje = "";

            result = new KeyValuePair<string, string>("", "");
            IFormFile archivoRespuestaPDF = formulario.Files["fileDocResponder"];
            if (nombreArchivo == null) { nombreArchivo = ""; }
            if (nombreArchivo.Length > UTConstantes.VALOR_CERO)
            {
                string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileDocResponder"].FileName);

                int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
                string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();

                if (archivoRespuestaPDF.Length / 1024 < TamFileGrande)//2MB
                {
                    if (extensionArchivo == ".pdf")
                    {
                        result = new KeyValuePair<string, string>("", "");
                    }
                    else
                    {
                        mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de archivo.";
                        result = new KeyValuePair<string, string>("0", mensaje);
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                }
            }
            if (result.Key == "")
            {
                if (archivoRespuestaPDF.Length > 0)
                {
                    DateTime hoy = DateTime.Now;
                    string fecha = hoy.ToString("ddMMyyyyHHmmss");
                    Int64 idAdjunto = 0;

                    idAdjunto = adjuntarArchivoResponder(formulario);

                    BE_REQUERIMIENTO_SOL objParametro = new BE_REQUERIMIENTO_SOL();
                    objParametro.idTipoAtencion = Int32.Parse(formulario["hdopcConServicio"].ToString());
                    objParametro.idDocumentoAdjunto = idAdjunto;
                    var imeisPorResponder = formulario["hdimeisPorResponder"].ToString();
                    imeisPorResponder = imeisPorResponder.Substring(0, imeisPorResponder.Length - 1);
                    var listaImei = imeisPorResponder.Split("-");


                    BL_REQUERIMIENTO_SOL objLogicaSol = new BL_REQUERIMIENTO_SOL();

                    objParametro.idRequerimiento = Convert.ToInt64(formulario["ihidrequeri"].ToString());
                    objParametro.usuCre = HttpContext.Session.GetString("IdUsuario"); ;
                    objParametro.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;
                    objParametro.cantidadAtendido = listaImei.Length;

                    long idSol = objLogicaSol.Insertar(objParametro);

                    BE_REQUERIMIENTO_IMEI objEntidad = new BE_REQUERIMIENTO_IMEI();
                    BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI();
                    if (listaImei != null)
                    {
                        foreach (var item in listaImei)
                        {
                            objEntidad.idRequerimiento = Convert.ToInt64(formulario["ihidrequeri"].ToString());
                            objEntidad.idRequerimientoSol = idSol;
                            objEntidad.idImei = Int32.Parse(item);
                            objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario"); ;
                            objEntidad.idEstado = UTConstantes.ID_ESTADO_ATENDIDO;
                            objLogica.AtenderIMEI(objEntidad);
                        }
                    }

                    /*** enviando el correo con el certificado */
                    BL_PARAMETROGENERAL objLogicaParametroCorreo = new BL_PARAMETROGENERAL();
                    BE_VARIABLEPARAMETRO objParametroCorreo = new BE_VARIABLEPARAMETRO();
                    objParametroCorreo.idRequerimientoSol = idSol;
                    objParametroCorreo = objLogicaParametroCorreo.valoresNotificarSigrei(objParametroCorreo);

                    Boolean EnviadoCorreo = false;
                    var seEnvia =configuration["enviarConCertificado"].ToString();
                    if (seEnvia.Equals("1"))
                    {
                        EnviadoCorreo = EnviarCorreoConCertificado(objParametroCorreo);
                    }
                    else {
                        EnviadoCorreo = EnviarCorreoSinCertificado(objParametroCorreo);
                    }
                    result = new KeyValuePair<string, string>("1", "Imeis Respondido correctamente");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", " documento con información");
                }
            }
            return Json(result);

        }

        [HttpPost]
        public ActionResult ValidarDescargaImeisDevolver(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {

                var imeisPorResponder = formulario["hdimeisPorDevolver"].ToString();
                imeisPorResponder = imeisPorResponder.Substring(0, imeisPorResponder.Length - 1);

                List<BE_REQUERIMIENTO_IMEI> listadoIMEI = new List<BE_REQUERIMIENTO_IMEI>();

                var listaImei = imeisPorResponder.Split("-");

                //var hdopcionResponder = formulario["hdopcionResponder"].ToString();
                //var hdopcConServicio = formulario["hdopcConServicio"].ToString();

                var idRequerimiento = formulario["ihidrequeri"].ToString();

                BE_REQUERIMIENTO_IMEI objImei = new BE_REQUERIMIENTO_IMEI();
                BL_REQUERIMIENTO_IMEI objLogicaImei = new BL_REQUERIMIENTO_IMEI();
                if (listaImei != null)
                {
                    if (listaImei.Length > 0)
                    {
                        foreach (var item in listaImei)
                        {
                            objImei = new BE_REQUERIMIENTO_IMEI();
                            objImei.idRequerimientoImei = Int64.Parse(item);
                            objImei.idRequerimiento = Convert.ToInt64(idRequerimiento);
                            objImei = objLogicaImei.ConsultarIMEI(objImei);
                            listadoIMEI.Add(objImei);
                        }
                    }
                }

                byte[] FileExcel = null;

                ExportExcel objExportExcel = new ExportExcel();

                FileExcel = objExportExcel.generarIMEI_SIN_INFO(listadoIMEI, "IMEI");


                DateTime hoy = DateTime.Now;
                string fecha = hoy.ToString("ddMMyyyy");
                var rutaTemporal = configuration["CarpetaTemporal"];

                String archivoImei = Path.Combine(rutaTemporal, "IMEIS_SELECCIONADO.xlsx");
                if (System.IO.File.Exists(archivoImei))
                {
                    System.IO.File.Delete(archivoImei);
                }

                System.IO.File.WriteAllBytes(archivoImei, FileExcel);

                result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaImeisDevolver");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los archivos de imeis seleccionados");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ValidarDescargaDocumentoDevolverFusionado(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                var imeisPorResponder = formulario["hdimeisPorDevolver"].ToString();
                imeisPorResponder = imeisPorResponder.Substring(0, imeisPorResponder.Length - 1);
                var listaImei = imeisPorResponder.Split("-");
                List<BE_REQUERIMIENTO_IMEI> listadoIMEI = new List<BE_REQUERIMIENTO_IMEI>();

                var idRequerimiento = formulario["ihidrequeri"].ToString();

                Int64 idRequerimientoImei = 0;

                var rutaPlantillas = configuration["PlantillasSIGREI"];
                if (listaImei.Length > 1)
                {                
                    rutaPlantillas = Path.Combine(rutaPlantillas, UTConstantes.CARTA_DEVOLVER_VARIOS);
                    idRequerimientoImei = 0;
                }
                else
                {
                    rutaPlantillas = Path.Combine(rutaPlantillas, UTConstantes.CARTA_DEVOLVER_INDIVIDUAL);
                    idRequerimientoImei = Int64.Parse(listaImei[0]);                  
                }


                String nombreArchivoDoc = "";
                nombreArchivoDoc = "CartaRespuesta.docx";
                var rutaTemporal = configuration["CarpetaTemporal"];
                var rutaArchivoCopiado = Path.Combine(rutaTemporal, nombreArchivoDoc);
                if (System.IO.File.Exists(rutaArchivoCopiado))
                {
                    System.IO.File.Delete(rutaArchivoCopiado);
                }
                System.IO.File.Copy(rutaPlantillas, rutaArchivoCopiado);
                Boolean convertido = false;
                try
                {
                    convertido = fusionarDocumentoWord(rutaArchivoCopiado, Int64.Parse(idRequerimiento), idRequerimientoImei);
                }
                catch (Exception ex)
                {
                    LogError.RegistrarErrorMetodo(ex, "Convertir");
                    result = new KeyValuePair<string, string>("0", "Error al intentar fusionar el documento word con los datos del requerimiento imei");
                }
                if (convertido)
                {
                    result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "No se ha podido fusionar los datos del requerimiento con la plantilla");
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaImeisResponder");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los archivos de imeis seleccionados");
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult GrabarDevolucionImei(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();

            var nombreArchivo = formulario.Files["fileDocDevolver"].FileName;
            String mensaje = "";

            result = new KeyValuePair<string, string>("", "");

            IFormFile archivoRespuestaPDF = formulario.Files["fileDocDevolver"];

            if (nombreArchivo == null) { nombreArchivo = ""; }
            if (nombreArchivo.Length > UTConstantes.VALOR_CERO)
            {
                string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileDocDevolver"].FileName);

                int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
                string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();

                if (archivoRespuestaPDF.Length / 1024 < TamFileGrande)//2MB
                {
                    if (extensionArchivo == ".pdf")
                    {
                        if (archivoRespuestaPDF.Length == 0)
                        {
                            result = new KeyValuePair<string, string>("0", " Adjuntar un documento con información");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("", "");
                        }
                    }
                    else
                    {
                        mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de archivo.";
                        result = new KeyValuePair<string, string>("0", mensaje);
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                }
            }
            else
            {
                result = new KeyValuePair<string, string>("1", "Tiene que seleccionar un documento pdf");
            }

            if (result.Key == "")
            {

                DateTime hoy = DateTime.Now;
                string fecha = hoy.ToString("ddMMyyyyHHmmss");
                Int64 idAdjunto = 0;
                Boolean conError = false;
                BE_REQUERIMIENTO_SOL objEntidadSol = new BE_REQUERIMIENTO_SOL();
                BL_REQUERIMIENTO_SOL objLogicaSol = new BL_REQUERIMIENTO_SOL();

                try
                {

                    var imeisPorResponder = formulario["hdimeisPorDevolver"].ToString();
                    imeisPorResponder = imeisPorResponder.Substring(0, imeisPorResponder.Length - 1);
                    var listaImei = imeisPorResponder.Split("-");

                    idAdjunto = adjuntarArchivoDevolver(formulario);
                    objEntidadSol.idRequerimiento = Convert.ToInt64(formulario["ihidrequeri"].ToString());
                    objEntidadSol.idTipoAtencion = UTConstantes.ID_TIPOATENCION_SININFORMACION;
                    objEntidadSol.idDocumentoAdjunto = idAdjunto;
                    objEntidadSol.comentario = formulario["comentariosDevolver"].ToString();
                    objEntidadSol.usuCre = HttpContext.Session.GetString("IdUsuario");
                    objEntidadSol.idEstado = UTConstantes.ID_ESTADO_REGISTRADO;
                    objEntidadSol.cantidadAtendido = listaImei.Length;

                    long idSol = objLogicaSol.Insertar(objEntidadSol);

                    BE_REQUERIMIENTO_IMEI objEntidad = new BE_REQUERIMIENTO_IMEI();
                    BL_REQUERIMIENTO_IMEI objLogica = new BL_REQUERIMIENTO_IMEI();


                    if (listaImei != null)
                    {
                        foreach (var item in listaImei)
                        {
                            objEntidad.idRequerimiento = Convert.ToInt64(formulario["ihidrequeri"].ToString());
                            objEntidad.idRequerimientoSol = idSol;
                            objEntidad.idImei = Int32.Parse(item);
                            objEntidad.idAdjunto = idAdjunto;
                            objEntidad.usuMod = HttpContext.Session.GetString("IdUsuario");
                            objEntidad.idEstado = UTConstantes.ID_ESTADO_ATENDIDO;
                            objLogica.DevolverIMEI(objEntidad);
                        }
                    }
                    /*** enviando el correo con el certificado */
                    BL_PARAMETROGENERAL objLogicaParametro = new BL_PARAMETROGENERAL();
                    BE_VARIABLEPARAMETRO objParametro = new BE_VARIABLEPARAMETRO();
                    objParametro.idRequerimientoSol = idSol;
                    objParametro = objLogicaParametro.valoresNotificarSigrei(objParametro);

                    Boolean EnviadoCorreo = EnviarCorreoConCertificado(objParametro);

                }
                catch (Exception ex)
                {
                    conError = true;
                    LogError.RegistrarErrorMetodo(ex, "Error al intentar devolver los imeis");
                }

                if (conError)
                {
                    result = new KeyValuePair<string, string>("0", "Error al intentar devolver los imeis");
                }
                else { result = new KeyValuePair<string, string>("1", "Imeis Respondido correctamente"); }
            }
            return Json(result);

        }


        #region "Metodos"
        public BE_REQUERIMIENTO obtenerCriterios(IFormCollection formulario)
        {
            BE_REQUERIMIENTO objResultado = new BE_REQUERIMIENTO();

            objResultado.idTipoSolicitante = formulario["ddlTipoSolicitante"] == "" ? 0 : Convert.ToInt32(formulario["ddlTipoSolicitante"].ToString());
            objResultado.nombreInstitucion = formulario["txtNombreInstitucion"].ToString();
            objResultado.nombreSolicitante = formulario["txtNombreSolicitante"].ToString();
            objResultado.nroOficio = formulario["txtNroOficio"].ToString();
            objResultado.nroRequerimientoI = formulario["txtNroRequerimientoI"].ToString();
            objResultado.nroRequerimientoF = formulario["txtNroRequerimientoF"].ToString();
            objResultado.fechaInicio = formulario["txtFechaIngresoInicio"].ToString();
            objResultado.fechaFin = formulario["txtFechaIngresoHasta"].ToString();
            objResultado.idEstado = formulario["ddlEstado"] == "" ? 0 : Convert.ToInt32(formulario["ddlEstado"].ToString());
            objResultado.nroImei = formulario["txtNroImei"].ToString();
            objResultado.usuarioAsignacion = formulario["ddlUsuarioAsignado"] == "" ? "" : formulario["ddlUsuarioAsignado"].ToString();
            objResultado.opcion = UTConstantes.VALOR_UNO;

            return objResultado;
        }

        public Boolean fusionarDocumentoWord(string rutaArchivo, Int64 idRequerimiento, Int64 idRequerimientoImei)
        {

            Boolean convertido = true;

            List<BE_VARIABLEPARAMETRO> listaParametros = new List<BE_VARIABLEPARAMETRO>();

            BE_REQUERIMIENTO_IMEI objParametro = new BE_REQUERIMIENTO_IMEI();
            objParametro.idRequerimiento = idRequerimiento;
            objParametro.idRequerimientoImei = idRequerimientoImei;

            BL_PARAMETROGENERAL obj = new BL_PARAMETROGENERAL();
            listaParametros = obj.obtenerValoresParametroDocumento(objParametro);

            try
            {
                using (docPacking.WordprocessingDocument wordDocument = docPacking.WordprocessingDocument.Open(rutaArchivo, true))
                {
                   
                  

                    var body = wordDocument.MainDocumentPart.Document.Body;
                    var paras = body.Elements<docWodProcesing.Paragraph>();
                    //body.Descendants
                    foreach (var para in paras)
                    {
                        foreach (var run in para.Elements<docWodProcesing.Run>())
                        {
                            foreach (var item in listaParametros)
                            {
                                foreach (var text in run.Elements<docWodProcesing.Text>())
                                {
                                    if (item.variable.Equals(text.Text.ToString()))
                                    {
                                        text.Text = text.Text.Replace(item.variable, item.valorVariable);
                                    }
                                }
                            }
                        }
                    }
                    wordDocument.MainDocumentPart.Document.Save();
                }
            }
            catch (Exception ex)
            {
                convertido = false;
                LogError.RegistrarErrorMetodo(ex, "fusionarDocumentoWord");
            }
            return convertido;
        }


        private long adjuntarArchivoResponderMovil(IFormCollection formulario)
        {
            long idAdjunto = 0;
            try
            {

                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");

                IFormFile fileDocDevolver = formulario.Files["fileDocResponderMovil"];
                var rutaCarpetaSIGREI = configuration["RutaDocumentos"];
                var nombreArchivoImei = Path.GetFileName(formulario.Files["fileDocResponderMovil"].FileName);
                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");


                BL_DOCUMENTO_ADJUNTO objLogicaArchivoAdjunto = new BL_DOCUMENTO_ADJUNTO();
                BE_DOCUMENTO_ADJUNTO objAdjunto = new BE_DOCUMENTO_ADJUNTO();

                if (fileDocDevolver.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaSIGREI, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileDocDevolver.CopyTo(fileStream);
                    }
                    objAdjunto.nombreDocumento = nombreArchivoImei;
                    objAdjunto.descDocumento = "Funcionario Osiptel, Archivo adjuntado al momento de responder a la institución solicitante";

                    objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");
                    idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);

                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "Registrado documento adjunto al responder imei");
                idAdjunto = 0;
            }
            return idAdjunto;
        }
        private long adjuntarArchivoResponder(IFormCollection formulario)
        {
            long idAdjunto = 0;
            try
            {

                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");

                IFormFile fileDocDevolver = formulario.Files["fileDocResponder"];
                var rutaCarpetaSIGREI = configuration["RutaDocumentos"];
                var nombreArchivoImei = Path.GetFileName(formulario.Files["fileDocResponder"].FileName);
                nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");


                BL_DOCUMENTO_ADJUNTO objLogicaArchivoAdjunto = new BL_DOCUMENTO_ADJUNTO();
                BE_DOCUMENTO_ADJUNTO objAdjunto = new BE_DOCUMENTO_ADJUNTO();

                if (fileDocDevolver.Length > 0)
                {
                    var filePath = Path.Combine(rutaCarpetaSIGREI, nombreArchivoImei);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        fileDocDevolver.CopyTo(fileStream);
                    }
                    objAdjunto.nombreDocumento = nombreArchivoImei;
                    objAdjunto.descDocumento = "Funcionario Osiptel, Archivo adjuntado al momento de responder a la institución solicitante";

                    objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");
                    idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);

                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "Registrado documento adjunto al responder imei");
                idAdjunto = 0;
            }
            return idAdjunto;
        }

        private long adjuntarArchivoDevolver(IFormCollection formulario)
        {
            long idAdjunto = 0;
            try
            {

                DateTime fechaDos = DateTime.Now;
                string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");

                IFormFile fileDocDevolver = formulario.Files["fileDocDevolver"];
                var rutaCarpetaSIGREI = configuration["RutaDocumentos"];
                var nombreArchivoImei = Path.GetFileName(formulario.Files["fileDocDevolver"].FileName);

                if (nombreArchivoImei.Length > 0)
                {
                    nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                    BL_DOCUMENTO_ADJUNTO objLogicaArchivoAdjunto = new BL_DOCUMENTO_ADJUNTO();
                    BE_DOCUMENTO_ADJUNTO objAdjunto = new BE_DOCUMENTO_ADJUNTO();

                    if (fileDocDevolver.Length > 0)
                    {
                        var filePath = Path.Combine(rutaCarpetaSIGREI, nombreArchivoImei);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            fileDocDevolver.CopyTo(fileStream);
                        }
                        objAdjunto.nombreDocumento = nombreArchivoImei;
                        objAdjunto.descDocumento = "Funcionario Osiptel, Archivo adjuntado al momento de devolver a la institución solicitante";

                        objAdjunto.usuCre = HttpContext.Session.GetString("IdUsuario");
                        idAdjunto = objLogicaArchivoAdjunto.Insertar(objAdjunto);
                    }
                }
                else
                {
                    idAdjunto = 0;
                }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "Registrado documento adjunto al responder imei");
                idAdjunto = 0;
            }
            return idAdjunto;
        }

        public Boolean EnviarCorreoConCertificado(BE_VARIABLEPARAMETRO objParametro)
        {
            Boolean EnviadoCorreo = false;

            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(objParametro.deParte);
                message.Subject = objParametro.asunto;
                //{
                var Correos = objParametro.para.Split(";");
                foreach (var item in Correos)
                {
                    message.To.Add(new MailAddress(item));
                }

                var conCopia = objParametro.conCopia.Split(";");
                if (conCopia.Length > 0)
                {
                    foreach (var item in conCopia)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }
                var conCopiaOculta = objParametro.conCopiaOculta.Split(";");
                if (conCopiaOculta.Length > 0)
                {
                    foreach (var item in conCopiaOculta)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }

                var certificado = configuration["RutaCertificado"];
                var claveCertificado = configuration["ClaveCertificado"];               
                X509Certificate2 objCert = new X509Certificate2(certificado, claveCertificado, X509KeyStorageFlags.MachineKeySet
                             | X509KeyStorageFlags.PersistKeySet
                             | X509KeyStorageFlags.Exportable);

                //Creamos el ContentInfo  
                ContentInfo objContent = new ContentInfo(Encoding.ASCII.GetBytes("Content-Type: text/html; charset=ISO-8859-1;Content-Transfer-Encoding: 7bit \r\n\r\n" + objParametro.mensaje));
                //Creamos el objeto que representa los datos firmados  
                SignedCms objSignedData = new SignedCms(objContent);
                //Creamos el "firmante"  
                CmsSigner objSigner = new CmsSigner(objCert);
                objSigner.IncludeOption = X509IncludeOption.EndCertOnly;
                //Firmamos los datos  
                //mensaje("Antes de firmar los datos");
                objSignedData.ComputeSignature(objSigner);
                //Obtenemos el resultado  
                byte[] bytSigned = objSignedData.Encode();

                CmsRecipientCollection toCollection = new CmsRecipientCollection();

                //foreach (string address in to)
             

                X509Certificate2 certificate = new X509Certificate2(certificado, claveCertificado, X509KeyStorageFlags.MachineKeySet
                     | X509KeyStorageFlags.PersistKeySet
                     | X509KeyStorageFlags.Exportable);

                CmsRecipient recipient = new CmsRecipient(SubjectIdentifierType.SubjectKeyIdentifier, certificate);
                toCollection.Add(recipient);                

                MemoryStream stream = new MemoryStream(bytSigned);
                AlternateView view = new AlternateView(stream, "application/pkcs7-mime; smime-type=signed-data;name=smime.p7m");

                message.AlternateViews.Add(view);

                message.IsBodyHtml = true;

                SmtpClient client = new SmtpClient(objParametro.hostname, Int32.Parse(objParametro.puerto));
                var UsuarioRemitente = configuration["UsuarioRemitente"];
                var ClaveRemitente = configuration["ClaveRemitente"];
                var Dominio = configuration["Dominio"];

                client.Credentials = new System.Net.NetworkCredential(UsuarioRemitente, ClaveRemitente, Dominio);               

                client.EnableSsl = false;
                client.Send(message);
                EnviadoCorreo = true;
            }
            catch (Exception ex)
            {
                EnviadoCorreo = false;
                LogError.RegistrarErrorMetodo(ex, "EnviarCorreoConCertificado");
            }

            return EnviadoCorreo;
        }

        private Boolean EnviarCorreoSinCertificado(BE_VARIABLEPARAMETRO objParametro)
        {
            Boolean isEnviado = false;
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(objParametro.deParte);
                message.Subject = objParametro.asunto;                
                var Correos = objParametro.para.Split(";");
                foreach (var item in Correos)
                {
                    message.To.Add(new MailAddress(item));
                }

                var conCopia = objParametro.conCopia.Split(";");
                if (conCopia.Length > 0)
                {
                    foreach (var item in conCopia)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }
                var conCopiaOculta = objParametro.conCopiaOculta.Split(";");
                if (conCopiaOculta.Length > 0)
                {
                    foreach (var item in conCopiaOculta)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }           
                message.Body = objParametro.mensaje;
                message.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient(objParametro.hostname, Int32.Parse(objParametro.puerto));

                var UsuarioRemitente = configuration["UsuarioRemitente"];
                var ClaveRemitente = configuration["ClaveRemitente"];
                var Dominio = configuration["Dominio"];

                smtpMail.UseDefaultCredentials = false;
                smtpMail.Credentials = new System.Net.NetworkCredential(UsuarioRemitente, ClaveRemitente, Dominio);
                smtpMail.EnableSsl = false;
                smtpMail.Send(message);
                isEnviado = true;



            }
            catch (Exception ex)
            {
                isEnviado = false;
                LogError.RegistrarErrorMetodo(ex, "EnviarCorreoSinCertificado");

            }
            return isEnviado;
        }
        #endregion



    }
}