﻿using System;
using System.Collections.Generic;


using Microsoft.AspNetCore.Mvc;

using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;


using docPacking = DocumentFormat.OpenXml.Packaging;
using docWodProcesing = DocumentFormat.OpenXml.Wordprocessing;


using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using ClienteSIGREI.Util;
using Newtonsoft.Json;

using System.Net;
using System.IO;
using System.Threading.Tasks;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using OfficeOpenXml.Style.XmlAccess;
using System.Xml;
using System.Linq;
using System.Net.Mail;

using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
namespace ClienteSIGREI.Controllers
{
    public class GestorContrasenia : Controller
    {
        private readonly IConfiguration configuration;
        public GestorContrasenia(IConfiguration config)
        {
            this.configuration = config;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GenerarEnlace()
        {
            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> listaTipoDocumento = ObjLogica.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);
            ViewBag.ddlTipoDocumento = listaTipoDocumento;
            return View();
        }

        [HttpPost]
        public ActionResult GenerarEnlaceUEP(IFormCollection formulario)
        {
            KeyValuePair<string, string> result;

            try
            {

                //VALIDANDO SI EXSTE EL CORREO
                BlUsuarioEP objParametroUEP = new BlUsuarioEP();
                List<BeUsuarioEP> listaUser;
                listaUser = objParametroUEP.SP_VALIDAR_CORREO_USER_EP(formulario["txtMail"].ToString(), formulario["ddlTipoDocumento"].ToString(), formulario["txtUsuario"].ToString());
                if (listaUser.Count > 0)
                {
                    try
                    {
                        List<BeUsuarioEP> listaUserEnlc;
                        listaUserEnlc = objParametroUEP.SP_GENERAR_NUEVO_ENLACE_UEP(listaUser[0].IDUSUARIO);

                        if (listaUserEnlc.Count > 0)
                        {
                            //ENVIANDO CORREO AL USUARIO

                            EnviarCorreoSinCertificado(
                                listaUserEnlc[0].HOSTNAME,
                                listaUserEnlc[0].PUERTO,
                                listaUserEnlc[0].DE_PARTE,
                                listaUserEnlc[0].CLAVE_PARTE,
                                listaUserEnlc[0].PARA,
                                listaUserEnlc[0].ASUNTO,
                                listaUserEnlc[0].MENSAJE,
                                listaUserEnlc[0].COPIA);

                            //
                            result = new KeyValuePair<string, string>("1", "¡Enviamos el nuevo enlace a tu correo electronico por favor verificar!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "El Usuario ya cambio su clave generada.");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "Los datos ingresados no son corectos o usted ya actualizó su contraseña");
                }

                //







            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        public IActionResult CambioClave()
        {
            BlParametro ObPara = new BlParametro();
            List<BE_PARAMETRO> listaTDocumentoUser = ObPara.ListarValoresTipDocumento(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOEP,
                                                                                        UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOEP);
            ViewBag.ddlTipoDocumento = listaTDocumentoUser;

            return View();
        }
        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890*";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--) { res.Append(valid[rnd.Next(valid.Length)]); }
            return res.ToString();
        }


        [HttpPost]
        public ActionResult GenerarNuevaClaveUEP(IFormCollection formulario)
        {
            KeyValuePair<string, string> result;

            try
            {

                //VALIDANDO SI EXSTE EL CORREO
                BlUsuarioEP objParametroUEP = new BlUsuarioEP();
                List<BeUsuarioEP> listaUser;
                Encriptador encry = new Encriptador();
                Constantes consta = new Constantes();
                Random rnd = new Random();
                String nueva_contrasenia = "SEP_" + CreatePassword(6);

                String nueva_contrasenia_enc = encry.EncryptStringToBytes_Aes(nueva_contrasenia, consta.Key256, consta.IVAES256);

                listaUser = objParametroUEP.SP_VALIDAR_CORREO_CAMBIO_CLAVE_USER_EP(formulario["txtMail"].ToString(), formulario["ddlTipoDocumento"].ToString(), formulario["txtUsuario"].ToString());
                if (listaUser.Count > 0)
                {
                    try
                    {
                        List<BeUsuarioEP> listaUserEnlc;
                        listaUserEnlc = objParametroUEP.SP_GENERAR_NUEVO_CLAVE_TEMPORAL_UEP(listaUser[0].IDUSUARIO, nueva_contrasenia_enc, nueva_contrasenia);

                        if (listaUserEnlc.Count > 0)
                        {
                            //ENVIANDO CORREO AL USUARIO
                            EnviarCorreoSinCertificado(
                                listaUserEnlc[0].HOSTNAME,
                                listaUserEnlc[0].PUERTO,
                                listaUserEnlc[0].DE_PARTE,
                                listaUserEnlc[0].CLAVE_PARTE,
                                listaUserEnlc[0].PARA,
                                listaUserEnlc[0].ASUNTO,
                                listaUserEnlc[0].MENSAJE,
                                listaUserEnlc[0].COPIA);

                            //
                            result = new KeyValuePair<string, string>("1", "¡Enviamos la nueva clave a tu correo electrónico por favor verificar!");
                        }
                        else
                        {
                            result = new KeyValuePair<string, string>("0", "El Usuario ya cambio su clave generada.");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError.RegistrarError(ex);
                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                    }
                }
                else
                {
                    result = new KeyValuePair<string, string>("0", "El correo ingresado no es correcto.");
                }

                //







            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }

        public IActionResult GestionContrasenia()
        {

            return View();
        }

        [HttpPost]
        public ActionResult ActualizarContraseniaUEP(IFormCollection formulario)
        {
            BeContrasenia ParametroEPBE = new BeContrasenia();
            KeyValuePair<string, string> result;
            Constantes cont = new Constantes();
            Encriptador encry = new Encriptador();
            try
            {
                BlContrasenia objParametro = new BlContrasenia();
                ParametroEPBE.USUARIO = formulario["txtUsuario"].ToString();

                ParametroEPBE.CONTRASENIA = encry.EncryptStringToBytes_Aes(formulario["txtClave"].ToString(), cont.Key256, cont.IVAES256);

                ParametroEPBE.NUEVA_CONTRASENIA = encry.EncryptStringToBytes_Aes(formulario["txtClaveNew"].ToString(), cont.Key256, cont.IVAES256);

                ParametroEPBE.NUEVA_CONTRASENIA_CONF = encry.EncryptStringToBytes_Aes(formulario["txtClaveNewConf"].ToString(), cont.Key256, cont.IVAES256);

                //VALIDANDO QUE LOS DATOS DE LA CONTRSEÑA ACTUAL Y USUARIO SEAN CORRECTOS
                BlUsuarioEP objParametroUEP = new BlUsuarioEP();
                List<BeUsuarioEP> listaUser;


                listaUser = objParametroUEP.SP_VALIDAR_DATOS_USER_AFTER(formulario["txtUsuario"].ToString(), encry.EncryptStringToBytes_Aes(formulario["txtClave"].ToString(), cont.Key256, cont.IVAES256));

                //CONULTAR PARAMETROS A BD
                BlParametro ObjLogica = new BlParametro();
                List<BE_PARAMETRO> listaParametros1 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRALMIN", "IDTIPCONTRASENIA");
                List<BE_PARAMETRO> listaParametros2 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRALMAY", "IDTIPCONTRASENIA");
                List<BE_PARAMETRO> listaParametros3 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRANCESP", "IDTIPCONTRASENIA");
                List<BE_PARAMETRO> listaParametros4 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRACMIN", "IDTIPCONTRASENIA");
                List<BE_PARAMETRO> listaParametros5 = ObjLogica.ListarValoresContrasenia("COMUN", "GESTORCONTRACMAX", "IDTIPCONTRASENIA");
                string minusculas = "";
                string mayusculas = "";

                string minimo = "";
                string maximo = "";
                for (var i = 0; i < listaParametros1.Count; i++)
                {
                    if (listaParametros1[i].activo == "1")
                    {
                        minusculas = "a-z";
                    }
                    if (listaParametros2[i].activo == "1")
                    {
                        mayusculas = "A-Z";
                    }
                    if (listaParametros4[i].activo == "1")
                    {
                        //minimo = Convert.ToString(listaParametros[i].orden);
                        List<BE_PARAMETRO> listaParametrosMin = ObjLogica.ListarValoresContrasenia("COMUN", "CONTRASENIAMINIMO", "CONTRASENIAMINIMO");
                        minimo = Convert.ToString(listaParametrosMin[0].orden);

                    }
                    if (listaParametros5[i].activo == "1")
                    {
                        //maximo = Convert.ToString(listaParametros[i].orden);
                        List<BE_PARAMETRO> listaParametrosMax = ObjLogica.ListarValoresContrasenia("COMUN", "CONTRASENIAMAXIMO", "CONTRASENIAMAXIMO");
                        maximo = Convert.ToString(listaParametrosMax[0].orden);
                    }
                }
                //
                switch (listaUser.Count)
                {
                    case 0:
                        result = new KeyValuePair<string, string>("0", "Verificar su Usuario o Contraseña ya que son incorrectos.");
                        break;
                    default:
                        //VALIDANDO SI EL USUARIO YA ACTUALIZO SU CONTRASENIA

                        List<BeUsuarioEP> lista;
                        lista = objParametroUEP.SP_VALIDAR_CAMBIO_CONTRASENIA(formulario["txtUsuario"].ToString());

                        switch (lista.Count)
                        {
                            case 0:
                                result = new KeyValuePair<string, string>("0", "Usted ya realizo la actualización de su contraseña");
                                break;
                            default:

                                switch (lista[0].INDCAMBIOCNIA)
                                {
                                    case 2:
                                        //
                                        try
                                        {

                                            //VALIDANDO ESTRUCTURA DE CONTRASEÑA 

                                            Regex r = new Regex(@"^(?=.*\d)(?=.*[" + minusculas + "])(?=.*[" + mayusculas + "]).{" + minimo + "," + maximo + "}$");
                                            string clave = formulario["txtClaveNew"].ToString().Trim();
                                            if (r.IsMatch(clave))
                                            {
                                                String resultado = objParametro.SP_ACTUALIZAR_CONTRASENIA_UEP(ParametroEPBE);
                                                switch (resultado.Trim())
                                                {
                                                    case "1":
                                                        result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                                                        break;
                                                    default:
                                                        result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                                        break;
                                                }

                                            }
                                            else
                                            {
                                                result = new KeyValuePair<string, string>("0", "Sú contraseña debe contener al menos: <br> *1 letra minúscula <br> * 1 letra mayúscula <br> *1 Número o caracter especial <br> *Contener mínimo " + minimo + " caracteres <br> * Contener un máximo " + maximo + " caracteres");
                                            }
                                            //

                                        }
                                        catch (Exception ex)
                                        {
                                            LogError.RegistrarError(ex);
                                            result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                        }
                                        break;
                                    default:
                                        //VALIDANDO FECHA DE EXPIRACION DEL ENLACE
                                        List<BeUsuarioEP> listaExp;
                                        listaExp = objParametroUEP.SP_VALIDAR_EXPIRACION_CONTRASENIA_ENLACE(formulario["txtUsuario"].ToString());
                                        //
                                        switch (listaExp.Count)
                                        {
                                            case 0:
                                                result = new KeyValuePair<string, string>("0", "No se puede realizar el proceso de cambio de clave, ya que el enlace se encuentra expirado, debe generar un nuevo enlace.");
                                                break;
                                            default:
                                                //
                                                try
                                                {
                                                    //VALIDANDO ESTRUCTURA DE CONTRASEÑA 

                                                    Regex r = new Regex(@"^(?=.*\d)(?=.*[" + minusculas + "])(?=.*[" + mayusculas + "]).{" + minimo + "," + maximo + "}$");
                                                    string clave = formulario["txtClaveNew"].ToString().Trim();
                                                    if (r.IsMatch(clave))
                                                    {
                                                        String resultado = objParametro.SP_ACTUALIZAR_CONTRASENIA_UEP(ParametroEPBE);
                                                        switch (resultado.Trim())
                                                        {
                                                            case "1":
                                                                result = new KeyValuePair<string, string>("1", "¡Actualización Correcta!");
                                                                break;
                                                            default:
                                                                result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                                                break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        result = new KeyValuePair<string, string>("0", "Sú contraseña debe contener al menos: <br> *1 letra minúscula <br> * 1 letra mayúscula <br> *1 Número o caracter especial <br> *Contener mínimo " + minimo + " caracteres <br> * Contener un máximo " + maximo + " caracteres");
                                                    }
                                                    //

                                                }
                                                catch (Exception ex)
                                                {
                                                    LogError.RegistrarError(ex);
                                                    result = new KeyValuePair<string, string>("0", "Ocurrió un error al procesar su registro, consulte con el administrador del sistema");
                                                }
                                                break;
                                        }
                                        break;
                                }

                                break;
                        }

                        break;
                }

            }
            catch (Exception ex)
            {
                LogError.RegistrarError(ex);
                result = new KeyValuePair<string, string>("0", "Ocurrió un error al validar el usuario si ya existe");
            }
            return Json(result);
        }
        private Boolean EnviarCorreoSinCertificado(string HOSTNAME, string PUERTO, string DE_PARTE, string CLAVE_PARTE, string PARA, string ASUNTO, string MENSAJE, string COPIA)
        {
            Boolean isEnviado = false;
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(DE_PARTE);
                message.To.Add(new MailAddress(PARA));
                //message.From = new MailAddress("info@chromoperu.com", "SIGREI", System.Text.Encoding.UTF8);
                message.HeadersEncoding = System.Text.Encoding.UTF8;
                message.Subject = ASUNTO;

                message.SubjectEncoding = System.Text.Encoding.UTF8;

                //message.CC.Add(new MailAddress("carlos.sistemas1314@gmail.com"));
                if (COPIA == null)
                {
                    COPIA = "";
                }
                var conCopia = COPIA.Split(";");
                if (conCopia.Length > 0)
                {
                    foreach (var item in conCopia)
                    {
                        if (item != "")
                        {
                            message.CC.Add(new MailAddress(item));
                        }
                    }
                }

                message.Body = MENSAJE;


                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                message.BodyEncoding = Encoding.GetEncoding("utf-8");
                message.SubjectEncoding = Encoding.GetEncoding("utf-8");
                message.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient(HOSTNAME, Int32.Parse(PUERTO));
                //SmtpClient smtpMail = new SmtpClient("mail.chromoperu.com", Int32.Parse("25"));

                var UsuarioRemitente = configuration["UsuarioRemitente"];
                var ClaveRemitente = configuration["ClaveRemitente"];
                var Dominio = configuration["Dominio"];

                smtpMail.UseDefaultCredentials = false;
                smtpMail.Credentials = new System.Net.NetworkCredential(UsuarioRemitente, ClaveRemitente, Dominio);
                //smtpMail.Credentials = new System.Net.NetworkCredential("info@chromoperu.com", "gHMljBTgK1hN");
                smtpMail.EnableSsl = true;
                //smtpMail.
                smtpMail.Send(message);
                isEnviado = true;



            }
            catch (Exception ex)
            {
                isEnviado = false;
                LogError.RegistrarErrorMetodo(ex, "EnviarCorreoSinCertificado");

            }
            return isEnviado;
        }

    }
}
