﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Osiptel.Clases;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BE.General;
using Osiptel.SIGREI.BE.Reporte;
using Osiptel.SIGREI.BE.Reporte.Req;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class EstadisticoGerencialController : Controller
    {
        private readonly IConfiguration configuration;
        public EstadisticoGerencialController(IConfiguration config)
        {
            configuration = config;
        }
        public IActionResult Index()
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            ViewBag.ListaAnio = CargaComboAnio();
            ViewBag.ListaRequerimiento = CargarCombo(UTConstantes.APLICACION_TABLA.TIPOREQUERIMIENTO, UTConstantes.APLICACION_COLUMNA.IDTIPOREQUERIMIENTO);
            //ViewBag.ListaRequerimiento = CargarComboRequerimiento();
            ViewBag.ListaInstitucion = CargarCombo(UTConstantes.APLICACION_TABLA.TIPOSOLICITANTE, UTConstantes.APLICACION_COLUMNA.IDTIPOSOLICITANTE);
            ViewBag.ListaPeriodo = objLogica.ListaComboMes();
            ViewBag.ListaRegion = objLogica.ListaComboUbigeo();
            ViewBag.ListaEO = objLogica.ListaComboEO();
            return View();
        }

        public List<BE_COMBO> CargaComboAnio()
        {
            Funciones objFunciones = new Funciones();
            List<BE_COMBO> lstAnio = new List<BE_COMBO>();
            BE_COMBO objAnio;

            String anioMinimo = objFunciones.ObtenerAnio();
            int anioactual = DateTime.Now.Year;
            Int32 anioInicio = Int32.Parse(anioMinimo.ToString());

            for (int i = anioactual; i >= anioInicio; i--)
            {
                objAnio = new BE_COMBO();
                objAnio.id = i.ToString();
                objAnio.descripcion = i.ToString();

                lstAnio.Add(objAnio);
            }
            return lstAnio;
        }

        public List<BE_COMBO> CargarCombo(String tabla, String columna)
        {
            List<BE_COMBO> lstCombo = new List<BE_COMBO>();
            BE_COMBO objR;
            BlParametro objParametro = new BlParametro();
            List<BE_PARAMETRO> lstParametro = objParametro.ListarValores(UTConstantes.APLICACION, tabla, columna);
            foreach (var item in lstParametro)
            {
                objR = new BE_COMBO();
                objR.id = item.valor;
                objR.descripcion = item.descripcion;
                lstCombo.Add(objR);
            }
            return lstCombo;
        }

        public List<BE_COMBO> CargarComboRequerimiento()
        {
            List<BE_COMBO> lstCombo = new List<BE_COMBO>();
            BE_COMBO objR;
            objR = new BE_COMBO();
            objR.id = "1";
            objR.descripcion = "SERVICIO MÓVIL";
            lstCombo.Add(objR);
            objR = new BE_COMBO();
            objR.id = "2";
            objR.descripcion = "IMEI FÍSICO";
            lstCombo.Add(objR);
            objR = new BE_COMBO();
            objR.id = "3";
            objR.descripcion = "SERVICIO MÓVIL - IMEI FÍSICO";
            lstCombo.Add(objR);
            return lstCombo;
        }

        [HttpPost]
        public ActionResult GraficarEOGerencial(string anio, string requerimiento, string EmpOpe, string InsSol, string periodo)
        {
            try
            {
                BL_REPORTE objLogica = new BL_REPORTE();
                BE_GERENCIAL_EO objEO = new BE_GERENCIAL_EO();
                objEO.anio = anio;
                objEO.mes = periodo;
                objEO.empresa = EmpOpe;
                objEO.requerimiento = requerimiento;
                objEO.institucion = InsSol;

                DataTable obj = objLogica.GraficaGerencialEO(objEO);

                var label = new List<String>()
                {
                    "EN PLAZO","FUERA DE PLAZO"
                };

                List<String> paseDatosColores = new List<string>()
                    {
                        "#3e95cd",
                        "#8e5ea2",
                        "#3cba9f",
                        "#0000FF",
                        "#8A2BE2",
                        "#A52A2A"
                    };

                BE_GRAFICA barraAtendido = new BE_GRAFICA();
                barraAtendido.labels = label;

                BE_GRAFICA barraPendiente = new BE_GRAFICA();
                barraPendiente.labels = label;

                BE_GRAFICA_PIE pieGrafica = new BE_GRAFICA_PIE();

                List<BE_GRAFICA_FORMATO> atendidoFormato = new List<BE_GRAFICA_FORMATO>();
                List<BE_GRAFICA_FORMATO> pendienteFormato = new List<BE_GRAFICA_FORMATO>();
                List<BE_PIE_GRAFICA_FORMATO> objFormatoPie = new List<BE_PIE_GRAFICA_FORMATO>();
                List<int> cantidadPie = new List<int>();

                var empresa = (from d in obj.Select().AsEnumerable() select d["empresa"].ToString()).Distinct().ToList();
                var indexEO = 0;

                if (indexEO > 6)
                {
                    indexEO = 0;
                }

                foreach (var item in empresa)
                {
                    List<int> cantA = new List<int>();
                    List<int> cantP = new List<int>();
                    List<int> cantPieA = new List<int>();
                    List<int> cantPieP = new List<int>();
                    foreach (var itemL in label)
                    {
                        var cantidadA = obj.Select().SingleOrDefault(x => x["empresa"].ToString() == item && x["atendido"].ToString() == itemL);
                        var cantidadP = obj.Select().SingleOrDefault(x => x["empresa"].ToString() == item && x["pendiente"].ToString() == itemL);
                        cantA.Add(cantidadA == null ? 0 : Convert.ToInt32(cantidadA["cantatendidos"]));
                        cantP.Add(cantidadP == null ? 0 : Convert.ToInt32(cantidadP["cantpendientes"]));

                        cantPieA.Add(cantidadA == null ? 0 : Convert.ToInt32(cantidadA["cantatendidos"]));
                        cantPieP.Add(cantidadP == null ? 0 : Convert.ToInt32(cantidadP["cantpendientes"]));
                    }

                    var sumatoria = cantPieA.Sum() + cantPieP.Sum();

                    cantidadPie.Add(sumatoria);

                    BE_GRAFICA_FORMATO aF = new BE_GRAFICA_FORMATO();
                    aF.label = item;
                    aF.data = cantA;
                    aF.backgroundColor = paseDatosColores[indexEO];
                    atendidoFormato.Add(aF);

                    BE_GRAFICA_FORMATO pF = new BE_GRAFICA_FORMATO();
                    pF.label = item;
                    pF.data = cantP;
                    pF.backgroundColor = paseDatosColores[indexEO];
                    pendienteFormato.Add(pF);
                    indexEO++;
                }

                barraAtendido.datasets = atendidoFormato;
                barraPendiente.datasets = pendienteFormato;

                BE_PIE_GRAFICA_FORMATO oPie = new BE_PIE_GRAFICA_FORMATO();
                oPie.label = "DataSets 1";
                oPie.data = cantidadPie;
                oPie.backgroundColor = paseDatosColores;

                objFormatoPie.Add(oPie);

                pieGrafica.labels = empresa;
                pieGrafica.datasets = objFormatoPie;

                var resultado = new { barraAtendido, barraPendiente, pieGrafica };

                return Json(resultado);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult TablaEOGerencial(IFormCollection formulario)
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            BE_GERENCIAL_EO objEO = new BE_GERENCIAL_EO();
            objEO.anio = formulario["anio"];
            objEO.mes = formulario["periodo"];
            objEO.empresa = formulario["EmpOpe"];
            objEO.requerimiento = formulario["requerimiento"];
            List<BE_TABLA_EO> obj = objLogica.TablaGerencialEO(objEO);

            List<List<String>> dataSeteada = new List<List<String>>();

            var parseGroup = (from u in obj
                              group u by new { u.empresa, u.tiposervicio, u.estado, u.atencion } into g
                              select new BE_TABLA_EO()
                              {
                                  empresa = g.Key.empresa,
                                  tiposervicio = g.Key.tiposervicio,
                                  estado = g.Key.estado,
                                  atencion = g.Key.atencion
                              }).ToList();

            var Meses = (from u in obj orderby u.idMes group u by new { u.idMes, u.mes } into g select g.Key).ToList();
            List<string> parseMeses = (from u in Meses select u.mes).ToList();
            List<List<string>> parseDiasFront = (from u in Meses
                                                 select (from m in obj
                                                         where m.idMes == u.idMes
                                                         group m by m.dia into g
                                                         orderby g.Key
                                                         select g.Key).ToList()).ToList();


            foreach (var item in parseGroup)
            {
                List<String> nuevaEntidad = new List<String>();
                List<int> calcularTotal = new List<int>();
                nuevaEntidad.Add(item.empresa);
                nuevaEntidad.Add(item.tiposervicio);
                nuevaEntidad.Add(item.estado);
                nuevaEntidad.Add(item.atencion);
                for (var i = 0; i < parseMeses.Count(); i++)
                {
                    var buscarEntidad = obj.Where(x => x.empresa == item.empresa
                                                && x.tiposervicio == item.tiposervicio
                                                && x.estado == item.estado
                                                && x.atencion == item.atencion
                                                && x.mes == parseMeses[i]).Select(x => x).ToList();
                    foreach (var item4 in parseDiasFront[i])
                    {
                        var diaVal = buscarEntidad.Where(x => x.dia == item4).Select(x => x.cantidad == null ? 0 : Convert.ToInt32(x.cantidad)).Sum();

                        nuevaEntidad.Add(diaVal.ToString());
                        calcularTotal.Add(diaVal);
                    }
                }
                nuevaEntidad.Add(calcularTotal.Sum().ToString());
                dataSeteada.Add(nuevaEntidad);
            }

            var oR = new
            {
                data = dataSeteada,
                dias = parseDiasFront,
                meses = parseMeses
            };

            return Json(oR);
        }

        [HttpPost]
        public ActionResult TablaInsGerencial(IFormCollection formulario)
        {
            BL_REPORTE objLogica = new BL_REPORTE();
            BE_GERENCIAL_EO objEO = new BE_GERENCIAL_EO();
            objEO.anio = formulario["anio"];
            objEO.mes = formulario["periodo"];
            objEO.institucion = formulario["InsSol"];
            objEO.requerimiento = formulario["requerimiento"];
            List<BE_TABLA_INSTITUCION> obj = objLogica.TablaGerencialIns(objEO);

            List<List<String>> dataSeteada = new List<List<String>>();

            var parseGroup = (from u in obj
                              group u by new { u.entidad, u.externa, u.unidad, u.ruc, u.servicio, u.estado } into g
                              select g.Key).ToList();

            var Meses = (from u in obj orderby u.idMes group u by new { u.idMes, u.mes } into g select g.Key).ToList();
            List<string> parseMeses = (from u in Meses select u.mes).ToList();
            List<List<string>> parseDiasFront = (from u in Meses
                                                 select (from m in obj
                                                         where m.idMes == u.idMes
                                                         group m by m.dia into g
                                                         orderby g.Key
                                                         select g.Key).ToList()).ToList();

            foreach (var item in parseGroup)
            {
                List<String> nuevaEntidad = new List<String>();
                List<int> calcularTotal = new List<int>();
                nuevaEntidad.Add(item.estado);
                nuevaEntidad.Add(item.entidad);
                nuevaEntidad.Add(item.externa);
                nuevaEntidad.Add(item.unidad);
                nuevaEntidad.Add(item.ruc);
                for (var i = 0; i < parseMeses.Count(); i++)
                {
                    var buscarEntidad = obj.Where(x => x.estado == item.estado
                                                && x.entidad == item.entidad
                                                && x.externa == item.externa
                                                && x.unidad == item.unidad
                                                && x.ruc == item.ruc
                                                && x.mes == parseMeses[i]).Select(x => x).ToList();
                    foreach (var item4 in parseDiasFront[i])
                    {
                        var diaVal = buscarEntidad.Where(x => x.dia == item4).Select(x => x.cantidad == null ? 0 : Convert.ToInt32(x.cantidad)).Sum();

                        nuevaEntidad.Add(diaVal.ToString());
                        calcularTotal.Add(diaVal);
                    }
                }
                nuevaEntidad.Add(calcularTotal.Sum().ToString());
                dataSeteada.Add(nuevaEntidad);
            }

            var oR = new
            {
                data = dataSeteada,
                dias = parseDiasFront,
                meses = parseMeses
            };

            return Json(oR);
        }

        [HttpPost]
        public ActionResult ExportarReporte(string parametro)
        {
            try
            {
                BL_REPORTE objLogica = new BL_REPORTE();
                BE_GERENCIAL_EO objEO = new BE_GERENCIAL_EO();

                var parametros = parametro.Split("|");

                objEO.anio = parametros[0];
                objEO.requerimiento = parametros[1];
                objEO.empresa = parametros[2];
                objEO.institucion = parametros[3];
                objEO.mes = parametros[4] == "0" ? "1,2,3,4,5,6,7,8,9,10,11,12" : parametros[4];

                DataTable obj = objLogica.ExportarReporteGeneral(objEO);

                var stream = new MemoryStream();
                using (ExcelPackage p = new ExcelPackage(stream))
                {
                    var excel = p.Workbook.Worksheets.Add("Matriz General");
                    excel.Cells[1, 1].Value = "NRO. DE REQUERIMIENTO";
                    excel.Cells[1, 2].Value = "REGION";
                    excel.Cells[1, 3].Value = "ENTIDAD PUBLICA";
                    excel.Cells[1, 4].Value = "UNIDAD ENTIDAD PUBLICA";
                    excel.Cells[1, 5].Value = "SUB UNIDAD ENTIDAD PUBLICA";
                    excel.Cells[1, 6].Value = "PERSONA SOLICITANTE";
                    excel.Cells[1, 7].Value = "CARGO DE PERSONA SOLICITANTE";
                    excel.Cells[1, 8].Value = "NRO. DE CARPETA FISCAL/DENUNCIA";
                    excel.Cells[1, 9].Value = "TIPO DE REQUERIMIENTO";
                    excel.Cells[1, 10].Value = "ESTADO DE INFORMACION";
                    excel.Cells[1, 11].Value = "EMPRESA";
                    excel.Cells[1, 12].Value = "ESTADO EN EO";
                    excel.Cells[1, 13].Value = "TIPO DE ESTADO EO";
                    excel.Cells[1, 14].Value = "FECHA DE REGISTRO DEL REQUERIMIENTO";
                    excel.Cells[1, 15].Value = "FECHA DE ASIGNACION";
                    excel.Cells[1, 16].Value = "PERSONA QUE LO TIENE ASIGNADO PARA ATENCION";
                    excel.Cells[1, 17].Value = "CANTIDAD DE SOLICITUDES POR REQUERIMIENTO";
                    excel.Cells[1, 18].Value = "NRO DE RESPUESTA DEL REQUERIMIENTO";
                    excel.Cells[1, 19].Value = "FECHA DE RESPUESTA DEL REQUERIMIENTO";
                    excel.Cells[1, 20].Value = "CORREO ELECTRONICO DESTINATARIO";
                    excel.Cells[1, 21].Value = "FECHA/HORA DE ENVIO DE NOTIFICACIÓN";
                    excel.Cells[1, 22].Value = "NRO IMEI/NRO MOVIL";

                    excel.Cells["A1:V1"].Style.Font.Color.SetColor(Color.White);
                    excel.Cells["A1:V1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    excel.Cells["A1:V1"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#4472C4"));

                    for (int i = 1; i < 23; i++)
                    {
                        excel.Column(i).Width = 18;
                        excel.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        excel.Column(i).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        excel.Column(i).Style.WrapText = true;
                    }
                    int cont = 2;
                    int filtro = 0;
                    string ay = "";
                    for (int i = 0; i < obj.Rows.Count; i++)
                    {
                        var ayuda = 0;
                        for (int j = 0; j < obj.Rows.Count; j++)
                        {
                            if (obj.Rows[i]["IDREQUERIMIENTO"].ToString().Trim() == obj.Rows[j]["IDREQUERIMIENTO"].ToString().Trim())
                            {
                                ay = obj.Rows[i]["IDREQUERIMIENTO"].ToString().Trim();
                                ayuda++;
                            }
                        }
                        excel.Cells[cont, 1].Value = obj.Rows[i]["nrorequerimiento"];
                        excel.Cells[cont, 2].Value = obj.Rows[i]["DEPARTAMENTO"];
                        excel.Cells[cont, 3].Value = obj.Rows[i]["ENTIDAD"];
                        excel.Cells[cont, 4].Value = obj.Rows[i]["DES_RAZ_SOC"];
                        excel.Cells[cont, 5].Value = obj.Rows[i]["NOMBREUNIDADP"];
                        excel.Cells[cont, 6].Value = obj.Rows[i]["NOMSOLI"];
                        excel.Cells[cont, 7].Value = obj.Rows[i]["cargo"];
                        excel.Cells[cont, 8].Value = obj.Rows[i]["NRODOCREFENCIADO"];
                        excel.Cells[cont, 9].Value = obj.Rows[i]["TIPOREQ"];
                        excel.Cells[cont, 10].Value = obj.Rows[i]["TIPOINFO"];
                        excel.Cells[cont, 11].Value = obj.Rows[i]["EMPRESA"];
                        excel.Cells[cont, 12].Value = obj.Rows[i]["ESTADO"];
                        excel.Cells[cont, 13].Value = obj.Rows[i]["TIPOESTADO"];
                        excel.Cells[cont, 14].Style.Numberformat.Format = "dd/mm/yyyy";
                        excel.Cells[cont, 14].Value = obj.Rows[i]["FECREGISTRO"];
                        excel.Cells[cont, 15].Style.Numberformat.Format = "dd/mm/yyyy";
                        excel.Cells[cont, 15].Value = obj.Rows[i]["FECASIGNACION"];
                        excel.Cells[cont, 16].Value = obj.Rows[i]["USUASIGNACION"];
                        excel.Cells[cont, 17].Value = obj.Rows[i]["CANTIDAD"];
                        excel.Cells[cont, 18].Value = obj.Rows[i]["NRORESRQ"];
                        excel.Cells[cont, 22].Value = obj.Rows[i]["VAL_REG"];


                        excel.Cells[cont, 20].Value = obj.Rows[i]["CORREO"];
                        if (Convert.ToString(obj.Rows[i]["IND_EXCEPCION"]).Trim() != "0")
                        {
                            excel.Cells[cont, 19].Style.Numberformat.Format = "dd/mm/yyyy";
                            excel.Cells[cont, 19].Value = obj.Rows[i]["FECREGRQ"];
                            excel.Cells[cont, 21].Style.Numberformat.Format = "dd/mm/yyyy";
                            excel.Cells[cont, 21].Value = obj.Rows[i]["FECHORREGRQ"];
                        }
                        else
                        {
                            excel.Cells[cont, 19].Value = "";
                            excel.Cells[cont, 21].Value = "";
                        }



                        if ((i + 1) < obj.Rows.Count)
                        {
                            if (obj.Rows[i]["IDREQUERIMIENTO"].ToString().Trim() != obj.Rows[i + 1]["IDREQUERIMIENTO"].ToString().Trim())
                            {
                                if (filtro == 0)
                                {
                                    excel.Cells["Q2:Q" + cont].Merge = true;
                                    filtro = 1;
                                }
                                else
                                {
                                    //excel.Cells["Q" + (cont - ayuda + 1) + ":Q" + cont].Merge = true;
                                    //excel.Cells["Q" + (cont - ayuda + 1) + ":Q" + cont].Merge = false;
                                    //excel.Cells["Q" + (cont - ayuda + 1) + ":Q" + cont].Clear();
                                }

                            }
                        }
                        else
                        {
                            if (filtro == 0)
                            {
                                excel.Cells["Q2:Q" + cont].Merge = true;
                                filtro = 1;
                            }
                            else
                            {
                                //excel.Cells["Q" + (cont - ayuda + 1) + ":Q" + cont].Merge = true;
                                //excel.Cells["Q" + (cont - ayuda + 1) + ":Q" + cont].Merge = false;
                                //excel.Cells["Q" + (cont - ayuda + 1) + ":Q" + cont].Clear();
                            }

                        }



                        cont++;
                    }

                    //excel.Cells["A2"].LoadFromDataTable(obj,false);

                    p.Save();

                    string handle = Guid.NewGuid().ToString();
                    stream.Position = 0;
                    //TempData[handle] = stream.ToArray();

                    HttpContext.Session.Set(handle, stream.ToArray());

                    return new JsonResult(new
                    {
                        Data = new { FileGuid = handle, FileName = "ReporteGeneral.xlsx" }
                    });

                    //HttpContext.Session.Set("DownloadExcel_FileManager", p.GetAsByteArray());

                    //return Json("");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        public virtual ActionResult Download(string fileGuid, string fileName)
        {

            if (HttpContext.Session.Get(fileGuid) != null)
            {
                try
                {
                    byte[] data = HttpContext.Session.Get(fileGuid) as byte[];
                    return File(data, "application/vnd.ms-excel", fileName);   
                }
                catch(Exception ex)
                {
                    return new EmptyResult();
                }
                finally{
                    HttpContext.Session.Remove(fileGuid);
                }
                
            }
            else
            {
                return new EmptyResult();
            }
        }

        [HttpPost]
        public ActionResult ObtenerInfoGrafico(IFormCollection formulario)
        {
            var anio = formulario["ddlAnio"];
            var tipoReporte = formulario["ddlTipoReporte"];
            BE_REPORTE_IMEI objReporte = new BE_REPORTE_IMEI();
            BL_REPORTE objLogica = new BL_REPORTE();

            objReporte.idAnio = anio == "" ? 0 : Convert.ToInt32(anio);
            List<BE_REPORTE_IMEI> lista = new List<BE_REPORTE_IMEI>();
            var objeto = new BE_GENERAL();

            if (tipoReporte == "1")
            {
                lista = objLogica.ListaPromedioAtencionEO(objReporte);
                objeto.listaTitulo = new List<string>();
                if (lista != null)
                {
                    if (lista.Count > 0)
                    {
                        foreach (var item in lista)
                        {
                            objeto.listaTitulo.Add(item.descripcion);
                        }
                    }
                }
                objeto.listaResultado = lista;
                objeto.idCondicion = "1";
            }
            else if (tipoReporte == "2")
            {
                lista = objLogica.ListaRatioDiasAtencionOsiptel(objReporte);
                objeto.idCondicion = "1";
                objeto.listaTitulo = new List<string>();
                if (lista != null)
                {
                    if (lista.Count > 0)
                    {
                        foreach (var item in lista)
                        {
                            objeto.listaTitulo.Add(item.descripcion);
                        }
                    }
                }
                objeto.listaResultado = lista;
            }
            else if (tipoReporte == "3")
            {
                lista = objLogica.ListaRatioPorcentajeAtencionOsiptel(objReporte);
                objeto.idCondicion = "1";
                objeto.listaTitulo = new List<string>();
                if (lista != null)
                {
                    if (lista.Count > 0)
                    {
                        foreach (var item in lista)
                        {
                            objeto.listaTitulo.Add(item.descripcion);
                        }
                    }
                }
                objeto.listaResultado = lista;
            }
            else if (tipoReporte == "4")
            {
                lista = objLogica.ListaPromedioAtencionOsiptel(objReporte);
                objeto.idCondicion = "1";
                objeto.listaTitulo = new List<string>();
                if (lista != null)
                {
                    if (lista.Count > 0)
                    {
                        foreach (var item in lista)
                        {
                            objeto.listaTitulo.Add(item.descripcion);
                        }
                    }
                }
                objeto.listaResultado = lista;
            }
            else
            {
                objeto.idCondicion = "0";
                objeto.descripcion = "Error al obtener la información";
            }
            return Json(objeto);
        }

        [HttpPost]
        public ActionResult BuscarInformacion(IFormCollection formulario)
        {
            var anio = formulario["ddlAnio"];
            var tipoReporte = formulario["ddlTipoReporte"];
            BE_REPORTE_IMEI objReporte = new BE_REPORTE_IMEI();
            BL_REPORTE objLogica = new BL_REPORTE();
            objReporte.idAnio = anio == "" ? 0 : Convert.ToInt32(anio);
            List<BE_REPORTE_IMEI> lista = new List<BE_REPORTE_IMEI>();


            if (tipoReporte == "1")
            {
                lista = objLogica.ListaPromedioAtencionEO(objReporte);
                ViewBag.TipoReporte = "Días en Promedio que Atiende las empresas Operadoras";
            }
            else if (tipoReporte == "2")
            {
                lista = objLogica.ListaRatioDiasAtencionOsiptel(objReporte);
                ViewBag.TipoReporte = "Ratio de Atencion";

                if (lista.Count > 0)
                {
                    BE_REPORTE_IMEI objTotalFila = new BE_REPORTE_IMEI();
                    decimal totalEnero = 0, totalFebrero = 0, totalMarzo = 0, totalAbril = 0, totalMayo = 0, totalJunio = 0,
                        totalJulio = 0, totalAgosto = 0, totalSetiembre = 0, totalOctubre = 0, totalNoviembre = 0, totalDiciembre = 0;

                    foreach (var item in lista)
                    {
                        totalEnero = totalEnero + item.enero;
                        totalFebrero = totalFebrero + item.febrero;
                        totalMarzo = totalMarzo + item.marzo;
                        totalAbril = totalAbril + item.abril;
                        totalMayo = totalMayo + item.mayo;
                        totalJunio = totalJunio + item.junio;
                        totalJulio = totalJulio + item.julio;
                        totalAgosto = totalAgosto + item.agosto;
                        totalSetiembre = totalSetiembre + item.setiembre;
                        totalOctubre = totalOctubre + item.octubre;
                        totalNoviembre = totalNoviembre + item.noviembre;
                        totalDiciembre = totalDiciembre + item.diciembre;
                    }
                    objTotalFila.descripcion = "Total";
                    objTotalFila.enero = totalEnero;
                    objTotalFila.febrero = totalFebrero;
                    objTotalFila.marzo = totalMarzo;
                    objTotalFila.abril = totalAbril;
                    objTotalFila.mayo = totalMayo;
                    objTotalFila.junio = totalJunio;
                    objTotalFila.julio = totalJulio;
                    objTotalFila.agosto = totalAgosto;
                    objTotalFila.setiembre = totalSetiembre;
                    objTotalFila.octubre = totalOctubre;
                    objTotalFila.noviembre = totalNoviembre;
                    objTotalFila.diciembre = totalDiciembre;
                    lista.Add(objTotalFila);
                }
            }
            else if (tipoReporte == "3")
            {
                lista = objLogica.ListaRatioPorcentajeAtencionOsiptel(objReporte);
                ViewBag.TipoReporte = "Ratio de Atencion (%)";
            }
            else if (tipoReporte == "4")
            {
                lista = objLogica.ListaPromedioAtencionOsiptel(objReporte);
                ViewBag.TipoReporte = "Días en Promedio que Atiende en Osiptel";
            }

            return PartialView("_DetalleIndex", lista);
        }


    }
}