﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OracleClient;
using System.IO;
using System.Linq;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class ConsultaController : Controller
    {
        private readonly IConfiguration configuration;


        public ConsultaController(IConfiguration config)
        {
            this.configuration = config;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ConsultaGSMA()
        {
            return View();
        }

        public IActionResult ConsultaAbonado()
        {

            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> listado = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOABONADO, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOABONADO);
            ViewBag.Lista_TipoDocumento = listado;

            return View();
        }
        public IActionResult ConsultaListaNegra()
        {
            return View();
        }

        public IActionResult ConsultaAbonadoPG()
        {
            BlParametro ObjLogica = new BlParametro();
            List<BE_PARAMETRO> listado = ObjLogica.ListarValores(UTConstantes.APLICACION, UTConstantes.APLICACION_TABLA.TIPODOCUMENTOABONADO, UTConstantes.APLICACION_COLUMNA.IDTIPODOCUMENTOABONADO);
            ViewBag.Lista_TipoDocumento = listado;

            return View();
        }
        public IActionResult ConsultaListaNegraPG()
        {
            return View();
        }

        public IActionResult ConsultaVinculacion()
        {
            return View();
        }
        public IActionResult ConsultaEIR()
        {
            return View();
        }

        #region "GMSA"
        [HttpPost]
        public ActionResult BuscarInformacionGSMA(DataTableParamModel request, BE_GSMA models)
        {

            var paginacion = new BE_PAGINACION
            {

                Tamanio_Pagina = request.length,
                Numero_Pagina = (request.start / request.length) + 1
            };




            List<BE_GSMA> ListBingo = new List<BE_GSMA>();
            if (models.opcion == 1)
            {
                ListBingo = new List<BE_GSMA>();
            }
            else
            {
                ListBingo = new BL_GSMA().Listar(ref paginacion, models);
            }
            var data = ListBingo.Select(x => new[]{

                                                  x.nroItem.ToString()
                                                , x.nroTAc
                                                , x.marca
                                                , x.modeloComercial
                                                , x.modeloTecnico
                                }).ToArray();

            return Json(new

            {

                sEcho = string.Empty,
                aaData = data,
                iTotalRecords = paginacion.Total_Archivos,
                iTotalDisplayRecords = paginacion.Total_Archivos

            });

        }

        [HttpPost]
        public ActionResult ValidarDescargaExportarGSMA(BE_GSMA objGSMA)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                DataTable obResultado = new DataTable();
                BL_GSMA objLogica = new BL_GSMA();

                obResultado = objLogica.ListarConsultaGSMA_DT(objGSMA);

                byte[] FileExcel = null;
                ExportExcel objExportExcel = new ExportExcel();
                FileExcel = objExportExcel.exportarConsultaGSMA(obResultado, "Reporte");


                var rutaTemporal = configuration["CarpetaTemporal"];

                String archivoImei = Path.Combine(rutaTemporal, "ReporteGSMA.xlsx");
                if (System.IO.File.Exists(archivoImei))
                {
                    System.IO.File.Delete(archivoImei);
                }

                System.IO.File.WriteAllBytes(archivoImei, FileExcel);

                result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaExportarGSMA");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los datos de la consulta gsma");
            }
            return Json(result);
        }

        public FileResult DescargarInformacionGSMA()
        {
            var rutaTemporal = configuration["CarpetaTemporal"];
            var archivoImei = Path.Combine(rutaTemporal, "ReporteGSMA.xlsx");


            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoImei);
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");
            String nombreImei = fecha + "_Reporte.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreImei);
        }
        #endregion

        #region "Lista de Abonados"
        [HttpPost]
        public ActionResult ValidarCriteriosBusqueda(IFormCollection formulario)
        {
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();


            String tipoConsulta = "";
            String cadenaFinal = "";
            tipoConsulta = formulario["tipoBuscador"];
            String mensaje = "";
            Boolean continuarBusqueda = false;
            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            if (tipoConsulta == "1")// POR NUMERO DE TELEFONO
            {
                string valorIndividual = formulario["txtValorIndividualTelefono"];
                if (valorIndividual.Length == 0)
                {
                    string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileNumeroTelefono"].FileName);
                    IFormFile archivo = formulario.Files["fileNumeroTelefono"];
                    if (archivo.Length / 1024 < TamFileGrande)//2MB
                    {
                        if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                        {
                            continuarBusqueda = true;
                        }
                        else
                        {
                            mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                            keyValueError = new KeyValuePair<string, string>("0", mensaje);
                        }
                    }
                    else
                    {
                        keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);

                    }

                    if (keyValueError.Key != "")
                    {
                        #region "Copiar a una carpeta temporal para revisar si hay registro que leer"


                        DateTime fechaDos = DateTime.Now;
                        string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                        IFormFile fileArchivoImei = formulario.Files["fileNumeroTelefono"];
                        var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                        System.IO.DirectoryInfo di = new DirectoryInfo(rutaCarpetaTemporal);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        string nombreArchivoImei = Path.GetFileName(formulario.Files["fileNumeroTelefono"].FileName);

                        nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                        var ubicacionArchivoTemporal = "";

                        if (fileArchivoImei.Length > 0)
                        {
                            var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                fileArchivoImei.CopyTo(fileStream);
                            }
                            ubicacionArchivoTemporal = filePath;
                        }
                        FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                        List<String> listaImei = new List<string>();
                        using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                        {
                            try
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets["VALOR"];
                                int totalRows = workSheet.Dimension.Rows;

                                String valor = "";

                                for (int i = 2; i <= totalRows; i++)
                                {
                                    valor = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim() : "";
                                    if (valor != "")
                                    {
                                        listaImei.Add(valor);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "ValidarCriteriosBusqueda");
                                keyValueError = new KeyValuePair<string, string>("0", "No existe la Hoja VALOR dentro de la plantilla");
                            }
                        }

                        if (listaImei == null)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count == 0)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count > 100)
                        {
                            continuarBusqueda = false;
                            keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 100 Valores");
                        }
                        else
                        {
                            continuarBusqueda = true;
                        }

                        if (continuarBusqueda)
                        {
                            foreach (var item in listaImei)
                            {
                                cadenaFinal += item + ",";
                            }
                            if (cadenaFinal.Length < 16000)
                            {
                                cadenaFinal = cadenaFinal.Substring(0, cadenaFinal.Length - 1);
                                HttpContext.Session.SetString("Cadenavalores", cadenaFinal);
                                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                            }
                            else
                            {
                                keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 16000 caracteres");
                            }
                        }



                        #endregion
                    }
                }
                else
                {

                    HttpContext.Session.SetString("Cadenavalores", valorIndividual);
                    keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                }

            }
            else if (tipoConsulta == "2")// NUMERO DE IMEI
            {
                string valorIndividual = formulario["txtValorIndividualImei"];
                if (valorIndividual.Length == 0)
                {
                    string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileNumeroImei"].FileName);
                    IFormFile archivo = formulario.Files["fileNumeroImei"];
                    if (archivo.Length / 1024 < TamFileGrande)//2MB
                    {
                        if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                        {
                            continuarBusqueda = true;
                        }
                        else
                        {
                            mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                            keyValueError = new KeyValuePair<string, string>("0", mensaje);
                        }
                    }
                    else
                    {
                        keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                    }

                    if (keyValueError.Key != "")
                    {
                        #region "Copiar a una carpeta temporal para revisar si hay registro que leer"

                        DateTime fechaDos = DateTime.Now;
                        string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                        IFormFile fileArchivoImei = formulario.Files["fileNumeroImei"];
                        var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                        System.IO.DirectoryInfo di = new DirectoryInfo(rutaCarpetaTemporal);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        string nombreArchivoImei = Path.GetFileName(formulario.Files["fileNumeroImei"].FileName);

                        nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                        var ubicacionArchivoTemporal = "";

                        if (fileArchivoImei.Length > 0)
                        {
                            var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                fileArchivoImei.CopyTo(fileStream);
                            }
                            ubicacionArchivoTemporal = filePath;
                        }
                        FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                        List<String> listaImei = new List<string>();
                        using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                        {
                            try
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets["VALOR"];
                                int totalRows = workSheet.Dimension.Rows;
                                String valor = "";
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    valor = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim() : "";
                                    if (valor != "")
                                    {
                                        listaImei.Add(valor.Substring(0, 14));
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "ValidarCriteriosBusqueda");
                                keyValueError = new KeyValuePair<string, string>("0", "No existe la Hoja VALOR dentro de la plantilla");
                            }
                        }

                        if (listaImei == null)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count == 0)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count > 100)
                        {
                            continuarBusqueda = false;
                            keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 100 Valores");
                        }
                        else
                        {
                            continuarBusqueda = true;
                        }
                        if (continuarBusqueda)
                        {
                            foreach (var item in listaImei)
                            {
                                cadenaFinal += item + ",";
                            }
                            if (cadenaFinal.Length < 16000)
                            {
                                cadenaFinal = cadenaFinal.Substring(0, cadenaFinal.Length - 1);
                                HttpContext.Session.SetString("Cadenavalores", cadenaFinal);
                                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                            }
                            else
                            {
                                keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 16000 caracteres ");
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    HttpContext.Session.SetString("Cadenavalores", valorIndividual.Substring(0, 14));
                    keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                }

            }
            else if (tipoConsulta == "3")// NUMERO DE DOCUMENTO
            {
                string valorIndividual = formulario["txtValorIndividualDoc"];
                if (valorIndividual.Length == 0)
                {
                    string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileNroDocumento"].FileName);
                    IFormFile archivo = formulario.Files["fileNroDocumento"];
                    if (archivo.Length / 1024 < TamFileGrande)//2MB
                    {
                        if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                        {
                            continuarBusqueda = true;
                        }
                        else
                        {
                            mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                            keyValueError = new KeyValuePair<string, string>("0", mensaje);
                        }
                    }
                    else
                    {
                        keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                    }

                    if (keyValueError.Key != "")
                    {
                        #region "Copiar a una carpeta temporal para revisar si hay registro que leer"

                        DateTime fechaDos = DateTime.Now;
                        string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                        IFormFile fileArchivoImei = formulario.Files["fileNroDocumento"];
                        var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                        System.IO.DirectoryInfo di = new DirectoryInfo(rutaCarpetaTemporal);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        string nombreArchivoImei = Path.GetFileName(formulario.Files["fileNroDocumento"].FileName);

                        nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                        var ubicacionArchivoTemporal = "";

                        if (fileArchivoImei.Length > 0)
                        {
                            var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                fileArchivoImei.CopyTo(fileStream);
                            }
                            ubicacionArchivoTemporal = filePath;
                        }
                        FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                        List<String> listaImei = new List<string>();
                        using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                        {
                            try
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets["VALOR"];
                                int totalRows = workSheet.Dimension.Rows;
                                String valor = "";
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    valor = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim() : "";
                                    if (valor != "")
                                    {
                                        listaImei.Add(valor);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "ValidarCriteriosBusqueda");
                                keyValueError = new KeyValuePair<string, string>("0", "No existe la Hoja VALOR dentro de la plantilla");
                            }
                        }
                        if (listaImei == null)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count == 0)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count > 100)
                        {
                            continuarBusqueda = false;
                            keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 100 Valores");
                        }
                        else
                        {
                            continuarBusqueda = true;
                        }
                        if (continuarBusqueda)
                        {
                            foreach (var item in listaImei)
                            {
                                cadenaFinal += item + ",";
                            }
                            if (cadenaFinal.Length < 16000)
                            {
                                cadenaFinal = cadenaFinal.Substring(0, cadenaFinal.Length - 1);
                                HttpContext.Session.SetString("Cadenavalores", cadenaFinal);
                                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                            }
                            else
                            {
                                keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 16000 caracteres");
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    HttpContext.Session.SetString("Cadenavalores", valorIndividual);
                    keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                }
            }
            else if (tipoConsulta == "4")
            {
                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
            }
            return Json(keyValueError);
        }

        [HttpPost]
        public ActionResult BuscarInformacionAbonados(DataTableParamModel request, BE_ABONADO models, IFormCollection formulario)
        {

            var paginacion = new BE_PAGINACION
            {
                Tamanio_Pagina = request.length,
                Numero_Pagina = (request.start / request.length) + 1
            };
            List<BE_ABONADO> ListAbonados = new List<BE_ABONADO>();

            if (models.opcion == 1)
            {
                ListAbonados = new List<BE_ABONADO>();
            }
            else
            {
                if (models.tipoConsulta == 1 || models.tipoConsulta == 2 || models.tipoConsulta == 3)
                {
                    String valores = HttpContext.Session.GetString("Cadenavalores");

                    Int32 indiceInicio = 0;
                    Int32 indiceFinal = valores.Length;
                    Int32 CantidadTomar = indiceFinal / 4;

                    String CadenaUno = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = CantidadTomar;
                    String CadenaDos = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    String CadenaTres = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    CantidadTomar = indiceFinal - indiceInicio;
                    String CadenaCuatro = valores.Substring(indiceInicio, CantidadTomar);
                    models.valoresUno = CadenaUno;
                    models.valoresDos = CadenaDos;
                    models.valoresTres = CadenaTres;
                    models.valoresCuatro = CadenaCuatro;
                    models.valores = valores;

                    ListAbonados = new BL_ABONADO().BuscarAbonados(ref paginacion, models);
                }
                else if (models.tipoConsulta == 4)
                {
                    models.fechaActivacionInicio = models.fechaActivacionInicio == null ? "" : models.fechaActivacionInicio;
                    models.fechaActivacionFin = models.fechaActivacionFin == null ? "" : models.fechaActivacionFin;

                    if (models.fechaActivacionInicio.Length > 0)
                    {
                        String[] fechaVinculacion = models.fechaActivacionInicio.Split("/");
                        String fechaBD = fechaVinculacion[2] + "-" + fechaVinculacion[1] + "-" + fechaVinculacion[0] + " 00:00:00";
                        models.fechaActivacionInicio = fechaBD;
                    }
                    if (models.fechaActivacionFin.Length > 0)
                    {
                        String[] fechaVinculacion = models.fechaActivacionFin.Split("/");
                        String fechaBD = fechaVinculacion[2] + "-" + fechaVinculacion[1] + "-" + fechaVinculacion[0] + " 23:59:59";
                        models.fechaActivacionFin = fechaBD;
                    }
                    ListAbonados = new BL_ABONADO().BuscarAbonados(ref paginacion, models);
                }
                else
                {
                    ListAbonados = new List<BE_ABONADO>();
                }


            }
            var data = ListAbonados.Select(x => new[]{

                                                    x.nroItem.ToString()
                                                  , x.nroServicioMovil
                                                  , x.nombreConcesionaria
                                                  , x.tipoAbonado
                                                  , x.tipoDocLegal
                                                  , x.nroDocLegal
                                                  , x.nombresAbonado
                                                  , x.ApPaternoAbonado
                                                  , x.ApMaternoAbonado
                                                  , x.razonSocial
                                                  , x.tipoDocRepLegal
                                                  , x.nroDocRepLegal
                                                  , x.nombreRepLegal
                                                  , x.ApPatenoRepLegal
                                                  , x.ApMatenoRepLegal
                                                  , x.fechaActivacion
                                                  , x.estadoServicio
                                                  , x.vinculacion
                                                  , x.fechaVinculacion
                                                  , x.nroImei
                                                  , x.marcaEquipo
                                                  , x.modeloEquipo
                                                  , x.marcaGsma
                                                  , x.modeloGsma
                                                  , x.fechaRegistro

                                  }).ToArray();

            return Json(new

            {

                sEcho = string.Empty,
                aaData = data,
                iTotalRecords = paginacion.Total_Archivos,
                iTotalDisplayRecords = paginacion.Total_Archivos
            });

        }


        [HttpPost]
        public ActionResult ValidarDescargaExportarAbonados(IFormCollection formulario, DataTableParamModel request, BE_ABONADO objAbonado)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                DataTable obResultado = new DataTable();
                BL_ABONADO objLogica = new BL_ABONADO();

                // obResultado = objLogica.ExportarAbonados_DT(objAbonado);

                byte[] FileExcel = null;
                ExportExcel objExportExcel = new ExportExcel();
                objAbonado.idPerfil = Int32.Parse(HttpContext.Session.GetString("PerfilUsuario"));
                if (objAbonado.tipoConsulta == 1 || objAbonado.tipoConsulta == 2 || objAbonado.tipoConsulta == 3)
                {
                    String valores = HttpContext.Session.GetString("Cadenavalores");

                    Int32 indiceInicio = 0;
                    Int32 indiceFinal = valores.Length;
                    Int32 CantidadTomar = indiceFinal / 4;

                    String CadenaUno = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = CantidadTomar;
                    String CadenaDos = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    String CadenaTres = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    CantidadTomar = indiceFinal - indiceInicio;
                    String CadenaCuatro = valores.Substring(indiceInicio, CantidadTomar);
                    objAbonado.valoresUno = CadenaUno;
                    objAbonado.valoresDos = CadenaDos;
                    objAbonado.valoresTres = CadenaTres;
                    objAbonado.valoresCuatro = CadenaCuatro;
                    objAbonado.valores = valores;

                    //ListAbonados = new BL_ABONADO().BuscarAbonados(ref paginacion, objAbonado);
                    obResultado = objLogica.ExportarAbonados_DT(objAbonado);
                }
                else if (objAbonado.tipoConsulta == 4)
                {
                    objAbonado.fechaActivacionInicio = objAbonado.fechaActivacionInicio == null ? "" : objAbonado.fechaActivacionInicio;
                    objAbonado.fechaActivacionFin = objAbonado.fechaActivacionFin == null ? "" : objAbonado.fechaActivacionFin;

                    if (objAbonado.fechaActivacionInicio.Length > 0)
                    {
                        String[] fechaVinculacion = objAbonado.fechaActivacionInicio.Split("/");
                        String fechaBD = fechaVinculacion[2] + "-" + fechaVinculacion[1] + "-" + fechaVinculacion[0] + " 00:00:00";
                        objAbonado.fechaActivacionInicio = fechaBD;
                    }
                    if (objAbonado.fechaActivacionFin.Length > 0)
                    {
                        String[] fechaVinculacion = objAbonado.fechaActivacionFin.Split("/");
                        String fechaBD = fechaVinculacion[2] + "-" + fechaVinculacion[1] + "-" + fechaVinculacion[0] + " 23:59:59";
                        objAbonado.fechaActivacionFin = fechaBD;
                    }
                    obResultado = objLogica.ExportarAbonados_DT(objAbonado);
                }

                if (objAbonado.idPerfil == 5 || objAbonado.idPerfil == 4)
                {
                    FileExcel = objExportExcel.exportarConsultaAbonadosPerfilGestion(obResultado, "RegistroAbonado");
                }
                else
                {
                    FileExcel = objExportExcel.exportarConsultaAbonados(obResultado, "RegistroAbonado");
                }
                //    FileExcel = objExportExcel.exportarConsultaAbonados(obResultado, "Reporte");

                var rutaTemporal = configuration["CarpetaTemporal"];

                String archivoExportar = Path.Combine(rutaTemporal, "RegistroAbonado.xlsx");
                if (System.IO.File.Exists(archivoExportar))
                {
                    System.IO.File.Delete(archivoExportar);
                }

                System.IO.File.WriteAllBytes(archivoExportar, FileExcel);

                result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaExportarAbonados");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los datos de la consulta gsma");
            }
            return Json(result);
        }

        public FileResult DescargarInformacionAbonado()
        {
            var rutaTemporal = configuration["CarpetaTemporal"];
            var archivoImei = Path.Combine(rutaTemporal, "RegistroAbonado.xlsx");


            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoImei);
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");
            String nombreImei = fecha + "_RegistroAbonado.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreImei);
        }
        #endregion

        #region "Lista Negra"

        public ActionResult ValidarCriteriosBusquedaListaNegra(IFormCollection formulario)
        {
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();


            String tipoConsulta = "";
            String cadenaFinal = "";
            tipoConsulta = formulario["tipoBuscador"];
            String mensaje = "";
            Boolean continuarBusqueda = false;
            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            if (tipoConsulta == "1")
            {
                string valorIndividual = formulario["txtValorIndividualImei"];
                if (valorIndividual.Length == 0)
                {
                    string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileNumeroImei"].FileName);
                    IFormFile archivo = formulario.Files["fileNumeroImei"];
                    if (archivo.Length / 1024 < TamFileGrande)//2MB
                    {
                        if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                        {
                            continuarBusqueda = true;
                        }
                        else
                        {
                            mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                            keyValueError = new KeyValuePair<string, string>("0", mensaje);
                        }
                    }
                    else
                    {
                        keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                    }

                    if (keyValueError.Key != "")
                    {
                        #region "Copiar a una carpeta temporal para revisar si hay registro que leer"

                        DateTime fechaDos = DateTime.Now;
                        string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                        IFormFile fileArchivoImei = formulario.Files["fileNumeroImei"];
                        var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                        System.IO.DirectoryInfo di = new DirectoryInfo(rutaCarpetaTemporal);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        string nombreArchivoImei = Path.GetFileName(formulario.Files["fileNumeroImei"].FileName);

                        nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                        var ubicacionArchivoTemporal = "";

                        if (fileArchivoImei.Length > 0)
                        {
                            var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                fileArchivoImei.CopyTo(fileStream);
                            }
                            ubicacionArchivoTemporal = filePath;
                        }
                        FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                        List<String> listaImei = new List<string>();
                        using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                        {
                            try
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets["VALOR"];
                                int totalRows = workSheet.Dimension.Rows;
                                String valor = "";
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    valor = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim() : "";
                                    if (valor != "")
                                    {
                                        listaImei.Add(valor.Substring(0, 14));
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "ValidarCriteriosBusqueda");
                                keyValueError = new KeyValuePair<string, string>("0", "No existe la Hoja VALOR dentro de la plantilla");
                            }
                        }

                        if (listaImei == null)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count == 0)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count > 100)
                        {
                            continuarBusqueda = false;
                            keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 100 Valores");
                        }
                        else
                        {
                            continuarBusqueda = true;
                        }
                        if (continuarBusqueda)
                        {
                            foreach (var item in listaImei)
                            {
                                cadenaFinal += item + ",";
                            }
                            if (cadenaFinal.Length < 16000)
                            {
                                cadenaFinal = cadenaFinal.Substring(0, cadenaFinal.Length - 1);
                                HttpContext.Session.SetString("Cadenavalores", cadenaFinal);
                                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                            }
                            else
                            {
                                keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 16000 caracteres");
                            }
                        }
                        #endregion
                    }

                }
                else
                {
                    HttpContext.Session.SetString("Cadenavalores", valorIndividual.Substring(0, 14));
                    keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                }
            }
            else
            {
                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
            }

            return Json(keyValueError);
        }

        [HttpPost]
        public ActionResult BuscarInformacionListaNegra(DataTableParamModel request, BE_ABONADO models)
        {
            // var columna = request.columns.FirstOrDefault(x => x.data == request.order[0].column.ToString());

            var paginacion = new BE_PAGINACION
            {
                //    Sort_Column = columna.name,
                //  Sort_Order = request.order[0].dir.ToString(),
                Tamanio_Pagina = request.length,
                Numero_Pagina = (request.start / request.length) + 1
            };

            List<BE_ABONADO> ListAbonados = new List<BE_ABONADO>();
            if (models.opcion == 1)
            {
                ListAbonados = new List<BE_ABONADO>();
            }
            else
            {
                if (models.tipoConsulta == 1)
                {
                    String valores = HttpContext.Session.GetString("Cadenavalores");

                    Int32 indiceInicio = 0;
                    Int32 indiceFinal = valores.Length;
                    Int32 CantidadTomar = indiceFinal / 4;

                    String CadenaUno = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = CantidadTomar;
                    String CadenaDos = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    String CadenaTres = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    CantidadTomar = indiceFinal - indiceInicio;
                    String CadenaCuatro = valores.Substring(indiceInicio, CantidadTomar);
                    models.valoresUno = CadenaUno;
                    models.valoresDos = CadenaDos;
                    models.valoresTres = CadenaTres;
                    models.valoresCuatro = CadenaCuatro;
                    models.valores = valores;

                    ListAbonados = new BL_ABONADO().BuscarListaNegra(ref paginacion, models);
                }
                else if (models.tipoConsulta == 2)
                {
                    models.fechaReportaInicio = models.fechaReportaInicio == null ? "" : models.fechaReportaInicio;
                    models.fechaReportaFin = models.fechaReportaFin == null ? "" : models.fechaReportaFin;

                    if (models.fechaReportaInicio.Length > 0)
                    {
                        String[] arrayFecha = models.fechaReportaInicio.Split("/");
                        String fechaBD = arrayFecha[2] + "-" + arrayFecha[1] + "-" + arrayFecha[0] + " 00:00:00";
                        models.fechaReportaInicio = fechaBD;
                    }
                    if (models.fechaReportaFin.Length > 0)
                    {
                        String[] arrayFecha = models.fechaReportaFin.Split("/");
                        String fechaBD = arrayFecha[2] + "-" + arrayFecha[1] + "-" + arrayFecha[0] + " 23:59:59";
                        models.fechaReportaFin = fechaBD;
                    }
                    ListAbonados = new BL_ABONADO().BuscarListaNegra(ref paginacion, models);
                }
                else if (models.tipoConsulta == 3)
                {
                    models.fechaBloqueoInicio = models.fechaBloqueoInicio == null ? "" : models.fechaBloqueoInicio;
                    models.fechaBloqueoFin = models.fechaBloqueoFin == null ? "" : models.fechaBloqueoFin;

                    if (models.fechaBloqueoInicio.Length > 0)
                    {
                        String[] arrayFecha = models.fechaBloqueoInicio.Split("/");
                        String fechaBD = arrayFecha[2] + "-" + arrayFecha[1] + "-" + arrayFecha[0] + " 00:00:00";
                        models.fechaBloqueoInicio = fechaBD;
                    }
                    if (models.fechaBloqueoFin.Length > 0)
                    {
                        String[] arrayFecha = models.fechaBloqueoFin.Split("/");
                        String fechaBD = arrayFecha[2] + "-" + arrayFecha[1] + "-" + arrayFecha[0] + " 23:59:59";
                        models.fechaBloqueoFin = fechaBD;
                    }
                    ListAbonados = new BL_ABONADO().BuscarListaNegra(ref paginacion, models);

                }
                else
                {
                    ListAbonados = new List<BE_ABONADO>();
                }

                // ListAbonados = new BL_ABONADO().BuscarListaNegra(ref paginacion, models);
            }
            var data = ListAbonados.Select(x => new[]{
                                                  x.nroItem.ToString()
                                                , x.nombreConcesionaria
                                                , x.nroServicioMovil
                                                , x.nroImsi
                                                , x.nroImei
                                                , x.marcaEquipo
                                                , x.modeloEquipo
                                                , x.marcaGsma
                                                , x.modeloGsma
                                                , x.nroTelefonoReporta
                                                , x.fuenteReporte
                                                ,x.motivoReporte
                                                ,x.codigoReporte
                                                ,x.fechaReporte
                                                ,x.fechaBloqueoDesbloqueo
                                                , x.nombresAbonado
                                                , x.ApPaternoAbonado
                                                , x.ApMaternoAbonado
                                                , x.razonSocial
                                                , x.tipoDocLegal
                                                , x.nroDocLegal
                                                , x.descEstado
                                                , x.fechaRegistro

                                }).ToArray();

            return Json(new
            {

                sEcho = string.Empty,
                aaData = data,
                iTotalRecords = paginacion.Total_Archivos,
                iTotalDisplayRecords = paginacion.Total_Archivos

            });

        }



        [HttpPost]
        public ActionResult ValidarDescargaExportarListaNegra(IFormCollection formulario, BE_ABONADO objAbonado)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                DataTable obResultado = new DataTable();
                BL_ABONADO objLogica = new BL_ABONADO();

                byte[] FileExcel = null;
                ExportExcel objExportExcel = new ExportExcel();
                objAbonado.idPerfil = Int32.Parse(HttpContext.Session.GetString("PerfilUsuario"));


                if (objAbonado.tipoConsulta == 1)
                {
                    String valores = HttpContext.Session.GetString("Cadenavalores");
                    Int32 indiceInicio = 0;
                    Int32 indiceFinal = valores.Length;
                    Int32 CantidadTomar = indiceFinal / 4;
                    String CadenaUno = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = CantidadTomar;
                    String CadenaDos = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    String CadenaTres = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    CantidadTomar = indiceFinal - indiceInicio;
                    String CadenaCuatro = valores.Substring(indiceInicio, CantidadTomar);
                    objAbonado.valoresUno = CadenaUno;
                    objAbonado.valoresDos = CadenaDos;
                    objAbonado.valoresTres = CadenaTres;
                    objAbonado.valoresCuatro = CadenaCuatro;
                    objAbonado.valores = valores;

                    obResultado = objLogica.ExportarListaNegra_DT(objAbonado);
                }
                else if (objAbonado.tipoConsulta == 2)
                {
                    objAbonado.fechaReportaInicio = objAbonado.fechaReportaInicio == null ? "" : objAbonado.fechaReportaInicio;
                    objAbonado.fechaReportaFin = objAbonado.fechaReportaFin == null ? "" : objAbonado.fechaReportaFin;

                    if (objAbonado.fechaReportaInicio.Length > 0)
                    {
                        String[] arrayFecha = objAbonado.fechaReportaInicio.Split("/");
                        String fechaBD = arrayFecha[2] + "-" + arrayFecha[1] + "-" + arrayFecha[0] + " 00:00:00";
                        objAbonado.fechaReportaInicio = fechaBD;
                    }
                    if (objAbonado.fechaReportaFin.Length > 0)
                    {
                        String[] arrayFecha = objAbonado.fechaReportaFin.Split("/");
                        String fechaBD = arrayFecha[2] + "-" + arrayFecha[1] + "-" + arrayFecha[0] + " 23:59:59";
                        objAbonado.fechaReportaFin = fechaBD;
                    }
                    obResultado = objLogica.ExportarListaNegra_DT(objAbonado);
                }
                else if (objAbonado.tipoConsulta == 3)
                {
                    objAbonado.fechaBloqueoInicio = objAbonado.fechaBloqueoInicio == null ? "" : objAbonado.fechaBloqueoInicio;
                    objAbonado.fechaBloqueoFin = objAbonado.fechaBloqueoFin == null ? "" : objAbonado.fechaBloqueoFin;

                    if (objAbonado.fechaBloqueoInicio.Length > 0)
                    {
                        String[] arrayFecha = objAbonado.fechaBloqueoInicio.Split("/");
                        String fechaBD = arrayFecha[2] + "-" + arrayFecha[1] + "-" + arrayFecha[0] + " 00:00:00";
                        objAbonado.fechaBloqueoInicio = fechaBD;
                    }
                    if (objAbonado.fechaBloqueoFin.Length > 0)
                    {
                        String[] arrayFecha = objAbonado.fechaBloqueoFin.Split("/");
                        String fechaBD = arrayFecha[2] + "-" + arrayFecha[1] + "-" + arrayFecha[0] + " 23:59:59";
                        objAbonado.fechaBloqueoFin = fechaBD;
                    }
                    obResultado = objLogica.ExportarListaNegra_DT(objAbonado);
                }
                //byte[] FileExcel = null;
                //ExportExcel objExportExcel = new ExportExcel();
                if (objAbonado.idPerfil == 5 || objAbonado.idPerfil == 4)
                {
                    FileExcel = objExportExcel.exportarListaNegraPerfilGestion(obResultado, "ReporteListaNegra");
                }
                else
                {
                    FileExcel = objExportExcel.exportarListaNegra(obResultado, "ReporteListaNegra");
                }


                var rutaTemporal = configuration["CarpetaTemporal"];

                String archivoExportar = Path.Combine(rutaTemporal, "ReporteListaNegra.xlsx");
                if (System.IO.File.Exists(archivoExportar))
                {
                    System.IO.File.Delete(archivoExportar);
                }

                System.IO.File.WriteAllBytes(archivoExportar, FileExcel);

                result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaExportarAbonados");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los datos de la consulta gsma");
            }
            return Json(result);
        }

        public FileResult DescargarInformacionListaNegra()
        {
            var rutaTemporal = configuration["CarpetaTemporal"];
            var archivoImei = Path.Combine(rutaTemporal, "ReporteListaNegra.xlsx");


            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoImei);
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");
            String nombreImei = fecha + "_ReporteListaNegra.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreImei);
        }
        #endregion



        #region "Lista Vinculacion"

        [HttpPost]
        public ActionResult ValidarBusquedaLV(IFormCollection formulario)
        {
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();


            String tipoConsulta = "";
            String cadenaFinal = "";
            tipoConsulta = formulario["tipoBuscador"];
            String mensaje = "";
            Boolean continuarBusqueda = false;
            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            if (tipoConsulta == "1")
            {
                string valorIndividual = formulario["txtValorIndividualTelefono"];
                if (valorIndividual.Length == 0)
                {
                    string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileNumeroTelefono"].FileName);
                    IFormFile archivo = formulario.Files["fileNumeroTelefono"];
                    if (archivo.Length / 1024 < TamFileGrande)//2MB
                    {
                        if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                        {
                            continuarBusqueda = true;
                        }
                        else
                        {
                            mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                            keyValueError = new KeyValuePair<string, string>("0", mensaje);
                        }
                    }
                    else
                    {
                        keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);

                    }

                    if (keyValueError.Key != "")
                    {
                        #region "Copiar a una carpeta temporal para revisar si hay registro que leer"


                        DateTime fechaDos = DateTime.Now;
                        string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                        IFormFile fileArchivoImei = formulario.Files["fileNumeroTelefono"];
                        var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                        System.IO.DirectoryInfo di = new DirectoryInfo(rutaCarpetaTemporal);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        string nombreArchivoImei = Path.GetFileName(formulario.Files["fileNumeroTelefono"].FileName);

                        nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                        var ubicacionArchivoTemporal = "";

                        if (fileArchivoImei.Length > 0)
                        {
                            var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                fileArchivoImei.CopyTo(fileStream);
                            }
                            ubicacionArchivoTemporal = filePath;
                        }
                        FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                        List<String> listaImei = new List<string>();
                        using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                        {
                            try
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets["VALOR"];
                                int totalRows = workSheet.Dimension.Rows;

                                String valor = "";

                                for (int i = 2; i <= totalRows; i++)
                                {
                                    valor = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim() : "";
                                    if (valor != "")
                                    {
                                        listaImei.Add(valor);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "ValidarCriteriosBusqueda");
                                keyValueError = new KeyValuePair<string, string>("0", "No existe la Hoja VALOR dentro de la plantilla");
                            }
                        }

                        if (listaImei == null)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count == 0)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count > 100)
                        {
                            continuarBusqueda = false;
                            keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 100 Valores");
                        }
                        else
                        {
                            continuarBusqueda = true;
                        }

                        if (continuarBusqueda)
                        {
                            foreach (var item in listaImei)
                            {
                                cadenaFinal += item + ",";
                            }
                            if (cadenaFinal.Length < 16000)
                            {
                                cadenaFinal = cadenaFinal.Substring(0, cadenaFinal.Length - 1);
                                HttpContext.Session.SetString("Cadenavalores", cadenaFinal);
                                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                            }
                            else
                            {
                                keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido -  16000 caracteres");
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    HttpContext.Session.SetString("Cadenavalores", valorIndividual);
                    keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                }
            }
            else if (tipoConsulta == "2")
            {
                string valorIndividual = formulario["txtValorIndividualImei"];
                if (valorIndividual.Length == 0)
                {
                    string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileNumeroImei"].FileName);
                    IFormFile archivo = formulario.Files["fileNumeroImei"];
                    if (archivo.Length / 1024 < TamFileGrande)//2MB
                    {
                        if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                        {
                            continuarBusqueda = true;
                        }
                        else
                        {
                            mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                            keyValueError = new KeyValuePair<string, string>("0", mensaje);
                        }
                    }
                    else
                    {
                        keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                    }

                    if (keyValueError.Key != "")
                    {
                        #region "Copiar a una carpeta temporal para revisar si hay registro que leer"

                        DateTime fechaDos = DateTime.Now;
                        string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                        IFormFile fileArchivoImei = formulario.Files["fileNumeroImei"];
                        var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                        System.IO.DirectoryInfo di = new DirectoryInfo(rutaCarpetaTemporal);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        string nombreArchivoImei = Path.GetFileName(formulario.Files["fileNumeroImei"].FileName);

                        nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                        var ubicacionArchivoTemporal = "";

                        if (fileArchivoImei.Length > 0)
                        {
                            var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                fileArchivoImei.CopyTo(fileStream);
                            }
                            ubicacionArchivoTemporal = filePath;
                        }
                        FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                        List<String> listaImei = new List<string>();
                        using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                        {
                            try
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets["VALOR"];
                                int totalRows = workSheet.Dimension.Rows;
                                String valor = "";
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    valor = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim() : "";
                                    if (valor != "")
                                    {
                                        listaImei.Add(valor.Substring(0, 14));
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "ValidarCriteriosBusqueda");
                                keyValueError = new KeyValuePair<string, string>("0", "No existe la Hoja VALOR dentro de la plantilla");
                            }
                        }

                        if (listaImei == null)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count == 0)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count > 100)
                        {
                            continuarBusqueda = false;
                            keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 100 Valores");
                        }
                        else
                        {
                            continuarBusqueda = true;
                        }
                        if (continuarBusqueda)
                        {
                            foreach (var item in listaImei)
                            {
                                cadenaFinal += item + ",";
                            }
                            if (cadenaFinal.Length < 16000)
                            {
                                cadenaFinal = cadenaFinal.Substring(0, cadenaFinal.Length - 1);
                                HttpContext.Session.SetString("Cadenavalores", cadenaFinal);
                                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                            }
                            else
                            {
                                keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 16000 caracteres");
                            }
                        }
                        #endregion
                    }
                }
                else
                {
                    HttpContext.Session.SetString("Cadenavalores", valorIndividual.Substring(0, 14));
                    keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                }

            }
            else
            {
                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
            }
            return Json(keyValueError);
        }


        [HttpPost]
        public ActionResult BuscarInformacionVinculacion(DataTableParamModel request, BE_VINCULACION models)
        {


            var paginacion = new BE_PAGINACION
            {

                Tamanio_Pagina = request.length,
                Numero_Pagina = (request.start / request.length) + 1
            };

            List<BE_VINCULACION> ListVinculacion = new List<BE_VINCULACION>();
            if (models.opcion == 1)
            {
                ListVinculacion = new List<BE_VINCULACION>();
            }
            else
            {
                if (models.tipoConsulta == 1 || models.tipoConsulta == 2)
                {
                    String valores = HttpContext.Session.GetString("Cadenavalores");

                    Int32 indiceInicio = 0;
                    Int32 indiceFinal = valores.Length;
                    Int32 CantidadTomar = indiceFinal / 4;

                    String CadenaUno = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = CantidadTomar;
                    String CadenaDos = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    String CadenaTres = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    CantidadTomar = indiceFinal - indiceInicio;
                    String CadenaCuatro = valores.Substring(indiceInicio, CantidadTomar);
                    models.valoresUno = CadenaUno;
                    models.valoresDos = CadenaDos;
                    models.valoresTres = CadenaTres;
                    models.valoresCuatro = CadenaCuatro;
                    models.valores = valores;

                    ListVinculacion = new BL_VINCULACION().BuscarVinculacion(ref paginacion, models);
                }
                else
                {
                    ListVinculacion = new List<BE_VINCULACION>();
                }
            }
            var data = ListVinculacion.Select(x => new[]{
                                                  x.nroItem.ToString()
                                                , x.nroImei
                                                , x.nroImsi
                                                , x.nroTelefonoMovil
                                                , x.fechaVinculacionVoz
                                                , x.fechaVinculacionDatos
                                                , x.nombreConcesionaria
                                                , x.mes
                                                , x.anio

                                }).ToArray();

            return Json(new
            {
                sEcho = string.Empty,
                aaData = data,
                iTotalRecords = paginacion.Total_Archivos,
                iTotalDisplayRecords = paginacion.Total_Archivos
            });
        }

        [HttpPost]
        public ActionResult ValidarDescargaExportarVinculacion(IFormCollection formulario, BE_VINCULACION objVinculacion)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                DataTable obResultado = new DataTable();
                BL_VINCULACION objLogica = new BL_VINCULACION();

                byte[] FileExcel = null;
                ExportExcel objExportExcel = new ExportExcel();
                if (objVinculacion.tipoConsulta == 1 || objVinculacion.tipoConsulta == 2)
                {
                    String valores = HttpContext.Session.GetString("Cadenavalores");

                    Int32 indiceInicio = 0;
                    Int32 indiceFinal = valores.Length;
                    Int32 CantidadTomar = indiceFinal / 4;

                    String CadenaUno = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = CantidadTomar;
                    String CadenaDos = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    String CadenaTres = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    CantidadTomar = indiceFinal - indiceInicio;
                    String CadenaCuatro = valores.Substring(indiceInicio, CantidadTomar);
                    objVinculacion.valoresUno = CadenaUno;
                    objVinculacion.valoresDos = CadenaDos;
                    objVinculacion.valoresTres = CadenaTres;
                    objVinculacion.valoresCuatro = CadenaCuatro;
                    objVinculacion.valores = valores;
                    obResultado = objLogica.ExportarVinculacion_DT(objVinculacion);
                }
                FileExcel = objExportExcel.exportarListaVinculacion(obResultado, "Lista Vinculación");


                var rutaTemporal = configuration["CarpetaTemporal"];

                String archivoExportar = Path.Combine(rutaTemporal, "ReporteVinculacion.xlsx");
                if (System.IO.File.Exists(archivoExportar))
                {
                    System.IO.File.Delete(archivoExportar);
                }

                System.IO.File.WriteAllBytes(archivoExportar, FileExcel);

                result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaExportarVinculacion");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los datos de la consulta gsma");
            }
            return Json(result);
        }

        public FileResult DescargarInformacionVinculacion()
        {
            var rutaTemporal = configuration["CarpetaTemporal"];
            var archivoImei = Path.Combine(rutaTemporal, "ReporteVinculacion.xlsx");


            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoImei);
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");
            String nombreImei = fecha + "_Reporte.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreImei);
        }

        [HttpPost]
        public ActionResult ValidarBusquedaEIR(IFormCollection formulario)
        {
            KeyValuePair<string, string> keyValueError = new KeyValuePair<string, string>();

            String tipoConsulta = "";
            String cadenaFinal = "";
            tipoConsulta = formulario["tipoBuscador"];
            String mensaje = "";
            Boolean continuarBusqueda = false;
            int TamFileGrande = int.Parse(configuration["TamFileGrande"]);
            string TamFileGrandeText = configuration["TamFileGrandeText"].ToString();
            if (tipoConsulta == "1")
            {
                string valorIndividual = formulario["txtValorIndividualImei"];
                if (valorIndividual.Length == 0)
                {
                    string extensionArchivo = System.IO.Path.GetExtension(formulario.Files["fileNumeroImei"].FileName);
                    IFormFile archivo = formulario.Files["fileNumeroImei"];
                    if (archivo.Length / 1024 < TamFileGrande)//2MB
                    {
                        if (extensionArchivo == ".xls" || extensionArchivo == ".xlsx")
                        {
                            continuarBusqueda = true;
                        }
                        else
                        {
                            mensaje += "El tipo de archivo seleccionado no esta permitido, intentelo nuevamente con archivo de tipo de excel.";
                            keyValueError = new KeyValuePair<string, string>("0", mensaje);
                        }
                    }
                    else
                    {
                        keyValueError = new KeyValuePair<string, string>("0", "*Advertencia: No se permite subir un archivo mayor a " + TamFileGrandeText);
                    }

                    if (keyValueError.Key != "")
                    {
                        #region "Copiar a una carpeta temporal para revisar si hay registro que leer"

                        DateTime fechaDos = DateTime.Now;
                        string tiempoActual = fechaDos.ToString("ddMMyyyyHHmmss");
                        IFormFile fileArchivoImei = formulario.Files["fileNumeroImei"];
                        var rutaCarpetaTemporal = configuration["CarpetaTemporal"];
                        System.IO.DirectoryInfo di = new DirectoryInfo(rutaCarpetaTemporal);
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        string nombreArchivoImei = Path.GetFileName(formulario.Files["fileNumeroImei"].FileName);

                        nombreArchivoImei = tiempoActual + "" + nombreArchivoImei.Replace(" ", "");
                        var ubicacionArchivoTemporal = "";

                        if (fileArchivoImei.Length > 0)
                        {
                            var filePath = Path.Combine(rutaCarpetaTemporal, nombreArchivoImei);
                            using (var fileStream = new FileStream(filePath, FileMode.Create))
                            {
                                fileArchivoImei.CopyTo(fileStream);
                            }
                            ubicacionArchivoTemporal = filePath;
                        }
                        FileInfo fileInfoArchivoImei = new FileInfo(ubicacionArchivoTemporal);
                        List<String> listaImei = new List<string>();
                        using (ExcelPackage package = new ExcelPackage(fileInfoArchivoImei))
                        {
                            try
                            {
                                ExcelWorksheet workSheet = package.Workbook.Worksheets["VALOR"];
                                int totalRows = workSheet.Dimension.Rows;
                                String valor = "";
                                for (int i = 2; i <= totalRows; i++)
                                {
                                    valor = workSheet.Cells[i, 1].Value != null ? workSheet.Cells[i, 1].Value.ToString().Trim() : "";
                                    if (valor != "")
                                    {
                                        listaImei.Add(valor.Substring(0, 14));
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                LogError.RegistrarErrorMetodo(ex, "ValidarCriteriosBusqueda");
                                keyValueError = new KeyValuePair<string, string>("0", "No existe la Hoja VALOR dentro de la plantilla");
                            }
                        }

                        if (listaImei == null)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count == 0)
                        {
                            continuarBusqueda = false;
                        }
                        else if (listaImei.Count > 100)
                        {
                            continuarBusqueda = false;
                            keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 100 Valores");
                        }
                        else
                        {
                            continuarBusqueda = true;
                        }
                        if (continuarBusqueda)
                        {
                            foreach (var item in listaImei)
                            {
                                cadenaFinal += item + ",";
                            }
                            if (cadenaFinal.Length < 16000)
                            {
                                cadenaFinal = cadenaFinal.Substring(0, cadenaFinal.Length - 1);
                                HttpContext.Session.SetString("Cadenavalores", cadenaFinal);
                                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                            }
                            else
                            {
                                keyValueError = new KeyValuePair<string, string>("0", "La cantidad de valores supera lo permitido - 16000 caracteres");
                            }
                        }
                        #endregion
                    }
                }
                else
                {

                    HttpContext.Session.SetString("Cadenavalores", valorIndividual.Substring(0, 14));
                    keyValueError = new KeyValuePair<string, string>("1", "Pasa");
                }
            }
            else
            {
                keyValueError = new KeyValuePair<string, string>("1", "Pasa");
            }
            return Json(keyValueError);
        }

        #endregion

        #region "Lista EIR"
        [HttpPost]
        public ActionResult BuscarInformacionEIR(DataTableParamModel request, BE_EIR models)
        {
            var paginacion = new BE_PAGINACION
            {
                Tamanio_Pagina = request.length,
                Numero_Pagina = (request.start / request.length) + 1
            };

            List<BE_EIR> ListaEIR = new List<BE_EIR>();
            if (models.opcion == 1)
            {
                ListaEIR = new List<BE_EIR>();
            }
            else
            {
                if (models.tipoConsulta == 1)
                {
                    String valores = HttpContext.Session.GetString("Cadenavalores");

                    Int32 indiceInicio = 0;
                    Int32 indiceFinal = valores.Length;
                    Int32 CantidadTomar = indiceFinal / 4;
                    String CadenaUno = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = CantidadTomar;
                    String CadenaDos = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    String CadenaTres = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    CantidadTomar = indiceFinal - indiceInicio;
                    String CadenaCuatro = valores.Substring(indiceInicio, CantidadTomar);
                    models.valoresUno = CadenaUno;
                    models.valoresDos = CadenaDos;
                    models.valoresTres = CadenaTres;
                    models.valoresCuatro = CadenaCuatro;
                    models.valores = valores;

                    ListaEIR = new BL_EIR().BuscarEIR(ref paginacion, models);

                }
                else
                {
                    ListaEIR = new List<BE_EIR>();
                }
            }
            var data = ListaEIR.Select(x => new[]{
                                                  x.nroItem.ToString()
                                                , x.nroImei
                                                , x.nombreConcesionaria
                                                , x.mes
                                                , x.anio
                                                 , x.fechaRegistro

                                }).ToArray();

            return Json(new
            {
                sEcho = string.Empty,
                aaData = data,
                iTotalRecords = paginacion.Total_Archivos,
                iTotalDisplayRecords = paginacion.Total_Archivos
            });
        }

        [HttpPost]
        public ActionResult ValidarDescargaExportarEIR(IFormCollection formulario, BE_EIR objEIR)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                DataTable obResultado = new DataTable();


                if (objEIR.tipoConsulta == 1)
                {
                    String valores = HttpContext.Session.GetString("Cadenavalores");

                    Int32 indiceInicio = 0;
                    Int32 indiceFinal = valores.Length;
                    Int32 CantidadTomar = indiceFinal / 4;
                    String CadenaUno = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = CantidadTomar;
                    String CadenaDos = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    String CadenaTres = valores.Substring(indiceInicio, CantidadTomar);
                    indiceInicio = indiceInicio + CantidadTomar;
                    CantidadTomar = indiceFinal - indiceInicio;
                    String CadenaCuatro = valores.Substring(indiceInicio, CantidadTomar);
                    objEIR.valoresUno = CadenaUno;
                    objEIR.valoresDos = CadenaDos;
                    objEIR.valoresTres = CadenaTres;
                    objEIR.valoresCuatro = CadenaCuatro;
                    objEIR.valores = valores;
                    obResultado = new BL_EIR().ExportarEIR_DT(objEIR);
                }

                byte[] FileExcel = null;
                ExportExcel objExportExcel = new ExportExcel();

                FileExcel = objExportExcel.exportarListaEIR(obResultado, "Lista EIR");

                var rutaTemporal = configuration["CarpetaTemporal"];

                String archivoExportar = Path.Combine(rutaTemporal, "ReporteEIR.xlsx");
                if (System.IO.File.Exists(archivoExportar))
                {
                    System.IO.File.Delete(archivoExportar);
                }
                System.IO.File.WriteAllBytes(archivoExportar, FileExcel);
                result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaExportarEIR");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los datos de la consulta de información");
            }
            return Json(result);
        }

        public FileResult DescargarInformacionEIR()
        {
            var rutaTemporal = configuration["CarpetaTemporal"];
            var archivoImei = Path.Combine(rutaTemporal, "ReporteEIR.xlsx");


            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoImei);
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");
            String nombreImei = fecha + "_ReporteEIR.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreImei);
        }
        #endregion
    }
}