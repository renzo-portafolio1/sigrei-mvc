﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class ReporteController : Controller
    {
        private readonly IConfiguration configuration;
        public ReporteController(IConfiguration config)
        {
            this.configuration = config;
        }

        public IActionResult Index()
        {
            BL_EMPRESA ObjLogica = new BL_EMPRESA();
            List<BE_EMPRESA> lista = ObjLogica.Listar(0);

            ViewBag.Lista_EmpresaEO = lista;

            return View();
        }
        [HttpGet]
        public ActionResult BandejaInicial(IFormCollection formulario)
        {
            List<BE_REPORTE_IMEI> listaResultado = new List<BE_REPORTE_IMEI>();

            return PartialView("_DetalleIndex", listaResultado);
        }


        [HttpPost]
        public ActionResult BuscarReporte(IFormCollection formulario)
        {
            List<BE_REPORTE_IMEI> listaBE_REQUERIMIENTO = new List<BE_REPORTE_IMEI>();
            BE_REPORTE_IMEI objParametro = obtenerCriterios(formulario);
            BL_REPORTE objLogica = new BL_REPORTE();

            listaBE_REQUERIMIENTO = objLogica.ListaReporte(objParametro);

            return PartialView("_DetalleIndex", listaBE_REQUERIMIENTO);
        }

        public BE_REPORTE_IMEI obtenerCriterios(IFormCollection formulario)
        {
            BE_REPORTE_IMEI objResultado = new BE_REPORTE_IMEI();

            objResultado.idEmpresaEO = formulario["ddlEmpresaOperadora"] == "" ? 0 : Convert.ToInt32(formulario["ddlEmpresaOperadora"].ToString());
            objResultado.nroImei = formulario["txtNroImei"].ToString();
            objResultado.fechaFin = formulario["txtFechaFin"].ToString();
            objResultado.fechaInicio = formulario["txtFechaInicio"].ToString();
            objResultado.nombreInstitucion = formulario["txtNombreInstitucion"].ToString();
            return objResultado;
        }


        [HttpPost]
        public ActionResult ValidarDescargarReporte(IFormCollection formulario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {
                List<BE_REPORTE_IMEI> listaBE_Reporte = new List<BE_REPORTE_IMEI>();
                BE_REPORTE_IMEI objParametro = obtenerCriterios(formulario);
                BL_REPORTE objLogica = new BL_REPORTE();

                listaBE_Reporte = objLogica.ListaReporte(objParametro);

                byte[] FileExcel = null;
                ExportExcel objExportExcel = new ExportExcel();
                FileExcel = objExportExcel.generarReporte(listaBE_Reporte, "Reporte");                

             
                var rutaTemporal = configuration["CarpetaTemporal"];

                String archivoImei = Path.Combine(rutaTemporal, "Reporte.xlsx");
                if (System.IO.File.Exists(archivoImei))
                {
                    System.IO.File.Delete(archivoImei);
                }

                System.IO.File.WriteAllBytes(archivoImei, FileExcel);

                result = new KeyValuePair<string, string>("1", "¡Se procede a descargar!");
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargarReporte");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar los archivos de imeis seleccionados");
            }
            return Json(result);
        }


        public FileResult DescargarReporte()
        {
            var rutaTemporal = configuration["CarpetaTemporal"];
            var archivoImei = Path.Combine(rutaTemporal, "Reporte.xlsx");


            byte[] fileBytes = System.IO.File.ReadAllBytes(archivoImei);
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("ddMMyyyy");            
            String nombreImei = fecha + "_Reporte.xlsx";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreImei);
        }

    }
}