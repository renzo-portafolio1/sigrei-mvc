﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using PPBMS.GLOBALTPA.ClienteWeb.Filter;

namespace ClienteSIGREI.Controllers
{
    [FilterUsuario]
    public class PrincipalController : Controller
    {
        private readonly IConfiguration configuration;
        public PrincipalController(IConfiguration config)
        {
            this.configuration = config;
        }

        public IActionResult Index()
        {

            return View();
        }

        [HttpGet]
        public ActionResult ValidarDescargaManual(String ifUsuario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {

                String usuario = ifUsuario;
                if (ifUsuario.Length > 2) { ifUsuario = ifUsuario.Substring(0, 3).ToLower(); }
                var rutaManual = configuration["PlantillasSIGREI"];

                if (ifUsuario == "ep_")
                {
                    rutaManual = Path.Combine(rutaManual, "ManualEP.pdf");
                    if (System.IO.File.Exists(rutaManual))
                    {
                        result = new KeyValuePair<string, string>("1", "Se procede con la descarga");
                        HttpContext.Session.SetString("NombreManual", "ManualEP.pdf");
                    }
                    else { result = new KeyValuePair<string, string>("0", "No se encuentra el manual"); }

                }
                else if (ifUsuario == "eo_")
                {
                    rutaManual = Path.Combine(rutaManual, "ManualEO.pdf");
                    if (System.IO.File.Exists(rutaManual))
                    {
                        result = new KeyValuePair<string, string>("1", "Se procede con la descarga");
                        HttpContext.Session.SetString("NombreManual", "ManualEO.pdf");
                    }
                    else { result = new KeyValuePair<string, string>("0", "No se encuentra el manual"); }
                }
                else
                {
                    rutaManual = Path.Combine(rutaManual, "ManualOsiptel.pdf");
                    if (System.IO.File.Exists(rutaManual))
                    {
                        result = new KeyValuePair<string, string>("1", "Se procede con la descarga");
                        HttpContext.Session.SetString("NombreManual", "ManualOsiptel.pdf");
                    }
                    else { result = new KeyValuePair<string, string>("0", "No se encuentra el manual"); }
                }

            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaManual");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar el manual requerido");
            }
            return Json(result);
        }

        public FileResult DescargarManual()
        {
            var rutaManual = configuration["PlantillasSIGREI"];
            String nombreManual = HttpContext.Session.GetString("NombreManual");
            rutaManual = Path.Combine(rutaManual, nombreManual);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = "Manual.pdf";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpGet]
        public ActionResult ValidarDescargaPlantillaBusqueda(String ifUsuario)
        {
            KeyValuePair<string, string> result = new KeyValuePair<string, string>();
            try
            {

                var rutaPlantilla = configuration["PlantillasSIGREI"];

                rutaPlantilla = Path.Combine(rutaPlantilla, "PlantillaBusqueda.xlsx");
                if (System.IO.File.Exists(rutaPlantilla))
                {
                    result = new KeyValuePair<string, string>("1", "Se procede con la descarga");
                    HttpContext.Session.SetString("NombrePlantilla", "PlantillaBusqueda.xlsx");
                }
                else { result = new KeyValuePair<string, string>("0", "No se encuentra la plantilla"); }
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidarDescargaPlantillaBusqueda");
                result = new KeyValuePair<string, string>("0", "Error al intentar descargar la plantilla requerido");
            }
            return Json(result);
        }

        public FileResult DescargarPlantillaBusqueda()
        {
            var rutaManual = configuration["PlantillasSIGREI"];
            String nombrePlantilla = HttpContext.Session.GetString("NombrePlantilla");
            rutaManual = Path.Combine(rutaManual, nombrePlantilla);

            byte[] fileBytes = System.IO.File.ReadAllBytes(rutaManual);
            string fileName = nombrePlantilla;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        private String createMenu()
        {
            UsuarioBL BL_Acceso_opcion = new UsuarioBL();
            var idPerfil = HttpContext.Session.GetString("PerfilUsuario").ToString();
           
            List<BeAccesoOpcion> lista = BL_Acceso_opcion.obtenerListaOpcionPerfil(idPerfil);

            StringBuilder menu = new StringBuilder();

            menu.Append("<ul class='nav navbar-nav' id='side-menu'>");


            Int32 cantidadRegistro = 0;
            cantidadRegistro = lista.Count;

            string url = Request.Host.ToString();
            string app_path = Request.PathBase;

            String hostAplicacion = Request.Scheme + "://" + url + "" + app_path + "/";
            foreach (BeAccesoOpcion itemPadre in lista)
            {
                if (itemPadre.IDPADRE == "0")
                {
                    menu.Append(" <li class='menu-titulo'> <a href='#' urlOpcion='' class='dropdown-toggle' data-toggle='dropdown'>" + itemPadre.DESCRIPCION + " <span class='fa arrow'></span></a>");
                    menu.Append(" <ul class='dropdown-menu'> ");
                    foreach (BeAccesoOpcion itemOpcion in lista)
                    {
                        if (itemOpcion.IDPADRE == itemPadre.IDOPCION)
                        {
                            menu.Append("<li> <a href='" + hostAplicacion + itemOpcion.RUTA + "' class='Menulink'>" + itemOpcion.DESCRIPCION + " </a> </li>");
                        }
                    }
                    menu.Append("</ul> ");
                    menu.Append("</li> ");
                }
            }

            menu.Append("</ul>");

            return menu.ToString();
        }
        /*
         * private String createMenu()
        {
            UsuarioBL BL_Acceso_opcion = new UsuarioBL();
            //var idPerfil = HttpContext.Session.GetString("PerfilUsuario").ToString();
            //List<BE_ACCESO_OPCION> lista = BL_Acceso_opcion.obtenerListaOpcionPerfil(idPerfil);

            StringBuilder menu = new StringBuilder();
            string url = Request.Host.ToString();
            string app_path = Request.PathBase;

            String hostAplicacion = Request.Scheme + "://" + url + "" + app_path + "/";
            menu.Append("<ul class='nav navbar-nav' id='side-menu'>");
            menu.Append(" <li class='menu-titulo'> <a href='#' urlOpcion='' class='dropdown-toggle' data-toggle='dropdown'>Consulta de Información <span class='fa arrow'></span></a>");
            menu.Append(" <ul class='dropdown-menu'> ");
            menu.Append("<li> <a href='" + hostAplicacion + "Consulta/ConsultaAbonado" + "' class='Menulink'>Consulta de Registro de Abonados </a> </li>");
            menu.Append("<li> <a href='" + hostAplicacion + "Consulta/ConsultaListaNegra" + "' class='Menulink'>Consulta de Lista Negra </a> </li>");
            menu.Append("<li> <a href='" + hostAplicacion + "Consulta/ConsultaGSMA" + "' class='Menulink'>Consulta GSMA  </a> </li>");
            menu.Append("<li> <a href='" + hostAplicacion + "Consulta/ConsultaVinculacion" + "' class='Menulink'>Lista de Vinculación  </a> </li>");
            menu.Append("<li> <a href='" + hostAplicacion + "Consulta/ConsultaEIR" + "' class='Menulink'>Consulta EIR </a> </li>");
            menu.Append("</ul> ");
            menu.Append("</li> ");
            //Int32 cantidadRegistro = 0;
            //cantidadRegistro = lista.Count;
            menu.Append("</ul>");

            //foreach (BE_ACCESO_OPCION itemPadre in lista)
            //{
            //    if (itemPadre.IDPADRE == "0")
            //    {
                  
            //        foreach (BE_ACCESO_OPCION itemOpcion in lista)
            //        {
            //            if (itemOpcion.IDPADRE == itemPadre.IDOPCION)
            //            {
                           
            //            }
            //        }
                  
            //    }
            //}

        

            return menu.ToString();
        }
        */
        [HttpGet]
        public ActionResult CrearMenu()
        {
            try
            {
                String menu = createMenu();
                return Json(menu);
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

    }
}