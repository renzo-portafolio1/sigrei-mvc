﻿using ClienteSIGREI.Util;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NETCore.Encrypt;
using Newtonsoft.Json;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;
using reCAPTCHA.AspNetCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.Net;
using System.Net.Http;

namespace ClienteSIGREI.Controllers
{

    public class LoginController : Controller
    {
        // GET: Login
        private readonly IConfiguration configuration;
        private string valor_token;

        Microsoft.Extensions.Configuration.IConfiguration _config;

        public LoginController(IConfiguration config)
        {
            this.configuration = config;
        }

        protected static string ReCaptcha_Key = "6LfJPwATAAAAAEneZUzSrBmBStil1YRMsRL3Akum";
        protected static string ReCaptcha_Secret = "6LfJPwATAAAAAFmb19YL_SQxvyhGTAgYD9c-p7N5";

        public ActionResult Index()
        {
            string cadena = HttpContext.Session.GetString("NumDocIdeEP");
            if (HttpContext.Session.GetString("NumDocIdeEP") != null &&  HttpContext.Session.GetString("NumDocIdeEP") != "0")
            {
                HttpContext.Session.SetString("NumDocIdeEP", "0");
                HttpContext.Session.SetString("CodDocIdeEP", "0");

                //HttpContext.Session = null;
               //string cadena2 = HttpContext.Session.GetString("NumDocIdeEP");

                string metodo = "Index";
                string Controlador = "LoginEp";
                //HttpContext.Session = null;
                HttpContext.Session.Remove("NumDocIdeEP");
                HttpContext.Session.Remove("CodDocIdeEP");
                HttpContext.Session.Remove("IdUsuario");
                HttpContext.Session.Remove("DesUsuario");
                HttpContext.Session.Remove("PerfilUsuario");
                HttpContext.Session.Remove("cod_Empresa");
                HttpContext.Session.Remove("DescripcionPerfil");
                HttpContext.Session.Remove(UTConstantes.USUARIO_SESION);
                HttpContext.Session.Remove("ContraWS");

                //HttpContext.Session.Clear();

                //HttpContext.Response.Cookies.Delete("UsuarioEP");
                foreach (var cookie in Request.Cookies.Keys)
                {
                    Response.Cookies.Delete(cookie);
                }
                configuration.GetReloadToken();
                HttpContext.SignOutAsync();
                HttpContext.Session.Clear();
                
                /*HttpContext.SignOutAsync(
                    scheme: "FiverSecurityCookie");*/
                return RedirectToAction(metodo, Controlador);
            }
            else
            {
                HttpContext.Session = null;

                return View();
            }
           
        }

        public RecaptchaResponse ValidateCaptcha(string response)
        {
            //string secret = System.Web.Configuration.WebConfigurationManager.AppSettings["6LfJPwATAAAAAFmb19YL_SQxvyhGTAgYD9c-p7N5"];
            RecaptchaResponse resultado = new RecaptchaResponse();
            string secret = "6LfJPwATAAAAAFmb19YL_SQxvyhGTAgYD9c-p7N5";

            var proxyTres = new WebProxy("srvproxy01.osiptel.gob.pe", 3128)
            {
                UseDefaultCredentials = false,
                Credentials = CredentialCache.DefaultCredentials
            };

            var handler = new HttpClientHandler
            {
                Proxy = proxyTres,
                PreAuthenticate = true,
                UseDefaultCredentials = true
            };


            var client = new WebClient();

            var usarProxy = configuration["usarProxy"];
            var jsonResult = "";
            if (usarProxy.Equals("1"))
            {
                client.Proxy = proxyTres;
            }
            try
            {
                jsonResult = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
                resultado = JsonConvert.DeserializeObject<RecaptchaResponse>(jsonResult.ToString());
            }
            catch (Exception ex)
            {
                LogError.RegistrarErrorMetodo(ex, "ValidateCaptcha");
                resultado.success = false;
            }

            return resultado;
        }
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ValidarUsuario(IFormCollection collection)
        {
            RecaptchaResponse response = ValidateCaptcha(Request.Form["g-recaptcha-response"]);
            var captcha = response.success;
            //if (true)
            if (captcha)
            {
                try
                {
                    String userId = collection["txtUsuario"].ToString();
                    String resultado = validarUsuarioBD(collection);
                    String metodo = "", Controlador = "";
                    if (resultado == "0")
                    {
                        var listaRequerientoJSON = JsonConvert.SerializeObject(new List<BE_REQUERIMIENTO>());
                        HttpContext.Session.SetString("ListaIMEI", listaRequerientoJSON);

                        string caracter = "";
                        if (userId.Length > 2) { caracter = userId.Substring(0, 3).ToLower(); }
                        if (caracter == "ep_")
                        {
                            metodo = "Index";
                            Controlador = "Registro";
                        }
                        else
                        {
                            metodo = "Index";
                            Controlador = "Principal";
                        }
                    }
                    else
                    {
                        TempData["Mensaje"] = resultado;

                        metodo = "Index";
                        Controlador = "Login";
                    }
                    return RedirectToAction(metodo, Controlador);
                }
                catch (Exception ex)
                {
                    LogError.RegistrarErrorMetodo(ex, "ValidarUsuario");
                    TempData["Mensaje"] = ex.Message.ToString();
                    return RedirectToAction(nameof(Index));
                }
            }
            else
            {
                ViewBag.if_error = true;
                ViewBag.msg_error = "Captcha no válido, vuelva a ingresar";
                return View("Index");
            }
        }

        /*Esta función valida los datos del usuario y devuelve un string que indica: 
 * 0=Correcto
 * 1=Usuario invalido
 * 2=El usuario no tiene asociado un perfil
 * 3=Ha ocurrido una excepcion
 */

        protected string validarUsuarioBD(IFormCollection formulario)
        {
            String strUsuario = formulario["txtUsuario"].ToString().Trim();
            String strContrasenia = formulario["txtClave"].ToString().Trim();
            String strError = String.Empty;
            UsuarioBE objUsu = new UsuarioBE();
            String lsIDUsuario = String.Empty;

            string caracter = "";
            if (strUsuario.Length > 2)
            {
                caracter = strUsuario.Substring(0, 3).ToLower();
            }

            strError = "";

            if (caracter == "eo_")
            {

                using (UsuarioBL objBLUsuario = new UsuarioBL())
                {
                    UsuarioBE objBEUsuario = new UsuarioBE();
                    objBEUsuario.IdUsuario = strUsuario;
                    objBEUsuario.Clave = strContrasenia;
                    var claveSha = EncryptProvider.Sha1(strContrasenia);

                    lsIDUsuario = objBLUsuario.ValidarUsuEO(strUsuario, claveSha, UTConstantes.APLICACION);
                    
                    if (lsIDUsuario == "0")
                    {
                        return "Usuario o contraseña incorrecta.";
                    }

                    DataTable dt = objBLUsuario.ObtenerUsuEO(objBEUsuario.IdUsuario, UTConstantes.APLICACION);
                    if (dt.Rows.Count == 0)
                    {
                        return "Usuario no válido";
                    }

                    try
                    {
                        DataTable dtPerfilUser = objBLUsuario.ObtenerIdPerfil(UTConstantes.APLICACION, Convert.ToString(objBEUsuario.IdUsuario));
                        objBEUsuario.Nombres = dt.Rows[0]["NOMBRE"].ToString() + ", " + dt.Rows[0]["APEPAT"].ToString() + " " + dt.Rows[0]["APEMAT"].ToString();                        

                        var idUsuario = Convert.ToString(objBEUsuario.IdUsuario);
                        var PerfilUsuario = dtPerfilUser.Rows[0]["IDPERFIL"].ToString();
                        var cod_Empresa = dt.Rows[0]["IDEMPRESA"].ToString();
                        var DescripcionPerfil = dtPerfilUser.Rows[0]["DESCRIPCION"].ToString();
                        objBEUsuario.Cod_Empresa = Convert.ToInt32(dt.Rows[0]["IDEMPRESA"].ToString());
                        var objUsuarioJSON = JsonConvert.SerializeObject(objBEUsuario);


                        HttpContext.Session.SetString("IdUsuario", idUsuario);
                        HttpContext.Session.SetString("DesUsuario", idUsuario);
                        HttpContext.Session.SetString("PerfilUsuario", PerfilUsuario);
                        HttpContext.Session.SetString("cod_Empresa", cod_Empresa);
                        HttpContext.Session.SetString("DescripcionPerfil", DescripcionPerfil);
                        HttpContext.Session.SetString(UTConstantes.USUARIO_SESION, objUsuarioJSON);
                        HttpContext.Session.SetString("ContraWS", formulario["txtClave"].ToString().Trim());

                        return "0";
                    }
                    catch (Exception)
                    {
                        return "Los datos ingresados son incorrectos.";
                    }
                }
            }
            if (caracter == "ep_")
            {

                UsuarioOEIBL objBLUsuario = new UsuarioOEIBL();

                UsuarioOEIBE objBEUsuario = new UsuarioOEIBE();
                objBEUsuario.IDUSUARIO = strUsuario;

                var claveSha = EncryptProvider.Sha1(strContrasenia);
                objBEUsuario.CLAVE = claveSha;
                objBEUsuario.APLICACION = UTConstantes.APLICACION;
                lsIDUsuario = objBLUsuario.SP_VALIDAR_USUARIO_OEI(objBEUsuario, "C").ToString();
                if (lsIDUsuario == "0")
                {
                    return "Usuario o contraseña incorrecta.";
                }
                objBEUsuario = objBLUsuario.SP_OBTENER_USUARIO_OEI(objBEUsuario);
                if (objBEUsuario.IDUSUARIO.Length == 0)
                {
                    return "Usuario no válido";
                }
                try
                {

                    var idUsuario = Convert.ToString(objBEUsuario.IDUSUARIO);
                    HttpContext.Session.SetString("IdUsuario", idUsuario);
                    HttpContext.Session.SetString("NombEmpresa", "OSIPTEL");

                    UsuarioBL objLogica = new UsuarioBL();
                    UsuarioBE objUsuario = objLogica.ObtenerUsuarioSolicitante(strUsuario);

                    var idTipoSolicitante = Convert.ToString(objUsuario.idTipoSolicitante);
                    var descTipoSolicitante = Convert.ToString(objUsuario.descTipoSolicitante);

                    HttpContext.Session.SetString("idTipoSolicitante", idTipoSolicitante);
                    HttpContext.Session.SetString("descTipoSolicitante", descTipoSolicitante);
                    HttpContext.Session.SetString("ContraWS", formulario["txtClave"].ToString().Trim());

                    return "0";
                }
                catch (Exception ex)
                {
                    LogError.RegistrarErrorMetodo(ex, "validarUsuarioBD");
                    return "Los datos ingresados son incorrectos.";
                }

            }
            else
            {
                strError = validaLDAP(strUsuario, strContrasenia);

                //strError = ""; // HABILITADO PARA PRUEBAS PPBMS
                if (strError.Length != 0)
                {
                    return "Usuario o contraseña incorrecta.";
                }

                using (UsuarioBL objBLUsuario = new UsuarioBL())
                {
                    UsuarioBE objBEUsuario = new UsuarioBE();
                    objBEUsuario.IdUsuario = strUsuario;
                    objBEUsuario.Clave = strContrasenia;
                    lsIDUsuario = objBLUsuario.Validar(strUsuario).ToString();

                    if (lsIDUsuario == "0")
                    {
                        return "Usuario no válido";
                    }

                    objBEUsuario = objBLUsuario.Obtener(strUsuario);
                    if (objBEUsuario == null)
                    {
                        return "Usuario no válido";
                    }

                    if (objBEUsuario.IdUsuario.ToUpper() != strUsuario.ToUpper())
                    {
                        return "Usuario no válido";
                    }
                    else
                    {
                        try
                        {
                            DataTable dtPerfilUser = objBLUsuario.ObtenerIdPerfil(UTConstantes.APLICACION, Convert.ToString(objBEUsuario.IdUsuario));

                            var IdUsuario = objBEUsuario.IdUsuario.ToString();
                            var PerfilUsuario = dtPerfilUser.Rows[0]["IDPERFIL"].ToString();
                            var cod_Empresa = "-1";
                            var NombEmpresa = "OSIPTEL";
                            var DescripcionPerfil = dtPerfilUser.Rows[0]["DESCRIPCION"].ToString();
                            var objBEUsuarioJSON = JsonConvert.SerializeObject(objBEUsuario);

                            HttpContext.Session.SetString("IdUsuario", IdUsuario);
                            HttpContext.Session.SetString("DesUsuario", IdUsuario);
                            HttpContext.Session.SetString("PerfilUsuario", PerfilUsuario);
                            HttpContext.Session.SetString("cod_Empresa", cod_Empresa);
                            HttpContext.Session.SetString("NombEmpresa", NombEmpresa);
                            HttpContext.Session.SetString("DescripcionPerfil", DescripcionPerfil);
                            HttpContext.Session.SetString("ContraWS", formulario["txtClave"].ToString().Trim());

                            HttpContext.Session.SetString(UTConstantes.USUARIO_SESION, objBEUsuarioJSON);

                            return "0";
                        }
                        catch (Exception ex)
                        {
                            LogError.RegistrarErrorMetodo(ex, "validarUsuarioBD");
                            return "El usuario no tiene asociado un perfil";
                        }
                    }
                }
            }
        }



        private String validaLDAP(String username, String pwd)
        {

            Funciones objFunciones;

            try
            {
                //string strPath = "LDAP://srvdc/DC=osiptel,DC=gob,DC=pe";
                objFunciones = new Funciones();
                String strPath = objFunciones.ObtenerLDAP();
                string strDomain = "osiptel";
                string domainAndUsername = strDomain + @"\" + username;

                DirectoryEntry entry = new DirectoryEntry(strPath, domainAndUsername, pwd);
                DirectorySearcher search = new DirectorySearcher(entry);

                search.Filter = "name=" + username;

                SearchResultCollection results = search.FindAll();
                foreach (SearchResult resultados in results)
                {

                    ResultPropertyCollection colProperties = resultados.Properties;

                    String ls = String.Empty;
                    foreach (string key in colProperties.PropertyNames)
                    {
                        foreach (object value in colProperties[key])
                        {
                            ls = ls + "" + key.ToString() + ": " + value + "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return "";
        }

        public JsonResult CheckSession(int valor)
        {
            if (valor == 2)
            {
                HttpContext.Session.SetString("IdUsuario", null);
                HttpContext.Session.Clear();

            }

            String mUsuario = HttpContext.Session.GetString("IdUsuario");

            if (mUsuario == null)
            {
                valor = UTConstantes.IND_ACTIVO;
            }

            return Json(valor);
        }
             
    }
}