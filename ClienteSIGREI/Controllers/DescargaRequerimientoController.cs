﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Osiptel.SIGREI.BE;
using Osiptel.SIGREI.BL;

namespace ClienteSIGREI.Controllers
{
    public class DescargaRequerimientoController : Controller
    {
        private readonly IConfiguration configuration;
        public DescargaRequerimientoController(IConfiguration config)
        {
            this.configuration = config;
        }


        public FileResult Index(String id)
        {

            BL_REQUERIMIENTO_SOL objLogica = new BL_REQUERIMIENTO_SOL();
            BE_REQUERIMIENTO_SOL objRequerimiento = new BE_REQUERIMIENTO_SOL();

            objRequerimiento.nroRequerimientoSol = id;
            objRequerimiento = objLogica.Recibir(objRequerimiento);

            objRequerimiento.nroRequerimientoSol = id;

            objRequerimiento = objLogica.Consultar(objRequerimiento);

            String nombreDocumento = objRequerimiento.descDocumentoAdjunto;

            var pathServerDocumemto = configuration["RutaDocumentos"];
            pathServerDocumemto = pathServerDocumemto + "/" + nombreDocumento.ToString();
            //string extensionArchivo = System.IO.Path.GetExtension(nombreDocumento.ToString());
            byte[] fileBytes = System.IO.File.ReadAllBytes(pathServerDocumemto);
            
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreDocumento);
        }
    }
}