﻿using Osiptel.SIGREI.BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Osiptel.Clases
{
    public class BE_GENERAL
    { 
        public string anio { get; set; }
        public string descripcion { get; set; }
        public string idCondicion { get; set; }
        public Object objeto { get; set; }
        public List<String> listaTitulo { get; set; }
        public List<BE_REPORTE_IMEI> listaResultado { get; set; }
    }
}
