﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using Osiptel.SIGREI.BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

public class ExportExcel
{
    public static void PropiedadesArchivo(ExcelPackage excel)
    {
        excel.Workbook.Properties.Title = "Ubigeo";
        excel.Workbook.Properties.Author = "OSIPTEL";
        excel.Workbook.Properties.Comments = "OSIPTEL";
        excel.Workbook.Properties.Company = "OSIPTEL";
    }

    public static void ColorHeader(ExcelWorksheet sheet, int r0, int c0, int r1, int c1, Color headerColor)
    {
        sheet.Cells[r0, c0, r1, c1].Style.Border.BorderAround(ExcelBorderStyle.Thin);
        sheet.Cells[r0, c0, r1, c1].Style.Fill.PatternType = ExcelFillStyle.Solid;
        sheet.Cells[r0, c0, r1, c1].Style.Fill.BackgroundColor.SetColor(headerColor);
        sheet.Cells[r0, c0, r1, c1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        sheet.Cells[r0, c0, r1, c1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
        sheet.Cells[r0, c0, r1, c1].Style.Font.Bold = true;
    }

    public byte[] generarIMEI_EO(List<BE_REQUERIMIENTO_IMEI_EO> listaIMEI, string filename)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(filename);

            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(91, 155, 213);
            int r = 1;
            int columna = 1, fila = 1;


            var titleCell = sheet.Cells[fila, columna]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "Nro.";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();


            columna++;
            titleCell = sheet.Cells[fila, columna]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "ESTADO DEL REPORTE ";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "FECHA DEL REPORTE \n (dd/mm/aaaa  hh:mi:ss)";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NÚMERO DE SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "APELLIDO PATERNO DEL POSIBLE TITULAR DEL EQUIPO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "APELLIDO MATERNO DEL POSIBLE TITULAR DEL EQUIPO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NOMBRES DEL POSIBLE TITULAR DEL EQUIPO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna]; // Tomamos un rango de celdas
            titleCell.Merge = true;
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "TIPO DE DOCUMENTO LEGAL 1=DNI,\n 2=RUC,\n 3=Carné de Extranjería,\n 4=Pasaporte,\n5=Documento Legal de Identidad válido requerido por la SNM.";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.Style.Numberformat.Format = "";

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NÚMERO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;



            ExportExcel.ColorHeader(sheet, r, 1, r, 10, headerColor);

            Color headerAmarillo = Color.FromArgb(249, 255, 51);
            #endregion

            r++;
            columna = 0;
            int filaExcel = 2;

            for (int i = 0; i < listaIMEI.Count; i++)
            {
                columna = 1; ;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = (i + 1).ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].estadoImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].fecReporte.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroServicio.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }


                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].apPaternoTitular.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }


                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].apMaternoTitular.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nombreTitular.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].idTipoDocumento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                titleCell.Style.Numberformat.Format = "";
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroDocumento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                if (listaIMEI[i].idTipoObservacion > 0)
                {
                    titleCell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    titleCell.Style.Fill.BackgroundColor.SetColor(headerAmarillo);
                }

                titleCell.Style.Numberformat.Format = "@";

                filaExcel++;
            }
            sheet.Column(1).Width = 8;
            sheet.Column(2).Width = 18;
            sheet.Column(3).Width = 24;
            sheet.Column(4).Width = 16;
            sheet.Column(9).Width = 30;
            sheet.Column(9).Style.WrapText = true;
            sheet.Column(9).Style.Numberformat.Format = "@";
            sheet.Row(1).Height = 110;

            return excel.GetAsByteArray();
        }
    }

    public byte[] generarIMEI(List<BE_REQUERIMIENTO_IMEI> listaIMEI, string filename)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera


            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(filename);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(91, 155, 213);
            int r = 1;

            var titleCell = sheet.Cells["A1"]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "Nro.";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["B1"]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();


            titleCell = sheet.Cells["C1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "ESTADO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);

            titleCell.Style.WrapText = true;
            titleCell = sheet.Cells["D1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "FECHA DEL REPORTE";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["E1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NÚMERO DE SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["F1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "nombreTitular Y APELLIDOS DEL PROPIETARIOS";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["G1"]; // Tomamos un rango de celdas
            titleCell.Merge = true;
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "TIPO DE DOCUMENTO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;


            titleCell = sheet.Cells["H1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NÚMERO DE DOCUMENTO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, r, 1, r, 8, headerColor);
            #endregion


            r++;
            int columna = 0;
            int filaExcel = 2;

            for (int i = 0; i < listaIMEI.Count; i++)
            {
                columna = 1; ;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = (i + 1).ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].descTipoReporte.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].fecReporte.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroServicio.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;


                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nombreCompletoTitular.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].descTipoDocumento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroDocumento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                filaExcel++;
            }



            return excel.GetAsByteArray();
        }
    }

    public byte[] generarIMEI_SOL(List<BE_REQUERIMIENTO_IMEI> listaIMEI, string filename)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(filename);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(91, 155, 213);
            int r = 1;
            int columna = 1;

            var titleCell = sheet.Cells[1, columna]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "Nro.";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            columna++;
            titleCell = sheet.Cells[1, columna]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            columna++;
            titleCell = sheet.Cells[1, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "ESTADO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            //columna++;
            //titleCell = sheet.Cells[1, columna];
            //titleCell.Style.Font.Bold = true;
            //titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            //titleCell.Style.Font.Color.SetColor(Color.Black);
            //titleCell.Value = "FECHA DEL REPORTE";
            //titleCell.AutoFitColumns(20);
            //titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            //titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[1, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NÚMERO DE SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[1, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NOMBRES Y APELLIDOS DEL POSIBLE PROPIETARIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[1, columna]; // Tomamos un rango de celdas
            titleCell.Merge = true;
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "TIPO DE DOCUMENTO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[1, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NÚMERO DE DOCUMENTO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, r, 1, r, columna, headerColor);
            #endregion

            r++;
            columna = 0;

            int filaExcel = 2;

            for (int i = 0; i < listaIMEI.Count; i++)
            {
                columna = 1; ;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = (i + 1).ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].estadoImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                //columna++;
                //titleCell = sheet.Cells[filaExcel, columna];
                //titleCell.Merge = true;
                //titleCell.Style.Font.Bold = true;
                //titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //titleCell.Style.Font.Color.SetColor(Color.Black);
                //titleCell.Value = listaIMEI[i].fecReporte.ToString();
                //titleCell.AutoFitColumns(20);
                //titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                //titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroServicio.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nombreCompletoTitular.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].descTipoDocumento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroDocumento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                filaExcel++;
            }
            sheet.Column(1).Width = 6;
            sheet.Column(2).Width = 16;
            sheet.Column(3).Width = 10;
            //sheet.Column(4).Width = 12;
            sheet.Column(4).Width = 12;
            sheet.Column(5).Width = 26;
            sheet.Column(6).Width = 14;
            sheet.Column(7).Width = 16;

            sheet.Row(1).Height = 60;

            return excel.GetAsByteArray();
        }
    }

    public byte[] generarIMEI_SOL_SS(List<BE_REQUERIMIENTO_IMEI> listaIMEI, string filename)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(filename);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(91, 155, 213);

            int r = 1;

            int columna = 1;
            var titleCell = sheet.Cells[1, columna]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "Nro.";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            columna++;
            titleCell = sheet.Cells[1, columna]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            columna++;
            titleCell = sheet.Cells[1, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "ESTADO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            //columna++;
            //titleCell = sheet.Cells[1, columna];
            //titleCell.Style.Font.Bold = true;
            //titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            //titleCell.Style.Font.Color.SetColor(Color.Black);
            //titleCell.Value = "FECHA DEL REPORTE";
            //titleCell.AutoFitColumns(20);
            //titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            //titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[1, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NOMBRES Y APELLIDOS DEL POSIBLE PROPIETARIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[1, columna]; // Tomamos un rango de celdas
            titleCell.Merge = true;
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "TIPO DE DOCUMENTO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[1, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "NÚMERO DE DOCUMENTO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, r, 1, r, columna, headerColor);
            #endregion



            int filaExcel = 2;

            for (int i = 0; i < listaIMEI.Count; i++)
            {
                columna = 1; ;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = (i + 1).ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].estadoImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                //columna++;
                //titleCell = sheet.Cells[filaExcel, columna];
                //titleCell.Merge = true;
                //titleCell.Style.Font.Bold = true;
                //titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //titleCell.Style.Font.Color.SetColor(Color.Black);
                //titleCell.Value = listaIMEI[i].fecReporte.ToString();
                //titleCell.AutoFitColumns(20);
                //titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                //titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nombreCompletoTitular.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].descTipoDocumento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroDocumento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                filaExcel++;
            }

            sheet.Column(1).Width = 6;
            sheet.Column(2).Width = 16;
            sheet.Column(3).Width = 10;
            //sheet.Column(4).Width = 12;            
            sheet.Column(4).Width = 26;
            sheet.Column(5).Width = 14;
            sheet.Column(6).Width = 16;

            sheet.Row(1).Height = 60;

            return excel.GetAsByteArray();
        }
    }

    public byte[] generarIMEI_SIN_INFO(List<BE_REQUERIMIENTO_IMEI> listaIMEI, string filename)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(filename);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(91, 155, 213);
            int r = 1;

            var titleCell = sheet.Cells["A1"]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "Nro.";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["B1"]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["C1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "MOTIVO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);


            ExportExcel.ColorHeader(sheet, r, 1, r, 3, headerColor);
            #endregion

            r++;
            int columna = 0;
            int filaExcel = 2;

            for (int i = 0; i < listaIMEI.Count; i++)
            {
                columna = 1; ;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = (i + 1).ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].nroImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaIMEI[i].descTipoMotivoAtencion.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;
                sheet.Row(filaExcel).Height = 120;
                filaExcel++;
            }
            sheet.Column(1).Width = 4;
            sheet.Column(2).Width = 16;
            sheet.Column(3).Width = 84;
            sheet.Row(1).Height = 80;

            return excel.GetAsByteArray();
        }
    }

    public byte[] VariasHojas(string HojaUno, string HojaDos, string HojaTres)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(HojaUno);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(91, 155, 213);
            int r = 1;

            var titleCell = sheet.Cells["A1"]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "Nro.";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["B1"]; // Tomamos un rango de celdas          
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["C1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.Black);
            titleCell.Value = "MOTIVO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);


            ExportExcel.ColorHeader(sheet, r, 1, r, 3, headerColor);
            #endregion

            var sheetHojaDos = excel.Workbook.Worksheets.Add(HojaDos);
            sheetHojaDos.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheetHojaDos.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheetHojaDos.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheetHojaDos.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            headerColor = Color.FromArgb(91, 155, 213);
            r = 1;

            var titleCellHojaDos = sheetHojaDos.Cells["A1"]; // Tomamos un rango de celdas          
            titleCellHojaDos.Style.Font.Bold = true;
            titleCellHojaDos.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCellHojaDos.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCellHojaDos.Style.Font.Color.SetColor(Color.Black);
            titleCellHojaDos.Value = "Nro.";
            titleCellHojaDos.AutoFitColumns(20);
            titleCellHojaDos.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCellHojaDos.Style.WrapText = true;
            titleCellHojaDos.AutoFitColumns();

            titleCellHojaDos = sheetHojaDos.Cells["B1"]; // Tomamos un rango de celdas          
            titleCellHojaDos.Style.Font.Bold = true;
            titleCellHojaDos.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCellHojaDos.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCellHojaDos.Style.Font.Color.SetColor(Color.Black);
            titleCellHojaDos.Value = "IMEI";
            titleCellHojaDos.AutoFitColumns(20);
            titleCellHojaDos.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCellHojaDos.Style.WrapText = true;
            titleCellHojaDos.AutoFitColumns();

            titleCellHojaDos = sheetHojaDos.Cells["C1"];
            titleCellHojaDos.Style.Font.Bold = true;
            titleCellHojaDos.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCellHojaDos.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCellHojaDos.Style.Font.Color.SetColor(Color.Black);
            titleCellHojaDos.Value = "MOTIVO";
            titleCellHojaDos.AutoFitColumns(20);
            titleCellHojaDos.Style.Border.BorderAround(ExcelBorderStyle.Thin);


            ExportExcel.ColorHeader(sheetHojaDos, r, 1, r, 3, headerColor);


            var sheetHojaTres = excel.Workbook.Worksheets.Add(HojaTres);
            sheetHojaTres.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheetHojaTres.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheetHojaTres.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheetHojaTres.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            headerColor = Color.FromArgb(91, 155, 213);
            r = 1;

            var titleCellHojaTres = sheetHojaTres.Cells["A1"]; // Tomamos un rango de celdas          
            titleCellHojaTres.Style.Font.Bold = true;
            titleCellHojaTres.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCellHojaTres.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCellHojaTres.Style.Font.Color.SetColor(Color.Black);
            titleCellHojaTres.Value = "Nro.";
            titleCellHojaTres.AutoFitColumns(20);
            titleCellHojaTres.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCellHojaTres.Style.WrapText = true;
            titleCellHojaTres.AutoFitColumns();

            titleCellHojaTres = sheetHojaTres.Cells["B1"]; // Tomamos un rango de celdas          
            titleCellHojaTres.Style.Font.Bold = true;
            titleCellHojaTres.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCellHojaTres.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCellHojaTres.Style.Font.Color.SetColor(Color.Black);
            titleCellHojaTres.Value = "IMEI";
            titleCellHojaTres.AutoFitColumns(20);
            titleCellHojaTres.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCellHojaTres.Style.WrapText = true;
            titleCellHojaTres.AutoFitColumns();

            titleCellHojaTres = sheetHojaTres.Cells["C1"];
            titleCellHojaTres.Style.Font.Bold = true;
            titleCellHojaTres.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCellHojaTres.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCellHojaTres.Style.Font.Color.SetColor(Color.Black);
            titleCellHojaTres.Value = "MOTIVO";
            titleCellHojaTres.AutoFitColumns(20);
            titleCellHojaTres.Style.Border.BorderAround(ExcelBorderStyle.Thin);


            ExportExcel.ColorHeader(sheetHojaTres, r, 1, r, 3, headerColor);



            return excel.GetAsByteArray();
        }
    }


    public byte[] generarReporte(List<BE_REPORTE_IMEI> listaReporte, string filename)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(filename);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(5, 65, 97);
            int r = 1;

            var titleCell = sheet.Cells["A1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Departamento";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["B1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Nombre de la Institución Pública";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["C1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Código de Solicitud";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);

            titleCell.Style.WrapText = true;
            titleCell = sheet.Cells["D1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["E1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Tipo de Respuesta";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["F1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Fecha en la que Ingresó el Requerimiento de la Institución Pública";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["G1"]; // Tomamos un rango de celdas
            titleCell.Merge = true;
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Fecha en la que se Solicita Información a la E.O.";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["H1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Empresa Operadora";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["I1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "¿Cumplió con el plazo establecido?";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["J1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Fecha de respuesta de la Empresa Operadora";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["K1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Fecha de Respuesta a la Institución Pública";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["L1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "Usuario que Atendió";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, r, 1, r, 12, headerColor);
            #endregion

            r++;
            int columna = 0;
            int filaExcel = 2;

            for (int i = 0; i < listaReporte.Count; i++)
            {
                columna = 1; ;
                //titleCell = sheet.Cells[filaExcel, columna];
                //titleCell.Merge = true;
                //titleCell.Style.Font.Bold = true;
                //titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //titleCell.Style.Font.Color.SetColor(Color.Black);
                //titleCell.Value = (i + 1).ToString();
                //titleCell.AutoFitColumns(20);
                //titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                //titleCell.Style.WrapText = true;

                //columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].departamento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].nombreInstitucion.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].nroRequerimiento.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].nroImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].estadoImei.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].fechaRegistro.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].fechaEnvio.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].empresaOperadora.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].cumplioPlazo.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].fechaAtendido.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].fecRespuesta.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;

                columna++;
                titleCell = sheet.Cells[filaExcel, columna];
                titleCell.Merge = true;
                titleCell.Style.Font.Bold = true;
                titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                titleCell.Style.Font.Color.SetColor(Color.Black);
                titleCell.Value = listaReporte[i].usuRespuesta.ToString();
                titleCell.AutoFitColumns(20);
                titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                titleCell.Style.WrapText = true;


                filaExcel++;
            }
            sheet.Column(1).Width = 16;
            sheet.Column(2).Width = 30;
            sheet.Column(3).Width = 14;
            sheet.Column(4).Width = 16;
            sheet.Column(5).Width = 18;
            sheet.Column(6).Width = 26;
            sheet.Column(7).Width = 16;
            sheet.Column(8).Width = 24;

            sheet.Column(9).Width = 24;
            sheet.Column(10).Width = 24;
            sheet.Column(11).Width = 24;
            sheet.Column(12).Width = 16;

            sheet.Row(1).Height = 80;

            return excel.GetAsByteArray();
        }
    }

    public byte[] exportarConsultaGSMA(DataTable objResultado, String nombreHoja)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(nombreHoja);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(5, 65, 97);
            int r = 1;

            var titleCell = sheet.Cells["A1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NRO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["B1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TAC";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            titleCell = sheet.Cells["C1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);

            titleCell.Style.WrapText = true;
            titleCell = sheet.Cells["D1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO COMERCIAL";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            titleCell = sheet.Cells["E1"];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO TÉCNICO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;


            ExportExcel.ColorHeader(sheet, r, 1, r, 5, headerColor);
            #endregion

            r++;

            //titleCell.LoadFromDataTable(objResultado,true);
            sheet.Cells["A2"].LoadFromDataTable(objResultado, false);
            //sheet.Column(1).Width = 16;
            //sheet.Column(2).Width = 30;
            //sheet.Column(3).Width = 14;
            //sheet.Column(4).Width = 16;
            //sheet.Column(5).Width = 18;
            //sheet.Column(6).Width = 26;
            //sheet.Column(7).Width = 16;
            //sheet.Column(8).Width = 24;

            //sheet.Column(9).Width = 24;
            //sheet.Column(10).Width = 24;
            //sheet.Column(11).Width = 24;
            //sheet.Column(12).Width = 16;

            //sheet.Row(1).Height = 80;

            return excel.GetAsByteArray();
        }
    }

    public byte[] exportarConsultaAbonados(DataTable objResultado, String nombreHoja)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(nombreHoja);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(5, 65, 97);
            int columna = 1;
            int fila = 1;

            var titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE SERVICIO TELEFÓNICO MÓVIL";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;         

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "CONCESIONARIO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;                       

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE ABONADO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NOMBRES DEL ABONADO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO PATERNO DEL ABONADO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO MATERNO DEL ABONADO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "RAZON SOCIAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            
            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE DOCUMENTO LEGAL DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE DOCUMENTO LEGAL DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NOMBRES DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO PATERNO DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO MATERNO DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
                       
            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NACIONALIDAD DEL ABONADO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMSI";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODALIDAD DE CONTRATO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA Y HORA DE ACTIVACION";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "ESTADO DEL SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MOTIVO DE LA SUSPENSIÓN DEL SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MOTIVO DE LA BAJA DEL SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "VINCULACIÓN DEL SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA DEL EQUIPO (RENTESEG)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO DEL EQUIPO (RENTESEG)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA DEL EQUIPO (GSMA)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO DEL EQUIPO (GSMA)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;


            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA Y HORA DE VINCULACIÓN";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "USO DEL EQUIPO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "EQUIPO TERMINAL MÓVIL ADQUIRIDO EN EXTRANJERO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA DE DECLARACIÓN JURADA";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA DE REGISTRO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, fila, 1, fila, columna, headerColor);
            #endregion

            fila++;
            sheet.Cells[fila, 1].LoadFromDataTable(objResultado, false);

            sheet.Column(1).Width = 20;
            sheet.Column(2).Width = 30;
            sheet.Column(3).Width = 18;
            sheet.Column(4).Width = 16;
            sheet.Column(5).Width = 21;
            sheet.Column(6).Width = 25;
            sheet.Column(7).Width = 25;
            sheet.Column(8).Width = 25;
            sheet.Column(9).Width = 25;
            sheet.Column(10).Width = 26;
            sheet.Column(11).Width = 26;
            sheet.Column(12).Width = 26;
            sheet.Column(13).Width = 25;
            sheet.Column(14).Width = 25;
            sheet.Column(15).Width = 17;
            sheet.Column(16).Width = 18;
            sheet.Column(17).Width = 16;
            sheet.Column(18).Width = 22;
            sheet.Column(19).Width = 24;
            sheet.Column(20).Width = 18;
            sheet.Column(21).Width = 22;
            sheet.Column(22).Width = 26;
            sheet.Column(23).Width = 16;
            sheet.Column(24).Width = 37;
            sheet.Column(25).Width = 23;
            sheet.Column(26).Width = 37;
            sheet.Column(27).Width = 18;
            sheet.Column(28).Width = 22;
            sheet.Column(29).Width = 16;
            sheet.Column(30).Width = 20;
            sheet.Column(31).Width = 22;
            sheet.Column(32).Width = 22;

            return excel.GetAsByteArray();
        }
    }

    public byte[] exportarConsultaAbonadosPerfilGestion(DataTable objResultado, String nombreHoja)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(nombreHoja);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(5, 65, 97);
            int columna = 1;
            int fila = 1;

            var titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE SERVICIO TELEFÓNICO MÓVIL";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;


            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "CONCESIONARIO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;
            
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE ABONADO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;


            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NOMBRES DEL ABONADO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO PATERNO DEL ABONADO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO MATERNO DEL ABONADO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "RAZON SOCIAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE DOCUMENTO LEGAL DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE DOCUMENTO LEGAL DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;


            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NOMBRES DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO PATERNO DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO MATERNO DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;
                      

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA Y HORA DE ACTIVACION";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "ESTADO SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "VINCULACIÓN DEL SERVICIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA Y HORA DE VINCULACIÓN";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;


            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;
            
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA DEL EQUIPO (RENTESEG)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO DEL EQUIPO (RENTESEG)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA DEL EQUIPO (GSMA)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO DEL EQUIPO (GSMA)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA REGISTRO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            

            ExportExcel.ColorHeader(sheet, fila, 1, fila, columna, headerColor);
            #endregion

            fila++;
            sheet.Cells[fila, 1].LoadFromDataTable(objResultado, false);
            sheet.Column(1).Width = 18;
            sheet.Column(2).Width = 35;
            sheet.Column(3).Width = 20;
            sheet.Column(4).Width = 20;
            sheet.Column(5).Width = 25;
            sheet.Column(6).Width = 25;
            sheet.Column(7).Width = 25;
            sheet.Column(8).Width = 25;
            sheet.Column(9).Width = 25;
            sheet.Column(10).Width = 26;
            sheet.Column(11).Width = 26;
            sheet.Column(12).Width = 25;
            sheet.Column(13).Width = 25;
            sheet.Column(14).Width = 25;
            sheet.Column(15).Width = 22;
            sheet.Column(16).Width = 26;
            sheet.Column(17).Width = 27;
            sheet.Column(18).Width = 22;
            sheet.Column(19).Width = 18;
            sheet.Column(20).Width = 38;
            sheet.Column(21).Width = 32;
            sheet.Column(22).Width = 38;
            sheet.Column(23).Width = 28;
            sheet.Column(24).Width = 22;


            return excel.GetAsByteArray();
        }
    }

    public byte[] exportarListaNegra(DataTable objResultado, String nombreHoja)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(nombreHoja);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(5, 65, 97);
            int columna = 1;
            int fila = 1;

            var titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "CONCESIONARIO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE SERVICIO TELEFÓNICO MÓVIL";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMSI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA DEL EQUIPO (RENTESEG)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO DEL EQUIPO (RENTESEG)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA DEL EQUIPO (GSMA)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO DEL EQUIPO (GSMA)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO SERVICIO TELEFONICO DESDE EL CUAL REPORTA HECHO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FUENTE DEL REPORTE";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MOTIVO DEL REPORTE";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "CODIGO DEL REPORTE";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA Y HORA DEL REPORTE";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA Y HORA DEL BLOQUEO O DESBLOQUEO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NOMBRES DEL ABONADO O USUARIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO PATERNO DEL ABONADO O USUARIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO MATERNO DEL ABONADO O USUARIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "RAZON SOCIAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NOMBRES DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO PATERNO DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO MATERNO DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE DOCUMENTO LEGAL DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE DOCUMENTO LEGAL DEL REPRESENTANTE LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "ESTADO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA DE REGISTRO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, fila, 1, fila, columna, headerColor);
            #endregion

            fila++;
            sheet.Cells[fila, 1].LoadFromDataTable(objResultado, false);


            return excel.GetAsByteArray();
        }
    }

    public byte[] exportarListaNegraPerfilGestion(DataTable objResultado, String nombreHoja)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(nombreHoja);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(5, 65, 97);
            int columna = 1;
            int fila = 1;

            var titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "CONCESIONARIO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE SERVICIO TELEFÓNICO MÓVIL";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMSI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMEI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA DEL EQUIPO (RENTESEG)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO DEL EQUIPO (RENTESEG)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MARCA DEL EQUIPO (GSMA)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MODELO DEL EQUIPO (GSMA)";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;


            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO SERVICIO TELEFONICO DESDE EL CUAL REPORTA HECHO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FUENTE DEL REPORTE";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MOTIVO DEL REPORTE";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "CODIGO DEL REPORTE";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA Y HORA DEL REPORTE";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA Y HORA DEL BLOQUEO O DESBLOQUEO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NOMBRES DEL ABONADO O USUARIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO PATERNO DEL ABONADO O USUARIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "APELLIDO MATERNO DEL ABONADO O USUARIO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "RAZON SOCIAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "TIPO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE DOCUMENTO LEGAL";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "ESTADO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA DE REGISTRO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, fila, 1, fila, columna, headerColor);
            #endregion

            fila++;
            sheet.Cells[fila, 1].LoadFromDataTable(objResultado, false);


            return excel.GetAsByteArray();
        }
    }

    public byte[] exportarListaVinculacion(DataTable objResultado, String nombreHoja)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(nombreHoja);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(5, 65, 97);
            int columna = 1;
            int fila = 1;


           var  titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMEI (14 DIGITOS)";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMSI";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "NUMERO DE SERVICIO TELEFÓNICO MÓVIL";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
          
            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA DE VINCULACION VOZ";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA DE VINCULACION DATOS";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            columna++;

            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "CONCESIONARIO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();
     

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MES";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "AÑO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, fila, 1, fila, columna, headerColor);
            #endregion

            fila++;
            sheet.Cells[fila, 1].LoadFromDataTable(objResultado, false);
            sheet.Column(1).Width = 20;
            sheet.Column(2).Width = 23;
            sheet.Column(3).Width = 17;
            sheet.Column(4).Width = 18;
            sheet.Column(5).Width = 42;
            sheet.Column(6).Width = 19;
            sheet.Column(7).Width = 11;



            return excel.GetAsByteArray();
        }
    }

    public byte[] exportarListaEIR(DataTable objResultado, String nombreHoja)
    {

        using (var excel = new ExcelPackage())
        {
            #region Cabecera

            ExportExcel.PropiedadesArchivo(excel);

            var sheet = excel.Workbook.Worksheets.Add(nombreHoja);
            sheet.HeaderFooter.OddHeader.CenteredText = "&amp;24&amp;U&amp;\"Arial,Regular Bold\" Inventory";
            sheet.HeaderFooter.OddHeader.RightAlignedText =
            string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            sheet.HeaderFooter.OddHeader.CenteredText = ExcelHeaderFooter.SheetName;
            sheet.HeaderFooter.OddHeader.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            Color headerColor = Color.FromArgb(5, 65, 97);
            int columna = 1;
            int fila = 1;
           var titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "IMEI (14 DIGITOS)";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "CONCESIONARIO";
            titleCell.AutoFitColumns(20);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;
            titleCell.AutoFitColumns();

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "MES";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "AÑO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            columna++;
            titleCell = sheet.Cells[fila, columna];
            titleCell.Style.Font.Bold = true;
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            titleCell.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            titleCell.Style.Font.Color.SetColor(Color.White);
            titleCell.Value = "FECHA DE REGISTRO";
            titleCell.AutoFitColumns(50);
            titleCell.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            titleCell.Style.WrapText = true;

            ExportExcel.ColorHeader(sheet, fila, 1, fila, columna, headerColor);
            #endregion

            fila++;
            sheet.Cells[fila, 1].LoadFromDataTable(objResultado, false);
            sheet.Column(1).Width = 42;
            sheet.Column(2).Width = 15;
            sheet.Column(3).Width = 11;
            sheet.Column(4).Width = 22;

            return excel.GetAsByteArray();
        }
    }

}
