﻿$(function () {    
    $("#btnSolicitar").click(function () {
             
        var mensaje = validarDatos();
        if (mensaje == "") {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false,
            });
            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: "¿Está seguro de registrar la información ingresada?",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    registrarRequeriento();
                }
            });
        } else {
            mostrarMensajeError(mensaje);
        }
    });
    $("#btnDescargar").click(function () {
        $("#frmDescargarPlantilla").submit();
    });
});

function registrarRequeriento() {
    MostrarCargando();
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });

    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmRegistroRequerimiento")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Registro/RegistrarRequerimiento",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);            
            mostrarMensajeError(msg);            
        },
        complete: function () {
            CerrarCargando();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        $("#frmRecargarPagina").submit();
                    }
                });
            } else if (key == "2") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        $("#frmDescargarError").submit();
                    }
                })

            }
        }
    });
}
function validarDatos() {

    var mensaje = "";

    var nombreInstitucion = $("#nombreInstitucion").val();
    if (nombreInstitucion == "") {
        mensaje += "* Ingrese el nombre de la Institución.";
        mensaje += "<br/>";
    }
    var txtNombreSolicitante = $("#nombreSolicitante").val();
    var txtCargo = $("#cargo").val();
    var ddlDepartamento = $("#ddlUbigeo").val();
    var txtDireccion = $("#direccion").val();
    var txtCorreo = $("#correo").val();


    if (txtNombreSolicitante == "") {
        mensaje += "* Ingrese el nombre del solicitante.";
        mensaje += "<br/>";
    }
    if (txtCargo == "") {
        mensaje += "* Ingrese el cargo del solicitante.";
        mensaje += "<br/>";
    }
    if (ddlDepartamento == "") {
        mensaje += "* Seleccione el departamento.";
        mensaje += "<br/>";
    }
    if (txtDireccion == "") {
        mensaje += "* Ingrese la dirección.";
        mensaje += "<br/>";
    }
    
    if (txtCorreo == "") {
        mensaje += "* Ingrese un correo electrónico Institucional.";
        mensaje += "<br/>";
    } else {
        var isValid = validateEmail(txtCorreo);
        if (!isValid) {
            valido = 0;
            mensaje = mensaje + "* Ingrese un correo Institucional válido.";
            mensaje += "<br/>";
        } else {

            var h_tipousuario = $("#h_tipousuario").val();            
            var arroba = $("#h_arroba").val();

            var posicion = txtCorreo.indexOf(arroba);
            posicion = posicion + 1;
            var extenCorreo = txtCorreo.substring(posicion, txtCorreo.length).toLowerCase();

            if (h_tipousuario === "1") {
                if (extenCorreo != "mpfn.gob.pe") {
                    mensaje = mensaje + "* El correo no corresponde al Ministerio Público.";
                    mensaje += "<br/>";
                }
            } else if (h_tipousuario === "2") {
                if (extenCorreo != 'pnp.gob.pe') {
                    mensaje = mensaje + "* El correo no corresponde a la PNP";
                    mensaje += "<br/>";
                }
            } else if (h_tipousuario === "3") {
                if (extenCorreo != 'mininter.gob.pe') {
                    mensaje = mensaje + "* El correo no corresponde al Ministerio del Interior.";
                    mensaje += "<br/>";
                }
            }
        }
    }
    var fileOficio = $("#fileOficio").val();

    if (fileOficio == "") {
        mensaje += "* Tiene que adjuntar un oficio con extensión .pdf.";
        mensaje += "<br/>";
    } else {
        var extension = fileOficio.substring(fileOficio.lastIndexOf("."));
        if (extension != ".pdf") {
            mensaje += "* El archivo de oficio no es válido.";
            mensaje += "<br/>";
        }
    }
    var txtnroOficio = $("#nroOficio").val();

    if (txtnroOficio == "") {
        mensaje += "* Ingrese el número de Oficio.";
        mensaje += "<br/>";
    }

    var txtComentario = $("#comentario").val();

    if (txtComentario == "") {
        mensaje += "* Ingrese el motivo del requerimiento.";
        mensaje += "<br/>";
    }

    var fileArchivoImei = $("#fileArchivoImei").val(); 

    if (fileArchivoImei == "") {
        mensaje += "* Tiene que adjuntar el archivo IMEI.";
        mensaje += "<br/>";
    } else {
        var extension = fileArchivoImei.substring(fileArchivoImei.lastIndexOf("."));
        if (extension != ".xlsx") {
            mensaje += "* El archivo IMEI no es válido.";
            mensaje += "<br/>";
        }
    }

    return mensaje;
}



function fn_descargarManual() {
    var msg = "";
    var key = 0;
    var ifUsuario = $("#ifUsuario").val();
    $.ajax({
        type: "GET",
        url: URLSERVICIO_IMEI + "/Principal/ValidarDescargaManual",
        cache: false,
        data: { 'ifUsuario': ifUsuario },
        dataType: "json",
        success: function (data) {
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                window.location = URLSERVICIO_IMEI + '/Principal/DescargarManual';
            }
        }
    });
}