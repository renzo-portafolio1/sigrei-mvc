﻿$(function () {
    $("#txtFechaInicio").datepicker({ changeMonth: !0, changeYear: !0, showOn: "button", buttonImage: URLSERVICIO_IMEI + "/images/calendar-32-xxl.png", buttonImageOnly: !0 });
    $("#txtFechaFin").datepicker({ changeMonth: !0, changeYear: !0, showOn: "button", buttonImage: URLSERVICIO_IMEI + "/images/calendar-32-xxl.png", buttonImageOnly: !0 });
    $("#txtFechaVencimiento").datepicker({ changeMonth: !0, changeYear: !0, showOn: "button", buttonImage: URLSERVICIO_IMEI + "/images/calendar-32-xxl.png", buttonImageOnly: !0 });

    cargaInicial();
    $("#btnBuscarSolicitudes").click(function () {
        fn_BuscarSolicitudes();
    });

    $("#btnLimpiarBandeja").click(function () {
        MostrarCargando();
        $("#ddlEmpresaOperadora").val("");
        $("#txtNroImei").val("");
        $("#ddlEstado").val("");
        $("#txtFechaInicio").val("");
        $("#txtFechaFin").val("");
        var divSolicitudes = $("#divSolicitudes");
        var formdata = $("#frmBandejaSolicitud").serialize();
        $.ajax({
            type: "POST",
            url: URLSERVICIO_IMEI + "/BandejaSolicitud/LimpiarCriteriosBandeja",
            cache: false,
            data: formdata,
            success: function (data) {
                divSolicitudes.html("");
                divSolicitudes.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                CerrarCargando();
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError("Error en obtener la información");
            },
            complete: function () {
                CerrarCargando();
            }
        });
    });

    $("#btnRenovarVencimiento").click(function () {        
        var mensaje = "";
        var ddlMotivo = $("#ddlMotivo").val();
        var txtFechaVencimiento = $("#txtFechaVencimiento").val();
        if (ddlMotivo === "") {
            mensaje += "Seleccione un motivo de ampliación";
            mensaje += "<br/>";
        }
        if (txtFechaVencimiento === "") {
            mensaje += "Ingrese la nueva fecha de vencimiento ";
            mensaje += "<br/>";
        }
        if (mensaje == "") {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false,
            })
            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: "¿Desea ampliar la fecha de vencimiento de la solicitud?",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    fn_AmpliarVencimientoPlazo();
                }
            });
        } else {
            mostrarMensajeError(mensaje);
        }

    });
});

function cargaInicial() {
    MostrarCargando();
    var divSolicitudes = $("#divSolicitudes");
    var formdata = $("#frmBandejaSolicitud").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/BandejaSolicitud/CargaInicial",
        cache: false,
        data: formdata,
        success: function (data) {
            divSolicitudes.html("");
            divSolicitudes.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

function fn_BuscarSolicitudes() {
    MostrarCargando();
    var divSolicitudes = $("#divSolicitudes");
    var formdata = $("#frmBandejaSolicitud").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/BandejaSolicitud/BuscarSolicitudes",
        cache: false,
        data: formdata,
        success: function (data) {
            divSolicitudes.html("");
            divSolicitudes.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

function fn_AmpliarVencimientoPlazo() {

    MostrarCargando();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmBandejaSolicitud")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/BandejaSolicitud/AmpliarVencimientoPlazo",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: "¡Mensaje del Sistema",
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        $("#modalApliacion").modal("hide");
                        fn_BuscarSolicitudes();
                    }
                });
            }
        }
    });
}
