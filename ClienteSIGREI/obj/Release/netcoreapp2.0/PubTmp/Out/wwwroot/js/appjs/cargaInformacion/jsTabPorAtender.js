﻿
$(document).ready(function () {


    $('#dtImeisPorAtender').DataTable({
        colReorder: true,
        //ordering: false,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },

    });

    $(".cerrar-modal-descarte").click(function () {
        $("#ihidImeiEO").val("");
        $("#ddlMotivo").val("");
        $("#comentariosDescarte").val("");
        $("#fileDocDescartar").val("");
        $("#modalDescartar").modal("hide");
    });

    $("#btnDescartarImei").click(function () {
        var valorMotivo = $("#ddlMotivo").val();
        var comentario = $("#comentariosDescarte").val();
        var mensaje = "";
        if (valorMotivo === "") { mensaje += "Seleccion el motivo <br/>"; }
        if (comentario === "") { mensaje += "Ingrese una observación<br/>"; }

        if (mensaje === "") {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false,
            });
            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: "¿Desea descartar el requerimiento de información de IMEI?",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    fn_descartarInformacionImei();
                }
            });
        } else {
            mostrarMensajeError(mensaje);
        }
    });

    $("#btnCargarArchivoImei").click(function () {
        var archivoImei = $("#fileArchivoImei").val();
        if (archivoImei == "") {
            mostrarMensajeError("Tiene que adjuntar el archivo excel.");
        } else {
            var extension = archivoImei.substring(archivoImei.lastIndexOf("."));
            if (extension != ".xlsx") {
                mostrarMensajeError("El archivo no es válido");
            } else {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-secondary'
                    },
                    buttonsStyling: false,
                });
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: "¿Desea cargar la información de IMEI del archivo adjunto?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        fn_validarCargarInformacion();
                    }
                });
            }
        }
    });

    $("#btnActualizarImei").click(function () {

        var cantidad = $("#icantidadactualizar").val();

        if (parseInt(cantidad) == 0) {
            mostrarMensajeError("No hay imeis que actualizar.");
        } else {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false,
            });
            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: "¿Desea actualizar la información de IMEI cargado?",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    fn_grabarInformacionCargado();
                }
            });
        }
    });
    var cantidad = $("#icantidadactualizar").val();
    if (cantidad === "") { $("#icantidadactualizar").val("0"); }
    if (cantidad == "0") {
        $("#btnActualizarImei").css("display", "none");
    } else {
        $("#btnActualizarImei").css("display", "initial");
    }
});

function fn_DescartarImei(idieo) {
    $("#ihidImeiEO").val();
    $("#ihidImeiEO").val(idieo);
    $("#modalDescartar").modal({ backdrop: 'static', keyboard: false });
}
function fn_validarCargarInformacion() {
    MostrarCargando();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    })
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmCargaInformacion")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/CargarInformacionExcel",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                parent.fn_RefrescarPorAtender();
            } else if (key == "2") {
                swalWithBootstrapButtons.fire({
                    title: 'Mensaje del Sistema',
                    text: msg,
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                    }
                });
            } else if (key == "3") {
                swalWithBootstrapButtons.fire({
                    title: 'Mensaje del Sistema',
                    text: msg,
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        $("#frmDescargarError").submit();
                    }
                });
            }

        }
    });
}

function fn_grabarInformacionCargado() {
    MostrarCargando();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    })
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmCargaInformacion")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/ActualizarInformacionImei",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: 'Mensaje del Sistema',
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        parent.fn_CargarModalCargaInfo();
                    }
                });
            }
        }
    });
}

function fn_descartarInformacionImei() {
    MostrarCargando();

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    })
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmCargaInformacion")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/DescartarInformacionImei",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                $("#modalDescartar").modal("hide");
                swalWithBootstrapButtons.fire({
                    title: 'Mensaje del Sistema',
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        parent.fn_CargarModalCargaInfo();
                    }
                });
            }
        }
    });
}

