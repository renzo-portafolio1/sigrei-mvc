﻿$(document).ready(function () {
    $('#dtDevolverImeis').DataTable({
        colReorder: true,
        ordering: false,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    });
});
$(function () {
    $("#hdimeiseleccion").val("");
    $("#chkAllDevolver").click(function () {
        var checked = $(this).is(':checked');
        $(".sidDevolver").prop('checked', checked);
        var valores = "";
        $(".sidDevolver").each(function () {
            var valor = $(this).attr('value');
            valores += valor + "-";
        });
        $("#hdimeiseleccion").val(valores);
    });

    $(".sidDevolver").click(function () {
        var valid = true;
        $("#hdimeiseleccion").val("");
        var valores = "";
        $(".sidDevolver").each(function () {
            var checked = $(this).is(':checked');
            if (!checked) {
                valid = false;
            } else {
                var valor = $(this).attr('value');
                valores += valor + "-";
            }
        });
        $("#hdimeiseleccion").val(valores);
        $("#chkAllDevolver").prop('checked', valid);
    });

    $("#btnAgregarImei").click(function () {        
        $("#NroImeiAgregar").val("");
        $("#modalAgregarImei").modal({ backdrop: 'static', keyboard: false });
    });

    $("#btnAgregarImeiPopup").click(function () {
        var mensaje = "";

        var NroImeiAgregar = $("#NroImeiAgregar").val();

        if (NroImeiAgregar === "") { mensaje = "Ingrese el número de imei"; }
        else if (NroImeiAgregar.length != 15) {
            mensaje = "Ingrese el número de imei debe tener 15 dígitos";
        }


        if (mensaje === "") {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false,
            });

            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: '¿Está seguro de agregar este número imei?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    BloquearPantalla();                    
                    var msg = "";
                    var key = 0;
                    var formdata = new FormData($("#frmBandejaAsignar")[0]);
                    $.ajax({
                        type: "POST",
                        url: URLSERVICIO_IMEI + "/Asignar/RegistrarImeiIndividual",
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        data: formdata,
                        success: function (data) {                            
                            key = data.key;
                            msg = data.value;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                            mostrarMensajeError(msg);
                            $.hideLoading();
                        },
                        complete: function () {

                            DesbloquearPantalla();
                            if (key == "0") {
                                mostrarMensajeError(msg);
                            } else if (key == "1") {
                                swalWithBootstrapButtons.fire({
                                    title: MSJ_MENSAJE_SISTEMA,
                                    text: msg,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Aceptar',
                                    cancelButtonText: 'Cancelar',
                                    reverseButtons: false
                                }).then(function (result) {
                                    if (result.value) {
                                        $("#modalAgregarImei").modal("hide");
                                        $("#modalAnalizarImei").modal("hide");                                        
                                        parent.buscarRequerimientos();
                                    }
                                });
                            }
                        }
                    });
                }
            });

        } else {
            mostrarMensajeError(mensaje);
        }
    });


    $("#btnDevolverImei").click(function () {
        $("#comentariosDevolver").val("");
        $("#fileDocDevolver").val("");
        var imeiSeleccionado = $("#hdimeiseleccion").val();
        if (imeiSeleccionado === "") {
            mostrarMensajeError("Debe de seleccionar al menos un IMEI");
        } else {
            $("#modalDevolverImei").modal({ backdrop: 'static', keyboard: false });
        }
    });
    $("#btnDevolver").click(function () {
        var mensaje = "";        
        var fileDocDevolver = $("#fileDocDevolver").val();
        var comentariosDevolver = $("#comentariosDevolver").val();        
        if (comentariosDevolver === "") {
            mensaje += "Agregar un comentario de devolución";
            mensaje += "<br/>";
        }
        if (fileDocDevolver == "") {
            mensaje += "Tiene que agregar un documento .pdf";
            mensaje += "<br/>";
        } else {
            var extension = fileDocDevolver.substring(fileDocDevolver.lastIndexOf("."));
            if (extension != ".pdf") {
                mensaje += "* El archivo  .pdf no es válido.";
                mensaje += "<br/>";
            }
        }
        if (mensaje === "") {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false
            });
            var msg = "";
            var key = 0;
            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: '¿Está seguro de devolver?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    var formdata = new FormData($("#frmBandejaAsignar")[0]);
                    $.ajax({
                        type: "POST",
                        url: URLSERVICIO_IMEI + "/Asignar/DevolverImeiRequerimiento",
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        data: formdata,
                        success: function (data) {                            
                            key = data.key;
                            msg = data.value;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                            mostrarMensajeError(msg);
                        },
                        complete: function () {
                            if (key == "0") {
                                mostrarMensajeError(msg);
                            } else if (key == "1") {
                                swalWithBootstrapButtons.fire({
                                    title: MSJ_MENSAJE_SISTEMA,
                                    text: msg,
                                    type: 'success',
                                    showCancelButton: false,
                                    confirmButtonText: 'Aceptar',
                                    cancelButtonText: 'Cancelar',
                                    reverseButtons: false
                                }).then(function (result) {
                                    if (result.value) {
                                        $("#modalDevolverImei").modal("hide");
                                        $("#modalAnalizarImei").modal("hide");
                                    }
                                });
                            }
                        }
                    });
                }
            });
        } else {
            mostrarMensajeError(mensaje);
        }
    });

    $(".btnCancelarDevolver").click(function () {
        $("#modalDevolverImei").modal("hide");
    });
    $(".btnCancelarAgregar").click(function () {
        $("#modalAgregarImei").modal("hide");
    });

    $("#btnDescargarDocumento").click(function () {
        var msg = "";
        var key = 0;
        var formdata = $("#frmBandejaAsignar").serialize();
        var imeiSeleccionado = $("#hdimeiseleccion").val();

        if (imeiSeleccionado === "") {
            mostrarMensajeError("Debe de seleccionar al menos un IMEI");
        } else {
            $.ajax({
                type: "POST",
                url: URLSERVICIO_IMEI + "/Asignar/DescargarDocumentoImei",
                cache: false,
                data: formdata,
                dataType: "json",
                success: function (data) {                    
                    key = data.key;
                    msg = data.value;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                    mostrarMensajeError(msg);
                },
                complete: function () {
                    if (key == "0") {
                        mostrarMensajeError(msg);
                    } else if (key == "1") {
                        window.location = URLSERVICIO_IMEI + '/Asignar/DescargarDocumentoTemporal';
                    }
                }
            });
        }

    });
});