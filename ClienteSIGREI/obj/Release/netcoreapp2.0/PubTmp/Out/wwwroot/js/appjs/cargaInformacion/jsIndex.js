﻿$(function () {

    $("#txtFechaInicio").datepicker({ changeMonth: !0, changeYear: !0, showOn: "button", buttonImage: URLSERVICIO_IMEI + "/images/calendar-32-xxl.png", buttonImageOnly: !0 });
    $("#txtFechaFin").datepicker({ changeMonth: !0, changeYear: !0, showOn: "button", buttonImage: URLSERVICIO_IMEI + "/images/calendar-32-xxl.png", buttonImageOnly: !0 });

    $("#btnBuscarSolicitudes").click(function () {
        MostrarCargando();
        var divSolicitudes = $("#divSolicitudes");
        var formdata = $("#frmCargaInformacion").serialize();
        $.ajax({
            type: "POST",
            url: URLSERVICIO_IMEI + "/CargaInformacion/BucarSolicitudes",
            cache: false,
            data: formdata,
            success: function (data) {
                divSolicitudes.html("");
                divSolicitudes.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                CerrarCargando();
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError(msg);
            },
            complete: function () {
                CerrarCargando();
            }
        });
    });
    $("#btnLimpiarBandeja").click(function () {
        MostrarCargando();
        $("#txtNroImei").val("");
        $("#ddlEstado").val("");
        $("#txtFechaInicio").val("");
        $("#txtFechaFin").val("");
        var divSolicitudes = $("#divSolicitudes");
        var formdata = $("#frmCargaInformacion").serialize();
        $.ajax({
            type: "POST",
            url: URLSERVICIO_IMEI + "/CargaInformacion/LimpiarCriteriosBandeja",
            cache: false,
            data: formdata,
            success: function (data) {
                divSolicitudes.html("");
                divSolicitudes.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                CerrarCargando();
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError(msg);
            },
            complete: function () {
                CerrarCargando();
            }
        });
    });

    $("#btnCerrarModalCargarInfo").click(function () {
        $("#modalCargaInfo").modal("hide");
        BuscarSolicitudes();
    });
    var idEstado = $("#ddlEstado").val();
    if (idEstado === "") {
        $("#ddlEstado").val("1");
    };
    CargaInicial();
});


function BuscarSolicitudes() {
    MostrarCargando();
    var divSolicitudes = $("#divSolicitudes");
    var formdata = $("#frmCargaInformacion").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/BucarSolicitudes",
        cache: false,
        data: formdata,
        success: function (data) {
            divSolicitudes.html("");
            divSolicitudes.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

function CargaInicial() {
    var divSolicitudes = $("#divSolicitudes");
    var formdata = $("#frmCargaInformacion").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/CargaInicial",
        cache: false,
        data: formdata,
        success: function (data) {
            divSolicitudes.html("");
            divSolicitudes.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
        }
    });
}