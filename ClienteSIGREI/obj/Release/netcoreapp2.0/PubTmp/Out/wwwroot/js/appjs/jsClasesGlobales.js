﻿
var MSJ_MENSAJE_SISTEMA = "Mensaje del Sistema";

function validateEmail(sEmail) {
    var filter = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

function mostrarMensaje(mensaje, tipoMensaje, formulario) {
    if (tipoMensaje == 1) {

        //var unique_id = $.gritter.add({
        //    title: VJS_TITULO_ALERT,
        //    text: mensaje,
        //    image: aliasAplicativoGlobal + '/Imagenes/aviso.png',
        //    time: '3000',
        //    class_name: 'my-sticky-class'
        //});
    }

    if (tipoMensaje == 2) {
        swal({
            title: VJS_TITULO_ALERT_EXITO,
            html: true,
            text: mensaje,
            type: "success"
        },
            function () {
                if (formulario.length > 0) {
                    $("#" + formulario).submit();
                }
            });
    }
}

function mostrarMensajeError(mensaje) {
    $().toastmessage('showErrorToast', mensaje);
}

function BloquearPantalla() {
    $.showLoading({ imgLoading: URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif', body: true });
}
function DesbloquearPantalla() {
    $.hideLoading();
}

function MostrarCargando() {
    $('body').loading({
        stoppable: false,
        message: 'Body loading, another message...',
        theme: 'dark',
        overlay: $("#custom-overlay")       
    });
    //$.showLoading({ imgLoading: URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif', body: true });
}

function CerrarCargando() {
    $('body').loading('stop');
    //$.showLoading({ imgLoading: URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif', body: true });
}

function soloNumeros(event) {
    var regex = new RegExp("^[0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    } else {
        return true;
    }
}


function BuscandoInformacion() {
    $.blockUI({ message: '<div class="row"><div class="col-md-12 text-center"> <h4><img src=" ' + URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif" class="" /> </br> Buscando Información...</h4> </div></div>' });
}
function QuitarBloqueo() {
    $.unblockUI();
}