﻿
$(function () {
    CargarInicial();

    $("#btnBuscarRequerimiento").click(function () {
        buscarRequerimientos();
    });


    $("#btnLimpiarRequerimiento").click(function () {
        MostrarCargando();
        fn_LimpiarCriteriosBusqueda();
        var divGrilla = $("#divGrilla");
        var formdata = $("#frmBandejaAsignar").serialize();
        $.ajax({
            type: "GET",
            url: URLSERVICIO_IMEI + "/Asignar/BandejaInicial",
            cache: false,
            data: formdata,
            success: function (data) {
                divGrilla.html("");
                divGrilla.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                CerrarCargando();
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError("Error al ejecutar la función");
            },
            complete: function () {
                CerrarCargando();
            }
        });
    });


    $("#btnRechazarAsignacion").click(function () {
        var mensaje = "";
        var comentariosRechazo = $("#comentariosRechazo").val();
        if (comentariosRechazo == "") {
            mensaje += "* Ingrese un comentario de rechazo";
            mensaje += "\n";
        }
        if (mensaje == "") {
            var msg = " ¿Desea rechazar el requerimiento de información de IMEI?";
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false,
            });
            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: msg,
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    rechazarRequerimientoDoc();
                }
            });

        } else {
            var mensajeCabecera = "No se puede rechazar el requerimiento de información de IMEI por los siguientes motivos: <br/>";
            mensajeCabecera = mensajeCabecera + mensaje;
            mostrarMensajeError(mensaje);
        }
    });
    $("#btnDescargarOficio").click(function () {      
        var msg = "";
        var key = 0;        
        var nombreOficio = $("#ihDocumentoOficio").val();
        $.ajax({
            type: "GET",
            url: URLSERVICIO_IMEI + "/Asignar/ValidarDocumentoSIGREI",
            cache: false,
            data: { 'nombreDoc': nombreOficio },
            dataType: "json",
            success: function (data) {                
                key = data.key;
                msg = data.value;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError(msg);
            },
            complete: function () {
                if (key == "0") {
                    mostrarMensajeError(msg);
                } else if (key == "1") {
                    window.location = URLSERVICIO_IMEI + '/Asignar/DescargarDocumento';
                }
            }
        });

    });
    $("#btnDescargarImei").click(function () {
        var msg = "";
        var key = 0;        
        var nombreIMEI = $("#ihDocumentoIMEI").val();
        $.ajax({
            type: "GET",
            url: URLSERVICIO_IMEI + "/Asignar/ValidarDocumentoSIGREI",
            cache: false,
            data: { 'nombreDoc': nombreIMEI },
            dataType: "json",
            success: function (data) {                
                key = data.key;
                msg = data.value;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError(msg);
            },
            complete: function () {
                if (key == "0") {
                    mostrarMensajeError(msg);
                } else if (key == "1") {
                    window.location = URLSERVICIO_IMEI + '/Asignar/DescargarDocumento';
                }
            }
        });

    });

    $("#btnAsignarRequerimiento").click(function () {        
        var hdreqeele = $("#hdreqeele").val();
        if (hdreqeele === "" || hdreqeele == null) {
            mensaje = "¡Debe de realizar la búsqueda y selecionar al menos un requerimiento para poder asignar!";
            mostrarMensajeError(mensaje);
        } else {
            AsignarRequerimientoVarios();
        }
    });
});


function fn_LimpiarCriteriosBusqueda() {
    $("#ddlTipoSolicitante").val("");
    $("#txtNombreInstitucion").val("");
    $("#txtNombreSolicitante").val("");
    $("#txtNroOficio").val("");
    $("#txtNroRequerimientoI").val("");
    $("#txtNroRequerimientoF").val("");
    $("#txtNroImei").val("");
}

function rechazarRequerimientoDoc() {
    var msg = "";
    var key = 0;
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/RechazarRequerimiento",
        cache: false,
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-secondary' 
                    },
                    buttonsStyling: false,
                });
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,//'
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        CerrarRechazo();
                    }
                });
            }

        }
    });

}

function buscarRequerimientos() {
    $.showLoading({ imgLoading: URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif', body: true });
    var divGrilla = $("#divGrilla");
    var msg = "";
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/BuscarRequerimientos",
        cache: false,
        data: formdata,
        success: function (data) {
            divGrilla.html("");
            divGrilla.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.hideLoading();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            $.hideLoading();
        }
    });
}

function CargarInicial() {
    //alert('fdfdfdfdf');
    var divGrilla = $("#divGrilla");
    var msg = "";
    var key = 0;
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "GET",
        url: URLSERVICIO_IMEI + "/Asignar/BandejaInicial",
        cache: false,
        data: formdata,
        success: function (data) {            
            divGrilla.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError("Error");
        },
        complete: function () {

        }
    });
}
function mostrarImeiAsignados(id) {
    
    $("#hdimei").val("");
    $("#hdimei").val(id);
    $("#modalConsulta").modal({ backdrop: 'static', keyboard: false });

    var divGrillaImei = $("#divGrillaImei");
    var msg = "";
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/ConsultarImeiAsignados",
        cache: false,
        data: formdata,
        success: function (data) {
            divGrillaImei.html("");
            divGrillaImei.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {

        }
    });
}
function verRechazar(id) {    
    $("#comentariosRechazo").val("");
    $("#hdimei").val("");
    $("#hdimei").val(id);
    $("#modalRechazar").modal({ backdrop: 'static', keyboard: false });
}
function Asignar(id) {
    
    $("#hdimei").val("");
    $("#hdimei").val(id);
    $("#modalAsignar").modal({ backdrop: 'static', keyboard: false });

    var divPartialAsignar = $("#divPartialAsignar");
    var msg = "";
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/AbrirAsignarRequerimiento",
        cache: false,
        data: formdata,
        success: function (data) {
            divPartialAsignar.html("");
            divPartialAsignar.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {

        }
    });
}

function VerAnalizarImei(id) {
    $.showLoading({ imgLoading: URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif', body: true });
    
    $("#hdimei").val("");
    $("#hdimei").val(id);
    $("#modalAnalizarImei").modal({ backdrop: 'static', keyboard: false });

    var divAnalinarImei = $("#divAnalinarImei");
    var msg = "";
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/VerAnalizarImei",
        cache: false,
        data: formdata,
        success: function (data) {
            divAnalinarImei.html("");
            divAnalinarImei.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.hideLoading();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(jqXHR.responseText);
        },
        complete: function () {
            $.hideLoading();
        }
    });
}

function AsignarRequerimientoVarios() {
    BloquearPantalla();
    $("#modalAsignaRequerimiento").modal({ backdrop: 'static', keyboard: false });

    var divAsignar = $("#divAsignar");
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/VerAsignarRequerimientoVarios",
        cache: false,
        data: formdata,
        success: function (data) {
            divAsignar.html("");
            divAsignar.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            DesbloquearPantalla();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(jqXHR.responseText);
        },
        complete: function () {
            DesbloquearPantalla();
        }
    });
}
function VerDescargas(id) {
    
    $.showLoading({ imgLoading: URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif', body: true });
    $("#hdimei").val("");
    $("#hdimei").val(id);
    $("#modalDescargar").modal({ backdrop: 'static', keyboard: false });
    $("#ihDocumentoOficio").val("");
    $("#ihDocumentoIMEI").val("");
    var msg = "";
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/AbrirDescargaDocumentos",
        cache: false,
        data: formdata,
        dataType: 'json',
        success: function (data) {
            $("#ihDocumentoOficio").val(data.nombreArchivoAdjuntoOficio);
            $("#ihDocumentoIMEI").val(data.nombreArchivoAdjuntoIMEI);            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            $.hideLoading();
        }
    });
}
function verRequermientoImei(id) {
    
    $("#hdimei").val("");
    $("#hdimei").val(id);
    $("#modalConsultaRequerimiento").modal({ backdrop: 'static', keyboard: false });

    var divConsultaRequerimiento = $("#divConsultaRequerimiento");
    var msg = "";
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/ConsultaRequerimiento",
        cache: false,
        data: formdata,
        success: function (data) {
            divConsultaRequerimiento.html("");
            divConsultaRequerimiento.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {

        }
    });
}
function CerrarAsignar() {
    $("#modalAsignar").modal("hide");
    buscarRequerimientos();
}
function CerrarAsignarVarios() {
    $("#modalAsignaRequerimiento").modal("hide");
    buscarRequerimientos();
}
function CerrarRechazo() {
    $("#modalRechazar").modal("hide");
    $("#comentariosRechazo").val("");
    buscarRequerimientos();
}
