﻿        
        function showFadeConfirmacion(mensaje) {
            $().toastmessage('showSuccessToast', mensaje);
        }
        function showStickySuccessToast(mensaje) {
            $().toastmessage('showToast', {
                text: mensaje,
                sticky: true,
                stayTime:300,
                position: 'top-right',
                type: 'success',
                closeText: '',
                close: function () {
                }
            });
 
        }
        function showFadeNotificacion(mensaje) {
            $().toastmessage('showNoticeToast', mensaje);
        }
        function showStickyNoticeToast() {
            $().toastmessage('showToast', {
                text: 'Notice Dialog which is sticky',
                sticky: true,
                position: 'top-left',
                type: 'notice',
                closeText: '',
                close: function () { console.log("toast is closed ..."); }
            });
        }
        function showFadeValidacion(mensaje) {
            $().toastmessage('showWarningToast', mensaje);
        }
        function showStickyWarningToast() {
            $().toastmessage('showToast', {
                text: 'Warning Dialog which is sticky',
                sticky: true,
                position: 'middle-right',
                type: 'warning',
                closeText: '',
                close: function () {
                    console.log("toast is closed ...");
                }
            });
        }
        function showFadeError(mensaje) {
            $().toastmessage('showErrorToast', mensaje);
        }
        function showStickyErrorToast() {
            $().toastmessage('showToast', {
                text: 'Error Dialog which is sticky',
                sticky: true,
                position: 'center',
                type: 'error',
                closeText: '',
                close: function () {
                    console.log("toast is closed ...");
                }
            });
        }