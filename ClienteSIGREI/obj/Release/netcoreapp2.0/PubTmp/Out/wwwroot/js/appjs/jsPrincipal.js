﻿$(function () {
    cargarMenu();
});
function cargarEvento() {
    $("#divMenu h3").click(function () {
        
        $("#divMenu ul ul").slideUp();
        if (!$(this).next().is(":visible")) {
            $(this).next().slideDown();
        }
    });
    $('ul.navbar-nav').responsiveCollapse();

   
}

function cargarMenu() {
    $.showLoading({ imgLoading: URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif', loadingClass: "fade" });
    var idDivMenu = $("#divMenu");
    var divMenuHead = $("#divMenuHead");
    $.ajax({
        type: "GET",
        url: URLSERVICIO_IMEI + "/Principal/CrearMenu",
        contentType: false,
        processData: false,
        dataType: "json",
        success: function (data) {
            idDivMenu.html(data);
            divMenuHead.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.hideLoading();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(errorThrown.toString());
        },
        complete: function () {
            cargarEvento();
            $.hideLoading();

        }
    });
}


function fn_descargarManual() {
    var msg = "";
    var key = 0;
    var ifUsuario = $("#ifUsuario").val();
    $.ajax({
        type: "GET",
        url: URLSERVICIO_IMEI + "/Principal/ValidarDescargaManual",
        cache: false,
        data: { 'ifUsuario': ifUsuario },
        dataType: "json",
        success: function (data) {
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                window.location = URLSERVICIO_IMEI + '/Principal/DescargarManual';
            }
        }
    });
}

function fn_descargarPlantillaBusqueda() {
    var msg = "";
    var key = 0;
    var ifUsuario = $("#ifUsuario").val();
    $.ajax({
        type: "GET",
        url: URLSERVICIO_IMEI + "/Principal/ValidarDescargaPlantillaBusqueda",
        cache: false,
        data: { 'ifUsuario': ifUsuario },
        dataType: "json",
        success: function (data) {
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                window.location = URLSERVICIO_IMEI + '/Principal/DescargarPlantillaBusqueda';
            }
        }
    });
}