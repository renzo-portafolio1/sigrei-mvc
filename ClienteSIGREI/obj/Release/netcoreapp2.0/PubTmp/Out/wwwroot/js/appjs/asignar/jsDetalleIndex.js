﻿$(document).ready(function () {
    $('#dtResultadoBusqueda').DataTable({
        colReorder: true,
        //ordering: false,
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true
    });
});


$(function () {
    $("#chkAll").click(function () {
        var checked = $(this).is(':checked');
        $(".sid").prop('checked', checked);      
        var valores = "";
        $(".sid").each(function () {            
            var valor = $(this).attr('value');
            valores += valor + "-";
        });
        $("#hdreqeele").val(valores);
    });

    $(".sid").click(function () {
        var valid = true;
        $("#hdreqeele").val("");
        var valores = "";        
        $(".sid").each(function () {            
            var checked = $(this).is(':checked');
            if (!checked) {
                valid = false;
            } else {
                var valor = $(this).attr('value');
                valores += valor + "-";
            }
        });
        $("#hdreqeele").val(valores);
        $("#chkAll").prop('checked', valid);
    });
});

function fn_verDetalleMovil(id) {
    $("#ihidrequeri").val("");
    $("#ihidrequeri").val(id);
    $("#modalConsultaMovil").modal({ backdrop: 'static', keyboard: false });
    BloquearPantalla();
    var divConsultaImei = $("#divConsultaMovil");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/ConsultaRequerimientoMovil",
        cache: false,
        data: formdata,
        success: function (data) {
            divConsultaImei.html("");
            divConsultaImei.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            DesbloquearPantalla();
        }
    });
}