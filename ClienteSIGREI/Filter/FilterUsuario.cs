﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;


namespace PPBMS.GLOBALTPA.ClienteWeb.Filter
{
    public class FilterUsuario : ActionFilterAttribute
    {
        //private object context;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            var idIdUsuario = filterContext.HttpContext.Session.GetString("IdUsuario");
           
            if (idIdUsuario == null)
            {
                var values = new
                {
                    controller = "Login",
                    action = "Index"
                };
                filterContext.Result = new RedirectToRouteResult("Default", new RouteValueDictionary(values));                
                return;
            }

        }
    }
}