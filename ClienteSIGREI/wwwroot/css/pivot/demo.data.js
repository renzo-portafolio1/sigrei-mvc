// Ensure orb.demo namespace is created
//orb.utils.ns('orb.demo');

(function() {

	var model = function(voter, party, precinct, ageGroup, lastVoted, yearsReg, ballotStatus) {
		this.voter = voter; 
		this.party = party;
		this.precinct = precinct;
		this.ageGroup = ageGroup;
		this.lastVoted = lastVoted;
		this.yearsReg = yearsReg;
		this.ballotStatus = ballotStatus;
	};

	window.demo = {};
	window.demo.data = [];

	var data = getData();
	for(var t=0;t<1;t++) {
		for(var j = 0;j < data.length; j++) {
			window.demo.data.push(data[j]);
		}
	}

	function getData() {
		return [
['MINISTERIO', 'CON SERVICIO', 'ENERO', 4],
['PNP', 'SIN SERVICIO', 'ENERO', 4],
['SOLI', 'CON SERVICIO', 'ENERO', 0],
['SOLI', 'SIN SERVICIO', 'ENERO', 9],
['SOLI', 'CON SERVICIO', 'FEBRERO', 4],
['PNP', 'CON SERVICIO', 'FEBRERO', 4],
['MINISTERIO', 'SIN SERVICIO', 'MARZO', 4]
		];
	}

}());