﻿$(function () {
    $("#btnGrabarAsignacionVarios").click(function () {
        fc_validarAsignar();
    });
});

function fc_validarAsignar() {
    var mensaje = "";
    var ddlUsuario = $("#ddlUsuarioVarios").val();

    if (ddlUsuario == "0" || ddlUsuario == "") {
        mensaje += "* Seleccione el Usuario";
        mensaje += "\n";
    }

    if (mensaje == "") {
        var msg = "¿Desea asignar el requerimiento de información de IMEI?";

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-secondary' 
            },
            buttonsStyling: false,
        })

        swalWithBootstrapButtons.fire({
            title: MSJ_MENSAJE_SISTEMA,
            text: msg,
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: false
        }).then(function (result) {
            if (result.value) {
                asignarRequerimientoDoc();
            }
        });

    } else {
        var mensajeCabecera = "No se puede asignar el requerimiento de información de IMEI por los siguientes motivos: <br/>";
        mensajeCabecera = mensajeCabecera + mensaje;
        mostrarMensajeError(mensaje);
    }
}

function asignarRequerimientoDoc() {
    var msg = "";
    var key = 0;
    var formdata = $("#frmBandejaAsignar").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/AsignarRequerimientoVarios",
        cache: false,
        data: formdata,
        success: function (data) {           
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-secondary' 
                    },
                    buttonsStyling: false,
                });
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,//'
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        parent.CerrarAsignarVarios();
                    }
                });
            }

        }
    });

}