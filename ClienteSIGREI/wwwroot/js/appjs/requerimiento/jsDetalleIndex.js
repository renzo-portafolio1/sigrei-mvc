﻿$(document).ready(function () {
    var oTable=  $('#dtResultadoBusqueda').DataTable({
        colReorder: true,      
        responsive: true,
        'columnDefs': [{
            'targets': [0, 1], 
            'orderable': false 
        }],        
         lengthMenu: [
            [10, 25, 50, 100],
            ['10 ', '25', '50', '100']
        ],      
        "lengthChange": false
    });
    $('#length_change').val(oTable.page.len());
    $('#length_change').change(function () {
        $("#chkAll").prop('checked', false);
        $("#hdreqeele").val("");
        $(".sid").prop('checked', false);
        oTable.page.len($(this).val()).draw();

    }); 

});


function fn_DevolverAsignado(idImei) {   
    BloquearPantalla();
    $("#ihidrequeri").val(idImei);
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary' 
        },
        buttonsStyling: false
    });
    swalWithBootstrapButtons.fire({
        title: MSJ_MENSAJE_SISTEMA,
        text: "¿Está seguro de devolver el requerimiento?",
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        reverseButtons: false
    }).then(function (result) {
        if (result.value) {
            DevolverImei();
        } else {
            DesbloquearPantalla();
        }
    });
}



function DevolverImei() {    
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary' 
        },
        buttonsStyling: false,
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmBandejaRequerimiento")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/DevolverRequerimiento",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
            DesbloquearPantalla();
        },
        complete: function () {
            DesbloquearPantalla();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        parent.buscarRequerimientos();
                    }
                });
            }
        }
    });
}

function fn_Finalizar(idImei) {
    
    BloquearPantalla();
    $("#ihidrequeri").val(idImei);
    $("#comentariosFinalizar").val("");    
    $("#modalFinalizar").modal({ backdrop: 'static', keyboard: false });
    DesbloquearPantalla();
}

function fn_verDetalleImei(id) {
    $("#ihidrequeri").val("");
    $("#ihidrequeri").val(id);
    $("#modalConsultaImei").modal({ backdrop: 'static', keyboard: false });
    BloquearPantalla();
    var divConsultaImei = $("#divConsultaImei");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/ConsultaRequerimientoImei",
        cache: false,
        data: formdata,
        success: function (data) {
            divConsultaImei.html("");
            divConsultaImei.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            DesbloquearPantalla();
        }
    });
}

function fn_VerSeguimiento(idreq) {

    MostrarCargando();
    $("#ihidrequeri").val("");
    $("#ihidrequeri").val(idreq);
    $("#modalSeguimiento").modal({ backdrop: 'static', keyboard: false });
    
    var divSeguimiento = $("#divSeguimiento");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/AbrirSeguimiento",
        cache: false,
        data: formdata,
        success: function (data) {
            divSeguimiento.html("");
            divSeguimiento.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            //CerrarCargando();
            fn_CargarTabRespondidas();
        }
    });
}

function fn_CargarTabRespondidas() {   
    MostrarCargando();
    var divRespondidas = $("#divRespondidas");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/verTabsRespondidas",
        cache: false,
        data: formdata,
        success: function (data) {
            divRespondidas.html("");
            divRespondidas.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

