﻿$(function () {
    $('.nav-tabs a').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('id');

        if (id == "tabRespondida") {
            $(".tabSeguimiento").html("");
            fn_RefrescarTabRespondidas();
        } else if (id == "tabPorResponder") {
            $(".tabSeguimiento").html("");
            fn_RefrescarPorResponder();            
        } else if (id == "tabEnviarEo") {
            $(".tabSeguimiento").html("");
            fn_RefrescarEnviadaEO();
        } else if (id == "tabDevolver") {
            $(".tabSeguimiento").html("");
            fn_RefrescarPorDevolverInstitucion();
        } else if (id == "tabServMovil") {
            $(".tabSeguimiento").html("");
            fn_verDetalleMovil();
        }
    });

    //var irTabPorAtender = $("#irTabPorAtender").val();
    //if (irTabPorAtender === "1") {
    //    $('.nav-tabs a').tab('show');
    //    fn_RefrescarPorAtenderNew();

    //}
});

// tab 1
function fn_RefrescarTabRespondidas() {
    MostrarCargando();

    var divRespondidas = $("#divRespondidas");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/verTabsRespondidas",
        cache: false,
        data: formdata,
        success: function (data) {
            divRespondidas.html("");
            divRespondidas.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}
// tab 2
function fn_RefrescarPorResponder() {
    MostrarCargando();
    var divPorResponder = $("#divPorResponder");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/verTabsPorResponder",
        cache: false,
        data: formdata,
        success: function (data) {
            divPorResponder.html("");
            divPorResponder.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

// tab 3
function fn_RefrescarEnviadaEO() {
    MostrarCargando();
    var divEnviadaEO = $("#divEnviadaEO");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/verTabsEnviadaEO",
        cache: false,
        data: formdata,
        success: function (data) {
            divEnviadaEO.html("");
            divEnviadaEO.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

// tab 4
function fn_RefrescarPorDevolverInstitucion() {
    MostrarCargando();
    var divDevolverInstitucion = $("#divDevolverInstitucion");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/verTabsDevolverInstitucion",
        cache: false,
        data: formdata,
        success: function (data) {
            divDevolverInstitucion.html("");
            divDevolverInstitucion.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

//tab 5

function fn_verDetalleMovil(id) {
    //$("#ihidrequeri").val("");
    //$("#ihidrequeri").val(id);
    //$("#modalConsultaMovil").modal({ backdrop: 'static', keyboard: false });
    //BloquearPantalla();
    MostrarCargando();
    var divConsultaImei = $("#divServMovil");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/ConsultaRequerimientoMovil",
        cache: false,
        data: formdata,
        success: function (data) {
            divConsultaImei.html("");
            divConsultaImei.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

function fn_DescargarArchivoImei(nomArchivo) {
    var msg = "";
    var key = 0;
    $.ajax({
        type: "GET",
        url: URLSERVICIO_IMEI + "/Asignar/ValidarDocumentoSIGREI",
        cache: false,
        data: { 'nombreDoc': nomArchivo },
        dataType: "json",
        success: function (data) {           
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                window.location = URLSERVICIO_IMEI + '/Asignar/DescargarDocumento';
            }
        }
    });
}

//function fn_RefrescarPorAtenderNew() {

//    MostrarCargando();
//    var divAtendidas = $("#divPorAtender");
//    var msg = "";
//    var formdata = $("#frmCargaInformacion").serialize();

//    $.ajax({
//        type: "POST",
//        url: URLSERVICIO_IMEI + "/CargaInformacion/verTabsPorAtenderNew",
//        cache: false,
//        data: formdata,
//        success: function (data) {
//            divAtendidas.html("");
//            divAtendidas.html(data);
//        },
//        error: function (jqXHR, textStatus, errorThrown) {
//            CerrarCargando();
//            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
//            mostrarMensajeError(msg);
//        },
//        complete: function () {
//            CerrarCargando();
//        }
//    });
//}