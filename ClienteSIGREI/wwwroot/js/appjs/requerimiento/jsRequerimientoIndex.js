﻿
$(function () {
    CargarInicial();

    $("#txtFechaIngresoHasta").datepicker({ changeMonth: !0, changeYear: !0, showOn: "button", buttonImage: URLSERVICIO_IMEI + "/images/calendar-32-xxl.png", buttonImageOnly: !0 });
    $("#txtFechaIngresoInicio").datepicker({ changeMonth: !0, changeYear: !0, showOn: "button", buttonImage: URLSERVICIO_IMEI + "/images/calendar-32-xxl.png", buttonImageOnly: !0 });

    $("#btnBuscarRequerimiento").click(function () {
        buscarRequerimientos();
    });

    $("#btnCerrarModalSeguimiento").click(function () {
        $("#ihidrequeri").val("");        
        $("#modalSeguimiento").modal("hide");
        buscarRequerimientos();
    });    

    $("#btnLimpiarRequerimiento").click(function () {
        MostrarCargando();
        fn_LimpiarCriterios();

        var divSolicitudes = $("#divSolicitudes");
        var formdata = $("#frmBandejaSolicitud").serialize();
        $.ajax({
            type: "POST",
            url: URLSERVICIO_IMEI + "/Requerimiento/LimpiarBandejaRequerimientos",
            cache: false,
            data: formdata,
            success: function (data) {
                divSolicitudes.html("");
                divSolicitudes.html(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                CerrarCargando();
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError("Error en obtener la información");
            },
            complete: function () {
                CerrarCargando();
            }
        });
    });

    $("#btnAnalizarRequerimiento").click(function () {

        var hdreqeele = $("#hdreqeele").val();
        if (hdreqeele === "" || hdreqeele == null) {
            mensaje = "¡Debe de realizar la búsqueda y selecionar al menos un requerimiento para poder analizar!";
            mostrarMensajeError(mensaje);
        } else {
            irAnalizar(hdreqeele);
        }
    });

    $("#btnFinalizarRequerimiento").click(function () {

        var hdreqeele = $("#comentariosFinalizar").val();
        if (hdreqeele === "" || hdreqeele == null) {
            mensaje = "¡Ingrese un comentario para finalizar el requerimiento!";
            mostrarMensajeError(mensaje);
        } else {
            BloquearPantalla();
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary' 
                },
                buttonsStyling: false
            });
            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: "¿Está seguro de finalizar el requerimiento?",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    DesbloquearPantalla();
                    finalizarRequerimiento();
                }
            });
        }
    });

    $("#btnDescargarOficio").click(function () {
        var msg = "";
        var key = 0;
        var nombreOficio = $("#ihDocumentoOficio").val();
        $.ajax({
            type: "GET",
            url: URLSERVICIO_IMEI + "/Asignar/ValidarDocumentoSIGREI",
            cache: false,
            data: { 'nombreDoc': nombreOficio },
            dataType: "json",
            success: function (data) {                
                key = data.key;
                msg = data.value;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError(msg);
            },
            complete: function () {
                if (key == "0") {
                    mostrarMensajeError(msg);
                } else if (key == "1") {
                    window.location = URLSERVICIO_IMEI + '/Asignar/DescargarDocumento';
                }
            }
        });

    });
    $("#btnDescargarImei").click(function () {

        var msg = "";
        var key = 0;

        var nombreIMEI = $("#ihDocumentoIMEI").val();
        $.ajax({
            type: "GET",
            url: URLSERVICIO_IMEI + "/Asignar/ValidarDocumentoSIGREI",
            cache: false,
            data: { 'nombreDoc': nombreIMEI },
            dataType: "json",
            success: function (data) {                
                key = data.key;
                msg = data.value;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
                mostrarMensajeError(msg);
            },
            complete: function () {
                if (key == "0") {
                    mostrarMensajeError(msg);
                } else if (key == "1") {
                    window.location = URLSERVICIO_IMEI + '/Asignar/DescargarDocumento';
                }
            }
        });

    });



    var idEstado = $("#ddlEstado").val();
    if (idEstado === "") {
        $("#ddlEstado").val("9");
    };


});


function fn_LimpiarCriterios() {

    $("#ddlTipoSolicitante").val("");
    $("#txtNombreInstitucion").val("");
    $("#txtNombreSolicitante").val("");
    $("#txtNroOficio").val("");
    $("#txtNroRequerimientoI").val("");
    $("#txtNroRequerimientoF").val("");
    $("#txtFechaIngresoInicio").val("");
    $("#txtFechaIngresoHasta").val("");
    $("#ddlEstado").val("");
    $("#ddlUsuarioAsignado").val("");
    $("#txtNroImei").val("");
}

function CargarInicial() {    
    var divGrilla = $("#divGrilla");
    var msg = "";    
    var formdata = $("#frmBandejaRequerimiento").serialize();
    $.ajax({
        type: "GET",
        url: URLSERVICIO_IMEI + "/Requerimiento/BandejaInicial",
        cache: false,
        data: formdata,
        success: function (data) {
            divGrilla.html("");
            divGrilla.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {            
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
        }
    });
}

function buscarRequerimientos() {
    BloquearPantalla();
    var divGrilla = $("#divGrilla");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/BuscarRequerimientos",
        cache: false,
        data: formdata,
        success: function (data) {
            divGrilla.html("");
            divGrilla.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            DesbloquearPantalla();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            DesbloquearPantalla();
        }
    });
}

function VerDescargas(id) {

    $.showLoading({ imgLoading: URLSERVICIO_IMEI + '/images/osiptel/ajax-loader.gif', body: true });
    $("#hdimei").val("");
    $("#hdimei").val(id);
    $("#modalDescargar").modal({ backdrop: 'static', keyboard: false });
    $("#ihDocumentoOficio").val("");
    $("#ihDocumentoIMEI").val("");
    var msg = "";
    var formdata = $("#frmBandejaRequerimiento").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Asignar/AbrirDescargaDocumentos",
        cache: false,
        data: formdata,
        dataType: 'json',
        success: function (data) {
            $("#ihDocumentoOficio").val(data.nombreArchivoAdjuntoOficio);
            $("#ihDocumentoIMEI").val(data.nombreArchivoAdjuntoIMEI);            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            $.hideLoading();
        }
    });
}

function irAnalizar(ids) {
    BloquearPantalla();
    $("#ihimeianaliz").val("");
    $("#ihimeianaliz").val(ids);
    $("#frmIrAnalizar").submit();
}

function finalizarRequerimiento() {    
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary' 
        },
        buttonsStyling: false,
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmBandejaRequerimiento")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/Requerimiento/FinalizarRequerimiento",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);          
        },
        complete: function () {          
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        $("#modalFinalizar").modal("hide");
                        buscarRequerimientos();
                    }
                });
            }
        }
    });

}

