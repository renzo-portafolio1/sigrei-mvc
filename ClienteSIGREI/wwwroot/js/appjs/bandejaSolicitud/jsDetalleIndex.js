﻿$(document).ready(function () {
    $('#dtResultSolicitud').DataTable({
        colReorder: true,       
        rowReorder: {
            selector: 'td:nth-child(2)'
        }
    });
});


function fn_Descargar(idreo) {
    $("#ihrequerimientoeoDes").val("");
    $("#ihrequerimientoeoDes").val(idreo);
    $("#frmDescargarImei").submit();
}

function fn_verConsultar(idreo) {
    $("#ihrequerimientoeo").val("");
    $("#ihrequerimientoeo").val(idreo);
    $("#modalConsultaSolicitud").modal({ backdrop: 'static', keyboard: false });
    MostrarCargando();
    var divConsultaImei = $("#divConsultaImei");
    var msg = "";
    var formdata = $("#frmBandejaSolicitud").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/ConsultaRequerimientoSolicitud",
        cache: false,
        data: formdata,
        success: function (data) {
            divConsultaImei.html("");
            divConsultaImei.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

function fn_CargarModalCargaInfo() {
    MostrarCargando();
    var divCargaInformacion = $("#divCargaInformacion");
    var msg = "";
    var formdata = $("#frmCargaInformacion").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/AbrirCargaInfoSolicitud",
        cache: false,
        data: formdata,
        success: function (data) {
            divCargaInformacion.html("");
            divCargaInformacion.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {            
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

function fn_CambiarFechaVencimiento(idreo, fecvenci) {
    $("#ihrequerimientoeo").val("");
    $("#ihrequerimientoeo").val(idreo);
    $("#lblFechaEnviadaEO").html("");
    $("#lblFechaEnviadaEO").html(fecvenci);

    $("#ddlMotivo").val("");
    $("#txtFechaVencimiento").val("");
    $("#modalApliacion").modal({ backdrop: 'static', keyboard: false });
}


