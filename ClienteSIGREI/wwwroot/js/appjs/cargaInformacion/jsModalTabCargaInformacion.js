﻿$(function () {
    $('.nav-tabs a').click(function (e) {
        e.preventDefault();

        var id = $(this).attr('id');
        if (id == "tabAtendida") {
            $("#divPorAtender").html("");
            fn_RefrescarTabAtendidas();
        } else if (id == "tabPorAtender") {
            fn_RefrescarPorAtender();
            $("#divAtendidas").html("");
        }
    });

    var irTabPorAtender = $("#irTabPorAtender").val();
    if (irTabPorAtender === "1") {
        $('.nav-tabs a').tab('show');
        fn_RefrescarPorAtenderNew();

    }
});


function fn_RefrescarTabAtendidas() {
    MostrarCargando();

    var divPorAtender = $("#divAtendidas");
    var msg = "";
    var formdata = $("#frmCargaInformacion").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/verTabsAtendidas",
        cache: false,
        data: formdata,
        success: function (data) {
            divPorAtender.html("");
            divPorAtender.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

function fn_RefrescarPorAtender() {

    MostrarCargando();
    var divAtendidas = $("#divPorAtender");
    var msg = "";
    var formdata = $("#frmCargaInformacion").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/verTabsPorAtender",
        cache: false,
        data: formdata,
        success: function (data) {
            divAtendidas.html("");
            divAtendidas.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}

function fn_RefrescarPorAtenderNew() {

    MostrarCargando();
    var divAtendidas = $("#divPorAtender");
    var msg = "";
    var formdata = $("#frmCargaInformacion").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/verTabsPorAtenderNew",
        cache: false,
        data: formdata,
        success: function (data) {
            divAtendidas.html("");
            divAtendidas.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}