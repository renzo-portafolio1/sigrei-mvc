﻿
$(document).ready(function () {
    $('#dtResultSolicitud').DataTable({
        colReorder: true,
        //ordering: false,
        rowReorder: {
            selector: 'td:nth-child(2)'
        }

    });

});
function fn_Descargar(idreo) {
    $("#ihrequerimientoeoDes").val("");
    $("#ihrequerimientoeoDes").val(idreo);
    $("#frmDescargarImei").submit();
}

function fn_AtenderEO(idreo) {
    $("#ihrequerimientoeo").val("");
    $("#ihrequerimientoeo").val(idreo);
    MostrarCargando();
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmCargaInformacion")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/ConsultarIMEIPendientes",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
            CerrarCargando();
        },
        complete: function () {
            CerrarCargando();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: "¿Desea atender el requerimiento de información de IMEI?",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        AtenderSolicitudEO()
                    }
                });
            } else if (key === "2") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        
                    }
                });

            }
        }
    });




}
function AtenderSolicitudEO() {
    MostrarCargando();
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmCargaInformacion")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/AtenderRequerimientoEO",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {
            console.log(JSON.stringify(data));
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
            CerrarCargando();
        },
        complete: function () {
            CerrarCargando();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        parent.buscarRequerimientos();
                    }
                });
            }
        }
    });
}
function fn_verConsultar(idreo) {
    $("#ihrequerimientoeo").val("");
    $("#ihrequerimientoeo").val(idreo);
    $("#modalConsultaSolicitud").modal({ backdrop: 'static', keyboard: false });
    MostrarCargando();
    var divConsultaImei = $("#divConsultaImei");
    var msg = "";
    var formdata = $("#frmCargaInformacion").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/ConsultaRequerimientoSolicitud",
        cache: false,
        data: formdata,
        success: function (data) {
            divConsultaImei.html("");
            divConsultaImei.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}
function fn_CargarInformacion(idreo) {

    MostrarCargando();
    $("#ihrequerimientoeo").val("");
    $("#ihrequerimientoeo").val(idreo);
    $("#modalCargaInfo").modal({ backdrop: 'static', keyboard: false });
    //BloquearPantalla();
    var divCargaInformacion = $("#divCargaInformacion");
    var msg = "";
    var formdata = $("#frmCargaInformacion").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/AbrirCargaInfoSolicitud",
        cache: false,
        data: formdata,
        success: function (data) {
            divCargaInformacion.html("");
            divCargaInformacion.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            //CerrarCargando();
            fn_CargarTabAtendidos();
        }
    });
}
function fn_CargarTabAtendidos() {
    //BloquearPantalla();
    MostrarCargando();
    var divAtendidas = $("#divAtendidas");
    var msg = "";
    var formdata = $("#frmCargaInformacion").serialize();

    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/verTabsAtendidas",
        cache: false,
        data: formdata,
        success: function (data) {
            divAtendidas.html("");
            divAtendidas.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            CerrarCargando();
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();
        }
    });
}
function fn_CargarModalCargaInfo() {
    MostrarCargando();
    var divCargaInformacion = $("#divCargaInformacion");
    var msg = "";
    var formdata = $("#frmCargaInformacion").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/CargaInformacion/AbrirCargaInfoSolicitud",
        cache: false,
        data: formdata,
        success: function (data) {
            divCargaInformacion.html("");
            divCargaInformacion.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            CerrarCargando();

        }
    });
}


