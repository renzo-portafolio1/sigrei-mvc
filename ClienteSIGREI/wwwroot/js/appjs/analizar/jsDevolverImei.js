﻿$(function () {

    $("#btnGrabarDevolver").click(function () {
        var ddlMovito = $("#ddlMotivo").val();
        if (ddlMovito == "") {
            mostrarMensajeError("!Debe de seleccionar el motivo de devolución!")
        } else {

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-secondary'
                },
                buttonsStyling: false,
            });
            swalWithBootstrapButtons.fire({
                title: MSJ_MENSAJE_SISTEMA,
                text: "¿Está seguro de marcar para devolver a la Institución?",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                reverseButtons: false
            }).then(function (result) {
                if (result.value) {
                    fn_devolverImei();
                } else {
                    DesbloquearPantalla();
                }
            });
        }
    });
});

function fn_devolverImei() {
    BloquearPantalla();
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmAnalizarIMEI")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/AnalizarIMEI/DevolverImei",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
            DesbloquearPantalla();
        },
        complete: function () {
            DesbloquearPantalla();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        $("#modalDevolver").modal("hide");
                        parent.buscarRequerimientoInicial();
                    }
                });
            }
        }
    });
}