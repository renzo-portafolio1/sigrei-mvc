﻿$(function () {
    //BloquearPantalla();    
    buscarRequerimientoInicial();
    $("#btnCerrarAnalisis").click(function () {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-secondary'
            },
            buttonsStyling: false,
        });
        swalWithBootstrapButtons.fire({
            title: MSJ_MENSAJE_SISTEMA,
            text: "¿Está seguro de retornar a la bandeja de requerimiento?",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: false
        }).then(function (result) {
            if (result.value) {
                $("#frmRetornar").submit();
            } else {
                DesbloquearPantalla();
            }
        });
    });
});

function buscarRequerimientoInicial() {
    MostrarCargando();
    var divPartialAnalizar = $("#divPartialAnalizar");
    $("#divSeleccionEvento").html("");    
    var msg = "";
    var formdata = $("#frmAnalizarIMEI").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/AnalizarIMEI/CargarImei",
        cache: false,
        data: formdata,
        success: function (data) {
            divPartialAnalizar.html("");
            divPartialAnalizar.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError("Error al Analizar los IMEIs seleccionados.");
            CerrarCargando();
        },
        complete: function () {
            CerrarCargando();
        }
    });
}
