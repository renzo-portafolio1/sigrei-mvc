﻿$(document).ready(function () {
    $('#dtResultadoAnalizar').DataTable({
        colReorder: true,
        ordering: false,
        responsive: true
    });
});

function fn_VerRequermientoImei(idImei) {

    BloquearPantalla();
    $("#ihidimei").val(idImei);

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    swalWithBootstrapButtons.fire({
        title: MSJ_MENSAJE_SISTEMA,
        text: "¿Está seguro de marcar para enviar a la empresa operadora?",
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        reverseButtons: false
    }).then(function (result) {
        if (result.value) {
            enviarInfoEO();
        } else {
            DesbloquearPantalla();
        }
    });

}

function enviarInfoEO() {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmEnviarEO")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/AnalizarIMEI/EnviarEmpresaOperadora",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
            DesbloquearPantalla();
        },
        complete: function () {
            DesbloquearPantalla();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        parent.buscarRequerimientoInicial();
                    }
                });
            }
        }
    });
}

function fn_Deshacer(idImei) {

    BloquearPantalla();
    $("#ihidimei").val(idImei);

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    swalWithBootstrapButtons.fire({
        title: MSJ_MENSAJE_SISTEMA,
        text: "¿Está seguro de deshacer la acción en el registro seleccionado?",
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        reverseButtons: false
    }).then(function (result) {
        if (result.value) {
            deshacerAccionPrevia();
        } else {
            DesbloquearPantalla();
        }
    });
}

function deshacerAccionPrevia() {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmEnviarEO")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/AnalizarIMEI/DeshacerAccionPrevia",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
            DesbloquearPantalla();
        },
        complete: function () {
            DesbloquearPantalla();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        parent.buscarRequerimientoInicial();
                    }
                });
            }
        }
    });
}


function fn_ParaResponder(idImei) {

    BloquearPantalla();
    $("#ihidimei").val(idImei);

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    swalWithBootstrapButtons.fire({
        title: MSJ_MENSAJE_SISTEMA,
        text: "¿Está seguro de responder a la Institución?",
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        reverseButtons: false
    }).then(function (result) {
        if (result.value) {
            enviarAtender();
        } else {
            DesbloquearPantalla();
        }
    });
}

function enviarAtender() {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: false,
    });
    var msg = "";
    var key = 0;
    var formdata = new FormData($("#frmEnviarEO")[0]);
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/AnalizarIMEI/EnviarAtencionIMEI",
        contentType: false,
        processData: false,
        dataType: "json",
        data: formdata,
        success: function (data) {            
            key = data.key;
            msg = data.value;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
            DesbloquearPantalla();
        },
        complete: function () {
            DesbloquearPantalla();
            if (key == "0") {
                mostrarMensajeError(msg);
            } else if (key == "1") {
                swalWithBootstrapButtons.fire({
                    title: MSJ_MENSAJE_SISTEMA,
                    text: msg,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    reverseButtons: false
                }).then(function (result) {
                    if (result.value) {
                        parent.buscarRequerimientoInicial();
                    }
                });
            }
        }
    });
}

function fn_ParaDevolver(idImei) {
    BloquearPantalla();
    $("#ihidimeiDevolver").val("");
    $("#ihidimeiDevolver").val(idImei);
    $("#modalDevolver").modal({ backdrop: 'static', keyboard: false });

    var divDevolverImei = $("#divDevolverImei");
    var msg = "";
    var formdata = $("#frmEnviarEO").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/AnalizarIMEI/verDevolverImei",
        cache: false,
        data: formdata,
        success: function (data) {
            divDevolverImei.html("");
            divDevolverImei.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            DesbloquearPantalla()
        }
    });
}


function fn_verEventos(id, idimei) {

    BloquearPantalla();
    $("#ihni").val("");
    $("#ihni").val(id);
    $("#ihidimeiban").val(idimei);
    var divSeleccionEvento = $("#divSeleccionEvento");
    var msg = "";
    var formdata = $("#frmAnalizarIMEI").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/AnalizarIMEI/verSeleccionEvento",
        cache: false,
        data: formdata,
        success: function (data) {
            divSeleccionEvento.html("");
            divSeleccionEvento.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            DesbloquearPantalla();
            $("#modalSeleccionarEvento").modal({ backdrop: 'static', keyboard: false });
        }
    });   
}


function fn_RefrescarEventos() {

    //BloquearPantalla();
  
    var divSeleccionEvento = $("#divSeleccionEvento");
    var msg = "";
    var formdata = $("#frmAnalizarIMEI").serialize();
    $.ajax({
        type: "POST",
        url: URLSERVICIO_IMEI + "/AnalizarIMEI/refrescarEventosSeleccionado",
        cache: false,
        data: formdata,
        success: function (data) {
            divSeleccionEvento.html("");
            divSeleccionEvento.html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error: " + errorThrown + " status: " + textStatus + " er:" + jqXHR.responseText);
            mostrarMensajeError(msg);
        },
        complete: function () {
            //DesbloquearPantalla();            
        }
    });
}
