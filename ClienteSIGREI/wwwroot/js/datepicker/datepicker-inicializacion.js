$(function () {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    //if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer, return version number
    //{
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi\u00E9rcoles', 'Jueves', 'Viernes', 'S\u00E1bado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mi\u00E9', 'Juv', 'Vie', 'S\u00E1b'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S\u00E1'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);

    $(".datepicker").datepicker({
        format: "dd/mm/yy",
        autoclose: true,
        showOn: "button",
        buttonText: '<i class="ui-icon ui-icon-calendar"></i>'
    });
    //$(".datepicker").datepicker();
    //$(".datepicker").addClass("ui-icon ui-icon-calendar");

    $(".datepicker").each(function () {
        var xx = $(this).val();
        if (xx.length >= 10) {
            var dArr = xx.split("-");  // ex input "2010-01-18"
            var d = dArr[2] + "/" + dArr[1] + "/" + dArr[0]; //ex out: "18/01/10"
            $(this).val(d);
        }
    });

    $(".horaminutos").datepicker({
        timeFormat: "hh:mm"
    });
    
    //}

});
