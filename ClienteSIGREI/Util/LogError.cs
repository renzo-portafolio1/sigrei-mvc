﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace ClienteSIGREI.Util
{
    public class LogError
    {

        //private readonly IConfiguration configuration;
        //private readonly IHostingEnvironment hosting;

        public static IConfiguration Configuration { get; set; }

        //public LogError(IConfiguration config, IHostingEnvironment host)
        //{
        //    this.configuration = config;
        //    this.hosting = host;
        //}

        public static void RegistrarError(Exception ex)
        {

            var builder = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            String rutaDocumentosPlantilla = Configuration["RutaLog"];


            using (System.IO.StreamWriter file = new System.IO.StreamWriter(rutaDocumentosPlantilla, true))
            {
                file.WriteLine("-----           -INICIO DE ERROR          ----");
                file.WriteLine("Empresa                  : P & P BMS");
                file.WriteLine("Elaborado                : Programador");
                file.WriteLine("Fecha                    : " + DateTime.Now.ToLongDateString()); // Escribe la Traza
                file.WriteLine("Hora de incidencia       : " + DateTime.Now.ToString("hh:mm:ss").ToString()); // Escribe la Traza

                file.WriteLine("Mensaje de Error         : " + ex.Message);// escribe el mensaje
                file.WriteLine("HelpLink                 : " + ex.HelpLink); // Escribe la Traza
                file.WriteLine("Source                   : " + ex.Source); // Escribe la Traza
                file.WriteLine("StackTrace               : " + ex.StackTrace); // Escribe la Traza
                file.WriteLine("TargetSite               : " + ex.TargetSite.ToString()); // Escribe la Traza
                file.WriteLine("-----           - FIN DE ERROR           ----");
            }

        }

        public static void RegistrarErrorMetodo(Exception ex, String metodo)
        {
            var builder = new ConfigurationBuilder()
                  .SetBasePath(Directory.GetCurrentDirectory())
                  .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            String rutaDocumentosPlantilla = Configuration["RutaLog"];
            if (ex != null)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(rutaDocumentosPlantilla, true))
                {

                    file.WriteLine("-----           -INICIO DE ERROR          ----");
                    file.WriteLine("Empresa                  : P & P BMS");
                    file.WriteLine("Elaborado                : Programador");
                    file.WriteLine("Fecha                    : " + DateTime.Now.ToLongDateString()); // Escribe la Traza
                    file.WriteLine("Hora de incidencia       : " + DateTime.Now.ToString("hh:mm:ss").ToString()); // Escribe la Traza
                    file.WriteLine("Metodo que genera el Error         :   " + metodo);
                    file.WriteLine("Mensaje de Error         :   " + ex.Message);// escribe el mensaje
                    file.WriteLine(ex.HelpLink); // Escribe la Traza
                    file.WriteLine(ex.Source); // Escribe la Traza
                    file.WriteLine(ex.StackTrace); // Escribe la Traza
                    file.WriteLine(ex.TargetSite.ToString()); // Escribe la Traza

                    file.WriteLine("-----           - FIN DE ERROR           ----");

                }
            }
            else
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(rutaDocumentosPlantilla, true))
                {
                    file.WriteLine("------INICIO DE PROCESO         ----");
                    file.WriteLine("Empresa                  : P & P BMS");
                    file.WriteLine("Elaborado                : Programador");
                    file.WriteLine("Fecha                    : " + DateTime.Now.ToLongDateString()); // Escribe la Traza
                    file.WriteLine("Hora de incidencia       : " + DateTime.Now.ToString("hh:mm:ss").ToString()); // Escribe la Traza

                    file.WriteLine("Metodo=> Se esta ejecuntando ==> "+ metodo); 
                    file.WriteLine("-----           - FIN DE PROCESO           ----");
                }
            }
        }


    }
}