﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ClienteSIGREI.Util
{
    public class Constantes
    {
        public byte[] IVAES256 = new byte[] { 234, 221, 241, 238, 230, 155, 39, 192, 10, 95, 161, 242, 181, 172, 138, 176 };

        public byte[] Key256 = new byte[] { 46, 199, 212, 126, 67, 220, 86, 160, 184, 110, 147, 35, 123, 241, 233, 99, 56, 99, 108, 107, 231, 140, 142, 117, 194, 45, 125, 175, 180, 107, 146, 228 };
    }
}
