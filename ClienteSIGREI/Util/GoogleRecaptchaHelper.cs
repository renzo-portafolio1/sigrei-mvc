﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClienteSIGREI.Util
{
    public class GoogleRecaptchaHelper
    {
        private readonly IConfiguration configuration;
        public GoogleRecaptchaHelper(IConfiguration config)
        {
            this.configuration = config;
        }

        // A function that checks reCAPTCHA results
        public async Task<bool> IsReCaptchaPassedAsync(string gRecaptchaResponse, string secret)
        {

            var proxyTres = new WebProxy("srvproxy01.osiptel.gob.pe", 3128)
            {
                UseDefaultCredentials = false,
                Credentials = CredentialCache.DefaultCredentials
            };
            var handler = new HttpClientHandler
            {
                Proxy = proxyTres,
                PreAuthenticate = true,
                UseDefaultCredentials = true
            };

            var usarProxy = configuration["usarProxy"];

            if (usarProxy.Equals("1"))
            {
                HttpClient httpClient = new HttpClient(handler);
                var content = new FormUrlEncodedContent(new[]
         {
                new KeyValuePair<string, string>("secret", secret),
                new KeyValuePair<string, string>("response", gRecaptchaResponse)
            });
                var res = await httpClient.PostAsync($"https://www.google.com/recaptcha/api/siteverify", content);
                if (res.StatusCode != HttpStatusCode.OK)
                {
                    return false;
                }

                string JSONres = res.Content.ReadAsStringAsync().Result;
                dynamic JSONdata = JObject.Parse(JSONres);
                if (JSONdata.success != "true")
                {
                    return false;
                }
            }
            else
            {
                HttpClient httpClient = new HttpClient();
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("secret", secret),
                    new KeyValuePair<string, string>("response", gRecaptchaResponse)
                });

                var res = await httpClient.PostAsync($"https://www.google.com/recaptcha/api/siteverify", content);
                if (res.StatusCode != HttpStatusCode.OK)
                {
                    return false;
                }

                string JSONres = res.Content.ReadAsStringAsync().Result;
                dynamic JSONdata = JObject.Parse(JSONres);
                if (JSONdata.success != "true")
                {
                    return false;
                }
            }




            return true;
        }

        internal static bool IsReCaptchaPassedAsync(StringValues stringValues)
        {
            var proxyTres = new WebProxy("srvproxy01.osiptel.gob.pe", 3128)
            {
                UseDefaultCredentials = false,
                Credentials = CredentialCache.DefaultCredentials
            };
            var handler = new HttpClientHandler
            {
                Proxy = proxyTres,
                PreAuthenticate = true,
                UseDefaultCredentials = true
            };

            HttpClient httpClient = new HttpClient(handler);
            var content = new FormUrlEncodedContent(new[]
     {
                new KeyValuePair<string, string>("secret", "6LfJPwATAAAAAFmb19YL_SQxvyhGTAgYD9c-p7N5"),
                new KeyValuePair<string, string>("response", stringValues)
            });
            var res = httpClient.PostAsync($"https://www.google.com/recaptcha/api/siteverify", content);
            if (res.Result.StatusCode != HttpStatusCode.OK)
            {
                return false;
            }

            string JSONres = res.Result.RequestMessage.Content.ReadAsStringAsync().ToString();
            dynamic JSONdata = JObject.Parse(JSONres);
            if (JSONdata.success != "true")
            {
                return false;
            }


            return true;
        }
    }
}
