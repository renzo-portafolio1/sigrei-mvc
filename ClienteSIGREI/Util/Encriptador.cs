﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace ClienteSIGREI.Util
{
    public class Encriptador
    {
        public string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)

        {

            string valorEncriptado = "";

            // Check arguments.

            if (plainText == null || plainText.Length <= 0)

                throw new ArgumentNullException("plainText");

            if (Key == null || Key.Length <= 0)

                throw new ArgumentNullException("Key");

            if (IV == null || IV.Length <= 0)

                throw new ArgumentNullException("IV");

            byte[] encrypted;



            // Create an Aes object

            // with the specified key and IV.

            using (Aes aesAlg = Aes.Create())

            {

                aesAlg.Key = Key;

                aesAlg.IV = IV;

                // Create an encryptor to perform the stream transform.

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);



                // Create the streams used for encryption.

                using (MemoryStream msEncrypt = new MemoryStream())

                {

                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))

                    {

                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))

                        {

                            //Write all data to the stream.

                            swEncrypt.Write(plainText);

                        }

                        encrypted = msEncrypt.ToArray();

                    }

                }

            }

            valorEncriptado = Convert.ToBase64String(encrypted);

            // Return the encrypted bytes from the memory stream.

            return valorEncriptado;

        }



        public string DecryptStringFromBytes_Aes(string encriptado, byte[] Key, byte[] IV)

        {

            encriptado = encriptado.Replace(" ", "+");

            byte[] cipherText = Convert.FromBase64String(encriptado);

            //var cipherText = Encoding.ASCII.GetBytes(encriptado);

            // Check arguments.

            if (cipherText == null || cipherText.Length <= 0)

                throw new ArgumentNullException("cipherText");

            if (Key == null || Key.Length <= 0)

                throw new ArgumentNullException("Key");

            if (IV == null || IV.Length <= 0)

                throw new ArgumentNullException("IV");



            // Declare the string used to hold

            // the decrypted text.

            string plaintext = null;



            // Create an Aes object

            // with the specified key and IV.

            using (Aes aesAlg = Aes.Create())

            {

                aesAlg.Key = Key;

                aesAlg.IV = IV;



                // Create a decryptor to perform the stream transform.

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);



                // Create the streams used for decryption.

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))

                {

                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))

                    {

                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))

                        {

                            // Read the decrypted bytes from the decrypting stream

                            // and place them in a string.

                            plaintext = srDecrypt.ReadToEnd();

                        }

                    }

                }

            }

            return plaintext;

        }
    }
}
