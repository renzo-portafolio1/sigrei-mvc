﻿
using System;

using Osiptel.SIGREI.BL;

/// <summary>
/// Descripción breve de Funciones
/// </summary>
public class Funciones
{

    public String ObtenerAnio()
    {
        String strAnio = String.Empty;
        try
        {
            GlobalBL oglAnio = new GlobalBL();
            strAnio = oglAnio.Obtener("SIGREI","ANIO_REPORTE");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            throw;
        }
        return strAnio;
    }

    public String ObtenerLDAP()
    {
        String lsLDAP = String.Empty;
        try
        {
            GlobalBL oGlobalBL = new GlobalBL();
            lsLDAP = oGlobalBL.Obtener("COMUN", "ADDRESS_LDAP");
        }catch (Exception ex){
            Console.WriteLine(ex);
            throw;
        }
        return lsLDAP;
    }

}