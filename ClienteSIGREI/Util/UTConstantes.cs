﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public static class UTConstantes
{
    public static string APLICACION = "SIGREI";
    public static string APLICACION_COMUN = "COMUN";
    public static string APLICACION_ACCESO = "ACCESO";

    public struct TILDES
    {
        public static string A = "Á";
        public static string E = "É";
        public static string I = "Í";
        public static string O = "Ó";
        public static string U = "Ú";
    }

    public struct TILDESASCII
    {
        public static string A = "&#193;";
        public static string E = "&#201;";
        public static string I = "&#205;";
        public static string O = "&#211;";
        public static string U = "&#218;";
    }

    public struct APLICACION_TABLA
    {
        public static string TIPOSOLICITANTE = "TIPOSOLICITANTE";
        //public static string TIPOSOLICITANTE = "MODULO";
        public static string TIPOREQCARGA = "SIGREI_REQUERIMIENTO";

        public static readonly string MODULO = "MODULO";
        public static readonly string DOMINIONOPERMITIDO = "DOMINIONOPERMITIDO";

        public static string TIPOESTADO = "TIPOESTADO";
        public static string TIPOMOTIVOAPROBACION = "TIPOMOTIVOAPROBACION";
        public static string TIPOMOTIVORECHAZO = "TIPOMOTIVORECHAZO";
        public static string TIPOMOTIVODEVOLVER = "TIPOMOTIVODEVOLVER";
        public static string TIPOORIGEN = "TIPOORIGEN";
        public static string TIPOOBSERVACION = "TIPOOBSERVACION";
        public static string TIPOATENCION = "TIPOATENCION";
        public static string TIPOMOTIVOVENCIMIENTO = "TIPOMOTIVOVENCIMIENTO";
        public static string TIPODOCUMENTOIDENTIDAD = "TIPODOCUMENTOIDENTIDAD";
        public static string TIPOESTADOABONADO = "TIPOESTADOABONADO";
        public static string TIPOMOTIVOREPORTE = "TIPOMOTIVOREPORTE";
        public static string TIPODOCUMENTOABONADO = "TIPODOCUMENTOABONADO";


        public static readonly string GESTIONDEPERFIL = "GESTIONDEPERFIL";
        public static readonly string GESTIONDEMAESTROS = "GESTIONDEMAESTROS";
        public static readonly string COMUN = "COMUN";
        public static readonly string INS_EXTERNA = "ACC_INSTITUCION_EXTERNA";



        public static readonly string TIPOREPORTE = "TIPOREPORTE";
        //public static readonly string TIPOREQUERIMIENTO = "TIPOREQUERIMIENTO";
        public static readonly string TIPOREQUERIMIENTO = "SIGREI_REQUERIMIENTO";

        //public static readonly string TIPODOCUMENTOEP = "TIPODOCUMENTOEP";
        public static readonly string TIPODOCUMENTOEP = "ACC_USUARIO_IE";
        public static readonly string URLSUNAT = "URLSUNAT";
        public static readonly string URLWSLOGIN = "URLWSLOGIN";
        public static readonly string URLRENIEC = "URLRENIEC";
        

    }

    public struct APLICACION_COLUMNA
    {
        public static string IDTIPOSOLICITANTE = "IDTIPOSOLICITANTE";
        //public static string IDTIPOSOLICITANTE = "IDMODULO";
        public static string IDTIPOREQCARGA = "TIP_CARGA";

        public static readonly string IDMODULO = "IDMODULO";

        public static string IDTIPOESTADO = "IDTIPOESTADO";
        public static string IDTIPOMOTIVOAPROBACION = "IDTIPOMOTIVOAPROBACION";
        public static string IDTIPOMOTIVORECHAZO = "IDTIPOMOTIVORECHAZO";
        public static string IDTIPOMOTIVODEVOLVER = "IDTIPOMOTIVODEVOLVER";
        public static string IDTIPOORIGEN = "IDTIPOORIGEN";
        public static string IDTIPOOBSERVACION = "IDTIPOOBSERVACION";
        public static string IDTIPOATENCION = "IDTIPOATENCION";
        public static string IDTIPOMOTIVOVENCIMIENTO = "IDTIPOMOTIVOVENCIMIENTO";
        public static string IDTIPODOCUMENTOIDENTIDAD = "IDTIPODOCUMENTOIDENTIDAD";
        public static string IDTIPOESTADOABONADO = "IDTIPOESTADOABONADO";
        public static string IDTIPOMOTIVOREPORTE = "IDTIPOMOTIVOREPORTE";
        public static string IDTIPODOCUMENTOABONADO = "IDTIPODOCUMENTOABONADO";


        public static readonly string IDGESTIONDEPERFIL = "IDGESTIONPERFIL";
        public static readonly string IDGESTIONDEMAESTROS = "IDGESTIONMAESTROS";

        public static readonly string IDTIPOREPORTE = "IDTIPOREPORTE";
        //public static readonly string IDTIPOREQUERIMIENTO = "IDTIPOREQUERIMIENTO";
        public static readonly string IDTIPOREQUERIMIENTO = "TIP_DATO";

        public static readonly string IDDOMINIO = "IDDOMINIO";
        public static readonly string IDSEXO = "SEXO";
        public static readonly string TIP_INSTEXT = "TIP_INSTEXT";

        //public static readonly string IDTIPODOCUMENTOEP = "IDTIPODOCUMENTOEP";
        public static readonly string IDTIPODOCUMENTOEP = "COD_DOCIDE";

    }

    public static string COMBO_SINVALOR = "";
    public static string COMBO_VALORSELECCIONE = "0";
    public static string COMBO_SELECCIONE = "Seleccione";
    public static string COMBO_VALORINICIAL = "-1";
    public static string COMBO_TODOS = "-Todos-";
    public static string COMBO_NINGUNO = "-Ninguno-";
    public static string COMBO_SEL = "-SEL-";


    public static int ID_ESTADO_REGISTRADO = 1;
    public static int ID_ESTADO_APROBADO = 2;
    public static int ID_ESTADO_ENVIADO = 3;
    public static int ID_ESTADO_ATENDIDO = 4;
    public static int ID_ESTADO_RECHAZADO = 5;
    public static int ID_ESTADO_PORATENDER = 6;
    public static int ID_ESTADO_PORDEVOLVER = 7;
    public static int ID_ESTADO_PORENVIAR = 8;
    public static int ID_ESTADO_ASIGNADO = 9;
    public static int ID_ESTADO_FINALIZADO = 10;

    public static string ESTADO_REGISTRADO = "REGISTRADO";
    public static string ESTADO_RECIBIDO = "RECIBIDO";
    public static string ESTADO_ENREVISION = "ENREVISION";
    public static string ESTADO_CONCLUIDO = "CONCLUIDO";
    public static string ESTADO_ANULADO = "ANULADO";

    public static int ID_TIPOORIGEN_VIRTUAL = 1;
    public static int ID_TIPOORIGEN_FISICO = 2;

    public static int ID_TIPOFINALIZACION_ATENDIDO = 1;
    public static int ID_TIPOFINALIZACION_INCOMPLETO = 2;

    public static int ID_TIPOATENCION_CONSERVICIO = 1;
    public static int ID_TIPOATENCION_SINSERVICIO = 2;
    public static int ID_TIPOATENCION_SININFORMACION = 3;
    public static int ID_NOEXISTE_EN_OFICIO =9;


    public static int VALOR_CERO = 0;
    public static int VALOR_UNO = 1;
    public static int VALOR_DOS = 2;
    public static int TAMANIO_UN_KB = 1000;
    public static int TAMANIO_CARGA_DOCUMENTO = 20000000;

    public static string FORMATO_FECHA = "dd/MM/yyyy";

    public static string MSG_ERROR_OPERACION = "Ocurrió un error en la operación";
    public static string MSG_ERROR_REGISTRO = "Ocurrió un error al registrar";
    public static string MSG_ERROR_REGISTRO_CONSULTE_ADMIN = "Ocurrió un error al registrar, consulte con el administrador del sistema";
    public static string MSG_ERROR_ACTUALIZO_CONSULTE_ADMIN = "Ocurrió un error al actualizar, consulte con el administrador del sistema";
    public static string MSG_ERROR_ACTUALIZO_INFO_CONSULTE_ADMIN = "Ocurrió un error al actualizar la información ingresada, consulte con el administrador del sistema";
    public static string MSG_NOEXISTE_REGISTRO = "No se encontraron registros";
    public static string MSG_EXISTE_REGISTRO = "Existe un registro";
    public static string MSG_ERROR_UBIGEO = "Debe ingresar 10 números en el Ubigeo";
    public static string MSG_ERROR_ARCHIVO = "¡Error al grabar! Nombre de archivo duplicado. Cambiar el nombre del archivo ";
    public static string MSG_REGISTRAR_OK = "Se registró correctamente.";
    public static string MSG_ELIMINAR_OK = "Se eliminó correctamente.";
    public static string MSG_MODIFICAR_OK = "Se modificó correctamente.";
    public static string MSG_ANULAR_OK = "Se anuló correctamente.";
    public static string MSG_PROCESAR_OK = "Se proceso correctamente.";
    public static string MSG_ENVIAR_OK = "Se envió correctamente.";
    public static string MSG_VALIDAR_OK = "Se validó correctamente";
    public static string MSG_INACTIVAR_OK = "Se inactivó correctamente";
    public static string MSG_EXPORTAR_OK = "Se exportó correctamente";
    public static string MSG_APROBAR_OK = "Se aprobó correctamente";
    public static string MSG_DESAPROBAR_OK = "Se desaprobó correctamente";
    public static string MSG_DESCARGAR_OK = "Se descargó correctamente";
    public static string MSG_GENERAR_OK = "Se generó correctamente";
    public static string MSG_ACTIVAR_OK = "Se activó correctamente";
    public static string MSG_DEVOLVER_OK = "Se devolvió correctamente";
    public static string MSG_AGREGAR_OK = "¡Se agrego correctamente a la empresa!";

    public static string MSG_ERROR_TAMANIO_MAXIMO_EXCEDIDO = "¡Se ha excedido el tamaño máximo de carga de documentos!";

    public static string MSG_REVISADO_OK = "Se ha revisado correctamente.";
    public static string MSG_CONCLUIDO_OK = "Se ha concluido correctamente.";

    public static string MSG_REGISTRAR_CONFIRM = "¿Desea registrar?";
    public static string MSG_ELIMINAR_CONFIRM = "¿Desea eliminar el registro seleccionado?";
    public static string MSG_MODIFICAR_CONFIRM = "¿Desea actualizar los cambios realizados?";
    public static string MSG_ANULAR_CONFIRM = "¿Desea anular el registro seleccionado?";
    public static string MSG_COMENTAR_CONFIRM = "¿Desea comentar referente a : {0} ?";
    public static string MSG_AGREGAR_CONFIRM = "¿Desea agregar a la empresa infractora?";
    public static string MSG_AGREGAR_DOCUMENTO_CONFIRM = "¿Desea agregar el documento seleccionado?";

    public static string MSG_PROCESAR_CONFIRM = "Desea procesar";
    public static string MSG_ENVIAR_CONFIRM = "Desea enviar";
    public static string MSG_VALIDAR_CONFIRM = "Desea Validar";
    public static string MSG_INACTIVAR_CONFIRM = "Desea inactivar";
    public static string MSG_GENERAR_CONFIRM = "Desea generar";
    public static string MSG_EXPORTAR_CONFIRM = "Desea exportar";
    public static string MSG_APROBAR_CONFIRM = "Desea aprobar";
    public static string MSG_DESAPROBAR_CONFIRM = "Desea desaprobar";
    public static string MSG_CERRAR_CONFIRM = "Desea cerrar";
    public static string MSG_DESCARGAR_CONFIRM = "Desea descargar";
    public static string MSG_ACTIVAR_CONFIRM = "Desea activar";

    public static string PREGUNTA_REGISTRAR_REQUERIMIENTO = "¿Desea registrar el requerimiento de información de IMEI?";
    public static string PREGUNTA_APROBAR_REQUERIMIENTO = "¿Desea aprobar el requerimiento de información de IMEI?";
    public static string PREGUNTA_ENVIAR_REQUERIMIENTO = "¿Desea enviar el requerimiento de información de IMEI?";
    public static string PREGUNTA_RECHAZAR_REQUERIMIENTO = "¿Desea rechazar el requerimiento de información de IMEI?";
    public static string PREGUNTA_ASIGNAR_REQUERIMIENTO = "¿Desea asignar el requerimiento de información de IMEI?";
    public static string PREGUNTA_ATENDER_REQUERIMIENTO = "¿Desea atender el requerimiento de información de IMEI?";
    public static string PREGUNTA_ACTUALIZAR_IMEI_REQUERIMIENTO = "¿Desea actualizar la información de IMEI con el archivo adjunto?";
    public static string PREGUNTA_FINALIZAR_REQUERIMIENTO = "¿Desea finalizar el requerimiento de información de IMEI?";
    public static string PREGUNTA_RESPONDER_REQUERIMIENTO = "¿Desea responder con número de servicio el requerimiento de información de IMEI?";
    public static string PREGUNTA_RESPONDERSN_REQUERIMIENTO = "¿Desea responder sin número de servicio el requerimiento de información de IMEI?";
    public static string PREGUNTA_DEVOLVER_REQUERIMIENTO = "¿Desea devolver el requerimiento de información de IMEI?";
    public static string PREGUNTA_DESCARTAR_REQUERIMIENTO = "¿Desea descartar el requerimiento de información de IMEI?";
    public static string PREGUNTA_ATENDER_EO_REQUERIMIENTO = "¿Desea atender el requerimiento y la carga de información de IMEI?";
    public static string PREGUNTA_ATENDER_REQUERIMIENTO_PARCIAL = "¿Desea atender el requerimiento de información de IMEI?";
    public static string PREGUNTA_CARGAR_ARCHIVO_IMEI = "¿Desea cargar la información del archivo IMEI?";
    public static string PREGUNTA_AGREGAR_REQUERIMIENTO = "¿Desea registar el IMEI en el requerimiento de información?";

    public static string PREGUNTA_ACTUALIZAR_IMEI_REQUERIMIENTO_CARGADO = "¿Desea actualizar la información de IMEI cargado";

    public static string PREGUNTA_GRABAR_ARCHIVO_IMEI = "¿Desea grabar la información del archivo IMEI?";

    public static string PREGUNTA_REGISTRAR_USUARIO_EO = "¿Desea registrar el usuario operador?";
    public static string PREGUNTA_ACTUALIZAR_USUARIO_EO = "¿Desea actualizar el usuario operador?";
    public static string PREGUNTA_ACTUALIZAR_CLAVE_USUARIO_EO = "¿Desea actualizar la clave del usuario operador?";


    public static string MSG_REGISTRO_USUARIO_EO_OK = "Se registró correctamente el usuario operador ";
    public static string MSG_ACTUALIZO_USUARIO_EO_OK = "Se actualizó correctamente el usuario operador ";
    public static string MSG_ACTUALIZO_ESTADO_USUARIO_EO_OK = "Se actualizó correctamente el estado del usuario operador ";
    public static string MSG_ACTUALIZO_CLAVE_USUARIO_EO_OK = "Se actualizó correctamente la clave";

    public static string ALERT_EXISTE_USUARIO_EO = "Ya existe un usuario con ese nombre, ingrese uno diferente ";

    public static string MENSAJE_VALIDACION = "¡Mensaje de validación!";

    public static string MSG_REGISTRO_REQUERIMIENTO_OK = "Se registró correctamente el requerimiento ";
    public static string MSG_REGISTRO_REQUERIMIENTO_ARCHIVO_OK = "Se grabo correctamente los requerimientos";
    public static string MSG_ERROR_REGISTRO_REQUERIMIENTO = "Ocurrió un error al registrar el requerimiento";
    public static string MSG_ERROR_GRABAR = "Ocurrió un error al grabar la información";

    public static string MSG_APROBACION_REQUERIMIENTO_OK = "Se aprobó correctamente el requerimiento de información";
    public static string MSG_RECHAZO_REQUERIMIENTO_OK = "Se rechazado correctamente el requerimiento de información";
    public static string MSG_ENVIO_REQUERIMIENTO_OK = "Se envió correctamente el requerimiento de información";
    public static string MSG_ATENDIDO_REQUERIMIENTO_OK = "Se atendió correctamente el requerimiento de información";
    public static string MSG_ASIGNADO_REQUERIMIENTO_OK = "Se asignó correctamente el requerimiento de información";
    public static string MSG_ACTUALIZACION_IMEI_OK = "Se actualizo la información de IMEI correctamente";

    public static string MSG_CLAVE_INCORRECTO_REQUERIMIENTO = "La clave que ingreso es incorrecto";
    public static string MSG_DOCUMENTO_NO_EXISTE = "El documento que intenta descargar no existe, consulte con su administrador";
    public static string MSG_VALIDACION_LISTA_IMEI_VACIO = "Debe de cargar el archivo con la lista SISDOC y códigos IMEIs";
    public static string USUARIO_SESION = "UsuarioSistema";
    public static string MENSAJE_SISDOC_SELECCION_REQUERIDO = "¡Debe de seleccionar al menos un registro!";


    public static string SISDOC_ANALIZ_REQUERIMIENTO = "ListadoSeleccionAnalizReque";

    public static string MSG_DOCUMENTO_EXCEL_REGISTRO_PUBLICO_VACIO = "El archivo IMEI adjunto no contiene información de IMEIs";
    public static string PLANTILLA_EXCEL_NO_EXISTE_HOJA = "El documento no contiene la Hoja IMEI, vuelva a cargar un documento correcto";
    public static string PLANTILLA_EXCEL_NO_EXISTE_HOJA_DATA = "El documento no contiene la Hoja DATA, vuelva a cargar un documento correcto";

    public static string IMEI_POR_RESPONDER = "ListadoImeisPorResponder";
    public static string IMEI_POR_DEVOLVER = "ListadoImeisPorDevolver";

    public static string MENSAJE_ERROR_PROCESO_TRANFORMACIONWORD = "Se ha tenido problema al fusionar el documento";

    public static string DOCUMENTO_GENERADO_RESPONDER_IMEI = "Documento generado automatico para responder a usuario solicitante";
    public static string DOCUMENTO_ADJUNTADO_RESPONDER_IMEI = "Documento Adjuntado para responder a usuario solicitante";
    public static string DOCUMENTO_ADJUNTADO_DEVOLVER_IMEI = "Documento Adjuntado para devolver al usuario solicitante";

    public static string CARTA_RESPONDER_CON_SERVICIO_INDIVIDUAL = "CartaResponderConServicioIndividual.docx";
    public static string CARTA_RESPONDER_SIN_SERVICIO_INDIVIDUAL = "CartaResponderSinServicioIndividual.docx";
    public static string CARTA_RESPONDER_VARIOS = "CartaResponderVarios.docx";

    public static string CARTA_DEVOLVER_INDIVIDUAL = "CartaDevolverIndividual.docx";
    public static string CARTA_DEVOLVER_VARIOS = "CartaDevolverVarios.docx";

    public static string CARTA_DEVOLVER_IMEI_DIGITO_INCORRECTO = "CartaDevolverImeiDigitoIncorrecto.docx";
    public static string CARTA_DEVOLVER_IMEI_INVALIDO= "CartaDevolverImeiInvalido.docx";
    public static string CARTA_DEVOLVER_IMEI_NOROBADO= "CartaDevolverImeiNoRobado.docx";
    public static string CARTA_DEVOLVER_IMEI_LOGICO_SISDOC= "CartaDevolverImeiLogicoSisDoc.docx";
    public static string CARTA_DEVOLVER_IMEI_NO_BD = "CartaDevolverImeiNoBD.docx";
    public static string CARTA_DEVOLVER_IMEI_NO_EO = "CartaDevolverImeiNoEO.docx";
    public static string CARTA_DEVOLVER_IMEI_CARACTER_NONUMERICO = "CartaDevolverImeiCaracteresNoNumerico.docx";
    public static string CARTA_DEVOLVER_IMEI_LUNH_INVALIDO = "CartaDevolverImeiLunhInvalido.docx";
    public static string CARTA_DEVOLVER_IMEI_NO_OFICIO = "CartaDevolverImeiNoExisteOficio.docx";
    public static string CARTA_DEVOLVER_IMEI_NO_REPORTE = "CartaDevolverImeiNoRealiceReporteÑ.docx";
    public static string CARTA_DEVOLVER_IMEI_OTRO= "CartaDevolverImeiOtros.docx";



    public static string DESCTIPODOCIDENTIDAD_DNI = "DNI";
    public static string DESCTIPODOCIDENTIDAD_RUC = "RUC";
    public static string DESCTIPODOCIDENTIDAD_CARNET = "Carné de Extranjería";
    public static string DESCTIPODOCIDENTIDAD_PASAPORTE = "Pasaporte";
    public static string DESCTIPODOCIDENTIDAD_DOCUMENTO_LEGAL = "Documento Legal de Identidad";


    public static int ID_TIPOMOTIVODEVOLVER_DIGINC = 1;	//IMEI dígitos incorrecto
    public static int ID_TIPOMOTIVODEVOLVER_INVAL = 2;	//	IMEI inválido
    public static int ID_TIPOMOTIVODEVOLVER_NOROB = 3;	//	IMEI no robado
    public static int ID_TIPOMOTIVODEVOLVER_LOGICO = 4;	//	IMEI lógico por SISDOC
    public static int ID_TIPOMOTIVODEVOLVER_NOBD = 5;	//	IMEI No se encuentra en la BD
    public static int ID_TIPOMOTIVODEVOLVER_NOEO = 6;	//	IMEI Empresa no encontrado
    public static int ID_TIPOMOTIVODEVOLVER_NONUM = 7;	//	IMEI caracteres no numéricos
    public static int ID_TIPOMOTIVODEVOLVER_NOLUNH = 8;	//	IMEI no cumple algoritmo Lunh
    public static int ID_TIPOMOTIVODEVOLVER_NOOFICIO = 9;	//	IMEI no cumple algoritmo Lunh


    public const Int16 IND_ACTIVO = 1;

}


