﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de DeclaracionJurada
/// </summary>
public class ErrorImei
{
    public ErrorImei()
	{
		//
		// TODO: Agregar aquí la lógica del constructor
		//

	}
    public Int32 nroItem { get; set; }
    public String nroImei{ get; set; }
    public string codError { get; set; }    

}

public class ErrorUser
{
    public ErrorUser()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //

    }
    public Int32 nroItem { get; set; }
    public String dni { get; set; }
    public string error { get; set; }

}
public class DataTableParamModel
{
    public string draw { get; set; }
    public int length { get; set; }
    public int start { get; set; }
    public List<Order> order { get; set; }
    public List<Column> columns { get; set; }
    public Search search { get; set; }
}
public class Order
{
    public int column { get; set; }
    public string dir { get; set; }
}
public class Column
{
    public string data { get; set; }
    public string name { get; set; }
    public bool searchable { get; set; }
    public bool orderable { get; set; }
    public Search search { get; set; }
}
public class Search
{
    public string value { get; set; }
    public string regex { get; set; }
}